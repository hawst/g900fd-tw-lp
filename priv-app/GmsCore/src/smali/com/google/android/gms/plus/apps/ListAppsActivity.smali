.class public final Lcom/google/android/gms/plus/apps/ListAppsActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/plus/apps/c;

.field private final b:Ljava/util/ArrayList;

.field private c:Lcom/google/android/gms/plus/apps/bf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    return-void
.end method

.method private a(Lcom/google/android/gms/plus/apps/bf;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 352
    invoke-virtual {p1}, Lcom/google/android/gms/plus/apps/bf;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "connected_apps"

    if-ne v0, v1, :cond_1

    .line 353
    sget v0, Lcom/google/android/gms/j;->gh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 357
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 358
    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 359
    invoke-virtual {v0, p1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 360
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    if-eq v1, p1, :cond_0

    .line 361
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 363
    :cond_0
    invoke-virtual {v0, v2}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/aj;

    .line 364
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 365
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/apps/bf;->b(Landroid/support/v7/app/a;)V

    .line 366
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    .line 367
    return-void

    .line 355
    :cond_1
    sget v0, Lcom/google/android/gms/j;->gh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final i_()Z
    .locals 1

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->finish()V

    .line 262
    const/4 v0, 0x1

    return v0
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 191
    packed-switch p1, :pswitch_data_0

    .line 224
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/d;->onActivityResult(IILandroid/content/Intent;)V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 195
    :pswitch_0
    if-ne p2, v0, :cond_0

    .line 196
    const-string v0, "com.google.android.gms.plus.DISCONNECTED_APP_ID"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    if-eqz v0, :cond_0

    .line 201
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/c;->c(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/bf;->b(Landroid/support/v7/app/a;)V

    goto :goto_0

    .line 208
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/apps/c;->b(I)V

    goto :goto_0

    .line 211
    :pswitch_2
    if-ne p2, v0, :cond_0

    .line 212
    const-string v0, "deleted_moment_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v2, "activity_log"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/bd;

    .line 216
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/bd;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    const-string v0, "AppSettings"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->setResult(I)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->finish()V

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 80
    const-string v0, "com.google.android.gms.plus.action.VIEW_ACTIVITY_LOG"

    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 83
    const-string v0, "com.google.android.gms.extras.ACCOUNT_NAME"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 84
    if-nez v1, :cond_4

    .line 86
    if-eqz v7, :cond_2

    const-string v0, "com.google.android.gms.extras.APP_ID"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "application"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 89
    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/c;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 91
    :cond_3
    if-nez v1, :cond_4

    .line 92
    const-string v0, "AppSettings"

    const-string v1, "This activity is requires an account name."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->setResult(I)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->finish()V

    goto :goto_0

    .line 99
    :cond_4
    sget v0, Lcom/google/android/gms/l;->dM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->setContentView(I)V

    .line 101
    sget v0, Lcom/google/android/gms/j;->fw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 103
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    const/16 v2, 0x14

    const/16 v3, 0x1c

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/a;->a(II)V

    .line 109
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v5

    .line 110
    if-nez v5, :cond_5

    .line 111
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 116
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v2

    .line 117
    const-string v0, "apps_util"

    invoke-virtual {v2, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/c;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    if-nez v0, :cond_6

    .line 119
    invoke-static {v1, v5}, Lcom/google/android/gms/plus/apps/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/apps/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/apps/c;->setRetainInstance(Z)V

    .line 121
    invoke-virtual {v2}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    const-string v3, "apps_util"

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 124
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, v5}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v9

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/support/v4/app/aj;->a(I)Landroid/support/v4/app/aj;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v0, "com.google.android.gms.extras.APP_ID"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v0, "application"

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    const/4 v2, 0x0

    if-eqz v8, :cond_18

    const-string v2, "com.google.android.gms.extras.COLLECTION_FILTER"

    invoke-virtual {v10, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v11, :cond_a

    if-eqz v2, :cond_a

    const-string v0, "AppSettings"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "AppSettings"

    const-string v2, "EXTRA_APP_ID and EXTRA_COLLECTION_FILTER filters for Activity Log are mutually exclusive."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->finish()V

    .line 127
    :goto_1
    const/4 v3, 0x0

    .line 128
    const/4 v4, 0x0

    .line 130
    if-eqz p1, :cond_13

    .line 132
    const-string v0, "selected_page_tag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 154
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/bf;

    .line 156
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/apps/bf;

    .line 157
    invoke-virtual {v2}, Lcom/google/android/gms/plus/apps/bf;->getTag()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    move-object v6, v2

    .line 162
    :goto_3
    if-eqz v3, :cond_9

    .line 165
    sget-object v2, Lcom/google/android/gms/common/analytics/c;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 168
    :cond_9
    invoke-direct {p0, v6}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a(Lcom/google/android/gms/plus/apps/bf;)V

    goto/16 :goto_0

    :cond_a
    move-object v4, v2

    .line 124
    :goto_4
    if-eqz v7, :cond_b

    if-nez v11, :cond_e

    if-nez v0, :cond_e

    if-nez v4, :cond_e

    :cond_b
    const-string v2, "connected_apps"

    invoke-virtual {v3, v2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/apps/bf;

    if-nez v2, :cond_d

    const-string v2, "com.google.android.gms.extras.PRESELECTED_FILTER"

    const/4 v12, -0x1

    invoke-virtual {v10, v2, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/4 v12, -0x1

    if-ne v2, v12, :cond_c

    const-string v2, "com.google.android.gms.extras.ALL_APPS"

    const/4 v12, 0x0

    invoke-virtual {v10, v2, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_12

    const/4 v2, 0x0

    :cond_c
    :goto_5
    invoke-static {v2}, Lcom/google/android/gms/plus/apps/k;->b(I)Lcom/google/android/gms/plus/apps/k;

    move-result-object v2

    sget v12, Lcom/google/android/gms/j;->gq:I

    const-string v13, "connected_apps"

    invoke-virtual {v9, v12, v2, v13}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    :cond_d
    invoke-virtual {v9, v2}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    iget-object v12, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    const-string v2, "activity_log"

    invoke-virtual {v3, v2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/apps/bf;

    if-nez v2, :cond_11

    if-nez v0, :cond_10

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-eqz v8, :cond_f

    const-string v2, "com.google.android.gms.extras.APP_NAME"

    invoke-virtual {v10, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v2, "com.google.android.gms.extras.APP_ICON_URL"

    invoke-virtual {v10, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_f
    if-eqz v11, :cond_10

    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-direct {v0, v3, v2, v11}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    invoke-static {v0, v4}, Lcom/google/android/gms/plus/apps/bd;->a(Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/lang/String;)Lcom/google/android/gms/plus/apps/bd;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->gq:I

    const-string v3, "activity_log"

    invoke-virtual {v9, v0, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    :cond_11
    invoke-virtual {v9, v2}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9}, Landroid/support/v4/app/aj;->a()I

    goto/16 :goto_1

    :cond_12
    const/4 v2, 0x1

    goto :goto_5

    .line 133
    :cond_13
    if-eqz v7, :cond_14

    .line 135
    const-string v0, "activity_log"

    .line 136
    sget-object v4, Lcom/google/android/gms/common/analytics/d;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 137
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 139
    sget-object v3, Lcom/google/android/gms/common/analytics/j;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v6, v0

    goto/16 :goto_2

    .line 143
    :cond_14
    const-string v0, "connected_apps"

    .line 144
    const-string v2, "com.google.android.gms.extras.ALL_APPS"

    const/4 v3, 0x0

    invoke-virtual {v6, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 146
    sget-object v3, Lcom/google/android/gms/common/analytics/d;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 147
    sget-object v4, Lcom/google/android/gms/common/analytics/d;->q:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v6, v0

    goto/16 :goto_2

    .line 150
    :cond_15
    sget-object v3, Lcom/google/android/gms/plus/a/b;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 151
    sget-object v4, Lcom/google/android/gms/common/analytics/d;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v6, v0

    goto/16 :goto_2

    :cond_16
    move-object v6, v0

    goto/16 :goto_3

    :cond_17
    move-object v6, v0

    goto/16 :goto_2

    :cond_18
    move-object v4, v2

    goto/16 :goto_4
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/apps/bf;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    move v1, v2

    .line 234
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/bf;

    .line 237
    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    if-eq v0, v3, :cond_0

    const/16 v3, 0x15

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 238
    const/4 v3, 0x2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/bf;->c()I

    move-result v0

    invoke-interface {p1, v2, v1, v3, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 234
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 241
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected final onDestroy()V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0}, Landroid/support/v7/app/d;->onDestroy()V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/c;->a()V

    .line 181
    :cond_0
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 246
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->b:Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/bf;

    .line 248
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    if-eq v0, v2, :cond_0

    .line 249
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a:Lcom/google/android/gms/plus/apps/c;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/apps/bf;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/bf;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 251
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->a(Lcom/google/android/gms/plus/apps/bf;)V

    .line 252
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ListAppsActivity;->supportInvalidateOptionsMenu()V

    :cond_0
    move v0, v1

    .line 256
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/apps/bf;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 185
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 186
    const-string v0, "selected_page_tag"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ListAppsActivity;->c:Lcom/google/android/gms/plus/apps/bf;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/apps/bf;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    return-void
.end method
