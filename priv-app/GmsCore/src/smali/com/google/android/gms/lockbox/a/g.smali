.class public final Lcom/google/android/gms/lockbox/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/lockbox/m;


# instance fields
.field private final a:Landroid/app/ActivityManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/g;->a:Landroid/app/ActivityManager;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(J)Ljava/util/List;
    .locals 7

    .prologue
    .line 38
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/g;->a:Landroid/app/ActivityManager;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 40
    const/4 v0, -0x1

    .line 41
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 42
    add-int/lit8 v1, v1, 0x1

    .line 43
    new-instance v4, Lcom/google/android/gms/lockbox/a/d;

    invoke-direct {v4}, Lcom/google/android/gms/lockbox/a/d;-><init>()V

    iput-wide p1, v4, Lcom/google/android/gms/lockbox/a/d;->a:J

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/google/android/gms/lockbox/a/d;->b:Z

    iput v1, v4, Lcom/google/android/gms/lockbox/a/d;->d:I

    iget-object v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    if-eqz v5, :cond_0

    iget-object v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-static {v5}, Lcom/google/android/gms/lockbox/a/f;->a(Landroid/content/ComponentName;)Lcom/google/android/gms/lockbox/a/c;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/gms/lockbox/a/d;->e:Lcom/google/android/gms/lockbox/a/c;

    :cond_0
    iget-object v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->description:Ljava/lang/CharSequence;

    if-eqz v5, :cond_1

    iget-object v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->description:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/gms/lockbox/a/d;->f:Ljava/lang/String;

    :cond_1
    iget v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    iput v5, v4, Lcom/google/android/gms/lockbox/a/d;->g:I

    iget v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->numActivities:I

    iput v5, v4, Lcom/google/android/gms/lockbox/a/d;->h:I

    iget v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->numRunning:I

    iput v5, v4, Lcom/google/android/gms/lockbox/a/d;->i:I

    iget-object v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v5, :cond_2

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-static {v0}, Lcom/google/android/gms/lockbox/a/f;->a(Landroid/content/ComponentName;)Lcom/google/android/gms/lockbox/a/c;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/lockbox/a/d;->j:Lcom/google/android/gms/lockbox/a/c;

    .line 44
    :cond_2
    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    :cond_3
    return-object v2
.end method
