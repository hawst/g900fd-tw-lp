.class public final Lcom/google/android/gms/ads/jams/NegotiationService;
.super Lcom/google/android/gms/gcm/ae;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/ads/jams/b;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/gms/ads/jams/b;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/jams/b;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/jams/NegotiationService;-><init>(Lcom/google/android/gms/ads/jams/b;)V

    .line 17
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/ads/jams/b;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/gcm/ae;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/gms/ads/jams/NegotiationService;->a:Lcom/google/android/gms/ads/jams/b;

    .line 21
    return-void
.end method

.method static b()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 52
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/gcm/an;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/an;-><init>()V

    const-class v2, Lcom/google/android/gms/ads/jams/NegotiationService;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/an;

    move-result-object v2

    sget-object v0, Lcom/google/android/gms/ads/d;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v4, v6

    invoke-virtual {v2, v6, v7, v4, v5}, Lcom/google/android/gms/gcm/an;->a(JJ)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    const-string v2, "jams-negotiation-task"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/gcm/an;->a(Z)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/an;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[JAMS] Next negotiation to run in 0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method static c()V
    .locals 3

    .prologue
    .line 66
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v0

    const-string v1, "jams-negotiation-task"

    const-class v2, Lcom/google/android/gms/ads/jams/NegotiationService;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/gcm/y;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 68
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/gcm/be;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const-wide/16 v12, 0x0

    const/4 v1, 0x0

    .line 35
    iget-object v0, p1, Lcom/google/android/gms/gcm/be;->a:Ljava/lang/String;

    .line 36
    const-string v3, "jams-negotiation-task"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[JAMS] Unexpected task tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 38
    const/4 v0, 0x2

    .line 45
    :goto_0
    return v0

    .line 41
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/ads/jams/NegotiationService;->a:Lcom/google/android/gms/ads/jams/b;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "[JAMS] Negotiator starts running at "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    iget-object v4, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    iget-object v0, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v5, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v6, "temporal_cookie_expiration_timestamp"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v6, "temporal_cookie_expiration_timestamp"

    invoke-interface {v5, v6, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-gez v0, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v5, "temporal_cookie"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v5, "temporal_cookie_expiration_timestamp"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/gms/ads/jams/c;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    iget-object v0, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/c;->b()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_9

    iget-object v0, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/c;->c()V

    :cond_2
    :goto_3
    move v0, v1

    .line 45
    goto :goto_0

    :cond_3
    move v0, v1

    .line 41
    goto :goto_1

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/ads/jams/c;->a()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_5

    const-string v0, "[JAMS] No account to perform negotiation for"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    iget-object v0, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/d;->a()V

    move v0, v1

    goto :goto_2

    :cond_5
    iget-object v5, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v5}, Lcom/google/android/gms/ads/jams/d;->e()Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v5}, Lcom/google/android/gms/ads/jams/d;->a()V

    iget-object v4, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/ads/jams/d;->a(Ljava/lang/String;)V

    :cond_6
    :goto_4
    move v0, v2

    goto :goto_2

    :cond_7
    iget-object v5, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v5}, Lcom/google/android/gms/ads/jams/d;->b()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v5}, Lcom/google/android/gms/ads/jams/d;->a()V

    iget-object v4, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/ads/jams/d;->a(Ljava/lang/String;)V

    goto :goto_4

    :cond_8
    iget-object v0, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v5, "scheduled_next_negotiation_timestamp"

    invoke-interface {v0, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v4, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v5, "scheduled_next_negotiation_timestamp"

    invoke-interface {v0, v5, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    iget-object v0, v4, Lcom/google/android/gms/ads/jams/c;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-ltz v0, :cond_6

    const-string v0, "[JAMS] Too early to perform negotiation"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/ads/jams/c;->a(J)V

    move v0, v1

    goto/16 :goto_2

    :cond_9
    iget-object v5, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    iget-object v6, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    sget-object v0, Lcom/google/android/gms/ads/d;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    :goto_5
    const-string v6, ""

    invoke-static {v6, v0}, Lcom/google/android/a/t;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/a/t;

    move-result-object v6

    invoke-interface {v6, v0}, Lcom/google/android/a/p;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Lcom/google/l/a/a/b;

    invoke-direct {v6}, Lcom/google/l/a/a/b;-><init>()V

    iget-object v7, v5, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v7, v7, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v8, "temporal_cookie"

    invoke-interface {v7, v8}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, v5, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v7, v7, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v8, "temporal_cookie"

    const-string v9, ""

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/l/a/a/b;->a:Ljava/lang/String;

    :cond_a
    iput-object v0, v6, Lcom/google/l/a/a/b;->b:Ljava/lang/String;

    const-string v0, "gmscore"

    iput-object v0, v6, Lcom/google/l/a/a/b;->c:Ljava/lang/String;

    new-instance v0, Lcom/google/l/a/a/e;

    invoke-direct {v0}, Lcom/google/l/a/a/e;-><init>()V

    new-instance v7, Lcom/google/l/a/a/e;

    invoke-direct {v7}, Lcom/google/l/a/a/e;-><init>()V

    const-string v0, "20141203-01"

    iput-object v0, v7, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v8, "time_to_next_negotiation_mins"

    invoke-interface {v0, v8}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, v5, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v8, "time_to_next_negotiation_mins"

    invoke-interface {v0, v8, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    long-to-int v0, v8

    iput v0, v7, Lcom/google/l/a/a/e;->b:I

    :cond_b
    iget-object v0, v5, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/d;->f()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, v5, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/d;->d()J

    move-result-wide v8

    long-to-int v0, v8

    iput v0, v7, Lcom/google/l/a/a/e;->c:I

    :cond_c
    iget-object v0, v5, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v8, "ideal_next_negotiation_timestamp"

    invoke-interface {v0, v8}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v8, v5, Lcom/google/android/gms/ads/jams/c;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v8}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    iget-object v5, v5, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v5, v5, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    const-string v10, "ideal_next_negotiation_timestamp"

    invoke-interface {v5, v10, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    sub-long/2addr v8, v10

    invoke-static {v12, v13, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v8

    long-to-int v0, v8

    iput v0, v7, Lcom/google/l/a/a/e;->d:I

    :cond_d
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->b()I

    move-result v0

    iput v0, v7, Lcom/google/l/a/a/e;->f:I

    new-instance v5, Lcom/google/l/a/a/f;

    invoke-direct {v5}, Lcom/google/l/a/a/f;-><init>()V

    sget-object v0, Lcom/google/android/gms/ads/d;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iput-wide v8, v5, Lcom/google/l/a/a/f;->a:J

    sget-object v0, Lcom/google/android/gms/ads/d;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v5, Lcom/google/l/a/a/f;->b:Z

    sget-object v0, Lcom/google/android/gms/ads/d;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iput-wide v8, v5, Lcom/google/l/a/a/f;->c:J

    iput-object v5, v7, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    iput-object v7, v6, Lcom/google/l/a/a/b;->f:Lcom/google/l/a/a/e;

    iget-object v0, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    invoke-static {v6, v4}, Lcom/google/android/gms/ads/jams/c;->a(Lcom/google/l/a/a/b;Ljava/lang/String;)Lcom/google/l/a/a/c;

    move-result-object v4

    if-nez v4, :cond_f

    const-string v0, "[JAMS] Failed to negotiate"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/c;->c()V

    goto/16 :goto_3

    :cond_e
    iget-object v0, v6, Lcom/google/android/gms/ads/jams/c;->a:Landroid/content/Context;

    goto/16 :goto_5

    :cond_f
    iget-object v0, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    if-eqz v4, :cond_10

    iget-object v0, v4, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, v4, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    iget-wide v6, v4, Lcom/google/l/a/a/c;->b:J

    sget-object v0, Lcom/google/android/gms/ads/d;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-ltz v0, :cond_10

    iget-wide v6, v4, Lcom/google/l/a/a/c;->b:J

    sget-object v0, Lcom/google/android/gms/ads/d;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-gtz v0, :cond_10

    move v0, v2

    :goto_6
    if-nez v0, :cond_11

    const-string v0, "[JAMS] Got an invalid response"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/c;->c()V

    goto/16 :goto_3

    :cond_10
    move v0, v1

    goto :goto_6

    :cond_11
    iget-object v2, v3, Lcom/google/android/gms/ads/jams/b;->a:Lcom/google/android/gms/ads/jams/c;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/jams/c;->d()Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "[JAMS] Drop negotiation response"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_12
    sget-object v0, Lcom/google/android/gms/ads/d;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-wide v8, v4, Lcom/google/l/a/a/c;->b:J

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    iget-object v3, v2, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v5, v4, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/gms/ads/jams/d;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    sget-object v0, Lcom/google/android/gms/ads/d;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    add-long/2addr v8, v10

    iget-object v0, v3, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "temporal_cookie"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "temporal_cookie_expiration_timestamp"

    invoke-interface {v0, v3, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, v2, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-wide v8, v4, Lcom/google/l/a/a/c;->b:J

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "time_to_next_negotiation_mins"

    invoke-interface {v0, v3, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, v2, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v0, v12, v13}, Lcom/google/android/gms/ads/jams/d;->a(J)V

    iget-object v0, v2, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v3, v2, Lcom/google/android/gms/ads/jams/c;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, v4, Lcom/google/l/a/a/c;->b:J

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v4, v8

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "ideal_next_negotiation_timestamp"

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/ads/jams/c;->a(J)V

    goto/16 :goto_3
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/ads/jams/d;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/jams/d;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    invoke-static {}, Lcom/google/android/gms/ads/jams/NegotiationService;->b()V

    .line 31
    :cond_0
    return-void
.end method
