.class public final Lcom/google/android/gms/games/ui/cy;
.super Lcom/google/android/gms/games/ui/cv;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Landroid/view/View$OnClickListener;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 72
    const/4 v8, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/games/ui/cy;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;I)V

    .line 74
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/cv;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/google/android/gms/games/ui/cy;->a:Landroid/content/Context;

    .line 80
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cy;->b:Landroid/view/LayoutInflater;

    .line 82
    iput p2, p0, Lcom/google/android/gms/games/ui/cy;->c:I

    .line 83
    iput p3, p0, Lcom/google/android/gms/games/ui/cy;->d:I

    .line 84
    iput p4, p0, Lcom/google/android/gms/games/ui/cy;->e:I

    .line 86
    iput-object p5, p0, Lcom/google/android/gms/games/ui/cy;->g:Landroid/view/View$OnClickListener;

    .line 87
    iput-object p6, p0, Lcom/google/android/gms/games/ui/cy;->h:Ljava/lang/String;

    .line 88
    iput-object p7, p0, Lcom/google/android/gms/games/ui/cy;->i:Ljava/lang/String;

    .line 90
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/cy;->j:I

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/cy;->f:I

    .line 92
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/cy;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/gms/games/ui/cy;->f:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/cy;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/gms/games/ui/cy;->j:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/cy;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cy;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/cy;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cy;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/cy;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cy;->g:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/cy;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cy;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/cy;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/gms/games/ui/cy;->c:I

    return v0
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/cy;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/gms/games/ui/cy;->d:I

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/games/ui/cy;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/gms/games/ui/cy;->e:I

    return v0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 96
    if-nez p2, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cy;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->bS:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/al;->c(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/gms/games/ui/cz;

    invoke-direct {v0, p0, p0, p2}, Lcom/google/android/gms/games/ui/cz;-><init>(Lcom/google/android/gms/games/ui/cy;Lcom/google/android/gms/games/ui/o;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 99
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/cz;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/cz;->a()V

    .line 101
    return-object p2
.end method
