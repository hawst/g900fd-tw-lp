.class final Lcom/google/android/gms/fitness/sensors/a/r;
.super Lcom/google/android/gms/fitness/sensors/a/o;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/sensors/a/e;

.field private final c:Landroid/bluetooth/BluetoothGattCharacteristic;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/sensors/a/e;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 1

    .prologue
    .line 645
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/a/o;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;B)V

    .line 646
    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/a/r;->c:Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 647
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/sensors/a/e;Landroid/bluetooth/BluetoothGattCharacteristic;B)V
    .locals 0

    .prologue
    .line 642
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/sensors/a/r;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;Landroid/bluetooth/BluetoothGattCharacteristic;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 652
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    const-string v1, "stopListening to characteristic %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/r;->c:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/fitness/sensors/a/ae;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 655
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v1, v0, Lcom/google/android/gms/fitness/sensors/a/e;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 656
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    .line 657
    const-string v0, "Not running stop listening command, no GATT."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 658
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/ae;->a()V

    .line 659
    monitor-exit v1

    .line 685
    :goto_0
    return-void

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/r;->c:Landroid/bluetooth/BluetoothGattCharacteristic;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 662
    const-string v0, "Couldn\'t disable notification for characteristic %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/a/r;->c:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 666
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/r;->c:Landroid/bluetooth/BluetoothGattCharacteristic;

    sget-object v2, Lcom/google/android/gms/fitness/sensors/a/d;->a:Lcom/google/android/gms/fitness/sensors/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/sensors/a/d;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    .line 669
    if-nez v0, :cond_2

    .line 670
    const-string v0, "Couldn\'t unsubscribe from characteristic %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/a/r;->c:Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/ae;->a()V

    .line 673
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 685
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 675
    :cond_2
    :try_start_1
    sget-object v2, Landroid/bluetooth/BluetoothGattDescriptor;->DISABLE_NOTIFICATION_VALUE:[B

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    move-result v2

    if-nez v2, :cond_3

    .line 676
    const-string v2, "Couldn\'t disable descriptor %s notification value"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 678
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/ae;->a()V

    .line 679
    monitor-exit v1

    goto :goto_0

    .line 681
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v2, v2, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v2, v0}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 682
    const-string v2, "Couldn\'t write descriptor %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 683
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/r;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/ae;->a()V

    .line 685
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 690
    const-string v0, "StopListeningCommand{mCharacteristic=%s}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/r;->c:Landroid/bluetooth/BluetoothGattCharacteristic;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
