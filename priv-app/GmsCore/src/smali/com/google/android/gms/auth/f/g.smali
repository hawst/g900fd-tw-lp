.class public final Lcom/google/android/gms/auth/f/g;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:F

.field private b:Z

.field private c:F

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1041
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 1046
    iput v0, p0, Lcom/google/android/gms/auth/f/g;->c:F

    .line 1063
    iput v0, p0, Lcom/google/android/gms/auth/f/g;->a:F

    .line 1080
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/g;->f:Z

    .line 1120
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/f/g;->g:I

    .line 1041
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1123
    iget v0, p0, Lcom/google/android/gms/auth/f/g;->g:I

    if-gez v0, :cond_0

    .line 1125
    invoke-virtual {p0}, Lcom/google/android/gms/auth/f/g;->b()I

    .line 1127
    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/f/g;->g:I

    return v0
.end method

.method public final a(F)Lcom/google/android/gms/auth/f/g;
    .locals 1

    .prologue
    .line 1050
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/g;->b:Z

    .line 1051
    iput p1, p0, Lcom/google/android/gms/auth/f/g;->c:F

    .line 1052
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/auth/f/g;
    .locals 1

    .prologue
    .line 1084
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/g;->e:Z

    .line 1085
    iput-boolean p1, p0, Lcom/google/android/gms/auth/f/g;->f:Z

    .line 1086
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 1038
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/g;->a(F)Lcom/google/android/gms/auth/f/g;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/g;->b(F)Lcom/google/android/gms/auth/f/g;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/g;->a(Z)Lcom/google/android/gms/auth/f/g;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 1109
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/g;->b:Z

    if-eqz v0, :cond_0

    .line 1110
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/auth/f/g;->c:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IF)V

    .line 1112
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/g;->d:Z

    if-eqz v0, :cond_1

    .line 1113
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/auth/f/g;->a:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IF)V

    .line 1115
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/g;->e:Z

    if-eqz v0, :cond_2

    .line 1116
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/g;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 1118
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 1132
    const/4 v0, 0x0

    .line 1133
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/g;->b:Z

    if-eqz v1, :cond_0

    .line 1134
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/auth/f/g;->c:F

    invoke-static {v0}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 1137
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/g;->d:Z

    if-eqz v1, :cond_1

    .line 1138
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/auth/f/g;->a:F

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1141
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/g;->e:Z

    if-eqz v1, :cond_2

    .line 1142
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/g;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1145
    :cond_2
    iput v0, p0, Lcom/google/android/gms/auth/f/g;->g:I

    .line 1146
    return v0
.end method

.method public final b(F)Lcom/google/android/gms/auth/f/g;
    .locals 1

    .prologue
    .line 1067
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/g;->d:Z

    .line 1068
    iput p1, p0, Lcom/google/android/gms/auth/f/g;->a:F

    .line 1069
    return-object p0
.end method
