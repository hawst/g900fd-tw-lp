.class final Lcom/google/android/gms/fitness/service/ax;
.super Lcom/google/android/gms/fitness/data/m;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/fitness/data/DataSource;

.field private final b:Lcom/google/android/gms/fitness/l/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/l/a/a;)V
    .locals 0

    .prologue
    .line 330
    invoke-direct {p0}, Lcom/google/android/gms/fitness/data/m;-><init>()V

    .line 331
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/ax;->a:Lcom/google/android/gms/fitness/data/DataSource;

    .line 332
    iput-object p2, p0, Lcom/google/android/gms/fitness/service/ax;->b:Lcom/google/android/gms/fitness/l/a/a;

    .line 333
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/data/DataPoint;)V
    .locals 3

    .prologue
    .line 337
    const-string v0, "Recording %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/ax;->b:Lcom/google/android/gms/fitness/l/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/l/a/a;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    .line 339
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RecordingListener{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/ax;->a:Lcom/google/android/gms/fitness/data/DataSource;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataSource;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
