.class final Lcom/google/android/gms/drive/ui/picker/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Landroid/widget/TextView;

.field final synthetic b:Landroid/widget/TextView;

.field final synthetic c:Landroid/widget/ImageView;

.field final synthetic d:Lcom/google/android/gms/drive/ui/picker/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/ui/picker/m;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 530
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/y;->d:Lcom/google/android/gms/drive/ui/picker/m;

    iput-object p2, p0, Lcom/google/android/gms/drive/ui/picker/y;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/google/android/gms/drive/ui/picker/y;->b:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/google/android/gms/drive/ui/picker/y;->c:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 4

    .prologue
    .line 530
    check-cast p1, Lcom/google/android/gms/drive/z;

    invoke-interface {p1}, Lcom/google/android/gms/drive/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/drive/z;->b()Lcom/google/android/gms/drive/aj;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/y;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/aj;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/y;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/aj;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/y;->d:Lcom/google/android/gms/drive/ui/picker/m;

    sget v3, Lcom/google/android/gms/p;->go:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/ui/picker/m;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/google/android/gms/drive/aj;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/aj;->p()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/String;Z)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/y;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/y;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/y;->d:Lcom/google/android/gms/drive/ui/picker/m;

    sget v3, Lcom/google/android/gms/p;->gb:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/ui/picker/m;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "PickEntryDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to fetch metadata, status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/drive/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
