.class public final Lcom/google/android/gms/googlehelp/f/l;
.super Lcom/google/android/gms/googlehelp/f/c;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private final c:Lcom/google/android/gms/googlehelp/common/o;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/o;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/f/c;-><init>()V

    .line 50
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/l;->a:Landroid/content/Context;

    .line 51
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/f/l;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 52
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/f/l;->c:Lcom/google/android/gms/googlehelp/common/o;

    .line 53
    return-void
.end method

.method public static a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 2

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/gms/googlehelp/common/p;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/p;-><init>()V

    const-string v1, "AUTOCOMPLETE_SEARCH_QUERY"

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/p;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/p;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/common/o;

    move-result-object v0

    .line 159
    new-instance v1, Lcom/google/android/gms/googlehelp/f/l;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/googlehelp/f/l;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/o;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 160
    return-void
.end method

.method public static a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/t;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 89
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 90
    :goto_1
    invoke-virtual {p2}, Lcom/google/android/gms/googlehelp/common/t;->a()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 91
    invoke-virtual {p2, v0}, Lcom/google/android/gms/googlehelp/common/t;->a(I)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v3

    .line 92
    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 95
    :cond_1
    new-instance v0, Lcom/google/android/gms/googlehelp/common/p;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/p;-><init>()V

    iput-object p1, v0, Lcom/google/android/gms/googlehelp/common/p;->a:Ljava/lang/String;

    const/4 v3, 0x1

    iput v3, v0, Lcom/google/android/gms/googlehelp/common/p;->d:I

    sget-object v3, Lcom/google/android/gms/googlehelp/common/o;->a:Lcom/google/android/gms/common/internal/bt;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/common/internal/bt;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/googlehelp/common/p;->e:Ljava/lang/String;

    iput-object p3, v0, Lcom/google/android/gms/googlehelp/common/p;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/common/p;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/common/o;

    move-result-object v0

    .line 100
    new-instance v2, Lcom/google/android/gms/googlehelp/f/l;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/googlehelp/f/l;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/o;)V

    new-array v0, v1, [Ljava/lang/Void;

    invoke-static {v2, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    :goto_0
    return-void

    .line 142
    :cond_0
    new-instance v0, Lcom/google/android/gms/googlehelp/common/p;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/p;-><init>()V

    iput-object p1, v0, Lcom/google/android/gms/googlehelp/common/p;->a:Ljava/lang/String;

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/p;->d:I

    iput-object p2, v0, Lcom/google/android/gms/googlehelp/common/p;->e:Ljava/lang/String;

    add-int/lit8 v1, p3, 0x1

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/p;->b:I

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/p;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/common/o;

    move-result-object v0

    .line 147
    new-instance v1, Lcom/google/android/gms/googlehelp/f/l;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/googlehelp/f/l;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/o;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 116
    :cond_0
    new-instance v0, Lcom/google/android/gms/googlehelp/common/p;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/p;-><init>()V

    iput-object p1, v0, Lcom/google/android/gms/googlehelp/common/p;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/googlehelp/common/p;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/p;

    move-result-object v0

    iput p3, v0, Lcom/google/android/gms/googlehelp/common/p;->b:I

    iput-object p4, v0, Lcom/google/android/gms/googlehelp/common/p;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/p;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/common/o;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/google/android/gms/googlehelp/f/l;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/googlehelp/f/l;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/o;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/l;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c()Lcom/google/android/gms/googlehelp/d/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/l;->c:Lcom/google/android/gms/googlehelp/common/o;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/d/d;->a(Lcom/google/android/gms/googlehelp/common/o;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/l;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/service/ConnectivityBroadcastReceiver;->a(Landroid/content/Context;Z)V

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/l;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/l;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/l;->c:Lcom/google/android/gms/googlehelp/common/o;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/googlehelp/a/o;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/gms/googlehelp/common/o;)V

    goto :goto_0
.end method
