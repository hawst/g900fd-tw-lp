.class Lcom/google/android/gms/auth/be/g;
.super Lcom/google/android/gms/auth/be/i;
.source "SourceFile"


# static fields
.field private static final k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/gms/auth/be/g;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/be/g;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/be/i;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/auth/a/c;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/be/i;-><init>(Lcom/google/android/gms/auth/a/c;Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 227
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 228
    if-eqz p5, :cond_1

    .line 229
    const-string v0, ","

    invoke-virtual {p5, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 237
    if-eqz p6, :cond_1

    const-string v0, "cl"

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 238
    const-string v0, "cl"

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 239
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 242
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "cl"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 245
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/gms/auth/be/g;->f:Z

    .line 246
    invoke-virtual {p0, p5, p2}, Lcom/google/android/gms/auth/be/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    if-eqz p3, :cond_2

    if-eqz p4, :cond_2

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/auth/be/g;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v4, p0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    const-string v5, "SID"

    invoke-virtual {v0, v4, v5, p3}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/auth/be/g;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    iget-object v4, p0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    const-string v5, "LSID"

    invoke-virtual {v0, v4, v5, p4}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/be/g;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/h;->a:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.server.checkin.CHECKIN"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 255
    if-eqz p5, :cond_3

    .line 257
    iget-object v4, p0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    const-string v5, "gmail-ls"

    const-string v0, "mail"

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v4, v5, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 259
    iget-object v4, p0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    const-string v5, "com.android.calendar"

    const-string v0, "cl"

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    invoke-static {v4, v5, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    const-string v3, "com.android.contacts"

    iget-object v4, p0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v5, "@youtube.com"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    :goto_2
    invoke-static {v0, v3, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 267
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 257
    goto :goto_0

    :cond_5
    move v0, v2

    .line 259
    goto :goto_1

    :cond_6
    move v2, v1

    .line 264
    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 19

    .prologue
    .line 94
    new-instance v17, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    .line 95
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .line 97
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 98
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 99
    const-string v2, "oauth2_authcode_verifier"

    move-object/from16 v0, p6

    invoke-virtual {v12, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_0
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 102
    const-string v2, "oauth2_redirect_uri"

    move-object/from16 v0, p7

    invoke-virtual {v12, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_1
    const-string v3, "ac2dm"

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->e:Lcom/google/android/gms/auth/a/c;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/a/c;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v11

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p5

    move/from16 v6, p3

    move-object/from16 v13, p8

    move-object/from16 v16, p9

    invoke-virtual/range {v2 .. v16}, Lcom/google/android/gms/auth/be/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v18

    .line 119
    sget-object v2, Lcom/google/android/gms/auth/be/w;->k:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 120
    const-string v2, "SID"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 121
    const-string v2, "LSID"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 122
    sget-object v2, Lcom/google/android/gms/auth/be/w;->v:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 123
    sget-object v2, Lcom/google/android/gms/auth/be/w;->w:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 126
    if-eqz v13, :cond_7

    .line 131
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/be/g;->a(Ljava/util/Map;Z)V

    .line 132
    sget-object v2, Lcom/google/android/gms/auth/be/w;->b:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 133
    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->b:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 137
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/gms/auth/be/g;->b:Ljava/lang/String;

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->b:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/util/a;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v2, v2, Lcom/google/android/gms/auth/a/c;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/g;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/auth/be/g;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v5, v5, Lcom/google/android/gms/auth/a/c;->c:Ljava/lang/String;

    invoke-static {v2, v3, v5}, Lcom/google/android/gms/common/util/a;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 148
    sget-object v2, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 149
    sget-object v2, Lcom/google/android/gms/auth/be/w;->l:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 150
    sget-object v2, Lcom/google/android/gms/auth/be/w;->x:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 151
    sget-object v2, Lcom/google/android/gms/auth/be/w;->y:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 152
    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object/from16 v2, p0

    move-object/from16 v3, v17

    invoke-virtual/range {v2 .. v12}, Lcom/google/android/gms/auth/be/g;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    .line 201
    :goto_0
    return-object v2

    .line 166
    :cond_2
    sget-object v2, Lcom/google/android/gms/auth/be/w;->i:Lcom/google/android/gms/auth/be/w;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/w;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v10, p0

    move/from16 v11, p2

    move/from16 v16, p3

    invoke-direct/range {v10 .. v16}, Lcom/google/android/gms/auth/be/g;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 173
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/gms/auth/be/g;->d:Z

    .line 174
    const-string v2, "useGoogleMail"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/h;->a:Landroid/content/Context;

    const/16 v3, 0x11

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "GoogleMail"

    const-string v4, "switching device to Google Mail mode"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gsf.loginservice.GOOGLE_MAIL_SWITCH"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "useGoogleMail"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "use_google_mail"

    const-string v4, "1"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const v2, 0x320d3

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/google/android/gms/auth/a/b;->a(ILjava/lang/String;)V

    const-string v2, "GoogleMail"

    const-string v3, "done switching to Google Mail mode"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_3
    :goto_1
    if-eqz p4, :cond_4

    .line 178
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/h;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    new-instance v4, Lcom/google/android/gms/backup/b;

    invoke-direct {v4, v2}, Lcom/google/android/gms/backup/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, Lcom/google/android/gms/backup/b;->a(Landroid/accounts/Account;)V

    .line 195
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    if-eqz v2, :cond_5

    if-eqz v8, :cond_5

    if-eqz v9, :cond_5

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    const-string v4, "firstName"

    invoke-virtual {v2, v3, v4, v8}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->i:Lcom/google/android/gms/auth/be/h;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/h;->c:Landroid/accounts/AccountManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/auth/be/g;->c:Landroid/accounts/Account;

    const-string v4, "lastName"

    invoke-virtual {v2, v3, v4, v9}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->e:Lcom/google/android/gms/auth/a/c;

    iget v5, v2, Lcom/google/android/gms/auth/a/c;->e:I

    const-string v6, "SID"

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/auth/be/g;->e:Lcom/google/android/gms/auth/a/c;

    iget-object v9, v2, Lcom/google/android/gms/auth/a/c;->c:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, v17

    move-object/from16 v4, v18

    invoke-virtual/range {v2 .. v11}, Lcom/google/android/gms/auth/be/g;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Ljava/util/Map;ILjava/lang/String;ZZLjava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    goto/16 :goto_0

    .line 175
    :cond_6
    const-string v2, "GoogleMail"

    const-string v3, "Platform version too old to switch to Google Mail mode"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 180
    :cond_7
    if-eqz p3, :cond_4

    .line 185
    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const-string v15, ""

    const/16 v16, 0x1

    move-object/from16 v10, p0

    invoke-direct/range {v10 .. v16}, Lcom/google/android/gms/auth/be/g;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2
.end method
