.class public Lcom/google/android/gms/wallet/service/analytics/AnalyticsIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/wallet/service/analytics/a/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "AnalyticsIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/wallet/service/analytics/a/a;)V
    .locals 1

    .prologue
    .line 39
    const-string v0, "AnalyticsIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/analytics/AnalyticsIntentService;->a:Lcom/google/android/gms/wallet/service/analytics/a/a;

    .line 41
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/AnalyticsIntentService;->a:Lcom/google/android/gms/wallet/service/analytics/a/a;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/google/android/gms/wallet/service/analytics/a/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/service/analytics/a/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/AnalyticsIntentService;->a:Lcom/google/android/gms/wallet/service/analytics/a/a;

    .line 50
    :cond_0
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 54
    sget-object v0, Lcom/google/android/gms/wallet/b/e;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    const-string v0, "wallet.analytics.event"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;

    .line 60
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;->b()Ljava/lang/String;

    move-result-object v4

    .line 61
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 62
    const-string v0, "AnalyticsIntentService"

    const-string v1, "Session id required for log correlation. Ignoring analytics event."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 65
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/analytics/AnalyticsIntentService;->a:Lcom/google/android/gms/wallet/service/analytics/a/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/analytics/a/i;->a(Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)Ljava/lang/Class;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/gms/wallet/service/analytics/a/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    move-object v1, v3

    .line 68
    :goto_1
    if-nez v1, :cond_e

    .line 69
    invoke-static {v0}, Lcom/google/android/gms/wallet/service/analytics/a/i;->a(Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)Ljava/lang/Class;

    move-result-object v1

    if-eqz v1, :cond_a

    const-class v2, Lcom/google/android/gms/wallet/service/analytics/a/b;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Lcom/google/android/gms/wallet/service/analytics/a/b;

    invoke-direct {v2}, Lcom/google/android/gms/wallet/service/analytics/a/b;-><init>()V

    .line 70
    :goto_2
    if-eqz v2, :cond_0

    .line 75
    invoke-virtual {v2, p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 77
    instance-of v1, v0, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 78
    check-cast v1, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;

    .line 80
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->a()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_b

    .line 81
    const-string v0, "AnalyticsIntentService"

    const-string v1, "TransactionKey must not be null. Ignoring analytics event."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 65
    :cond_3
    invoke-static {v2, v1}, Lcom/google/android/gms/wallet/service/analytics/a/i;->a(Ljava/lang/Class;Ljava/lang/String;)Lcom/google/android/gms/wallet/service/analytics/a/h;

    move-result-object v1

    goto :goto_1

    .line 69
    :cond_4
    const-class v2, Lcom/google/android/gms/wallet/service/analytics/a/c;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v2, Lcom/google/android/gms/wallet/service/analytics/a/c;

    invoke-direct {v2}, Lcom/google/android/gms/wallet/service/analytics/a/c;-><init>()V

    goto :goto_2

    :cond_5
    const-class v2, Lcom/google/android/gms/wallet/service/analytics/a/e;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v2, Lcom/google/android/gms/wallet/service/analytics/a/e;

    invoke-direct {v2}, Lcom/google/android/gms/wallet/service/analytics/a/e;-><init>()V

    goto :goto_2

    :cond_6
    const-class v2, Lcom/google/android/gms/wallet/service/analytics/a/g;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v2, Lcom/google/android/gms/wallet/service/analytics/a/g;

    invoke-direct {v2}, Lcom/google/android/gms/wallet/service/analytics/a/g;-><init>()V

    goto :goto_2

    :cond_7
    const-class v2, Lcom/google/android/gms/wallet/service/analytics/a/d;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    new-instance v2, Lcom/google/android/gms/wallet/service/analytics/a/d;

    invoke-direct {v2}, Lcom/google/android/gms/wallet/service/analytics/a/d;-><init>()V

    goto :goto_2

    :cond_8
    const-class v2, Lcom/google/android/gms/wallet/service/analytics/a/f;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    new-instance v2, Lcom/google/android/gms/wallet/service/analytics/a/f;

    invoke-direct {v2}, Lcom/google/android/gms/wallet/service/analytics/a/f;-><init>()V

    goto :goto_2

    :cond_9
    const-class v2, Lcom/google/android/gms/wallet/service/analytics/a/j;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v2, Lcom/google/android/gms/wallet/service/analytics/a/j;

    invoke-direct {v2}, Lcom/google/android/gms/wallet/service/analytics/a/j;-><init>()V

    goto :goto_2

    :cond_a
    move-object v2, v3

    goto :goto_2

    .line 84
    :cond_b
    iget-object v5, p0, Lcom/google/android/gms/wallet/service/analytics/AnalyticsIntentService;->a:Lcom/google/android/gms/wallet/service/analytics/a/a;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->a()Ljava/lang/String;

    move-result-object v6

    const-string v1, "transactionKey must not be null"

    invoke-static {v6, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, v5, Lcom/google/android/gms/wallet/service/analytics/a/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1, v6, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v1, Lcom/google/android/gms/wallet/service/analytics/a/k;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, v5, Lcom/google/android/gms/wallet/service/analytics/a/a;->b:J

    add-long/2addr v8, v10

    invoke-direct {v1, v8, v9, v7}, Lcom/google/android/gms/wallet/service/analytics/a/k;-><init>(JLjava/lang/String;)V

    iget-object v5, v5, Lcom/google/android/gms/wallet/service/analytics/a/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/analytics/a/k;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v5}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    :goto_3
    iget-object v1, v1, Lcom/google/android/gms/wallet/service/analytics/a/k;->b:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/android/gms/wallet/service/analytics/a/h;->c:Lcom/google/k/f/a/a/l;

    iput-object v1, v5, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    move-object v1, v2

    .line 99
    :goto_4
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 101
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 102
    new-instance v2, Lcom/google/android/gms/playlog/a;

    const/16 v5, 0x1f

    invoke-direct {v2, p0, v5, v3}, Lcom/google/android/gms/playlog/a;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 104
    const-string v5, "walletAnalytics"

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/analytics/a/h;->b()Lcom/google/protobuf/nano/j;

    move-result-object v6

    invoke-static {v6}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v6

    invoke-virtual {v2, v5, v6, v3}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 106
    invoke-virtual {v2}, Lcom/google/android/gms/playlog/a;->a()V

    .line 107
    const-string v2, "AnalyticsIntentService"

    const-string v3, "Uploading analytics data"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/analytics/AnalyticsIntentService;->a:Lcom/google/android/gms/wallet/service/analytics/a/a;

    iget-object v2, v2, Lcom/google/android/gms/wallet/service/analytics/a/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v2}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 110
    instance-of v0, v0, Lcom/google/android/gms/wallet/analytics/events/v;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/AnalyticsIntentService;->a:Lcom/google/android/gms/wallet/service/analytics/a/a;

    iget-object v1, v1, Lcom/google/android/gms/wallet/service/analytics/a/h;->d:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/wallet/service/analytics/a/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_0

    .line 84
    :cond_d
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ag;

    move-result-object v5

    new-instance v1, Lcom/google/android/gms/wallet/service/analytics/a/k;

    invoke-direct {v1, v5}, Lcom/google/android/gms/wallet/service/analytics/a/k;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    goto :goto_3

    .line 96
    :cond_e
    invoke-virtual {v1, p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    goto :goto_4

    .line 114
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/AnalyticsIntentService;->a:Lcom/google/android/gms/wallet/service/analytics/a/a;

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/analytics/a/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, v0, Lcom/google/android/gms/wallet/service/analytics/a/a;->b:J

    add-long/2addr v6, v8

    iput-wide v6, v1, Lcom/google/android/gms/wallet/service/analytics/a/h;->b:J

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/analytics/a/h;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v2}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    new-instance v1, Lcom/google/android/gms/wallet/service/m;

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/analytics/a/a;->d:Landroid/content/Context;

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/analytics/a/a;->a:Landroid/content/SharedPreferences;

    iget v0, v0, Lcom/google/android/gms/wallet/service/analytics/a/a;->c:I

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/wallet/service/m;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;I)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/service/m;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method
