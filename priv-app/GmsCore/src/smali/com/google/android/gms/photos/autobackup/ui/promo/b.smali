.class public final Lcom/google/android/gms/photos/autobackup/ui/promo/b;
.super Lcom/google/android/libraries/social/a/a/c/c;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/gms/photos/autobackup/ui/promo/h;

.field private e:Landroid/widget/RadioGroup;

.field private f:Lcom/google/android/gms/photos/autobackup/l;

.field private g:Lcom/google/android/gms/photos/e;

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/libraries/social/a/a/c/c;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->c:Lcom/google/android/libraries/social/i/r;

    const-string v2, "dialog_sync_disabled"

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/gms/photos/autobackup/ui/promo/h;-><init>(Lcom/google/android/libraries/social/i/w;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->d:Lcom/google/android/gms/photos/autobackup/ui/promo/h;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->h:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)V
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)Lcom/google/android/gms/photos/autobackup/ui/promo/h;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->d:Lcom/google/android/gms/photos/autobackup/ui/promo/h;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->e:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->bx:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 159
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->f:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/photos/autobackup/l;->b(Z)V

    .line 160
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->f:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/photos/autobackup/l;->c(Z)V

    .line 161
    return-void

    .line 158
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)Lcom/google/android/gms/photos/autobackup/l;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->f:Lcom/google/android/gms/photos/autobackup/l;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)Lcom/google/android/gms/photos/e;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->g:Lcom/google/android/gms/photos/e;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)Landroid/widget/RadioGroup;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->e:Landroid/widget/RadioGroup;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->b()V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/a/a/c/c;->a(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->b:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/e;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/e;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->g:Lcom/google/android/gms/photos/e;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->b:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/promo/k;

    new-instance v2, Lcom/google/android/gms/photos/autobackup/ui/promo/c;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/c;-><init>(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->b:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/ad;

    new-instance v2, Lcom/google/android/gms/photos/autobackup/ui/promo/d;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/d;-><init>(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->b:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/l;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->f:Lcom/google/android/gms/photos/autobackup/l;

    .line 67
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/a/a/c/c;->onCreate(Landroid/os/Bundle;)V

    .line 72
    if-eqz p1, :cond_0

    .line 73
    const-string v0, "should_log_impression"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->h:Z

    .line 75
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 94
    sget v0, Lcom/google/android/gms/l;->H:I

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const-string v3, "auto_backup"

    const-string v4, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v0, v3, v4}, Lcom/google/android/libraries/social/j/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->xv:I

    sget v0, Lcom/google/android/gms/j;->mg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/gms/photos/autobackup/ui/promo/m;->a:Landroid/text/Html$TagHandler;

    if-nez v4, :cond_0

    new-instance v4, Lcom/google/android/gms/photos/autobackup/ui/promo/n;

    invoke-direct {v4}, Lcom/google/android/gms/photos/autobackup/ui/promo/n;-><init>()V

    sput-object v4, Lcom/google/android/gms/photos/autobackup/ui/promo/m;->a:Landroid/text/Html$TagHandler;

    :cond_0
    sget-object v4, Lcom/google/android/gms/photos/autobackup/ui/promo/m;->a:Landroid/text/Html$TagHandler;

    invoke-static {v2, v3, v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 100
    sget v0, Lcom/google/android/gms/j;->pV:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 101
    new-instance v2, Lcom/google/android/gms/photos/autobackup/ui/promo/e;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/e;-><init>(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    sget v0, Lcom/google/android/gms/j;->pW:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 115
    new-instance v2, Lcom/google/android/gms/photos/autobackup/ui/promo/f;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/f;-><init>(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    sget v0, Lcom/google/android/gms/j;->bw:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->e:Landroid/widget/RadioGroup;

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->e:Landroid/widget/RadioGroup;

    new-instance v2, Lcom/google/android/gms/photos/autobackup/ui/promo/g;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/g;-><init>(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)V

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 141
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->b()V

    .line 143
    return-object v1
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 148
    invoke-super {p0}, Lcom/google/android/libraries/social/a/a/c/c;->onResume()V

    .line 149
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->h:Z

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->g:Lcom/google/android/gms/photos/e;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->f:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/photos/j;->b:Lcom/google/android/gms/photos/j;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/photos/e;->a(Ljava/lang/String;Lcom/google/android/gms/photos/j;)V

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->h:Z

    .line 155
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/a/a/c/c;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 80
    const-string v0, "should_log_impression"

    iget-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 81
    return-void
.end method
