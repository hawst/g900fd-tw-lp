.class public final enum Lcom/google/android/gms/drive/database/model/cg;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/cg;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/cg;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/cg;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/cg;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/cg;

.field private static final synthetic g:[Lcom/google/android/gms/drive/database/model/cg;


# instance fields
.field private final f:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v7, 0x1b

    .line 37
    new-instance v0, Lcom/google/android/gms/drive/database/model/cg;

    const-string v1, "PERMISSIONS_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->d()Lcom/google/android/gms/drive/database/model/cf;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "permissionsId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v9, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/model/cg;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/cg;->a:Lcom/google/android/gms/drive/database/model/cg;

    .line 44
    new-instance v0, Lcom/google/android/gms/drive/database/model/cg;

    const-string v1, "DISPLAY_NAME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->d()Lcom/google/android/gms/drive/database/model/cf;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "displayName"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/cg;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/cg;->b:Lcom/google/android/gms/drive/database/model/cg;

    .line 50
    new-instance v0, Lcom/google/android/gms/drive/database/model/cg;

    const-string v1, "EMAIL_ADDRESS"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->d()Lcom/google/android/gms/drive/database/model/cf;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "emailAddress"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/cg;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/cg;->c:Lcom/google/android/gms/drive/database/model/cg;

    .line 56
    new-instance v0, Lcom/google/android/gms/drive/database/model/cg;

    const-string v1, "PICTURE_URL"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->d()Lcom/google/android/gms/drive/database/model/cf;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "pictureUrl"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/gms/drive/database/model/cg;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/cg;->d:Lcom/google/android/gms/drive/database/model/cg;

    .line 62
    new-instance v0, Lcom/google/android/gms/drive/database/model/cg;

    const-string v1, "IS_AUTHENTICATED_USER"

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->d()Lcom/google/android/gms/drive/database/model/cf;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "isAuthenticatedUser"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v9, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/cg;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/cg;->e:Lcom/google/android/gms/drive/database/model/cg;

    .line 33
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/cg;

    sget-object v1, Lcom/google/android/gms/drive/database/model/cg;->a:Lcom/google/android/gms/drive/database/model/cg;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/cg;->b:Lcom/google/android/gms/drive/database/model/cg;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/cg;->c:Lcom/google/android/gms/drive/database/model/cg;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/cg;->d:Lcom/google/android/gms/drive/database/model/cg;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/gms/drive/database/model/cg;->e:Lcom/google/android/gms/drive/database/model/cg;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/cg;->g:[Lcom/google/android/gms/drive/database/model/cg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 70
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/cg;->f:Lcom/google/android/gms/drive/database/model/ab;

    .line 71
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/cg;
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/gms/drive/database/model/cg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/cg;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/cg;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->g:[Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/cg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/cg;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/cg;->f:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/cg;->f:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
