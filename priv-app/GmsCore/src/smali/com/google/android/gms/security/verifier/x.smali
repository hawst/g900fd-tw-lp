.class final Lcom/google/android/gms/security/verifier/x;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Lcom/google/android/gms/common/a/d;

.field static b:Lcom/google/android/gms/common/a/d;

.field static c:Lcom/google/android/gms/common/a/d;

.field static d:Lcom/google/android/gms/common/a/d;

.field static e:Lcom/google/android/gms/common/a/d;

.field static f:Lcom/google/android/gms/common/a/d;

.field static g:Lcom/google/android/gms/common/a/d;

.field static h:Lcom/google/android/gms/common/a/d;

.field static i:Lcom/google/android/gms/common/a/d;

.field static j:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    const-string v0, "verifier_upload_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->a:Lcom/google/android/gms/common/a/d;

    .line 19
    const-string v0, "gms_verifier_max_apk_size"

    const/high16 v1, 0x3200000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->b:Lcom/google/android/gms/common/a/d;

    .line 26
    const-string v0, "gms_verifier_package_installation_timeout_ms"

    invoke-static {}, Lcom/google/android/gms/security/verifier/ag;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->c:Lcom/google/android/gms/common/a/d;

    .line 32
    const-string v0, "gms_verifier_entry_expiration_ms"

    invoke-static {}, Lcom/google/android/gms/security/verifier/ag;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->d:Lcom/google/android/gms/common/a/d;

    .line 38
    const-string v0, "gms_verifier_upload_url"

    const-string v1, "https://safebrowsing.google.com/safebrowsing/uploads/android"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->e:Lcom/google/android/gms/common/a/d;

    .line 46
    const-string v0, "gms_verifier_upload_timeout_ms"

    const/16 v1, 0x14

    invoke-static {v1}, Lcom/google/android/gms/security/verifier/ag;->b(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->f:Lcom/google/android/gms/common/a/d;

    .line 52
    const-string v0, "gms_verifier_start_upload_timeout_ms"

    const/4 v1, 0x5

    invoke-static {v1}, Lcom/google/android/gms/security/verifier/ag;->a(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->g:Lcom/google/android/gms/common/a/d;

    .line 60
    const-string v0, "gms_verifier_chunk_size"

    const/high16 v1, 0x40000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->h:Lcom/google/android/gms/common/a/d;

    .line 69
    const-string v0, "gms_verifier_chunk_upload_timeout_ms"

    const/16 v1, 0xf

    invoke-static {v1}, Lcom/google/android/gms/security/verifier/ag;->a(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->i:Lcom/google/android/gms/common/a/d;

    .line 75
    const-string v0, "gms_verifier_max_upload_attempts"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/verifier/x;->j:Lcom/google/android/gms/common/a/d;

    return-void
.end method
