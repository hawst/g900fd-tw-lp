.class final Lcom/google/android/gms/drive/ui/legacy/navigation/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 50
    const-class v0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    new-instance v1, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;-><init>(Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;)V

    const-class v0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a(Landroid/os/Parcelable;)V

    return-object v1
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    new-array v0, p1, [Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    return-object v0
.end method
