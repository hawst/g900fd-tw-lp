.class public Lcom/google/android/gms/kids/account/RetryKidsAccountSetupReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 18
    const-string v0, "RetryKidsAccountSetupReceiver"

    const-string v1, "onReceive: action=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    invoke-static {}, Lcom/google/android/gms/kids/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 28
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    invoke-static {}, Lcom/google/android/gms/kids/common/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 23
    const-class v0, Lcom/google/android/gms/kids/account/RetryKidsAccountSetupReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->b(Landroid/content/Context;Ljava/lang/Class;)V

    .line 25
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/kids/common/d;->a(Landroid/content/Context;)Lcom/google/android/gms/kids/account/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/kids/account/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-static {}, Lcom/google/android/gms/kids/account/KidsAccountSetupService;->a()V

    goto :goto_0
.end method
