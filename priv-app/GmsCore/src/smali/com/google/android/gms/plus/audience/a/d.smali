.class public final Lcom/google/android/gms/plus/audience/a/d;
.super Lcom/google/android/gms/plus/audience/a/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 28
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/audience/a/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 30
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/d;->d:Z

    .line 35
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/a/e;->a()V

    .line 36
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 18
    check-cast p1, Lcom/google/android/gms/people/g;

    invoke-interface {p1}, Lcom/google/android/gms/people/g;->c()Lcom/google/android/gms/people/model/h;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/people/g;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/people/model/h;->c()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/model/h;->b(I)Lcom/google/android/gms/people/model/g;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/plus/audience/a/d;->f:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/a/d;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/common/data/d;)V

    invoke-virtual {v2}, Lcom/google/android/gms/people/model/h;->w_()V

    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/people/g;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/plus/audience/a/d;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/common/data/d;)V

    goto :goto_1
.end method

.method protected final a(Lcom/google/android/gms/common/api/v;)V
    .locals 3

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/d;->d:Z

    if-nez v0, :cond_0

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/d;->d:Z

    .line 42
    sget-object v0, Lcom/google/android/gms/people/x;->h:Lcom/google/android/gms/people/ai;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a/e;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/e;->h:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/people/ai;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    .line 45
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a/e;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/e;->h:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 47
    return-void
.end method
