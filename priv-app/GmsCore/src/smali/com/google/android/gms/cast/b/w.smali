.class final Lcom/google/android/gms/cast/b/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[B

.field b:I

.field c:I

.field d:I

.field e:Z

.field private final f:[Ljava/nio/ByteBuffer;

.field private final g:[Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    if-gtz p1, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid capacity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; must be > 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    .line 45
    new-array v0, v1, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/gms/cast/b/w;->f:[Ljava/nio/ByteBuffer;

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/gms/cast/b/w;->g:[Ljava/nio/ByteBuffer;

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/b/w;->d:I

    .line 48
    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/w;->e:Z

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/channels/SocketChannel;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 451
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/w;->b()[Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 452
    if-nez v3, :cond_0

    .line 469
    :goto_0
    return v0

    .line 460
    :cond_0
    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v2, v3, v1

    .line 461
    invoke-virtual {p1, v2}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 462
    if-lez v2, :cond_1

    .line 463
    add-int/2addr v2, v0

    .line 460
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    .line 467
    :cond_1
    if-lez v0, :cond_2

    .line 468
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/w;->b(I)V

    goto :goto_0

    .line 474
    :cond_2
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
.end method

.method final a(B)V
    .locals 2

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->c:I

    aput-byte p1, v0, v1

    .line 636
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/b/w;->b(I)V

    .line 637
    return-void
.end method

.method final a(I)V
    .locals 2

    .prologue
    .line 591
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    .line 592
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 593
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    .line 597
    :cond_0
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->c:I

    if-ne v0, v1, :cond_1

    .line 598
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 601
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/w;->c()V

    .line 607
    :cond_1
    :goto_0
    return-void

    .line 604
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/w;->e:Z

    goto :goto_0
.end method

.method public final a()[Ljava/nio/ByteBuffer;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 166
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/w;->e:Z

    if-eqz v0, :cond_0

    .line 167
    const/4 v0, 0x0

    .line 197
    :goto_0
    return-object v0

    .line 171
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/w;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 172
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    if-nez v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->f:[Ljava/nio/ByteBuffer;

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget-object v2, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v2, v2

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->g:[Ljava/nio/ByteBuffer;

    .line 179
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget-object v3, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/gms/cast/b/w;->b:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 180
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->c:I

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v6

    goto :goto_0

    .line 182
    :cond_2
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->c:I

    if-ge v0, v1, :cond_3

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->f:[Ljava/nio/ByteBuffer;

    .line 185
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget v3, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iget v4, p0, Lcom/google/android/gms/cast/b/w;->b:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 187
    :cond_3
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    if-nez v0, :cond_4

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->f:[Ljava/nio/ByteBuffer;

    .line 189
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget-object v3, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/gms/cast/b/w;->b:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 192
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->g:[Ljava/nio/ByteBuffer;

    .line 193
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget-object v3, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/gms/cast/b/w;->b:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 194
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->c:I

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v6

    goto/16 :goto_0
.end method

.method final b(I)V
    .locals 2

    .prologue
    .line 614
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    .line 615
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 616
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    .line 618
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/w;->e:Z

    .line 619
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/b/w;->d:I

    .line 620
    return-void
.end method

.method public final b()[Ljava/nio/ByteBuffer;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 393
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/w;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x0

    .line 424
    :goto_0
    return-object v0

    .line 397
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/w;->e:Z

    if-eqz v0, :cond_2

    .line 398
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    if-nez v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->f:[Ljava/nio/ByteBuffer;

    .line 401
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget-object v2, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v2, v2

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 404
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->g:[Ljava/nio/ByteBuffer;

    .line 405
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget-object v3, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/gms/cast/b/w;->b:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 406
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->c:I

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v6

    goto :goto_0

    .line 408
    :cond_2
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->b:I

    if-ge v0, v1, :cond_3

    .line 410
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->f:[Ljava/nio/ByteBuffer;

    .line 411
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iget v3, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget v4, p0, Lcom/google/android/gms/cast/b/w;->c:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 413
    :cond_3
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    if-nez v0, :cond_4

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->f:[Ljava/nio/ByteBuffer;

    .line 416
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iget-object v3, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/gms/cast/b/w;->c:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    goto :goto_0

    .line 419
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->g:[Ljava/nio/ByteBuffer;

    .line 420
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iget-object v3, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/gms/cast/b/w;->c:I

    sub-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 421
    iget-object v1, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/b/w;->b:I

    invoke-static {v1, v5, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v6

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 482
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iput v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    .line 483
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/b/w;->d:I

    .line 484
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/w;->e:Z

    .line 485
    return-void
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 522
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/w;->e:Z

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v0, v0

    .line 527
    :goto_0
    return v0

    .line 524
    :cond_0
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->b:I

    if-ge v0, v1, :cond_1

    .line 525
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->c:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 527
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->c:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->b:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 535
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/w;->e:Z

    if-eqz v0, :cond_0

    .line 536
    const/4 v0, 0x0

    .line 540
    :goto_0
    return v0

    .line 537
    :cond_0
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->c:I

    if-ge v0, v1, :cond_1

    .line 538
    iget v0, p0, Lcom/google/android/gms/cast/b/w;->c:I

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 540
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->b:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->c:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 555
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/w;->e:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/b/w;->b:I

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->c:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final g()B
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/b/w;->b:I

    aget-byte v0, v0, v1

    .line 627
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/cast/b/w;->a(I)V

    .line 628
    return v0
.end method
