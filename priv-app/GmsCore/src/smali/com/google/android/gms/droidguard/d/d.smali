.class final Lcom/google/android/gms/droidguard/d/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/droidguard/d/m;


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Lcom/google/android/gms/droidguard/d/k;

.field final synthetic c:Lcom/google/android/gms/droidguard/d/c;


# direct methods
.method constructor <init>(Lcom/google/android/gms/droidguard/d/c;Ljava/lang/Object;Lcom/google/android/gms/droidguard/d/k;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/d;->c:Lcom/google/android/gms/droidguard/d/c;

    iput-object p2, p0, Lcom/google/android/gms/droidguard/d/d;->a:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/gms/droidguard/d/d;->b:Lcom/google/android/gms/droidguard/d/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 58
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/d;->c:Lcom/google/android/gms/droidguard/d/c;

    iget-object v0, v0, Lcom/google/android/gms/droidguard/d/c;->b:Lcom/google/android/gms/droidguard/d/a;

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/d;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/droidguard/d/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/gms/droidguard/d/b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 63
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/d;->b:Lcom/google/android/gms/droidguard/d/k;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/d;->c:Lcom/google/android/gms/droidguard/d/c;

    iget-object v1, v0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    monitor-enter v1

    .line 65
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/d;->c:Lcom/google/android/gms/droidguard/d/c;

    iget-object v0, v0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/gms/droidguard/d/d;->a:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 61
    const-string v1, "DG"

    const-string v2, "Write to cache failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/d;->b:Lcom/google/android/gms/droidguard/d/k;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Throwable;)Lcom/google/android/gms/droidguard/d/k;

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/d;->c:Lcom/google/android/gms/droidguard/d/c;

    iget-object v1, v0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    monitor-enter v1

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/d;->c:Lcom/google/android/gms/droidguard/d/c;

    iget-object v0, v0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/gms/droidguard/d/d;->a:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
