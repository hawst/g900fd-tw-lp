.class final Lcom/google/android/gms/drive/metadata/a/k;
.super Lcom/google/android/gms/drive/metadata/a/ak;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/metadata/f;)V
    .locals 2

    .prologue
    .line 501
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/metadata/a/ak;-><init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 501
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->W()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 501
    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->h(Z)V

    return-void
.end method

.method protected final synthetic a(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 501
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->u()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/drive/internal/model/File$Labels;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File;->a(Lcom/google/android/gms/drive/internal/model/File$Labels;)Lcom/google/android/gms/drive/internal/model/File;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->u()Lcom/google/android/gms/drive/internal/model/File$Labels;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/internal/model/File$Labels;->b(Z)Lcom/google/android/gms/drive/internal/model/File$Labels;

    return-void
.end method
