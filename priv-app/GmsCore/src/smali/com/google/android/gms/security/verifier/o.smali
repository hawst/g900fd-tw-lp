.class public final Lcom/google/android/gms/security/verifier/o;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:[Lcom/google/android/gms/security/verifier/s;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1074
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1075
    iput-object v1, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    invoke-static {}, Lcom/google/android/gms/security/verifier/s;->a()[Lcom/google/android/gms/security/verifier/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    iput-object v1, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/verifier/o;->cachedSize:I

    .line 1076
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 1176
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1177
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1178
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1181
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1182
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1185
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 1186
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1187
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    aget-object v2, v2, v0

    .line 1188
    if-eqz v2, :cond_2

    .line 1189
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1186
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1194
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 1195
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1198
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 1199
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1202
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1090
    if-ne p1, p0, :cond_1

    .line 1129
    :cond_0
    :goto_0
    return v0

    .line 1093
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/verifier/o;

    if-nez v2, :cond_2

    move v0, v1

    .line 1094
    goto :goto_0

    .line 1096
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/verifier/o;

    .line 1097
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1098
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1099
    goto :goto_0

    .line 1101
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1102
    goto :goto_0

    .line 1104
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    if-nez v2, :cond_5

    .line 1105
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1106
    goto :goto_0

    .line 1108
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1109
    goto :goto_0

    .line 1111
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1113
    goto :goto_0

    .line 1115
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    if-nez v2, :cond_8

    .line 1116
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    move v0, v1

    .line 1117
    goto :goto_0

    .line 1119
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1120
    goto :goto_0

    .line 1122
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    if-nez v2, :cond_a

    .line 1123
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1124
    goto :goto_0

    .line 1126
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1127
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1134
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1137
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1139
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1141
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1143
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1145
    return v0

    .line 1134
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1137
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1141
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1143
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1042
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/security/verifier/s;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/security/verifier/s;

    invoke-direct {v3}, Lcom/google/android/gms/security/verifier/s;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/security/verifier/s;

    invoke-direct {v3}, Lcom/google/android/gms/security/verifier/s;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1151
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1152
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1154
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1155
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1157
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1158
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 1159
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->c:[Lcom/google/android/gms/security/verifier/s;

    aget-object v1, v1, v0

    .line 1160
    if-eqz v1, :cond_2

    .line 1161
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1158
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1165
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1166
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1168
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1169
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/o;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1171
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1172
    return-void
.end method
