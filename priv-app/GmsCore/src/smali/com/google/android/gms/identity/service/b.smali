.class final Lcom/google/android/gms/identity/service/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/address/c;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/google/android/gms/identity/intents/UserAddressRequest;

.field final synthetic c:Lcom/google/android/gms/identity/intents/a/a;

.field final synthetic d:Lcom/google/android/gms/identity/service/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/identity/service/a;Landroid/os/Bundle;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lcom/google/android/gms/identity/intents/a/a;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/gms/identity/service/b;->d:Lcom/google/android/gms/identity/service/a;

    iput-object p2, p0, Lcom/google/android/gms/identity/service/b;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/google/android/gms/identity/service/b;->b:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    iput-object p4, p0, Lcom/google/android/gms/identity/service/b;->c:Lcom/google/android/gms/identity/intents/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 97
    array-length v0, p1

    if-lez v0, :cond_1

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/identity/service/b;->a:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/gms/identity/service/b;->d:Lcom/google/android/gms/identity/service/a;

    invoke-static {v0}, Lcom/google/android/gms/identity/service/a;->a(Lcom/google/android/gms/identity/service/a;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/identity/service/b;->d:Lcom/google/android/gms/identity/service/a;

    invoke-static {v0}, Lcom/google/android/gms/identity/service/a;->b(Lcom/google/android/gms/identity/service/a;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/identity/service/b;->b:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    new-instance v5, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.identity.REQUEST_USER_ADDRESS"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.google.android.gms"

    const-class v6, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->a()Lcom/google/android/gms/wallet/shared/a;

    move-result-object v7

    const-string v0, "com.google.android.gms.identity.intents.EXTRA_ACCOUNT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.identity.intents.EXTRA_ACCOUNT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-virtual {v7, v0}, Lcom/google/android/gms/wallet/shared/a;->a(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/shared/a;

    :cond_0
    invoke-virtual {v7, v8}, Lcom/google/android/gms/wallet/shared/a;->a(Z)Lcom/google/android/gms/wallet/shared/a;

    const-string v0, "com.google.android.gms.identity.intents.EXTRA_THEME"

    const/4 v8, 0x0

    invoke-virtual {v1, v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v7, v0}, Lcom/google/android/gms/wallet/shared/a;->b(I)Lcom/google/android/gms/wallet/shared/a;

    invoke-static {}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a()Lcom/google/android/gms/wallet/shared/d;

    move-result-object v0

    iget-object v1, v7, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/d;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/shared/d;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v0

    const-string v1, "requestUserAddress"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/d;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/d;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    const-string v1, "com.google.android.gms.identity.intents.EXTA_CONFIG"

    invoke-virtual {v5, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.identity.intents.EXTRA_REQUEST"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v3, v5, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v1, "com.google.android.gms.identity.intents.EXTRA_PENDING_INTENT"

    invoke-virtual {v6, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/identity/service/b;->c:Lcom/google/android/gms/identity/intents/a/a;

    const/4 v1, 0x6

    invoke-interface {v0, v1, v6}, Lcom/google/android/gms/identity/intents/a/a;->a(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 106
    const-string v1, "AddressService"

    const-string v2, "Exception getting requestUserAddress intent"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 109
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 110
    const-string v1, "com.google.android.gms.identity.intents.EXTRA_ERROR_CODE"

    const/16 v2, 0x22b

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/identity/service/b;->c:Lcom/google/android/gms/identity/intents/a/a;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/identity/intents/a/a;->a(ILandroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 115
    :catch_1
    move-exception v0

    .line 117
    const-string v1, "AddressService"

    const-string v2, "Exception returning no addresses from getAddress"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
