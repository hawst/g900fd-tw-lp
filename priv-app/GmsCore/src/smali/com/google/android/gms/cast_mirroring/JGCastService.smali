.class public Lcom/google/android/gms/cast_mirroring/JGCastService;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final FLAG_SHOW_RECEIVER_STATS:I = 0x2

.field public static final FLAG_SHOW_SENDER_STATS:I = 0x1

.field public static final FLAG_USE_TDLS:I = -0x80000000

.field private static final TAG:Ljava/lang/String; = "JGCastService"


# instance fields
.field private mEventHandler:Landroid/os/Handler;

.field private mListener:Lcom/google/android/gms/cast_mirroring/m;

.field private mNativeContext:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "jgcastservice"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 30
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/JGCastService;->native_init()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/cast_mirroring/m;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/cast_mirroring/JGCastService;->mListener:Lcom/google/android/gms/cast_mirroring/m;

    .line 41
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 42
    if-nez v0, :cond_0

    .line 43
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    .line 46
    :cond_0
    if-eqz v0, :cond_1

    .line 47
    new-instance v1, Lcom/google/android/gms/cast_mirroring/l;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/cast_mirroring/l;-><init>(Lcom/google/android/gms/cast_mirroring/JGCastService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/cast_mirroring/JGCastService;->mEventHandler:Landroid/os/Handler;

    .line 55
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/cast_mirroring/JGCastService;->native_setup(Ljava/lang/Object;Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast_mirroring/JGCastService;)Lcom/google/android/gms/cast_mirroring/m;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/JGCastService;->mListener:Lcom/google/android/gms/cast_mirroring/m;

    return-object v0
.end method

.method private final createSink(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/Surface;I)V
    .locals 7

    .prologue
    .line 102
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/cast_mirroring/JGCastService;->createSourceOrSink(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/Surface;I)V

    .line 109
    return-void
.end method

.method private final createSource(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 87
    const/4 v1, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/cast_mirroring/JGCastService;->createSourceOrSink(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/Surface;I)V

    .line 94
    return-void
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_setup(Ljava/lang/Object;Landroid/content/Context;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;III)V
    .locals 2

    .prologue
    .line 115
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast_mirroring/JGCastService;

    .line 117
    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/cast_mirroring/JGCastService;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2, p3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    .line 122
    iget-object v0, v0, Lcom/google/android/gms/cast_mirroring/JGCastService;->mEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method


# virtual methods
.method public final native createSourceOrSink(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/Surface;I)V
.end method

.method public final native disconnect()V
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/cast_mirroring/JGCastService;->native_finalize()V

    .line 61
    return-void
.end method

.method public final native release()V
.end method
