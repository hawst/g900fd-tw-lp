.class public Lcom/google/android/gms/wallet/service/analytics/a/b;
.super Lcom/google/android/gms/wallet/service/analytics/a/h;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/k/f/a/a/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>()V

    .line 35
    new-instance v0, Lcom/google/k/f/a/a/e;

    invoke-direct {v0}, Lcom/google/k/f/a/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    .line 36
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/wallet/common/ag;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    .line 40
    const-class v0, Lcom/google/k/f/a/a/e;

    new-instance v1, Lcom/google/k/f/a/a/e;

    invoke-direct {v1}, Lcom/google/k/f/a/a/e;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/k/f/a/a/e;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V
    .locals 3

    .prologue
    .line 63
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;

    if-eqz v0, :cond_0

    .line 64
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;

    .line 65
    iget-object v0, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/b;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/content/Context;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->c:Lcom/google/k/f/a/a/l;

    iget-object v1, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    .line 67
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->d:Ljava/lang/String;

    .line 102
    :goto_0
    return-void

    .line 68
    :cond_0
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;

    if-eqz v0, :cond_1

    .line 69
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->c:I

    iput v1, v0, Lcom/google/k/f/a/a/e;->k:I

    goto :goto_0

    .line 72
    :cond_1
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;

    if-eqz v0, :cond_2

    .line 73
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->a:I

    iput v1, v0, Lcom/google/k/f/a/a/e;->b:I

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->b:I

    iput v1, v0, Lcom/google/k/f/a/a/e;->f:I

    goto :goto_0

    .line 77
    :cond_2
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;

    if-eqz v0, :cond_3

    .line 78
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget-boolean v1, v0, Lcom/google/k/f/a/a/e;->d:Z

    iget-boolean v2, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->a:Z

    or-int/2addr v1, v2

    iput-boolean v1, v0, Lcom/google/k/f/a/a/e;->d:Z

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget-boolean v1, v0, Lcom/google/k/f/a/a/e;->h:Z

    iget-boolean v2, p2, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->b:Z

    or-int/2addr v1, v2

    iput-boolean v1, v0, Lcom/google/k/f/a/a/e;->h:Z

    goto :goto_0

    .line 83
    :cond_3
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;

    if-eqz v0, :cond_5

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget v0, v0, Lcom/google/k/f/a/a/e;->i:I

    if-gez v0, :cond_4

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    const/4 v1, 0x1

    iput v1, v0, Lcom/google/k/f/a/a/e;->i:I

    goto :goto_0

    .line 87
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget v1, v0, Lcom/google/k/f/a/a/e;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/k/f/a/a/e;->i:I

    goto :goto_0

    .line 89
    :cond_5
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;

    if-eqz v0, :cond_6

    .line 90
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget-boolean v1, p2, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;->a:Z

    iput-boolean v1, v0, Lcom/google/k/f/a/a/e;->a:Z

    goto :goto_0

    .line 92
    :cond_6
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;

    if-eqz v0, :cond_7

    .line 93
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->a:I

    iput v1, v0, Lcom/google/k/f/a/a/e;->j:I

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget-boolean v1, p2, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->b:Z

    iput-boolean v1, v0, Lcom/google/k/f/a/a/e;->e:Z

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget-boolean v1, p2, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->c:Z

    iput-boolean v1, v0, Lcom/google/k/f/a/a/e;->c:Z

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget-boolean v1, p2, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->d:Z

    iput-boolean v1, v0, Lcom/google/k/f/a/a/e;->g:Z

    goto :goto_0

    .line 100
    :cond_7
    const-string v0, "CreditCardSessionState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to fill data for event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected final a(Lcom/google/android/gms/wallet/common/ah;)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Lcom/google/android/gms/wallet/common/ah;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;

    .line 48
    return-void
.end method

.method protected final a(Lcom/google/k/f/a/a/j;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iput-object v0, p1, Lcom/google/k/f/a/a/j;->b:Lcom/google/k/f/a/a/e;

    .line 53
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/b;->a:Lcom/google/k/f/a/a/e;

    iget v0, v0, Lcom/google/k/f/a/a/e;->k:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
