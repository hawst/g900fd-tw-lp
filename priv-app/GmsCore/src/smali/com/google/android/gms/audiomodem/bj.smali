.class public final Lcom/google/android/gms/audiomodem/bj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/audiomodem/r;

.field public b:Lcom/google/android/gms/audiomodem/Encoding;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/audiomodem/r;Lcom/google/android/gms/audiomodem/Encoding;)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/bj;->a:Lcom/google/android/gms/audiomodem/r;

    .line 143
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/bj;->b:Lcom/google/android/gms/audiomodem/Encoding;

    .line 144
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 155
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/gms/audiomodem/bj;

    if-nez v1, :cond_1

    .line 159
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    check-cast p1, Lcom/google/android/gms/audiomodem/bj;

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bj;->a:Lcom/google/android/gms/audiomodem/r;

    iget-object v2, p1, Lcom/google/android/gms/audiomodem/bj;->a:Lcom/google/android/gms/audiomodem/r;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bj;->b:Lcom/google/android/gms/audiomodem/Encoding;

    iget-object v2, p1, Lcom/google/android/gms/audiomodem/bj;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 164
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/bj;->a:Lcom/google/android/gms/audiomodem/r;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/bj;->b:Lcom/google/android/gms/audiomodem/Encoding;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
