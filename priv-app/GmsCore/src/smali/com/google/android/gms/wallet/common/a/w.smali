.class public final Lcom/google/android/gms/wallet/common/a/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Comparator;


# instance fields
.field private final b:Ljava/lang/CharSequence;

.field private final c:Ljava/lang/CharSequence;

.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/wallet/common/a/x;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/a/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/a/w;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    if-lez p3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "matchTypes is required"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 82
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 83
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/a/w;->b:Ljava/lang/CharSequence;

    .line 84
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/a/w;->c:Ljava/lang/CharSequence;

    .line 85
    iput p3, p0, Lcom/google/android/gms/wallet/common/a/w;->d:I

    .line 86
    iput-object p4, p0, Lcom/google/android/gms/wallet/common/a/w;->e:Ljava/lang/String;

    .line 87
    return-void

    :cond_0
    move v0, v2

    .line 81
    goto :goto_0

    :cond_1
    move v1, v2

    .line 82
    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/w;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/google/android/gms/wallet/common/a/w;->d:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/w;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/gms/wallet/common/a/w;->d:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/w;->e:Ljava/lang/String;

    return-object v0
.end method
