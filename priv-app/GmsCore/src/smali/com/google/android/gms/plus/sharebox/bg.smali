.class public final Lcom/google/android/gms/plus/sharebox/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Lcom/google/android/gms/plus/model/posts/PlusPage;

.field c:Ljava/lang/String;

.field d:I

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:I

.field j:Z

.field k:Z

.field l:Z

.field m:Ljava/lang/String;

.field n:Ljava/lang/String;

.field o:Lcom/google/android/gms/plus/sharebox/a/b;

.field p:Lcom/google/android/gms/plus/sharebox/a/a;

.field q:Lcom/google/android/gms/common/people/data/Audience;

.field r:Lcom/google/android/gms/common/people/data/AudienceMember;

.field s:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->a:Ljava/lang/String;

    .line 57
    const-string v0, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->n:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/a/b;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    .line 59
    const-string v0, "com.google.android.apps.plus.CALL_TO_ACTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/a/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->p:Lcom/google/android/gms/plus/sharebox/a/a;

    .line 61
    const-string v0, "com.google.android.apps.plus.RECIPIENT_IDS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/at;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->q:Lcom/google/android/gms/common/people/data/Audience;

    .line 63
    const-string v0, "com.google.android.gms.plus.intent.extra.INTERNAL_PREFILLED_PLUS_MENTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->r:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->i:I

    .line 68
    sget-object v0, Lcom/google/android/gms/plus/c/a;->ab:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->j:Z

    .line 69
    sget-object v0, Lcom/google/android/gms/plus/c/a;->ac:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->k:Z

    .line 70
    sget-object v0, Lcom/google/android/gms/plus/c/a;->ad:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->l:Z

    .line 71
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->s:Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->s:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 76
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    .line 78
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->s:Ljava/lang/String;

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/bg;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/a/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/bg;->a(Ljava/lang/String;)V

    .line 85
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/bg;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->s:Ljava/lang/String;

    .line 88
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/bg;->a(Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bg;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bg;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->s:Ljava/lang/String;

    .line 97
    :cond_2
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 105
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 109
    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    .line 110
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v1

    const-class v2, Landroid/text/style/URLSpan;

    invoke-virtual {v0, v3, v1, v2}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 111
    array-length v1, v0

    if-lez v1, :cond_0

    .line 112
    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->n:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    const-string v0, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_ID"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    const-string v3, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_IMAGE_URL"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 124
    const-string v4, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_DISPLAY_NAME"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 127
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 128
    new-instance v5, Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-direct {v5, v0, v4, v3}, Lcom/google/android/gms/plus/model/posts/PlusPage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v5, p0, Lcom/google/android/gms/plus/sharebox/bg;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    .line 130
    :cond_0
    const-string v0, "com.google.android.gms.plus.intent.extra.INTERNAL_APP_NAME"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->c:Ljava/lang/String;

    .line 131
    const-string v0, "com.google.android.gms.plus.intent.extra.INTERNAL_APP_ICON_RESOURCE"

    const/4 v3, -0x1

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->d:I

    .line 132
    const-string v0, "com.google.android.gms.plus.intent.extra.INTERNAL_SHARE_BUTTON_NAME"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->e:Ljava/lang/String;

    .line 133
    const-string v0, "com.google.android.gms.plus.intent.extra.SHARE_CONTEXT_TYPE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v3

    .line 139
    const-string v4, "com.google.android.gms.plus.intent.extra.OVERRIDE_FIRST_VIEW"

    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p2, v4, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->i:I

    .line 143
    const-string v4, "com.google.android.gms.plus.intent.extra.SHOW_ACL_SUGGESTED_SECTION"

    if-nez v3, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p2, v4, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->j:Z

    .line 145
    const-string v4, "com.google.android.gms.plus.intent.extra.INCLUDE_SUGGESTIONS_WITH_PEOPLE"

    if-nez v3, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p2, v4, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->k:Z

    .line 147
    const-string v0, "com.google.android.gms.plus.intent.extra.SHOW_ADD_TO_CIRCLE"

    if-nez v3, :cond_4

    :goto_3
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->l:Z

    .line 150
    const-string v0, "com.google.android.gms.plus.intent.extra.CLIENT_APPLICATION_ID"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    .line 151
    return-void

    :cond_1
    move v0, v2

    .line 139
    goto :goto_0

    :cond_2
    move v0, v2

    .line 143
    goto :goto_1

    :cond_3
    move v0, v2

    .line 145
    goto :goto_2

    :cond_4
    move v1, v2

    .line 147
    goto :goto_3
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/bg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/PlusPage;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/PlusPage;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->p:Lcom/google/android/gms/plus/sharebox/a/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bg;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
