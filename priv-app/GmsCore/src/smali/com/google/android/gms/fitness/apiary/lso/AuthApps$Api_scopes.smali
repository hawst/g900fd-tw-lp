.class public final Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/fitness/apiary/lso/b;

.field private static final g:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:I

.field f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 129
    new-instance v0, Lcom/google/android/gms/fitness/apiary/lso/b;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/lso/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/b;

    .line 142
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 145
    sput-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->g:Ljava/util/HashMap;

    const-string v1, "description"

    const-string v2, "description"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->g:Ljava/util/HashMap;

    const-string v1, "detail"

    const-string v2, "detail"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->g:Ljava/util/HashMap;

    const-string v1, "scope_id"

    const-string v2, "scope_id"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->g:Ljava/util/HashMap;

    const-string v1, "summary"

    const-string v2, "summary"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 181
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->b:I

    .line 182
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->a:Ljava/util/Set;

    .line 183
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 194
    iput-object p1, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->a:Ljava/util/Set;

    .line 195
    iput p2, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->b:I

    .line 196
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->c:Ljava/lang/String;

    .line 197
    iput-object p4, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->d:Ljava/lang/String;

    .line 198
    iput p5, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->e:I

    .line 199
    iput-object p6, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->f:Ljava/lang/String;

    .line 200
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 316
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 317
    packed-switch v0, :pswitch_data_0

    .line 322
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an int."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 319
    :pswitch_0
    iput p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->e:I

    .line 325
    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 326
    return-void

    .line 317
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 331
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 332
    packed-switch v0, :pswitch_data_0

    .line 343
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 334
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->c:Ljava/lang/String;

    .line 346
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 347
    return-void

    .line 337
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->d:Ljava/lang/String;

    goto :goto_0

    .line 340
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->f:Ljava/lang/String;

    goto :goto_0

    .line 332
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 286
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 296
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->c:Ljava/lang/String;

    .line 294
    :goto_0
    return-object v0

    .line 290
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->d:Ljava/lang/String;

    goto :goto_0

    .line 292
    :pswitch_2
    iget v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 294
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->f:Ljava/lang/String;

    goto :goto_0

    .line 286
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 271
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/b;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 364
    instance-of v0, p1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;

    if-nez v0, :cond_0

    move v0, v1

    .line 395
    :goto_0
    return v0

    .line 369
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 370
    goto :goto_0

    .line 373
    :cond_1
    check-cast p1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;

    .line 374
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 375
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 376
    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 378
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 380
    goto :goto_0

    :cond_3
    move v0, v1

    .line 385
    goto :goto_0

    .line 388
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 390
    goto :goto_0

    :cond_5
    move v0, v2

    .line 395
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 351
    const/4 v0, 0x0

    .line 352
    sget-object v1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 353
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 354
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 355
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 357
    goto :goto_0

    .line 358
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 276
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/b;

    invoke-static {p0, p1}, Lcom/google/android/gms/fitness/apiary/lso/b;->a(Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Api_scopes;Landroid/os/Parcel;)V

    .line 277
    return-void
.end method
