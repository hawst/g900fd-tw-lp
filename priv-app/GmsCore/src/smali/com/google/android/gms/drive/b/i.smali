.class public final Lcom/google/android/gms/drive/b/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/b/g;


# instance fields
.field private final a:Lcom/google/android/gms/drive/auth/g;

.field private final b:Lcom/google/android/gms/drive/database/r;

.field private final c:Lcom/google/android/gms/drive/database/model/EntrySpec;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/i;->a:Lcom/google/android/gms/drive/auth/g;

    .line 45
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/i;->b:Lcom/google/android/gms/drive/database/r;

    .line 46
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/EntrySpec;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/i;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 47
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/i;->d:Ljava/lang/String;

    .line 48
    iput-object p5, p0, Lcom/google/android/gms/drive/b/i;->e:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 20
    iget-object v0, p0, Lcom/google/android/gms/drive/b/i;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/i;->a:Lcom/google/android/gms/drive/auth/g;

    iget-object v2, p0, Lcom/google/android/gms/drive/b/i;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "EntryPersistenceStrategy"

    const-string v1, "Unable to persist content to entry %s because it could not be read."

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/b/i;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ah;->i(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/b/i;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->f(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/b/i;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->d(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ah;->e(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/b/i;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->g(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->r()V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    goto :goto_0
.end method
