.class public final Lcom/google/android/gms/plus/apps/z;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private j:Lcom/google/android/gms/plus/apps/aa;

.field private k:Landroid/accounts/Account;

.field private l:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 43
    return-void
.end method

.method public static a(Landroid/accounts/Account;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;Ljava/lang/String;)Lcom/google/android/gms/plus/apps/z;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 58
    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    const-string v1, "moment"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 60
    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    new-instance v1, Lcom/google/android/gms/plus/apps/z;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/z;-><init>()V

    .line 62
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/support/v4/app/m;->d:Z

    .line 63
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/z;->setArguments(Landroid/os/Bundle;)V

    .line 64
    return-object v1
.end method

.method public static a(Landroid/accounts/Account;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/apps/z;
    .locals 3

    .prologue
    .line 70
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/apps/z;->a(Landroid/accounts/Account;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;Ljava/lang/String;)Lcom/google/android/gms/plus/apps/z;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/z;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 73
    const-string v2, "acl_text"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/z;->setArguments(Landroid/os/Bundle;)V

    .line 75
    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->dismiss()V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->setResult(I)V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    .line 178
    return-void
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 91
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->up:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->tr:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/google/android/gms/l;->dI:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/z;->l:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/z;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/j;->r:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    sget v5, Lcom/google/android/gms/p;->tm:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/q;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/z;->n:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onAttach(Landroid/app/Activity;)V

    .line 116
    instance-of v0, p1, Lcom/google/android/gms/plus/apps/aa;

    if-eqz v0, :cond_0

    .line 117
    check-cast p1, Lcom/google/android/gms/plus/apps/aa;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/z;->j:Lcom/google/android/gms/plus/apps/aa;

    return-void

    .line 119
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DeleteMomentDialog has to be hosted by an Activity that implements OnDeleteMomentAcceptedListener."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 6

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/z;->k:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/common/analytics/c;->w:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/d;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/z;->m:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 165
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/z;->b()V

    .line 168
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 132
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/z;->j:Lcom/google/android/gms/plus/apps/aa;

    if-nez v0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/z;->j:Lcom/google/android/gms/plus/apps/aa;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/z;->k:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/z;->l:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/apps/aa;->a(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/z;->k:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/common/analytics/c;->v:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/d;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/z;->m:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 153
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->dismiss()V

    goto :goto_0

    .line 142
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/z;->k:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/common/analytics/c;->w:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/d;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/apps/z;->m:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 149
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/z;->b()V

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/z;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 83
    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/z;->k:Landroid/accounts/Account;

    .line 84
    const-string v0, "moment"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/z;->l:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    .line 85
    const-string v0, "calling_package_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/z;->m:Ljava/lang/String;

    .line 86
    const-string v0, "acl_text"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/z;->n:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Landroid/support/v4/app/m;->onDetach()V

    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/z;->j:Lcom/google/android/gms/plus/apps/aa;

    .line 128
    return-void
.end method
