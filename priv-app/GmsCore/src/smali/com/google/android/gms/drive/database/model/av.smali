.class final enum Lcom/google/android/gms/drive/database/model/av;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/av;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/av;

.field private static final synthetic d:[Lcom/google/android/gms/drive/database/model/av;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    new-instance v0, Lcom/google/android/gms/drive/database/model/av;

    const-string v1, "CASCADE"

    const-string v2, "CASCADE"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/gms/drive/database/model/av;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/av;->a:Lcom/google/android/gms/drive/database/model/av;

    .line 49
    new-instance v0, Lcom/google/android/gms/drive/database/model/av;

    const-string v1, "SET_NULL"

    const-string v2, "SET NULL"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/database/model/av;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/av;->b:Lcom/google/android/gms/drive/database/model/av;

    .line 47
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/av;

    sget-object v1, Lcom/google/android/gms/drive/database/model/av;->a:Lcom/google/android/gms/drive/database/model/av;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/database/model/av;->b:Lcom/google/android/gms/drive/database/model/av;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/drive/database/model/av;->d:[Lcom/google/android/gms/drive/database/model/av;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 54
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iput-object p3, p0, Lcom/google/android/gms/drive/database/model/av;->c:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/av;
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/google/android/gms/drive/database/model/av;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/av;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/av;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/drive/database/model/av;->d:[Lcom/google/android/gms/drive/database/model/av;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/av;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/av;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/av;->c:Ljava/lang/String;

    return-object v0
.end method
