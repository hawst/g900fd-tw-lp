.class public final Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;
.super Lcom/google/android/gms/plus/circlesbutton/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/people/w;
.implements Lcom/google/android/gms/plus/circlesbutton/h;


# static fields
.field private static v:I

.field private static w:I


# instance fields
.field private final d:Landroid/content/Context;

.field private e:Lcom/google/android/gms/plus/circlesbutton/e;

.field private f:Lcom/google/android/gms/common/api/v;

.field private g:Lcom/google/android/gms/common/api/v;

.field private h:Lcom/google/android/gms/common/people/data/AudienceMember;

.field private i:[Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:Z

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 82
    sput v0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->v:I

    .line 83
    sput v0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->w:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/plus/circlesbutton/d;-><init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    iput-object p1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->d:Landroid/content/Context;

    .line 98
    sget v0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->v:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 99
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->ad:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->v:I

    .line 101
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->ab:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->w:I

    .line 106
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->c()V

    .line 108
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->d()V

    .line 109
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 44
    iget-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->t:Z

    new-instance v0, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->r:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->s:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;-><init>(Ljava/lang/String;I)V

    iget v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    if-ne v1, v3, :cond_3

    :cond_2
    sget-object v1, Lcom/google/android/gms/plus/f;->j:Lcom/google/android/gms/plus/b;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/a/m;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/plus/b;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/gms/plus/f;->j:Lcom/google/android/gms/plus/b;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/a/m;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/plus/b;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;I)V
    .locals 0

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->b(I)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/e;)V
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->j:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/people/model/e;->c()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/android/gms/people/model/e;->b(I)Lcom/google/android/gms/people/model/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->j:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/people/model/e;->d()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->o:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lcom/google/android/gms/people/model/e;->d()V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/k;)V
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/people/model/k;->c()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/android/gms/people/model/k;->b(I)Lcom/google/android/gms/people/model/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/model/j;->d()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->i:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/gms/people/model/k;->d()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->n:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->i:[Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lcom/google/android/gms/people/model/k;->d()V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->r:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->s:I

    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x1

    invoke-super {p0, p1, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Ljava/lang/String;Z)V

    .line 378
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->a(Z)Z

    .line 379
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 181
    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->n:Z

    .line 182
    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->o:Z

    .line 183
    iput-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->j:Ljava/lang/String;

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/p;->dZ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->p:Ljava/lang/String;

    .line 185
    iput-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->i:[Ljava/lang/String;

    .line 186
    iput-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->e:Lcom/google/android/gms/plus/circlesbutton/e;

    .line 187
    iput-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->c:Lcom/google/android/gms/plus/d/c;

    .line 188
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->c(I)V

    .line 197
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->b(Z)V

    .line 198
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    .line 280
    new-instance v0, Lcom/google/android/gms/people/h;

    invoke-direct {v0}, Lcom/google/android/gms/people/h;-><init>()V

    .line 281
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/h;->a(Ljava/util/Collection;)Lcom/google/android/gms/people/h;

    .line 282
    sget-object v1, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/h;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/circlesbutton/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/circlesbutton/b;-><init>(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 290
    new-instance v0, Lcom/google/android/gms/people/d;

    invoke-direct {v0}, Lcom/google/android/gms/people/d;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/d;->a(Ljava/lang/String;)Lcom/google/android/gms/people/d;

    move-result-object v0

    .line 292
    sget-object v1, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/circlesbutton/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/circlesbutton/c;-><init>(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 299
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    if-nez v0, :cond_1

    .line 346
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->d()V

    .line 352
    :goto_0
    return-void

    .line 349
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/x;->m:Lcom/google/android/gms/people/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->k:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    const/4 v5, 0x6

    move-object v2, p0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/v;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;

    .line 351
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->e()V

    goto :goto_0
.end method

.method private g()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 386
    iget-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->e:Lcom/google/android/gms/plus/circlesbutton/e;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/circlesbutton/e;->b:Z

    if-nez v0, :cond_1

    .line 428
    :cond_0
    :goto_0
    return-void

    .line 389
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->i:[Ljava/lang/String;

    if-eqz v0, :cond_2

    array-length v3, v0

    if-nez v3, :cond_3

    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->a(Ljava/lang/String;)V

    .line 390
    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->c(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->b(Z)V

    .line 392
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->i:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->i:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 394
    iget-object v4, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->i:[Ljava/lang/String;

    array-length v5, v4

    move v0, v2

    :goto_2
    if-ge v0, v5, :cond_5

    aget-object v6, v4, v0

    .line 395
    iget-object v7, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->e:Lcom/google/android/gms/plus/circlesbutton/e;

    invoke-virtual {v7, v6}, Lcom/google/android/gms/plus/circlesbutton/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 389
    :cond_3
    array-length v3, v0

    if-ne v3, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->e:Lcom/google/android/gms/plus/circlesbutton/e;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->i:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/circlesbutton/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/p;->dX:I

    new-array v5, v1, [Ljava/lang/Object;

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 403
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    move v0, v1

    :goto_4
    if-eqz v0, :cond_6

    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    .line 405
    :cond_6
    invoke-static {}, Lcom/google/android/gms/common/audience/a/i;->a()Lcom/google/android/gms/common/audience/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->k:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/k;->m(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/k;->l(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/gms/common/audience/a/k;->d(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/k;->b(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/audience/a/k;

    move-result-object v0

    sget v1, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->v:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/k;->a(I)Lcom/google/android/gms/common/audience/a/k;

    move-result-object v0

    sget v1, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->w:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/k;->b(I)Lcom/google/android/gms/common/audience/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->q:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/k;->i(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;

    move-result-object v0

    .line 414
    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/k;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 403
    goto :goto_4

    .line 420
    :cond_8
    new-instance v0, Lcom/google/android/gms/common/audience/a/m;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->q:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/audience/a/m;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/m;->a(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/m;->b(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/m;->a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/audience/a/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->r:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/m;->a:Landroid/content/Intent;

    const-string v3, "EXTRA_START_VIEW_NAMESPACE"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->s:I

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/m;->a:Landroid/content/Intent;

    const-string v3, "EXTRA_START_VIEW_TYPE_NUM"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 427
    iget-object v0, v0, Lcom/google/android/gms/common/audience/a/m;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final Z_()V
    .locals 0

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->e()V

    .line 277
    return-void
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 304
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g()V

    .line 305
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 271
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/lang/String;Lcom/google/android/gms/plus/d/c;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 123
    iput-object p1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->k:Ljava/lang/String;

    .line 124
    iput-object p2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    .line 125
    iput-object p3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 126
    iput-object p4, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->q:Ljava/lang/String;

    .line 127
    invoke-static {p4}, Lcom/google/android/gms/common/analytics/a;->a(Ljava/lang/String;)I

    move-result v0

    .line 128
    iput-boolean v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->t:Z

    .line 129
    iput v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->u:I

    .line 130
    const-string v1, "sg"

    iput-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->r:Ljava/lang/String;

    .line 131
    iput v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->s:I

    .line 133
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->c()V

    .line 134
    iput-object p5, p0, Lcom/google/android/gms/plus/circlesbutton/d;->c:Lcom/google/android/gms/plus/d/c;

    .line 136
    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    if-nez v1, :cond_0

    .line 137
    new-instance v1, Lcom/google/android/gms/common/api/w;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->d:Landroid/content/Context;

    invoke-direct {v1, v2, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    sget-object v2, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    new-instance v3, Lcom/google/android/gms/people/ad;

    invoke-direct {v3}, Lcom/google/android/gms/people/ad;-><init>()V

    iput v0, v3, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v3}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    .line 143
    :cond_0
    new-instance v0, Lcom/google/android/gms/plus/circlesbutton/e;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/gms/plus/circlesbutton/e;-><init>(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/circlesbutton/h;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->e:Lcom/google/android/gms/plus/circlesbutton/e;

    .line 145
    new-instance v0, Lcom/google/android/gms/common/api/w;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/plus/f;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->k:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 151
    new-instance v0, Lcom/google/android/gms/plus/circlesbutton/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/circlesbutton/a;-><init>(Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;)V

    .line 157
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 159
    iget-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->m:Z

    if-nez v0, :cond_1

    .line 169
    :goto_0
    return-void

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->e:Lcom/google/android/gms/plus/circlesbutton/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/circlesbutton/e;->a()V

    .line 165
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f()V

    goto :goto_0

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method

.method protected final a(Z)Z
    .locals 2

    .prologue
    .line 363
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Z)Z

    move-result v1

    .line 364
    if-eqz v1, :cond_0

    .line 365
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->b(Z)V

    .line 367
    :cond_0
    return v1

    .line 365
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->e:Lcom/google/android/gms/plus/circlesbutton/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/circlesbutton/e;->a()V

    .line 260
    invoke-direct {p0}, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f()V

    .line 261
    return-void
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 266
    return-void
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 235
    invoke-super {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->onAttachedToWindow()V

    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->m:Z

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 243
    :cond_1
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 356
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/circlesbutton/d;->onClick(Landroid/view/View;)V

    .line 357
    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->u:I

    .line 358
    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->u:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_1

    .line 359
    :cond_0
    return-void

    .line 358
    :cond_1
    new-instance v0, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->r:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->s:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;-><init>(Ljava/lang/String;I)V

    :goto_0
    iget v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->u:I

    if-lez v1, :cond_0

    sget-object v1, Lcom/google/android/gms/plus/f;->j:Lcom/google/android/gms/plus/b;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->l:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/a/m;->o:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/plus/b;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iget v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->u:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->u:I

    goto :goto_0
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 253
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/AddToCirclesButtonImpl;->m:Z

    .line 254
    invoke-super {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->onDetachedFromWindow()V

    .line 255
    return-void
.end method
