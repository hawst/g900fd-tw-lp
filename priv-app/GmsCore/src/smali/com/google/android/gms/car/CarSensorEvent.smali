.class public Lcom/google/android/gms/car/CarSensorEvent;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:I

.field public c:J

.field public final d:[F

.field public final e:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/car/gk;

    invoke-direct {v0}, Lcom/google/android/gms/car/gk;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarSensorEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIJ[F[B)V
    .locals 1

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    iput p1, p0, Lcom/google/android/gms/car/CarSensorEvent;->a:I

    .line 223
    iput p2, p0, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    .line 224
    iput-wide p3, p0, Lcom/google/android/gms/car/CarSensorEvent;->c:J

    .line 225
    iput-object p5, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    .line 226
    iput-object p6, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    .line 227
    return-void
.end method

.method public constructor <init>(IJII)V
    .locals 2

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->a:I

    .line 248
    iput p1, p0, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    .line 249
    iput-wide p2, p0, Lcom/google/android/gms/car/CarSensorEvent;->c:J

    .line 250
    new-array v0, p4, [F

    iput-object v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    .line 251
    new-array v0, p5, [B

    iput-object v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    .line 252
    return-void
.end method

.method private static a([BI)I
    .locals 5

    .prologue
    .line 760
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    .line 761
    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    .line 762
    add-int/lit8 v2, p1, 0x2

    aget-byte v2, p0, v2

    shl-int/lit8 v2, v2, 0x10

    const/high16 v3, 0xff0000

    and-int/2addr v2, v3

    .line 763
    add-int/lit8 v3, p1, 0x3

    aget-byte v3, p0, v3

    shl-int/lit8 v3, v3, 0x18

    const/high16 v4, -0x1000000

    and-int/2addr v3, v4

    .line 764
    or-int/2addr v0, v1

    or-int/2addr v0, v2

    or-int/2addr v0, v3

    return v0
.end method

.method private a(I)V
    .locals 5

    .prologue
    .line 263
    iget v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    if-ne v0, p1, :cond_0

    .line 264
    return-void

    .line 266
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Invalid sensor type: expected %d, got %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a([BII)V
    .locals 2

    .prologue
    .line 769
    and-int/lit16 v0, p2, 0xff

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 770
    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 771
    add-int/lit8 v0, p1, 0x2

    shr-int/lit8 v1, p2, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 772
    add-int/lit8 v0, p1, 0x3

    shr-int/lit8 v1, p2, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 773
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->a:I

    return v0
.end method

.method public final a(Landroid/location/Location;)Landroid/location/Location;
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v5, 0x1

    const/4 v2, 0x0

    const-wide v6, 0x3e7ad7f29abcaf48L    # 1.0E-7

    const/4 v4, 0x2

    .line 540
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/CarSensorEvent;->a(I)V

    .line 541
    if-nez p1, :cond_0

    .line 542
    new-instance p1, Landroid/location/Location;

    const-string v0, "Car-GPS"

    invoke-direct {p1, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    aget-byte v0, v0, v2

    .line 546
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_1

    .line 547
    iget v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->a:I

    if-lt v1, v4, :cond_7

    .line 548
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    invoke-static {v1, v5}, Lcom/google/android/gms/car/CarSensorEvent;->a([BI)I

    move-result v1

    .line 549
    int-to-double v2, v1

    mul-double/2addr v2, v6

    invoke-virtual {p1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 554
    :cond_1
    :goto_0
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_2

    .line 555
    iget v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->a:I

    if-lt v1, v4, :cond_8

    .line 556
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    invoke-static {v1, v8}, Lcom/google/android/gms/car/CarSensorEvent;->a([BI)I

    move-result v1

    .line 557
    int-to-double v2, v1

    mul-double/2addr v2, v6

    invoke-virtual {p1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 562
    :cond_2
    :goto_1
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_3

    .line 563
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    aget v1, v1, v4

    invoke-virtual {p1, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 565
    :cond_3
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_4

    .line 566
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v2, 0x3

    aget v1, v1, v2

    float-to-double v2, v1

    invoke-virtual {p1, v2, v3}, Landroid/location/Location;->setAltitude(D)V

    .line 568
    :cond_4
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_5

    .line 569
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    invoke-virtual {p1, v1}, Landroid/location/Location;->setSpeed(F)V

    .line 571
    :cond_5
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 572
    iget-object v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    aget v0, v0, v8

    invoke-virtual {p1, v0}, Landroid/location/Location;->setBearing(F)V

    .line 574
    :cond_6
    iget-wide v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    .line 578
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 579
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    .line 580
    iget-wide v4, p0, Lcom/google/android/gms/car/CarSensorEvent;->c:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setTime(J)V

    .line 582
    return-object p1

    .line 551
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    aget v1, v1, v2

    float-to-double v2, v1

    invoke-virtual {p1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    goto :goto_0

    .line 559
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    aget v1, v1, v5

    float-to-double v2, v1

    invoke-virtual {p1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/car/gf;)Lcom/google/android/gms/car/gf;
    .locals 2

    .prologue
    .line 492
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/CarSensorEvent;->a(I)V

    .line 493
    if-nez p1, :cond_0

    .line 494
    new-instance p1, Lcom/google/android/gms/car/gf;

    invoke-direct {p1}, Lcom/google/android/gms/car/gf;-><init>()V

    .line 496
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->c:J

    iput-wide v0, p1, Lcom/google/android/gms/car/gf;->a:J

    .line 497
    iget-object v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p1, Lcom/google/android/gms/car/gf;->b:F

    .line 498
    return-object p1
.end method

.method public final a(Lcom/google/android/gms/car/gg;)Lcom/google/android/gms/car/gg;
    .locals 2

    .prologue
    .line 600
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/CarSensorEvent;->a(I)V

    .line 601
    if-nez p1, :cond_0

    .line 602
    new-instance p1, Lcom/google/android/gms/car/gg;

    invoke-direct {p1}, Lcom/google/android/gms/car/gg;-><init>()V

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    iput-byte v0, p1, Lcom/google/android/gms/car/gg;->a:B

    .line 605
    return-object p1
.end method

.method public final a(Lcom/google/android/gms/car/gh;)Lcom/google/android/gms/car/gh;
    .locals 2

    .prologue
    .line 365
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/CarSensorEvent;->a(I)V

    .line 366
    if-nez p1, :cond_0

    .line 367
    new-instance p1, Lcom/google/android/gms/car/gh;

    invoke-direct {p1}, Lcom/google/android/gms/car/gh;-><init>()V

    .line 369
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->c:J

    iput-wide v0, p1, Lcom/google/android/gms/car/gh;->a:J

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    iput v0, p1, Lcom/google/android/gms/car/gh;->b:I

    .line 371
    return-object p1
.end method

.method public final a(Lcom/google/android/gms/car/gi;)Lcom/google/android/gms/car/gi;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 341
    const/16 v2, 0x9

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/CarSensorEvent;->a(I)V

    .line 342
    if-nez p1, :cond_0

    .line 343
    new-instance p1, Lcom/google/android/gms/car/gi;

    invoke-direct {p1}, Lcom/google/android/gms/car/gi;-><init>()V

    .line 345
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/car/CarSensorEvent;->c:J

    iput-wide v2, p1, Lcom/google/android/gms/car/gi;->a:J

    .line 346
    iget-object v2, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    aget-byte v2, v2, v1

    if-ne v2, v0, :cond_1

    :goto_0
    iput-boolean v0, p1, Lcom/google/android/gms/car/gi;->b:Z

    .line 347
    return-object p1

    :cond_1
    move v0, v1

    .line 346
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/gj;)Lcom/google/android/gms/car/gj;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 389
    const/4 v2, 0x6

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/CarSensorEvent;->a(I)V

    .line 390
    if-nez p1, :cond_0

    .line 391
    new-instance p1, Lcom/google/android/gms/car/gj;

    invoke-direct {p1}, Lcom/google/android/gms/car/gj;-><init>()V

    .line 393
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/car/CarSensorEvent;->c:J

    iput-wide v2, p1, Lcom/google/android/gms/car/gj;->a:J

    .line 394
    iget-object v2, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    aget-byte v2, v2, v1

    if-ne v2, v0, :cond_1

    :goto_0
    iput-boolean v0, p1, Lcom/google/android/gms/car/gj;->b:Z

    .line 395
    return-object p1

    :cond_1
    move v0, v1

    .line 394
    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 778
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 779
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 780
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "type:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 781
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    array-length v1, v1

    if-lez v1, :cond_0

    .line 782
    const-string v1, " float values:"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 783
    iget-object v3, p0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 784
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 783
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 787
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    array-length v1, v1

    if-lez v1, :cond_1

    .line 788
    const-string v1, " byte values:"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    iget-object v1, p0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-byte v4, v1, v0

    .line 790
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 793
    :cond_1
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 236
    invoke-static {p0, p1}, Lcom/google/android/gms/car/gk;->a(Lcom/google/android/gms/car/CarSensorEvent;Landroid/os/Parcel;)V

    .line 237
    return-void
.end method
