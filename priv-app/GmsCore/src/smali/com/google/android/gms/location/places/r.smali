.class final Lcom/google/android/gms/location/places/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/i;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/location/places/r;-><init>(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/gms/location/places/r;->a:Ljava/lang/String;

    .line 74
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 110
    const v0, 0x7fffffff

    return v0
.end method

.method public final synthetic a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/lang/Object;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/h;
    .locals 9

    .prologue
    .line 62
    check-cast p4, Lcom/google/android/gms/location/places/s;

    iget-object v0, p0, Lcom/google/android/gms/location/places/r;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/location/places/r;->a:Ljava/lang/String;

    :goto_0
    if-nez p4, :cond_1

    new-instance v0, Lcom/google/android/gms/location/places/t;

    invoke-direct {v0}, Lcom/google/android/gms/location/places/t;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/t;->a()Lcom/google/android/gms/location/places/s;

    move-result-object v8

    :goto_1
    new-instance v0, Lcom/google/android/gms/location/places/internal/l;

    const-string v6, "places"

    iget-object v1, p3, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->a()Ljava/lang/String;

    move-result-object v7

    move-object v1, p1

    move-object v2, p2

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/location/places/internal/l;-><init>(Landroid/content/Context;Landroid/os/Looper;Ljava/lang/String;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/s;)V

    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    move-object v8, p4

    goto :goto_1
.end method
