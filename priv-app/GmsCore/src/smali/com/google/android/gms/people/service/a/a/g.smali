.class public abstract Lcom/google/android/gms/people/service/a/a/g;
.super Lcom/google/android/gms/people/service/a/a/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/a/a/e;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V

    .line 30
    return-void
.end method

.method private static a(Landroid/os/ParcelFileDescriptor;Z)Lcom/google/android/gms/people/f/m;
    .locals 4

    .prologue
    .line 72
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "PeopleService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BasePeoplePfdImageOperation: returnFileBasedPfd="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    if-nez p0, :cond_1

    .line 77
    const/4 v0, 0x0

    .line 103
    :goto_0
    return-object v0

    .line 79
    :cond_1
    if-eqz p1, :cond_2

    .line 81
    new-instance v0, Lcom/google/android/gms/people/f/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/f/m;-><init>(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    .line 87
    :cond_2
    sget-object v0, Lcom/google/android/gms/people/f/l;->a:Lcom/google/android/gms/people/f/l;

    invoke-static {}, Lcom/google/android/gms/people/f/l;->a()[Lcom/google/android/gms/people/f/m;

    move-result-object v0

    .line 88
    const/4 v1, 0x0

    aget-object v1, v0, v1

    .line 89
    const/4 v2, 0x1

    aget-object v2, v0, v2

    .line 94
    :try_start_0
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/people/a/a;->aA:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v3, Lcom/google/android/gms/people/service/a/a/h;

    invoke-direct {v3, p0, v2, v0}, Lcom/google/android/gms/people/service/a/a/h;-><init>(Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/people/f/m;I)V

    invoke-static {v3}, Lcom/google/android/gms/people/f/j;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 103
    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 98
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 99
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 100
    throw v0
.end method


# virtual methods
.method protected abstract g()Lcom/google/android/gms/people/f/a;
.end method

.method protected final h()Lcom/google/android/gms/people/service/a/a/f;
    .locals 6

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/a/g;->g()Lcom/google/android/gms/people/f/a;

    move-result-object v2

    .line 35
    if-nez v2, :cond_0

    .line 36
    sget-object v0, Lcom/google/android/gms/people/service/a/a/f;->a:Lcom/google/android/gms/people/service/a/a/f;

    .line 52
    :goto_0
    return-object v0

    .line 39
    :cond_0
    :try_start_0
    iget-object v1, v2, Lcom/google/android/gms/people/f/a;->a:Landroid/os/ParcelFileDescriptor;

    sget-object v0, Lcom/google/android/gms/people/a/a;->aF:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v1, v3, v0}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    .line 41
    :goto_1
    sget-object v0, Lcom/google/android/gms/people/a/a;->aG:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 43
    iget-object v0, v2, Lcom/google/android/gms/people/f/a;->b:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0, v3}, Lcom/google/android/gms/people/service/a/a/g;->a(Landroid/os/ParcelFileDescriptor;Z)Lcom/google/android/gms/people/f/m;

    move-result-object v4

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v5, v0, v3}, Lcom/google/android/gms/people/service/a/a/f;->a(Lcom/google/android/gms/people/f/m;IIZ)Lcom/google/android/gms/people/service/a/a/f;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 52
    iget-object v1, v2, Lcom/google/android/gms/people/f/a;->a:Landroid/os/ParcelFileDescriptor;

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    .line 39
    :cond_1
    const/4 v0, -0x1

    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 47
    :catch_0
    move-exception v0

    .line 48
    :try_start_2
    iget-object v1, v2, Lcom/google/android/gms/people/f/a;->b:Landroid/os/ParcelFileDescriptor;

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    .line 49
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 52
    :catchall_0
    move-exception v0

    iget-object v1, v2, Lcom/google/android/gms/people/f/a;->a:Landroid/os/ParcelFileDescriptor;

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0
.end method
