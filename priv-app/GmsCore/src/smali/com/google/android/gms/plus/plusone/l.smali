.class final Lcom/google/android/gms/plus/plusone/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/et;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/plusone/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/plusone/e;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/l;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    .line 266
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    .line 267
    const-string v1, "PlusOneButtonView"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    const-string v1, "PlusOneButtonView"

    const-string v2, "onConnectionFailed: errorCode=%d resolution=%s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->d()Landroid/app/PendingIntent;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_0
    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/l;->a:Lcom/google/android/gms/plus/plusone/e;

    iput-boolean v6, v0, Lcom/google/android/gms/plus/plusone/e;->o:Z

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/l;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-static {v0}, Lcom/google/android/gms/plus/plusone/e;->j(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/l;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-static {v0, p1}, Lcom/google/android/gms/plus/plusone/e;->a(Lcom/google/android/gms/plus/plusone/e;Lcom/google/android/gms/common/c;)Lcom/google/android/gms/common/c;

    .line 282
    :cond_2
    return-void
.end method
