.class public Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;
.super Lcom/google/android/gms/wallet/common/ui/dq;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field protected a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field c:Landroid/widget/ProgressBar;

.field d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

.field e:Lcom/google/android/gms/wallet/common/ui/bb;

.field f:Lcom/google/android/gms/wallet/common/ui/bb;

.field g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private i:Z

.field private j:Lcom/google/checkout/inapp/proto/a/b;

.field private k:Ljava/lang/String;

.field private l:Lcom/google/checkout/inapp/proto/q;

.field private m:Z

.field private n:Lcom/google/android/gms/wallet/common/ui/dh;

.field private o:Z

.field private p:I

.field private final q:Lcom/google/android/gms/wallet/service/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "updateAddress"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dq;-><init>()V

    .line 62
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->i:Z

    .line 68
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->o:Z

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->p:I

    .line 396
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/dj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/dj;-><init>(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->q:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;Z)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 303
    if-eqz p1, :cond_1

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 308
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 309
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez p1, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Z)V

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_4

    :goto_3
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 313
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->m:Z

    .line 314
    return-void

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->c:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 308
    goto :goto_1

    :cond_3
    move v0, v2

    .line 309
    goto :goto_2

    :cond_4
    move v1, v2

    .line 311
    goto :goto_3
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->o:Z

    return v0
.end method

.method private b(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 334
    const/4 v0, 0x2

    new-array v4, v0, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v0, v4, v1

    .line 336
    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 338
    if-eqz v6, :cond_0

    .line 339
    if-eqz p1, :cond_2

    .line 342
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    .line 336
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 342
    goto :goto_1

    .line 343
    :cond_2
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 347
    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Lcom/google/checkout/inapp/proto/q;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->l:Lcom/google/checkout/inapp/proto/q;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->i:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/bb;->c()Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "UpdateAddressActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "UpdateAddressActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 291
    packed-switch p2, :pswitch_data_0

    .line 297
    const-string v0, "UpdateAddressActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :goto_0
    return-void

    .line 294
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a(Z)V

    goto :goto_0

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b(Z)Z

    move-result v0

    return v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 318
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->finish()V

    .line 319
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/b;->D:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->overridePendingTransition(II)V

    .line 320
    return-void
.end method

.method public final g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 352
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    :goto_0
    return v0

    .line 355
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 356
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 357
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto :goto_0

    .line 360
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final h()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 366
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onCreate(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 90
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ck;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 93
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 94
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 96
    const-string v1, "com.google.android.gms.wallet.address"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    const-string v5, "Activity requires address extra!"

    invoke-static {v1, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 98
    const-string v1, "com.google.android.gms.wallet.address"

    const-class v5, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v4, v1, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/a/b;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    .line 101
    const-string v1, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->k:Ljava/lang/String;

    .line 103
    const-string v1, "com.google.android.gms.wallet.accountReference"

    const-class v5, Lcom/google/checkout/inapp/proto/q;

    invoke-static {v4, v1, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/q;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->l:Lcom/google/checkout/inapp/proto/q;

    .line 106
    const-string v1, "com.google.android.gms.wallet.localMode"

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->i:Z

    .line 108
    sget v1, Lcom/google/android/gms/l;->gp:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->setContentView(I)V

    .line 110
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v1

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->i:Z

    if-nez v5, :cond_5

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/support/v7/app/a;->a()V

    sget v5, Lcom/google/android/gms/p;->CT:I

    invoke-virtual {v1, v5}, Landroid/support/v7/app/a;->c(I)V

    :cond_0
    :goto_0
    sget v1, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->c:Landroid/widget/ProgressBar;

    sget v1, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    new-instance v5, Lcom/google/android/gms/wallet/common/ui/di;

    invoke-direct {v5, p0}, Lcom/google/android/gms/wallet/common/ui/di;-><init>(Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;)V

    invoke-virtual {v1, v5}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    const-string v1, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->o:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    sget v5, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v1, v5}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez v1, :cond_2

    const-string v1, "com.google.android.gms.wallet.allowedCountryCodes"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v5, v5, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v5, v5, Lcom/google/t/a/b;->a:Ljava/lang/String;

    const-string v6, "com.google.android.gms.wallet.addressHints"

    const-class v7, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->o:Z

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v8}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v8}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v8

    if-nez v8, :cond_1

    move v2, v3

    :cond_1
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v8

    invoke-virtual {v8, v1}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/List;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v5

    iget-object v1, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/Collection;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->o:Z

    invoke-virtual {v1, v5}, Lcom/google/android/gms/wallet/common/ui/s;->b(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/s;->c(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/t/a/b;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->gr:I

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1, v2, v5}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    :cond_2
    sget v1, Lcom/google/android/gms/j;->ua:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    if-nez v1, :cond_3

    .line 115
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->i:Z

    if-nez v1, :cond_7

    .line 116
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v3, v1, v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 123
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 127
    :cond_3
    return-void

    .line 110
    :cond_4
    sget v1, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    sget v5, Lcom/google/android/gms/p;->CT:I

    invoke-virtual {v1, v5}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    goto/16 :goto_0

    :cond_5
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/support/v7/app/a;->a()V

    sget v5, Lcom/google/android/gms/p;->BT:I

    invoke-virtual {v1, v5}, Landroid/support/v7/app/a;->c(I)V

    :goto_2
    sget v1, Lcom/google/android/gms/j;->fn:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v5, v1, Lcom/google/t/a/b;->t:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, v1, Lcom/google/t/a/b;->t:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    sget v1, Lcom/google/android/gms/j;->nK:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    sget v5, Lcom/google/android/gms/p;->BT:I

    invoke-virtual {v1, v5}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(I)V

    goto :goto_2

    .line 119
    :cond_7
    const-string v1, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    goto :goto_1
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 273
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onPause()V

    .line 275
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->q:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;)V

    .line 277
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 131
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onPostCreate(Landroid/os/Bundle;)V

    .line 133
    if-eqz p1, :cond_1

    .line 134
    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->m:Z

    .line 135
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->p:I

    .line 143
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->m:Z

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a(Z)V

    .line 146
    :cond_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "update_address"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 253
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onResume()V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "UpdateAddressActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 260
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "UpdateAddressActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 266
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->q:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->p:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    .line 269
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 281
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 282
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->q:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->p:I

    .line 285
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 286
    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateAddressActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 287
    return-void
.end method
