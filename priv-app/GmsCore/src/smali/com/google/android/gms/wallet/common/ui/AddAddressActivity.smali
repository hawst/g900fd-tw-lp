.class public Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;
.super Lcom/google/android/gms/wallet/common/ui/dq;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# static fields
.field private static final i:Ljava/lang/String;


# instance fields
.field protected a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field c:Landroid/widget/ProgressBar;

.field d:Landroid/widget/CheckBox;

.field e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

.field f:Lcom/google/android/gms/wallet/common/ui/bb;

.field g:Lcom/google/android/gms/wallet/common/ui/bb;

.field h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Lcom/google/checkout/inapp/proto/q;

.field private m:Z

.field private n:Lcom/google/android/gms/wallet/common/ui/dh;

.field private o:Z

.field private p:I

.field private final q:Lcom/google/android/gms/wallet/service/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "addAddress"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dq;-><init>()V

    .line 63
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j:Z

    .line 68
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->o:Z

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:I

    .line 420
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/d;-><init>(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->q:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;Z)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 308
    if-eqz p1, :cond_2

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 313
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 314
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez p1, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Z)V

    .line 315
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 316
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d:Landroid/widget/CheckBox;

    if-nez p1, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_6

    :goto_4
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 321
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->m:Z

    .line 322
    return-void

    .line 311
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 313
    goto :goto_1

    :cond_4
    move v0, v2

    .line 314
    goto :goto_2

    :cond_5
    move v0, v2

    .line 316
    goto :goto_3

    :cond_6
    move v1, v2

    .line 319
    goto :goto_4
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->o:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->k:Ljava/lang/String;

    return-object v0
.end method

.method private b(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 336
    const/4 v0, 0x2

    new-array v4, v0, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v0, v4, v1

    .line 338
    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 340
    if-eqz v6, :cond_0

    .line 341
    if-eqz p1, :cond_2

    .line 344
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    .line 338
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 344
    goto :goto_1

    .line 345
    :cond_2
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 349
    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Lcom/google/checkout/inapp/proto/q;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->l:Lcom/google/checkout/inapp/proto/q;

    return-object v0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 389
    if-eqz p1, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    sget v1, Lcom/google/android/gms/p;->Cw:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    .line 394
    :goto_0
    return-void

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    sget v1, Lcom/google/android/gms/p;->Au:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/bb;->c()Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "AddAddressActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "AddAddressActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 296
    packed-switch p2, :pswitch_data_0

    .line 302
    const-string v0, "AddAddressActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :goto_0
    return-void

    .line 299
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(Z)V

    goto :goto_0

    .line 296
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Z)Z

    move-result v0

    return v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 376
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->finish()V

    .line 377
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/b;->D:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->overridePendingTransition(II)V

    .line 378
    return-void
.end method

.method public final g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 354
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    :goto_0
    return v0

    .line 357
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 358
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 359
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto :goto_0

    .line 362
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final h()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 368
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j:Z

    if-eqz v0, :cond_0

    .line 384
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c(Z)V

    .line 386
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 89
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onCreate(Landroid/os/Bundle;)V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ck;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;

    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 95
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 96
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 97
    const-string v1, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v5, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->k:Ljava/lang/String;

    .line 99
    const-string v1, "com.google.android.gms.wallet.accountReference"

    const-class v2, Lcom/google/checkout/inapp/proto/q;

    invoke-static {v5, v1, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/q;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->l:Lcom/google/checkout/inapp/proto/q;

    .line 101
    const-string v1, "com.google.android.gms.wallet.localMode"

    invoke-virtual {v5, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j:Z

    .line 103
    sget v1, Lcom/google/android/gms/l;->fW:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->setContentView(I)V

    .line 105
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v6

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j:Z

    if-nez v1, :cond_4

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/support/v7/app/a;->a()V

    sget v1, Lcom/google/android/gms/p;->zt:I

    invoke-virtual {v6, v1}, Landroid/support/v7/app/a;->c(I)V

    move v2, v3

    :goto_0
    sget v1, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c:Landroid/widget/ProgressBar;

    sget v1, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c(Z)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    new-instance v2, Lcom/google/android/gms/wallet/common/ui/c;

    invoke-direct {v2, p0}, Lcom/google/android/gms/wallet/common/ui/c;-><init>(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    const-string v1, "com.google.android.gms.wallet.allowedCountryCodes"

    invoke-virtual {v5, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    const-string v1, "com.google.android.gms.wallet.defaultCountryCode"

    invoke-virtual {v5, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v1, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v5, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->o:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    sget v4, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v1, v4}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez v1, :cond_1

    const-string v1, "com.google.android.gms.wallet.addressHints"

    const-class v4, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v1, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/List;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v2

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/Collection;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->o:Z

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/s;->b(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    const-string v1, "com.google.android.gms.wallet.baseAddress"

    const-class v2, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v1, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v4, v1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/t/a/b;)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->gr:I

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1, v2, v4}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    :cond_1
    sget v1, Lcom/google/android/gms/j;->ua:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;)V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    if-nez v1, :cond_2

    .line 110
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->j:Z

    if-nez v1, :cond_7

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v3, v1, v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 118
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 122
    :cond_2
    return-void

    .line 105
    :cond_3
    sget v1, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    sget v2, Lcom/google/android/gms/p;->zt:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_4
    sget v1, Lcom/google/android/gms/j;->qz:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d:Landroid/widget/CheckBox;

    const-string v1, "com.google.android.gms.wallet.allowSaveToChromeOption"

    invoke-virtual {v5, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move v2, v3

    :goto_2
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Landroid/support/v7/app/a;->a()V

    sget v1, Lcom/google/android/gms/p;->BQ:I

    invoke-virtual {v6, v1}, Landroid/support/v7/app/a;->c(I)V

    :goto_3
    sget v1, Lcom/google/android/gms/j;->fn:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    move v2, v4

    goto :goto_2

    :cond_6
    sget v1, Lcom/google/android/gms/j;->nK:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    sget v6, Lcom/google/android/gms/p;->BQ:I

    invoke-virtual {v1, v6}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(I)V

    goto :goto_3

    .line 114
    :cond_7
    const-string v1, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v5, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->n:Lcom/google/android/gms/wallet/common/ui/dh;

    goto :goto_1
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 277
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onPause()V

    .line 279
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->q:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;)V

    .line 281
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onPostCreate(Landroid/os/Bundle;)V

    .line 128
    if-eqz p1, :cond_1

    .line 129
    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->m:Z

    .line 130
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:I

    .line 138
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->m:Z

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(Z)V

    .line 141
    :cond_0
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "add_address"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 257
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onResume()V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "AddAddressActivity.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 264
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "AddAddressActivity.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->q:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    .line 273
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 285
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 287
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->q:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:I

    .line 290
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 291
    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 292
    return-void
.end method
