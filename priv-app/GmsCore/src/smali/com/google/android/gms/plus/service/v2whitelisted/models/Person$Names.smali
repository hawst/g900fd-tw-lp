.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/bg;

.field private static final o:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:Ljava/lang/String;

.field n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 7298
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/bg;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/bg;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/bg;

    .line 7363
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 7366
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "displayName"

    const-string v2, "displayName"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7367
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "familyName"

    const-string v2, "familyName"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7368
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "formatted"

    const-string v2, "formatted"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7369
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "givenName"

    const-string v2, "givenName"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7370
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "honorificPrefix"

    const-string v2, "honorificPrefix"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7371
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "honorificSuffix"

    const-string v2, "honorificSuffix"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7372
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "metadata"

    const-string v2, "metadata"

    const/16 v3, 0x8

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7375
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "middleName"

    const-string v2, "middleName"

    const/16 v3, 0x9

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7376
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "phoneticFamilyName"

    const-string v2, "phoneticFamilyName"

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7377
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "phoneticGivenName"

    const-string v2, "phoneticGivenName"

    const/16 v3, 0xb

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7378
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "phoneticHonorificPrefix"

    const-string v2, "phoneticHonorificPrefix"

    const/16 v3, 0xc

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7379
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    const-string v1, "phoneticHonorificSuffix"

    const-string v2, "phoneticHonorificSuffix"

    const/16 v3, 0xd

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7380
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7471
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 7472
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b:I

    .line 7473
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->a:Ljava/util/Set;

    .line 7474
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 7492
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 7493
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->a:Ljava/util/Set;

    .line 7494
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b:I

    .line 7495
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->c:Ljava/lang/String;

    .line 7496
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d:Ljava/lang/String;

    .line 7497
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->e:Ljava/lang/String;

    .line 7498
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->f:Ljava/lang/String;

    .line 7499
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->g:Ljava/lang/String;

    .line 7500
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->h:Ljava/lang/String;

    .line 7501
    iput-object p9, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->i:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    .line 7502
    iput-object p10, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->j:Ljava/lang/String;

    .line 7503
    iput-object p11, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->k:Ljava/lang/String;

    .line 7504
    iput-object p12, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->l:Ljava/lang/String;

    .line 7505
    iput-object p13, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->m:Ljava/lang/String;

    .line 7506
    iput-object p14, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->n:Ljava/lang/String;

    .line 7507
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 7384
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 7832
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 7833
    packed-switch v0, :pswitch_data_0

    .line 7838
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7835
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->i:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    .line 7842
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 7843
    return-void

    .line 7833
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 7787
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 7788
    packed-switch v0, :pswitch_data_0

    .line 7823
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7790
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->c:Ljava/lang/String;

    .line 7826
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 7827
    return-void

    .line 7793
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d:Ljava/lang/String;

    goto :goto_0

    .line 7796
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->e:Ljava/lang/String;

    goto :goto_0

    .line 7799
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->f:Ljava/lang/String;

    goto :goto_0

    .line 7802
    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->g:Ljava/lang/String;

    goto :goto_0

    .line 7805
    :pswitch_6
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->h:Ljava/lang/String;

    goto :goto_0

    .line 7808
    :pswitch_7
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->j:Ljava/lang/String;

    goto :goto_0

    .line 7811
    :pswitch_8
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->k:Ljava/lang/String;

    goto :goto_0

    .line 7814
    :pswitch_9
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->l:Ljava/lang/String;

    goto :goto_0

    .line 7817
    :pswitch_a
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->m:Ljava/lang/String;

    goto :goto_0

    .line 7820
    :pswitch_b
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->n:Ljava/lang/String;

    goto :goto_0

    .line 7788
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 7736
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 7741
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 7767
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7743
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->c:Ljava/lang/String;

    .line 7765
    :goto_0
    return-object v0

    .line 7745
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d:Ljava/lang/String;

    goto :goto_0

    .line 7747
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->e:Ljava/lang/String;

    goto :goto_0

    .line 7749
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->f:Ljava/lang/String;

    goto :goto_0

    .line 7751
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->g:Ljava/lang/String;

    goto :goto_0

    .line 7753
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->h:Ljava/lang/String;

    goto :goto_0

    .line 7755
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->i:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    goto :goto_0

    .line 7757
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->j:Ljava/lang/String;

    goto :goto_0

    .line 7759
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->k:Ljava/lang/String;

    goto :goto_0

    .line 7761
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->l:Ljava/lang/String;

    goto :goto_0

    .line 7763
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->m:Ljava/lang/String;

    goto :goto_0

    .line 7765
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->n:Ljava/lang/String;

    goto :goto_0

    .line 7741
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7548
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7563
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7578
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 7726
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/bg;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7593
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 7860
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    if-nez v0, :cond_0

    move v0, v1

    .line 7891
    :goto_0
    return v0

    .line 7865
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 7866
    goto :goto_0

    .line 7869
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    .line 7870
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 7871
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 7872
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 7874
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 7876
    goto :goto_0

    :cond_3
    move v0, v1

    .line 7881
    goto :goto_0

    .line 7884
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 7886
    goto :goto_0

    :cond_5
    move v0, v2

    .line 7891
    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7608
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7623
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;
    .locals 1

    .prologue
    .line 7638
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->i:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 7847
    const/4 v0, 0x0

    .line 7848
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 7849
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 7850
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 7851
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 7853
    goto :goto_0

    .line 7854
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7653
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7668
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7683
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7698
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7713
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 7731
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/bg;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/bg;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;Landroid/os/Parcel;I)V

    .line 7732
    return-void
.end method
