.class public abstract Lcom/google/android/gms/drive/metadata/a/al;
.super Lcom/google/android/gms/drive/metadata/a/ay;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/metadata/b;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/metadata/a/ay;-><init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 17
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot convert a collection metadata value directly to a database value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract b(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;
.end method
