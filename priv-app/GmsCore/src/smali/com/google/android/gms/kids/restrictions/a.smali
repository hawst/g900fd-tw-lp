.class public final Lcom/google/android/gms/kids/restrictions/a;
.super Lcom/google/android/gms/kids/common/sync/d;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/kids/restrictions/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/gms/kids/restrictions/a;

    invoke-direct {v0}, Lcom/google/android/gms/kids/restrictions/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/kids/restrictions/a;->a:Lcom/google/android/gms/kids/restrictions/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/kids/common/sync/d;-><init>()V

    return-void
.end method

.method private a(Lcom/google/s/a/g;Landroid/content/ContentProviderClient;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 98
    .line 99
    iget-object v4, p1, Lcom/google/s/a/g;->a:[Lcom/google/s/a/r;

    array-length v5, v4

    move v3, v1

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v2, v4, v3

    .line 100
    iget-object v6, v2, Lcom/google/s/a/r;->c:[Lcom/google/s/a/q;

    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    .line 101
    add-int/lit8 v2, v2, 0x1

    .line 102
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 103
    const-string v10, "key"

    iget-object v11, v8, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    invoke-static {v11}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 106
    const-string v10, "restriction"

    invoke-static {v8}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v8

    invoke-virtual {v9, v10, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/kids/restrictions/a;->d()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {p2, v8, v9}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 99
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_0

    .line 112
    :cond_1
    const-string v2, "RestrictionsSyncInfoProvider"

    const-string v3, "Updating %d groups with %d restrictions"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/s/a/g;->a:[Lcom/google/s/a/r;

    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    return-void
.end method

.method public static e()Lcom/google/android/gms/kids/restrictions/a;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/gms/kids/restrictions/a;->a:Lcom/google/android/gms/kids/restrictions/a;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.kids.restrictions.SYNCED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/ContentProviderClient;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 71
    const-string v0, "v"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    const-string v1, "RestrictionsSyncInfoProvider"

    const-string v3, "filter token : %s"

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    new-instance v4, Lcom/google/s/a/f;

    invoke-direct {v4}, Lcom/google/s/a/f;-><init>()V

    .line 76
    new-instance v1, Lcom/google/s/a/d;

    invoke-direct {v1}, Lcom/google/s/a/d;-><init>()V

    iput-object v1, v4, Lcom/google/s/a/f;->apiHeader:Lcom/google/s/a/d;

    .line 77
    const-string v1, "me"

    iput-object v1, v4, Lcom/google/s/a/f;->a:Ljava/lang/String;

    .line 78
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v4, Lcom/google/s/a/f;->d:Ljava/lang/Boolean;

    .line 79
    if-eqz v0, :cond_0

    .line 80
    iput-object v0, v4, Lcom/google/s/a/f;->c:Ljava/lang/String;

    .line 82
    :cond_0
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->a()Lcom/google/android/gms/kids/common/d;

    invoke-static {p2}, Lcom/google/android/gms/kids/common/d;->a(Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 83
    new-instance v0, Lcom/google/android/gms/kids/common/a/a;

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->a()Lcom/google/android/gms/kids/common/d;

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->e()Lcom/google/android/gms/kids/a/a;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/gms/kids/common/a/a;-><init>(Lcom/google/android/gms/common/server/q;)V

    .line 85
    iget-object v0, v0, Lcom/google/android/gms/kids/common/a/a;->a:Lcom/google/android/gms/common/server/q;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/people/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lcom/google/s/a/f;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/restrictions:listByGroups?alt=proto"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/s/a/g;

    invoke-direct {v5}, Lcom/google/s/a/g;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/s/a/g;

    .line 87
    invoke-direct {p0, v0, p4}, Lcom/google/android/gms/kids/restrictions/a;->a(Lcom/google/s/a/g;Landroid/content/ContentProviderClient;)V

    .line 88
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 119
    const-string v0, "rt"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string v0, "restrictions"

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/kids/restrictions/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/kids/restrictions/b;-><init>(Lcom/google/android/gms/kids/restrictions/a;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
