.class final Lcom/google/android/gms/games/ui/common/leaderboards/q;
.super Lcom/google/android/gms/games/ui/cx;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private k:Landroid/view/View;

.field private n:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/Spinner;

.field private q:Lcom/google/android/gms/games/ui/common/leaderboards/t;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 155
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cx;-><init>(Landroid/view/View;)V

    .line 156
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->kX:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->n:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->kY:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->o:Landroid/widget/TextView;

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->sO:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->p:Landroid/widget/Spinner;

    .line 161
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/t;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/t;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->q:Lcom/google/android/gms/games/ui/common/leaderboards/t;

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->p:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->q:Lcom/google/android/gms/games/ui/common/leaderboards/t;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->p:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->oS:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->r:Landroid/view/View;

    .line 166
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->sp:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->t:Landroid/widget/TextView;

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->so:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->u:Landroid/widget/TextView;

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->dc:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->s:Landroid/view/View;

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->s:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pZ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->v:Landroid/view/View;

    .line 187
    :goto_0
    return-void

    .line 179
    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->t:Landroid/widget/TextView;

    .line 180
    iput-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->u:Landroid/widget/TextView;

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pZ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 184
    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 185
    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->v:Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/ac;I)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 199
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/cx;->a(Lcom/google/android/gms/games/ui/ac;I)V

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/o;

    .line 206
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->k:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->a(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 208
    const/16 v1, 0xe

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 210
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->v:Landroid/view/View;

    check-cast v1, Landroid/widget/Switch;

    .line 211
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->b(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 221
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->n:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->c(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Landroid/net/Uri;

    move-result-object v2

    sget v6, Lcom/google/android/gms/h;->ar:I

    invoke-virtual {v1, v2, v6}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 224
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->o:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->d(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->e(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->f(I)I

    move-result v1

    .line 228
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->p:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 231
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->q:Lcom/google/android/gms/games/ui/common/leaderboards/t;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->e(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/common/leaderboards/t;->a(I)V

    .line 235
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->f(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/e/k;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/e/k;->h()J

    move-result-wide v6

    .line 236
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->f(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/e/k;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/e/k;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v8, -0x1

    cmp-long v1, v6, v8

    if-nez v1, :cond_4

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->r:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 268
    :goto_2
    return-void

    :cond_1
    move v2, v4

    .line 211
    goto :goto_0

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->v:Landroid/view/View;

    check-cast v1, Landroid/widget/CheckBox;

    .line 216
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->b(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v2

    if-ne v2, v3, :cond_3

    move v2, v3

    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_3
    move v2, v4

    goto :goto_3

    .line 243
    :cond_4
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->f(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/e/k;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/e/k;->k()J

    move-result-wide v8

    .line 244
    invoke-static {v6, v7}, Lcom/google/android/gms/games/ui/d/o;->a(J)Z

    move-result v2

    .line 245
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->r:Landroid/view/View;

    sget v10, Lcom/google/android/gms/j;->oR:I

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 247
    if-eqz v2, :cond_5

    .line 248
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->f(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/e/k;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/e/k;->i()Ljava/lang/String;

    move-result-object v2

    .line 249
    sget v6, Lcom/google/android/gms/p;->jA:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {v5, v6, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 258
    :goto_4
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->r:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 262
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->r:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->oN:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 264
    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;)V

    .line 265
    sget v2, Lcom/google/android/gms/g;->F:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 267
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->g(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-virtual {v1, v0, v4}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;Z)V

    goto :goto_2

    .line 254
    :cond_5
    const-wide/16 v10, 0x64

    mul-long/2addr v6, v10

    div-long/2addr v6, v8

    long-to-int v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 255
    sget v6, Lcom/google/android/gms/p;->jB:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {v5, v6, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 272
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/o;

    .line 276
    if-nez p2, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->a(Lcom/google/android/gms/games/ui/common/leaderboards/o;I)I

    .line 280
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->h(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/ui/common/leaderboards/p;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->b(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/p;->b(I)V

    .line 282
    :cond_0
    return-void

    .line 276
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const v6, 0x3ecccccd    # 0.4f

    const/16 v5, 0xe

    .line 306
    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 311
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 312
    sget v1, Lcom/google/android/gms/j;->dc:I

    if-ne v0, v1, :cond_0

    .line 313
    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->v:Landroid/view/View;

    check-cast v0, Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->v:Landroid/view/View;

    check-cast v0, Landroid/widget/Switch;

    if-nez v4, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/o;

    if-eqz v4, :cond_4

    :goto_2
    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->a(Lcom/google/android/gms/games/ui/common/leaderboards/o;I)I

    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->b(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setAlpha(F)V

    :cond_2
    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->h(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/ui/common/leaderboards/p;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->b(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/p;->b(I)V

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_3
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/o;

    .line 289
    invoke-static {p3}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->h(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->b(Lcom/google/android/gms/games/ui/common/leaderboards/o;I)I

    .line 292
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->h(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/ui/common/leaderboards/p;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->e(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/ui/common/leaderboards/p;->c(I)V

    .line 295
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/q;->q:Lcom/google/android/gms/games/ui/common/leaderboards/t;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->e(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/t;->a(I)V

    .line 296
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 301
    return-void
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    return v0
.end method
