.class public final Lcom/google/android/gms/icing/c/a/v;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lcom/google/android/gms/icing/c/a/u;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1623
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1624
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/icing/c/a/u;->a()[Lcom/google/android/gms/icing/c/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/v;->cachedSize:I

    .line 1625
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/c/a/v;
    .locals 1

    .prologue
    .line 1750
    new-instance v0, Lcom/google/android/gms/icing/c/a/v;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/v;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/c/a/v;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 1688
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1689
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1690
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1693
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 1694
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1695
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    aget-object v2, v2, v0

    .line 1696
    if-eqz v2, :cond_1

    .line 1697
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1694
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1702
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1637
    if-ne p1, p0, :cond_1

    .line 1638
    const/4 v0, 0x1

    .line 1655
    :cond_0
    :goto_0
    return v0

    .line 1640
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/v;

    if-eqz v1, :cond_0

    .line 1643
    check-cast p1, Lcom/google/android/gms/icing/c/a/v;

    .line 1644
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 1645
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1651
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1655
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/v;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1648
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1663
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1665
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/v;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1666
    return v0

    .line 1660
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1600
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/v;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/u;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/c/a/u;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/c/a/u;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1672
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1673
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1675
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1676
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1677
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;

    aget-object v1, v1, v0

    .line 1678
    if-eqz v1, :cond_1

    .line 1679
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1676
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1683
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1684
    return-void
.end method
