.class public Lcom/google/android/gms/social/location/LocationSharingAclCardView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/view/View;

.field private g:I

.field private h:Lcom/google/android/gms/social/location/af;

.field private i:Lcom/google/android/gms/social/location/q;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/social/location/af;Lcom/google/android/gms/social/location/q;)V
    .locals 3

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->h:Lcom/google/android/gms/social/location/af;

    .line 60
    iput-object p2, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->i:Lcom/google/android/gms/social/location/q;

    .line 62
    sget-object v0, Lcom/google/android/gms/social/location/p;->a:[I

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/af;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 79
    :goto_0
    return-void

    .line 64
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->c:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->qQ:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 65
    sget v0, Lcom/google/android/gms/p;->qP:I

    iput v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->g:I

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->d:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/h;->bV:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 72
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->c:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->qM:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 73
    sget v0, Lcom/google/android/gms/p;->qL:I

    iput v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->g:I

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->d:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/h;->bM:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 91
    if-eqz p2, :cond_1

    .line 92
    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->d:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->g:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 98
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->b:Landroid/widget/ImageView;

    if-eqz p2, :cond_0

    const/4 v0, 0x4

    :cond_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->e:Landroid/widget/TextView;

    if-eqz p2, :cond_2

    sget v0, Lcom/google/android/gms/p;->qK:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 102
    return-void

    .line 94
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->qJ:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 99
    :cond_2
    sget v0, Lcom/google/android/gms/p;->qO:I

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->i:Lcom/google/android/gms/social/location/q;

    if-nez v0, :cond_1

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->e:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->i:Lcom/google/android/gms/social/location/q;

    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->h:Lcom/google/android/gms/social/location/af;

    invoke-interface {v0, v1}, Lcom/google/android/gms/social/location/q;->a(Lcom/google/android/gms/social/location/af;)V

    goto :goto_0

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->b:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->i:Lcom/google/android/gms/social/location/q;

    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->h:Lcom/google/android/gms/social/location/af;

    invoke-interface {v0, v1}, Lcom/google/android/gms/social/location/q;->b(Lcom/google/android/gms/social/location/af;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->a:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 115
    sget v0, Lcom/google/android/gms/j;->jV:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->a:Landroid/widget/ImageView;

    .line 116
    sget v0, Lcom/google/android/gms/j;->kk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->b:Landroid/widget/ImageView;

    .line 117
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->c:Landroid/widget/TextView;

    .line 118
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->d:Landroid/widget/TextView;

    .line 119
    sget v0, Lcom/google/android/gms/j;->fi:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->e:Landroid/widget/TextView;

    .line 120
    sget v0, Lcom/google/android/gms/j;->ep:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->f:Landroid/view/View;

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 106
    iget-object v1, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->f:Landroid/view/View;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/social/location/LocationSharingAclCardView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 109
    return-void

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
