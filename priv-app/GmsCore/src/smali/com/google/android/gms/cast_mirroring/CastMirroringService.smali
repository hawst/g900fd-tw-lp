.class public Lcom/google/android/gms/cast_mirroring/CastMirroringService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Landroid/os/Handler;

.field private static b:Lcom/google/android/gms/cast/media/CastMirroringProvider;

.field private static c:Ljava/util/concurrent/CountDownLatch;

.field private static d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->c:Ljava/util/concurrent/CountDownLatch;

    .line 58
    const v0, 0x401640

    sput v0, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 108
    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 50
    sput p0, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->d:I

    return p0
.end method

.method static synthetic a()Lcom/google/android/gms/cast/media/CastMirroringProvider;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->b:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/media/CastMirroringProvider;)Lcom/google/android/gms/cast/media/CastMirroringProvider;
    .locals 0

    .prologue
    .line 50
    sput-object p0, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->b:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    return-object p0
.end method

.method static synthetic b()Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->c:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method static synthetic c()I
    .locals 1

    .prologue
    .line 50
    sget v0, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->d:I

    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 75
    const-string v0, "com.google.android.gms.cast_mirroring.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Lcom/google/android/gms/cast_mirroring/k;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/cast_mirroring/k;-><init>(Lcom/google/android/gms/cast_mirroring/CastMirroringService;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/cast_mirroring/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 63
    sput-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast_mirroring/h;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast_mirroring/h;-><init>(Lcom/google/android/gms/cast_mirroring/CastMirroringService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 71
    return-void
.end method
