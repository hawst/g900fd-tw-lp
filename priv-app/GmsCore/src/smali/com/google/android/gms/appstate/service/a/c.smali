.class public final Lcom/google/android/gms/appstate/service/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/appstate/service/c;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/appstate/c/h;

.field private final c:Ljava/lang/String;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/gms/appstate/service/a/c;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/appstate/service/a/c;->b:Lcom/google/android/gms/appstate/c/h;

    .line 32
    iput-object p3, p0, Lcom/google/android/gms/appstate/service/a/c;->c:Ljava/lang/String;

    .line 33
    iput p4, p0, Lcom/google/android/gms/appstate/service/a/c;->d:I

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/appstate/a/e;)V
    .locals 3

    .prologue
    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/c;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a/c;->c:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/appstate/service/a/c;->d:I

    invoke-virtual {p2, p1, v0, v1, v2}, Lcom/google/android/gms/appstate/a/e;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)I
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/appstate/d/b/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    .line 56
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a/c;->b:Lcom/google/android/gms/appstate/c/h;

    iget v2, p0, Lcom/google/android/gms/appstate/service/a/c;->d:I

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/appstate/c/h;->a(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3

    .line 60
    :goto_1
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    const-string v1, "DeleteStateOp"

    const-string v2, "Auth error while performing operation, requesting reconnect"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 43
    const/4 v0, 0x2

    .line 52
    goto :goto_0

    .line 44
    :catch_1
    move-exception v0

    .line 45
    const-string v1, "DeleteStateOp"

    invoke-virtual {v0}, Lcom/google/android/gms/appstate/d/b/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 46
    invoke-virtual {v0}, Lcom/google/android/gms/appstate/d/b/a;->a()I

    move-result v0

    goto :goto_0

    .line 47
    :catch_2
    move-exception v0

    .line 49
    const-string v1, "DeleteStateOp"

    const-string v2, "Runtime exception while performing operation"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 50
    const-string v1, "DeleteStateOp"

    const-string v2, "Killing (on development devices) due to RuntimeException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 51
    const/4 v0, 0x1

    goto :goto_0

    .line 60
    :catch_3
    move-exception v0

    goto :goto_1
.end method
