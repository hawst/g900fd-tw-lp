.class public abstract Lcom/google/android/gms/smart_profile/card/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;
.implements Lcom/google/android/gms/smart_profile/b/f;


# instance fields
.field a:Ljava/lang/Object;

.field b:Landroid/view/View;

.field public c:Lcom/google/android/gms/smart_profile/card/h;

.field d:Lcom/google/android/gms/smart_profile/bg;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 2

    .prologue
    .line 212
    new-instance v0, Lcom/google/android/gms/smart_profile/b/e;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/smart_profile/b/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/smart_profile/b/f;)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 147
    const-string v0, "loaderId"

    iget v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    return-void
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method public final a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 204
    return-void
.end method

.method public a(Landroid/support/v4/app/au;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V
    .locals 1

    .prologue
    .line 83
    if-eqz p2, :cond_0

    const-string v0, "loaderId"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    const-string v0, "loaderId"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->e:I

    .line 88
    :goto_0
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    .line 89
    return-void

    .line 86
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/smart_profile/card/a/c;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->e:I

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    .line 59
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    .line 60
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    return v0
.end method

.method public b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/google/android/gms/smart_profile/c;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1

    .prologue
    .line 137
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    return-object v0
.end method
