.class public abstract Lcom/google/android/gms/drive/metadata/a/an;
.super Lcom/google/android/gms/drive/metadata/a/ay;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "\'%s\'"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "PUBLIC"

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/an;->a:Ljava/lang/String;

    .line 36
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "\'%s\'"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "PRIVATE"

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/an;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->c:Lcom/google/android/gms/drive/metadata/internal/a/c;

    const/4 v1, 0x0

    const-string v2, "properties"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/ay;-><init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V

    .line 41
    return-void
.end method


# virtual methods
.method protected final synthetic a(Lorg/json/JSONObject;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 30
    new-instance v2, Lcom/google/android/gms/drive/metadata/internal/a;

    invoke-direct {v2}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    new-instance v4, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v5, "key"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "visibility"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    const-string v5, "value"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v4, v1}, Lcom/google/android/gms/drive/metadata/internal/a;->a(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/internal/a;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "value"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/internal/a;->a()Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot convert Custom Properties directly to a database value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 30
    check-cast p1, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    const-string v3, "value should have only 1 element"

    invoke-static {v1, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/g/ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "key"

    invoke-static {v4, v3}, Lcom/google/android/gms/drive/metadata/sync/b/c;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/g/ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "value"

    invoke-static {v4, v3}, Lcom/google/android/gms/drive/metadata/sync/b/c;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-wide v2, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/g/ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "app_id"

    invoke-static {v2, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "visibility"

    sget-object v2, Lcom/google/android/gms/drive/metadata/a/an;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected final synthetic a(Lorg/json/JSONObject;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 30
    check-cast p2, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "key"

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v4, "visibility"

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v4, "value"

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    sget-object v0, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    :goto_1
    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method protected final synthetic b(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 30
    check-cast p3, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    new-instance v1, Lcom/google/android/gms/drive/metadata/internal/a;

    invoke-direct {v1}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>()V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/drive/metadata/a/an;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->b()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p3}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Lcom/google/android/gms/drive/metadata/internal/a;->a(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/internal/a;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v1, v4, v0}, Lcom/google/android/gms/drive/metadata/internal/a;->a(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/internal/a;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/a;->a()Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v0

    return-object v0
.end method
