.class public final Lcom/google/android/gms/backup/ag;
.super Landroid/app/backup/BackupManager;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/backup/ag;->a:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method public final isBackupEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 27
    const/16 v1, 0x15

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 28
    invoke-super {p0}, Landroid/app/backup/BackupManager;->isBackupEnabled()Z

    move-result v0

    .line 30
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/backup/ag;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "backup_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
