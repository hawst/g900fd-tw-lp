.class public final Lcom/google/android/gms/smart_profile/ar;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([B)Lcom/google/ac/c/a/a/a/r;
    .locals 3

    .prologue
    .line 35
    :try_start_0
    invoke-static {p0}, Lcom/google/ac/c/a/a/a/r;->a([B)Lcom/google/ac/c/a/a/a/r;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    const-string v1, "SmartProfile"

    const-string v2, "Failed to parse GetSmartProfileResponse"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 39
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Ljava/util/List;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 77
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v0, "["

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 78
    if-eqz p0, :cond_5

    .line 79
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 81
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/g;

    .line 82
    iget-object v3, v0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    if-eqz v3, :cond_1

    .line 83
    const-string v0, "communicate"

    .line 93
    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 94
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_0

    .line 95
    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 79
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 84
    :cond_1
    iget-object v3, v0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    if-eqz v3, :cond_2

    .line 85
    const-string v0, "people"

    goto :goto_1

    .line 86
    :cond_2
    iget-object v3, v0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    if-eqz v3, :cond_3

    .line 87
    const-string v0, "photos"

    goto :goto_1

    .line 88
    :cond_3
    iget-object v0, v0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    if-eqz v0, :cond_4

    .line 89
    const-string v0, "interactions"

    goto :goto_1

    .line 91
    :cond_4
    const-string v0, "unknown"

    goto :goto_1

    .line 99
    :cond_5
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;
    .locals 3

    .prologue
    .line 107
    new-instance v1, Lcom/google/ac/c/a/a/a/f;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/f;-><init>()V

    .line 108
    new-instance v0, Lcom/google/ac/c/a/a/a/ai;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/ai;-><init>()V

    iput-object v0, v1, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    .line 109
    if-eqz p0, :cond_0

    .line 110
    iget-object v2, v1, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/ac/c/a/a/a/ag;

    invoke-interface {p0, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ac/c/a/a/a/ag;

    iput-object v0, v2, Lcom/google/ac/c/a/a/a/ai;->b:[Lcom/google/ac/c/a/a/a/ag;

    .line 113
    :cond_0
    return-object v1
.end method

.method public static b([B)Lcom/google/ac/c/a/a/a/h;
    .locals 3

    .prologue
    .line 47
    :try_start_0
    invoke-static {p0}, Lcom/google/ac/c/a/a/a/h;->a([B)Lcom/google/ac/c/a/a/a/h;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    const-string v1, "SmartProfile"

    const-string v2, "Failed to parse Cards"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c([B)Lcom/google/c/f/c/a/b/c;
    .locals 3

    .prologue
    .line 141
    :try_start_0
    invoke-static {p0}, Lcom/google/c/f/c/a/b/c;->a([B)Lcom/google/c/f/c/a/b/c;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 145
    :goto_0
    return-object v0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    const-string v1, "SmartProfile"

    const-string v2, "Failed to parse LookupPeopleForProfilesResponse"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 145
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/util/List;)Ljava/util/List;
    .locals 2

    .prologue
    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 132
    invoke-static {p0}, Lcom/google/android/gms/smart_profile/ar;->b(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    return-object v0
.end method
