.class public Lcom/google/android/gms/backup/SetBackupAccountActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private b:[Landroid/os/Parcelable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/accounts/AccountManagerCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "Backup"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "SetBackupAccountActivity"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->b:[Landroid/os/Parcelable;

    .line 50
    new-instance v0, Lcom/google/android/gms/backup/ap;

    invoke-direct {v0, p0}, Lcom/google/android/gms/backup/ap;-><init>(Lcom/google/android/gms/backup/SetBackupAccountActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->e:Landroid/accounts/AccountManagerCallback;

    .line 182
    return-void
.end method

.method static synthetic a()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method private a(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    .line 159
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/backup/BackupAccountManagerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v1, Lcom/google/android/gms/backup/ar;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/backup/ar;-><init>(Lcom/google/android/gms/backup/SetBackupAccountActivity;Landroid/accounts/Account;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 175
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/backup/SetBackupAccountActivity;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a(Landroid/accounts/Account;)V

    return-void
.end method

.method private b()Landroid/graphics/drawable/Drawable;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 117
    const/4 v1, 0x0

    .line 118
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/accounts/AccountManager;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    move-result-object v2

    array-length v3, v2

    move v7, v0

    move-object v0, v1

    move v1, v7

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 119
    const-string v5, "com.google"

    iget-object v6, v4, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 121
    :try_start_0
    iget-object v5, v4, Landroid/accounts/AuthenticatorDescription;->packageName:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v5

    .line 122
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget v4, v4, Landroid/accounts/AuthenticatorDescription;->iconId:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 118
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 128
    :cond_1
    return-object v0

    :catch_0
    move-exception v4

    goto :goto_1
.end method


# virtual methods
.method protected final a(Landroid/widget/ListView;I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 132
    invoke-virtual {p1}, Landroid/widget/ListView;->getCount()I

    move-result v0

    .line 133
    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_0

    .line 134
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    iget-object v6, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->e:Landroid/accounts/AccountManagerCallback;

    move-object v3, v2

    move-object v4, v2

    move-object v5, p0

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 140
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->b:[Landroid/os/Parcelable;

    aget-object v0, v0, p2

    check-cast v0, Landroid/accounts/Account;

    .line 137
    invoke-direct {p0, v0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a(Landroid/accounts/Account;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 79
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 83
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->b:[Landroid/os/Parcelable;

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->b:[Landroid/os/Parcelable;

    array-length v2, v0

    .line 86
    add-int/lit8 v0, v2, 0x1

    new-array v3, v0, [Ljava/lang/String;

    .line 87
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->b:[Landroid/os/Parcelable;

    aget-object v0, v0, v1

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v3, v1

    .line 87
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 90
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->pH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    .line 93
    sget v0, Lcom/google/android/gms/l;->fm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->setContentView(I)V

    .line 96
    invoke-direct {p0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->d:Landroid/graphics/drawable/Drawable;

    .line 97
    invoke-virtual {p0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->bS:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->c:Landroid/graphics/drawable/Drawable;

    .line 100
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 102
    new-instance v1, Lcom/google/android/gms/backup/as;

    iget-object v2, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->d:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/gms/backup/SetBackupAccountActivity;->c:Landroid/graphics/drawable/Drawable;

    invoke-direct {v1, p0, v3, v2, v4}, Lcom/google/android/gms/backup/as;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 106
    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 107
    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 108
    new-instance v1, Lcom/google/android/gms/backup/aq;

    invoke-direct {v1, p0}, Lcom/google/android/gms/backup/aq;-><init>(Lcom/google/android/gms/backup/SetBackupAccountActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 114
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 144
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 145
    return-void
.end method
