.class public final Lcom/google/android/gms/plus/audience/ar;
.super Lcom/google/android/gms/plus/audience/ao;
.source "SourceFile"


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View$OnClickListener;

.field private d:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/audience/ao;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 38
    sget v1, Lcom/google/android/gms/l;->dx:I

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 39
    sget v0, Lcom/google/android/gms/j;->aZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/ar;->b:Landroid/widget/TextView;

    .line 41
    sget v0, Lcom/google/android/gms/j;->bh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    sget v0, Lcom/google/android/gms/j;->aU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/ar;->a:Landroid/widget/CheckBox;

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/ar;->d:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/ar;->d:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ar;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 47
    return-void

    .line 46
    :cond_1
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/audience/ar;-><init>(Landroid/content/Context;)V

    .line 51
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->a()V

    .line 64
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/ar;->c:Landroid/view/View$OnClickListener;

    .line 112
    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/plus/audience/ap;)V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->a(Lcom/google/android/gms/plus/audience/ap;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 84
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 85
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 86
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 87
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ar;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/f;->ae:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v3, v6, v1, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 92
    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/ar;->d:Z

    if-eqz v1, :cond_1

    .line 93
    if-lez p2, :cond_0

    .line 94
    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 95
    const-string v1, "  "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 97
    :cond_0
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ar;->b:Landroid/widget/TextView;

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 107
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ar;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void

    .line 100
    :cond_1
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 101
    if-lez p2, :cond_2

    .line 102
    const-string v1, "  "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 103
    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 105
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ar;->b:Landroid/widget/TextView;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Z)V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->a(Z)V

    return-void
.end method

.method public final bridge synthetic b(Z)V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->b(Z)V

    return-void
.end method

.method public final bridge synthetic c(Z)V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->c(Z)V

    return-void
.end method

.method public final bridge synthetic d(Z)V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->d(Z)V

    return-void
.end method

.method public final bridge synthetic d()Z
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->d()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic e()V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->e()V

    return-void
.end method

.method public final bridge synthetic f()V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->f()V

    return-void
.end method

.method public final bridge synthetic isChecked()Z
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->isChecked()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/audience/ao;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->onClick(Landroid/view/View;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ar;->c:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ar;->c:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 59
    :cond_0
    return-void
.end method

.method public final bridge synthetic setChecked(Z)V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->setChecked(Z)V

    return-void
.end method

.method public final bridge synthetic toggle()V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->toggle()V

    return-void
.end method
