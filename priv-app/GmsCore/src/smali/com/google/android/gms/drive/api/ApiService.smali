.class public Lcom/google/android/gms/drive/api/ApiService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/List;


# instance fields
.field private b:Lcom/google/android/gms/drive/api/c;

.field private c:Lcom/google/android/gms/drive/api/h;

.field private d:Lcom/google/android/gms/drive/database/r;

.field private e:Lcom/google/android/gms/drive/events/w;

.field private f:Lcom/google/android/gms/drive/g/i;

.field private g:Lcom/google/android/gms/drive/g/i;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/api/ApiService;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/api/ApiService;->h:Z

    .line 215
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/api/ApiService;)Lcom/google/android/gms/drive/api/c;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/api/ApiService;->b:Lcom/google/android/gms/drive/api/c;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 185
    sget-object v1, Lcom/google/android/gms/drive/api/ApiService;->a:Ljava/util/List;

    monitor-enter v1

    .line 187
    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/api/ApiService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/api/ApiService;->d:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/ApiService;->stopSelf()V

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/api/ApiService;->h:Z

    .line 191
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/drive/api/ApiService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    const-string v1, "com.google.android.gms.drive.ApiService.STOP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 74
    return-void
.end method

.method private static a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 157
    sget-object v1, Lcom/google/android/gms/drive/api/ApiService;->a:Ljava/util/List;

    monitor-enter v1

    .line 158
    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/api/ApiService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 159
    invoke-virtual {v0, p0}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    monitor-exit v1

    .line 164
    :goto_0
    return-void

    .line 163
    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/api/ApiService;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/drive/api/ApiService;)Lcom/google/android/gms/drive/api/h;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/api/ApiService;->c:Lcom/google/android/gms/drive/api/h;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 133
    const-string v0, "com.google.android.gms.drive.ApiService.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-static {p1}, Lcom/google/android/gms/drive/api/ApiService;->a(Landroid/content/Intent;)V

    .line 135
    new-instance v0, Lcom/google/android/gms/drive/api/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/gms/drive/api/a;-><init>(Lcom/google/android/gms/drive/api/ApiService;Landroid/content/Context;B)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/api/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 78
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 79
    invoke-static {p0}, Lcom/google/android/gms/drive/u;->a(Landroid/content/Context;)V

    .line 81
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/google/android/gms/drive/api/c;

    invoke-direct {v1}, Lcom/google/android/gms/drive/api/c;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/drive/api/ApiService;->b:Lcom/google/android/gms/drive/api/c;

    new-instance v1, Lcom/google/android/gms/drive/api/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->s()Lcom/google/android/gms/drive/api/b;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->k()Lcom/google/android/gms/drive/api/k;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/api/h;-><init>(Lcom/google/android/gms/drive/api/b;Lcom/google/android/gms/drive/api/k;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/api/ApiService;->c:Lcom/google/android/gms/drive/api/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/drive/api/ApiService;->d:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->r()Lcom/google/android/gms/drive/events/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/ApiService;->e:Lcom/google/android/gms/drive/events/w;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->b()Lcom/google/android/gms/drive/g/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/ApiService;->f:Lcom/google/android/gms/drive/g/i;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->d()Lcom/google/android/gms/drive/g/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/ApiService;->g:Lcom/google/android/gms/drive/g/i;

    .line 83
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/gms/drive/ai;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/c/g;->a()Lcom/google/android/gms/drive/metadata/sync/c/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/c/g;->b()V

    .line 90
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 91
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 142
    const-string v0, "com.google.android.gms.drive.ApiService.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-static {p1}, Lcom/google/android/gms/drive/api/ApiService;->a(Landroid/content/Intent;)V

    .line 145
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/api/ApiService;->h:Z

    .line 115
    if-eqz p1, :cond_0

    .line 116
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 117
    const-string v1, "com.google.android.gms.drive.ApiService.STOP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 118
    invoke-direct {p0}, Lcom/google/android/gms/drive/api/ApiService;->a()V

    .line 128
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    .line 119
    :cond_1
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 120
    const-string v0, "ApiService"

    const-string v1, "Scheduling reset intent"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/drive/api/ApiService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.gms.drive.ApiService.RESET_AFTER_BOOT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/drive/ai;->R:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v2, v0

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/drive/api/ApiService;->a()V

    goto :goto_0

    .line 122
    :cond_2
    const-string v1, "com.google.android.gms.drive.ApiService.RESET_AFTER_BOOT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const-string v0, "ApiService"

    const-string v1, "Clearing unrefreshed subscriptions"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/drive/api/ApiService;->f:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/drive/api/ApiService;->g:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/gms/drive/api/ApiService;->e:Lcom/google/android/gms/drive/events/w;

    iget-object v2, v2, Lcom/google/android/gms/drive/events/w;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/drive/database/r;->d(J)V

    .line 124
    invoke-direct {p0}, Lcom/google/android/gms/drive/api/ApiService;->a()V

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    .line 149
    sget-object v1, Lcom/google/android/gms/drive/api/ApiService;->a:Ljava/util/List;

    monitor-enter v1

    .line 150
    :try_start_0
    sget-object v2, Lcom/google/android/gms/drive/api/ApiService;->a:Ljava/util/List;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v0, Lcom/google/android/gms/drive/api/ApiService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/drive/api/ApiService;->a()V

    .line 152
    monitor-exit v1

    .line 153
    const/4 v0, 0x1

    return v0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 152
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
