.class public final Lcom/google/android/gms/drive/api/a/n;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/api/k;

.field private final f:Lcom/google/android/gms/drive/internal/CreateFileRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/internal/CreateFileRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x4

    invoke-direct {p0, p1, p4, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 36
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/n;->e:Lcom/google/android/gms/drive/api/k;

    .line 37
    iput-object p3, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    const-string v2, "Invalid create request: no request"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->c()I

    move-result v3

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->d()I

    move-result v0

    if-nez v0, :cond_0

    .line 46
    if-eqz v3, :cond_3

    move v0, v1

    :goto_0
    const-string v2, "Invalid create request: invalid contents"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    const-string v2, "Invalid create request: no metadata"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v2, "Invalid create request: no parent"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->g()I

    move-result v5

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->f()Ljava/lang/String;

    move-result-object v7

    .line 57
    invoke-static {v5}, Lcom/google/android/gms/drive/ad;->b(I)Z

    move-result v0

    const-string v2, "Invalid commitStrategy."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 61
    if-eqz v5, :cond_1

    .line 63
    if-eqz v7, :cond_1

    .line 64
    invoke-static {v7}, Lcom/google/android/gms/drive/ad;->a(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Tracking tag is invalid"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->c:Lcom/google/android/gms/drive/c/a;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/drive/ad;

    iget-object v4, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->e()Z

    move-result v4

    invoke-direct {v2, v7, v4, v5}, Lcom/google/android/gms/drive/ad;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->d()I

    move-result v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/c/a;->d(I)Lcom/google/android/gms/drive/c/a;

    .line 75
    if-ne v3, v1, :cond_2

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->e:Lcom/google/android/gms/drive/api/k;

    const/high16 v1, 0x20000000

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/n;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v2}, Lcom/google/android/gms/drive/internal/ca;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v0, p2, v1, v2}, Lcom/google/android/gms/drive/api/k;->a(Lcom/google/android/gms/drive/auth/g;ILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->e()I

    move-result v3

    .line 82
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/n;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->d()I

    move-result v4

    iget-object v6, p0, Lcom/google/android/gms/drive/api/a/n;->f:Lcom/google/android/gms/drive/internal/CreateFileRequest;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/internal/CreateFileRequest;->e()Z

    move-result v6

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;IIIZLjava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/n;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnDriveIdResponse;)V

    .line 87
    return-void

    .line 46
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
