.class public final Lcom/google/android/gms/fitness/sensors/a/s;
.super Lcom/google/android/gms/fitness/sensors/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/sensors/a/m;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final a:Ljava/util/Set;


# instance fields
.field private final b:Ljava/util/List;

.field private final c:Lcom/google/android/gms/fitness/sensors/a/e;

.field private d:Lcom/google/android/gms/fitness/sensors/a/c;

.field private final e:Lcom/google/k/c/dn;

.field private final f:Lcom/google/k/k/a/aq;

.field private final g:Lcom/google/k/c/dn;

.field private final h:Ljava/util/Map;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/gms/fitness/c/a/a;->f:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/c/a/a;->b:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/fitness/c/a/a;->g:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/c/a/a;->c:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/fitness/c/a/a;->d:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/fitness/c/a/a;->i:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v5}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/util/UUID;

    invoke-static/range {v0 .. v6}, Lcom/google/k/c/cd;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/k/c/cd;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/sensors/a/s;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Landroid/bluetooth/BluetoothDevice;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/b/a;-><init>()V

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->h:Ljava/util/Map;

    .line 121
    new-instance v3, Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-direct {v3, p2}, Lcom/google/android/gms/fitness/sensors/a/ae;-><init>(Landroid/os/Handler;)V

    .line 122
    new-instance v0, Lcom/google/android/gms/fitness/sensors/a/e;

    move-object v1, p1

    move-object v2, p3

    move-object v4, p2

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/sensors/a/e;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/google/android/gms/fitness/sensors/a/ae;Landroid/os/Handler;Lcom/google/android/gms/fitness/sensors/a/m;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    .line 124
    iput-object p4, p0, Lcom/google/android/gms/fitness/sensors/a/s;->b:Ljava/util/List;

    .line 125
    invoke-static {}, Lcom/google/k/c/ba;->r()Lcom/google/k/c/ba;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/do;->a(Lcom/google/k/c/el;)Lcom/google/k/c/el;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    .line 127
    invoke-static {}, Lcom/google/k/c/ba;->r()Lcom/google/k/c/ba;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/do;->a(Lcom/google/k/c/el;)Lcom/google/k/c/el;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->g:Lcom/google/k/c/dn;

    .line 129
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->f:Lcom/google/k/k/a/aq;

    .line 130
    return-void
.end method

.method private a(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/Device;)Lcom/google/android/gms/fitness/data/DataSource;
    .locals 3

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->i:Ljava/lang/String;

    .line 485
    :goto_0
    new-instance v1, Lcom/google/android/gms/fitness/data/f;

    invoke-direct {v1}, Lcom/google/android/gms/fitness/data/f;-><init>()V

    iput-object p1, v1, Lcom/google/android/gms/fitness/data/f;->a:Lcom/google/android/gms/fitness/data/DataType;

    const/4 v2, 0x0

    iput v2, v1, Lcom/google/android/gms/fitness/data/f;->b:I

    iput-object p2, v1, Lcom/google/android/gms/fitness/data/f;->d:Lcom/google/android/gms/fitness/data/Device;

    iput-object v0, v1, Lcom/google/android/gms/fitness/data/f;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/util/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/f;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    return-object v0

    .line 481
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->c:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/sensors/a/s;Ljava/util/Collection;)Lcom/google/k/k/a/af;
    .locals 4

    .prologue
    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/fitness/sensors/a/e;->a(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/google/k/k/a/af;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lcom/google/k/k/a/n;->a(Ljava/lang/Iterable;)Lcom/google/k/k/a/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/y;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/fitness/sensors/a/y;-><init>(B)V

    invoke-static {v0, v1}, Lcom/google/k/k/a/n;->b(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/af;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/sensors/a/s;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/fitness/sensors/a/s;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->j:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/util/List;)V
    .locals 8

    .prologue
    .line 391
    invoke-static {}, Lcom/google/k/c/ba;->r()Lcom/google/k/c/ba;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/do;->a(Lcom/google/k/c/el;)Lcom/google/k/c/el;

    move-result-object v3

    .line 394
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 395
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/c/a/b;

    .line 396
    invoke-interface {v1}, Lcom/google/android/gms/fitness/c/a/b;->c()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 398
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v6

    .line 399
    invoke-interface {v1}, Lcom/google/android/gms/fitness/c/a/b;->c()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataType;

    .line 402
    iget-object v7, p0, Lcom/google/android/gms/fitness/sensors/a/s;->g:Lcom/google/k/c/dn;

    invoke-interface {v7, v6, v2}, Lcom/google/k/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 403
    new-instance v7, Lcom/google/android/gms/fitness/sensors/a/ad;

    invoke-direct {v7, v6, v2}, Lcom/google/android/gms/fitness/sensors/a/ad;-><init>(Ljava/util/UUID;Lcom/google/android/gms/fitness/data/DataType;)V

    .line 405
    iget-object v6, p0, Lcom/google/android/gms/fitness/sensors/a/s;->h:Ljava/util/Map;

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    invoke-interface {v3, v2, v0}, Lcom/google/k/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 409
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    invoke-interface {v1, v2}, Lcom/google/k/c/dn;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 410
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/sensors/a/e;->a(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/google/k/k/a/af;

    goto :goto_0

    .line 415
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->f:Lcom/google/k/k/a/aq;

    invoke-virtual {v0, v3}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Object;)Z

    .line 416
    return-void
.end method

.method private c()Lcom/google/android/gms/fitness/data/Device;
    .locals 5

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->k:Ljava/lang/String;

    .line 462
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->j:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->j:Ljava/lang/String;

    .line 465
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 466
    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/s;->m:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 467
    const-string v3, "f:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/a/s;->m:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 469
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/s;->l:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 470
    const-string v3, "h:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/a/s;->l:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 472
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/s;->n:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 473
    const-string v3, "s:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/s;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 476
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v2, v2, Lcom/google/android/gms/fitness/sensors/a/e;->c:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 477
    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/data/Device;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Device;

    move-result-object v0

    return-object v0

    .line 461
    :cond_3
    const-string v0, ""

    goto :goto_0

    .line 462
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->i:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->i:Ljava/lang/String;

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v1, v1, Lcom/google/android/gms/fitness/sensors/a/e;->c:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/gms/fitness/sensors/a/s;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->k:Ljava/lang/String;

    return-object v0
.end method

.method private c(Lcom/google/android/gms/fitness/data/DataType;)Z
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/c/a/b;

    .line 383
    invoke-interface {v0}, Lcom/google/android/gms/fitness/c/a/b;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    const/4 v0, 0x1

    .line 387
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/fitness/sensors/a/s;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/fitness/sensors/a/s;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/fitness/sensors/a/s;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/fitness/sensors/a/s;)Lcom/google/android/gms/fitness/sensors/a/e;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/data/BleDevice;)Lcom/google/k/k/a/af;
    .locals 3

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/n;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/fitness/sensors/a/n;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/sensors/a/e;->a(Lcom/google/android/gms/fitness/sensors/a/o;)V

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->e:Lcom/google/k/k/a/aq;

    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/t;

    invoke-direct {v1, p0}, Lcom/google/android/gms/fitness/sensors/a/t;-><init>(Lcom/google/android/gms/fitness/sensors/a/s;)V

    invoke-static {v0, v1}, Lcom/google/k/k/a/n;->b(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/u;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/fitness/sensors/a/u;-><init>(Lcom/google/android/gms/fitness/sensors/a/s;Lcom/google/android/gms/fitness/data/BleDevice;)V

    invoke-static {v0, v1}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/e;)Lcom/google/k/k/a/af;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 245
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/sensors/a/s;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    .line 258
    :goto_0
    return-object v0

    .line 248
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    .line 249
    const-string v1, "Registering for BLE device for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 250
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/sensors/a/s;->c(Lcom/google/android/gms/fitness/data/DataType;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/fitness/sensors/a/c;->a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;

    move-result-object v0

    goto :goto_0

    .line 254
    :cond_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    goto :goto_0

    .line 257
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/k/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 258
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->f:Lcom/google/k/k/a/aq;

    new-instance v2, Lcom/google/android/gms/fitness/sensors/a/w;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/fitness/sensors/a/w;-><init>(Lcom/google/android/gms/fitness/sensors/a/s;Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-static {v1, v2}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/e;)Lcom/google/k/k/a/af;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)Lcom/google/k/k/a/af;
    .locals 6

    .prologue
    .line 352
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 353
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 354
    const-string v3, "Characteristic discovered: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 355
    sget-object v3, Lcom/google/android/gms/fitness/sensors/a/s;->a:Ljava/util/Set;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 356
    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/fitness/sensors/a/e;->b(Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/google/k/k/a/af;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 360
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/sensors/a/s;->b(Ljava/util/List;)V

    .line 361
    invoke-static {v1}, Lcom/google/k/k/a/n;->a(Ljava/lang/Iterable;)Lcom/google/k/k/a/af;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/c;->a()V

    .line 201
    :cond_0
    return-void
.end method

.method public final a(Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 366
    const-string v0, "Characteristic data received: %s"

    new-array v1, v9, [Ljava/lang/Object;

    aput-object p1, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 367
    sget-object v0, Lcom/google/android/gms/fitness/sensors/a/s;->a:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/android/gms/fitness/c/a/a;->f:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v8}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->k:Ljava/lang/String;

    const-string v0, "received manufacturer: %s"

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/s;->k:Ljava/lang/String;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    sget-object v0, Lcom/google/android/gms/fitness/c/a/a;->b:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v8}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->i:Ljava/lang/String;

    const-string v0, "received deviceName: %s"

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/s;->i:Ljava/lang/String;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    sget-object v0, Lcom/google/android/gms/fitness/c/a/a;->g:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v8}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->j:Ljava/lang/String;

    const-string v0, "received modelNumber: %s"

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/s;->j:Ljava/lang/String;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_2
    sget-object v0, Lcom/google/android/gms/fitness/c/a/a;->c:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, v8}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->m:Ljava/lang/String;

    const-string v0, "received firmwareRevision: %s"

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/s;->m:Ljava/lang/String;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_3
    sget-object v0, Lcom/google/android/gms/fitness/c/a/a;->d:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1, v8}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->l:Ljava/lang/String;

    const-string v0, "received hardwareRevision: %s"

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/s;->l:Ljava/lang/String;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_4
    sget-object v0, Lcom/google/android/gms/fitness/c/a/a;->i:Lcom/google/android/gms/fitness/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/c/a/a;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1, v8}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->n:Ljava/lang/String;

    const-string v0, "received softwareRevision: %s"

    new-array v1, v9, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/s;->n:Ljava/lang/String;

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/a/s;->c()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/sensors/a/c;->a(Lcom/google/android/gms/fitness/data/Device;)V

    .line 368
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->g:Lcom/google/k/c/dn;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/k/c/dn;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->g:Lcom/google/k/c/dn;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/s;->g:Lcom/google/k/c/dn;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/k/c/dn;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "Notifying BLE listeners for %s"

    new-array v2, v9, [Ljava/lang/Object;

    aput-object v0, v2, v8

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/ad;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-direct {v1, v3, v0}, Lcom/google/android/gms/fitness/sensors/a/ad;-><init>(Ljava/util/UUID;Lcom/google/android/gms/fitness/data/DataType;)V

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    invoke-interface {v3, v0}, Lcom/google/k/c/dn;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    monitor-enter v3

    :try_start_1
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    invoke-interface {v5, v0}, Lcom/google/k/c/dn;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/a/s;->h:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/c/a/b;

    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/a/s;->c()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/fitness/sensors/a/s;->a(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/Device;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-interface {v1, v0, p1}, Lcom/google/android/gms/fitness/c/a/b;->a(Lcom/google/android/gms/fitness/data/DataSource;Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/l;

    :try_start_2
    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/data/l;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v4, "Exception while notifying listeners"

    const-string v5, "Bluetooth device: %s : %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v7, v7, Lcom/google/android/gms/fitness/sensors/a/e;->c:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    aput-object v4, v6, v9

    invoke-static {v0, v5, v6}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    .line 370
    :cond_8
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/internal/ble/b;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p1, Lcom/google/android/gms/fitness/internal/ble/b;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->i:Ljava/lang/String;

    .line 189
    iget-object v0, p1, Lcom/google/android/gms/fitness/internal/ble/b;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->j:Ljava/lang/String;

    .line 190
    iget-object v0, p1, Lcom/google/android/gms/fitness/internal/ble/b;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->k:Ljava/lang/String;

    .line 191
    iget-object v0, p1, Lcom/google/android/gms/fitness/internal/ble/b;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->l:Ljava/lang/String;

    .line 192
    iget-object v0, p1, Lcom/google/android/gms/fitness/internal/ble/b;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->m:Ljava/lang/String;

    .line 193
    iget-object v0, p1, Lcom/google/android/gms/fitness/internal/ble/b;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->n:Ljava/lang/String;

    .line 194
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/sensors/a/c;)V
    .locals 0

    .prologue
    .line 530
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    .line 531
    return-void
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/fitness/sensors/a/c;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 344
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    const-string v0, "  BleDeviceConnectionManager["

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    iget-object v2, v1, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    const-string v0, "    GattRequestPermit["

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "      requestPermitQueueLength="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/gms/fitness/sensors/a/ae;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->getQueueLength()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "      config[bleRequestTimeoutSecs="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->ah:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v3, "]\n"

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "      mRequestPermitHolder="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v2, v2, Lcom/google/android/gms/fitness/sensors/a/ae;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "    ]\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "    BluetoothDevice="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/fitness/sensors/a/e;->c:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "    GattState="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget v2, v1, Lcom/google/android/gms/fitness/sensors/a/e;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "    ServicesFuture[isDone="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/fitness/sensors/a/e;->e:Lcom/google/k/k/a/aq;

    invoke-virtual {v2}, Lcom/google/k/k/a/aq;->isDone()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "]\n"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "    Config[bleAutoConnect="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v2

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->am:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "]\n"

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "    EnqueuedGattCommands=["

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    iget-object v0, v1, Lcom/google/android/gms/fitness/sensors/a/e;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/a/o;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    goto :goto_0

    :cond_1
    const-string v0, "    ]\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "     CloseRequested="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/gms/fitness/sensors/a/e;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v0, "  ]\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 345
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;)Z
    .locals 2

    .prologue
    .line 218
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v1, v1, Lcom/google/android/gms/fitness/sensors/a/e;->c:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Device;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataType;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 205
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/sensors/a/s;->c(Lcom/google/android/gms/fitness/data/DataType;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 213
    :goto_0
    return v0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/c/a/b;

    .line 209
    invoke-interface {v0}, Lcom/google/android/gms/fitness/c/a/b;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 210
    goto :goto_0

    .line 213
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/l;)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 283
    const-string v0, "unregister request received"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/fitness/sensors/a/c;->a(Lcom/google/android/gms/fitness/data/l;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v3

    .line 287
    :goto_0
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    invoke-interface {v0}, Lcom/google/k/c/dn;->h()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 289
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/data/l;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 290
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move v2, v4

    .line 285
    goto :goto_0

    .line 293
    :cond_2
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    .line 294
    iget-object v6, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    invoke-interface {v6, v0, p1}, Lcom/google/k/c/dn;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 296
    iget-object v6, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    monitor-enter v6

    .line 297
    :try_start_0
    iget-object v7, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    invoke-interface {v7, v0}, Lcom/google/k/c/dn;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    .line 298
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    if-eqz v7, :cond_3

    .line 300
    iget-object v6, p0, Lcom/google/android/gms/fitness/sensors/a/s;->f:Lcom/google/k/k/a/aq;

    new-instance v7, Lcom/google/android/gms/fitness/sensors/a/x;

    invoke-direct {v7, p0, v0}, Lcom/google/android/gms/fitness/sensors/a/x;-><init>(Lcom/google/android/gms/fitness/sensors/a/s;Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-static {v6, v7}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    goto :goto_2

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 323
    :cond_4
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz v2, :cond_6

    :cond_5
    move v4, v3

    :cond_6
    return v4
.end method

.method public final b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;
    .locals 5

    .prologue
    .line 225
    invoke-virtual {p0, p1}, Lcom/google/android/gms/fitness/sensors/a/s;->a(Lcom/google/android/gms/fitness/data/DataType;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    .line 228
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/sensors/a/s;->c(Lcom/google/android/gms/fitness/data/DataType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/fitness/sensors/a/c;->b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 232
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 235
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/a/s;->c()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v0

    .line 236
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/a/s;->a(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/Device;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 238
    const-string v1, "returning %d BLE data sources for %s: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->d:Lcom/google/android/gms/fitness/sensors/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/c;->b()V

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    invoke-interface {v0}, Lcom/google/k/c/dn;->f()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/l;

    .line 334
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/sensors/a/s;->a(Lcom/google/android/gms/fitness/data/l;)Z

    goto :goto_0

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->e:Lcom/google/k/c/dn;

    invoke-interface {v0}, Lcom/google/k/c/dn;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Disconnecting"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/s;->c:Lcom/google/android/gms/fitness/sensors/a/e;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/e;->a()Lcom/google/k/k/a/af;

    .line 337
    :goto_1
    return-void

    .line 336
    :cond_2
    const-string v0, "Not Disconnecting - still listening"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
