.class public Lcom/google/android/gms/ads/internal/p/c;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field protected final a:Lcom/google/android/gms/ads/internal/p/a;

.field private final b:Ljava/util/HashMap;

.field private final c:Ljava/lang/Object;

.field private d:Lcom/google/android/gms/ads/internal/a;

.field private e:Lcom/google/android/gms/ads/internal/overlay/i;

.field private f:Lcom/google/android/gms/ads/internal/p/e;

.field private g:Lcom/google/android/gms/ads/internal/g/b;

.field private h:Z

.field private i:Lcom/google/android/gms/ads/internal/g/n;

.field private j:Lcom/google/android/gms/ads/internal/g/p;

.field private k:Z

.field private l:Lcom/google/android/gms/ads/internal/overlay/n;

.field private final m:Lcom/google/android/gms/ads/internal/l/j;

.field private n:Lcom/google/android/gms/ads/internal/i;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/p/a;Z)V
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/gms/ads/internal/l/j;

    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/ads/internal/c/a;

    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/ads/internal/c/a;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/ads/internal/l/j;-><init>(Lcom/google/android/gms/ads/internal/p/a;Landroid/content/Context;Lcom/google/android/gms/ads/internal/c/a;)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/ads/internal/p/c;-><init>(Lcom/google/android/gms/ads/internal/p/a;ZLcom/google/android/gms/ads/internal/l/j;)V

    .line 90
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/ads/internal/p/a;ZLcom/google/android/gms/ads/internal/l/j;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->b:Ljava/util/HashMap;

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->c:Ljava/lang/Object;

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/c;->h:Z

    .line 94
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    .line 95
    iput-boolean p2, p0, Lcom/google/android/gms/ads/internal/p/c;->k:Z

    .line 96
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/p/c;->m:Lcom/google/android/gms/ads/internal/l/j;

    .line 97
    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 425
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 427
    if-eqz v0, :cond_1

    .line 429
    invoke-static {p1}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v2

    .line 432
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/client/b;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 433
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received GMSG: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 434
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 435
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 438
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/g/m;

    .line 440
    iget-object v3, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-interface {v0, v3, v2}, Lcom/google/android/gms/ads/internal/g/m;->a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V

    goto :goto_1

    .line 443
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No GMSG handler found for GMSG: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 445
    :cond_2
    return-void
.end method

.method private a(Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;)V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/ads/internal/overlay/c;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;)V

    .line 277
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/ads/internal/i;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->n:Lcom/google/android/gms/ads/internal/i;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;ZLcom/google/android/gms/ads/internal/g/n;Lcom/google/android/gms/ads/internal/g/p;Lcom/google/android/gms/ads/internal/i;)V
    .locals 8

    .prologue
    .line 160
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;ZLcom/google/android/gms/ads/internal/g/n;Lcom/google/android/gms/ads/internal/i;)V

    .line 167
    const-string v0, "/setInterstitialProperties"

    new-instance v1, Lcom/google/android/gms/ads/internal/g/o;

    invoke-direct {v1, p6}, Lcom/google/android/gms/ads/internal/g/o;-><init>(Lcom/google/android/gms/ads/internal/g/p;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 169
    iput-object p6, p0, Lcom/google/android/gms/ads/internal/p/c;->j:Lcom/google/android/gms/ads/internal/g/p;

    .line 170
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;ZLcom/google/android/gms/ads/internal/g/n;Lcom/google/android/gms/ads/internal/i;)V
    .locals 2

    .prologue
    .line 110
    if-nez p7, :cond_0

    .line 111
    new-instance p7, Lcom/google/android/gms/ads/internal/i;

    const/4 v0, 0x0

    invoke-direct {p7, v0}, Lcom/google/android/gms/ads/internal/i;-><init>(B)V

    .line 114
    :cond_0
    const-string v0, "/appEvent"

    new-instance v1, Lcom/google/android/gms/ads/internal/g/a;

    invoke-direct {v1, p3}, Lcom/google/android/gms/ads/internal/g/a;-><init>(Lcom/google/android/gms/ads/internal/g/b;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 116
    const-string v0, "/canOpenURLs"

    sget-object v1, Lcom/google/android/gms/ads/internal/g/c;->b:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 118
    const-string v0, "/canOpenIntents"

    sget-object v1, Lcom/google/android/gms/ads/internal/g/c;->c:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 121
    const-string v0, "/click"

    sget-object v1, Lcom/google/android/gms/ads/internal/g/c;->d:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 122
    const-string v0, "/close"

    sget-object v1, Lcom/google/android/gms/ads/internal/g/c;->e:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 123
    const-string v0, "/customClose"

    sget-object v1, Lcom/google/android/gms/ads/internal/g/c;->f:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 125
    const-string v0, "/httpTrack"

    sget-object v1, Lcom/google/android/gms/ads/internal/g/c;->g:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 127
    const-string v0, "/log"

    sget-object v1, Lcom/google/android/gms/ads/internal/g/c;->h:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 128
    const-string v0, "/open"

    new-instance v1, Lcom/google/android/gms/ads/internal/g/r;

    invoke-direct {v1, p6, p7}, Lcom/google/android/gms/ads/internal/g/r;-><init>(Lcom/google/android/gms/ads/internal/g/n;Lcom/google/android/gms/ads/internal/i;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 131
    const-string v0, "/touch"

    sget-object v1, Lcom/google/android/gms/ads/internal/g/c;->i:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 132
    const-string v0, "/video"

    sget-object v1, Lcom/google/android/gms/ads/internal/g/c;->j:Lcom/google/android/gms/ads/internal/g/m;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 133
    const-string v0, "/mraid"

    new-instance v1, Lcom/google/android/gms/ads/internal/g/q;

    invoke-direct {v1}, Lcom/google/android/gms/ads/internal/g/q;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 137
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p/c;->d:Lcom/google/android/gms/ads/internal/a;

    .line 138
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/p/c;->e:Lcom/google/android/gms/ads/internal/overlay/i;

    .line 139
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/p/c;->g:Lcom/google/android/gms/ads/internal/g/b;

    .line 140
    iput-object p6, p0, Lcom/google/android/gms/ads/internal/p/c;->i:Lcom/google/android/gms/ads/internal/g/n;

    .line 141
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/p/c;->l:Lcom/google/android/gms/ads/internal/overlay/n;

    .line 142
    iput-object p7, p0, Lcom/google/android/gms/ads/internal/p/c;->n:Lcom/google/android/gms/ads/internal/i;

    .line 145
    iput-boolean p5, p0, Lcom/google/android/gms/ads/internal/p/c;->h:Z

    .line 146
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/overlay/AdLauncherIntentInfoParcel;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->j()Z

    move-result v1

    .line 208
    new-instance v0, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/p/a;->e()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v2, :cond_0

    move-object v2, v3

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/ads/internal/p/c;->l:Lcom/google/android/gms/ads/internal/overlay/n;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->i()Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;-><init>(Lcom/google/android/gms/ads/internal/overlay/AdLauncherIntentInfoParcel;Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/overlay/n;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;)V

    .line 214
    return-void

    .line 208
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/c;->d:Lcom/google/android/gms/ads/internal/a;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/ads/internal/p/c;->e:Lcom/google/android/gms/ads/internal/overlay/i;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/ads/internal/p/e;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p/c;->f:Lcom/google/android/gms/ads/internal/p/e;

    .line 329
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 285
    if-nez v0, :cond_0

    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 287
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 392
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/c;->h:Z

    .line 393
    return-void
.end method

.method public final a(ZI)V
    .locals 8

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->j()Z

    move-result v1

    .line 222
    new-instance v0, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->e()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/c;->e:Lcom/google/android/gms/ads/internal/overlay/i;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/p/c;->l:Lcom/google/android/gms/ads/internal/overlay/n;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v5}, Lcom/google/android/gms/ads/internal/p/a;->i()Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-result-object v7

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;-><init>(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/overlay/n;Lcom/google/android/gms/ads/internal/p/a;ZILcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;)V

    .line 230
    return-void

    .line 222
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->d:Lcom/google/android/gms/ads/internal/a;

    goto :goto_0
.end method

.method public final a(ZILjava/lang/String;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->j()Z

    move-result v3

    .line 239
    new-instance v0, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->e()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v1, :cond_0

    move-object v1, v2

    :goto_0
    if-eqz v3, :cond_1

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/ads/internal/p/c;->g:Lcom/google/android/gms/ads/internal/g/b;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/p/c;->l:Lcom/google/android/gms/ads/internal/overlay/n;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v6, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v6}, Lcom/google/android/gms/ads/internal/p/a;->i()Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/gms/ads/internal/p/c;->i:Lcom/google/android/gms/ads/internal/g/n;

    move v6, p1

    move v7, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;-><init>(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;Lcom/google/android/gms/ads/internal/p/a;ZILjava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/ads/internal/g/n;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;)V

    .line 250
    return-void

    .line 239
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->d:Lcom/google/android/gms/ads/internal/a;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/c;->e:Lcom/google/android/gms/ads/internal/overlay/i;

    goto :goto_1
.end method

.method public final a(ZILjava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->j()Z

    move-result v2

    .line 260
    new-instance v0, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->e()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-eqz v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/ads/internal/p/c;->g:Lcom/google/android/gms/ads/internal/g/b;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/p/c;->l:Lcom/google/android/gms/ads/internal/overlay/n;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v6, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v6}, Lcom/google/android/gms/ads/internal/p/a;->i()Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/gms/ads/internal/p/c;->i:Lcom/google/android/gms/ads/internal/g/n;

    move v6, p1

    move v7, p2

    move-object v8, p3

    move-object/from16 v9, p4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;-><init>(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;Lcom/google/android/gms/ads/internal/p/a;ZILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/ads/internal/g/n;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel;)V

    .line 272
    return-void

    .line 260
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->d:Lcom/google/android/gms/ads/internal/a;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/c;->e:Lcom/google/android/gms/ads/internal/overlay/i;

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 299
    if-nez v0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 302
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 181
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 182
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/c;->k:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 196
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->m:Lcom/google/android/gms/ads/internal/l/j;

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "width"

    iget v3, v1, Lcom/google/android/gms/ads/internal/l/j;->e:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "height"

    iget v3, v1, Lcom/google/android/gms/ads/internal/l/j;->f:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "density"

    iget v3, v1, Lcom/google/android/gms/ads/internal/l/j;->d:F

    float-to-double v4, v3

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "rotation"

    iget v3, v1, Lcom/google/android/gms/ads/internal/l/j;->g:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/l/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    const-string v3, "onScreenInfoChanged"

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/ads/internal/p/a;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/google/android/gms/ads/internal/l/i;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/l/i;-><init>()V

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/l/j;->b:Lcom/google/android/gms/ads/internal/c/a;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.DIAL"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "tel:"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/internal/c/a;->a(Landroid/content/Intent;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/gms/ads/internal/l/i;->b:Z

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/l/j;->b:Lcom/google/android/gms/ads/internal/c/a;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "sms:"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/internal/c/a;->a(Landroid/content/Intent;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/gms/ads/internal/l/i;->a:Z

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/l/j;->b:Lcom/google/android/gms/ads/internal/c/a;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/c/a;->b()Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/gms/ads/internal/l/i;->c:Z

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/l/j;->b:Lcom/google/android/gms/ads/internal/c/a;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/c/a;->a()Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/gms/ads/internal/l/i;->d:Z

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/l/j;->b:Lcom/google/android/gms/ads/internal/c/a;

    iput-boolean v6, v0, Lcom/google/android/gms/ads/internal/l/i;->e:Z

    new-instance v2, Lcom/google/android/gms/ads/internal/l/h;

    invoke-direct {v2, v0, v6}, Lcom/google/android/gms/ads/internal/l/h;-><init>(Lcom/google/android/gms/ads/internal/l/i;B)V

    iget-object v0, v1, Lcom/google/android/gms/ads/internal/l/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    const-string v3, "onDeviceFeaturesReceived"

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/l/h;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/ads/internal/p/a;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "x"

    iget-object v3, v1, Lcom/google/android/gms/ads/internal/l/j;->j:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "y"

    iget-object v3, v1, Lcom/google/android/gms/ads/internal/l/j;->j:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "width"

    iget v3, v1, Lcom/google/android/gms/ads/internal/l/j;->h:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "height"

    iget v3, v1, Lcom/google/android/gms/ads/internal/l/j;->i:I

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/l/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    const-string v3, "onDefaultPositionReceived"

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/ads/internal/p/a;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Dispatching Ready Event."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    :cond_0
    iget-object v0, v1, Lcom/google/android/gms/ads/internal/l/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    const-string v1, "onReadyEventReceived"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/internal/p/a;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 199
    :cond_1
    return-void

    .line 197
    :catch_0
    move-exception v0

    const-string v2, "Error occured while obtaining screen information."

    invoke-static {v2, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v2, "Error occured while dispatching default position."

    invoke-static {v2, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 309
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 310
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 311
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->d:Lcom/google/android/gms/ads/internal/a;

    .line 312
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->e:Lcom/google/android/gms/ads/internal/overlay/i;

    .line 313
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->f:Lcom/google/android/gms/ads/internal/p/e;

    .line 314
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->g:Lcom/google/android/gms/ads/internal/g/b;

    .line 315
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/c;->h:Z

    .line 316
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/c;->k:Z

    .line 317
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->i:Lcom/google/android/gms/ads/internal/g/n;

    .line 318
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->l:Lcom/google/android/gms/ads/internal/overlay/n;

    .line 319
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 399
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 400
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/c;->h:Z

    .line 401
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/c;->k:Z

    .line 404
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->d()Lcom/google/android/gms/ads/internal/overlay/c;

    move-result-object v0

    .line 405
    if-eqz v0, :cond_0

    .line 406
    invoke-static {}, Lcom/google/android/gms/ads/internal/util/client/a;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 408
    sget-object v2, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/ads/internal/p/d;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/ads/internal/p/d;-><init>(Lcom/google/android/gms/ads/internal/p/c;Lcom/google/android/gms/ads/internal/overlay/c;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 418
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 415
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/overlay/c;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 418
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 333
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loading resource: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 337
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 338
    const-string v1, "gmsg"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "mobileads.google.com"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 340
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/p/c;->a(Landroid/net/Uri;)V

    .line 342
    :cond_0
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->f:Lcom/google/android/gms/ads/internal/p/e;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->f:Lcom/google/android/gms/ads/internal/p/e;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/p/e;->a(Lcom/google/android/gms/ads/internal/p/a;)V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->f:Lcom/google/android/gms/ads/internal/p/e;

    .line 192
    :cond_0
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AdWebView shouldOverrideUrlLoading: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 349
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 352
    const-string v1, "gmsg"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "mobileads.google.com"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 354
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/p/c;->a(Landroid/net/Uri;)V

    :goto_0
    move v0, v8

    .line 385
    :goto_1
    return v0

    .line 355
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/ads/internal/p/c;->h:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    if-ne p1, v1, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "https"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move v1, v8

    :goto_2
    if-eqz v1, :cond_3

    .line 357
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 355
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 358
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->willNotDraw()Z

    move-result v1

    if-nez v1, :cond_7

    .line 361
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->h()Lcom/google/android/a/w;

    move-result-object v1

    .line 363
    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Lcom/google/android/a/w;->b(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 364
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/c;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/a/w;->a(Landroid/net/Uri;Landroid/content/Context;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/google/android/a/x; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_4
    move-object v2, v0

    .line 369
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->n:Lcom/google/android/gms/ads/internal/i;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->n:Lcom/google/android/gms/ads/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/i;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 370
    :cond_5
    new-instance v0, Lcom/google/android/gms/ads/internal/overlay/AdLauncherIntentInfoParcel;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/ads/internal/overlay/AdLauncherIntentInfoParcel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/overlay/AdLauncherIntentInfoParcel;)V

    goto :goto_0

    .line 367
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to append parameter to URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    move-object v2, v0

    goto :goto_3

    .line 379
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/c;->n:Lcom/google/android/gms/ads/internal/i;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/ads/internal/i;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 382
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AdWebView unable to handle URL: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
