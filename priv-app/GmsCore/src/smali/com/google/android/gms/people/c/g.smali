.class final Lcom/google/android/gms/people/c/g;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/c/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/c/f;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 789
    iput-object p1, p0, Lcom/google/android/gms/people/c/g;->a:Lcom/google/android/gms/people/c/f;

    .line 790
    const/4 v0, 0x0

    const/16 v1, 0x2bc

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 791
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1348
    if-nez p2, :cond_0

    .line 1349
    const-string v0, "DELETE FROM sqlite_stat1 WHERE tbl=? AND idx IS NULL"

    new-array v1, v3, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1355
    :goto_0
    const-string v0, "INSERT INTO sqlite_stat1 (tbl,idx,stat) VALUES (?,?,?)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    aput-object p3, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1357
    return-void

    .line 1352
    :cond_0
    const-string v0, "DELETE FROM sqlite_stat1 WHERE tbl=? AND idx=?"

    new-array v1, v4, [Ljava/lang/String;

    aput-object p1, v1, v2

    aput-object p2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 1228
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1230
    :try_start_0
    const-string v0, "DELETE FROM sqlite_stat1"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1261
    const-string v0, "people"

    const-string v1, "sqlite_autoindex_people_1"

    const-string v2, "500 250 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    const-string v0, "owners"

    const/4 v1, 0x0

    const-string v2, "3"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1265
    const-string v0, "owner_sync_requests"

    const-string v1, "sqlite_autoindex_owner_sync_requests_1"

    const-string v2, "3 1 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1267
    const-string v0, "sync_tokens"

    const-string v1, "sqlite_autoindex_sync_tokens_1"

    const-string v2, "15"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1269
    const-string v0, "circle_members"

    const-string v1, "sqlite_autoindex_circle_members_1"

    const-string v2, "1000 300 2 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    const-string v0, "circles"

    const-string v1, "sqlite_autoindex_circles_1"

    const-string v2, "20 10 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1277
    const-string v0, "search_index"

    const-string v1, "search_value"

    const-string v2, "1500 3"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1280
    const-string v0, "search_index"

    const-string v1, "search_person_id_index"

    const-string v2, "1500 3"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1283
    const-string v0, "emails"

    const-string v1, "email_person"

    const-string v2, "500 150 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    const-string v0, "phones"

    const-string v1, "phone_person"

    const-string v2, "500 150 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    const-string v0, "postal_address"

    const-string v1, "postal_address_person"

    const-string v2, "500 150 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    const-string v0, "owner_emails"

    const/4 v1, 0x0

    const-string v2, "6"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1294
    const-string v0, "owner_phones"

    const/4 v1, 0x0

    const-string v2, "6"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    const-string v0, "owner_postal_address"

    const/4 v1, 0x0

    const-string v2, "6"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1297
    const-string v0, "properties"

    const/4 v1, 0x0

    const-string v2, "10"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    const-string v0, "gaia_id_map"

    const-string v1, "sqlite_autoindex_gaia_id_map_1"

    const-string v2, "500 200 2 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304
    const-string v0, "ac_people"

    const-string v1, "sqlite_autoindex_ac_people_1"

    const-string v2, "500 250 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1306
    const-string v0, "ac_people"

    const-string v1, "ac_people_v2_id"

    const-string v2, "500 250 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1308
    const-string v0, "ac_container"

    const-string v1, "ac_item_container_person_id"

    const-string v2, "1000 2"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1310
    const-string v0, "ac_item"

    const-string v1, "ac_item_container"

    const-string v2, "2000 2"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    const-string v0, "ac_index"

    const-string v1, "ac_index_1"

    const-string v2, "8000 2000 2 2"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    const-string v0, "ac_index"

    const-string v1, "ac_index_item_id"

    const-string v2, "8000 4"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1318
    const-string v0, "applications"

    const-string v1, "sqlite_autoindex_applications_1"

    const-string v2, "1 1 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1320
    const-string v0, "application_packages"

    const/4 v1, 0x0

    const-string v2, "1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1322
    const-string v0, "facl_people"

    const-string v1, "sqlite_autoindex_facl_people_1"

    const-string v2, "50 50 50 1"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1326
    const-string v0, "temp_gaia_ordinal"

    const/4 v1, 0x0

    const-string v2, "3"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    const-string v0, "ANALYZE sqlite_master;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1330
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1335
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1336
    :goto_0
    return-void

    .line 1331
    :catch_0
    move-exception v0

    .line 1333
    :try_start_1
    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "Could not update index stats"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1335
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 1361
    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "Wiping the database..."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1369
    const-string v0, "DROP TABLE IF EXISTS temp_gaia_ordinal;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1370
    const-string v0, "DROP TABLE IF EXISTS ac_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1371
    const-string v0, "DROP TABLE IF EXISTS ac_item;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1372
    const-string v0, "DROP TABLE IF EXISTS ac_container;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1373
    const-string v0, "DROP TABLE IF EXISTS ac_people;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1374
    const-string v0, "DROP TABLE IF EXISTS owner_emails;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1375
    const-string v0, "DROP TABLE IF EXISTS owner_phones;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1376
    const-string v0, "DROP TABLE IF EXISTS owner_postal_address;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1377
    const-string v0, "DROP TABLE IF EXISTS owner_sync_requests;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1378
    const-string v0, "DROP TABLE IF EXISTS applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1379
    const-string v0, "DROP TABLE IF EXISTS application_packages;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1380
    const-string v0, "DROP TABLE IF EXISTS facl_application;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1381
    const-string v0, "DROP TABLE IF EXISTS facl_applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1382
    const-string v0, "DROP TABLE IF EXISTS facl_circles;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1383
    const-string v0, "DROP TABLE IF EXISTS facl_people;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1384
    const-string v0, "DROP TABLE IF EXISTS owners;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1385
    const-string v0, "DROP TABLE IF EXISTS search_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1386
    const-string v0, "DROP TABLE IF EXISTS emails;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1387
    const-string v0, "DROP TABLE IF EXISTS phones;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1388
    const-string v0, "DROP TABLE IF EXISTS postal_address;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1389
    const-string v0, "DROP TABLE IF EXISTS circle_members;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1390
    const-string v0, "DROP TABLE IF EXISTS circles;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1391
    const-string v0, "DROP TABLE IF EXISTS people;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1392
    const-string v0, "DROP TABLE IF EXISTS sync_tokens;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1393
    const-string v0, "DROP TABLE IF EXISTS properties;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1394
    const-string v0, "DROP TABLE IF EXISTS email_gaia_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1395
    const-string v0, "DROP TABLE IF EXISTS gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1396
    return-void
.end method

.method public static d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 1399
    const-string v0, "CREATE TABLE search_index (person_id INTEGER NOT NULL,kind INTEGER NOT NULL,value TEXT NOT NULL,FOREIGN KEY (person_id) REFERENCES people(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1408
    const-string v0, "CREATE INDEX search_value ON search_index (value);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1411
    const-string v0, "CREATE INDEX search_person_id_index ON search_index (person_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1415
    const-string v0, "CREATE TABLE ac_index (item_id INTEGER NOT NULL,owner_id INTEGER NOT NULL,is_normalized INTEGER NOT NULL,kind INTEGER NOT NULL,value TEXT NOT NULL COLLATE NOCASE,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE,FOREIGN KEY (item_id) REFERENCES ac_item(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1429
    const-string v0, "CREATE INDEX ac_index_1 ON ac_index (owner_id,value,kind);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1435
    const-string v0, "CREATE INDEX ac_index_item_id ON ac_index (item_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1439
    const-string v0, "CREATE INDEX ac_item_container ON ac_item (container_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1443
    const-string v0, "CREATE INDEX ac_item_container_person_id ON ac_container (people_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1447
    const-string v0, "CREATE INDEX ac_people_v2_id ON ac_people (owner_id,people_v2_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1452
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 808
    const-string v0, "CREATE TABLE owners (_id INTEGER PRIMARY KEY AUTOINCREMENT,account_name TEXT NOT NULL,gaia_id TEXT,page_gaia_id TEXT,display_name TEXT,avatar TEXT,cover_photo_url TEXT,cover_photo_height INTEGER NOT NULL DEFAULT 0,cover_photo_width INTEGER NOT NULL DEFAULT 0,cover_photo_id TEXT,last_sync_start_time INTEGER NOT NULL DEFAULT 0,last_sync_finish_time INTEGER NOT NULL DEFAULT 0,last_sync_status INTEGER NOT NULL DEFAULT 0,last_successful_sync_time INTEGER NOT NULL DEFAULT 0,sync_to_contacts INTEGER NOT NULL DEFAULT 0,is_dasher INTEGER NOT NULL DEFAULT 0 ,dasher_domain TEXT,etag TEXT,sync_circles_to_contacts INTEGER NOT NULL DEFAULT 0,sync_evergreen_to_contacts INTEGER NOT NULL DEFAULT 0,last_full_people_sync_time INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 834
    const-string v0, "CREATE TABLE owner_sync_requests (_id INTEGER PRIMARY KEY AUTOINCREMENT,account_name TEXT NOT NULL,page_gaia_id TEXT,sync_requested_time INTEGER NOT NULL DEFAULT 0,UNIQUE (account_name,page_gaia_id));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 844
    const-string v0, "CREATE TABLE people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,gaia_id TEXT,v2_id TEXT NOT NULL,name TEXT,given_name TEXT,family_name TEXT,middle_name TEXT,name_verified INTEGER NOT NULL DEFAULT 0,profile_type INTEGER NOT NULL,sort_key TEXT,sort_key_last_name TEXT,sort_key_irank TEXT,avatar TEXT,tagline TEXT,blocked INTEGER NOT NULL DEFAULT 0,etag TEXT,last_modified INTEGER NOT NULL DEFAULT 0,invisible_3p INTEGER NOT NULL DEFAULT 0,in_viewer_domain INTEGER NOT NULL DEFAULT 0 ,in_circle INTEGER NOT NULL DEFAULT 0,in_contacts INTEGER NOT NULL DEFAULT 0,affinity1 REAL,affinity2 REAL,affinity3 REAL,affinity4 REAL,affinity5 REAL,people_in_common TEXT,sync_is_alive INTEGER NOT NULL DEFAULT 0,logging_id TEXT,logging_id2 TEXT,logging_id3 TEXT,logging_id4 TEXT,logging_id5 TEXT,mobile_owner_id TEXT,UNIQUE (owner_id,qualified_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 890
    const-string v0, "CREATE TABLE sync_tokens (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,name TEXT NOT NULL,value TEXT NOT NULL,UNIQUE (owner_id,name),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 902
    const-string v0, "CREATE TABLE circles (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,circle_id TEXT NOT NULL,name TEXT,sort_key TEXT,type INTEGER NOT NULL,for_sharing INTEGER NOT NULL DEFAULT 0,people_count INTEGER NOT NULL DEFAULT -1,client_policies INTEGER NOT NULL DEFAULT 0,etag TEXT,last_modified INTEGER NOT NULL DEFAULT 0,sync_to_contacts INTEGER NOT NULL DEFAULT 0,UNIQUE (owner_id,circle_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 922
    const-string v0, "CREATE TABLE emails (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,email TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,affinity1 REAL,affinity2 REAL,affinity3 REAL,affinity4 REAL,affinity5 REAL,logging_id TEXT,logging_id2 TEXT,logging_id3 TEXT,logging_id4 TEXT,logging_id5 TEXT,FOREIGN KEY (owner_id,qualified_id) REFERENCES people(owner_id,qualified_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 946
    const-string v0, "CREATE INDEX IF NOT EXISTS email_person ON emails (owner_id,qualified_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 949
    const-string v0, "CREATE TABLE phones (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,phone TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id,qualified_id) REFERENCES people(owner_id,qualified_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 963
    const-string v0, "CREATE INDEX IF NOT EXISTS phone_person ON phones (owner_id,qualified_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 966
    const-string v0, "CREATE TABLE postal_address (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,postal_address TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id,qualified_id) REFERENCES people(owner_id,qualified_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 980
    const-string v0, "CREATE INDEX IF NOT EXISTS postal_address_person ON postal_address (owner_id,qualified_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 987
    const-string v0, "CREATE TABLE owner_emails (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,email TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 998
    const-string v0, "CREATE TABLE owner_phones (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,phone TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1009
    const-string v0, "CREATE TABLE owner_postal_address (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,postal_address TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1020
    const-string v0, "CREATE TABLE circle_members (owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,circle_id TEXT NOT NULL,PRIMARY KEY (owner_id,qualified_id,circle_id),FOREIGN KEY (owner_id,qualified_id) REFERENCES people(owner_id,qualified_id) ON DELETE CASCADE,FOREIGN KEY (owner_id,circle_id) REFERENCES circles(owner_id,circle_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1040
    const-string v0, "CREATE TABLE gaia_id_map (owner_id INTEGER NOT NULL,contact_id TEXT NOT NULL,value TEXT NOT NULL,gaia_id TEXT NOT NULL,type INTEGER NOT NULL,UNIQUE (owner_id,contact_id,value), FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1054
    const-string v0, "CREATE TABLE applications (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,UNIQUE (owner_id,dev_console_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1064
    const-string v0, "CREATE TABLE application_packages (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,package_name TEXT NOT NULL,certificate_hash TEXT NOT NULL,FOREIGN KEY (owner_id, dev_console_id)REFERENCES applications(owner_id, dev_console_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1076
    const-string v0, "CREATE TABLE facl_people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,qualified_id TEXT NOT NULL,UNIQUE (owner_id,dev_console_id,qualified_id) ON CONFLICT IGNORE,FOREIGN KEY (owner_id, qualified_id) REFERENCES people(owner_id, qualified_id) ON DELETE CASCADE,FOREIGN KEY (owner_id, dev_console_id) REFERENCES applications(owner_id, dev_console_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1095
    const-string v0, "CREATE TABLE ac_people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,people_v2_id TEXT NOT NULL,qualified_id TEXT,sync_is_alive INTEGER NOT NULL DEFAULT 0,affinity1 REAL,affinity2 REAL,affinity3 REAL,affinity4 REAL,affinity5 REAL,logging_id TEXT,logging_id2 TEXT,logging_id3 TEXT,logging_id4 TEXT,logging_id5 TEXT,UNIQUE (owner_id,people_v2_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1118
    const-string v0, "CREATE TABLE ac_container (_id INTEGER PRIMARY KEY AUTOINCREMENT,people_id INTEGER NOT NULL,container_type INTEGER NOT NULL,profile_type INTEGER NOT NULL,gaia_id TEXT,contact_id TEXT,compressed_avatar_url TEXT,has_avatar INTEGER NOT NULL DEFAULT 0,in_circle INTEGER NOT NULL DEFAULT 0,in_viewer_domain INTEGER NOT NULL DEFAULT 0,display_name TEXT,formatted_name TEXT,given_name TEXT,family_name TEXT,middle_name TEXT,honorific_prefix TEXT,honorific_suffix TEXT,yomi_given_name TEXT,yomi_family_name TEXT,yomi_honorific_prefix TEXT,yomi_honorific_suffix TEXT,nickname TEXT,FOREIGN KEY (people_id) REFERENCES ac_people(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1148
    const-string v0, "CREATE TABLE ac_item (_id INTEGER PRIMARY KEY AUTOINCREMENT,container_id INTEGER NOT NULL,item_type INTEGER NOT NULL,is_edge_key INTEGER,value TEXT NOT NULL,value2 TEXT,value_type INTEGER,custom_label TEXT,affinity1 REAL,affinity2 REAL,affinity3 REAL,affinity4 REAL,affinity5 REAL,logging_id TEXT,logging_id2 TEXT,logging_id3 TEXT,logging_id4 TEXT,logging_id5 TEXT,FOREIGN KEY (container_id) REFERENCES ac_container(_id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1173
    invoke-static {p1}, Lcom/google/android/gms/people/c/g;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1175
    const-string v0, "CREATE TABLE properties (name TEXT NOT NULL PRIMARY KEY,value TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1181
    const-string v0, "CREATE TABLE temp_gaia_ordinal (ordinal INTEGER NOT NULL,gaia_id TEXT NULL,qualified_id TEXT NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1188
    const-string v0, "ANALYZE;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1189
    invoke-static {p1}, Lcom/google/android/gms/people/c/g;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1190
    return-void
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 802
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 804
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    .line 805
    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 1457
    invoke-static {}, Lcom/google/android/gms/people/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1459
    invoke-super {p0, p1, p2, p3}, Landroid/database/sqlite/SQLiteOpenHelper;->onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 1471
    :goto_0
    return-void

    .line 1461
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Downgrading from version "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1463
    iget-object v1, p0, Lcom/google/android/gms/people/c/g;->a:Lcom/google/android/gms/people/c/f;

    invoke-static {v1}, Lcom/google/android/gms/people/c/f;->a(Lcom/google/android/gms/people/c/f;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "PeopleDatabaseHelper"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1464
    const-string v1, "PeopleDatabaseHelper"

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1466
    iget-object v0, p0, Lcom/google/android/gms/people/c/g;->a:Lcom/google/android/gms/people/c/f;

    invoke-static {v0, p1}, Lcom/google/android/gms/people/c/f;->a(Lcom/google/android/gms/people/c/f;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1469
    iget-object v0, p0, Lcom/google/android/gms/people/c/g;->a:Lcom/google/android/gms/people/c/f;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Lcom/google/android/gms/people/c/f;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 795
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v0

    if-nez v0, :cond_0

    .line 796
    const-string v0, "PRAGMA foreign_keys=ON;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 798
    :cond_0
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 10

    .prologue
    const/16 v3, 0x22

    const/16 v1, 0x21

    const/16 v4, 0x20

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 1735
    new-instance v5, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v5, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 1738
    :try_start_0
    const-string v6, "PeopleDatabaseHelper"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Upgrading from version "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    if-ge p2, v4, :cond_2a

    invoke-static {p1}, Lcom/google/android/gms/people/c/g;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/c/g;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    if-ge v4, v1, :cond_29

    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_1
    if-ge v1, v3, :cond_28

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->e(Landroid/database/sqlite/SQLiteDatabase;)V

    :goto_2
    const/16 v1, 0x23

    if-ge v3, v1, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->f(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x23

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_0
    const/16 v1, 0x24

    if-ge v3, v1, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->g(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x24

    :cond_1
    const/16 v1, 0x25

    if-ge v3, v1, :cond_27

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->h(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x25

    move v1, v2

    :goto_3
    const/16 v4, 0x26

    if-ge v3, v4, :cond_2

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->i(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x26

    :cond_2
    const/16 v4, 0x27

    if-ge v3, v4, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x27

    :cond_3
    const/16 v4, 0x28

    if-ge v3, v4, :cond_4

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->k(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x28

    :cond_4
    const/16 v4, 0x29

    if-ge v3, v4, :cond_5

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->l(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x29

    :cond_5
    const/16 v4, 0x2a

    if-ge v3, v4, :cond_6

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x2a

    :cond_6
    const/16 v4, 0x2c

    if-ge v3, v4, :cond_7

    iget-object v3, p0, Lcom/google/android/gms/people/c/g;->a:Lcom/google/android/gms/people/c/f;

    invoke-static {v3}, Lcom/google/android/gms/people/c/f;->a(Lcom/google/android/gms/people/c/f;)Landroid/content/Context;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/google/android/gms/people/c/f;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    const/16 v3, 0x2c

    :cond_7
    const/16 v4, 0x2d

    if-ge v3, v4, :cond_8

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->m(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x2d

    :cond_8
    const/16 v4, 0x2e

    if-ge v3, v4, :cond_9

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->n(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x2e

    :cond_9
    const/16 v4, 0x2f

    if-ge v3, v4, :cond_a

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x2f

    :cond_a
    const/16 v4, 0x64

    if-ge v3, v4, :cond_b

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->o(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x64

    move v1, v2

    :cond_b
    const/16 v4, 0x65

    if-ge v3, v4, :cond_c

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->p(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x65

    move v1, v2

    :cond_c
    const/16 v4, 0x66

    if-ge v3, v4, :cond_d

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->q(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x66

    :cond_d
    const/16 v4, 0x67

    if-ge v3, v4, :cond_e

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->r(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x67

    :cond_e
    const/16 v4, 0x68

    if-ge v3, v4, :cond_f

    const/16 v3, 0x68

    :cond_f
    const/16 v4, 0x69

    if-ge v3, v4, :cond_10

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->s(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x69

    move v1, v2

    :cond_10
    const/16 v4, 0x6a

    if-ge v3, v4, :cond_11

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x6a

    :cond_11
    const/16 v4, 0x6b

    if-ge v3, v4, :cond_12

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->t(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x6b

    :cond_12
    const/16 v4, 0x6c

    if-ge v3, v4, :cond_13

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0x6c

    :cond_13
    const/16 v4, 0xc8

    if-ge v3, v4, :cond_14

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0xc8

    :cond_14
    const/16 v4, 0xc9

    if-ge v3, v4, :cond_15

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0xc9

    :cond_15
    const/16 v4, 0xca

    if-ge v3, v4, :cond_16

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->u(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v3, 0xca

    :cond_16
    const/16 v4, 0x19b

    if-ge v3, v4, :cond_26

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->v(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v1, 0x19b

    move v3, v2

    move v0, v2

    :goto_4
    const/16 v4, 0x19c

    if-ge v1, v4, :cond_17

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->w(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v1, 0x19c

    :cond_17
    const/16 v4, 0x19d

    if-ge v1, v4, :cond_18

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->x(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v1, 0x19d

    move v0, v2

    :cond_18
    const/16 v4, 0x19e

    if-ge v1, v4, :cond_19

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->y(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v1, 0x19e

    :cond_19
    const/16 v4, 0x19f

    if-ge v1, v4, :cond_1a

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->z(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v1, 0x19f

    move v0, v2

    :cond_1a
    const/16 v4, 0x1f5

    if-ge v1, v4, :cond_25

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->A(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x1f5

    move v1, v2

    :goto_5
    const/16 v4, 0x258

    if-ge v0, v4, :cond_1b

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->B(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x258

    :cond_1b
    const/16 v4, 0x259

    if-ge v0, v4, :cond_1c

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->C(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x259

    :cond_1c
    const/16 v4, 0x25a

    if-ge v0, v4, :cond_1d

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->D(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x25a

    :cond_1d
    const/16 v4, 0x25b

    if-ge v0, v4, :cond_1e

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->E(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x25b

    :cond_1e
    const/16 v4, 0x25c

    if-ge v0, v4, :cond_1f

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    const/16 v0, 0x25c

    :cond_1f
    const/16 v4, 0x2bc

    if-ge v0, v4, :cond_20

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->j(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_20
    if-eqz v1, :cond_21

    invoke-static {p1}, Lcom/google/android/gms/people/c/g;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_21
    if-eqz v3, :cond_22

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->d(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1755
    :cond_22
    :goto_6
    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1756
    iget-object v0, p0, Lcom/google/android/gms/people/c/g;->a:Lcom/google/android/gms/people/c/f;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Lcom/google/android/gms/people/c/f;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1758
    :cond_23
    return-void

    .line 1739
    :catch_0
    move-exception v0

    .line 1740
    invoke-static {}, Lcom/google/android/gms/common/ew;->a()Z

    move-result v1

    if-nez v1, :cond_24

    .line 1741
    throw v0

    .line 1744
    :cond_24
    const-string v1, "PeopleDatabaseHelper"

    const-string v3, "Upgrade failed.  Re-creating the database."

    invoke-static {v1, v3, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1746
    iget-object v1, p0, Lcom/google/android/gms/people/c/g;->a:Lcom/google/android/gms/people/c/f;

    invoke-static {v1}, Lcom/google/android/gms/people/c/f;->a(Lcom/google/android/gms/people/c/f;)Landroid/content/Context;

    move-result-object v1

    const-string v3, "PeopleDatabaseHelper"

    const-string v4, "Upgrade failed.  Re-creating the database."

    invoke-static {v1, v3, v4, v0}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1748
    iget-object v0, p0, Lcom/google/android/gms/people/c/g;->a:Lcom/google/android/gms/people/c/f;

    invoke-static {v0, p1}, Lcom/google/android/gms/people/c/f;->a(Lcom/google/android/gms/people/c/f;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1751
    invoke-virtual {v5, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_6

    :cond_25
    move v9, v0

    move v0, v1

    move v1, v9

    goto :goto_5

    :cond_26
    move v9, v0

    move v0, v1

    move v1, v3

    move v3, v9

    goto/16 :goto_4

    :cond_27
    move v1, v0

    goto/16 :goto_3

    :cond_28
    move v3, v1

    goto/16 :goto_2

    :cond_29
    move v1, v4

    goto/16 :goto_1

    :cond_2a
    move v4, p2

    goto/16 :goto_0
.end method
