.class public final Lcom/google/android/gms/wallet/common/ui/bb;
.super Lcom/google/android/gms/common/ui/a;
.source "SourceFile"


# instance fields
.field private j:Lcom/google/android/gms/wallet/common/ui/bf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/a;-><init>()V

    .line 194
    return-void
.end method

.method public static a(I)Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 3

    .prologue
    .line 75
    sget v0, Lcom/google/android/gms/p;->Cg:I

    sget v1, Lcom/google/android/gms/p;->Cf:I

    const/16 v2, 0x3e8

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(IIII)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    return-object v0
.end method

.method private static a(IIII)Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 53
    const-string v1, "buttonMode"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    const-string v1, "titleId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    const-string v1, "messageId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 56
    const-string v1, "errorCode"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 57
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/common/ui/bb;-><init>()V

    .line 58
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/bb;->setArguments(Landroid/os/Bundle;)V

    .line 59
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 3

    .prologue
    .line 64
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 65
    const-string v1, "buttonMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v1, "errorCode"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/common/ui/bb;-><init>()V

    .line 70
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/bb;->setArguments(Landroid/os/Bundle;)V

    .line 71
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/bb;)Lcom/google/android/gms/wallet/common/ui/bf;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bb;->j:Lcom/google/android/gms/wallet/common/ui/bf;

    return-object v0
.end method

.method public static b()Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 4

    .prologue
    .line 91
    const/4 v0, 0x1

    sget v1, Lcom/google/android/gms/p;->CJ:I

    sget v2, Lcom/google/android/gms/p;->Ab:I

    const/16 v3, 0x3e9

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/bb;->a(IIII)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    return-object v0
.end method

.method public static b(I)Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 3

    .prologue
    .line 83
    const/4 v0, 0x2

    sget v1, Lcom/google/android/gms/p;->Cg:I

    sget v2, Lcom/google/android/gms/p;->Cf:I

    invoke-static {v0, v1, v2, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(IIII)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 4

    .prologue
    .line 99
    const/4 v0, 0x1

    sget v1, Lcom/google/android/gms/p;->CJ:I

    sget v2, Lcom/google/android/gms/p;->Aa:I

    const/16 v3, 0x3e9

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/bb;->a(IIII)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 4

    .prologue
    .line 107
    const/4 v0, 0x1

    sget v1, Lcom/google/android/gms/p;->CP:I

    sget v2, Lcom/google/android/gms/p;->CO:I

    const/16 v3, 0x3ea

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/bb;->a(IIII)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/common/ui/bf;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bb;->j:Lcom/google/android/gms/wallet/common/ui/bf;

    .line 116
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bb;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 125
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bb;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 126
    const-string v0, "titleId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    const-string v0, "titleId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 131
    :goto_0
    const-string v0, "messageId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    const-string v0, "messageId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 145
    :goto_1
    const-string v0, "buttonMode"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 146
    if-ne v0, v6, :cond_3

    .line 147
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Z)V

    .line 148
    sget v0, Lcom/google/android/gms/p;->Ch:I

    new-instance v3, Lcom/google/android/gms/wallet/common/ui/bc;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/wallet/common/ui/bc;-><init>(Lcom/google/android/gms/wallet/common/ui/bb;Landroid/os/Bundle;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 178
    :cond_0
    :goto_2
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 129
    :cond_1
    const-string v0, "title"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 136
    :cond_2
    new-instance v3, Landroid/text/SpannableString;

    const-string v0, "message"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bb;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 139
    sget v4, Lcom/google/android/gms/l;->hv:I

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 141
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    const/16 v3, 0xf

    invoke-static {v0, v3}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 143
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 157
    :cond_3
    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 158
    invoke-virtual {p0, v6}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Z)V

    .line 159
    sget v0, Lcom/google/android/gms/p;->Cv:I

    new-instance v3, Lcom/google/android/gms/wallet/common/ui/bd;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/wallet/common/ui/bd;-><init>(Lcom/google/android/gms/wallet/common/ui/bb;Landroid/os/Bundle;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 168
    sget v0, Lcom/google/android/gms/p;->Af:I

    new-instance v3, Lcom/google/android/gms/wallet/common/ui/be;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/wallet/common/ui/be;-><init>(Lcom/google/android/gms/wallet/common/ui/bb;Landroid/os/Bundle;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 183
    invoke-super {p0, p1}, Lcom/google/android/gms/common/ui/a;->onCancel(Landroid/content/DialogInterface;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bb;->j:Lcom/google/android/gms/wallet/common/ui/bf;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bb;->j:Lcom/google/android/gms/wallet/common/ui/bf;

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bb;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "errorCode"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bf;->a(II)V

    .line 189
    :cond_0
    return-void
.end method
