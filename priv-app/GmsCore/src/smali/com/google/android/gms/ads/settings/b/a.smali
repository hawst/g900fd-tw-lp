.class public final Lcom/google/android/gms/ads/settings/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a()V
    .locals 2

    .prologue
    .line 32
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->a()Lcom/google/android/gms/ads/identifier/b/b;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/b/b;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 35
    invoke-static {}, Lcom/google/android/gms/ads/settings/b/f;->a()Lcom/google/android/gms/ads/settings/b/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/settings/b/f;->a(Lcom/google/android/gms/ads/identifier/b/b;)V

    .line 40
    :cond_0
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/i;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    new-instance v0, Lcom/google/android/gms/ads/settings/c/i;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/settings/c/i;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/i;->a()V

    .line 44
    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/i;->f()V

    .line 45
    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/i;->b()V

    .line 49
    :cond_1
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/b;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 51
    new-instance v0, Lcom/google/android/gms/ads/settings/c/b;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/settings/c/b;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/b;->a()V

    .line 53
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/settings/c/b;->b(I)V

    .line 54
    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/b;->b()V

    .line 62
    :cond_2
    :goto_0
    return-void

    .line 55
    :cond_3
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/b;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57
    new-instance v0, Lcom/google/android/gms/ads/settings/c/b;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/settings/c/b;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/b;->a()V

    .line 59
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/settings/c/b;->b(I)V

    .line 60
    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/b;->b()V

    goto :goto_0
.end method
