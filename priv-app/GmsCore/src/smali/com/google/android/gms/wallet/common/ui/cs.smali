.class public final Lcom/google/android/gms/wallet/common/ui/cs;
.super Landroid/support/v4/view/n;
.source "SourceFile"


# instance fields
.field public d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

.field private final e:Lcom/google/android/gms/wallet/common/ui/cq;

.field private final f:[Lcom/google/android/gms/wallet/common/ui/a;

.field private g:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/cq;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 30
    invoke-direct {p0, p1}, Landroid/support/v4/view/n;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/cs;->e:Lcom/google/android/gms/wallet/common/ui/cq;

    .line 34
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    iget-object v0, p0, Landroid/support/v4/view/n;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a([Landroid/accounts/Account;)[Lcom/google/android/gms/wallet/common/ui/a;

    move-result-object v0

    .line 38
    array-length v1, v0

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/wallet/common/ui/a;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cs;->f:[Lcom/google/android/gms/wallet/common/ui/a;

    .line 39
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cs;->f:[Lcom/google/android/gms/wallet/common/ui/a;

    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->f:[Lcom/google/android/gms/wallet/common/ui/a;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cs;->f:[Lcom/google/android/gms/wallet/common/ui/a;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    new-instance v2, Lcom/google/android/gms/wallet/common/ui/a;

    iget-object v3, p0, Landroid/support/v4/view/n;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v4/view/n;->a:Landroid/content/Context;

    sget v5, Lcom/google/android/gms/p;->BD:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/wallet/common/ui/a;-><init>(Landroid/accounts/Account;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 44
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v4/view/n;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 49
    sget v1, Lcom/google/android/gms/l;->hm:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->g:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cs;->g:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cs;->f:[Lcom/google/android/gms/wallet/common/ui/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a([Lcom/google/android/gms/wallet/common/ui/a;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cs;->e:Lcom/google/android/gms/wallet/common/ui/cq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Lcom/google/android/gms/wallet/common/ui/cq;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b()V

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/cs;->g:Landroid/accounts/Account;

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    .line 65
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cs;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->setEnabled(Z)V

    .line 69
    return-void
.end method
