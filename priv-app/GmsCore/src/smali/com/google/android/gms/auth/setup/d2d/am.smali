.class public final Lcom/google/android/gms/auth/setup/d2d/am;
.super Landroid/app/Fragment;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private b:Lcom/google/android/gms/auth/setup/d2d/ap;

.field private c:Landroid/nfc/NfcAdapter;

.field private d:Landroid/bluetooth/BluetoothAdapter;

.field private e:[B

.field private f:Lcom/google/android/gms/auth/setup/d2d/a/e;

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TargetResourcesFragment"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/am;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/am;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->g:J

    return-wide v0
.end method

.method static synthetic a()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/am;->a:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/setup/d2d/am;)Lcom/google/android/gms/auth/setup/d2d/ap;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->b:Lcom/google/android/gms/auth/setup/d2d/ap;

    return-object v0
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 68
    check-cast p1, Lcom/google/android/gms/auth/setup/d2d/ap;

    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/am;->b:Lcom/google/android/gms/auth/setup/d2d/ap;

    .line 69
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 73
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/setup/d2d/am;->setRetainInstance(Z)V

    .line 76
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->g:J

    .line 78
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->d:Landroid/bluetooth/BluetoothAdapter;

    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/am;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->c:Landroid/nfc/NfcAdapter;

    .line 80
    new-instance v5, Lcom/google/ab/b/a/e/h;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Lcom/google/ab/b/a/e/h;-><init>(B)V

    iget-boolean v0, v5, Lcom/google/ab/b/a/e/h;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PartialConnectionContext can only generate one InitiatorHello message. Create additional PartialConnectionContexts for new connections."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean v2, v5, Lcom/google/ab/b/a/e/h;->b:Z

    new-instance v0, Lcom/google/ab/b/a/e/d;

    invoke-direct {v0}, Lcom/google/ab/b/a/e/d;-><init>()V

    iget-object v1, v5, Lcom/google/ab/b/a/e/h;->a:Ljava/security/KeyPair;

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v1

    invoke-static {v1}, Lcom/google/ab/b/a/f/d;->a(Ljava/security/PublicKey;)Lcom/google/ab/b/a/f/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ab/b/a/e/d;->a(Lcom/google/ab/b/a/f/m;)Lcom/google/ab/b/a/e/d;

    invoke-virtual {v0}, Lcom/google/ab/b/a/e/d;->g()[B

    move-result-object v6

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/an;

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/am;->d:Landroid/bluetooth/BluetoothAdapter;

    const-string v4, "D2D Account Setup"

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/auth/setup/d2d/an;-><init>(Lcom/google/android/gms/auth/setup/d2d/am;Landroid/bluetooth/BluetoothAdapter;Ljava/util/UUID;Ljava/lang/String;Lcom/google/ab/b/a/e/h;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->f:Lcom/google/android/gms/auth/setup/d2d/a/e;

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->f:Lcom/google/android/gms/auth/setup/d2d/a/e;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/a/e;->start()V

    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/d;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/d;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/am;->d:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/d;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/d;->a:Ljava/lang/String;

    iput-object v6, v0, Lcom/google/android/gms/auth/setup/d2d/b/d;->c:[B

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->e:[B

    .line 81
    return-void
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->f:Lcom/google/android/gms/auth/setup/d2d/a/e;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->f:Lcom/google/android/gms/auth/setup/d2d/a/e;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/a/e;->b()V

    .line 100
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 101
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    .line 91
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->c:Landroid/nfc/NfcAdapter;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/am;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, v2, v3}, Landroid/nfc/NfcAdapter;->setNdefPushMessage(Landroid/nfc/NdefMessage;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 93
    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 85
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 86
    const-string v0, "com.google.android.gms.auth.setup.d2d"

    const-string v1, "nfc2bt_bootstrap"

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/am;->e:[B

    invoke-static {v0, v1, v2}, Landroid/nfc/NdefRecord;->createExternal(Ljava/lang/String;Ljava/lang/String;[B)Landroid/nfc/NdefRecord;

    move-result-object v0

    new-instance v1, Landroid/nfc/NdefMessage;

    new-array v2, v4, [Landroid/nfc/NdefRecord;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-direct {v1, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/am;->c:Landroid/nfc/NfcAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/am;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Landroid/nfc/NfcAdapter;->setNdefPushMessage(Landroid/nfc/NdefMessage;Landroid/app/Activity;I)V

    .line 87
    return-void
.end method
