.class final Lcom/google/android/gms/fitness/sync/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field private final b:Landroid/content/Context;

.field private c:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/fitness/sync/f;->b:Landroid/content/Context;

    .line 37
    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/data/Session;Lcom/google/android/gms/fitness/l/z;)Lcom/google/android/gms/fitness/data/Session;
    .locals 8

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    move-object v0, p1

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v0

    .line 132
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Session;

    .line 133
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Session;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    invoke-virtual {v0, p0}, Lcom/google/android/gms/fitness/data/Session;->a(Lcom/google/android/gms/fitness/data/Session;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/l/c;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 145
    :try_start_0
    iget-object v1, p1, Lcom/google/android/gms/fitness/l/c;->d:[B

    invoke-static {v1}, Lcom/google/af/a/b/a/a/ah;->a([B)Lcom/google/af/a/b/a/a/ah;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 151
    iget-boolean v1, p1, Lcom/google/android/gms/fitness/l/c;->c:Z

    if-eqz v1, :cond_0

    .line 152
    invoke-interface {p0, v0}, Lcom/google/android/gms/fitness/sync/d;->a(Lcom/google/af/a/b/a/a/ah;)V

    .line 156
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 147
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to parse: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 154
    :cond_0
    invoke-interface {p0, v0}, Lcom/google/android/gms/fitness/sync/d;->b(Lcom/google/af/a/b/a/a/ah;)V

    goto :goto_0
.end method


# virtual methods
.method final a(Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/l/z;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/fitness/sync/f;->b:Landroid/content/Context;

    const-string v1, "fitness_session_sync"

    invoke-virtual {v0, v1, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 71
    const-string v0, "PAGE_TOKEN"

    const/4 v1, 0x0

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "downloading sessions: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 77
    :try_start_0
    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/sync/d;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/apiary/a;

    move-result-object v3

    .line 78
    iget v1, p0, Lcom/google/android/gms/fitness/sync/f;->c:I

    iget-object v4, v3, Lcom/google/android/gms/fitness/apiary/a;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v1, v4

    iput v1, p0, Lcom/google/android/gms/fitness/sync/f;->c:I

    .line 79
    iget v1, p0, Lcom/google/android/gms/fitness/sync/f;->c:I

    iget-object v4, v3, Lcom/google/android/gms/fitness/apiary/a;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v1, v4

    iput v1, p0, Lcom/google/android/gms/fitness/sync/f;->c:I
    :try_end_0
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_0 .. :try_end_0} :catch_1

    .line 88
    const-string v0, "Downloaded %d deletions and %d insertions"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, v3, Lcom/google/android/gms/fitness/apiary/a;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v7

    const/4 v4, 0x1

    iget-object v5, v3, Lcom/google/android/gms/fitness/apiary/a;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 91
    iget-object v0, v3, Lcom/google/android/gms/fitness/apiary/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/ah;

    .line 93
    :try_start_1
    sget-object v1, Lcom/google/android/gms/fitness/apiary/c;->a:Lcom/google/android/gms/fitness/apiary/b;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/apiary/b;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/data/Session;

    .line 94
    const/4 v5, 0x0

    invoke-interface {p2, v1, v5}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/Session;Z)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 95
    :catch_0
    move-exception v1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unable to save sessions: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v5}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 80
    :catch_1
    move-exception v1

    .line 81
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unable to download sessions: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 82
    invoke-virtual {v1}, Lcom/google/android/gms/fitness/sync/g;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    :goto_1
    return-void

    .line 85
    :cond_1
    throw v1

    .line 99
    :cond_2
    iget-object v0, v3, Lcom/google/android/gms/fitness/apiary/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/ah;

    .line 101
    :try_start_2
    sget-object v1, Lcom/google/android/gms/fitness/apiary/c;->a:Lcom/google/android/gms/fitness/apiary/b;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/apiary/b;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/data/Session;

    .line 102
    invoke-static {v1, p2}, Lcom/google/android/gms/fitness/sync/f;->a(Lcom/google/android/gms/fitness/data/Session;Lcom/google/android/gms/fitness/l/z;)Lcom/google/android/gms/fitness/data/Session;

    move-result-object v5

    .line 103
    if-eqz v5, :cond_3

    .line 104
    const/4 v5, 0x0

    invoke-interface {p2, v1, v5}, Lcom/google/android/gms/fitness/l/z;->c(Lcom/google/android/gms/fitness/data/Session;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 108
    :catch_2
    move-exception v1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unable to save sessions: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v5}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 106
    :cond_3
    const/4 v5, 0x0

    :try_start_3
    invoke-interface {p2, v1, v5}, Lcom/google/android/gms/fitness/l/z;->b(Lcom/google/android/gms/fitness/data/Session;Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 112
    :cond_4
    iget-object v0, v3, Lcom/google/android/gms/fitness/apiary/a;->d:Ljava/lang/String;

    .line 113
    invoke-virtual {v3}, Lcom/google/android/gms/fitness/apiary/a;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 115
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 116
    const-string v1, "PAGE_TOKEN"

    iget-object v2, v3, Lcom/google/android/gms/fitness/apiary/a;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 117
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1
.end method
