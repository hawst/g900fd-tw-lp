.class public final enum Lcom/google/android/gms/drive/database/model/ap;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/ap;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/ap;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/ap;

.field private static final synthetic e:[Lcom/google/android/gms/drive/database/model/ap;


# instance fields
.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 96
    new-instance v0, Lcom/google/android/gms/drive/database/model/ap;

    const-string v1, "UNTRASHED"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/gms/drive/database/model/ap;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ap;->a:Lcom/google/android/gms/drive/database/model/ap;

    .line 100
    new-instance v0, Lcom/google/android/gms/drive/database/model/ap;

    const-string v1, "IMPLICITLY_TRASHED"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/gms/drive/database/model/ap;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ap;->b:Lcom/google/android/gms/drive/database/model/ap;

    .line 104
    new-instance v0, Lcom/google/android/gms/drive/database/model/ap;

    const-string v1, "EXPLICITLY_TRASHED"

    const-wide/16 v2, 0x2

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/gms/drive/database/model/ap;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ap;->c:Lcom/google/android/gms/drive/database/model/ap;

    .line 92
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/ap;

    sget-object v1, Lcom/google/android/gms/drive/database/model/ap;->a:Lcom/google/android/gms/drive/database/model/ap;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/drive/database/model/ap;->b:Lcom/google/android/gms/drive/database/model/ap;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/drive/database/model/ap;->c:Lcom/google/android/gms/drive/database/model/ap;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/drive/database/model/ap;->e:[Lcom/google/android/gms/drive/database/model/ap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 109
    iput-wide p3, p0, Lcom/google/android/gms/drive/database/model/ap;->d:J

    .line 110
    return-void
.end method

.method public static a(J)Lcom/google/android/gms/drive/database/model/ap;
    .locals 6

    .prologue
    .line 117
    invoke-static {}, Lcom/google/android/gms/drive/database/model/ap;->values()[Lcom/google/android/gms/drive/database/model/ap;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 118
    iget-wide v4, v3, Lcom/google/android/gms/drive/database/model/ap;->d:J

    cmp-long v4, v4, p0

    if-nez v4, :cond_0

    .line 119
    return-object v3

    .line 117
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unaccepted TrashState sql value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ap;
    .locals 1

    .prologue
    .line 92
    const-class v0, Lcom/google/android/gms/drive/database/model/ap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ap;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/ap;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->e:[Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/ap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/ap;

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ap;->d:J

    return-wide v0
.end method
