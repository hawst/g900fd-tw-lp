.class public Lcom/google/android/gms/car/CarMediaBrowserRootNode;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/gms/car/ex;

    invoke-direct {v0}, Lcom/google/android/gms/car/ex;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->a:I

    .line 99
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->a:I

    .line 93
    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->b:Ljava/lang/String;

    .line 94
    iput-object p3, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    .line 95
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 108
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/ex;->a(Lcom/google/android/gms/car/CarMediaBrowserRootNode;Landroid/os/Parcel;I)V

    .line 109
    return-void
.end method
