.class public abstract Lcom/google/android/gms/common/server/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final i:Ljava/util/HashMap;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Z

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/concurrent/ConcurrentHashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/common/server/a;->i:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/server/a;->h:Ljava/util/concurrent/ConcurrentHashMap;

    .line 89
    iput-object p1, p0, Lcom/google/android/gms/common/server/a;->a:Landroid/content/Context;

    .line 91
    iput-object p2, p0, Lcom/google/android/gms/common/server/a;->g:Ljava/lang/String;

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/server/a;->f:Ljava/lang/String;

    .line 94
    iput-boolean p4, p0, Lcom/google/android/gms/common/server/a;->b:Z

    .line 96
    iput-boolean p5, p0, Lcom/google/android/gms/common/server/a;->e:Z

    .line 97
    iput-object p6, p0, Lcom/google/android/gms/common/server/a;->c:Ljava/lang/String;

    .line 98
    iput-object p7, p0, Lcom/google/android/gms/common/server/a;->d:Ljava/lang/String;

    .line 99
    return-void
.end method

.method protected static a(ILjava/util/Map;)I
    .locals 2

    .prologue
    .line 376
    const/4 v0, 0x7

    if-ne p0, v0, :cond_0

    .line 377
    const-string v0, "X-HTTP-Method-Override"

    const-string v1, "PATCH"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    const/4 p0, 0x1

    .line 380
    :cond_0
    return p0
.end method

.method public static a(Lcom/android/volley/ac;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 291
    iget-object v1, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-nez v1, :cond_1

    .line 302
    :cond_0
    :goto_0
    return v0

    .line 294
    :cond_1
    iget-object v1, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v1, v1, Lcom/android/volley/m;->a:I

    const/16 v2, 0x191

    if-ne v1, v2, :cond_0

    .line 298
    invoke-static {p0}, Lcom/google/android/gms/common/server/b/c;->b(Lcom/android/volley/ac;)Ljava/lang/String;

    move-result-object v1

    .line 299
    if-eqz v1, :cond_0

    .line 302
    const-string v2, "authError"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "expired"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/common/server/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 324
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 327
    const/4 v0, 0x0

    .line 330
    :goto_0
    return-object v0

    .line 329
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/a;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/a/a;

    move-result-object v0

    .line 330
    iget-object v1, p0, Lcom/google/android/gms/common/server/a;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/server/a/c;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 3

    .prologue
    .line 189
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 190
    iget-object v1, p0, Lcom/google/android/gms/common/server/a;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    const-string v1, "X-Google-Backend-Override"

    iget-object v2, p0, Lcom/google/android/gms/common/server/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    :cond_0
    return-object v0
.end method

.method public a(Lcom/android/volley/p;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 250
    new-instance v0, Lcom/google/android/gms/common/server/b;

    iget-object v1, p0, Lcom/google/android/gms/common/server/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p2}, Lcom/google/android/gms/common/server/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/android/volley/p;->a(Lcom/android/volley/z;)Lcom/android/volley/p;

    .line 251
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/common/server/a;->h:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    return-void
.end method

.method protected final a(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 272
    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 273
    instance-of v1, v0, Lcom/android/volley/ac;

    if-eqz v1, :cond_1

    .line 274
    check-cast v0, Lcom/android/volley/ac;

    .line 276
    invoke-static {v0}, Lcom/google/android/gms/common/server/a;->a(Lcom/android/volley/ac;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/google/android/gms/common/server/a;->a:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 279
    :cond_0
    throw v0

    .line 281
    :cond_1
    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/gms/common/server/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 349
    const-string v0, "auth_token"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/server/ClientContext;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 350
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 360
    :goto_0
    return-object v0

    .line 353
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 356
    const/4 v0, 0x0

    goto :goto_0

    .line 358
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/a;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/a/a;

    move-result-object v0

    .line 360
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/common/server/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/a/a;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 361
    :catch_0
    move-exception v0

    .line 364
    new-instance v1, Lcom/android/volley/ac;

    invoke-direct {v1, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 254
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 255
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/common/server/a;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 257
    const-string v0, "?"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const-string v0, "&"

    .line 258
    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "trace="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/server/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/common/server/a;->h:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 260
    const-string v1, "&"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v4, "="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 257
    :cond_0
    const-string v0, "?"

    goto :goto_0

    .line 263
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/a/a;
    .locals 1

    .prologue
    .line 313
    new-instance v0, Lcom/google/android/gms/common/server/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    return-object v0
.end method

.method public d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 334
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/a;->c(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/a/a;

    move-result-object v0

    .line 335
    iget-object v1, p0, Lcom/google/android/gms/common/server/a;->a:Landroid/content/Context;

    invoke-interface {v0}, Lcom/google/android/gms/common/server/a/c;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
