.class public final Lcom/google/android/gms/fitness/b/c/w;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/fitness/b/e;)Z
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/gms/fitness/b/c/v;->a:Ljava/util/Set;

    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms"

    invoke-static {p0}, Lcom/google/android/gms/fitness/b/c/w;->e(Lcom/google/android/gms/fitness/b/e;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/fitness/b/e;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/e;->a()Lcom/google/android/gms/fitness/b/b;

    move-result-object v3

    .line 27
    if-nez v3, :cond_1

    .line 37
    :cond_0
    :goto_0
    return v2

    .line 33
    :cond_1
    invoke-interface {v3}, Lcom/google/android/gms/fitness/b/b;->a()Ljava/lang/String;

    move-result-object v4

    .line 34
    invoke-interface {v3}, Lcom/google/android/gms/fitness/b/b;->c()Ljava/lang/String;

    move-result-object v5

    .line 35
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 36
    :goto_1
    invoke-interface {v3}, Lcom/google/android/gms/fitness/b/b;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v1

    .line 37
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_0

    :cond_2
    const-string v0, "com.google.android.gms"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 35
    goto :goto_1

    :cond_4
    move v3, v2

    .line 36
    goto :goto_2
.end method

.method public static c(Lcom/google/android/gms/fitness/b/e;)Z
    .locals 2

    .prologue
    .line 44
    const-string v0, "session_activity_segment"

    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms"

    invoke-static {p0}, Lcom/google/android/gms/fitness/b/c/w;->e(Lcom/google/android/gms/fitness/b/e;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/google/android/gms/fitness/b/e;)Z
    .locals 2

    .prologue
    .line 52
    const-string v0, "com.google.android.apps.fitness"

    invoke-static {p0}, Lcom/google/android/gms/fitness/b/c/w;->e(Lcom/google/android/gms/fitness/b/e;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "user_input"

    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Lcom/google/android/gms/fitness/b/e;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/e;->a()Lcom/google/android/gms/fitness/b/b;

    move-result-object v0

    .line 62
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/b;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
