.class final Lcom/google/android/gms/wallet/service/ow/z;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;

.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ow/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/z;->e:Lcom/google/android/gms/wallet/service/ow/y;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/z;->c:Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ow/z;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/z;->c:Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v4

    check-cast v4, Lcom/google/aa/b/a/a/a/a/o;

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/z;->e:Lcom/google/android/gms/wallet/service/ow/y;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/z;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z

    move-result v0

    iput-boolean v0, v4, Lcom/google/aa/b/a/a/a/a/o;->d:Z

    .line 74
    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/ao;->a()Lcom/google/aa/b/a/a/a/a/t;

    move-result-object v0

    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/o;->e:Lcom/google/aa/b/a/a/a/a/t;

    .line 75
    iget-object v0, v4, Lcom/google/aa/b/a/a/a/a/o;->c:[I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v0

    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/o;->c:[I

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/z;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->a(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/common/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/z;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/z;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    .line 82
    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/o;->b:Ljava/lang/String;

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/z;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->b(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/z;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/ow/a;->a(Ljava/lang/String;)Lcom/google/aa/b/a/a/a/a/b;

    move-result-object v0

    .line 89
    if-nez v0, :cond_1

    .line 90
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 94
    :goto_0
    return-object v0

    .line 92
    :cond_1
    iget-object v1, v4, Lcom/google/aa/b/a/a/a/a/o;->a:Lcom/google/aa/b/a/a/a/a/u;

    iput-object v0, v1, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/z;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->c(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/z;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v1

    const-string v2, "/online/v2/wallet/sdk/v1/getWalletItems"

    const/16 v5, 0xf

    const-string v6, "get_wallet_items"

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ak;->a(ILjava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/protobuf/nano/j;ILjava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
