.class public Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:F

.field private f:I

.field private g:I

.field private h:I

.field private i:[Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/d;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/model/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->a:I

    .line 166
    return-void
.end method

.method constructor <init>(IILjava/lang/String;Ljava/lang/String;FIII[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput p1, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->a:I

    .line 153
    iput p2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->b:I

    .line 154
    iput-object p3, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->c:Ljava/lang/String;

    .line 155
    iput-object p4, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->d:Ljava/lang/String;

    .line 156
    iput p5, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->e:F

    .line 157
    iput p6, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->f:I

    .line 158
    iput p7, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->g:I

    .line 159
    iput p8, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->h:I

    .line 160
    iput-object p9, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->i:[Ljava/lang/String;

    .line 161
    iput-object p10, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->j:Ljava/lang/String;

    .line 162
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->b:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()F
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->e:F

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->f:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->g:I

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->h:I

    return v0
.end method

.method public final h()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->i:[Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->j:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 249
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "accountName"

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "autoBackupState"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "currentItem"

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "currentItemProgress"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "numCompleted"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "numPending"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "numFailed"

    iget v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "failedItems"

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->i:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "enabledAccountName"

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 175
    invoke-static {p0, p1}, Lcom/google/android/gms/photos/autobackup/model/d;->a(Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;Landroid/os/Parcel;)V

    .line 176
    return-void
.end method
