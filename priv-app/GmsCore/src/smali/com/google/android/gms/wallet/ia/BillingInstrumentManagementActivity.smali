.class public Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;
.super Lcom/google/android/gms/wallet/common/ui/ds;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/bp;


# static fields
.field private static final q:Ljava/lang/String;


# instance fields
.field private A:Lcom/google/android/apps/common/a/a/i;

.field private B:Lcom/google/android/apps/common/a/a/h;

.field private C:Landroid/view/View;

.field private D:Landroid/view/View;

.field private final E:Lcom/google/android/gms/wallet/service/l;

.field protected a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field protected b:Landroid/accounts/Account;

.field c:Lcom/google/checkout/inapp/proto/t;

.field d:Lcom/google/checkout/inapp/proto/j;

.field e:Lcom/google/checkout/inapp/proto/j;

.field f:I

.field g:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field h:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field i:Landroid/widget/ProgressBar;

.field j:Landroid/view/View;

.field k:Lcom/google/android/gms/wallet/common/ui/cc;

.field l:Lcom/google/android/gms/wallet/common/ui/cc;

.field m:Lcom/google/android/gms/wallet/common/ui/bb;

.field n:Lcom/google/android/gms/wallet/common/ui/bb;

.field o:Lcom/google/android/gms/wallet/common/ui/cd;

.field p:Lcom/google/android/gms/wallet/common/ui/cd;

.field private r:Lcom/google/checkout/inapp/proto/q;

.field private s:I

.field private t:Lcom/google/android/gms/wallet/common/ui/dh;

.field private u:Z

.field private v:Z

.field private w:Ljava/util/ArrayList;

.field private x:Z

.field private y:Lcom/google/android/apps/common/a/a/i;

.field private z:Lcom/google/android/apps/common/a/a/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-string v0, "billingInstrumentManagement"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ds;-><init>()V

    .line 103
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Z

    .line 104
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Z

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f:I

    .line 686
    new-instance v0, Lcom/google/android/gms/wallet/ia/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/b;-><init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lcom/google/android/gms/wallet/common/ui/cd;

    .line 749
    new-instance v0, Lcom/google/android/gms/wallet/ia/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/c;-><init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lcom/google/android/gms/wallet/common/ui/cd;

    .line 795
    new-instance v0, Lcom/google/android/gms/wallet/ia/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/d;-><init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->E:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method private static a(Lcom/google/checkout/inapp/proto/j;)I
    .locals 5

    .prologue
    const/16 v0, 0x79

    .line 481
    if-nez p0, :cond_1

    .line 482
    const/16 v0, 0x77

    .line 507
    :cond_0
    :goto_0
    return v0

    .line 484
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 486
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v4, v1

    .line 487
    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_2

    .line 488
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    aget v1, v1, v2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 489
    const/16 v3, 0x7d

    .line 487
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    :cond_2
    move v0, v1

    .line 494
    goto :goto_0

    .line 496
    :cond_3
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 498
    :pswitch_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 499
    const/16 v0, 0x7c

    goto :goto_0

    .line 501
    :cond_4
    const/16 v0, 0x7f

    goto :goto_0

    .line 504
    :pswitch_1
    const/16 v0, 0x78

    goto :goto_0

    .line 496
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/google/android/gms/wallet/common/ui/bb;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/bb;
    .locals 2

    .prologue
    .line 601
    if-eqz p1, :cond_0

    .line 602
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 606
    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    .line 608
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 609
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 610
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/checkout/inapp/proto/q;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r:Lcom/google/checkout/inapp/proto/q;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/google/checkout/inapp/proto/t;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 60
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    iget-object v4, v4, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v4, :cond_0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Lcom/google/checkout/inapp/proto/t;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v7, 0x7f

    const/4 v6, 0x0

    .line 60
    iget-object v4, p1, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v4, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/checkout/inapp/proto/j;)I

    move-result v1

    if-eq v1, v7, :cond_4

    move-object v1, v2

    :goto_0
    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    aput-object v3, v0, v6

    invoke-static {v4, v0}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v3

    :cond_1
    invoke-static {v3}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/checkout/inapp/proto/j;)I

    move-result v5

    if-eq v5, v7, :cond_3

    :goto_1
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v3, v6}, Lcom/google/android/gms/wallet/common/ui/cc;->b_(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v3, v4}, Lcom/google/android/gms/wallet/common/ui/cc;->a([Lcom/google/checkout/inapp/proto/j;)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v3, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v3, v6}, Lcom/google/android/gms/wallet/common/ui/cc;->b_(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v3, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a([Lcom/google/checkout/inapp/proto/j;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    :cond_2
    return-void

    :cond_3
    move-object v2, v3

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Z)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Lcom/google/checkout/inapp/proto/j;)Z
    .locals 1

    .prologue
    .line 60
    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7c
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 535
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->s:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->s:I

    .line 536
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->g()V

    .line 537
    return-void

    .line 535
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Z

    return v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 239
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x:Z

    .line 240
    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->s:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->g()V

    .line 241
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.BillingInstrumentManagementActivity.PaymentOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 244
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.BillingInstrumentManagementActivity.UpdatePaymentSettingsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 248
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->E:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 262
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 265
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->E:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f:I

    .line 266
    return-void

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    if-eqz v0, :cond_2

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->E:Lcom/google/android/gms/wallet/service/l;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/t;)V

    goto :goto_0

    .line 258
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    .prologue
    .line 60
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private f()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 513
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 520
    iget v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->s:I

    if-lez v0, :cond_1

    move v0, v1

    .line 522
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    if-eq v0, v3, :cond_0

    .line 523
    if-eqz v0, :cond_3

    .line 524
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 528
    :goto_2
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k:Lcom/google/android/gms/wallet/common/ui/cc;

    if-nez v0, :cond_4

    move v3, v1

    :goto_3
    invoke-interface {v4, v3}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 529
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    if-nez v0, :cond_5

    :goto_4
    invoke-interface {v3, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 530
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h()V

    .line 532
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 520
    goto :goto_0

    :cond_2
    move v3, v2

    .line 522
    goto :goto_1

    .line 526
    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i:Landroid/widget/ProgressBar;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v3, v2

    .line 528
    goto :goto_3

    :cond_5
    move v1, v2

    .line 529
    goto :goto_4
.end method

.method static synthetic h(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 552
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 553
    return-void

    .line 550
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 563
    iget v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f:I

    if-gez v0, :cond_0

    .line 564
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->E:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f:I

    .line 569
    :cond_0
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 578
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B:Lcom/google/android/apps/common/a/a/h;

    if-nez v0, :cond_1

    .line 579
    :cond_0
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "billing_update_payment_settings"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lcom/google/android/apps/common/a/a/i;

    .line 581
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B:Lcom/google/android/apps/common/a/a/h;

    .line 584
    :cond_1
    new-instance v0, Lcom/google/checkout/inapp/proto/w;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/w;-><init>()V

    .line 586
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r:Lcom/google/checkout/inapp/proto/q;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/w;->a:Lcom/google/checkout/inapp/proto/q;

    .line 587
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/w;->b:Ljava/lang/String;

    .line 589
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_2

    .line 590
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/w;->c:Ljava/lang/String;

    .line 593
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b(Z)V

    .line 594
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/w;)V

    .line 596
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Z

    .line 597
    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->y:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 648
    new-instance v0, Lcom/google/checkout/inapp/proto/s;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/s;-><init>()V

    .line 649
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r:Lcom/google/checkout/inapp/proto/q;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/s;->a:Lcom/google/checkout/inapp/proto/q;

    .line 651
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/s;)V

    .line 653
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b(Z)V

    .line 654
    return-void
.end method

.method static synthetic l(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->y:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic p(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic q(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Z

    return v0
.end method

.method static synthetic r(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Lcom/google/android/gms/wallet/common/ui/bb;

    const-string v1, "inapp.BillingInstrumentManagementActivity.UpdatePaymentSettingsNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/common/ui/bb;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Lcom/google/android/gms/wallet/common/ui/bb;

    return-void
.end method

.method static synthetic s(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    const-string v1, "inapp.BillingInstrumentManagementActivity.PaymentOptionsNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/common/ui/bb;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->m:Lcom/google/android/gms/wallet/common/ui/bb;

    return-void
.end method

.method static synthetic t(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 627
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_1

    .line 628
    packed-switch p1, :pswitch_data_0

    .line 640
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 630
    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(ILandroid/content/Intent;)V

    .line 638
    :goto_0
    return-void

    .line 633
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    if-eqz v0, :cond_0

    .line 634
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->j()V

    goto :goto_0

    .line 636
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k()V

    goto :goto_0

    .line 643
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected errorCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 628
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 398
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->C:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->D:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 400
    return-void

    :cond_0
    move v0, v2

    .line 398
    goto :goto_0

    :cond_1
    move v1, v2

    .line 399
    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Z

    .line 302
    packed-switch p1, :pswitch_data_0

    .line 380
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "instrument_management"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x:Z

    if-eqz v0, :cond_0

    .line 388
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e()V

    .line 392
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/ds;->onActivityResult(IILandroid/content/Intent;)V

    .line 393
    return-void

    .line 304
    :pswitch_0
    if-ne p2, v1, :cond_1

    .line 305
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "Successfully added a primary instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 310
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    .line 311
    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    goto :goto_0

    .line 312
    :cond_1
    if-nez p2, :cond_2

    .line 313
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "User canceled adding a primary instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 316
    :cond_2
    const-string v0, "BillingInstrumentManagementActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding a primary instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 321
    :pswitch_1
    if-ne p2, v1, :cond_3

    .line 322
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "Successfully added a backup instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 327
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    .line 328
    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    goto :goto_0

    .line 329
    :cond_3
    if-nez p2, :cond_4

    .line 330
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "User canceled adding a backup instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 333
    :cond_4
    const-string v0, "BillingInstrumentManagementActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding a backup instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 338
    :pswitch_2
    packed-switch p2, :pswitch_data_1

    .line 353
    const-string v0, "BillingInstrumentManagementActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating a primary instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 340
    :pswitch_3
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "Successfully updated a primary instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 345
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    .line 346
    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    goto/16 :goto_0

    .line 349
    :pswitch_4
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "User canceled updating a primary instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 359
    :pswitch_5
    packed-switch p2, :pswitch_data_2

    .line 374
    const-string v0, "BillingInstrumentManagementActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating a backup instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 361
    :pswitch_6
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "Successfully updated a backup instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 366
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    .line 367
    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    goto/16 :goto_0

    .line 370
    :pswitch_7
    const-string v0, "BillingInstrumentManagementActivity"

    const-string v1, "User canceled updating a backup instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 302
    nop

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
    .end packed-switch

    .line 338
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 359
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 574
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->j()V

    .line 575
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 142
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onCreate(Landroid/os/Bundle;)V

    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 145
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ck;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;

    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 148
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 149
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b:Landroid/accounts/Account;

    .line 150
    const-string v0, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Activity requires pcid extra!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 152
    const-string v0, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 153
    new-instance v1, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r:Lcom/google/checkout/inapp/proto/q;

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r:Lcom/google/checkout/inapp/proto/q;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/q;->a:Ljava/lang/String;

    .line 156
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->c:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 159
    sget v0, Lcom/google/android/gms/l;->fZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->setContentView(I)V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/Window;)V

    .line 161
    sget v0, Lcom/google/android/gms/j;->cc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->C:Landroid/view/View;

    .line 162
    sget v0, Lcom/google/android/gms/j;->te:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->D:Landroid/view/View;

    .line 169
    :goto_0
    sget v0, Lcom/google/android/gms/j;->pE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k:Lcom/google/android/gms/wallet/common/ui/cc;

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/android/gms/wallet/common/ui/cd;)V

    .line 174
    sget v0, Lcom/google/android/gms/j;->bR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/android/gms/wallet/common/ui/cd;)V

    .line 178
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    sget v0, Lcom/google/android/gms/j;->fQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    .line 180
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k:Lcom/google/android/gms/wallet/common/ui/cc;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 181
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l:Lcom/google/android/gms/wallet/common/ui/cc;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 182
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bp;)V

    .line 185
    :cond_0
    sget v0, Lcom/google/android/gms/j;->nN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->j:Landroid/view/View;

    .line 186
    sget v0, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i:Landroid/widget/ProgressBar;

    .line 188
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->g:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->g:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->g:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    .line 192
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 195
    if-eqz p1, :cond_3

    .line 196
    const-string v0, "hasRequestedToUpdatePaymentSettings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Z

    .line 198
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f:I

    .line 200
    const-string v0, "paymentInstrumentsResponse"

    const-class v1, Lcom/google/checkout/inapp/proto/t;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/t;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    .line 202
    const-string v0, "selectedPrimaryInstrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    .line 204
    const-string v0, "selectedBackupInstrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    .line 206
    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Z

    .line 219
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-nez v0, :cond_1

    .line 220
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->b:Landroid/accounts/Account;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 222
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 226
    :cond_1
    return-void

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 166
    sget v0, Lcom/google/android/gms/l;->fY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 209
    :cond_3
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "billing_get_payment_options"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->y:Lcom/google/android/apps/common/a/a/i;

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->y:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z:Lcom/google/android/apps/common/a/a/h;

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "instrument_management"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 270
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onPause()V

    .line 271
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i()V

    .line 272
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 230
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onResume()V

    .line 231
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Z

    if-eqz v0, :cond_0

    .line 232
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x:Z

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 276
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 278
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i()V

    .line 280
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 281
    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 282
    const-string v0, "hasRequestedToUpdatePaymentSettings"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    if-eqz v0, :cond_0

    .line 285
    const-string v0, "paymentInstrumentsResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    .line 289
    const-string v0, "selectedPrimaryInstrument"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_2

    .line 293
    const-string v0, "selectedBackupInstrument"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e:Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 296
    :cond_2
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 681
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Z

    .line 682
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/ds;->startActivityForResult(Landroid/content/Intent;I)V

    .line 683
    sget v0, Lcom/google/android/gms/b;->E:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->overridePendingTransition(II)V

    .line 684
    return-void
.end method
