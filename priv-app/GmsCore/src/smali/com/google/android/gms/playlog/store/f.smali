.class public Lcom/google/android/gms/playlog/store/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/Object;

.field private static c:I


# instance fields
.field private final d:Landroid/database/sqlite/SQLiteOpenHelper;

.field private final e:Lcom/google/android/gms/playlog/store/i;

.field private final f:Lcom/google/android/gms/playlog/store/c;

.field private final g:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    const-class v0, Lcom/google/android/gms/playlog/store/f;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/playlog/store/f;->a:Z

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/playlog/store/f;->b:Ljava/lang/Object;

    .line 30
    sput v1, Lcom/google/android/gms/playlog/store/f;->c:I

    return-void

    :cond_0
    move v0, v1

    .line 22
    goto :goto_0
.end method

.method private constructor <init>(Landroid/database/sqlite/SQLiteOpenHelper;J)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteOpenHelper;

    iput-object v0, p0, Lcom/google/android/gms/playlog/store/f;->d:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 48
    new-instance v0, Lcom/google/android/gms/playlog/store/i;

    iget-object v1, p0, Lcom/google/android/gms/playlog/store/f;->d:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-direct {v0, v1}, Lcom/google/android/gms/playlog/store/i;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;)V

    iput-object v0, p0, Lcom/google/android/gms/playlog/store/f;->e:Lcom/google/android/gms/playlog/store/i;

    .line 49
    new-instance v0, Lcom/google/android/gms/playlog/store/c;

    iget-object v1, p0, Lcom/google/android/gms/playlog/store/f;->d:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-direct {v0, v1}, Lcom/google/android/gms/playlog/store/c;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;)V

    iput-object v0, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    .line 50
    iput-wide p2, p0, Lcom/google/android/gms/playlog/store/f;->g:J

    .line 51
    return-void
.end method

.method public static a()Lcom/google/android/gms/playlog/store/f;
    .locals 6

    .prologue
    .line 38
    sget-object v1, Lcom/google/android/gms/playlog/store/f;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 39
    :try_start_0
    sget-boolean v0, Lcom/google/android/gms/playlog/store/f;->a:Z

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/gms/playlog/store/f;->c:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 40
    :cond_0
    :try_start_1
    sget v0, Lcom/google/android/gms/playlog/store/f;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/gms/playlog/store/f;->c:I

    .line 41
    new-instance v2, Lcom/google/android/gms/playlog/store/f;

    invoke-static {}, Lcom/google/android/gms/playlog/store/a;->a()Lcom/google/android/gms/playlog/store/a;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/playlog/a/b;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gms/playlog/store/f;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;J)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method private b(Lcom/google/android/gms/playlog/store/h;[B)V
    .locals 8

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/gms/playlog/store/f;->d:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 253
    invoke-static {v1}, Lcom/google/android/gms/playlog/store/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 255
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/playlog/store/f;->e:Lcom/google/android/gms/playlog/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/playlog/store/i;->a(Lcom/google/android/gms/playlog/store/h;)J

    move-result-wide v2

    .line 256
    iget-object v4, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/playlog/store/c;->a(J[B)I

    move-result v5

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "hash"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "play_logger_context_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "log_event"

    invoke-virtual {v6, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    iget-object v0, v4, Lcom/google/android/gms/playlog/store/c;->a:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v4, "log_event"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/playlog/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LogEventTable"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "INSERT: id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_1

    new-instance v0, Landroid/database/SQLException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "INSERT: id="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " loggerContextId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    :try_start_1
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Could not write to SQLite"

    invoke-direct {v2, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    :catchall_0
    move-exception v0

    .line 262
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_2

    .line 264
    throw v0

    .line 257
    :cond_1
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 262
    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_1

    .line 265
    return-void

    .line 263
    :catch_1
    move-exception v0

    .line 264
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could end transaction after writing to SQLite"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 263
    :catch_2
    move-exception v0

    .line 264
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could end transaction after writing to SQLite"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private e()J
    .locals 6

    .prologue
    .line 74
    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/playlog/store/f;->e:Lcom/google/android/gms/playlog/store/i;

    invoke-virtual {v2}, Lcom/google/android/gms/playlog/store/i;->a()Lcom/google/android/gms/playlog/store/j;

    move-result-object v2

    const-string v3, "SUM(LENGTH(play_logger_context))"

    invoke-virtual {v2}, Lcom/google/android/gms/playlog/store/j;->a()V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SELECT "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " FROM play_logger_context"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/gms/playlog/store/j;->d:Ljava/lang/String;

    if-eqz v4, :cond_0

    const-string v4, " WHERE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/google/android/gms/playlog/store/j;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v4, v2, Lcom/google/android/gms/playlog/store/j;->a:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/gms/playlog/store/j;->e:[Ljava/lang/String;

    invoke-static {v4, v3, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 77
    iget-object v2, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    invoke-virtual {v2}, Lcom/google/android/gms/playlog/store/c;->a()Lcom/google/android/gms/playlog/store/d;

    move-result-object v2

    const-string v3, "SUM(LENGTH(log_event))"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/playlog/store/d;->a(Ljava/lang/String;)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 79
    return-wide v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/playlog/store/h;Lcom/google/android/gms/playlog/internal/LogEvent;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 199
    new-instance v3, Lcom/google/af/a/d/a/a/j;

    invoke-direct {v3}, Lcom/google/af/a/d/a/a/j;-><init>()V

    .line 200
    iget-wide v0, p2, Lcom/google/android/gms/playlog/internal/LogEvent;->b:J

    iput-wide v0, v3, Lcom/google/af/a/d/a/a/j;->a:J

    .line 201
    iget-object v0, p2, Lcom/google/android/gms/playlog/internal/LogEvent;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p2, Lcom/google/android/gms/playlog/internal/LogEvent;->c:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    .line 204
    :cond_0
    iget-object v0, p2, Lcom/google/android/gms/playlog/internal/LogEvent;->d:[B

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p2, Lcom/google/android/gms/playlog/internal/LogEvent;->d:[B

    iput-object v0, v3, Lcom/google/af/a/d/a/a/j;->h:[B

    .line 207
    :cond_1
    iget-object v0, p2, Lcom/google/android/gms/playlog/internal/LogEvent;->e:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p2, Lcom/google/android/gms/playlog/internal/LogEvent;->e:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/af/a/d/a/a/k;

    iput-object v0, v3, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    .line 210
    iget-object v0, p2, Lcom/google/android/gms/playlog/internal/LogEvent;->e:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 211
    new-instance v5, Lcom/google/af/a/d/a/a/k;

    invoke-direct {v5}, Lcom/google/af/a/d/a/a/k;-><init>()V

    .line 213
    iput-object v0, v5, Lcom/google/af/a/d/a/a/k;->a:Ljava/lang/String;

    .line 214
    iget-object v6, p2, Lcom/google/android/gms/playlog/internal/LogEvent;->e:Landroid/os/Bundle;

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/google/af/a/d/a/a/k;->b:Ljava/lang/String;

    .line 215
    iget-object v6, v3, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    add-int/lit8 v0, v1, 0x1

    aput-object v5, v6, v1

    move v1, v0

    .line 216
    goto :goto_0

    .line 219
    :cond_2
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(Lcom/google/protobuf/nano/j;)I

    move-result v0

    new-array v0, v0, [B

    .line 220
    array-length v1, v0

    invoke-static {v0, v2, v1}, Lcom/google/protobuf/nano/b;->a([BII)Lcom/google/protobuf/nano/b;

    move-result-object v1

    .line 221
    invoke-virtual {v1, v3}, Lcom/google/protobuf/nano/b;->a(Lcom/google/protobuf/nano/j;)V

    .line 222
    iget-object v2, v1, Lcom/google/protobuf/nano/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-eqz v2, :cond_3

    .line 223
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not serialize proto: spaceLeft="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/protobuf/nano/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/playlog/store/f;->b(Lcom/google/android/gms/playlog/store/h;[B)V

    .line 228
    return-void
.end method

.method public final a(Lcom/google/android/gms/playlog/store/h;[B)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 232
    array-length v0, p2

    int-to-long v0, v0

    const-wide/32 v2, 0x100000

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 233
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Log entry exceeds size limits: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > 1048576"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 237
    :cond_0
    array-length v0, p2

    invoke-static {v0}, Lcom/google/protobuf/nano/b;->e(I)I

    move-result v0

    .line 238
    array-length v1, p2

    add-int/2addr v1, v0

    new-array v1, v1, [B

    .line 239
    array-length v2, v1

    invoke-static {v1, v5, v2}, Lcom/google/protobuf/nano/b;->a([BII)Lcom/google/protobuf/nano/b;

    move-result-object v2

    .line 240
    array-length v3, p2

    invoke-virtual {v2, v3}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 241
    iget-object v3, v2, Lcom/google/protobuf/nano/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    array-length v4, p2

    if-eq v3, v4, :cond_1

    .line 242
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Prepend too much bytes for length data: spaceLeft="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/protobuf/nano/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != proto.length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245
    :cond_1
    array-length v2, p2

    invoke-static {p2, v5, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 247
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/playlog/store/f;->b(Lcom/google/android/gms/playlog/store/h;[B)V

    .line 248
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/gms/playlog/store/f;->e()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/playlog/store/f;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 9

    .prologue
    const/16 v8, 0x200

    const/4 v0, 0x0

    .line 84
    iget-object v1, p0, Lcom/google/android/gms/playlog/store/f;->d:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 85
    invoke-static {v4}, Lcom/google/android/gms/playlog/store/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 89
    const/4 v1, 0x3

    :try_start_0
    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "hash"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "play_logger_context"

    aput-object v3, v1, v2

    .line 94
    iget-object v2, p0, Lcom/google/android/gms/playlog/store/f;->e:Lcom/google/android/gms/playlog/store/i;

    invoke-virtual {v2}, Lcom/google/android/gms/playlog/store/i;->a()Lcom/google/android/gms/playlog/store/j;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/playlog/store/j;->a([Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 96
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 97
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/gms/playlog/store/i;->a([BI)Z

    move-result v2

    if-nez v2, :cond_0

    .line 98
    iget-object v2, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    invoke-virtual {v2}, Lcom/google/android/gms/playlog/store/c;->a()Lcom/google/android/gms/playlog/store/d;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/playlog/store/d;->a(Ljava/lang/Long;)Lcom/google/android/gms/playlog/store/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/playlog/store/d;->a()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 102
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 134
    :catch_0
    move-exception v0

    .line 135
    :try_start_3
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not delete SQLite rows"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 137
    :catchall_1
    move-exception v0

    .line 138
    :try_start_4
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_2

    .line 140
    throw v0

    .line 102
    :cond_1
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 105
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "hash"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "play_logger_context_id"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "log_event"

    aput-object v3, v1, v2

    .line 111
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 112
    iget-object v3, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    invoke-virtual {v3}, Lcom/google/android/gms/playlog/store/c;->a()Lcom/google/android/gms/playlog/store/d;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/gms/playlog/store/d;->a([Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v1

    .line 114
    :cond_2
    :goto_1
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 115
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v3, 0x3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const/4 v5, 0x1

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v6, v7, v3, v5}, Lcom/google/android/gms/playlog/store/c;->a(J[BI)Z

    move-result v3

    if-nez v3, :cond_2

    .line 117
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_1

    .line 121
    :catchall_2
    move-exception v0

    :try_start_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 123
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v8, :cond_4

    move v1, v0

    .line 124
    :goto_2
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 125
    add-int/lit16 v3, v1, 0x200

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 126
    iget-object v5, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    invoke-virtual {v5}, Lcom/google/android/gms/playlog/store/c;->a()Lcom/google/android/gms/playlog/store/d;

    move-result-object v5

    invoke-virtual {v2, v1, v3}, Ljava/util/LinkedList;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/google/android/gms/playlog/store/d;->a(Ljava/util/List;)Lcom/google/android/gms/playlog/store/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/playlog/store/d;->a()I

    .line 124
    add-int/lit16 v1, v1, 0x200

    goto :goto_2

    .line 129
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    invoke-virtual {v1}, Lcom/google/android/gms/playlog/store/c;->a()Lcom/google/android/gms/playlog/store/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/playlog/store/d;->a(Ljava/util/List;)Lcom/google/android/gms/playlog/store/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/playlog/store/d;->a()I

    .line 132
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/playlog/store/f;->e:Lcom/google/android/gms/playlog/store/i;

    invoke-virtual {v1}, Lcom/google/android/gms/playlog/store/i;->b()I

    .line 133
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_7
    .catch Landroid/database/SQLException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 138
    :try_start_8
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_8
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_8} :catch_1

    .line 144
    invoke-direct {p0}, Lcom/google/android/gms/playlog/store/f;->e()J

    move-result-wide v2

    iget-wide v6, p0, Lcom/google/android/gms/playlog/store/f;->g:J

    sub-long/2addr v2, v6

    .line 145
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-gtz v1, :cond_7

    .line 194
    :cond_6
    :goto_3
    return-void

    .line 139
    :catch_1
    move-exception v0

    .line 140
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not end transaction after deleting SQLite rows"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 139
    :catch_2
    move-exception v0

    .line 140
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not end transaction after deleting SQLite rows"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 148
    :cond_7
    invoke-static {v4}, Lcom/google/android/gms/playlog/store/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 150
    const/4 v1, 0x2

    :try_start_9
    new-array v1, v1, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v1, v5

    const/4 v5, 0x1

    const-string v6, "LENGTH(log_event)"

    aput-object v6, v1, v5

    .line 154
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 155
    iget-object v6, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    invoke-virtual {v6}, Lcom/google/android/gms/playlog/store/c;->a()Lcom/google/android/gms/playlog/store/d;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/google/android/gms/playlog/store/d;->a([Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_9
    .catch Landroid/database/SQLException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    move-result-object v1

    .line 157
    :cond_8
    :try_start_a
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 158
    const/4 v6, 0x0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 159
    const/4 v6, 0x1

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    move-result-wide v6

    sub-long/2addr v2, v6

    .line 160
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-gtz v6, :cond_8

    .line 161
    :cond_9
    :try_start_b
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 167
    invoke-virtual {v5}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    .line 168
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-le v1, v8, :cond_a

    .line 169
    :goto_4
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_b

    .line 170
    add-int/lit16 v1, v0, 0x200

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 171
    iget-object v2, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    invoke-virtual {v2}, Lcom/google/android/gms/playlog/store/c;->a()Lcom/google/android/gms/playlog/store/d;

    move-result-object v2

    invoke-virtual {v5, v0, v1}, Ljava/util/LinkedList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/playlog/store/d;->a(Ljava/util/List;)Lcom/google/android/gms/playlog/store/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/playlog/store/d;->a()I

    .line 169
    add-int/lit16 v0, v0, 0x200

    goto :goto_4

    .line 165
    :catchall_3
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_b
    .catch Landroid/database/SQLException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 179
    :catch_3
    move-exception v0

    .line 180
    :try_start_c
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not delete more SQLite rows"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 182
    :catchall_4
    move-exception v0

    .line 183
    :try_start_d
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_d
    .catch Landroid/database/SQLException; {:try_start_d .. :try_end_d} :catch_5

    .line 185
    throw v0

    .line 174
    :cond_a
    :try_start_e
    iget-object v0, p0, Lcom/google/android/gms/playlog/store/f;->f:Lcom/google/android/gms/playlog/store/c;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/store/c;->a()Lcom/google/android/gms/playlog/store/d;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/playlog/store/d;->a(Ljava/util/List;)Lcom/google/android/gms/playlog/store/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/store/d;->a()I

    .line 177
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/playlog/store/f;->e:Lcom/google/android/gms/playlog/store/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/store/i;->b()I

    .line 178
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e
    .catch Landroid/database/SQLException; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 183
    :try_start_f
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_f
    .catch Landroid/database/SQLException; {:try_start_f .. :try_end_f} :catch_4

    .line 189
    invoke-direct {p0}, Lcom/google/android/gms/playlog/store/f;->e()J

    move-result-wide v0

    .line 190
    iget-wide v2, p0, Lcom/google/android/gms/playlog/store/f;->g:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    .line 191
    const-string v2, "LogStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not maintain storage below size limit: size="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " > limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/playlog/store/f;->g:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 184
    :catch_4
    move-exception v0

    .line 185
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not end transaction after deleting SQLite rows"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 184
    :catch_5
    move-exception v0

    .line 185
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not end transaction after deleting SQLite rows"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public close()V
    .locals 2

    .prologue
    .line 55
    sget-object v1, Lcom/google/android/gms/playlog/store/f;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    :try_start_0
    sget-boolean v0, Lcom/google/android/gms/playlog/store/f;->a:Z

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/gms/playlog/store/f;->c:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 57
    :cond_0
    :try_start_1
    sget v0, Lcom/google/android/gms/playlog/store/f;->c:I

    add-int/lit8 v0, v0, -0x1

    .line 58
    sput v0, Lcom/google/android/gms/playlog/store/f;->c:I

    if-nez v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/playlog/store/f;->d:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V

    .line 61
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final d()Lcom/google/android/gms/playlog/store/e;
    .locals 2

    .prologue
    .line 270
    new-instance v0, Lcom/google/android/gms/playlog/store/e;

    iget-object v1, p0, Lcom/google/android/gms/playlog/store/f;->d:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-direct {v0, v1}, Lcom/google/android/gms/playlog/store/e;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;)V

    return-object v0
.end method
