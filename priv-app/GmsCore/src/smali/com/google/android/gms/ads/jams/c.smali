.class public final Lcom/google/android/gms/ads/jams/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/ads/jams/d;

.field final c:Lcom/google/android/gms/common/util/p;

.field private final d:Landroid/accounts/AccountManager;

.field private final e:Lcom/google/android/gms/gcm/y;

.field private final f:Lcom/google/android/gms/ads/jams/a;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 97
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/ads/jams/d;

    invoke-direct {v3, p1}, Lcom/google/android/gms/ads/jams/d;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/ads/jams/a;

    invoke-direct {v5, p1}, Lcom/google/android/gms/ads/jams/a;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/ads/jams/c;-><init>(Landroid/content/Context;Landroid/accounts/AccountManager;Lcom/google/android/gms/ads/jams/d;Lcom/google/android/gms/gcm/y;Lcom/google/android/gms/ads/jams/a;Lcom/google/android/gms/common/util/p;)V

    .line 104
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/accounts/AccountManager;Lcom/google/android/gms/ads/jams/d;Lcom/google/android/gms/gcm/y;Lcom/google/android/gms/ads/jams/a;Lcom/google/android/gms/common/util/p;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/google/android/gms/ads/jams/c;->a:Landroid/content/Context;

    .line 114
    iput-object p2, p0, Lcom/google/android/gms/ads/jams/c;->d:Landroid/accounts/AccountManager;

    .line 115
    iput-object p3, p0, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    .line 116
    iput-object p4, p0, Lcom/google/android/gms/ads/jams/c;->e:Lcom/google/android/gms/gcm/y;

    .line 117
    iput-object p5, p0, Lcom/google/android/gms/ads/jams/c;->f:Lcom/google/android/gms/ads/jams/a;

    .line 118
    iput-object p6, p0, Lcom/google/android/gms/ads/jams/c;->c:Lcom/google/android/gms/common/util/p;

    .line 119
    return-void
.end method

.method static a(Lcom/google/l/a/a/b;Ljava/lang/String;)Lcom/google/l/a/a/c;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 255
    .line 259
    :try_start_0
    new-instance v0, Ljava/net/URL;

    const-string v1, "https://www.googleapis.com/adsmeasurement/v1/measurement/transmit"

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 260
    invoke-static {p0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v3

    .line 263
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    :try_start_1
    sget-object v1, Lcom/google/android/gms/ads/d;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setConnectTimeout(I)V

    .line 265
    sget-object v1, Lcom/google/android/gms/ads/d;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setReadTimeout(I)V

    .line 266
    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 267
    const-string v1, "Content-type"

    const-string v4, "application/x-protobuf"

    invoke-virtual {v0, v1, v4}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v1, "Content-Length"

    array-length v4, v3

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string v1, "Authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bearer "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljavax/net/ssl/HttpsURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setDoInput(Z)V

    .line 271
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setDoOutput(Z)V

    .line 274
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p0}, Lcom/google/l/a/a/b;->getSerializedSize()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 276
    const/4 v4, 0x0

    array-length v5, v3

    invoke-virtual {v1, v3, v4, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 277
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    .line 278
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 281
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v1

    const/16 v3, 0xc8

    if-ne v1, v3, :cond_4

    .line 282
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;)[B

    move-result-object v3

    .line 284
    new-instance v1, Lcom/google/l/a/a/c;

    invoke-direct {v1}, Lcom/google/l/a/a/c;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 285
    :try_start_2
    invoke-static {v1, v3}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 292
    :goto_0
    if-eqz v0, :cond_3

    .line 293
    invoke-virtual {v0}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    move-object v0, v1

    .line 297
    :cond_0
    :goto_1
    return-object v0

    .line 288
    :catch_0
    move-exception v0

    move-object v0, v2

    move-object v1, v2

    :goto_2
    if-eqz v0, :cond_1

    move-object v0, v2

    .line 292
    :cond_1
    if-eqz v1, :cond_0

    .line 293
    invoke-virtual {v1}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    goto :goto_1

    .line 292
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v2, :cond_2

    .line 293
    invoke-virtual {v2}, Ljavax/net/ssl/HttpsURLConnection;->disconnect()V

    :cond_2
    throw v0

    .line 292
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    .line 288
    :catch_1
    move-exception v1

    move-object v1, v0

    move-object v0, v2

    goto :goto_2

    :catch_2
    move-exception v3

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method final a()Landroid/accounts/Account;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/ads/jams/c;->d:Landroid/accounts/AccountManager;

    const-string v4, "com.google"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    .line 170
    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v0, v5, v4

    .line 171
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v8, "@google.com"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 186
    :goto_1
    return-object v0

    .line 170
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 177
    :cond_1
    array-length v0, v5

    if-ne v0, v2, :cond_5

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/ads/jams/c;->f:Lcom/google/android/gms/ads/jams/a;

    aget-object v4, v5, v3

    if-nez v4, :cond_2

    move-object v0, v1

    .line 181
    :goto_2
    if-eqz v0, :cond_5

    const-string v4, "HOSTED"

    iget-object v6, v0, Lcom/google/android/gms/auth/a/d;->b:Landroid/accounts/AccountManager;

    iget-object v0, v0, Lcom/google/android/gms/auth/a/d;->a:Landroid/accounts/Account;

    const-string v7, "services"

    invoke-virtual {v6, v0, v7}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v6, ","

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v0, v3

    :goto_3
    if-ge v0, v7, :cond_4

    aget-object v8, v6, v0

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v0, v2

    :goto_4
    if-nez v0, :cond_5

    .line 182
    aget-object v0, v5, v3

    goto :goto_1

    .line 178
    :cond_2
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/auth/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/a/d;

    move-result-object v0

    goto :goto_2

    .line 181
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    move v0, v3

    goto :goto_4

    :cond_5
    move-object v0, v1

    .line 186
    goto :goto_1
.end method

.method final a(J)V
    .locals 7

    .prologue
    .line 359
    sget-object v0, Lcom/google/android/gms/ads/d;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, p1

    .line 365
    iget-object v2, p0, Lcom/google/android/gms/ads/jams/c;->e:Lcom/google/android/gms/gcm/y;

    new-instance v3, Lcom/google/android/gms/gcm/an;

    invoke-direct {v3}, Lcom/google/android/gms/gcm/an;-><init>()V

    const-class v4, Lcom/google/android/gms/ads/jams/NegotiationService;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/an;

    move-result-object v3

    invoke-virtual {v3, p1, p2, v0, v1}, Lcom/google/android/gms/gcm/an;->a(JJ)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    const-string v1, "jams-negotiation-task"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/an;->a(Z)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/an;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    .line 373
    iget-object v0, p0, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    iget-object v1, p0, Lcom/google/android/gms/ads/jams/c;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    iget-object v0, v0, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "scheduled_next_negotiation_timestamp"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[JAMS] Next negotiation to run in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 377
    return-void
.end method

.method final b()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 201
    iget-object v1, p0, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/jams/d;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 214
    :goto_0
    return-object v0

    .line 208
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/ads/jams/c;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/jams/d;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "oauth2:https://www.googleapis.com/auth/ads_measurement"

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 212
    :catch_0
    move-exception v1

    .line 213
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[JAMS] Failed to get OAuth token: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final c()V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 219
    invoke-virtual {p0}, Lcom/google/android/gms/ads/jams/c;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const-string v0, "[JAMS] Ignore negotiation failure"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 225
    :goto_0
    return-void

    .line 224
    :cond_0
    sget-object v0, Lcom/google/android/gms/ads/d;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/ads/d;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v0, p0, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/d;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/d;->d()J

    move-result-wide v0

    :goto_1
    const-wide/16 v8, 0x14

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    long-to-double v4, v4

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    long-to-double v2, v2

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v2, v4

    long-to-double v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    double-to-long v2, v2

    const-string v4, "[JAMS] Scheduling retry..."

    invoke-static {v4}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/ads/jams/c;->a(J)V

    iget-object v2, p0, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/ads/jams/d;->a(J)V

    goto :goto_0

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method

.method final d()Z
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/gms/ads/jams/c;->b:Lcom/google/android/gms/ads/jams/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/jams/d;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    const-string v0, "[JAMS] Negotiation has been disabled"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 396
    const/4 v0, 0x1

    .line 399
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
