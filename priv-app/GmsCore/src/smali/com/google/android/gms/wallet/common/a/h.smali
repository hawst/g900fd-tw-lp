.class final Lcom/google/android/gms/wallet/common/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:[Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 1

    .prologue
    .line 1177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v0, v0, Lcom/google/t/a/b;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->b:Ljava/lang/String;

    .line 1179
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v0, v0, Lcom/google/t/a/b;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->a:Ljava/lang/String;

    .line 1180
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v0, v0, Lcom/google/t/a/b;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->c:Ljava/lang/String;

    .line 1181
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v0, v0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    .line 1182
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/checkout/inapp/proto/a/b;B)V
    .locals 0

    .prologue
    .line 1171
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/a/h;-><init>(Lcom/google/checkout/inapp/proto/a/b;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1207
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 1241
    :cond_0
    :goto_0
    return v1

    .line 1209
    :cond_1
    if-eqz p1, :cond_0

    .line 1211
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v0, v3, :cond_0

    .line 1213
    check-cast p1, Lcom/google/android/gms/wallet/common/a/h;

    .line 1214
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1215
    iget-object v0, p1, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1226
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->b:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 1227
    iget-object v0, p1, Lcom/google/android/gms/wallet/common/a/h;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1231
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->a:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 1232
    iget-object v0, p1, Lcom/google/android/gms/wallet/common/a/h;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1236
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->c:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 1237
    iget-object v0, p1, Lcom/google/android/gms/wallet/common/a/h;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    :cond_5
    move v1, v2

    .line 1241
    goto :goto_0

    .line 1217
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    array-length v0, v0

    iget-object v3, p1, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    array-length v3, v3

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 1220
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1221
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    aget-object v3, v3, v0

    iget-object v4, p1, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1220
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1229
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/wallet/common/a/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    .line 1234
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/wallet/common/a/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    .line 1239
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/wallet/common/a/h;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1186
    const/4 v0, 0x1

    move v2, v0

    move v0, v1

    .line 1189
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1190
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_0

    move v2, v1

    :goto_1
    add-int/2addr v2, v3

    .line 1189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1190
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/a/h;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 1194
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1197
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1199
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/a/h;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 1202
    return v0

    .line 1194
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->b:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1197
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/h;->a:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1199
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/a/h;->c:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method
