.class public final Lcom/google/android/gms/drive/internal/model/File$Thumbnail;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/drive/internal/model/p;

.field private static final e:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2577
    new-instance v0, Lcom/google/android/gms/drive/internal/model/p;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/p;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->CREATOR:Lcom/google/android/gms/drive/internal/model/p;

    .line 2592
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2595
    sput-object v0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->e:Ljava/util/HashMap;

    const-string v1, "image"

    const-string v2, "image"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2596
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->e:Ljava/util/HashMap;

    const-string v1, "mimeType"

    const-string v2, "mimeType"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2597
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2628
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 2629
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->b:I

    .line 2630
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a:Ljava/util/Set;

    .line 2631
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2639
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 2640
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a:Ljava/util/Set;

    .line 2641
    iput p2, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->b:I

    .line 2642
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->c:Ljava/lang/String;

    .line 2643
    iput-object p4, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->d:Ljava/lang/String;

    .line 2644
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 2601
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2774
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 2775
    packed-switch v0, :pswitch_data_0

    .line 2783
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2777
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->c:Ljava/lang/String;

    .line 2786
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2787
    return-void

    .line 2780
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->d:Ljava/lang/String;

    goto :goto_0

    .line 2775
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 2732
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2737
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2743
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2739
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->c:Ljava/lang/String;

    .line 2741
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->d:Ljava/lang/String;

    goto :goto_0

    .line 2737
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2722
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2727
    const/4 v0, 0x0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2711
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->CREATOR:Lcom/google/android/gms/drive/internal/model/p;

    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File$Thumbnail;
    .locals 2

    .prologue
    .line 2694
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->c:Ljava/lang/String;

    .line 2695
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2696
    return-object p0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2804
    instance-of v0, p1, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    if-nez v0, :cond_0

    move v0, v1

    .line 2835
    :goto_0
    return v0

    .line 2809
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 2810
    goto :goto_0

    .line 2813
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    .line 2814
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2815
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2816
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2818
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2820
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2825
    goto :goto_0

    .line 2828
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2830
    goto :goto_0

    :cond_5
    move v0, v2

    .line 2835
    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File$Thumbnail;
    .locals 2

    .prologue
    .line 2703
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->d:Ljava/lang/String;

    .line 2704
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2705
    return-object p0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2791
    const/4 v0, 0x0

    .line 2792
    sget-object v1, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2793
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2794
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 2795
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 2797
    goto :goto_0

    .line 2798
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2716
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->CREATOR:Lcom/google/android/gms/drive/internal/model/p;

    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/model/p;->a(Lcom/google/android/gms/drive/internal/model/File$Thumbnail;Landroid/os/Parcel;)V

    .line 2717
    return-void
.end method
