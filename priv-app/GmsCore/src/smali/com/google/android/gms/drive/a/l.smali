.class public final Lcom/google/android/gms/drive/a/l;
.super Lcom/google/android/gms/drive/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/a/w;


# instance fields
.field c:Lcom/google/android/gms/drive/DriveId;

.field public d:Lcom/google/android/gms/drive/DriveId;

.field e:Ljava/lang/Long;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private final h:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/l;)V
    .locals 11

    .prologue
    .line 86
    sget-object v10, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/drive/a/l;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/l;Lcom/google/android/gms/drive/a/ac;)V

    .line 88
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/l;Lcom/google/android/gms/drive/a/ac;)V
    .locals 7

    .prologue
    .line 94
    sget-object v1, Lcom/google/android/gms/drive/a/e;->f:Lcom/google/android/gms/drive/a/e;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p9

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;Lcom/google/android/gms/drive/a/a/l;)V

    .line 96
    iput-object p3, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    .line 97
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 98
    iput-wide p5, p0, Lcom/google/android/gms/drive/a/l;->h:J

    .line 99
    iput-object p7, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    .line 100
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 104
    sget-object v0, Lcom/google/android/gms/drive/a/e;->f:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 105
    const-string v0, "metadata"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 107
    const-string v0, "contentId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    .line 108
    const-string v0, "writeOpenKey"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/l;->h:J

    .line 109
    const-string v0, "parent"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    const-string v0, "parent"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    .line 114
    :goto_0
    const-string v0, "newDriveId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    const-string v0, "newDriveId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    .line 117
    :cond_0
    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    .line 121
    :cond_1
    return-void

    .line 112
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/l;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Z)V
    .locals 9

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v1

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p6, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p5}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/d/a/c;->a(Ljava/lang/String;Z)Lcom/google/android/gms/drive/internal/model/FileLocalId;

    move-result-object v0

    .line 292
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/internal/model/File;->a(Lcom/google/android/gms/drive/internal/model/FileLocalId;)Lcom/google/android/gms/drive/internal/model/File;

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/model/File;->toString()Ljava/lang/String;

    move-result-object v5

    const/16 v0, 0x199

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object v0, p5

    move-object v1, p2

    move v6, p6

    move-object v7, p4

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/drive/b/b/n;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/g/aw;JLjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/drive/auth/g;Ljava/lang/Integer;)Lcom/google/android/gms/drive/b/b/n;

    move-result-object v0

    .line 299
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->B()Lcom/google/android/gms/drive/b/b/p;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p2, v2}, Lcom/google/android/gms/drive/b/b/p;->a(Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/g/aw;Z)Lcom/google/android/gms/drive/b/b/o;

    move-result-object v0

    .line 301
    :try_start_0
    new-instance v1, Lcom/google/android/gms/drive/b/b/f;

    invoke-direct {v1}, Lcom/google/android/gms/drive/b/b/f;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/b/b/o;->a(Lcom/google/android/gms/drive/b/b/l;)Lcom/google/android/gms/drive/d/e;

    move-result-object v0

    .line 303
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v1}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/l;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    invoke-interface {p3, v1, v2}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/drive/d/e;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/drive/d/e;->b:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gms/drive/d/e;->c:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/gms/drive/d/e;->e:Ljava/lang/String;

    iget-object v6, v0, Lcom/google/android/gms/drive/d/e;->d:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/model/ah;->c(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v1, v3}, Lcom/google/android/gms/drive/database/model/ah;->d(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/drive/database/model/ah;->f(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/drive/ai;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :cond_1
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->j(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1, v4}, Lcom/google/android/gms/drive/database/model/ah;->g(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Lcom/google/android/gms/drive/database/model/ah;->m(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lcom/google/android/gms/drive/database/model/ah;->h(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->m(Z)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 319
    :goto_1
    return-void

    .line 303
    :cond_3
    const-string v7, "CreateFileAction"

    const-string v8, "ResourceId has already been set, probably by a metadata feed, so no need to set it again."

    invoke-static {v7, v8}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/drive/b/b/j; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 304
    :catch_0
    move-exception v0

    .line 305
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 306
    :catch_1
    move-exception v0

    .line 309
    if-eqz p6, :cond_5

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    const-string v1, "Conflict should only happen while creating a singleton file"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/l;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p2, p4, p5}, Lcom/google/android/gms/drive/a/l;->a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)V

    :cond_4
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p5}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v2

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v0}, Lcom/google/android/gms/drive/database/model/ah;->c(Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/a/l;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Z)V

    goto :goto_1

    .line 314
    :cond_5
    const-string v1, "CreateFileAction"

    const-string v2, "Unexpected UploadConflictException"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 315
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 319
    :catch_2
    move-exception v0

    .line 318
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 324
    const-string v0, "CreateFileAction"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Conflict detected in applyOnServer before upload, driveId:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    invoke-direct {v1, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    .line 332
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, p2, v0, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;ZZ)V

    .line 334
    const-string v0, "CreateFileAction"

    const-string v1, "Completed syncing entry after conflict detection"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 348
    new-instance v0, Lcom/google/android/gms/drive/a/i;

    invoke-direct {v0}, Lcom/google/android/gms/drive/a/i;-><init>()V

    .line 349
    throw v0

    .line 335
    :catch_0
    move-exception v0

    .line 336
    const-string v1, "CreateFileAction"

    const-string v2, "Failed to sync entry after conflict detection."

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 339
    throw v0

    .line 340
    :catch_1
    move-exception v0

    .line 343
    const-string v1, "CreateFileAction"

    const-string v2, "Failed to sync entry after conflict detection."

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 344
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 159
    if-nez v0, :cond_0

    .line 161
    new-instance v0, Lcom/google/android/gms/drive/a/ab;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/a/ab;-><init>(Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    throw v0

    :cond_0
    move-object v8, v0

    .line 170
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    iget-wide v4, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-virtual {v8}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v6

    move-object v1, p1

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_2

    .line 178
    new-instance v1, Lcom/google/android/gms/drive/a/t;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/a/t;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    throw v1

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    const-string v1, "root"

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    move-object v8, v0

    goto :goto_0

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 184
    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 185
    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-interface {p1}, Lcom/google/android/gms/drive/database/r;->u()Lcom/google/android/gms/drive/database/t;

    move-result-object v3

    invoke-interface {p1, v2, v0, v1, v3}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/t;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v10

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_3

    move v0, v9

    :goto_1
    const-string v2, "Content does not exist: %s"

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    aput-object v4, v3, v7

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 189
    iget-wide v0, v1, Lcom/google/android/gms/drive/database/model/ax;->h:J

    invoke-virtual {v10, v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(J)V

    .line 193
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v10, v0, v1}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 196
    invoke-virtual {v10, v7}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 197
    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    invoke-virtual {v10}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v4

    iget-object v5, p2, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v6, p0, Lcom/google/android/gms/drive/a/l;->h:J

    move-object v0, p1

    move-object v2, v11

    move-object v3, v11

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/drive/database/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)Lcom/google/android/gms/drive/database/model/bi;

    move-result-object v0

    .line 206
    invoke-virtual {v8}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    invoke-interface {p1, v10, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/am;

    move-result-object v1

    .line 208
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/am;->i()V

    .line 211
    invoke-virtual {v8}, Lcom/google/android/gms/drive/database/model/ah;->C()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/Long;)V

    .line 214
    iget-wide v2, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {p1, v10, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;Ljava/util/Set;)V

    .line 217
    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    invoke-virtual {v10, v1}, Lcom/google/android/gms/drive/database/model/ah;->i(Ljava/lang/String;)V

    .line 218
    invoke-virtual {v10, v9}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 220
    invoke-virtual {v10}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    .line 221
    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    .line 222
    new-instance v1, Lcom/google/android/gms/drive/a/am;

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p2, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v5, p2, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/l;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/drive/a/am;-><init>(JLcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    return-object v1

    :cond_3
    move v0, v7

    .line 188
    goto :goto_1
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;)V
    .locals 7

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 246
    new-instance v1, Ljava/util/HashSet;

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    :cond_0
    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 252
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v3

    .line 253
    invoke-super {p0, v3}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v4

    .line 254
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/l;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v5

    .line 258
    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/l;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    invoke-direct {p0, p2, v4, v5}, Lcom/google/android/gms/drive/a/l;->a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)V

    .line 268
    :goto_0
    return-void

    .line 263
    :cond_2
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/a/l;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Z)V

    goto :goto_0

    .line 266
    :cond_3
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/a/l;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Z)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/drive/a/c;)Z
    .locals 1

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/a/c;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/drive/a/l;->b(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/drive/g/aw;)V
    .locals 4

    .prologue
    .line 272
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/drive/database/r;->b(J)Lcom/google/android/gms/drive/database/model/bi;

    move-result-object v0

    .line 274
    if-nez v0, :cond_0

    .line 275
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v0

    const-string v1, "CreateFileAction"

    const-string v2, "PendingUpload should not be null"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->b(Lcom/google/android/gms/drive/g/aw;)V

    .line 282
    return-void

    .line 278
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bi;->j()V

    .line 279
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->v()Lcom/google/android/gms/drive/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/d;->b()V

    goto :goto_0
.end method

.method protected final c(Lcom/google/android/gms/drive/g/aw;)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/a/l;->a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 234
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    .line 236
    :cond_0
    return-void
.end method

.method public final bridge synthetic d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final bridge synthetic e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method protected final e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 464
    if-ne p0, p1, :cond_1

    .line 472
    :cond_0
    :goto_0
    return v0

    .line 467
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 468
    goto :goto_0

    .line 471
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/a/l;

    .line 472
    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    iget-object v1, p1, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    iget-object v1, p1, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    iget-object v1, p1, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_6
    iget-object v2, p1, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final f()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 448
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->f()Lorg/json/JSONObject;

    move-result-object v0

    .line 449
    const-string v1, "contentId"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 450
    const-string v1, "metadata"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/a/ax;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 451
    const-string v1, "writeOpenKey"

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/l;->h:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 452
    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_0

    .line 453
    const-string v1, "parent"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 455
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_1

    .line 456
    const-string v1, "newDriveId"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 458
    :cond_1
    const-string v1, "pendingUploadSqlId"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 459
    return-object v0
.end method

.method public final bridge synthetic g()Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->g()Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 487
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 488
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 489
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 490
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 491
    return v0

    :cond_1
    move v0, v1

    .line 488
    goto :goto_0

    :cond_2
    move v0, v1

    .line 489
    goto :goto_1
.end method

.method public final j()Lcom/google/android/gms/drive/database/model/EntrySpec;
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 427
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/util/Set;
    .locals 4

    .prologue
    .line 438
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 439
    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_0

    .line 441
    iget-object v1, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 443
    :cond_0
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 496
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CreateFileAction[%s, pendingUploadSqlId=%d, initialMetadata=%s, parent=%s, newDriveId=%s, contentHash=%s]"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/l;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/a/l;->e:Ljava/lang/Long;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/drive/a/l;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/gms/drive/a/l;->c:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/gms/drive/a/l;->d:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/gms/drive/a/l;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
