.class public final Lcom/google/android/gms/plus/plusone/c;
.super Lcom/google/android/gms/plus/plusone/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;


# instance fields
.field h:Lcom/google/android/gms/plus/plusone/d;

.field private final i:Lcom/google/android/gms/plus/internal/ad;

.field private j:Lcom/google/android/gms/plus/internal/ab;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;)V
    .locals 6

    .prologue
    .line 62
    sget-object v5, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/plusone/c;-><init>(Landroid/content/Context;IILjava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V

    .line 63
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;IILjava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V
    .locals 6

    .prologue
    .line 72
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/plusone/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V

    .line 57
    new-instance v0, Lcom/google/android/gms/plus/plusone/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/d;-><init>(Lcom/google/android/gms/plus/plusone/c;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->h:Lcom/google/android/gms/plus/plusone/d;

    .line 73
    new-instance v0, Lcom/google/android/gms/plus/internal/cn;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/cn;->a()Lcom/google/android/gms/plus/internal/cn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    .line 74
    iput-object p5, p0, Lcom/google/android/gms/plus/plusone/c;->i:Lcom/google/android/gms/plus/internal/ad;

    .line 75
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/c;->i:Lcom/google/android/gms/plus/internal/ad;

    invoke-interface {v1, p1, v0, p0, p0}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/common/es;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/common/et;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/c;->a(Lcom/google/android/gms/plus/internal/ab;)V

    .line 79
    return-void
.end method


# virtual methods
.method public final T_()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/c;->h:Lcom/google/android/gms/plus/plusone/d;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/c;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V

    .line 106
    :cond_0
    return-void
.end method

.method public final W_()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 3

    .prologue
    .line 110
    const-string v0, "PlusOneButtonView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to establish connection with status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/c;->e()V

    .line 112
    return-void
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lcom/google/android/gms/plus/plusone/a;->onAttachedToWindow()V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 89
    :cond_0
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lcom/google/android/gms/plus/plusone/a;->onDetachedFromWindow()V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/c;->j:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 99
    :cond_1
    return-void
.end method
