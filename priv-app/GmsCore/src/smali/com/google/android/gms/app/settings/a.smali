.class final Lcom/google/android/gms/app/settings/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/gms/app/settings/DataManagementActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/app/settings/DataManagementActivity;Z)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/gms/app/settings/a;->b:Lcom/google/android/gms/app/settings/DataManagementActivity;

    iput-boolean p2, p0, Lcom/google/android/gms/app/settings/a;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 3

    .prologue
    .line 110
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/app/settings/a;->b:Lcom/google/android/gms/app/settings/DataManagementActivity;

    iget-boolean v1, p0, Lcom/google/android/gms/app/settings/a;->a:Z

    invoke-static {v0, v1}, Lcom/google/android/gms/app/settings/DataManagementActivity;->a(Lcom/google/android/gms/app/settings/DataManagementActivity;Z)V

    iget-object v0, p0, Lcom/google/android/gms/app/settings/a;->b:Lcom/google/android/gms/app/settings/DataManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/DataManagementActivity;->a(Lcom/google/android/gms/app/settings/DataManagementActivity;)Lcom/google/android/gms/common/widget/a/o;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/o;->a(Z)V

    const-string v0, "DataManagementActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to set preferences, result "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/app/settings/a;->b:Lcom/google/android/gms/app/settings/DataManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/DataManagementActivity;->b(Lcom/google/android/gms/app/settings/DataManagementActivity;)V

    :cond_0
    return-void
.end method
