.class public final Lcom/google/android/gms/smart_profile/m;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/people/identity/models/Person;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 30
    if-nez p0, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 33
    :cond_1
    invoke-interface {p0}, Lcom/google/android/gms/people/identity/models/Person;->q()Ljava/util/List;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 39
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/k;

    .line 40
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 41
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->e()Lcom/google/android/gms/people/identity/models/l;

    move-result-object v0

    .line 42
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "cp2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 44
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_2
    move v1, v0

    .line 47
    goto :goto_1

    :cond_2
    move v0, v1

    .line 48
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 83
    if-nez p2, :cond_0

    .line 95
    :goto_0
    return-void

    .line 86
    :cond_0
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 88
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 89
    const-string v3, "starred"

    if-eqz p1, :cond_1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 90
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1, v2, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 89
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
