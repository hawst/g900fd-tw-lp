.class public final Lcom/google/android/gms/wallet/common/ui/cj;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cb;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/ImageView;

.field e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private f:Lcom/google/checkout/inapp/proto/j;

.field private g:Lcom/google/android/gms/wallet/shared/common/b/a;

.field private h:Z

.field private i:Lcom/google/android/gms/wallet/common/ui/bh;

.field private j:Lcom/google/android/gms/wallet/common/ui/bi;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->h:Z

    return-void
.end method

.method public static a(Lcom/google/checkout/inapp/proto/j;)Lcom/google/android/gms/wallet/common/ui/cj;
    .locals 3

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cj;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/cj;-><init>()V

    .line 61
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 62
    const-string v2, "com.google.android.gms.wallet.instrument"

    invoke-static {v1, v2, p0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 63
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cj;->setArguments(Landroid/os/Bundle;)V

    .line 65
    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/cj;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/cj;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/cj;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 192
    return-void
.end method

.method private b(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 153
    const/4 v0, 0x3

    new-array v4, v0, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->i:Lcom/google/android/gms/wallet/common/ui/bh;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->j:Lcom/google/android/gms/wallet/common/ui/bi;

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v3, v4, v0

    .line 155
    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 157
    if-eqz v6, :cond_0

    .line 158
    if-eqz p1, :cond_2

    .line 161
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    .line 155
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 161
    goto :goto_1

    .line 162
    :cond_2
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 166
    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/google/checkout/a/a/a/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 201
    new-instance v2, Lcom/google/checkout/a/a/a/b;

    invoke-direct {v2}, Lcom/google/checkout/a/a/a/b;-><init>()V

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    new-instance v0, Lcom/google/t/a/b;

    invoke-direct {v0}, Lcom/google/t/a/b;-><init>()V

    iput-object v0, v2, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    .line 204
    iget-object v0, v2, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/t/a/b;->s:Ljava/lang/String;

    .line 209
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 215
    :goto_0
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit16 v3, v3, 0x7d0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 220
    :goto_1
    if-eqz v0, :cond_1

    .line 221
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Lcom/google/checkout/a/a/a/b;->b:I

    .line 223
    :cond_1
    if-eqz v1, :cond_2

    .line 224
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Lcom/google/checkout/a/a/a/b;->c:I

    .line 227
    :cond_2
    new-instance v0, Lcom/google/checkout/a/a/a/d;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/d;-><init>()V

    .line 228
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/checkout/a/a/a/d;->a:I

    .line 229
    iput-object v2, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    .line 230
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 184
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/cj;->h:Z

    .line 185
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/cj;->b()V

    .line 186
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cj;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cj;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 171
    const/4 v2, 0x3

    new-array v3, v2, [Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    .line 172
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 173
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 174
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 175
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    .line 179
    :goto_1
    return v0

    .line 172
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 179
    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cj;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 74
    const-string v1, "com.google.android.gms.wallet.instrument"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Fragment requires instrument extra!"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 76
    const-string v1, "com.google.android.gms.wallet.instrument"

    const-class v2, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->f:Lcom/google/checkout/inapp/proto/j;

    .line 79
    if-eqz p1, :cond_0

    .line 80
    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->h:Z

    .line 83
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->g:Lcom/google/android/gms/wallet/shared/common/b/a;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->g:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/image/a;->a(Landroid/content/Context;)Lcom/google/android/gms/wallet/dynamite/image/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Lcom/google/android/gms/wallet/dynamite/image/a;)V

    .line 85
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 90
    sget v0, Lcom/google/android/gms/l;->gC:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 93
    sget v0, Lcom/google/android/gms/j;->cy:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->f:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->f:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->f:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v0, v0, Lcom/google/t/a/b;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->f:Lcom/google/checkout/inapp/proto/j;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v2, v2, Lcom/google/t/a/b;->s:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    .line 99
    :cond_0
    sget v0, Lcom/google/android/gms/j;->cx:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->c:Landroid/widget/TextView;

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->f:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/j;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    sget v0, Lcom/google/android/gms/j;->cz:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->d:Landroid/widget/ImageView;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->d:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->f:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cj;->g:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/widget/ImageView;Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/common/b/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->d:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->f:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/j;)Lcom/google/aa/b/a/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/aa/b/a/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 113
    :goto_0
    sget v0, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 114
    sget v0, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 116
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bg;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/wallet/common/ui/bg;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    .line 118
    new-instance v2, Lcom/google/android/gms/wallet/common/ui/bh;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/gms/wallet/common/ui/bh;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/dn;)V

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->i:Lcom/google/android/gms/wallet/common/ui/bh;

    .line 119
    new-instance v2, Lcom/google/android/gms/wallet/common/ui/bi;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/wallet/common/ui/bi;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/dn;)V

    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->j:Lcom/google/android/gms/wallet/common/ui/bi;

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->i:Lcom/google/android/gms/wallet/common/ui/bh;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cj;->i:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v2, v3, v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->j:Lcom/google/android/gms/wallet/common/ui/bi;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cj;->j:Lcom/google/android/gms/wallet/common/ui/bi;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->i:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->j:Lcom/google/android/gms/wallet/common/ui/bi;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cj;->i:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v2, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v2, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusUpId(I)V

    .line 136
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/cj;->b()V

    .line 138
    return-object v1

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cj;->d:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 235
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 237
    const-string v0, "enabled"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/cj;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 238
    return-void
.end method
