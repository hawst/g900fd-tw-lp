.class public Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;
.super Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
.source "SourceFile"


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 51
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 44
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->b:Landroid/view/View;

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 45
    return-void

    .line 44
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->onFinishInflate()V

    .line 31
    sget v0, Lcom/google/android/gms/j;->bg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->b:Landroid/view/View;

    .line 32
    sget v0, Lcom/google/android/gms/j;->bd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->c:Landroid/widget/ImageView;

    .line 33
    return-void
.end method

.method public setChecked(Z)V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->setChecked(Z)V

    .line 38
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListSuggestedPersonView;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 39
    return-void

    .line 38
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
