.class public Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# instance fields
.field a:Landroid/widget/TextView;

.field public b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/gms/wallet/common/ui/cn;

.field private e:Lcom/google/android/gms/wallet/common/ui/cm;

.field private f:Lcom/google/android/gms/wallet/common/ui/co;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 60
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 61
    sget v1, Lcom/google/android/gms/l;->hy:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 63
    sget v0, Lcom/google/android/gms/j;->ny:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/cl;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/common/ui/cl;-><init>(Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 75
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/checkout/inapp/proto/a/d;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->c:Ljava/lang/String;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/d;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/d;Lcom/google/checkout/inapp/proto/a/d;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 102
    if-nez p2, :cond_0

    move-object p2, p1

    .line 105
    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-wide v2, p2, Lcom/google/checkout/inapp/proto/a/d;->a:J

    iget-wide v4, p1, Lcom/google/checkout/inapp/proto/a/d;->a:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->BY:I

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->d:Lcom/google/android/gms/wallet/common/ui/cn;

    if-eqz v1, :cond_2

    .line 112
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->d:Lcom/google/android/gms/wallet/common/ui/cn;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 114
    :cond_2
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/cn;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->Bj:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/wallet/common/ui/cn;-><init>(Ljava/lang/CharSequence;Lcom/google/checkout/inapp/proto/a/d;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->d:Lcom/google/android/gms/wallet/common/ui/cn;

    .line 116
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->d:Lcom/google/android/gms/wallet/common/ui/cn;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 118
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->e:Lcom/google/android/gms/wallet/common/ui/cm;

    if-eqz v1, :cond_3

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->e:Lcom/google/android/gms/wallet/common/ui/cm;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 121
    :cond_3
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/cm;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->Bk:I

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p2}, Lcom/google/android/gms/wallet/common/ui/cm;-><init>(Ljava/lang/CharSequence;Lcom/google/checkout/inapp/proto/a/d;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->e:Lcom/google/android/gms/wallet/common/ui/cm;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->e:Lcom/google/android/gms/wallet/common/ui/cm;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->f:Lcom/google/android/gms/wallet/common/ui/co;

    if-eqz v0, :cond_4

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->f:Lcom/google/android/gms/wallet/common/ui/co;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 129
    :cond_4
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/co;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/co;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->f:Lcom/google/android/gms/wallet/common/ui/co;

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->f:Lcom/google/android/gms/wallet/common/ui/co;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 133
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->c:Ljava/lang/String;

    .line 83
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    sget v0, Lcom/google/android/gms/j;->nw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a:Landroid/widget/TextView;

    .line 90
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    return-void

    .line 87
    :cond_0
    sget v0, Lcom/google/android/gms/j;->nx:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->a:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    .line 158
    if-nez v0, :cond_0

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    .line 161
    :cond_0
    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    .line 174
    const/4 v0, 0x1

    .line 176
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/MoneyAmountInputView;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 153
    return-void
.end method
