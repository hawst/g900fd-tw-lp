.class public final enum Lcom/google/android/gms/drive/database/model/aw;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/aw;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/aw;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/aw;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/aw;

.field private static final synthetic f:[Lcom/google/android/gms/drive/database/model/aw;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/google/android/gms/drive/database/model/aw;

    const-string v1, "INTEGER"

    const-string v2, "value_int"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/gms/drive/database/model/aw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    .line 32
    new-instance v0, Lcom/google/android/gms/drive/database/model/aw;

    const-string v1, "REAL"

    const-string v2, "value_real"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/database/model/aw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/aw;->b:Lcom/google/android/gms/drive/database/model/aw;

    .line 33
    new-instance v0, Lcom/google/android/gms/drive/database/model/aw;

    const-string v1, "TEXT"

    const-string v2, "value_txt"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/gms/drive/database/model/aw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    .line 34
    new-instance v0, Lcom/google/android/gms/drive/database/model/aw;

    const-string v1, "BLOB"

    const-string v2, "value_blob"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/database/model/aw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/aw;->d:Lcom/google/android/gms/drive/database/model/aw;

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/aw;

    sget-object v1, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/database/model/aw;->b:Lcom/google/android/gms/drive/database/model/aw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/drive/database/model/aw;->d:Lcom/google/android/gms/drive/database/model/aw;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/drive/database/model/aw;->f:[Lcom/google/android/gms/drive/database/model/aw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-object p3, p0, Lcom/google/android/gms/drive/database/model/aw;->e:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/aw;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/gms/drive/database/model/aw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/aw;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/aw;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/drive/database/model/aw;->f:[Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/aw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/aw;

    return-object v0
.end method
