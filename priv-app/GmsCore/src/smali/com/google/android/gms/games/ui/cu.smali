.class final Lcom/google/android/gms/games/ui/cu;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Landroid/support/v7/widget/cs;

.field final synthetic c:Landroid/view/ViewPropertyAnimator;

.field final synthetic d:Lcom/google/android/gms/games/ui/cs;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/cs;Landroid/view/View;Landroid/support/v7/widget/cs;Landroid/view/ViewPropertyAnimator;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/gms/games/ui/cu;->d:Lcom/google/android/gms/games/ui/cs;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/cu;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/cu;->b:Landroid/support/v7/widget/cs;

    iput-object p4, p0, Lcom/google/android/gms/games/ui/cu;->c:Landroid/view/ViewPropertyAnimator;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cu;->d:Lcom/google/android/gms/games/ui/cs;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cu;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/cs;->a(Landroid/view/View;)V

    .line 88
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cu;->c:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cu;->d:Lcom/google/android/gms/games/ui/cs;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cu;->b:Landroid/support/v7/widget/cs;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/cs;->f(Landroid/support/v7/widget/cs;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cu;->d:Lcom/google/android/gms/games/ui/cs;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/cs;->p:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cu;->b:Landroid/support/v7/widget/cs;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cu;->d:Lcom/google/android/gms/games/ui/cs;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/cs;->a(Lcom/google/android/gms/games/ui/cs;)V

    .line 96
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cu;->a:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cu;->d:Lcom/google/android/gms/games/ui/cs;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cu;->b:Landroid/support/v7/widget/cs;

    .line 84
    return-void
.end method
