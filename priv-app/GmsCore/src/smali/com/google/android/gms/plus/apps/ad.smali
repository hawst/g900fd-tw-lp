.class public final Lcom/google/android/gms/plus/apps/ad;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private j:Lcom/google/android/gms/plus/apps/ae;

.field private k:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 25
    return-void
.end method

.method static a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/apps/ad;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 41
    const-string v1, "message"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 42
    new-instance v1, Lcom/google/android/gms/plus/apps/ad;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/ad;-><init>()V

    .line 43
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/ad;->setArguments(Landroid/os/Bundle;)V

    .line 44
    return-object v1
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ad;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ad;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/google/android/gms/l;->dI:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ad;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onAttach(Landroid/app/Activity;)V

    .line 76
    instance-of v0, p1, Lcom/google/android/gms/plus/apps/ae;

    if-eqz v0, :cond_0

    .line 77
    check-cast p1, Lcom/google/android/gms/plus/apps/ae;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ad;->j:Lcom/google/android/gms/plus/apps/ae;

    .line 79
    :cond_0
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ad;->j:Lcom/google/android/gms/plus/apps/ae;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ad;->j:Lcom/google/android/gms/plus/apps/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/apps/ae;->a()V

    .line 102
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ad;->j:Lcom/google/android/gms/plus/apps/ae;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ad;->j:Lcom/google/android/gms/plus/apps/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/apps/ae;->a()V

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ad;->dismiss()V

    .line 95
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ad;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 51
    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ad;->k:Ljava/lang/CharSequence;

    .line 52
    return-void
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Landroid/support/v4/app/m;->onDetach()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ad;->j:Lcom/google/android/gms/plus/apps/ae;

    .line 85
    return-void
.end method
