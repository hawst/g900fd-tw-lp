.class public abstract Lcom/google/android/gms/ads/internal/util/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Ljava/lang/Runnable;

.field volatile e:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Lcom/google/android/gms/ads/internal/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/util/b;-><init>(Lcom/google/android/gms/ads/internal/util/a;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/util/a;->a:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract b()V
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/a;->a:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/c;->a(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 29
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/util/a;->b()V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/a;->e:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/a;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 42
    :cond_0
    return-void
.end method
