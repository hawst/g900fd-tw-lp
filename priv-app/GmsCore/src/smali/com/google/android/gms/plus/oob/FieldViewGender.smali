.class public final Lcom/google/android/gms/plus/oob/FieldViewGender;
.super Lcom/google/android/gms/plus/oob/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/oob/t;


# instance fields
.field private d:Lcom/google/android/gms/plus/oob/s;

.field private e:Lcom/google/android/gms/plus/oob/GenderSpinner;

.field private f:Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

.field private g:Lcom/google/android/gms/plus/oob/FieldViewPronoun;

.field private h:Z

.field private i:Z

.field private j:I


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;-><init>(Landroid/content/Context;Z)V

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    .line 85
    return-void
.end method

.method private c(I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 271
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->r()Z

    move-result v1

    if-nez v1, :cond_0

    .line 279
    :goto_0
    return-object v0

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->q()Ljava/util/List;

    move-result-object v1

    .line 275
    if-ltz p1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lt p1, v2, :cond_2

    .line 276
    :cond_1
    const-string v1, "FieldView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid position for options field: id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 279
    :cond_2
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private k()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 202
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    const-string v0, "custom"

    iget v3, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/oob/FieldViewGender;->c(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 211
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

    if-eqz v3, :cond_2

    .line 212
    iget-object v3, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/plus/oob/FieldViewCustomGender;->setVisibility(I)V

    if-ne v0, v2, :cond_2

    iget-object v4, v3, Lcom/google/android/gms/plus/oob/FieldViewCustomGender;->d:Landroid/widget/EditText;

    if-eqz v4, :cond_2

    iget-object v3, v3, Lcom/google/android/gms/plus/oob/FieldViewCustomGender;->d:Landroid/widget/EditText;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 214
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->g:Lcom/google/android/gms/plus/oob/FieldViewPronoun;

    if-eqz v3, :cond_0

    .line 215
    iget-object v3, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->g:Lcom/google/android/gms/plus/oob/FieldViewPronoun;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->setVisibility(I)V

    if-ne v0, v2, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    iput v0, v3, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    new-instance v0, Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v4, Lcom/google/android/gms/l;->eo:I

    iget-object v5, v3, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->f:Ljava/util/ArrayList;

    new-array v1, v1, [Ljava/lang/CharSequence;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v2, v4, v1}, Lcom/google/android/gms/plus/oob/s;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, v3, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    iget-object v0, v3, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/s;->setDropDownViewResource(I)V

    iget-object v0, v3, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    iget-object v1, v3, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/s;->a(Ljava/lang/CharSequence;)V

    iget-object v0, v3, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget-object v1, v3, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v3}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->k()V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->a:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/l;->ed:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/gms/l;->ec:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/oob/FieldViewCustomGender;Lcom/google/android/gms/plus/oob/FieldViewPronoun;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

    .line 196
    iput-object p2, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->g:Lcom/google/android/gms/plus/oob/FieldViewPronoun;

    .line 197
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->k()V

    .line 198
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 145
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->u()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v1, v2

    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->q()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v3, v4

    :goto_1
    if-ge v3, v7, :cond_2

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iput v3, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    iput-boolean v10, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->i:Z

    iget-boolean v8, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->c:Lcom/google/android/gms/plus/oob/e;

    sget-object v9, Lcom/google/android/gms/common/analytics/l;->v:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v8, v9}, Lcom/google/android/gms/plus/oob/e;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iput-boolean v10, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:Z

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->l()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/google/android/gms/l;->eo:I

    new-array v4, v4, [Ljava/lang/CharSequence;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/gms/plus/oob/s;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lcom/google/android/gms/plus/oob/s;

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lcom/google/android/gms/plus/oob/s;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/s;->setDropDownViewResource(I)V

    .line 148
    sget v0, Lcom/google/android/gms/j;->mQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/oob/GenderSpinner;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/oob/GenderSpinner;->a(Lcom/google/android/gms/plus/oob/t;)V

    .line 151
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 152
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->m()Lcom/google/android/gms/plus/service/v1whitelisted/models/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/f;->e()Ljava/lang/String;

    move-result-object v2

    .line 154
    :cond_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 155
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->tC:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 159
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setSelection(I)V

    .line 162
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->k()V

    .line 163
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:Z

    if-nez v0, :cond_0

    .line 180
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    if-ne v0, p1, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->c:Lcom/google/android/gms/plus/oob/e;

    sget-object v1, Lcom/google/android/gms/common/analytics/l;->v:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/oob/e;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 185
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:Z

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/s;->a()V

    .line 188
    iput p1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    .line 189
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->k()V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->c:Lcom/google/android/gms/plus/oob/e;

    invoke-interface {v0}, Lcom/google/android/gms/plus/oob/e;->a()V

    .line 191
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->c:Lcom/google/android/gms/plus/oob/e;

    sget-object v1, Lcom/google/android/gms/common/analytics/l;->u:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/oob/e;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 124
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    if-eq v0, v5, :cond_3

    move v0, v1

    .line 125
    :goto_0
    const-string v3, "custom"

    iget v4, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/oob/FieldViewGender;->c(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->g:Lcom/google/android/gms/plus/oob/FieldViewPronoun;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->f:Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/FieldViewCustomGender;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->g:Lcom/google/android/gms/plus/oob/FieldViewPronoun;

    iget v3, v0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    if-ne v3, v5, :cond_4

    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 130
    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->f()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    return v2

    :cond_3
    move v0, v2

    .line 124
    goto :goto_0

    .line 126
    :cond_4
    iget-object v3, v0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    iget v0, v0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    invoke-virtual {v3, v0}, Lcom/google/android/gms/plus/oob/s;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public final c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    .line 136
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->i()Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;-><init>()V

    iget v2, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/oob/FieldViewGender;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/i;)Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    if-eqz v0, :cond_0

    .line 168
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setSelection(I)V

    .line 173
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->k()V

    .line 175
    :cond_0
    return-void

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lcom/google/android/gms/plus/oob/s;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/s;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 100
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;

    if-nez v0, :cond_0

    .line 101
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 113
    :goto_0
    return-void

    .line 104
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;

    .line 105
    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 106
    iget-boolean v0, p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:Z

    .line 107
    iget-boolean v0, p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->i:Z

    .line 108
    iget v0, p1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->c:I

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    .line 109
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget v1, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->j:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setSelection(I)V

    .line 112
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewGender;->k()V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 89
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/d;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 90
    new-instance v1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 91
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->h:Z

    iput-boolean v0, v1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->a:Z

    .line 92
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->i:Z

    iput-boolean v0, v1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->b:Z

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->d:Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/s;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, v1, Lcom/google/android/gms/plus/oob/FieldViewGender$SavedState;->c:I

    .line 95
    return-object v1

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewGender;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getSelectedItemPosition()I

    move-result v0

    goto :goto_0
.end method
