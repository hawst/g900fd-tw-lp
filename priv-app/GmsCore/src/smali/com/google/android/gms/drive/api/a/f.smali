.class public final Lcom/google/android/gms/drive/api/a/f;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x20

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 32
    invoke-virtual {p2}, Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/a/f;->e:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 5

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/f;->e:Ljava/util/List;

    const-string v1, "CancelPendingActions with null tags."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    const-string v0, "CancelPendingActionsOperation"

    const-string v1, "Cancel with %d tags."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/api/a/f;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/f;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/f;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/api/d;->a(Ljava/util/List;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/f;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 42
    return-void
.end method
