.class public final Lcom/google/android/gms/drive/database/model/o;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/aa;

.field private final b:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;JLcom/google/android/gms/drive/aa;)V
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Lcom/google/android/gms/drive/database/model/p;->a()Lcom/google/android/gms/drive/database/model/p;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 28
    iput-wide p2, p0, Lcom/google/android/gms/drive/database/model/o;->b:J

    .line 29
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/aa;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/o;->a:Lcom/google/android/gms/drive/aa;

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/gms/drive/database/model/q;->a:Lcom/google/android/gms/drive/database/model/q;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/q;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/o;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 49
    sget-object v0, Lcom/google/android/gms/drive/database/model/q;->b:Lcom/google/android/gms/drive/database/model/q;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/q;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/o;->a:Lcom/google/android/gms/drive/aa;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/aa;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 50
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 54
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "AppScope [appAuthMetadataSqlId=%s, scope=%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/o;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/o;->a:Lcom/google/android/gms/drive/aa;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
