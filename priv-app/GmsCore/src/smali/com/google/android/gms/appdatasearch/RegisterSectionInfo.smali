.class public Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/am;


# instance fields
.field final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:I

.field public final f:Z

.field public final g:Ljava/lang/String;

.field public final h:[Lcom/google/android/gms/appdatasearch/Feature;

.field final i:[I

.field public final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/appdatasearch/am;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/am;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/am;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;[ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    iput p1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a:I

    .line 174
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    .line 175
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    .line 176
    iput-boolean p4, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->d:Z

    .line 177
    iput p5, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->e:I

    .line 178
    iput-boolean p6, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->f:Z

    .line 179
    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->g:Ljava/lang/String;

    .line 180
    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->h:[Lcom/google/android/gms/appdatasearch/Feature;

    .line 181
    iput-object p9, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->i:[I

    .line 182
    iput-object p10, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->j:Ljava/lang/String;

    .line 183
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;[ILjava/lang/String;)V
    .locals 11

    .prologue
    .line 203
    const/4 v1, 0x2

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>(ILjava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;[ILjava/lang/String;)V

    .line 205
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/ak;
    .locals 1

    .prologue
    .line 282
    new-instance v0, Lcom/google/android/gms/appdatasearch/ak;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/ak;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/appdatasearch/Feature;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->h:[Lcom/google/android/gms/appdatasearch/Feature;

    invoke-static {p1, v0}, Lcom/google/android/gms/appdatasearch/Feature;->a(I[Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 217
    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/am;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 228
    instance-of v1, p1, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    if-eqz v1, :cond_0

    .line 229
    check-cast p1, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    .line 230
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->d:Z

    iget-boolean v2, p1, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->d:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 234
    :cond_0
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 223
    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/am;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/am;->a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Landroid/os/Parcel;I)V

    .line 224
    return-void
.end method
