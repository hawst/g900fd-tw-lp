.class public abstract Lcom/google/android/gms/people/service/a/d;
.super Lcom/google/android/gms/people/service/a/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/people/service/a/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method protected static a(Lcom/google/android/gms/people/service/g;Lcom/google/android/gms/people/a/b;Landroid/os/Bundle;Lcom/google/android/gms/common/server/ClientContext;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 51
    invoke-virtual {p0, p3}, Lcom/google/android/gms/people/service/g;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v4

    .line 52
    invoke-virtual {p0, p3}, Lcom/google/android/gms/people/service/g;->d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v5

    .line 55
    invoke-virtual {p1, p2}, Lcom/google/android/gms/people/a/b;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 57
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    .line 58
    iget-object v0, p1, Lcom/google/android/gms/people/a/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v2

    .line 66
    new-instance v0, Lcom/google/android/gms/common/server/e;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move-object v3, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/e;-><init>(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v0, v7, v4}, Lcom/google/android/gms/people/service/g;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    .line 69
    const-string v0, "PeopleService"

    const-string v1, "Waiting for response"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :try_start_0
    sget-object v0, Lcom/google/android/gms/people/a/a;->aB:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, v3}, Lcom/android/volley/toolbox/aa;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p4, v0}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 85
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    new-instance v1, Lcom/android/volley/ac;

    const-string v2, "Timeout"

    invoke-direct {v1, v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p5, v1}, Lcom/android/volley/w;->a(Lcom/android/volley/ac;)V

    .line 76
    throw v0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    const-string v1, "PeopleService"

    const-string v2, "Request Failed"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 79
    new-instance v1, Lcom/android/volley/ac;

    const-string v2, "Network error"

    invoke-direct {v1, v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p5, v1}, Lcom/android/volley/w;->a(Lcom/android/volley/ac;)V

    goto :goto_0

    .line 82
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/server/e;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move-object v2, p4

    move-object v3, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/e;-><init>(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    invoke-virtual {p0, v0, v7, v4}, Lcom/google/android/gms/people/service/g;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    goto :goto_0
.end method
