.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public final c:Ljava/util/Set;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/util/List;

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->c:Ljava/util/Set;

    .line 135
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/t;
    .locals 12

    .prologue
    .line 235
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->c:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->e:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->f:Z

    iget-object v5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->g:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->h:Ljava/util/List;

    iget-boolean v7, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->i:Z

    iget-boolean v8, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->j:Z

    iget-boolean v9, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->a:Z

    iget-boolean v10, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->b:Z

    iget-object v11, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->k:Ljava/lang/String;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;ZZZZLjava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/u;
    .locals 2

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->h:Ljava/util/List;

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->c:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 182
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/plus/service/v1whitelisted/models/u;
    .locals 2

    .prologue
    .line 161
    iput-boolean p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->f:Z

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/u;->c:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 163
    return-object p0
.end method
