.class public final Lcom/google/android/gms/smart_profile/HeaderViewCreatorImpl;
.super Lcom/google/android/gms/smart_profile/w;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/DynamiteApi;
.end annotation

.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/w;-><init>()V

    .line 82
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    .prologue
    .line 420
    :try_start_0
    const-string v0, "com.google.android.gms"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 424
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/b/l;)Lcom/google/android/gms/smart_profile/x;
    .locals 4

    .prologue
    .line 409
    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 410
    invoke-static {v0}, Lcom/google/android/gms/smart_profile/HeaderViewCreatorImpl;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 412
    if-nez v1, :cond_1

    .line 413
    const-string v0, "Failed to load GMS context"

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-lt v1, v2, :cond_0

    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const-string v1, "HeaderViewCreatorImpl"

    invoke-static {v1, v0}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 415
    :cond_1
    new-instance v2, Lcom/google/android/gms/smart_profile/s;

    new-instance v3, Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-direct {v3, v1}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/smart_profile/s;-><init>(Landroid/content/Context;Lcom/google/android/gms/smart_profile/header/view/HeaderView;)V

    return-object v2
.end method
