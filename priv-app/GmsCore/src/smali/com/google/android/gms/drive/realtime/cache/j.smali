.class final Lcom/google/android/gms/drive/realtime/cache/j;
.super Lcom/google/android/gms/drive/realtime/cache/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/drive/realtime/cache/i;

.field private final c:Lcom/google/c/a/a/b/a/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/c/a/a/b/a/b;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/a;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;)V

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/j;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    .line 32
    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/cache/j;->c:Lcom/google/c/a/a/b/a/b;

    .line 33
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/a;->a:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/n;->e()V

    .line 52
    return-void
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/a;->a:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/n;->j()Lcom/google/android/gms/drive/database/model/br;

    move-result-object v0

    .line 39
    iget-boolean v1, v0, Lcom/google/android/gms/drive/database/model/br;->d:Z

    if-nez v1, :cond_0

    .line 40
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/drive/database/model/br;->d:Z

    .line 41
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/br;->i()V

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/j;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/j;->c:Lcom/google/c/a/a/b/a/b;

    iget-object v1, v1, Lcom/google/c/a/a/b/a/b;->a:Lcom/google/c/b/a/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a()Lcom/google/android/gms/drive/realtime/cache/a/c;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/j;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/j;->c:Lcom/google/c/a/a/b/a/b;

    iget-object v1, v1, Lcom/google/c/a/a/b/a/b;->b:Lcom/google/c/a/a/b/c/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/e;->a()Lcom/google/android/gms/drive/realtime/cache/a/e;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/drive/realtime/cache/a/e;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 47
    return-void
.end method
