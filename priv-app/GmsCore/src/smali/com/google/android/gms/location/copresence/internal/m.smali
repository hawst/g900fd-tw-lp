.class public abstract Lcom/google/android/gms/location/copresence/internal/m;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/location/copresence/internal/l;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/location/copresence/internal/m;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/l;
    .locals 2

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/location/copresence/internal/l;

    if-eqz v1, :cond_1

    .line 33
    check-cast v0, Lcom/google/android/gms/location/copresence/internal/l;

    goto :goto_0

    .line 35
    :cond_1
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/location/copresence/internal/n;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 122
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 47
    :sswitch_0
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v7

    .line 48
    goto :goto_0

    .line 52
    :sswitch_1
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    sget-object v0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;

    move-object v5, v0

    .line 69
    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/j;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/i;

    move-result-object v6

    move-object v0, p0

    .line 70
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/location/copresence/internal/m;->a(Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/BatchImpl;Lcom/google/android/gms/location/copresence/internal/i;)V

    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 72
    goto :goto_0

    .line 76
    :sswitch_2
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_1

    .line 81
    sget-object v1, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;->CREATOR:Lcom/google/android/gms/location/copresence/debug/a;

    invoke-static {p2}, Lcom/google/android/gms/location/copresence/debug/a;->a(Landroid/os/Parcel;)Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;

    move-result-object v5

    .line 87
    :cond_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/location/copresence/internal/j;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/i;

    move-result-object v1

    .line 88
    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/gms/location/copresence/internal/m;->a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeRequest;Lcom/google/android/gms/location/copresence/internal/i;)V

    .line 89
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 90
    goto :goto_0

    .line 94
    :sswitch_3
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_2

    .line 99
    sget-object v1, Lcom/google/android/gms/location/copresence/CopresenceSettings;->CREATOR:Lcom/google/android/gms/location/copresence/m;

    invoke-static {p2}, Lcom/google/android/gms/location/copresence/m;->a(Landroid/os/Parcel;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v5

    .line 105
    :cond_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/location/copresence/internal/j;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/i;

    move-result-object v1

    .line 106
    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/gms/location/copresence/internal/m;->a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/CopresenceSettings;Lcom/google/android/gms/location/copresence/internal/i;)V

    .line 107
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 108
    goto/16 :goto_0

    .line 112
    :sswitch_4
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/location/copresence/internal/j;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/i;

    move-result-object v1

    .line 117
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/location/copresence/internal/m;->a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/i;)V

    .line 118
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 119
    goto/16 :goto_0

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
