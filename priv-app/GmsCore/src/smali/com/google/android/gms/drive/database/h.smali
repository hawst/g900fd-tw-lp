.class public final Lcom/google/android/gms/drive/database/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Locale;

.field private static final b:Lcom/google/android/gms/drive/database/y;

.field private static final c:Lcom/google/android/gms/drive/database/y;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, Lcom/google/android/gms/drive/database/h;->a:Ljava/util/Locale;

    .line 37
    new-instance v0, Lcom/google/android/gms/drive/database/y;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

    sget-object v2, Lcom/google/android/gms/drive/database/h;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/y;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/google/android/gms/drive/database/h;->b:Lcom/google/android/gms/drive/database/y;

    .line 38
    new-instance v0, Lcom/google/android/gms/drive/database/y;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSSz"

    sget-object v2, Lcom/google/android/gms/drive/database/h;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/y;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/google/android/gms/drive/database/h;->c:Lcom/google/android/gms/drive/database/y;

    .line 39
    sget-object v0, Lcom/google/android/gms/drive/database/h;->b:Lcom/google/android/gms/drive/database/y;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/y;->a(Ljava/util/TimeZone;)V

    .line 40
    sget-object v0, Lcom/google/android/gms/drive/database/h;->c:Lcom/google/android/gms/drive/database/y;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/y;->a(Ljava/util/TimeZone;)V

    .line 41
    return-void
.end method

.method private static a(Lcom/google/android/gms/drive/internal/model/User;)Lcom/google/android/gms/drive/UserMetadata;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 197
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/model/User;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v3

    .line 202
    :goto_0
    return-object v0

    .line 200
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/model/User;->f()Lcom/google/android/gms/drive/internal/model/User$Picture;

    move-result-object v0

    .line 201
    if-nez v0, :cond_2

    .line 202
    :goto_1
    new-instance v0, Lcom/google/android/gms/drive/UserMetadata;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/model/User;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/model/User;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/model/User;->d()Z

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/gms/drive/internal/model/User;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/UserMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 201
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/User$Picture;->b()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public static a(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/drive/database/h;->b:Lcom/google/android/gms/drive/database/y;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/drive/database/y;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Z"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/util/Date;
    .locals 1

    .prologue
    .line 181
    if-nez p0, :cond_0

    .line 182
    const/4 v0, 0x0

    .line 186
    :goto_0
    return-object v0

    .line 184
    :cond_0
    const-string v0, "z"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Z"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/database/h;->b:Lcom/google/android/gms/drive/database/y;

    .line 186
    :goto_1
    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/database/y;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 184
    :cond_2
    sget-object v0, Lcom/google/android/gms/drive/database/h;->c:Lcom/google/android/gms/drive/database/y;

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/drive/d/d;Lcom/google/android/gms/drive/database/model/ah;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 59
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 61
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/database/h;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 62
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->k()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->m(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/util/Date;)V

    .line 65
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->f()Ljava/util/List;

    move-result-object v0

    .line 67
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 68
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 69
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/Long;)V

    .line 73
    :cond_0
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->q()Ljava/lang/String;

    move-result-object v0

    .line 75
    if-nez v0, :cond_2

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->b(Ljava/util/Date;)V

    .line 80
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/database/h;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 81
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->J()Ljava/util/Date;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_3

    .line 84
    if-eqz v1, :cond_1

    .line 87
    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 91
    :cond_1
    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->c(Ljava/util/Date;)V

    .line 93
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->h()Ljava/lang/String;

    move-result-object v0

    .line 94
    if-nez v0, :cond_4

    move-object v0, v2

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->c(Ljava/lang/Long;)V

    .line 97
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->i()Ljava/lang/String;

    move-result-object v0

    .line 98
    if-nez v0, :cond_5

    move-object v0, v2

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->b(Ljava/lang/Long;)V

    .line 100
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->n()Ljava/lang/String;

    .line 101
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->o()Ljava/lang/String;

    move-result-object v0

    .line 102
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v6

    .line 103
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->L()Ljava/lang/String;

    move-result-object v7

    .line 104
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->P()Lcom/google/android/gms/drive/internal/model/User;

    move-result-object v1

    .line 105
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->Q()Lcom/google/android/gms/drive/internal/model/User;

    move-result-object v5

    .line 107
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->g(Ljava/lang/String;)V

    .line 109
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->a()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->e(Z)V

    .line 110
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->b()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->f(Z)V

    .line 111
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->s()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 112
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->w()V

    .line 119
    :goto_4
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/String;)V

    .line 120
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->t()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->d(Z)V

    .line 121
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->l(Ljava/lang/String;)V

    .line 122
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    move v0, v3

    :goto_5
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->m(Z)V

    .line 123
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->n(Ljava/lang/String;)V

    .line 124
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->u()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/util/Collection;)V

    .line 125
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->w()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->o(Ljava/lang/String;)V

    .line 126
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->x()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->g(Z)V

    .line 127
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->y()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->p(Ljava/lang/String;)V

    .line 128
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->q(Ljava/lang/String;)V

    .line 129
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->A()J

    move-result-wide v8

    invoke-virtual {p1, v8, v9}, Lcom/google/android/gms/drive/database/model/ah;->a(J)V

    .line 130
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->B()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->h(Z)V

    .line 131
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->C()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->i(Z)V

    .line 132
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->D()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->r(Ljava/lang/String;)V

    .line 133
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->E()J

    move-result-wide v8

    invoke-virtual {p1, v8, v9}, Lcom/google/android/gms/drive/database/model/ah;->b(J)V

    .line 134
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->h(Ljava/lang/String;)V

    .line 135
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->G()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->s(Ljava/lang/String;)V

    .line 136
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->H()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    move v0, v3

    :goto_6
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->j(Z)V

    .line 137
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->K()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->u(Ljava/lang/String;)V

    .line 138
    invoke-static {v1}, Lcom/google/android/gms/drive/database/h;->a(Lcom/google/android/gms/drive/internal/model/User;)Lcom/google/android/gms/drive/UserMetadata;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Lcom/google/android/gms/drive/UserMetadata;)V

    .line 139
    invoke-static {v5}, Lcom/google/android/gms/drive/database/h;->a(Lcom/google/android/gms/drive/internal/model/User;)Lcom/google/android/gms/drive/UserMetadata;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->b(Lcom/google/android/gms/drive/UserMetadata;)V

    .line 140
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->R()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->k(Z)V

    .line 144
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->d()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_c

    invoke-static {v0}, Lcom/google/android/gms/drive/d/a/c;->b(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 146
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->b()V

    .line 147
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->I()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/Property;

    .line 148
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/Property;->f()Ljava/lang/String;

    move-result-object v1

    const-string v5, "PUBLIC"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    move v1, v4

    .line 150
    :goto_8
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/Property;->c()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/Property;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 151
    :goto_9
    new-instance v9, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    new-instance v10, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/Property;->d()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/Property;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v9, v10, v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;-><init>(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)V

    invoke-virtual {p1, v9, v5}, Lcom/google/android/gms/drive/database/model/ah;->a(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;Ljava/lang/Long;)V

    goto :goto_7

    .line 75
    :cond_2
    invoke-static {v0}, Lcom/google/android/gms/drive/database/h;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    move-object v0, v1

    .line 87
    goto/16 :goto_1

    .line 94
    :cond_4
    invoke-static {v0}, Lcom/google/android/gms/drive/database/h;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_2

    .line 98
    :cond_5
    invoke-static {v0}, Lcom/google/android/gms/drive/database/h;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_3

    .line 113
    :cond_6
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->n()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 114
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->v()V

    goto/16 :goto_4

    .line 116
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->t()V

    goto/16 :goto_4

    :cond_8
    move v0, v4

    .line 122
    goto/16 :goto_5

    :cond_9
    move v0, v4

    .line 136
    goto/16 :goto_6

    :cond_a
    move v1, v3

    .line 148
    goto :goto_8

    :cond_b
    move-object v5, v2

    .line 150
    goto :goto_9

    .line 159
    :cond_c
    invoke-virtual {p1, v7}, Lcom/google/android/gms/drive/database/model/ah;->d(Ljava/lang/String;)V

    .line 163
    if-eqz v6, :cond_d

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 164
    :cond_d
    invoke-virtual {p1, v2}, Lcom/google/android/gms/drive/database/model/ah;->e(Ljava/lang/String;)V

    .line 165
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->r()V

    .line 169
    :cond_e
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->M()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->N()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 171
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->M()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->N()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 173
    invoke-interface {p0}, Lcom/google/android/gms/drive/d/d;->O()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/ah;->j(Ljava/lang/String;)V

    .line 175
    :cond_f
    return-void
.end method
