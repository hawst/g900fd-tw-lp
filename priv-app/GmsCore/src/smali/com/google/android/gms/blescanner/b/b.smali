.class public Lcom/google/android/gms/blescanner/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/IntentFilter;


# instance fields
.field public b:Z

.field private final c:Landroid/content/Context;

.field private final d:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/blescanner/b/b;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Lcom/google/android/gms/blescanner/b/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/blescanner/b/c;-><init>(Lcom/google/android/gms/blescanner/b/b;)V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/b/b;->d:Landroid/content/BroadcastReceiver;

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/blescanner/b/b;->c:Landroid/content/Context;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/blescanner/b/b;->b:Z

    .line 27
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "android.bluetooth.adapter.extra.STATE"

    const/high16 v1, -0x80000000

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 78
    packed-switch v0, :pswitch_data_0

    .line 84
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 80
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/blescanner/b/b;->a()V

    goto :goto_0

    .line 83
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/blescanner/b/b;->b()V

    goto :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/blescanner/b/b;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/blescanner/b/b;->d:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/gms/blescanner/b/b;->a:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/blescanner/b/b;->b:Z

    .line 50
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/gms/blescanner/b/b;->b:Z

    if-eqz v0, :cond_0

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/blescanner/b/b;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/blescanner/b/b;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/blescanner/b/b;->b:Z

    .line 66
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
