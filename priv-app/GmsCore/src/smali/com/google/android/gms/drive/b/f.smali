.class public final Lcom/google/android/gms/drive/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/database/i;

.field public final b:Lcom/google/android/gms/drive/database/r;

.field public final c:Lcom/google/android/gms/drive/g/i;

.field final d:Lcom/google/android/gms/drive/g/w;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/g/w;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    .line 76
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    .line 77
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/f;->c:Lcom/google/android/gms/drive/g/i;

    .line 78
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/w;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/f;->d:Lcom/google/android/gms/drive/g/w;

    .line 79
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/model/ax;)Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 193
    if-eqz p1, :cond_0

    .line 195
    :try_start_0
    iget-object v1, p1, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    .line 196
    if-eqz v1, :cond_1

    .line 197
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    .line 207
    :cond_0
    :goto_0
    return v0

    .line 199
    :cond_1
    iget-object v1, p1, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 202
    :catch_0
    move-exception v1

    .line 203
    const-string v2, "ContentManager"

    const-string v3, "Content with hash %s was deleted outside of ContentManager."

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-static {v2, v1, v3, v4}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 137
    const-string v1, "ContentManager"

    const-string v2, "openReadOnly %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v6

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 138
    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, p1}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v3

    .line 139
    if-nez v3, :cond_0

    .line 149
    :goto_0
    return-object v0

    .line 143
    :cond_0
    :try_start_0
    iget-object v1, v3, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, v4}, Lcom/google/android/gms/drive/database/r;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/drive/g/s; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v1, v3, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {p0, v1, v5}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-object v0, v3, Lcom/google/android/gms/drive/database/model/ax;->d:Ljavax/crypto/SecretKey;

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/drive/g/g;->a(Ljava/security/Key;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/ax;->a(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ax;->i()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, v4}, Lcom/google/android/gms/drive/database/r;->e(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/google/android/gms/drive/g/s; {:try_start_5 .. :try_end_5} :catch_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/b/f;->c:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    iput-wide v0, v3, Lcom/google/android/gms/drive/database/model/ax;->g:J

    .line 148
    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ax;->i()V

    .line 149
    iget-object v0, v3, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v6}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    :goto_1
    :try_start_6
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_7
    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, v4}, Lcom/google/android/gms/drive/database/r;->e(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Lcom/google/android/gms/drive/g/s; {:try_start_7 .. :try_end_7} :catch_0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 143
    :catchall_2
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1

    :catchall_3
    move-exception v0

    goto :goto_1
.end method

.method public final a(I)Lcom/google/android/gms/drive/b/l;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 99
    const/high16 v0, 0x20000000

    if-eq p1, v0, :cond_0

    const/high16 v0, 0x30000000

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "Invalid file mode provided!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 101
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 102
    invoke-virtual {p0, v5, v1}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, v5}, Lcom/google/android/gms/drive/database/r;->d(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 105
    new-instance v0, Lcom/google/android/gms/drive/b/l;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/b/f;->c:Lcom/google/android/gms/drive/g/i;

    move-object v4, p0

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/b/l;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/b/f;Ljava/lang/String;I)V

    return-object v0

    :cond_1
    move v0, v1

    .line 99
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)Ljava/io/File;
    .locals 3

    .prologue
    .line 337
    if-nez p2, :cond_0

    .line 338
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->d:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/g/w;->e()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 345
    :goto_0
    return-object v0

    .line 339
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/drive/b/f;->d:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/w;->f()Ljava/io/File;

    move-result-object v1

    .line 341
    if-nez v1, :cond_1

    .line 342
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Cannot get path for a file on encrypted shared storage because shared storage is not available."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0

    .line 347
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported storageType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ah;)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 377
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->p()Ljava/lang/String;

    move-result-object v1

    .line 378
    const-string v2, "ContentManager"

    const-string v3, "isLocalContentValid: %s, localContentHash: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 379
    if-nez v1, :cond_0

    .line 420
    :goto_0
    return v0

    .line 383
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2, v1}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v2

    .line 384
    if-nez v2, :cond_1

    .line 385
    const-string v2, "ContentManager"

    const-string v3, "%s points to nonexistent content hash %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 389
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->m()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 397
    const-string v0, "ContentManager"

    const-string v2, "Relying on old database"

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v0, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 402
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 403
    const-string v1, "ContentManager"

    const-string v2, "Local content revision outdated"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 413
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->l()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 414
    const-string v1, "ContentManager"

    const-string v2, "Unexpected null head revision content hash with equal head revision and local content revision. Considering local content invalid"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 419
    :cond_4
    const-string v0, "ContentManager"

    const-string v1, "Local content revision matches. Relying on content existing"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    invoke-direct {p0, v2}, Lcom/google/android/gms/drive/b/f;->a(Lcom/google/android/gms/drive/database/model/ax;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v1

    .line 166
    if-nez v1, :cond_0

    .line 167
    const/4 v0, 0x0

    .line 178
    :goto_0
    return-object v0

    .line 169
    :cond_0
    iget-object v0, v1, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 170
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    goto :goto_0

    .line 174
    :cond_1
    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/drive/database/model/ax;->d:Ljavax/crypto/SecretKey;

    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 175
    const/4 v0, 0x2

    iget-object v3, v1, Lcom/google/android/gms/drive/database/model/ax;->d:Ljavax/crypto/SecretKey;

    invoke-virtual {v2, v0, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 176
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v0, v1, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 178
    new-instance v0, Ljavax/crypto/CipherInputStream;

    invoke-direct {v0, v3, v2}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 181
    :catch_1
    move-exception v0

    .line 182
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 183
    :catch_2
    move-exception v0

    .line 184
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method final c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 251
    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->j()V

    .line 253
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, p1}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v1

    .line 254
    if-nez v1, :cond_0

    .line 255
    const-string v1, "ContentManager"

    const-string v2, "Cannot move to shared storage. No content with hash: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->l()V

    .line 272
    :goto_0
    return-object v0

    .line 259
    :cond_0
    :try_start_1
    iget-object v2, v1, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_1

    .line 261
    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->l()V

    goto :goto_0

    .line 263
    :cond_1
    :try_start_2
    iget-object v2, v1, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 265
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/model/ax;->a(Ljava/lang/String;)V

    .line 266
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ax;->i()V

    .line 267
    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->k()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 268
    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->l()V

    goto :goto_0

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->l()V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/b/f;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->l()V

    throw v0
.end method
