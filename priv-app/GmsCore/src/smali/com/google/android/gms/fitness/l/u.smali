.class interface abstract Lcom/google/android/gms/fitness/l/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 286
    const-string v0, "CREATE TABLE %s (%s, %s, %s, %s, %s, %s, %s, %s);"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Sessions"

    aput-object v2, v1, v4

    const-string v2, "_id INTEGER PRIMARY KEY AUTOINCREMENT"

    aput-object v2, v1, v5

    const-string v2, "app_package TEXT NOT NULL"

    aput-object v2, v1, v6

    const-string v2, "start_time INTEGER NOT NULL"

    aput-object v2, v1, v7

    const-string v2, "end_time INTEGER NOT NULL DEFAULT 0"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "name TEXT NOT NULL"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "identifier TEXT"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "description TEXT"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "activity INTEGER NOT NULL"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/u;->a:Ljava/lang/String;

    .line 302
    const-string v0, "DELETE FROM %s WHERE EXISTS (SELECT 1 FROM %s s2 WHERE %s.%s = s2.%s and %s.%s < s2.%s)"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Sessions"

    aput-object v2, v1, v4

    const-string v2, "Sessions"

    aput-object v2, v1, v5

    const-string v2, "Sessions"

    aput-object v2, v1, v6

    const-string v2, "identifier"

    aput-object v2, v1, v7

    const-string v2, "identifier"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "Sessions"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "_id"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/u;->b:Ljava/lang/String;

    return-void
.end method
