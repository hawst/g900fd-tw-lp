.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;
.super Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/drive/database/model/a;

.field private final c:Lcom/google/android/gms/drive/database/r;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 27
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;->b:Lcom/google/android/gms/drive/database/model/a;

    .line 28
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;->c:Lcom/google/android/gms/drive/database/r;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/d/g;)V
    .locals 6

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a(Lcom/google/android/gms/drive/d/g;)V

    .line 35
    invoke-interface {p1}, Lcom/google/android/gms/drive/d/g;->c()Ljava/lang/Long;

    move-result-object v0

    .line 36
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 40
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;->c:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;->b:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, v2, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v1

    .line 42
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/c;->g()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 43
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/model/c;->c(J)V

    .line 44
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/c;->i()V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 49
    return-void

    .line 48
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ChangeFeedProcessor["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b;->b:Lcom/google/android/gms/drive/database/model/a;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
