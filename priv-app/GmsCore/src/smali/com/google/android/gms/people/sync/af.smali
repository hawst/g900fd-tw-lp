.class final Lcom/google/android/gms/people/sync/af;
.super Lcom/google/android/gms/people/sync/ag;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/sync/z;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/sync/z;ZZ)V
    .locals 0

    .prologue
    .line 2126
    iput-object p1, p0, Lcom/google/android/gms/people/sync/af;->a:Lcom/google/android/gms/people/sync/z;

    .line 2127
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/sync/ag;-><init>(Lcom/google/android/gms/people/sync/z;ZZ)V

    .line 2128
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/sync/z;ZZB)V
    .locals 0

    .prologue
    .line 2125
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/sync/af;-><init>(Lcom/google/android/gms/people/sync/z;ZZ)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2134
    iget-boolean v0, p0, Lcom/google/android/gms/people/sync/af;->b:Z

    if-nez v0, :cond_0

    .line 2135
    iget-object v0, p0, Lcom/google/android/gms/people/sync/af;->a:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->p()V

    .line 2137
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2141
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->q(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2143
    const-string v0, "PeopleSync"

    const-string v1, "Deleted person detected in full sync"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2148
    :goto_0
    return-void

    .line 2145
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/af;->a:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v2

    iget-boolean v0, p0, Lcom/google/android/gms/people/sync/af;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/people/sync/af;->a:Lcom/google/android/gms/people/sync/z;

    invoke-static {v3}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v3

    invoke-virtual {v2, p1, v1, v0, v3}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;ZZLcom/google/android/gms/people/sync/y;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2152
    iget-object v0, p0, Lcom/google/android/gms/people/sync/af;->a:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->q()V

    .line 2153
    return-void
.end method
