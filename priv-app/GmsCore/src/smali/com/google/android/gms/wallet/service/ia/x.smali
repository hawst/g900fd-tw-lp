.class final Lcom/google/android/gms/wallet/service/ia/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/util/w;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/google/android/gms/wallet/a/b;

.field final synthetic e:Lcom/google/checkout/inapp/proto/ap;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Lcom/google/android/gms/wallet/service/ia/o;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/checkout/inapp/proto/ap;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/x;->h:Lcom/google/android/gms/wallet/service/ia/o;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/x;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ia/x;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ia/x;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ia/x;->d:Lcom/google/android/gms/wallet/a/b;

    iput-object p6, p0, Lcom/google/android/gms/wallet/service/ia/x;->e:Lcom/google/checkout/inapp/proto/ap;

    iput-object p7, p0, Lcom/google/android/gms/wallet/service/ia/x;->f:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/wallet/service/ia/x;->g:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/16 v7, 0x9

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/x;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ia/x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/inapp-secure/api/v1/update_instrument?s7e=cvn"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/x;->h:Lcom/google/android/gms/wallet/service/ia/o;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ia/x;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ia/x;->d:Lcom/google/android/gms/wallet/a/b;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ia/x;->e:Lcom/google/checkout/inapp/proto/ap;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/x;->a:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/wallet/service/ia/x;->f:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/wallet/service/ia/x;->g:Ljava/lang/String;

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/wallet/service/ia/o;->a(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/protobuf/nano/j;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ia/x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/inapp/api/v1/update_instrument"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ia/x;->h:Lcom/google/android/gms/wallet/service/ia/o;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ia/x;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/wallet/service/ia/x;->d:Lcom/google/android/gms/wallet/a/b;

    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/x;->e:Lcom/google/checkout/inapp/proto/ap;

    iget-object v8, p0, Lcom/google/android/gms/wallet/service/ia/x;->f:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/wallet/service/ia/x;->g:Ljava/lang/String;

    invoke-static/range {v2 .. v9}, Lcom/google/android/gms/wallet/service/ia/o;->a(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/protobuf/nano/j;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
