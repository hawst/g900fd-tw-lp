.class public final Lcom/google/android/gms/people/service/a/af;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    .line 377
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    .line 379
    const-string v1, "SELECT DISTINCT gaia_id,value,type FROM gaia_id_map WHERE ((?1 = \'\') OR (value = ?1)) AND ((?2 = \'\') OR (gaia_id = ?2)) AND ((type& ?) != 0)"

    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    .line 394
    new-instance v1, Lcom/google/android/gms/common/data/DataHolder;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    .prologue
    .line 60
    and-int/lit8 v2, p12, 0x4

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    .line 64
    :goto_0
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 65
    const-string v3, "account"

    invoke-virtual {v4, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v3, "pagegaiaid"

    invoke-virtual {v4, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-static {p0, p1, p2, v4}, Lcom/google/android/gms/people/service/a/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 68
    const-string v3, "emails_with_affinities"

    invoke-virtual {v4, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 71
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v5

    .line 75
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    .line 78
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 83
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    const-string v3, "SELECT "

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    new-instance v3, Lcom/google/android/gms/people/service/a/ag;

    invoke-direct {v3, v9}, Lcom/google/android/gms/people/service/a/ag;-><init>(Ljava/lang/StringBuilder;)V

    .line 89
    const/16 v10, 0x4000

    const-string v11, "_id"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 92
    const/4 v10, 0x1

    const-string v11, "qualified_id"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 93
    const/4 v10, 0x2

    const-string v11, "gaia_id"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 94
    const/4 v10, 0x4

    const-string v11, "name"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 95
    const/16 v10, 0x1000

    const-string v11, "given_name"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 96
    const/16 v10, 0x2000

    const-string v11, "family_name"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 97
    const/16 v10, 0x800

    const-string v11, "name_verified"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 98
    const/16 v10, 0x8

    const-string v11, "sort_key"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 99
    const/16 v10, 0x10

    const-string v11, "sort_key_irank"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 101
    const/16 v10, 0x20

    const-string v11, "avatar"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 102
    const/16 v10, 0x40

    const-string v11, "profile_type"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 105
    const/16 v10, 0x80

    const-string v11, "(SELECT group_concat(circle_id) FROM circle_members AS CM  WHERE CM.owner_id==P.owner_id AND CM.qualified_id=P.qualified_id)AS v_circle_ids"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 114
    const/16 v10, 0x100

    const-string v11, "blocked"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 115
    const/16 v10, 0x200

    const-string v11, "last_modified"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 116
    const/16 v10, 0x400

    const-string v11, "in_viewer_domain"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 118
    const v10, 0x8000

    const-string v11, "affinity1"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 119
    const/high16 v10, 0x10000

    const-string v11, "affinity2"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 120
    const/high16 v10, 0x20000

    const-string v11, "affinity3"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 121
    const/high16 v10, 0x40000

    const-string v11, "affinity4"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 122
    const/high16 v10, 0x80000

    const-string v11, "affinity5"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 125
    const v10, 0x8000

    const-string v11, "logging_id"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 126
    const/high16 v10, 0x10000

    const-string v11, "logging_id2"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 127
    const/high16 v10, 0x20000

    const-string v11, "logging_id3"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 128
    const/high16 v10, 0x40000

    const-string v11, "logging_id4"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 129
    const/high16 v10, 0x80000

    const-string v11, "logging_id5"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 131
    const/high16 v10, 0x100000

    const-string v11, "people_in_common"

    move/from16 v0, p5

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 143
    if-nez v2, :cond_3

    .line 145
    const-string v2, ""

    .line 160
    :goto_1
    const/4 v10, 0x1

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "(SELECT group_concat(type||\"\u0001\"||ifnull(custom_label,\'\')||\"\u0001\"||email"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, ",\"\u0002\") FROM "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, "emails AS EM  WHERE EM.owner_id"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, "==P.owner_id AND EM."

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, "qualified_id=P.qualified_id)"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, "AS v_emails"

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move/from16 v0, p12

    invoke-virtual {v3, v0, v10, v2}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 176
    const/4 v2, 0x2

    const-string v10, "(SELECT group_concat(type||\"\u0001\"||ifnull(custom_label,\'\')||\"\u0001\"||phone,\"\u0002\") FROM phones AS PH  WHERE PH.owner_id==P.owner_id AND PH.qualified_id=P.qualified_id)AS v_phones"

    move/from16 v0, p12

    invoke-virtual {v3, v0, v2, v10}, Lcom/google/android/gms/people/service/a/ag;->a(IILjava/lang/String;)V

    .line 193
    const-string v2, " FROM people AS P"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string v2, " WHERE (owner_id = ?)"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 203
    const-string v2, " AND (qualified_id IN (SELECT qualified_id FROM circle_members WHERE owner_id=P.owner_id AND circle_id=?))"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    :goto_2
    if-eqz p6, :cond_0

    .line 228
    const-string v2, " AND (profile_type=1)"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p7, v2

    if-eqz v2, :cond_1

    .line 234
    const-string v2, " AND (last_modified>=?)"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    invoke-static/range {p7 .. p8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    :cond_1
    if-eqz p4, :cond_9

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_9

    .line 240
    const-string v2, " AND qualified_id IN ("

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    const/4 v2, 0x0

    :goto_3
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_8

    .line 242
    if-nez v2, :cond_7

    .line 243
    const-string v3, "?"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :goto_4
    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 60
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 147
    :cond_3
    const-string v2, "||\"\u0001\"||ifnull(affinity1,0)||\"\u0001\"||ifnull(affinity2,0)||\"\u0001\"||ifnull(affinity3,0)||\"\u0001\"||ifnull(affinity4,0)||\"\u0001\"||ifnull(affinity5,0)||\"\u0001\"||ifnull(logging_id,\'\')||\"\u0001\"||ifnull(logging_id2,\'\')||\"\u0001\"||ifnull(logging_id3,\'\')||\"\u0001\"||ifnull(logging_id4,\'\')||\"\u0001\"||ifnull(logging_id5,\'\')"

    goto/16 :goto_1

    .line 211
    :cond_4
    const-string v2, " AND ((in_circle =1)"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    if-eqz p13, :cond_6

    .line 215
    const-string v2, " OR (in_contacts=1"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    sget-object v2, Lcom/google/android/gms/people/a/a;->ae:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_5

    .line 218
    const-string v2, " AND EXISTS (SELECT 1 FROM gaia_id_map G  WHERE G.owner_id=P.owner_id AND G.type!=2 AND G.gaia_id=P.gaia_id)"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    :cond_5
    const-string v2, ")"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    :cond_6
    const-string v2, ")"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 245
    :cond_7
    const-string v3, ",?"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 249
    :cond_8
    const-string v2, ")"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_9
    invoke-static/range {p9 .. p9}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 254
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 255
    sget-object v3, Lcom/google/android/gms/people/internal/at;->f:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v6

    .line 262
    const/4 v2, 0x0

    :goto_5
    array-length v3, v6

    if-ge v2, v3, :cond_e

    .line 263
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    .line 265
    aget-object v3, v6, v2

    .line 266
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "%"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3}, Lcom/google/android/gms/people/c/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "%"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    const-string v3, " AND (_id IN "

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const-string v3, "(SELECT "

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "person_id"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " FROM search_index WHERE ((value LIKE ?"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " AND kind!=1) OR (value LIKE ?"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " AND kind=1))"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    move/from16 v0, p10

    if-eq v0, v3, :cond_d

    const-string v3, " AND kind IN("

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ""

    and-int/lit8 v7, p10, 0x1

    if-eqz v7, :cond_a

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ","

    :cond_a
    and-int/lit8 v7, p10, 0x2

    if-eqz v7, :cond_b

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ","

    :cond_b
    and-int/lit8 v7, p10, 0x4

    if-eqz v7, :cond_c

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_c
    const-string v3, ")"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v3, ")"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string v3, ")"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    .line 275
    :cond_e
    const-string v2, "ORDER BY "

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    packed-switch p11, :pswitch_data_0

    const-string v2, "sort_key"

    :goto_6
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    const-string v2, ", sort_key, _id"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    sget-object v2, Lcom/google/android/gms/people/internal/at;->d:[Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 283
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 284
    const-string v3, "PeopleService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Query="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  args="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    check-cast v2, Landroid/database/AbstractWindowedCursor;

    .line 289
    new-instance v3, Lcom/google/android/gms/common/data/DataHolder;

    const/4 v5, 0x0

    invoke-direct {v3, v2, v5, v4}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v3

    .line 276
    :pswitch_0
    const-string v2, "sort_key_irank DESC"

    goto :goto_6

    :pswitch_1
    const-string v2, "name COLLATE LOCALIZED"

    goto :goto_6

    :pswitch_2
    const-string v2, "affinity1 DESC"

    goto :goto_6

    :pswitch_3
    const-string v2, "affinity2 DESC"

    goto :goto_6

    :pswitch_4
    const-string v2, "affinity3 DESC"

    goto :goto_6

    :pswitch_5
    const-string v2, "affinity4 DESC"

    goto :goto_6

    :pswitch_6
    const-string v2, "affinity5 DESC"

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    .line 403
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v1

    .line 404
    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/people/sync/u;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 405
    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/people/sync/u;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 411
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 412
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v5

    const-string v6, "SELECT last_sync_start_time,last_sync_finish_time,last_successful_sync_time,last_sync_status FROM owners WHERE _id=?"

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 422
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 423
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 424
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 425
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 426
    const/4 v0, 0x3

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 429
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 431
    const-string v9, "is_tickle_sync_enabled"

    invoke-virtual {p3, v9, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 432
    const-string v8, "is_periodic_sync_enabled"

    invoke-virtual {p3, v8, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 433
    const-string v1, "last_sync_start_timestamp"

    invoke-virtual {p3, v1, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 434
    const-string v1, "last_sync_finish_timestamp"

    invoke-virtual {p3, v1, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 435
    const-string v1, "last_successful_sync_finish_timestamp"

    invoke-virtual {p3, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 436
    const-string v1, "last_sync_status"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 437
    return-void

    .line 429
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move-wide v4, v2

    move-wide v6, v2

    goto :goto_0
.end method
