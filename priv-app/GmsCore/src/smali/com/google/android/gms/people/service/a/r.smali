.class public final Lcom/google/android/gms/people/service/a/r;
.super Lcom/google/android/gms/people/service/a/d;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;)V
    .locals 9

    .prologue
    .line 45
    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/r;->g:Ljava/lang/String;

    .line 48
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/r;->h:Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 7

    .prologue
    .line 54
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const-string v0, "PeopleService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loading data from endpoint \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/r;->h:Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;

    invoke-virtual {v2}, Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_0
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/r;->h:Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/a/b;->b(Ljava/lang/String;)Lcom/google/android/gms/people/a/b;

    move-result-object v1

    .line 62
    invoke-static {p1, v1}, Lcom/google/android/gms/people/service/g;->a(Landroid/content/Context;Lcom/google/android/gms/people/a/b;)Lcom/google/android/gms/people/service/g;

    move-result-object v0

    .line 63
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/r;->h:Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;

    invoke-virtual {v2}, Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;->c()Landroid/os/Bundle;

    move-result-object v2

    .line 65
    const-string v3, "on_behalf_of"

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/r;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v3, v1, Lcom/google/android/gms/people/a/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 68
    invoke-virtual {v0}, Lcom/google/android/gms/people/service/g;->c()V

    .line 71
    :cond_1
    new-instance v4, Lcom/google/android/gms/people/service/a/s;

    invoke-direct {v4, p0, v6, v1}, Lcom/google/android/gms/people/service/a/s;-><init>(Lcom/google/android/gms/people/service/a/r;Landroid/os/Bundle;Lcom/google/android/gms/people/a/b;)V

    .line 84
    new-instance v5, Lcom/google/android/gms/people/service/a/t;

    invoke-direct {v5, p0}, Lcom/google/android/gms/people/service/a/t;-><init>(Lcom/google/android/gms/people/service/a/r;)V

    .line 92
    iget-object v3, p3, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/r;->a(Lcom/google/android/gms/people/service/g;Lcom/google/android/gms/people/a/b;Landroid/os/Bundle;Lcom/google/android/gms/common/server/ClientContext;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 99
    iget-object v1, v1, Lcom/google/android/gms/people/a/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 100
    invoke-virtual {v0}, Lcom/google/android/gms/people/service/g;->e()V

    .line 103
    :cond_2
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v1, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
