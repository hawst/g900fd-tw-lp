.class public final Lcom/google/android/gms/drive/metadata/sync/b/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/g/aw;

.field public final b:Lcom/google/android/gms/drive/database/r;

.field public final c:Lcom/google/android/gms/drive/database/model/a;

.field private final d:Lcom/google/android/gms/drive/auth/g;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/auth/g;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/aw;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/j;->a:Lcom/google/android/gms/drive/g/aw;

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/j;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/j;->b:Lcom/google/android/gms/drive/database/r;

    .line 48
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/j;->d:Lcom/google/android/gms/drive/auth/g;

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/j;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/j;->c:Lcom/google/android/gms/drive/database/model/a;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/query/Query;Ljava/util/Date;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 225
    new-instance v0, Lcom/google/android/gms/drive/query/a;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/j;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/query/a;-><init>(Lcom/google/android/gms/drive/auth/g;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->a()Lcom/google/android/gms/drive/query/Filter;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    .line 228
    :goto_0
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/gms/drive/metadata/sync/b/c;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    invoke-static {p2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/util/Date;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 225
    :cond_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/query/Filter;->a(Lcom/google/android/gms/drive/query/internal/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/b/c;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/a;)Ljava/util/Date;
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/j;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v0

    .line 217
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/c;->f()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method
