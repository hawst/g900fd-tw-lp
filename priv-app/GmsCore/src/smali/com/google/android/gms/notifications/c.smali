.class final Lcom/google/android/gms/notifications/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/c/e/a/a/a/a/b;

.field private final e:Lcom/google/android/gms/notifications/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/c/e/a/a/a/a/b;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/notifications/c;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/google/android/gms/notifications/c;->b:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/google/android/gms/notifications/c;->c:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/google/android/gms/notifications/c;->d:Lcom/google/c/e/a/a/a/a/b;

    .line 39
    new-instance v0, Lcom/google/android/gms/notifications/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/notifications/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/notifications/c;->e:Lcom/google/android/gms/notifications/a;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/c/e/b/c/a/a/a/b/a;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 45
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/notifications/c;->e:Lcom/google/android/gms/notifications/a;

    iget-object v3, p0, Lcom/google/android/gms/notifications/c;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/notifications/c;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/notifications/c;->c:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/notifications/c;->d:Lcom/google/c/e/a/a/a/a/b;

    new-instance v7, Lcom/google/c/e/b/c/a/a/a/a/a;

    invoke-direct {v7}, Lcom/google/c/e/b/c/a/a/a/a/a;-><init>()V

    const/4 v8, 0x1

    new-array v8, v8, [Lcom/google/c/e/a/a/a/a/b;

    iput-object v8, v7, Lcom/google/c/e/b/c/a/a/a/a/a;->a:[Lcom/google/c/e/a/a/a/a/b;

    iget-object v8, v7, Lcom/google/c/e/b/c/a/a/a/a/a;->a:[Lcom/google/c/e/a/a/a/a/b;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    new-instance v6, Lcom/google/c/e/a/a/a/a/e;

    invoke-direct {v6}, Lcom/google/c/e/a/a/a/a/e;-><init>()V

    iget-object v8, p0, Lcom/google/android/gms/notifications/c;->a:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget-object v8, v8, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/google/c/e/a/a/a/a/e;->b:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/notifications/c;->a:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v9, v8, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v10, 0x1e0

    if-lt v9, v10, :cond_1

    const/4 v0, 0x5

    :cond_0
    :goto_0
    iput v0, v6, Lcom/google/c/e/a/a/a/a/e;->a:I

    iput-object v6, v7, Lcom/google/c/e/b/c/a/a/a/a/a;->b:Lcom/google/c/e/a/a/a/a/e;

    invoke-static {v3, v4}, Lcom/google/android/gms/notifications/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    new-instance v4, Lcom/google/c/e/b/a/a/f;

    invoke-direct {v4}, Lcom/google/c/e/b/a/a/f;-><init>()V

    iput-object v7, v4, Lcom/google/c/e/b/a/a/f;->a:Lcom/google/c/e/b/c/a/a/a/a/a;

    sget-object v0, Lcom/google/android/gms/notifications/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v3, v5, v0}, Lcom/google/android/gms/plus/service/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/c/e/b/a/a/b;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/e/b/a/a/f;->apiHeader:Lcom/google/c/e/b/a/a/b;

    iget-object v0, v2, Lcom/google/android/gms/notifications/a;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/notifications/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/c/e/b/a/a/g;

    invoke-direct {v5}, Lcom/google/c/e/b/a/a/g;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/e/b/a/a/g;

    iget-object v0, v0, Lcom/google/c/e/b/a/a/g;->a:Lcom/google/c/e/b/c/a/a/a/b/a;

    .line 52
    :goto_1
    return-object v0

    .line 45
    :cond_1
    iget v9, v8, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v10, 0x140

    if-lt v9, v10, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    iget v9, v8, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v10, 0xf0

    if-lt v9, v10, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    iget v9, v8, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v10, 0xd5

    if-lt v9, v10, :cond_4

    const/4 v0, 0x4

    goto :goto_0

    :cond_4
    iget v8, v8, Landroid/util/DisplayMetrics;->densityDpi:I
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v9, 0xa0

    if-ge v8, v9, :cond_0

    move v0, v1

    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    :goto_2
    const-string v1, "GnotsFetchOperation"

    const-string v2, "Failed to fetch notification by identifer."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 52
    const/4 v0, 0x0

    goto :goto_1

    .line 48
    :catch_1
    move-exception v0

    goto :goto_2
.end method
