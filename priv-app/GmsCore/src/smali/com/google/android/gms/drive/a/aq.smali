.class public final Lcom/google/android/gms/drive/a/aq;
.super Lcom/google/android/gms/drive/a/b;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;)V
    .locals 6

    .prologue
    .line 29
    sget-object v1, Lcom/google/android/gms/drive/a/e;->i:Lcom/google/android/gms/drive/a/e;

    sget-object v5, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;)V

    .line 31
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/gms/drive/a/e;->i:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 4

    .prologue
    .line 40
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->h()V

    .line 41
    new-instance v0, Lcom/google/android/gms/drive/a/z;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v3, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/a/z;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 53
    if-ne p0, p1, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 61
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "UndoCreateShortcutFileAction[%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/aq;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
