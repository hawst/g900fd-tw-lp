.class public final Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/gencode/authzen/server/api/p;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/q;

.field private static final g:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:J

.field e:Z

.field f:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/q;

    invoke-direct {v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/q;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 62
    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->g:Ljava/util/HashMap;

    const-string v1, "callbackBluetoothAddress"

    const-string v2, "callbackBluetoothAddress"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->g:Ljava/util/HashMap;

    const-string v1, "maxLastUpdateTimeDeltaMillis"

    const-string v2, "maxLastUpdateTimeDeltaMillis"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->g:Ljava/util/HashMap;

    const-string v1, "offlineAllowed"

    const-string v2, "offlineAllowed"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->g:Ljava/util/HashMap;

    const-string v1, "retryCount"

    const-string v2, "retryCount"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 117
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->b:I

    .line 118
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a:Ljava/util/Set;

    .line 119
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;JZI)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 130
    iput-object p1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a:Ljava/util/Set;

    .line 131
    iput p2, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->b:I

    .line 132
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->c:Ljava/lang/String;

    .line 133
    iput-wide p4, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->d:J

    .line 134
    iput-boolean p6, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->e:Z

    .line 135
    iput p7, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->f:I

    .line 136
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 279
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 280
    packed-switch v0, :pswitch_data_0

    .line 285
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an int."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 282
    :pswitch_0
    iput p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->f:I

    .line 288
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 289
    return-void

    .line 280
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 294
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 295
    packed-switch v0, :pswitch_data_0

    .line 300
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a long."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 297
    :pswitch_0
    iput-wide p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->d:J

    .line 303
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 304
    return-void

    .line 295
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 324
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 325
    packed-switch v0, :pswitch_data_0

    .line 330
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 327
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->c:Ljava/lang/String;

    .line 333
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 334
    return-void

    .line 325
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 309
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 310
    packed-switch v0, :pswitch_data_0

    .line 315
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 312
    :pswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->e:Z

    .line 318
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 319
    return-void

    .line 310
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 249
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 259
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->c:Ljava/lang/String;

    .line 257
    :goto_0
    return-object v0

    .line 253
    :pswitch_1
    iget-wide v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 255
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 257
    :pswitch_3
    iget v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 249
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 23
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/q;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 362
    instance-of v0, p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 393
    :goto_0
    return v0

    .line 367
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 368
    goto :goto_0

    .line 371
    :cond_1
    check-cast p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;

    .line 372
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 373
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 374
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 376
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 378
    goto :goto_0

    :cond_3
    move v0, v1

    .line 383
    goto :goto_0

    .line 386
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 388
    goto :goto_0

    :cond_5
    move v0, v2

    .line 393
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 349
    const/4 v0, 0x0

    .line 350
    sget-object v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 351
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 352
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 353
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 355
    goto :goto_0

    .line 356
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 239
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/q;

    invoke-static {p0, p1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/q;->a(Lcom/google/android/gms/auth/gencode/authzen/server/api/FindEligibleUnlockDevicesRequestEntity;Landroid/os/Parcel;)V

    .line 240
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x1

    return v0
.end method
