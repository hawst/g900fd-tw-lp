.class public final Lcom/google/android/gms/games/ui/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(ILjava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 313
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The account name to use in the dest app cannot be null"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 316
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.API_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 317
    const-string v1, "com.google.android.gms.games.SCREEN"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 318
    const-string v1, "com.google.android.gms.games.MIN_VERSION"

    const v2, 0x1312d00

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 320
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 321
    invoke-static {v1, p1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 322
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 324
    return-object v0

    .line 313
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/Integer;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 392
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 399
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 400
    if-eqz v1, :cond_0

    .line 401
    const-string v2, "com.google.android.gms.version"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 403
    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 405
    :cond_0
    :goto_0
    return-object v0

    .line 395
    :catch_0
    move-exception v1

    const-string v1, "GamesDestApi"

    const-string v2, "Cannot find the destination app. We should have check that already."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 411
    instance-of v0, p0, Lcom/google/android/gms/games/ui/bj;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 412
    check-cast p0, Lcom/google/android/gms/games/ui/bj;

    invoke-interface {p0}, Lcom/google/android/gms/games/ui/bj;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    .line 414
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "This method can only be called from client or headless UI"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 416
    return-void

    .line 414
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 420
    instance-of v0, p0, Lcom/google/android/gms/games/ui/a/b;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 421
    check-cast p0, Lcom/google/android/gms/games/ui/a/b;

    .line 422
    invoke-interface {p0}, Lcom/google/android/gms/games/ui/a/b;->B()Lcom/google/android/gms/games/ui/a/a;

    move-result-object v0

    .line 424
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/a/a;->a(Landroid/content/Intent;)V

    .line 425
    return-void
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;ZLandroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 248
    invoke-static {p0}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;)V

    .line 250
    const/16 v0, 0x44d

    invoke-static {v0, p1}, Lcom/google/android/gms/games/ui/a/c;->a(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 253
    if-eqz p2, :cond_0

    .line 254
    const-string v1, "com.google.android.gms.games.FORCE_RESOLVE_ACCOUNT_KEY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 258
    :cond_0
    if-eqz p3, :cond_1

    .line 259
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 262
    :cond_1
    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 263
    return-void
.end method
