.class final Lcom/google/android/gms/plus/plusone/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/aj;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/plusone/p;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/plusone/p;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/a;)V
    .locals 6

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-static {v0}, Lcom/google/android/gms/plus/plusone/p;->f(Lcom/google/android/gms/plus/plusone/p;)Z

    .line 131
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_2

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_1

    .line 135
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-static {v1}, Lcom/google/android/gms/plus/plusone/p;->g(Lcom/google/android/gms/plus/plusone/p;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/plus/a/g;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/h;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-static {v5}, Lcom/google/android/gms/plus/plusone/p;->h(Lcom/google/android/gms/plus/plusone/p;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 141
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 142
    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-static {v2}, Lcom/google/android/gms/plus/plusone/p;->c(Lcom/google/android/gms/plus/plusone/p;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v1, "url"

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-static {v2}, Lcom/google/android/gms/plus/plusone/p;->c(Lcom/google/android/gms/plus/plusone/p;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v1, "type"

    const-string v2, "article"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    new-instance v2, Lcom/google/android/gms/plus/data/a/a;

    invoke-direct {v2, v0}, Lcom/google/android/gms/plus/data/a/a;-><init>(Landroid/content/ContentValues;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/plusone/p;->a(Lcom/google/android/gms/plus/plusone/p;Lcom/google/android/gms/plus/data/a/a;)Lcom/google/android/gms/plus/data/a/a;

    .line 150
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-static {v1}, Lcom/google/android/gms/plus/plusone/p;->i(Lcom/google/android/gms/plus/plusone/p;)Lcom/google/android/gms/plus/data/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/plusone/p;->b(Lcom/google/android/gms/plus/plusone/p;Lcom/google/android/gms/plus/data/a/a;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    iget-object v1, v1, Lcom/google/android/gms/plus/plusone/p;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/plusone/p;->b(Landroid/widget/ProgressBar;)V

    .line 152
    return-void

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/r;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-static {v0, p2}, Lcom/google/android/gms/plus/plusone/p;->a(Lcom/google/android/gms/plus/plusone/p;Lcom/google/android/gms/plus/data/a/a;)Lcom/google/android/gms/plus/data/a/a;

    goto :goto_0
.end method
