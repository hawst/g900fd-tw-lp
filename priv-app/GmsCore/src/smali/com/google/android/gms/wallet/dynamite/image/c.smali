.class public final Lcom/google/android/gms/wallet/dynamite/image/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(IIII)I
    .locals 4

    .prologue
    .line 59
    const/4 v0, 0x1

    .line 60
    if-gt p1, p3, :cond_0

    if-le p0, p2, :cond_2

    .line 62
    :cond_0
    if-le p0, p1, :cond_1

    .line 63
    int-to-float v0, p1

    int-to-float v1, p3

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 68
    :goto_0
    mul-int v1, p0, p1

    int-to-float v1, v1

    .line 69
    mul-int v2, p2, p3

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    .line 71
    :goto_1
    mul-int v3, v0, v0

    int-to-float v3, v3

    div-float v3, v1, v3

    cmpl-float v3, v3, v2

    if-lez v3, :cond_2

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 65
    :cond_1
    int-to-float v0, p0

    int-to-float v1, p2

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 75
    :cond_2
    return v0
.end method
