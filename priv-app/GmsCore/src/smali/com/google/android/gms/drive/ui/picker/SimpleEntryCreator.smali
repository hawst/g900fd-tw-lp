.class public Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/ac;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/picker/ac;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->a:Ljava/lang/String;

    .line 28
    iput p2, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->c:I

    .line 29
    iput p3, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->b:I

    .line 30
    iput p4, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->d:I

    .line 31
    iput p5, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->e:I

    .line 32
    iput p6, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->f:I

    .line 33
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->a:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 52
    return-void
.end method
