.class public Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Lcom/google/android/gms/drive/ui/picker/a/q;

.field private final b:Z

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/gms/drive/ui/legacy/navigation/d;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/a/q;->valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/a/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->createBooleanArray()[Z

    move-result-object v0

    .line 101
    const/4 v1, 0x0

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->b:Z

    .line 102
    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->d:Z

    .line 103
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/ui/picker/a/q;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->b:Z

    .line 44
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->d:Z

    .line 46
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/Filter;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/q;->b()Lcom/google/android/gms/drive/query/Filter;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->d:Z

    return v0
.end method

.method public final c()Lcom/google/android/gms/drive/ui/picker/a/q;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    if-ne p1, p0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;

    if-eqz v2, :cond_3

    .line 65
    check-cast p1, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;

    .line 66
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    iget-object v3, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/ui/picker/a/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->b:Z

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 70
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 80
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 126
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "EntriesFilterCriterion {accountName=%s, filter=%s, isInheritable=%s, isMainFilter=%s}"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/ui/picker/a/q;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->b:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-boolean v4, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->d:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/q;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x2

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->b:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->d:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 95
    return-void
.end method
