.class public Lcom/google/android/gms/notifications/GunsService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Random;

.field private b:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 50
    const-string v0, "GnotsService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 52
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/notifications/GunsService;->a:Ljava/util/Random;

    .line 53
    return-void
.end method

.method private a(Landroid/content/Intent;Lcom/google/c/e/a/a/a/a/c;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 225
    const-string v0, "com.google.android.gms.notifications.intents.accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const-string v0, "com.google.android.gms.notifications.intents.accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 244
    :goto_0
    return-object v0

    .line 230
    :cond_0
    iget-object v0, p2, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    iget-object v1, v0, Lcom/google/c/e/a/a/a/a/d;->a:Ljava/lang/String;

    .line 231
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 235
    :try_start_0
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 236
    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/google/android/gms/auth/r;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 237
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 238
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    :goto_1
    const-string v1, "GnotsService"

    const-string v2, "Failed to get account ID."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 244
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 241
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private static a(Lcom/google/c/e/a/a/a/a/c;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Intent;Lcom/google/c/e/a/a/a/a/c;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 332
    new-instance v1, Lcom/google/android/gms/notifications/c;

    iget-object v2, p2, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/d;->a:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    invoke-direct {v1, v0, p3, v2, v3}, Lcom/google/android/gms/notifications/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/c/e/a/a/a/a/b;)V

    .line 335
    invoke-virtual {v1}, Lcom/google/android/gms/notifications/c;->a()Lcom/google/c/e/b/c/a/a/a/b/a;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/c/e/b/c/a/a/a/b/a;->a:[Lcom/google/c/e/a/a/a/a/c;

    array-length v1, v1

    if-eqz v1, :cond_0

    .line 339
    iget-object v1, v0, Lcom/google/c/e/b/c/a/a/a/b/a;->a:[Lcom/google/c/e/a/a/a/a/c;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 340
    invoke-direct {p0, p1, v3}, Lcom/google/android/gms/notifications/GunsService;->b(Landroid/content/Intent;Lcom/google/c/e/a/a/a/a/c;)V

    .line 339
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 343
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/notifications/GunsService;->b(Landroid/content/Intent;Lcom/google/c/e/a/a/a/a/c;)V

    .line 345
    :cond_1
    return-void
.end method

.method private a(Lcom/google/c/e/a/a/a/a/c;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 309
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    new-instance v0, Lcom/google/android/gms/notifications/l;

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p1, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    iget-object v3, v2, Lcom/google/c/e/a/a/a/a/d;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/notifications/l;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/c/e/a/a/a/a/b;I)V

    .line 312
    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/notifications/l;->d:Lcom/google/c/e/a/a/a/a/b;

    iget v2, v0, Lcom/google/android/gms/notifications/l;->e:I

    new-instance v3, Lcom/google/c/e/b/c/a/a/a/a/b;

    invoke-direct {v3}, Lcom/google/c/e/b/c/a/a/a/a/b;-><init>()V

    iput-object v1, v3, Lcom/google/c/e/b/c/a/a/a/a/b;->a:Lcom/google/c/e/a/a/a/a/b;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v3, Lcom/google/c/e/b/c/a/a/a/a/b;->b:Ljava/lang/Integer;

    new-instance v2, Lcom/google/c/e/b/c/a/a/a/a/c;

    invoke-direct {v2}, Lcom/google/c/e/b/c/a/a/a/a/c;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/c/e/b/c/a/a/a/a/b;

    iput-object v1, v2, Lcom/google/c/e/b/c/a/a/a/a/c;->a:[Lcom/google/c/e/b/c/a/a/a/a/b;

    iget-object v1, v2, Lcom/google/c/e/b/c/a/a/a/a/c;->a:[Lcom/google/c/e/b/c/a/a/a/a/b;

    const/4 v4, 0x0

    aput-object v3, v1, v4

    iget-object v3, v0, Lcom/google/android/gms/notifications/l;->f:Lcom/google/android/gms/notifications/a;

    iget-object v4, v0, Lcom/google/android/gms/notifications/l;->a:Landroid/content/Context;

    iget-object v1, v0, Lcom/google/android/gms/notifications/l;->b:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/gms/notifications/l;->c:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/google/android/gms/notifications/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    new-instance v6, Lcom/google/c/e/b/a/a/h;

    invoke-direct {v6}, Lcom/google/c/e/b/a/a/h;-><init>()V

    iput-object v2, v6, Lcom/google/c/e/b/a/a/h;->a:Lcom/google/c/e/b/c/a/a/a/a/c;

    sget-object v0, Lcom/google/android/gms/notifications/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v4, v5, v0}, Lcom/google/android/gms/plus/service/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/c/e/b/a/a/b;

    move-result-object v0

    iput-object v0, v6, Lcom/google/c/e/b/a/a/h;->apiHeader:Lcom/google/c/e/b/a/a/b;

    iget-object v0, v3, Lcom/google/android/gms/notifications/a;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/notifications/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v6}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/c/e/b/a/a/i;

    invoke-direct {v5}, Lcom/google/c/e/b/a/a/i;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/e/b/a/a/i;

    iget-object v0, v0, Lcom/google/c/e/b/a/a/i;->a:Lcom/google/c/e/b/c/a/a/a/b/b;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 312
    :catch_0
    move-exception v0

    :goto_1
    const-string v1, "GnotsSRSOperation"

    const-string v2, "Failed to update the notification(s) read state."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private b(Landroid/content/Intent;Lcom/google/c/e/a/a/a/a/c;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 349
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 350
    new-instance v3, Landroid/content/Intent;

    const-class v0, Lcom/google/android/gms/notifications/GcmBroadcastReceiver;

    invoke-direct {v3, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "com.google.android.gms.notifications.intents.SHOW_NOTIFICATION"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 352
    const-string v0, "GunsService.refetch"

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 353
    const-string v4, "gms.gnots.payload"

    if-eqz p2, :cond_0

    invoke-static {p2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    const/16 v5, 0x9

    invoke-static {v0, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 356
    return-void

    :cond_0
    move-object v0, v1

    .line 353
    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/gms/notifications/GunsService;->b:Landroid/app/NotificationManager;

    .line 61
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v10, 0x2

    .line 66
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 69
    invoke-static {p1}, Lcom/google/android/gms/notifications/k;->a(Landroid/content/Intent;)Lcom/google/c/e/a/a/a/a/c;

    move-result-object v3

    .line 70
    if-nez v3, :cond_0

    .line 71
    const-string v0, "GnotsService"

    const-string v1, "Failed to retrieve payload from intent."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    invoke-static {p1}, Lcom/google/android/gms/notifications/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 105
    :goto_0
    return-void

    .line 76
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, v3}, Lcom/google/android/gms/notifications/GunsService;->a(Landroid/content/Intent;Lcom/google/c/e/a/a/a/a/c;)Ljava/lang/String;

    move-result-object v4

    .line 79
    iget-boolean v5, v3, Lcom/google/c/e/a/a/a/a/c;->c:Z

    if-nez v5, :cond_1

    const-string v5, "com.google.android.gms.notifications.intents.CANCEL_NOTIFICATION"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 81
    :cond_1
    const-string v0, "GnotsService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GnotsService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cancelling notification with key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsService;->b:Landroid/app/NotificationManager;

    invoke-static {v3}, Lcom/google/android/gms/notifications/GunsService;->a(Lcom/google/c/e/a/a/a/a/c;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    const/4 v0, 0x3

    invoke-direct {p0, v3, v4, v0}, Lcom/google/android/gms/notifications/GunsService;->a(Lcom/google/c/e/a/a/a/a/c;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :cond_3
    :goto_1
    invoke-static {p1}, Lcom/google/android/gms/notifications/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    goto :goto_0

    .line 82
    :cond_4
    :try_start_2
    const-string v5, "com.google.android.gms.notifications.intents.DISMISS_NOTIFICATION"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 83
    iget-object v0, v3, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/b;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "GnotsService"

    const-string v1, "Dismissed a notification without key."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 104
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/gms/notifications/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    throw v0

    .line 83
    :cond_5
    :try_start_3
    const-string v0, "GnotsService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "GnotsService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User dismissed notification with key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/4 v0, 0x4

    invoke-direct {p0, v3, v4, v0}, Lcom/google/android/gms/notifications/GunsService;->a(Lcom/google/c/e/a/a/a/a/c;Ljava/lang/String;I)V

    goto :goto_1

    .line 84
    :cond_7
    const-string v5, "com.google.android.gms.notifications.intents.START_SERVICE"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_16

    .line 86
    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-eqz v2, :cond_8

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    if-eqz v2, :cond_8

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    if-eqz v2, :cond_8

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/h;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    const-string v1, "GnotsService"

    const-string v2, "Payload contains insufficient data to show the system notification."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    :cond_9
    if-eqz v1, :cond_15

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    if-eqz v2, :cond_a

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/e;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->f:Lcom/google/c/e/a/a/a/a/e;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/e;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :goto_2
    if-nez v1, :cond_b

    const-string v1, "GunsService.refetch"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-direct {p0, p1, v3, v4}, Lcom/google/android/gms/notifications/GunsService;->a(Landroid/content/Intent;Lcom/google/c/e/a/a/a/a/c;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    move v1, v0

    goto :goto_2

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v1, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->b:Lcom/google/c/e/a/a/a/a/a;

    if-eqz v1, :cond_10

    iget-object v1, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->b:Lcom/google/c/e/a/a/a/a/a;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/a;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/notifications/f;->a(Landroid/content/Context;)Lcom/google/android/gms/notifications/f;

    move-result-object v1

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->b:Lcom/google/c/e/a/a/a/a/a;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/notifications/f;->a(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/notifications/f;->a([B)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/notifications/f;->a(Landroid/content/Context;)Lcom/google/android/gms/notifications/f;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/notifications/f;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    iget-object v1, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget v1, v1, Lcom/google/c/e/a/a/a/a/f;->a:I

    const/4 v7, -0x2

    if-lt v1, v7, :cond_11

    iget-object v1, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget v1, v1, Lcom/google/c/e/a/a/a/a/f;->a:I

    if-gt v1, v10, :cond_11

    iget-object v0, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget v0, v0, Lcom/google/c/e/a/a/a/a/f;->a:I

    move v1, v0

    :goto_4
    invoke-static {v3}, Lcom/google/android/gms/notifications/k;->a(Lcom/google/c/e/a/a/a/a/c;)I

    move-result v0

    const/4 v7, -0x1

    if-eq v0, v7, :cond_12

    :goto_5
    new-instance v7, Landroid/support/v4/app/bk;

    invoke-direct {v7, v5}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v0}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    iget-object v5, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v5, v5, Lcom/google/c/e/a/a/a/a/f;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    iget-object v5, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v5, v5, Lcom/google/c/e/a/a/a/a/f;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    iget-object v5, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v5, v5, Lcom/google/c/e/a/a/a/a/f;->d:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, p1, v4}, Lcom/google/android/gms/notifications/g;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/notifications/GunsService;->a:Ljava/util/Random;

    invoke-virtual {v8}, Ljava/util/Random;->nextInt()I

    move-result v8

    const/high16 v9, 0x8000000

    invoke-static {v5, v8, v7, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, v0, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/google/android/gms/notifications/GcmBroadcastReceiver;

    invoke-direct {v7, v5, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v8, "com.google.android.gms.notifications.intents.DISMISS_NOTIFICATION"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v8, p0, Lcom/google/android/gms/notifications/GunsService;->a:Ljava/util/Random;

    invoke-virtual {v8}, Ljava/util/Random;->nextInt()I

    move-result v8

    const/high16 v9, 0x8000000

    invoke-static {v5, v8, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/support/v4/app/bk;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v0

    iput v1, v0, Landroid/support/v4/app/bk;->j:I

    if-ne v1, v10, :cond_13

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v5, Landroid/content/Intent;

    const-class v7, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;

    invoke-direct {v5, v1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "com.google.android.gms.notifications.intents.SHOW_NOTIFICATION_DIALOG"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v7, "com.google.android.gms.notifications.intents.accountName"

    invoke-virtual {v5, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "com.google.android.gms.notifications.intents.ICON_FILE_PATH"

    invoke-virtual {v5, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/notifications/GunsService;->a:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    const/high16 v7, 0x8000000

    invoke-static {v1, v2, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/bk;->e:Landroid/app/PendingIntent;

    const/16 v1, 0x80

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/bk;->a(IZ)V

    :cond_c
    :goto_6
    if-eqz v6, :cond_d

    iput-object v6, v0, Landroid/support/v4/app/bk;->g:Landroid/graphics/Bitmap;

    :cond_d
    iget-object v1, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    new-instance v1, Landroid/support/v4/app/bj;

    invoke-direct {v1}, Landroid/support/v4/app/bj;-><init>()V

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bj;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/bj;

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_14

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bj;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bj;

    :goto_7
    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bj;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bj;

    :cond_e
    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->a(Landroid/support/v4/app/bv;)Landroid/support/v4/app/bk;

    :cond_f
    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/notifications/GunsService;->b:Landroid/app/NotificationManager;

    invoke-static {v3}, Lcom/google/android/gms/notifications/GunsService;->a(Lcom/google/c/e/a/a/a/a/c;)Ljava/lang/String;

    move-result-object v2

    const/16 v5, 0x3e8

    invoke-virtual {v1, v2, v5, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    const/4 v0, 0x2

    invoke-direct {p0, v3, v4, v0}, Lcom/google/android/gms/notifications/GunsService;->a(Lcom/google/c/e/a/a/a/a/c;Ljava/lang/String;I)V

    goto/16 :goto_1

    :cond_10
    const/4 v1, 0x0

    move-object v2, v1

    goto/16 :goto_3

    :cond_11
    move v1, v0

    goto/16 :goto_4

    :cond_12
    const v0, 0x1080027

    goto/16 :goto_5

    :cond_13
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_6

    :cond_14
    iget-object v2, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bj;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bj;

    goto :goto_7

    .line 93
    :cond_15
    iget-object v0, v3, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-nez v0, :cond_3

    const-string v0, "GunsService.refetch"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    invoke-direct {p0, p1, v3, v4}, Lcom/google/android/gms/notifications/GunsService;->a(Landroid/content/Intent;Lcom/google/c/e/a/a/a/a/c;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 99
    :cond_16
    const-string v0, "GnotsService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    const-string v0, "GnotsService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Intent not handled - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method
