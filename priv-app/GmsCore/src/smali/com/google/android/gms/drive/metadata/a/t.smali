.class final Lcom/google/android/gms/drive/metadata/a/t;
.super Lcom/google/android/gms/drive/metadata/a/ai;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/metadata/f;)V
    .locals 2

    .prologue
    .line 654
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/metadata/a/ai;-><init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 654
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final bridge synthetic a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 654
    return-void
.end method

.method protected final synthetic a(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 654
    check-cast p2, Lcom/google/android/gms/common/data/BitmapTeleporter;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->J()Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File;->a(Lcom/google/android/gms/drive/internal/model/File$Thumbnail;)Lcom/google/android/gms/drive/internal/model/File;

    :cond_0
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->WEBP:Landroid/graphics/Bitmap$CompressFormat;

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->J()Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/data/BitmapTeleporter;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gms/drive/metadata/a/t;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/model/File;->J()Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/metadata/a/aj;->a:[I

    invoke-virtual {v0}, Landroid/graphics/Bitmap$CompressFormat;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected CompressFormat: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    goto :goto_0

    :pswitch_0
    const-string v0, "image/jpeg"

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;->f(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    return-void

    :pswitch_1
    const-string v0, "image/png"

    goto :goto_1

    :pswitch_2
    const-string v0, "image/webp"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
