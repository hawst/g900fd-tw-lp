.class final Lcom/google/android/gms/feedback/r;
.super Lcom/google/android/gms/feedback/b/c;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/feedback/ErrorReport;

.field private b:Landroid/app/Service;


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/google/android/gms/feedback/b/c;-><init>()V

    .line 165
    iput-object p1, p0, Lcom/google/android/gms/feedback/r;->b:Landroid/app/Service;

    .line 166
    return-void
.end method

.method private a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 169
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 171
    iget-object v2, p0, Lcom/google/android/gms/feedback/r;->b:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 173
    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 174
    array-length v3, v1

    if-nez v3, :cond_0

    .line 188
    :goto_0
    return v0

    .line 179
    :cond_0
    new-instance v3, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v3}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    iput-object v3, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    .line 180
    iget-object v3, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, v3, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    aget-object v4, v1, v0

    iput-object v4, v3, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    .line 183
    iget-object v3, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    aget-object v4, v1, v0

    iput-object v4, v3, Lcom/google/android/gms/feedback/ErrorReport;->S:Ljava/lang/String;

    .line 184
    iget-object v3, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, v3, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    const/16 v4, 0xb

    iput v4, v3, Landroid/app/ApplicationErrorReport;->type:I

    .line 186
    iget-object v3, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, v3, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    aget-object v0, v1, v0

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Landroid/app/ApplicationErrorReport;->installerPackageName:Ljava/lang/String;

    .line 188
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 5

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/google/android/gms/feedback/r;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    :try_start_0
    const-class v0, Landroid/view/Surface;

    const-string v1, "screenshot"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 210
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 211
    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 212
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    invoke-static {v0}, Lcom/google/android/gms/feedback/Screenshot;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v0

    .line 216
    iget-object v1, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {v1, v0}, Lcom/google/android/gms/feedback/e;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 220
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 222
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/feedback/ErrorReport;)Z
    .locals 4

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/google/android/gms/feedback/r;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    const/4 v0, 0x0

    .line 239
    :goto_0
    return v0

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/r;->b:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/feedback/FeedbackService;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/ErrorReport;Ljava/io/File;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/feedback/r;->b:Landroid/app/Service;

    const-class v3, Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.android.feedback.SAFEPARCELABLE_REPORT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/feedback/r;->b:Landroid/app/Service;

    invoke-virtual {v0, v1}, Landroid/app/Service;->startActivity(Landroid/content/Intent;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/feedback/r;->b:Landroid/app/Service;

    invoke-virtual {v0}, Landroid/app/Service;->stopSelf()V

    .line 239
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/feedback/ErrorReport;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 244
    invoke-direct {p0}, Lcom/google/android/gms/feedback/r;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 246
    const/4 v0, 0x0

    .line 252
    :goto_0
    return v0

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, p0, Lcom/google/android/gms/feedback/r;->b:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-static {v1, p1, v2}, Lcom/google/android/gms/feedback/FeedbackService;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/ErrorReport;Ljava/io/File;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    .line 249
    iget-object v1, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-boolean v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    .line 250
    iget-object v1, p0, Lcom/google/android/gms/feedback/r;->b:Landroid/app/Service;

    iget-object v2, p0, Lcom/google/android/gms/feedback/r;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {v1, v2}, Lcom/google/android/gms/feedback/FeedbackAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 251
    iget-object v1, p0, Lcom/google/android/gms/feedback/r;->b:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->stopSelf()V

    goto :goto_0
.end method
