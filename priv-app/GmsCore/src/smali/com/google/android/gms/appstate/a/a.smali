.class public final Lcom/google/android/gms/appstate/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/appstate/d/a/c;

.field private final b:Lcom/google/android/gms/appstate/d/a/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    new-instance v0, Lcom/google/android/gms/appstate/d/a/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/appstate/d/a/c;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/appstate/a/a;->a:Lcom/google/android/gms/appstate/d/a/c;

    .line 100
    new-instance v0, Lcom/google/android/gms/appstate/d/a/d;

    invoke-direct {v0, p2}, Lcom/google/android/gms/appstate/d/a/d;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/appstate/a/a;->b:Lcom/google/android/gms/appstate/d/a/d;

    .line 101
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZ)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 474
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/appstate/a/a;->a:Lcom/google/android/gms/appstate/d/a/c;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Lcom/google/android/gms/appstate/d/a/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/Integer;)Lcom/google/android/gms/appstate/d/a/a;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 485
    invoke-static {p1, p2}, Lcom/google/android/gms/appstate/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    .line 486
    iget-object v1, v1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 487
    const-string v4, "app_id"

    invoke-virtual {v1, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    const-string v4, "key"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 489
    const-string v4, "data_dirty"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 490
    const-string v4, "upsync_required"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 491
    const-string v4, "client_context_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 492
    const-string v4, "local_data"

    invoke-virtual {v1, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 493
    const-string v4, "local_data"

    invoke-virtual {v1, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 497
    :cond_0
    invoke-static {v2, v3, p3, p4}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 498
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 499
    :goto_0
    return v0

    .line 475
    :catch_0
    move-exception v0

    .line 476
    const-string v1, "AppStateAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to retrieve state for app "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const/16 v1, 0x194

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 478
    const/16 v0, 0x7d2

    goto :goto_0

    .line 480
    :cond_1
    if-eqz p5, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;Ljava/lang/String;I)I
    .locals 4

    .prologue
    .line 746
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/a;->a:Lcom/google/android/gms/appstate/d/a/c;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/appstate/d/a/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/Integer;)Lcom/google/android/gms/appstate/d/a/a;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 756
    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-static {v0, p2}, Lcom/google/android/gms/appstate/a/a;->a(Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    .line 757
    const/16 v0, 0x7d0

    :goto_0
    return v0

    .line 747
    :catch_0
    move-exception v0

    .line 748
    const-string v1, "AppStateAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to retrieve state for app "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const/16 v1, 0x194

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750
    const/16 v0, 0x7d2

    goto :goto_0

    .line 752
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private static a(Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 761
    const-string v0, "local_version"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 762
    const-string v1, "local_data"

    invoke-virtual {p0, v1}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 763
    const-string v2, "upsync_required"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 764
    const-string v2, "conflict_version"

    invoke-virtual {p1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    const-string v0, "conflict_data"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 766
    return-void
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;[B)I
    .locals 10

    .prologue
    .line 630
    invoke-static {p1, p2}, Lcom/google/android/gms/appstate/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v8

    .line 631
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    .line 632
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/appstate/a/a;->a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;Ljava/lang/String;ILjava/lang/String;[B)I

    move-result v0

    .line 634
    sparse-switch v0, :sswitch_data_0

    .line 648
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown status code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 642
    :sswitch_0
    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 643
    invoke-static {v8, v9, p3, p4}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v1

    .line 644
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 650
    :cond_0
    return v0

    .line 634
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x7d0 -> :sswitch_0
        0x7d2 -> :sswitch_0
        0x7d3 -> :sswitch_0
        0x7d4 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 379
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/a;->b:Lcom/google/android/gms/appstate/d/a/d;

    const-string v1, "wipe"

    iget-object v0, v0, Lcom/google/android/gms/appstate/d/a/d;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v2, v1, v3}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/appstate/provider/d;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 391
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 392
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 380
    :catch_0
    move-exception v0

    .line 381
    const-string v1, "AppStateAgent"

    const-string v2, "Unable to wipe app state"

    invoke-static {v1, v2}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-static {}, Lcom/google/android/gms/appstate/c/g;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 383
    const-string v1, "AppStateAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 385
    :cond_0
    const/4 v0, 0x6

    goto :goto_0
.end method

.method final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 17

    .prologue
    .line 504
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/appstate/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v8

    .line 505
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    .line 506
    move-object/from16 v0, p3

    invoke-static {v8, v9, v0}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 511
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/appstate/a/a;->a:Lcom/google/android/gms/appstate/d/a/c;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const-string v5, "states"

    if-eqz v3, :cond_0

    const-string v4, "includeData"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v4, v3}, Lcom/google/android/gms/appstate/d/a/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_0
    iget-object v2, v2, Lcom/google/android/gms/appstate/d/a/c;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const-class v7, Lcom/google/android/gms/appstate/d/a/b;

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/appstate/d/a/b;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 522
    invoke-virtual {v2}, Lcom/google/android/gms/appstate/d/a/b;->getItems()Ljava/util/ArrayList;

    move-result-object v6

    .line 523
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 525
    sget-object v2, Lcom/google/android/gms/appstate/a/c;->a:[Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v11

    .line 527
    :cond_1
    :goto_0
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 529
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 530
    const/4 v2, 0x5

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 531
    const/4 v2, 0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    move v5, v2

    .line 532
    :goto_1
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 534
    invoke-static {v12, v13}, Lcom/google/android/gms/appstate/provider/d;->a(J)Landroid/net/Uri;

    move-result-object v12

    .line 537
    const/4 v2, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v13

    move v4, v2

    :goto_2
    if-ge v4, v13, :cond_1

    .line 538
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/appstate/d/a/a;

    .line 539
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/appstate/d/a/a;

    iget-object v3, v3, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 540
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 541
    invoke-virtual {v2}, Lcom/google/android/gms/appstate/d/a/a;->b()Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 543
    if-eqz v5, :cond_4

    .line 546
    invoke-static {v3, v15}, Lcom/google/android/gms/appstate/a/a;->a(Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    .line 552
    :goto_3
    invoke-static {v12}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 556
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 578
    :catchall_0
    move-exception v2

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v2

    .line 513
    :catch_0
    move-exception v2

    const-string v2, "AppStateAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to retrieve state for app "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    sget-object v2, Lcom/google/android/gms/appstate/a/c;->a:[Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v2

    .line 516
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 517
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 518
    if-lez v3, :cond_2

    const/4 v2, 0x3

    .line 609
    :goto_4
    return v2

    .line 518
    :cond_2
    const/4 v2, 0x4

    goto :goto_4

    .line 531
    :cond_3
    const/4 v2, 0x0

    move v5, v2

    goto :goto_1

    .line 550
    :cond_4
    :try_start_2
    const-string v2, "local_version"

    invoke-virtual {v3, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "local_data"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v3

    const-string v5, "upsync_required"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v15, v5, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "local_version"

    invoke-virtual {v15, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "local_data"

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_3

    .line 566
    :cond_5
    const-string v2, "upsync_required"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 567
    const-string v2, "data_dirty"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 568
    const-string v2, "conflict_version"

    invoke-virtual {v15, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 569
    const-string v2, "conflict_data"

    invoke-virtual {v15, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 570
    invoke-static {v12}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 574
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 537
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_2

    .line 578
    :cond_6
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 583
    const/4 v2, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_5
    if-ge v3, v4, :cond_8

    .line 584
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/appstate/d/a/a;

    iget-object v2, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 586
    const-string v5, "app_id"

    move-object/from16 v0, p3

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    const-string v5, "data_dirty"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v2, v5, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 588
    const-string v5, "upsync_required"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v2, v5, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 589
    const-string v5, "client_context_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v2, v5, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 590
    const-string v5, "local_data"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 591
    const-string v5, "local_data"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 595
    :cond_7
    const-string v5, "key"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object/from16 v0, p3

    invoke-static {v8, v9, v0, v5}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v5

    .line 597
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 601
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 605
    :cond_8
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_9

    .line 606
    const-string v2, "AppStateAgent"

    invoke-static {v10, v7, v2}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 609
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_4
.end method

.method final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)I
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 156
    .line 160
    invoke-static {p1, p2}, Lcom/google/android/gms/appstate/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    .line 161
    invoke-static {v2, v3, p3, p4}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v1

    .line 162
    sget-object v2, Lcom/google/android/gms/appstate/a/c;->a:[Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v2

    .line 164
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 166
    const/4 v1, 0x1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_1

    move v1, v5

    .line 167
    :goto_0
    const/4 v0, 0x3

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 168
    const/4 v3, 0x2

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    move-object v7, v0

    move v0, v1

    .line 171
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 177
    if-eqz v5, :cond_0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 179
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/appstate/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZ)I

    move-result v0

    .line 186
    :goto_2
    return v0

    :cond_1
    move v1, v0

    .line 166
    goto :goto_0

    .line 171
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, v7

    .line 183
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appstate/a/a;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;[B)I

    move-result v0

    goto :goto_2

    :cond_3
    move-object v7, v6

    move v5, v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;Ljava/lang/String;ILjava/lang/String;[B)I
    .locals 8

    .prologue
    .line 668
    const/4 v7, 0x0

    .line 669
    const/4 v6, -0x1

    .line 671
    :try_start_0
    invoke-static {p6}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v0

    .line 672
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/a;->a:Lcom/google/android/gms/appstate/d/a/c;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "states/%1$s/clear"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz p5, :cond_1

    const-string v1, "currentDataVersion"

    invoke-static {p5}, Lcom/google/android/gms/appstate/d/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v1, v2}, Lcom/google/android/gms/appstate/d/a/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/appstate/d/a/c;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/appstate/d/a/f;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appstate/d/a/f;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    move v0, v6

    .line 699
    :goto_0
    const-string v2, "upsync_required"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 700
    if-eqz v1, :cond_9

    .line 701
    const-string v0, "data_dirty"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 702
    const-string v0, "local_version"

    invoke-virtual {v1}, Lcom/google/android/gms/appstate/d/a/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    const-string v0, "conflict_version"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 704
    const-string v0, "conflict_data"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 705
    const/4 v0, 0x0

    .line 736
    :goto_1
    :pswitch_0
    return v0

    .line 677
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/appstate/a/a;->a:Lcom/google/android/gms/appstate/d/a/c;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v4, Lcom/google/android/gms/appstate/d/a/e;

    invoke-direct {v4, v0}, Lcom/google/android/gms/appstate/d/a/e;-><init>(Ljava/lang/String;)V

    const-string v0, "states/%1$s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz p5, :cond_3

    const-string v0, "currentStateVersion"

    invoke-static {p5}, Lcom/google/android/gms/appstate/d/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/appstate/d/a/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iget-object v0, v1, Lcom/google/android/gms/appstate/d/a/c;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/gms/appstate/d/a/f;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appstate/d/a/f;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    move-object v1, v0

    move v0, v6

    .line 695
    goto :goto_0

    .line 680
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 682
    iget-object v0, v1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    invoke-static {v1, v0}, Lcom/google/android/gms/common/server/b/c;->b(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/a;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_6

    :cond_4
    const/4 v0, -0x1

    .line 683
    :goto_2
    const/4 v2, -0x1

    if-ne v0, v2, :cond_a

    .line 684
    const-string v0, "AppStateAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to write app state for app "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    invoke-static {}, Lcom/google/android/gms/appstate/c/g;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 686
    const-string v0, "AppStateAgent"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 691
    :cond_5
    invoke-static {v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;)Z

    move-result v0

    .line 692
    const-string v1, "upsync_required"

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 693
    const/4 v0, 0x3

    goto/16 :goto_1

    .line 682
    :sswitch_0
    const/16 v0, 0x7d4

    goto :goto_2

    :sswitch_1
    const/16 v0, 0x7d0

    goto :goto_2

    :sswitch_2
    const/16 v0, 0x7d1

    goto :goto_2

    :sswitch_3
    const/16 v0, 0x7d2

    goto :goto_2

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "StateKeyLimitExceeded"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x7d3

    goto :goto_2

    :cond_7
    const/4 v0, -0x1

    goto :goto_2

    .line 692
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 709
    :cond_9
    packed-switch v0, :pswitch_data_0

    .line 733
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    .line 734
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 712
    :pswitch_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/appstate/a/a;->a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;Ljava/lang/String;I)I

    move-result v0

    goto/16 :goto_1

    .line 717
    :pswitch_2
    const-string v0, "AppStateAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "App state size was too large for app "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    const-string v0, "AppStateAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Max size is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/appstate/b/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 722
    :pswitch_3
    const-string v1, "AppStateAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No data in update request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/appstate/c/g;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 729
    :pswitch_4
    const-string v1, "AppStateAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "App "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is using too many state keys"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    move-object v1, v7

    goto/16 :goto_0

    .line 682
    nop

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_0
        0x194 -> :sswitch_3
        0x199 -> :sswitch_1
        0x19d -> :sswitch_2
    .end sparse-switch

    .line 709
    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;[B)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 209
    .line 211
    invoke-static {p1, p2}, Lcom/google/android/gms/appstate/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    .line 212
    invoke-static {v2, v3, p3, p4}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v7

    .line 213
    sget-object v0, Lcom/google/android/gms/appstate/a/b;->a:[Ljava/lang/String;

    invoke-static {p1, v7, v0}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v2

    .line 215
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 216
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 217
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 220
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 222
    if-nez v5, :cond_0

    .line 223
    const-string v0, "AppStateAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No conflicting data when calling resolve for app "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 274
    :goto_1
    return-object v0

    .line 220
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 230
    :cond_0
    invoke-virtual {v5, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 231
    const-string v0, "AppStateAgent"

    const-string v2, "Resolution attempted against incorrect version"

    invoke-static {v0, v2}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 233
    const-string v2, "local_data"

    invoke-virtual {v0, v2, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 234
    const-string v2, "local_version"

    invoke-virtual {v0, v2, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v2, "data_dirty"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 236
    const-string v2, "upsync_required"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 237
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v7, v0, v1, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 238
    const/16 v0, 0x7d0

    invoke-static {p1, v7, v0}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1

    .line 244
    :cond_1
    invoke-static {v0, p6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 245
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 246
    const-string v2, "local_data"

    invoke-virtual {v0, v2, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 247
    const-string v2, "local_version"

    invoke-virtual {v0, v2, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v2, "data_dirty"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 249
    const-string v2, "upsync_required"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 250
    const-string v2, "conflict_version"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 251
    const-string v2, "conflict_data"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 252
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v7, v0, v1, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 253
    invoke-static {p1, v7, v4}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1

    .line 260
    :cond_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 261
    const-string v2, "local_data"

    invoke-virtual {v0, v2, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 262
    const-string v2, "local_version"

    invoke-virtual {v0, v2, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v2, "data_dirty"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 264
    const-string v2, "upsync_required"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 265
    const-string v2, "conflict_version"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 266
    const-string v2, "conflict_data"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v7, v0, v1, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p6

    .line 272
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appstate/a/a;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;[B)I

    move-result v0

    .line 274
    invoke-static {p1, v7, v0}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto/16 :goto_1

    :cond_3
    move-object v0, v1

    move-object v5, v1

    goto/16 :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I[BZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    .prologue
    .line 292
    invoke-static {p1, p2}, Lcom/google/android/gms/appstate/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    .line 293
    const-wide/16 v0, -0x1

    .line 294
    const/4 v5, 0x0

    .line 295
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 296
    invoke-static {v2, v3, p3, p4}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v6

    .line 297
    sget-object v7, Lcom/google/android/gms/appstate/a/c;->a:[Ljava/lang/String;

    invoke-static {p1, v6, v7}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v6

    .line 299
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 300
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 301
    const/4 v5, 0x3

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 304
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 308
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 309
    const-string v7, "app_id"

    invoke-virtual {v6, v7, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    const-string v7, "key"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 311
    const-string v7, "local_data"

    invoke-virtual {v6, v7, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 312
    const-string v7, "data_dirty"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 313
    const-string v7, "upsync_required"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 314
    const-string v7, "client_context_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 316
    const-wide/16 v8, -0x1

    cmp-long v7, v0, v8

    if-nez v7, :cond_1

    .line 317
    invoke-static {v2, v3, p3, p4}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    .line 318
    invoke-virtual {v4, v0, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-object v7, v0

    .line 325
    :goto_0
    const/4 v0, 0x5

    .line 326
    if-eqz p6, :cond_2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    .line 327
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appstate/a/a;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;[B)I

    move-result v0

    .line 332
    :goto_1
    invoke-static {p1, v7, v0}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 304
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .line 320
    :cond_1
    invoke-static {v0, v1}, Lcom/google/android/gms/appstate/provider/d;->a(J)Landroid/net/Uri;

    move-result-object v0

    .line 321
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v4, v0, v6, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object v7, v0

    goto :goto_0

    .line 330
    :cond_2
    invoke-static {p2}, Lcom/google/android/gms/appstate/service/d;->a(Lcom/google/android/gms/common/server/ClientContext;)V

    goto :goto_1
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 350
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/appstate/a/a;->a:Lcom/google/android/gms/appstate/d/a/c;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "states/%1$s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/gms/appstate/d/a/c;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v1, p2, v3, v2, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    invoke-static {p1, p2}, Lcom/google/android/gms/appstate/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    .line 362
    invoke-static {v2, v3, p3, p4}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v1

    .line 363
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 364
    :goto_0
    return v0

    .line 351
    :catch_0
    move-exception v0

    .line 352
    const-string v1, "AppStateAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to delete app state for app "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/appstate/c/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-static {}, Lcom/google/android/gms/appstate/c/g;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 354
    const-string v1, "AppStateAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 356
    :cond_0
    const/4 v0, 0x6

    goto :goto_0
.end method
