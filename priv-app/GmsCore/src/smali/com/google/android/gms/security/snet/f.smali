.class Lcom/google/android/gms/security/snet/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static f:Z

.field private static g:Z

.field private static h:Z

.field private static i:J

.field private static j:J

.field private static k:Ljava/lang/String;

.field private static l:Lcom/google/android/gms/security/snet/d;

.field private static m:Ljava/util/List;


# instance fields
.field private final b:Lcom/google/android/gms/security/snet/a;

.field private c:I

.field private d:J

.field private final e:Landroid/content/Context;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    const-class v0, Lcom/google/android/gms/security/snet/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/snet/f;->a:Ljava/lang/String;

    .line 37
    sput-boolean v1, Lcom/google/android/gms/security/snet/f;->f:Z

    .line 39
    sput-boolean v1, Lcom/google/android/gms/security/snet/f;->g:Z

    .line 41
    sput-boolean v1, Lcom/google/android/gms/security/snet/f;->h:Z

    .line 49
    new-instance v0, Lcom/google/android/gms/security/snet/d;

    invoke-direct {v0}, Lcom/google/android/gms/security/snet/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/security/snet/f;->m:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/gms/security/snet/f;->e:Landroid/content/Context;

    .line 64
    iput p2, p0, Lcom/google/android/gms/security/snet/f;->c:I

    .line 65
    new-instance v0, Lcom/google/android/gms/security/snet/a;

    iget v1, p0, Lcom/google/android/gms/security/snet/f;->c:I

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/security/snet/a;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/gms/security/snet/f;->b:Lcom/google/android/gms/security/snet/a;

    .line 66
    return-void
.end method

.method constructor <init>(Landroid/content/Context;JI)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/gms/security/snet/f;->e:Landroid/content/Context;

    .line 57
    iput-wide p2, p0, Lcom/google/android/gms/security/snet/f;->d:J

    .line 58
    iput p4, p0, Lcom/google/android/gms/security/snet/f;->c:I

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/f;->b:Lcom/google/android/gms/security/snet/a;

    .line 60
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/security/snet/a;I)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/gms/security/snet/f;->e:Landroid/content/Context;

    .line 70
    iput p3, p0, Lcom/google/android/gms/security/snet/f;->c:I

    .line 71
    iput-object p2, p0, Lcom/google/android/gms/security/snet/f;->b:Lcom/google/android/gms/security/snet/a;

    .line 72
    return-void
.end method

.method static a()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/security/snet/f;->f:Z

    .line 80
    return-void
.end method

.method static a(J)V
    .locals 2

    .prologue
    .line 83
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/security/snet/f;->g:Z

    .line 84
    sput-wide p0, Lcom/google/android/gms/security/snet/f;->i:J

    .line 85
    return-void
.end method

.method static a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/gms/security/snet/f;->m:Ljava/util/List;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method static a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 96
    sput-object p0, Lcom/google/android/gms/security/snet/f;->k:Ljava/lang/String;

    .line 97
    return-void
.end method

.method static b()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/security/snet/f;->h:Z

    .line 93
    return-void
.end method

.method static b(J)V
    .locals 0

    .prologue
    .line 88
    sput-wide p0, Lcom/google/android/gms/security/snet/f;->j:J

    .line 89
    return-void
.end method


# virtual methods
.method final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/gms/security/snet/f;->n:Ljava/lang/String;

    .line 101
    return-void
.end method

.method final c()V
    .locals 1

    .prologue
    .line 104
    const-string v0, "snet_gcore"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/snet/f;->c(Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 111
    sget-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    iget v1, p0, Lcom/google/android/gms/security/snet/f;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/security/snet/f;->b:Lcom/google/android/gms/security/snet/a;

    if-eqz v0, :cond_6

    .line 113
    sget-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    iget-object v1, p0, Lcom/google/android/gms/security/snet/f;->b:Lcom/google/android/gms/security/snet/a;

    invoke-virtual {v1}, Lcom/google/android/gms/security/snet/a;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    .line 118
    :goto_0
    sget-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    sget-boolean v1, Lcom/google/android/gms/security/snet/f;->f:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    .line 120
    sget-boolean v0, Lcom/google/android/gms/security/snet/f;->g:Z

    if-eqz v0, :cond_0

    .line 121
    sget-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    sget-wide v2, Lcom/google/android/gms/security/snet/f;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    .line 122
    sget-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    sget-wide v2, Lcom/google/android/gms/security/snet/f;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    .line 123
    sput-boolean v4, Lcom/google/android/gms/security/snet/f;->g:Z

    .line 124
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/gms/security/snet/f;->j:J

    .line 127
    :cond_0
    sget-boolean v0, Lcom/google/android/gms/security/snet/f;->h:Z

    if-eqz v0, :cond_1

    .line 128
    sget-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    .line 129
    sput-boolean v4, Lcom/google/android/gms/security/snet/f;->h:Z

    .line 132
    :cond_1
    sget-object v0, Lcom/google/android/gms/security/snet/f;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 133
    sget-object v0, Lcom/google/android/gms/security/a/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v1, Lcom/google/android/gms/security/snet/f;->k:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 135
    sget-object v1, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    sget-object v2, Lcom/google/android/gms/security/snet/f;->k:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    .line 136
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/security/snet/f;->k:Ljava/lang/String;

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/snet/f;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 140
    sget-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    iget-object v1, p0, Lcom/google/android/gms/security/snet/f;->n:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    .line 143
    :cond_3
    sget-object v0, Lcom/google/android/gms/security/snet/f;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 144
    sget-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    sget-object v1, Lcom/google/android/gms/security/snet/f;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    .line 145
    sget-object v1, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    sget-object v0, Lcom/google/android/gms/security/snet/f;->m:Ljava/util/List;

    sget-object v2, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    iget-object v2, v2, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/security/snet/f;->m:Ljava/util/List;

    .line 150
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/security/snet/f;->e:Landroid/content/Context;

    const-string v1, "dropbox"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/DropBoxManager;

    .line 152
    if-eqz v0, :cond_5

    .line 153
    sget-object v1, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {v0, p1, v1, v4}, Landroid/os/DropBoxManager;->addData(Ljava/lang/String;[BI)V

    .line 161
    :cond_5
    new-instance v0, Lcom/google/android/gms/security/snet/d;

    invoke-direct {v0}, Lcom/google/android/gms/security/snet/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    .line 162
    return-void

    .line 115
    :cond_6
    sget-object v0, Lcom/google/android/gms/security/snet/f;->l:Lcom/google/android/gms/security/snet/d;

    iget-wide v2, p0, Lcom/google/android/gms/security/snet/f;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    goto/16 :goto_0
.end method
