.class public final Lcom/google/android/gms/smart_profile/be;
.super Lcom/google/android/gms/people/identity/internal/r;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/identity/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/gms/people/identity/internal/r;-><init>()V

    return-void
.end method

.method private c(Landroid/content/Context;Ljava/lang/Object;Lcom/google/android/gms/people/identity/s;Lcom/google/android/gms/people/identity/h;Lcom/google/android/gms/people/identity/k;)Lcom/google/android/gms/smart_profile/SmartProfilePerson;
    .locals 13

    .prologue
    .line 27
    invoke-super/range {p0 .. p5}, Lcom/google/android/gms/people/identity/internal/r;->b(Landroid/content/Context;Ljava/lang/Object;Lcom/google/android/gms/people/identity/s;Lcom/google/android/gms/people/identity/h;Lcom/google/android/gms/people/identity/k;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    .line 29
    if-eqz p4, :cond_2

    .line 30
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 31
    move-object/from16 v0, p4

    iget-object v1, v0, Lcom/google/android/gms/people/identity/h;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/identity/i;

    .line 32
    iget-object v2, v1, Lcom/google/android/gms/people/identity/i;->f:Lcom/google/android/gms/people/identity/j;

    if-eqz v2, :cond_0

    .line 33
    iget-object v9, v1, Lcom/google/android/gms/people/identity/i;->f:Lcom/google/android/gms/people/identity/j;

    new-instance v1, Lcom/google/android/gms/smart_profile/bd;

    invoke-virtual {v9}, Lcom/google/android/gms/people/identity/j;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/gms/people/identity/j;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/android/gms/people/identity/j;->c()I

    move-result v4

    invoke-virtual {v9}, Lcom/google/android/gms/people/identity/j;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/gms/people/identity/j;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9}, Lcom/google/android/gms/people/identity/j;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9}, Lcom/google/android/gms/people/identity/j;->g()I

    move-result v8

    invoke-virtual {v9}, Lcom/google/android/gms/people/identity/j;->h()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/smart_profile/bd;-><init>(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {v10, v11}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->a(Ljava/util/List;)V

    .line 39
    :cond_2
    return-object v10
.end method


# virtual methods
.method protected final synthetic a()Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;-><init>()V

    return-object v0
.end method

.method public final synthetic a(Landroid/content/Context;Ljava/lang/Object;Lcom/google/android/gms/people/identity/s;Lcom/google/android/gms/people/identity/h;Lcom/google/android/gms/people/identity/k;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/smart_profile/be;->c(Landroid/content/Context;Ljava/lang/Object;Lcom/google/android/gms/people/identity/s;Lcom/google/android/gms/people/identity/h;Lcom/google/android/gms/people/identity/k;)Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Landroid/content/Context;Ljava/lang/Object;Lcom/google/android/gms/people/identity/s;Lcom/google/android/gms/people/identity/h;Lcom/google/android/gms/people/identity/k;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 1

    .prologue
    .line 16
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/smart_profile/be;->c(Landroid/content/Context;Ljava/lang/Object;Lcom/google/android/gms/people/identity/s;Lcom/google/android/gms/people/identity/h;Lcom/google/android/gms/people/identity/k;)Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    return-object v0
.end method
