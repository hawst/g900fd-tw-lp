.class public final Lcom/google/android/gms/wallet/common/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    .line 75
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ah;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/wallet/common/ah;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    return-object p0
.end method

.method public final a(J)Lcom/google/android/gms/wallet/common/ah;
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    return-object p0
.end method

.method public final a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;
    .locals 2

    .prologue
    .line 138
    if-nez p1, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    :goto_0
    return-object p0

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->d(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/wallet/common/ah;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 160
    const-string v0, "\u2063"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;[Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
