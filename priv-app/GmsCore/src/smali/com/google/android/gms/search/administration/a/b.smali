.class public abstract Lcom/google/android/gms/search/administration/a/b;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/search/administration/a/a;


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 62
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 45
    :sswitch_0
    const-string v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationCallbacks"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 46
    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.google.android.gms.search.administration.internal.ISearchAdministrationCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->CREATOR:Lcom/google/android/gms/search/administration/c;

    invoke-static {p2}, Lcom/google/android/gms/search/administration/c;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;

    move-result-object v0

    .line 58
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/search/administration/a/b;->a(Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;)V

    move v0, v1

    .line 59
    goto :goto_0

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
