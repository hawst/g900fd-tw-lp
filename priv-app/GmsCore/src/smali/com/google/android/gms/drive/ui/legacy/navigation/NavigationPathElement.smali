.class public Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

.field private b:Landroid/os/Parcelable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gms/drive/ui/legacy/navigation/h;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    .line 24
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    return-object v0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->b:Landroid/os/Parcelable;

    .line 32
    return-void
.end method

.method public final b()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->b:Landroid/os/Parcelable;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 72
    if-ne p0, p1, :cond_0

    .line 73
    const/4 v0, 0x1

    .line 78
    :goto_0
    return v0

    .line 74
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    if-eqz v0, :cond_1

    .line 75
    check-cast p1, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    .line 76
    iget-object v0, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 78
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 89
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "NavigationPathElement { CriterionSet: %s, savedState=%s }"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->b:Landroid/os/Parcelable;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->b:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 47
    return-void
.end method
