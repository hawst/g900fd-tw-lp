.class final enum Lcom/google/android/gms/drive/database/model/al;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/al;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/al;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/al;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/al;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/al;

.field public static final enum f:Lcom/google/android/gms/drive/database/model/al;

.field public static final enum g:Lcom/google/android/gms/drive/database/model/al;

.field public static final enum h:Lcom/google/android/gms/drive/database/model/al;

.field public static final enum i:Lcom/google/android/gms/drive/database/model/al;

.field private static final m:Ljava/util/Map;

.field private static final synthetic n:[Lcom/google/android/gms/drive/database/model/al;


# instance fields
.field private final j:I

.field private final k:I

.field private final l:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/google/android/gms/drive/database/model/al;

    const-string v1, "AUDIO"

    sget v3, Lcom/google/android/gms/h;->bj:I

    sget v4, Lcom/google/android/gms/p;->fZ:I

    const/16 v5, 0xb

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "audio/annodex"

    aput-object v6, v5, v2

    const-string v6, "audio/basic"

    aput-object v6, v5, v9

    const-string v6, "audio/flac"

    aput-object v6, v5, v10

    const-string v6, "audio/mid"

    aput-object v6, v5, v11

    const-string v6, "audio/mpeg"

    aput-object v6, v5, v12

    const/4 v6, 0x5

    const-string v7, "audio/ogg"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "audio/x-aiff"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    const-string v7, "audio/x-mpegurl"

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "audio/x-pn-realaudio"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    const-string v7, "audio/wav"

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "audio/x-wav"

    aput-object v7, v5, v6

    invoke-static {v5}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/model/al;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/al;->a:Lcom/google/android/gms/drive/database/model/al;

    .line 36
    new-instance v3, Lcom/google/android/gms/drive/database/model/al;

    const-string v4, "IMAGE"

    sget v6, Lcom/google/android/gms/h;->br:I

    sget v7, Lcom/google/android/gms/p;->gn:I

    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "image/gif"

    aput-object v1, v0, v2

    const-string v1, "image/jpeg"

    aput-object v1, v0, v9

    const-string v1, "image/tiff"

    aput-object v1, v0, v10

    const-string v1, "image/png"

    aput-object v1, v0, v11

    const-string v1, "image/cgm"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v5, "image/fits"

    aput-object v5, v0, v1

    const/4 v1, 0x6

    const-string v5, "image/g3fax"

    aput-object v5, v0, v1

    const/4 v1, 0x7

    const-string v5, "image/ief"

    aput-object v5, v0, v1

    const/16 v1, 0x8

    const-string v5, "image/jp2"

    aput-object v5, v0, v1

    const/16 v1, 0x9

    const-string v5, "image/jpm"

    aput-object v5, v0, v1

    const/16 v1, 0xa

    const-string v5, "image/jpx"

    aput-object v5, v0, v1

    const/16 v1, 0xb

    const-string v5, "image/ktx"

    aput-object v5, v0, v1

    const/16 v1, 0xc

    const-string v5, "image/naplps"

    aput-object v5, v0, v1

    const/16 v1, 0xd

    const-string v5, "image/prs.bitf"

    aput-object v5, v0, v1

    const/16 v1, 0xe

    const-string v5, "image/prs.pti"

    aput-object v5, v0, v1

    const/16 v1, 0xf

    const-string v5, "image/svg+xml"

    aput-object v5, v0, v1

    const/16 v1, 0x10

    const-string v5, "image/tiff-fx"

    aput-object v5, v0, v1

    const/16 v1, 0x11

    const-string v5, "image/vnd.adobe.photoshop"

    aput-object v5, v0, v1

    const/16 v1, 0x12

    const-string v5, "image/vnd.svf"

    aput-object v5, v0, v1

    const/16 v1, 0x13

    const-string v5, "image/vnd.xiff"

    aput-object v5, v0, v1

    const/16 v1, 0x14

    const-string v5, "image/vnd.microsoft.icon"

    aput-object v5, v0, v1

    const/16 v1, 0x15

    const-string v5, "image/x-ms-bmp"

    aput-object v5, v0, v1

    const/16 v1, 0x16

    const-string v5, "application/vnd.google.panorama360+jpg"

    aput-object v5, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/al;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/al;->b:Lcom/google/android/gms/drive/database/model/al;

    .line 60
    new-instance v3, Lcom/google/android/gms/drive/database/model/al;

    const-string v4, "MSEXCEL"

    sget v6, Lcom/google/android/gms/h;->bm:I

    sget v7, Lcom/google/android/gms/p;->gj:I

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "application/vnd.ms-excel"

    aput-object v1, v0, v2

    const-string v1, "application/vnd.ms-excel.addin.macroEnabled.12"

    aput-object v1, v0, v9

    const-string v1, "application/vnd.ms-excel.sheet.binary.macroEnabled.12"

    aput-object v1, v0, v10

    const-string v1, "application/vnd.ms-excel.sheet.macroEnabled.12"

    aput-object v1, v0, v11

    const-string v1, "application/vnd.ms-excel.template.macroEnabled.12"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v5, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    aput-object v5, v0, v1

    const/4 v1, 0x6

    const-string v5, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    aput-object v5, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/al;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/al;->c:Lcom/google/android/gms/drive/database/model/al;

    .line 71
    new-instance v3, Lcom/google/android/gms/drive/database/model/al;

    const-string v4, "MSPOWERPOINT"

    sget v6, Lcom/google/android/gms/h;->bu:I

    sget v7, Lcom/google/android/gms/p;->gk:I

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "application/vnd.ms-powerpoint"

    aput-object v1, v0, v2

    const-string v1, "application/vnd.ms-powerpoint.addin.macroEnabled.12"

    aput-object v1, v0, v9

    const-string v1, "application/vnd.ms-powerpoint.presentation.macroEnabled.12"

    aput-object v1, v0, v10

    const-string v1, "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"

    aput-object v1, v0, v11

    const-string v1, "application/vnd.ms-powerpoint.template.macroEnabled.12"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v5, "application/vnd.openxmlformats-officedocument.presentationml.template"

    aput-object v5, v0, v1

    const/4 v1, 0x6

    const-string v5, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    aput-object v5, v0, v1

    const/4 v1, 0x7

    const-string v5, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    aput-object v5, v0, v1

    const/16 v1, 0x8

    const-string v5, "application/vnd.openxmlformats-officedocument.presentationml.slide"

    aput-object v5, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/al;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/al;->d:Lcom/google/android/gms/drive/database/model/al;

    .line 90
    new-instance v3, Lcom/google/android/gms/drive/database/model/al;

    const-string v4, "MSWORD"

    sget v6, Lcom/google/android/gms/h;->bA:I

    sget v7, Lcom/google/android/gms/p;->gl:I

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "application/msword"

    aput-object v1, v0, v2

    const-string v1, "application/vnd.ms-word.document.macroEnabled.12"

    aput-object v1, v0, v9

    const-string v1, "application/vnd.ms-word.template.macroEnabled.12"

    aput-object v1, v0, v10

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    aput-object v1, v0, v11

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    aput-object v1, v0, v12

    invoke-static {v0}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/al;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/al;->e:Lcom/google/android/gms/drive/database/model/al;

    .line 101
    new-instance v3, Lcom/google/android/gms/drive/database/model/al;

    const-string v4, "PDF"

    const/4 v5, 0x5

    sget v6, Lcom/google/android/gms/h;->bt:I

    sget v7, Lcom/google/android/gms/p;->gm:I

    new-array v0, v9, [Ljava/lang/String;

    const-string v1, "application/pdf"

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/al;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/al;->f:Lcom/google/android/gms/drive/database/model/al;

    .line 103
    new-instance v3, Lcom/google/android/gms/drive/database/model/al;

    const-string v4, "TEXT"

    const/4 v5, 0x6

    sget v6, Lcom/google/android/gms/h;->by:I

    sget v7, Lcom/google/android/gms/p;->gp:I

    new-array v0, v9, [Ljava/lang/String;

    const-string v1, "text/plain"

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/al;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/al;->g:Lcom/google/android/gms/drive/database/model/al;

    .line 105
    new-instance v3, Lcom/google/android/gms/drive/database/model/al;

    const-string v4, "VIDEO"

    const/4 v5, 0x7

    sget v6, Lcom/google/android/gms/h;->bz:I

    sget v7, Lcom/google/android/gms/p;->gr:I

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "video/3gpp"

    aput-object v1, v0, v2

    const-string v1, "video/3gp"

    aput-object v1, v0, v9

    const-string v1, "video/H261"

    aput-object v1, v0, v10

    const-string v1, "video/H263"

    aput-object v1, v0, v11

    const-string v1, "video/H264"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v8, "video/mp4"

    aput-object v8, v0, v1

    const/4 v1, 0x6

    const-string v8, "video/mpeg"

    aput-object v8, v0, v1

    const/4 v1, 0x7

    const-string v8, "video/quicktime"

    aput-object v8, v0, v1

    const/16 v1, 0x8

    const-string v8, "video/raw"

    aput-object v8, v0, v1

    const/16 v1, 0x9

    const-string v8, "video/vnd.motorola.video"

    aput-object v8, v0, v1

    const/16 v1, 0xa

    const-string v8, "video/vnd.motorola.videop"

    aput-object v8, v0, v1

    const/16 v1, 0xb

    const-string v8, "video/x-la-asf"

    aput-object v8, v0, v1

    const/16 v1, 0xc

    const-string v8, "video/x-m4v"

    aput-object v8, v0, v1

    const/16 v1, 0xd

    const-string v8, "video/x-matroska"

    aput-object v8, v0, v1

    const/16 v1, 0xe

    const-string v8, "video/x-ms-asf"

    aput-object v8, v0, v1

    const/16 v1, 0xf

    const-string v8, "video/x-msvideo"

    aput-object v8, v0, v1

    const/16 v1, 0x10

    const-string v8, "video/x-sgi-movie"

    aput-object v8, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/al;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/al;->h:Lcom/google/android/gms/drive/database/model/al;

    .line 123
    new-instance v3, Lcom/google/android/gms/drive/database/model/al;

    const-string v4, "ZIP"

    const/16 v5, 0x8

    sget v6, Lcom/google/android/gms/h;->bB:I

    sget v7, Lcom/google/android/gms/p;->gs:I

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "application/x-compress"

    aput-object v1, v0, v2

    const-string v1, "application/x-compressed"

    aput-object v1, v0, v9

    const-string v1, "application/x-gtar"

    aput-object v1, v0, v10

    const-string v1, "application/x-gzip"

    aput-object v1, v0, v11

    const-string v1, "application/x-tar"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v8, "application/zip"

    aput-object v8, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/al;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/al;->i:Lcom/google/android/gms/drive/database/model/al;

    .line 18
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/al;

    sget-object v1, Lcom/google/android/gms/drive/database/model/al;->a:Lcom/google/android/gms/drive/database/model/al;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/database/model/al;->b:Lcom/google/android/gms/drive/database/model/al;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/al;->c:Lcom/google/android/gms/drive/database/model/al;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/al;->d:Lcom/google/android/gms/drive/database/model/al;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/gms/drive/database/model/al;->e:Lcom/google/android/gms/drive/database/model/al;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v3, Lcom/google/android/gms/drive/database/model/al;->f:Lcom/google/android/gms/drive/database/model/al;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    sget-object v3, Lcom/google/android/gms/drive/database/model/al;->g:Lcom/google/android/gms/drive/database/model/al;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    sget-object v3, Lcom/google/android/gms/drive/database/model/al;->h:Lcom/google/android/gms/drive/database/model/al;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    sget-object v3, Lcom/google/android/gms/drive/database/model/al;->i:Lcom/google/android/gms/drive/database/model/al;

    aput-object v3, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/al;->n:[Lcom/google/android/gms/drive/database/model/al;

    .line 161
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 162
    invoke-static {}, Lcom/google/android/gms/drive/database/model/al;->values()[Lcom/google/android/gms/drive/database/model/al;

    move-result-object v3

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 163
    iget-object v0, v5, Lcom/google/android/gms/drive/database/model/al;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 164
    invoke-interface {v1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 162
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 167
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/database/model/al;->m:Ljava/util/Map;

    .line 168
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILjava/util/Set;)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 137
    iput p3, p0, Lcom/google/android/gms/drive/database/model/al;->j:I

    .line 138
    iput p4, p0, Lcom/google/android/gms/drive/database/model/al;->k:I

    .line 139
    iput-object p5, p0, Lcom/google/android/gms/drive/database/model/al;->l:Ljava/util/Set;

    .line 140
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/al;
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/google/android/gms/drive/database/model/al;->m:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/al;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/al;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/gms/drive/database/model/al;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/al;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/al;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/gms/drive/database/model/al;->n:[Lcom/google/android/gms/drive/database/model/al;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/al;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/al;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/gms/drive/database/model/al;->j:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/google/android/gms/drive/database/model/al;->k:I

    return v0
.end method
