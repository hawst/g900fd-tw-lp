.class public final Lcom/google/android/gms/backup/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const v2, 0x15180

    const/4 v1, 0x1

    .line 15
    const-string v0, "backup_gms_transport_enabled"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/backup/a/a;->a:Lcom/google/android/gms/common/a/d;

    .line 22
    const-string v0, "is_kv_backup_enabled"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/backup/a/a;->b:Lcom/google/android/gms/common/a/d;

    .line 29
    const-string v0, "kv_backup_min_delay"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/backup/a/a;->c:Lcom/google/android/gms/common/a/d;

    .line 37
    const-string v1, "backup_rate_limited_apps"

    const/16 v0, 0x16

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ALL"

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/backup/a/a;->d:Lcom/google/android/gms/common/a/d;

    .line 42
    const-string v0, "backup_min_delay_per_app_secs"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/backup/a/a;->e:Lcom/google/android/gms/common/a/d;

    .line 46
    const-string v0, "backup_rate_limit_fuzz_range_hrs"

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/backup/a/a;->f:Lcom/google/android/gms/common/a/d;

    return-void

    .line 37
    :cond_0
    const-string v0, "android,@pm@"

    goto :goto_0
.end method
