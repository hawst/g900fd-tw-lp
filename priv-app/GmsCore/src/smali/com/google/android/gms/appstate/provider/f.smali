.class public final Lcom/google/android/gms/appstate/provider/f;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/appstate/provider/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/appstate/provider/f;->a:Lcom/google/android/gms/appstate/provider/f;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 36
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 37
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/appstate/provider/f;
    .locals 3

    .prologue
    .line 52
    const-class v1, Lcom/google/android/gms/appstate/provider/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/appstate/provider/f;->a:Lcom/google/android/gms/appstate/provider/f;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/google/android/gms/appstate/provider/f;

    const-string v2, "app_state.db"

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/appstate/provider/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/appstate/provider/f;->a:Lcom/google/android/gms/appstate/provider/f;

    .line 55
    :cond_0
    sget-object v0, Lcom/google/android/gms/appstate/provider/f;->a:Lcom/google/android/gms/appstate/provider/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 65
    const-string v0, "DROP TABLE IF EXISTS client_contexts;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 66
    const-string v0, "DROP TABLE IF EXISTS app_states;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 69
    const-string v0, "DROP INDEX IF EXISTS app_states_app_id_index;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 75
    const-string v0, "CREATE TABLE client_contexts (_id INTEGER PRIMARY KEY AUTOINCREMENT,package_name TEXT NOT NULL,package_uid INTEGER NOT NULL,account_name TEXT NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 83
    const-string v0, "CREATE TABLE app_states (_id INTEGER PRIMARY KEY AUTOINCREMENT,app_id TEXT NOT NULL,client_context_id INTEGER REFERENCES client_contexts(_id) NOT NULL,key INTEGER NOT NULL,local_version TEXT,local_data BLOB,conflict_version TEXT,conflict_data BLOB,data_dirty INTEGER NOT NULL DEFAULT 0,upsync_required INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 97
    const-string v0, "CREATE INDEX app_states_app_id_index ON app_states (app_id);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 118
    const-string v0, "AppStateDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Downgrading from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/4 v0, 0x3

    if-gt p3, v0, :cond_0

    .line 121
    const-string v0, "AppStateDatabaseHelper"

    const-string v1, "Prelaunch policy: wipe all data!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-static {p1}, Lcom/google/android/gms/appstate/provider/f;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 123
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appstate/provider/f;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 128
    :cond_0
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 104
    const/4 v0, 0x3

    if-gt p2, v0, :cond_0

    .line 105
    const-string v0, "AppStateDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upgrading from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", all data will be wiped!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-static {p1}, Lcom/google/android/gms/appstate/provider/f;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 108
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appstate/provider/f;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    const-string v0, "AppStateDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upgrading from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
