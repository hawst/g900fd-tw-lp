.class public final Lcom/google/android/gms/googlehelp/search/a;
.super Lcom/google/android/gms/googlehelp/d/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/googlehelp/search/b;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/d/a;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/gms/googlehelp/search/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/googlehelp/search/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/a;->b:Lcom/google/android/gms/googlehelp/search/b;

    .line 51
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/search/a;->c:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->b()V

    .line 96
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "app_package_name=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" AND suggest_intent_query LIKE ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "%"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "suggestions"

    sget-object v2, Lcom/google/android/gms/googlehelp/search/r;->j:[Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "date DESC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->c()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->c()V

    throw v0
.end method

.method protected final a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/a;->b:Lcom/google/android/gms/googlehelp/search/b;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/b;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->b()V

    .line 61
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->c()V

    .line 73
    :goto_0
    return-void

    .line 65
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 66
    const-string v1, "app_package_name"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v1, "suggest_text_1"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v1, "suggest_intent_query"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v1, "date"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "suggestions"

    const-string v3, "suggest_intent_query"

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->c()V

    throw v0
.end method

.method public final d()Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->b()V

    .line 79
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "app_package_name=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "suggestions"

    sget-object v2, Lcom/google/android/gms/googlehelp/search/r;->j:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "date DESC"

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->c()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->c()V

    throw v0
.end method

.method public final e()I
    .locals 4

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->b()V

    .line 115
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "app_package_name=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 116
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/a;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "suggestions"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->c()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/a;->c()V

    throw v0
.end method
