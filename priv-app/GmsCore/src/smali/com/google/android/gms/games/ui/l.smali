.class final Lcom/google/android/gms/games/ui/l;
.super Lcom/google/android/gms/games/ui/cx;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic k:Lcom/google/android/gms/games/ui/j;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/j;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/gms/games/ui/l;->k:Lcom/google/android/gms/games/ui/j;

    .line 57
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/cx;-><init>(Landroid/view/View;)V

    .line 59
    sget v0, Lcom/google/android/gms/j;->mu:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/l;->n:Landroid/widget/TextView;

    .line 60
    sget v0, Lcom/google/android/gms/j;->hb:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/l;->o:Landroid/widget/TextView;

    .line 62
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/ac;I)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 67
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/cx;->a(Lcom/google/android/gms/games/ui/ac;I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/l;->k:Lcom/google/android/gms/games/ui/j;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/j;->a(Lcom/google/android/gms/games/ui/j;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/ui/l;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/games/ui/l;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    :goto_0
    return-void

    .line 71
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/l;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/l;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/l;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/j;

    .line 84
    iget-object v1, v0, Lcom/google/android/gms/games/ui/j;->c:Lcom/google/android/gms/games/ui/k;

    if-nez v1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/ui/j;->c:Lcom/google/android/gms/games/ui/k;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/k;->a()V

    goto :goto_0
.end method
