.class final Lcom/google/android/gms/wallet/common/ui/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 239
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;)I

    move-result v0

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 229
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)I

    .line 230
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;I)I

    .line 231
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a:Lcom/google/android/gms/wallet/common/ui/ak;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a:Lcom/google/android/gms/wallet/common/ui/ak;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aj;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ak;->a(I)V

    .line 235
    :cond_0
    return-void
.end method
