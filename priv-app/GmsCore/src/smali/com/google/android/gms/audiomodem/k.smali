.class public final Lcom/google/android/gms/audiomodem/k;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Lcom/google/android/gms/audiomodem/n;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/audiomodem/n;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/k;->a:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/k;->b:Landroid/os/Handler;

    .line 30
    iput-object p3, p0, Lcom/google/android/gms/audiomodem/k;->c:Lcom/google/android/gms/audiomodem/n;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/k;->d:Z

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/k;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/audiomodem/k;->b:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/audiomodem/k;->d:Z

    .line 60
    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 43
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    const-string v0, "audioModem"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const-string v0, "AudioModemBroadcastReceiver: Gservices settings have changed."

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/k;->c:Lcom/google/android/gms/audiomodem/n;

    iget-boolean v1, v0, Lcom/google/android/gms/audiomodem/n;->c:Z

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/n;->b:Lcom/google/android/gms/audiomodem/bh;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/audiomodem/bh;->c:Z

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/bh;->a()V

    .line 50
    :cond_1
    return-void
.end method
