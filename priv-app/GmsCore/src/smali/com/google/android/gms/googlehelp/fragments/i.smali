.class public final Lcom/google/android/gms/googlehelp/fragments/i;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# static fields
.field private static a:I


# instance fields
.field private final b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private final c:Lcom/google/android/gms/googlehelp/d/b;

.field private d:Lcom/google/android/gms/googlehelp/common/t;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/googlehelp/fragments/i;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/i;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d()Lcom/google/android/gms/googlehelp/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->c:Lcom/google/android/gms/googlehelp/d/b;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 65
    sget v1, Lcom/google/android/gms/googlehelp/fragments/i;->a:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 66
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/gms/googlehelp/fragments/i;->a:I

    .line 68
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/fragments/i;)Lcom/google/android/gms/googlehelp/d/b;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->c:Lcom/google/android/gms/googlehelp/d/b;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/fragments/i;)Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/googlehelp/fragments/i;)Lcom/google/android/gms/googlehelp/common/t;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/common/t;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    .line 149
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/i;->notifyDataSetChanged()V

    .line 150
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "GOOGLEHELP_HelpResponseMap"

    const-string v1, "Id %s does not exisit."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/i;->notifyDataSetChanged()V

    .line 145
    return-void

    .line 143
    :cond_0
    iput-object p1, v0, Lcom/google/android/gms/googlehelp/common/t;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    iget-object v0, v3, Lcom/google/android/gms/googlehelp/common/t;->e:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/android/gms/googlehelp/common/t;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 135
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/i;->notifyDataSetChanged()V

    move v0, v1

    .line 139
    :goto_1
    return v0

    .line 134
    :cond_0
    iget-object v0, v3, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    iget-object v4, v3, Lcom/google/android/gms/googlehelp/common/t;->e:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/common/t;->e:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 139
    goto :goto_1
.end method

.method public final b()Lcom/google/android/gms/googlehelp/common/t;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/t;->b()V

    .line 168
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/t;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/common/t;->a(I)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 82
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->ci:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/i;->d:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/common/t;->a(I)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v2

    .line 93
    sget v0, Lcom/google/android/gms/j;->ip:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 94
    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 95
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    sget v0, Lcom/google/android/gms/j;->io:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 99
    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 100
    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    new-instance v0, Lcom/google/android/gms/googlehelp/fragments/j;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/googlehelp/fragments/j;-><init>(Lcom/google/android/gms/googlehelp/fragments/i;Lcom/google/android/gms/googlehelp/common/k;)V

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    .line 130
    :goto_1
    return-object v1

    .line 102
    :cond_0
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 113
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    new-instance v0, Lcom/google/android/gms/googlehelp/fragments/k;

    invoke-direct {v0, p0, v2, p1}, Lcom/google/android/gms/googlehelp/fragments/k;-><init>(Lcom/google/android/gms/googlehelp/fragments/i;Lcom/google/android/gms/googlehelp/common/k;I)V

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    goto :goto_1

    .line 127
    :cond_2
    sget v0, Lcom/google/android/gms/googlehelp/fragments/i;->a:I

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1
.end method
