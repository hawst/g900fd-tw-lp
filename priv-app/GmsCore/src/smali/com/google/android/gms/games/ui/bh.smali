.class public final Lcom/google/android/gms/games/ui/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field private final b:Landroid/app/Activity;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/gms/games/ui/bh;->b:Landroid/app/Activity;

    .line 48
    iput p2, p0, Lcom/google/android/gms/games/ui/bh;->a:I

    .line 49
    if-eq p2, v2, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/bh;->a:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/bh;->a:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/bh;->a:I

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Invalid UI Type"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 52
    iput p3, p0, Lcom/google/android/gms/games/ui/bh;->c:I

    .line 53
    if-nez p3, :cond_2

    :goto_1
    const-string v0, "Invalid Device Type"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 54
    return-void

    :cond_1
    move v0, v1

    .line 49
    goto :goto_0

    :cond_2
    move v2, v1

    .line 53
    goto :goto_1
.end method

.method private h()Lcom/google/android/gms/games/ui/q;
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bh;->b:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/q;

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This method can only be called from a GamesFragmentActivity context"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bh;->b:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 57
    iget v1, p0, Lcom/google/android/gms/games/ui/bh;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/gms/games/ui/bh;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 65
    iget v0, p0, Lcom/google/android/gms/games/ui/bh;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 73
    iget v1, p0, Lcom/google/android/gms/games/ui/bh;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/bh;->h()Lcom/google/android/gms/games/ui/q;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    .line 89
    invoke-static {v2, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "GamesUiConfig"

    const-string v2, "getCurrentGameId: not connected; ignoring..."

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 111
    :goto_0
    return-object v0

    .line 93
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/k;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/k;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 94
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 95
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bh;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/bh;->a:I

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    .line 96
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bh;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 95
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 99
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bh;->b:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/bi;

    if-eqz v0, :cond_6

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bh;->b:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/gms/games/ui/bi;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/bi;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_5

    .line 102
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 104
    :cond_5
    const-string v0, "GamesUiConfig"

    const-string v2, "Trying to get a current game Id from a CurrentGameProvider but we don\'t have a current game yet."

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 106
    goto :goto_0

    .line 109
    :cond_6
    const-string v0, "GamesUiConfig"

    const-string v2, "Trying to get a current game Id in a destination context. Returning null as we are not game specific here"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 111
    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/bh;->h()Lcom/google/android/gms/games/ui/q;

    move-result-object v0

    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 119
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 120
    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    const-string v0, "GamesUiConfig"

    const-string v1, "getCurrentPlayerId: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/4 v0, 0x0

    .line 127
    :goto_0
    return-object v0

    .line 124
    :cond_1
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 127
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bh;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/bh;->h()Lcom/google/android/gms/games/ui/q;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 135
    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    const-string v0, "GamesUiConfig"

    const-string v1, "getCurrentAccountName: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const/4 v0, 0x0

    .line 142
    :goto_0
    return-object v0

    .line 139
    :cond_1
    invoke-static {v1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bh;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
