.class final Lcom/google/android/gms/ads/settings/c;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/common/ev;

.field final synthetic b:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

.field private c:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/gms/ads/settings/c;->b:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;B)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/google/android/gms/ads/settings/c;-><init>(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;)V

    return-void
.end method

.method private varargs a([Ljava/lang/Boolean;)Ljava/lang/Integer;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 164
    array-length v0, p1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 165
    aget-object v0, p1, v2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/settings/c;->c:Z

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c;->b:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    iget-boolean v3, p0, Lcom/google/android/gms/ads/settings/c;->c:Z

    invoke-static {v0, v3}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Z)V

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c;->b:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/gms/ads/settings/c;->c:Z

    const-string v4, "Calling this from your main thread can lead to deadlock"

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/gms/ads/identifier/b/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/b;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/common/eu; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/gms/common/ev; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v4

    :try_start_1
    invoke-virtual {v4}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/ads/identifier/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/ads/identifier/a/a;

    move-result-object v5

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v0}, Lcom/google/android/gms/ads/identifier/a/a;->a(Ljava/lang/String;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/common/eu; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/gms/common/ev; {:try_start_2 .. :try_end_2} :catch_4

    .line 182
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 164
    goto :goto_0

    .line 168
    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "AdvertisingIdSettersClient"

    const-string v5, "GMS remote exception "

    invoke-static {v2, v5, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Remote exception"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/android/gms/common/eu; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/google/android/gms/common/ev; {:try_start_4 .. :try_end_4} :catch_4

    .line 181
    :catch_1
    move-exception v0

    .line 172
    const-string v1, "AdsSettingsActivity"

    const-string v2, "Could not clear advertising ID."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 173
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 168
    :catch_2
    move-exception v0

    :try_start_5
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Interrupted exception"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 174
    :catch_3
    move-exception v0

    .line 175
    const-string v1, "AdsSettingsActivity"

    const-string v2, "Google Play services not available?"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 176
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 177
    :catch_4
    move-exception v0

    .line 178
    const-string v2, "AdsSettingsActivity"

    const-string v3, "Google Play services repairable."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 179
    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c;->a:Lcom/google/android/gms/common/ev;

    .line 180
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 158
    check-cast p1, [Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/google/android/gms/ads/settings/c;->a([Ljava/lang/Boolean;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 158
    check-cast p1, Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c;->b:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c;->b:Lcom/google/android/gms/ads/settings/AdsSettingsActivity;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c;->a:Lcom/google/android/gms/common/ev;

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/settings/AdsSettingsActivity;->a(Lcom/google/android/gms/ads/settings/AdsSettingsActivity;Lcom/google/android/gms/common/ev;)V

    :cond_0
    return-void
.end method
