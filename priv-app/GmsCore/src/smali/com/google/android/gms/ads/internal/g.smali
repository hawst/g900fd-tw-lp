.class final Lcom/google/android/gms/ads/internal/g;
.super Landroid/widget/ViewSwitcher;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/ads/internal/util/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 135
    invoke-direct {p0, p1}, Landroid/widget/ViewSwitcher;-><init>(Landroid/content/Context;)V

    .line 136
    new-instance v0, Lcom/google/android/gms/ads/internal/util/l;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/internal/util/l;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/g;->a:Lcom/google/android/gms/ads/internal/util/l;

    .line 137
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/ads/internal/g;)Lcom/google/android/gms/ads/internal/util/l;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/g;->a:Lcom/google/android/gms/ads/internal/util/l;

    return-object v0
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/g;->a:Lcom/google/android/gms/ads/internal/util/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/util/l;->a(Landroid/view/MotionEvent;)V

    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public final removeAllViews()V
    .locals 3

    .prologue
    .line 149
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/g;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 150
    invoke-virtual {p0, v1}, Lcom/google/android/gms/ads/internal/g;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/google/android/gms/ads/internal/p/a;

    if-eqz v2, :cond_0

    .line 152
    check-cast v0, Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->destroy()V

    .line 149
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 155
    :cond_1
    invoke-super {p0}, Landroid/widget/ViewSwitcher;->removeAllViews()V

    .line 156
    return-void
.end method
