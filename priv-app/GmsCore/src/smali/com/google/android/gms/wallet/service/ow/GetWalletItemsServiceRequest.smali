.class public Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private b:Lcom/google/aa/b/a/a/a/a/o;

.field private c:[B

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/u;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/service/ow/u;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/o;Z)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a:Landroid/accounts/Account;

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Lcom/google/aa/b/a/a/a/a/o;

    .line 31
    iput-boolean p3, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->d:Z

    .line 32
    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;[BZ)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a:Landroid/accounts/Account;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    .line 38
    iput-boolean p3, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->d:Z

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Landroid/accounts/Account;[BZB)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;-><init>(Landroid/accounts/Account;[BZ)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public final b()Lcom/google/aa/b/a/a/a/a/o;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Lcom/google/aa/b/a/a/a/a/o;

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    const-class v1, Lcom/google/aa/b/a/a/a/a/o;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/o;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Lcom/google/aa/b/a/a/a/a/o;

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Lcom/google/aa/b/a/a/a/a/o;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->d:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a:Landroid/accounts/Account;

    invoke-virtual {v0, p1, p2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b:Lcom/google/aa/b/a/a/a/a/o;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 88
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    return-void

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
