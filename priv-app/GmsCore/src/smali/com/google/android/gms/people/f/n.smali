.class public final Lcom/google/android/gms/people/f/n;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    const/16 v0, 0x50

    .line 147
    const-string v1, "com.google.android.gms"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v0

    .line 151
    :cond_1
    const-string v1, "com.google.android.gms.test.people"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms.test.aspen"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms.test.plus"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    const-string v0, "com.google.android.talk"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.google.android.apps.babel"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 159
    :cond_2
    const/16 v0, 0x75

    goto :goto_0

    .line 162
    :cond_3
    const-string v0, "com.google.android.play.games"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 163
    const/16 v0, 0x76

    goto :goto_0

    .line 166
    :cond_4
    const-string v0, "com.google.android.apps.plus"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 167
    const/4 v0, 0x1

    goto :goto_0

    .line 169
    :cond_5
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Exception;)Lcom/google/android/gms/people/service/b;
    .locals 3

    .prologue
    .line 75
    instance-of v0, p1, Lcom/google/android/gms/auth/ae;

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 77
    check-cast p1, Lcom/google/android/gms/auth/ae;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 81
    const-string v2, "pendingIntent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 83
    const/4 v1, 0x4

    invoke-static {v1, v0}, Lcom/google/android/gms/people/service/b;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/people/service/b;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    .line 86
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/auth/q;

    if-eqz v0, :cond_1

    .line 87
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/b;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/people/service/b;

    move-result-object v0

    goto :goto_0

    .line 90
    :cond_1
    instance-of v0, p1, Lcom/android/volley/ac;

    if-eqz v0, :cond_2

    .line 91
    check-cast p1, Lcom/android/volley/ac;

    invoke-static {p1}, Lcom/google/android/gms/people/f/n;->a(Lcom/android/volley/ac;)V

    .line 92
    sget-object v0, Lcom/google/android/gms/people/service/b;->g:Lcom/google/android/gms/people/service/b;

    goto :goto_0

    .line 94
    :cond_2
    sget-object v0, Lcom/google/android/gms/people/service/b;->f:Lcom/google/android/gms/people/service/b;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 114
    instance-of v0, p0, Lcom/android/volley/ac;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 115
    check-cast v0, Lcom/android/volley/ac;

    .line 116
    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-nez v1, :cond_0

    const-string v0, "none"

    .line 120
    :goto_0
    const-string v1, "%s [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 122
    :goto_1
    return-object v0

    .line 116
    :cond_0
    iget-object v0, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 122
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/RuntimeException;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 54
    if-nez p2, :cond_0

    .line 55
    throw p1

    .line 57
    :cond_0
    invoke-static {p0, p2}, Lcom/google/android/gms/people/f/n;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    throw p1

    .line 61
    :cond_1
    const-string v0, "PeopleService"

    const-string v1, "Caught exception, but account doesn\'t exist.  Ignoring."

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 62
    return-void
.end method

.method public static a(Lcom/android/volley/ac;)V
    .locals 2

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/gms/people/a/a;->aa:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 138
    :cond_0
    return-void

    .line 134
    :cond_1
    invoke-virtual {p0}, Lcom/android/volley/ac;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 135
    instance-of v1, p0, Lcom/android/volley/o;

    if-nez v1, :cond_2

    instance-of v0, v0, Lcom/google/android/gms/common/server/response/m;

    if-eqz v0, :cond_0

    .line 136
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Parse error"

    invoke-direct {v0, v1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 104
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 105
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 106
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    const-string v2, "%s (%s [%s])"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v0

    invoke-static {p2}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 110
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-nez v3, :cond_0

    instance-of v3, p2, Lcom/google/android/gms/auth/q;

    if-nez v3, :cond_1

    instance-of v3, p2, Lcom/android/volley/ac;

    if-nez v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    :goto_0
    invoke-static {p0, v2, p2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 111
    return-void

    .line 110
    :cond_2
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 41
    invoke-static {p0}, Lcom/google/android/gms/people/service/o;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 42
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 43
    const/4 v0, 0x1

    .line 46
    :cond_0
    return v0

    .line 41
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
