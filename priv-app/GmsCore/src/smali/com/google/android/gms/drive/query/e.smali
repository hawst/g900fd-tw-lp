.class public final Lcom/google/android/gms/drive/query/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/query/d;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/x;

.field private final b:Lcom/google/android/gms/drive/database/x;

.field private final c:Ljava/util/Collection;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/x;Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/drive/query/e;-><init>(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/x;Ljava/util/Collection;Z)V

    .line 23
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/x;Ljava/util/Collection;Z)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/drive/query/e;->a:Lcom/google/android/gms/drive/database/x;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/drive/query/e;->b:Lcom/google/android/gms/drive/database/x;

    .line 29
    iput-object p3, p0, Lcom/google/android/gms/drive/query/e;->c:Ljava/util/Collection;

    .line 30
    iput-boolean p4, p0, Lcom/google/android/gms/drive/query/e;->d:Z

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/d;
    .locals 5

    .prologue
    .line 35
    new-instance v1, Lcom/google/android/gms/drive/query/e;

    iget-object v2, p0, Lcom/google/android/gms/drive/query/e;->b:Lcom/google/android/gms/drive/database/x;

    iget-object v3, p0, Lcom/google/android/gms/drive/query/e;->a:Lcom/google/android/gms/drive/database/x;

    iget-object v4, p0, Lcom/google/android/gms/drive/query/e;->c:Ljava/util/Collection;

    iget-boolean v0, p0, Lcom/google/android/gms/drive/query/e;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/gms/drive/query/e;-><init>(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/x;Ljava/util/Collection;Z)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 40
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/query/e;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/query/d;

    .line 42
    iget-boolean v3, p0, Lcom/google/android/gms/drive/query/e;->d:Z

    if-eqz v3, :cond_0

    .line 43
    invoke-interface {v0}, Lcom/google/android/gms/drive/query/d;->a()Lcom/google/android/gms/drive/query/d;

    move-result-object v0

    .line 45
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/drive/query/d;->b()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/query/e;->a:Lcom/google/android/gms/drive/database/x;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/x;->a(Ljava/util/List;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method
