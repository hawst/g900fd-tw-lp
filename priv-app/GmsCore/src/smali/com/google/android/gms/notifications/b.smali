.class final Lcom/google/android/gms/notifications/b;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/notifications/GunsBrowserActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/notifications/GunsBrowserActivity;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/notifications/GunsBrowserActivity;B)V
    .locals 0

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/google/android/gms/notifications/b;-><init>(Lcom/google/android/gms/notifications/GunsBrowserActivity;)V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-static {v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a(Lcom/google/android/gms/notifications/GunsBrowserActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-static {v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c(Lcom/google/android/gms/notifications/GunsBrowserActivity;)V

    .line 249
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 233
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-static {v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a(Lcom/google/android/gms/notifications/GunsBrowserActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 235
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-static {v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a(Lcom/google/android/gms/notifications/GunsBrowserActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-static {v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b(Lcom/google/android/gms/notifications/GunsBrowserActivity;)Z

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-static {v0, p2, p3, p4}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a(Lcom/google/android/gms/notifications/GunsBrowserActivity;ILjava/lang/String;Ljava/lang/String;)V

    .line 243
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 253
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 255
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 256
    iget-object v1, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-static {v1}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->d(Lcom/google/android/gms/notifications/GunsBrowserActivity;)Ljava/util/Set;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-static {v1}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->d(Lcom/google/android/gms/notifications/GunsBrowserActivity;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "https"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getPort()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 258
    const/4 v0, 0x0

    .line 268
    :goto_0
    return v0

    .line 263
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 264
    iget-object v1, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->startActivity(Landroid/content/Intent;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/notifications/b;->a:Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->finish()V

    .line 268
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
