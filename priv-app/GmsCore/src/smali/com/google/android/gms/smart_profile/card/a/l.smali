.class final Lcom/google/android/gms/smart_profile/card/a/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/ac/c/a/a/a/ap;

.field final synthetic b:Lcom/google/android/gms/smart_profile/card/a/j;


# direct methods
.method constructor <init>(Lcom/google/android/gms/smart_profile/card/a/j;Lcom/google/ac/c/a/a/a/ap;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/a/l;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iput-object p2, p0, Lcom/google/android/gms/smart_profile/card/a/l;->a:Lcom/google/ac/c/a/a/a/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/l;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/smart_profile/c;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/l;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v1

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/l;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v2

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/l;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v3

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/l;->a:Lcom/google/ac/c/a/a/a/ap;

    iget-object v4, v0, Lcom/google/ac/c/a/a/a/ap;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/l;->a:Lcom/google/ac/c/a/a/a/ap;

    iget-object v5, v0, Lcom/google/ac/c/a/a/a/ap;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/l;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-array v2, v10, [Ljava/lang/Object;

    aput-object v1, v2, v8

    aput-object v5, v2, v9

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    move-object v1, v0

    .line 158
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/l;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 159
    return-void

    .line 156
    :cond_0
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->C:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v8

    aput-object v4, v7, v9

    aput-object v5, v7, v10

    invoke-static {v0, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v6, v1, v2, v3}, Lcom/google/android/gms/smart_profile/aj;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method
