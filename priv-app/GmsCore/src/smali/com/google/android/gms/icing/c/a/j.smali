.class public final Lcom/google/android/gms/icing/c/a/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/icing/c/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 175
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 176
    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/j;->cachedSize:I

    .line 177
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 227
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    if-eqz v1, :cond_0

    .line 229
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 188
    if-ne p1, p0, :cond_1

    .line 189
    const/4 v0, 0x1

    .line 204
    :cond_0
    :goto_0
    return v0

    .line 191
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/j;

    if-eqz v1, :cond_0

    .line 194
    check-cast p1, Lcom/google/android/gms/icing/c/a/j;

    .line 195
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    if-nez v1, :cond_3

    .line 196
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    if-nez v1, :cond_0

    .line 204
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/j;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 200
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 212
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/j;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    return v0

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/c/a/c;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 155
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/c/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    if-eqz v0, :cond_0

    .line 220
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/j;->a:Lcom/google/android/gms/icing/c/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 222
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 223
    return-void
.end method
