.class public final Lcom/google/android/gms/location/internal/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/location/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 44
    invoke-static {p1}, Lcom/google/android/gms/location/p;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/location/internal/w;

    move-result-object v0

    .line 46
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/w;->f()Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 159
    new-instance v0, Lcom/google/android/gms/location/internal/j;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/location/internal/j;-><init>(Lcom/google/android/gms/location/internal/d;Lcom/google/android/gms/common/api/v;Landroid/app/PendingIntent;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Landroid/location/Location;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 173
    new-instance v0, Lcom/google/android/gms/location/internal/k;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/gms/location/internal/k;-><init>(Lcom/google/android/gms/location/internal/d;Lcom/google/android/gms/common/api/v;Landroid/location/Location;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/n;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/gms/location/internal/e;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/location/internal/e;-><init>(Lcom/google/android/gms/location/internal/d;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/n;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/n;Landroid/os/Looper;)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/gms/location/internal/f;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/internal/f;-><init>(Lcom/google/android/gms/location/internal/d;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/n;Landroid/os/Looper;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/gms/location/internal/h;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/location/internal/h;-><init>(Lcom/google/android/gms/location/internal/d;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/internal/LocationRequestInternal;Landroid/app/PendingIntent;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/n;Landroid/os/Looper;)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    .line 106
    new-instance v0, Lcom/google/android/gms/location/internal/g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/internal/g;-><init>(Lcom/google/android/gms/location/internal/d;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/internal/LocationRequestInternal;Lcom/google/android/gms/location/n;Landroid/os/Looper;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/n;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lcom/google/android/gms/location/internal/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/location/internal/i;-><init>(Lcom/google/android/gms/location/internal/d;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/n;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
