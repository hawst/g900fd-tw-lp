.class final Lcom/google/android/gms/drive/ui/picker/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/aj;

.field final synthetic b:Lcom/google/android/gms/drive/ui/picker/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/aj;)V
    .locals 0

    .prologue
    .line 796
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/o;->b:Lcom/google/android/gms/drive/ui/picker/m;

    iput-object p2, p0, Lcom/google/android/gms/drive/ui/picker/o;->a:Lcom/google/android/gms/drive/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 4

    .prologue
    .line 796
    check-cast p1, Lcom/google/android/gms/drive/k;

    invoke-interface {p1}, Lcom/google/android/gms/drive/k;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/drive/k;->b()Lcom/google/android/gms/drive/ak;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/o;->b:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/ak;)Lcom/google/android/gms/drive/aj;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/o;->b:Lcom/google/android/gms/drive/ui/picker/m;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/o;->a:Lcom/google/android/gms/drive/aj;

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/aj;Lcom/google/android/gms/drive/aj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/ak;->w_()V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/ak;->w_()V

    throw v0

    :cond_0
    const-string v0, "PickEntryDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to list parents, status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/drive/k;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
