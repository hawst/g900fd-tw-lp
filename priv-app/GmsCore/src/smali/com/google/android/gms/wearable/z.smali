.class public final Lcom/google/android/gms/wearable/z;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/wearable/d;

.field public static final b:Lcom/google/android/gms/wearable/o;

.field public static final c:Lcom/google/android/gms/wearable/t;

.field public static final d:Lcom/google/android/gms/wearable/b;

.field public static final e:Lcom/google/android/gms/wearable/y;

.field public static final f:Lcom/google/android/gms/common/api/j;

.field public static final g:Lcom/google/android/gms/common/api/c;

.field private static final h:Lcom/google/android/gms/common/api/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/wearable/internal/e;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/z;->a:Lcom/google/android/gms/wearable/d;

    .line 27
    new-instance v0, Lcom/google/android/gms/wearable/internal/ao;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/ao;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/z;->b:Lcom/google/android/gms/wearable/o;

    .line 28
    new-instance v0, Lcom/google/android/gms/wearable/internal/as;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/as;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/z;->c:Lcom/google/android/gms/wearable/t;

    .line 32
    new-instance v0, Lcom/google/android/gms/wearable/internal/d;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/z;->d:Lcom/google/android/gms/wearable/b;

    .line 36
    new-instance v0, Lcom/google/android/gms/wearable/internal/bc;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/internal/bc;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/z;->e:Lcom/google/android/gms/wearable/y;

    .line 56
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/z;->f:Lcom/google/android/gms/common/api/j;

    .line 60
    new-instance v0, Lcom/google/android/gms/wearable/aa;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/aa;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/z;->h:Lcom/google/android/gms/common/api/i;

    .line 82
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/wearable/z;->h:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/wearable/z;->f:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/wearable/z;->g:Lcom/google/android/gms/common/api/c;

    return-void
.end method
