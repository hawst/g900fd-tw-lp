.class final Lcom/google/android/gms/plus/internal/az;
.super Lcom/google/android/gms/plus/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/ay;

.field private final b:Lcom/google/android/gms/plus/internal/ai;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ai;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/az;->a:Lcom/google/android/gms/plus/internal/ay;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/b;-><init>()V

    .line 152
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/az;->b:Lcom/google/android/gms/plus/internal/ai;

    .line 153
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 5

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    if-eqz p2, :cond_0

    .line 160
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 162
    :cond_0
    new-instance v1, Lcom/google/android/gms/common/c;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/az;->a:Lcom/google/android/gms/plus/internal/ay;

    new-instance v2, Lcom/google/android/gms/plus/internal/ba;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/az;->a:Lcom/google/android/gms/plus/internal/ay;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/az;->b:Lcom/google/android/gms/plus/internal/ai;

    invoke-direct {v2, v3, v4, v1, p3}, Lcom/google/android/gms/plus/internal/ba;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ai;Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/posts/Comment;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/internal/ay;->a(Lcom/google/android/gms/common/internal/al;)V

    .line 164
    return-void
.end method
