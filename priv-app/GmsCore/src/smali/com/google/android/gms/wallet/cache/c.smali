.class public final Lcom/google/android/gms/wallet/cache/c;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/android/volley/s;

.field private final c:Landroid/accounts/Account;

.field private final d:I

.field private final e:Lcom/google/android/gms/wallet/cache/e;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/volley/s;Landroid/accounts/Account;ILcom/google/android/gms/wallet/cache/e;Landroid/os/Looper;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0, p6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/cache/c;->i:I

    .line 62
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iput-object p1, p0, Lcom/google/android/gms/wallet/cache/c;->a:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/google/android/gms/wallet/cache/c;->b:Lcom/android/volley/s;

    .line 67
    iput-object p3, p0, Lcom/google/android/gms/wallet/cache/c;->c:Landroid/accounts/Account;

    .line 68
    iput p4, p0, Lcom/google/android/gms/wallet/cache/c;->d:I

    .line 69
    invoke-static {p4}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->f:Ljava/lang/String;

    .line 70
    iput-object p5, p0, Lcom/google/android/gms/wallet/cache/c;->e:Lcom/google/android/gms/wallet/cache/e;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/c;->c:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/wallet/cache/c;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    .line 72
    return-void
.end method

.method static synthetic a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 36
    sparse-switch p0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported environment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v0, "https://checkout.google.com/inapp/api/v1/get_profile"

    :goto_0
    return-object v0

    :sswitch_1
    const-string v0, "https://sandbox.google.com/checkout/inapp/api/v1/get_profile"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0x15 -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/cache/c;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/gms/wallet/cache/c;->h:Ljava/lang/String;

    return-object p1
.end method

.method private a(ILcom/google/checkout/inapp/proto/ai;)V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->e:Lcom/google/android/gms/wallet/cache/e;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->e:Lcom/google/android/gms/wallet/cache/e;

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/c;->c:Landroid/accounts/Account;

    iget v2, p0, Lcom/google/android/gms/wallet/cache/c;->d:I

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/google/android/gms/wallet/cache/e;->a(Landroid/accounts/Account;IILcom/google/checkout/inapp/proto/ai;)V

    .line 191
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/cache/c;I)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/cache/c;->a(ILcom/google/checkout/inapp/proto/ai;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/cache/c;)Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->c:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/cache/c;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/gms/wallet/cache/c;->d:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/cache/c;)Lcom/android/volley/s;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->b:Lcom/android/volley/s;

    return-object v0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 126
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 139
    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "unrecognized response type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :goto_0
    return-void

    .line 129
    :pswitch_0
    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    invoke-static {v0}, Lcom/google/checkout/inapp/proto/ai;->a([B)Lcom/google/checkout/inapp/proto/ai;

    move-result-object v0

    iget v1, v0, Lcom/google/checkout/inapp/proto/ai;->a:I

    sparse-switch v1, :sswitch_data_0

    const-string v1, "GetWalletProfileTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "error GetProfilePostResponse, accountStatus="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/google/checkout/inapp/proto/ai;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/cache/c;->a(ILcom/google/checkout/inapp/proto/ai;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "error parsing proto response"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-direct {p0, v4, v5}, Lcom/google/android/gms/wallet/cache/c;->a(ILcom/google/checkout/inapp/proto/ai;)V

    goto :goto_0

    .line 129
    :sswitch_0
    :try_start_1
    const-string v1, "GetWalletProfileTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "valid profile response"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/cache/c;->a(ILcom/google/checkout/inapp/proto/ai;)V

    goto :goto_0

    :sswitch_1
    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "NOT_AUTHENTICATED, retry"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/google/android/gms/wallet/cache/c;->i:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/cache/c;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/cache/c;->i:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/c;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/cache/c;->run()V

    goto/16 :goto_0

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/cache/c;->a(ILcom/google/checkout/inapp/proto/ai;)V
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 136
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/volley/ac;

    instance-of v1, v0, Lcom/android/volley/n;

    if-nez v1, :cond_1

    instance-of v1, v0, Lcom/android/volley/ab;

    if-eqz v1, :cond_2

    :cond_1
    const-string v1, "GetWalletProfileTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "network error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0, v5}, Lcom/google/android/gms/wallet/cache/c;->a(ILcom/google/checkout/inapp/proto/ai;)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v1, :cond_3

    const-string v1, "GetWalletProfileTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "error status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-direct {p0, v4, v5}, Lcom/google/android/gms/wallet/cache/c;->a(ILcom/google/checkout/inapp/proto/ai;)V

    goto/16 :goto_0

    :cond_3
    const-string v1, "GetWalletProfileTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/wallet/cache/c;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/volley/ac;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 129
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/gms/wallet/cache/d;

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/c;->a:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/wallet/cache/d;-><init>(Lcom/google/android/gms/wallet/cache/c;Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 77
    return-void
.end method
