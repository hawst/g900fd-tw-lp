.class public Lcom/google/android/gms/wallet/common/ui/validator/l;
.super Lcom/google/android/gms/wallet/common/ui/validator/r;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/r;-><init>(Ljava/lang/CharSequence;)V

    .line 17
    if-nez p2, :cond_0

    .line 18
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pattern must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/validator/l;->a:Ljava/util/regex/Pattern;

    .line 21
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;)Z
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/l;->a:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method
