.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;
.super Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;
.source "SourceFile"


# instance fields
.field private b:Ljava/util/Date;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;J)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 28
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p2, p3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;->b:Ljava/util/Date;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/d/g;Lcom/google/android/gms/drive/d/d;)V
    .locals 6

    .prologue
    .line 34
    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->p()Ljava/lang/String;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_0

    .line 37
    :try_start_0
    invoke-static {v1}, Lcom/google/android/gms/drive/database/h;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 38
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;->b:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;->b:Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a(Lcom/google/android/gms/drive/d/g;Lcom/google/android/gms/drive/d/d;)V

    .line 46
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    const-string v2, "UpdatedDateMonitorProcessor"

    const-string v3, "Error parsing date %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v0, v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;->b:Ljava/util/Date;

    .line 54
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a(Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public final b()Ljava/util/Date;
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;->b:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;->b:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method
