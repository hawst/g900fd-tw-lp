.class public final Lcom/google/android/gms/googlehelp/e/i;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/gms/googlehelp/e/c;

.field public b:[Lcom/google/android/gms/googlehelp/e/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1760
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1761
    invoke-static {}, Lcom/google/android/gms/googlehelp/e/c;->a()[Lcom/google/android/gms/googlehelp/e/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    invoke-static {}, Lcom/google/android/gms/googlehelp/e/c;->a()[Lcom/google/android/gms/googlehelp/e/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/e/i;->cachedSize:I

    .line 1762
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1825
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1826
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 1827
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1828
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    aget-object v3, v3, v0

    .line 1829
    if-eqz v3, :cond_0

    .line 1830
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1827
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1835
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 1836
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 1837
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    aget-object v2, v2, v1

    .line 1838
    if-eqz v2, :cond_3

    .line 1839
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1836
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1844
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1773
    if-ne p1, p0, :cond_1

    .line 1788
    :cond_0
    :goto_0
    return v0

    .line 1776
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/googlehelp/e/i;

    if-nez v2, :cond_2

    move v0, v1

    .line 1777
    goto :goto_0

    .line 1779
    :cond_2
    check-cast p1, Lcom/google/android/gms/googlehelp/e/i;

    .line 1780
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1782
    goto :goto_0

    .line 1784
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1786
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1793
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1796
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1798
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1737
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/googlehelp/e/c;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/googlehelp/e/c;

    invoke-direct {v3}, Lcom/google/android/gms/googlehelp/e/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/googlehelp/e/c;

    invoke-direct {v3}, Lcom/google/android/gms/googlehelp/e/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/googlehelp/e/c;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/googlehelp/e/c;

    invoke-direct {v3}, Lcom/google/android/gms/googlehelp/e/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/googlehelp/e/c;

    invoke-direct {v3}, Lcom/google/android/gms/googlehelp/e/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1804
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 1805
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1806
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/i;->a:[Lcom/google/android/gms/googlehelp/e/c;

    aget-object v2, v2, v0

    .line 1807
    if-eqz v2, :cond_0

    .line 1808
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1805
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1812
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1813
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 1814
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/i;->b:[Lcom/google/android/gms/googlehelp/e/c;

    aget-object v0, v0, v1

    .line 1815
    if-eqz v0, :cond_2

    .line 1816
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1813
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1820
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1821
    return-void
.end method
