.class public final Lcom/google/android/gms/people/sync/a/o;
.super Landroid/support/v4/g/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/support/v4/g/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/people/sync/a/k;)V
    .locals 8

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/a/o;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/sync/a/n;

    .line 59
    iget-wide v4, v0, Lcom/google/android/gms/people/sync/a/n;->a:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_2

    iget-boolean v1, v0, Lcom/google/android/gms/people/sync/a/n;->e:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v1, p1, Lcom/google/android/gms/people/sync/a/k;->c:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Lcom/google/android/gms/people/sync/a/k;->c:Ljava/util/ArrayList;

    :cond_0
    iget-object v1, p1, Lcom/google/android/gms/people/sync/a/k;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    iget-boolean v1, v0, Lcom/google/android/gms/people/sync/a/n;->e:Z

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/google/android/gms/people/sync/a/k;->e:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Lcom/google/android/gms/people/sync/a/k;->e:Ljava/util/ArrayList;

    :cond_3
    iget-object v1, p1, Lcom/google/android/gms/people/sync/a/k;->e:Ljava/util/ArrayList;

    iget-wide v4, v0, Lcom/google/android/gms/people/sync/a/n;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-object v1, p1, Lcom/google/android/gms/people/sync/a/k;->d:Ljava/util/ArrayList;

    if-nez v1, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Lcom/google/android/gms/people/sync/a/k;->d:Ljava/util/ArrayList;

    :cond_5
    iget-object v1, p1, Lcom/google/android/gms/people/sync/a/k;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 61
    :cond_6
    return-void
.end method
