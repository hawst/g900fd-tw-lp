.class public final Lcom/google/android/gms/googlehelp/common/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/googlehelp/common/d;

.field public static final b:Lcom/google/android/gms/googlehelp/common/d;

.field public static final c:Lcom/google/android/gms/googlehelp/common/d;

.field public static final d:Lcom/google/android/gms/googlehelp/common/d;

.field public static final e:Lcom/google/android/gms/googlehelp/common/d;

.field public static final f:Ljava/util/List;


# instance fields
.field private final g:Lcom/google/android/gms/googlehelp/e/c;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 17
    new-instance v0, Lcom/google/android/gms/googlehelp/common/d;

    const-string v1, "product_id_string"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/googlehelp/common/d;->a:Lcom/google/android/gms/googlehelp/common/d;

    .line 19
    new-instance v0, Lcom/google/android/gms/googlehelp/common/d;

    const-string v1, "top_level_topic_url"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/googlehelp/common/d;->b:Lcom/google/android/gms/googlehelp/common/d;

    .line 21
    new-instance v0, Lcom/google/android/gms/googlehelp/common/d;

    const-string v1, "contact_card_position"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/googlehelp/common/d;->c:Lcom/google/android/gms/googlehelp/common/d;

    .line 25
    new-instance v0, Lcom/google/android/gms/googlehelp/common/d;

    const-string v1, "answer_rendering_url_format"

    const-string v2, "https://clients6.google.com/support/v1/renderedObject?id=%s&hc_host=support.google.com&hc_path=%%2F%s&hl=%s&page_type=ANSWER&render=FULL&component=PCT_NONE&extra_param=gapi.1&key=AIzaSyC4gyROYSkqjyykTdfouAxjwLBLYAk-XJE"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/googlehelp/common/d;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    sput-object v0, Lcom/google/android/gms/googlehelp/common/d;->d:Lcom/google/android/gms/googlehelp/common/d;

    .line 41
    new-instance v0, Lcom/google/android/gms/googlehelp/common/d;

    const-string v1, "topic_rendering_url_format"

    const-string v2, "https://clients6.google.com/support/v1/renderedObject?id=%s&hc_host=support.google.com&hc_path=%%2F%s&hl=%s&page_type=TOPIC&render=FULL&component=PCT_NONE&extra_param=gapi.1&key=AIzaSyC4gyROYSkqjyykTdfouAxjwLBLYAk-XJE"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/googlehelp/common/d;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    sput-object v0, Lcom/google/android/gms/googlehelp/common/d;->e:Lcom/google/android/gms/googlehelp/common/d;

    .line 57
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/gms/googlehelp/common/d;

    sget-object v1, Lcom/google/android/gms/googlehelp/common/d;->a:Lcom/google/android/gms/googlehelp/common/d;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/googlehelp/common/d;->b:Lcom/google/android/gms/googlehelp/common/d;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/gms/googlehelp/common/d;->c:Lcom/google/android/gms/googlehelp/common/d;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/gms/googlehelp/common/d;->d:Lcom/google/android/gms/googlehelp/common/d;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/gms/googlehelp/common/d;->e:Lcom/google/android/gms/googlehelp/common/d;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/common/d;->f:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;I)Lcom/google/android/gms/googlehelp/e/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/d;->g:Lcom/google/android/gms/googlehelp/e/c;

    .line 68
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/common/d;->h:Ljava/lang/String;

    .line 69
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;B)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;I)Lcom/google/android/gms/googlehelp/e/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/d;->g:Lcom/google/android/gms/googlehelp/e/c;

    .line 73
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/common/d;->h:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public static d()Ljava/util/Map;
    .locals 4

    .prologue
    .line 93
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 94
    sget-object v0, Lcom/google/android/gms/googlehelp/common/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/d;

    .line 95
    iget-object v3, v0, Lcom/google/android/gms/googlehelp/common/d;->g:Lcom/google/android/gms/googlehelp/e/c;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 97
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/googlehelp/e/c;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/d;->g:Lcom/google/android/gms/googlehelp/e/c;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/d;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/d;->g:Lcom/google/android/gms/googlehelp/e/c;

    iget v0, v0, Lcom/google/android/gms/googlehelp/e/c;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
