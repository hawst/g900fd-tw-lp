.class public Lcom/google/android/gms/cast/media/al;
.super Lcom/google/android/gms/cast/internal/b;
.source "SourceFile"


# static fields
.field private static final a:I

.field public static final b:Ljava/lang/String;

.field private static final c:I


# instance fields
.field private final d:Lcom/google/android/gms/cast/CastDevice;

.field private final e:Ljava/lang/String;

.field private f:J

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "com.google.cast.webrtc"

    invoke-static {v0}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/media/al;->b:Ljava/lang/String;

    .line 28
    sget-object v0, Lcom/google/android/gms/cast/a/d;->a:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/media/al;->a:I

    .line 29
    sget-object v0, Lcom/google/android/gms/cast/a/d;->b:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/media/al;->c:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/gms/cast/media/al;->b:Ljava/lang/String;

    const-string v1, "MirroringControlChannel"

    invoke-direct {p0, v0, v1, p3}, Lcom/google/android/gms/cast/internal/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/cast/media/al;->f:J

    .line 67
    iput-object p1, p0, Lcom/google/android/gms/cast/media/al;->d:Lcom/google/android/gms/cast/CastDevice;

    .line 68
    iput-object p2, p0, Lcom/google/android/gms/cast/media/al;->e:Ljava/lang/String;

    .line 69
    return-void
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 12

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onAnswer"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    :try_start_0
    const-string v0, "result"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    const-string v1, "ok"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 133
    new-instance v0, Lorg/json/JSONObject;

    const-string v1, "answer"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 134
    const-string v1, "udpPort"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v2, v1

    .line 135
    const-string v1, "sendIndexes"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 136
    const-string v1, "ssrcs"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 137
    const-string v1, "rtpExtensions"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 139
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/al;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 140
    const-string v0, "sendFormats"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 141
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 142
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getInt(I)I

    move-result v0

    .line 143
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lt v0, v9, :cond_0

    .line 144
    new-instance v1, Lorg/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "received bad index: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :catch_0
    move-exception v0

    .line 166
    iget-object v1, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "Malformed message received: %s, error: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169
    :goto_1
    return-void

    .line 146
    :cond_0
    :try_start_1
    invoke-virtual {v8, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 147
    const-string v9, "remote_rtp_port"

    invoke-virtual {v0, v9, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 148
    if-eqz v5, :cond_1

    .line 149
    const-string v9, "remote_ssrc"

    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v0, v9, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 151
    :cond_1
    if-eqz v6, :cond_2

    .line 152
    const-string v9, "rtp_extensions"

    invoke-virtual {v6, v1}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 141
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 155
    :cond_3
    const-string v0, "remoteFriendlyName"

    iget-object v1, p0, Lcom/google/android/gms/cast/media/al;->d:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "device IP: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/cast/media/al;->d:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "device friendly name: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/cast/media/al;->d:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/cast/media/al;->d:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/al;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 159
    :cond_4
    const-string v1, "error"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 160
    const-string v0, "error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 161
    new-instance v1, Lorg/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "received error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 163
    :cond_5
    new-instance v1, Lorg/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "received invalid result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private b()Lorg/json/JSONObject;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 185
    const-string v1, "{                                              \"udpPort\": 2346,                         \"supportedStreams\": [                       {                                            \"index\": 0,                              \"type\": \"audio_source\",                \"codecName\": \"aac\",                    \"rtpProfile\": \"cast\",                  \"rtpPayloadType\": 127,                   \"ssrc\": 1,                               \"storeTime\": 100,                        \"targetDelay\": 100,                      \"bitRate\": 128000,                       \"sampleRate\": 48000,                     \"timeBase\": \"1/48000\",                 \"channels\": 2,                           \"rtpExtensions\": [                           \"adaptive_playout_delay\"             ]                                        },                                         {                                            \"index\": 1,                              \"type\": \"video_source\",                \"codecName\": \"h264\",                   \"rtpProfile\": \"cast\",                  \"rtpPayloadType\": 96,                    \"ssrc\": 11,                              \"storeTime\": 100,                        \"targetDelay\": 100,                      \"maxFrameRate\": \"60000/1001\",          \"timeBase\": \"1/90000\",                 \"maxBitRate\": 6000000,                   \"profile\": \"main\",                     \"level\": \"4\",                          \"resolutions\": [                             {                                            \"height\": 720,                           \"width\": 1280                          }                                      ],                                         \"rtpExtensions\": [                           \"adaptive_playout_delay\"             ]                                        }                                       ]                                      }"

    .line 234
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 235
    const-string v2, "supportedStreams"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 236
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 238
    const-string v4, "storeTime"

    sget v5, Lcom/google/android/gms/cast/media/al;->c:I

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 239
    const-string v4, "targetDelay"

    sget v5, Lcom/google/android/gms/cast/media/al;->c:I

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 241
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 243
    const-string v3, "aesKey"

    iget-object v4, p0, Lcom/google/android/gms/cast/media/al;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 244
    const-string v3, "aesIvMask"

    iget-object v4, p0, Lcom/google/android/gms/cast/media/al;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 246
    const-string v3, "maxBitRate"

    sget v4, Lcom/google/android/gms/cast/media/al;->a:I

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 247
    const-string v3, "storeTime"

    sget v4, Lcom/google/android/gms/cast/media/al;->c:I

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 248
    const-string v3, "targetDelay"

    sget v4, Lcom/google/android/gms/cast/media/al;->c:I

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    :goto_0
    return-object v0

    .line 252
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "Malformed config string; ignoring: %s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 258
    const-string v0, "{                                              \"sendFormats\": [                            {                                            \"ssrc\": 1,                               \"rtp_payload_type\": 127,                 \"rtp_port\": 2346,                        \"remote_ssrc\": 2,                        \"remote_rtp_port\": 2344,                 \"codec_name\": \"aac_ld_adts\",           \"index\": 0,                              \"rtp_profile\": \"cast\",                 \"sample_rate\": 48000,                    \"time_base\": 48000,                      \"channels\": 2,                           \"store_time\": 100,                       \"bit_rate\": 128000,                      \"type\": \"audio_source\"               },                                         {                                            \"ssrc\": 11,                              \"rtp_payload_type\": 96,                  \"rtp_port\": 2346,                        \"remote_ssrc\": 12,                       \"remote_rtp_port\": 2344,                 \"codec_name\": \"h264\",                  \"index\": 1,                              \"max_bit_rate\": 6000000,                 \"max_frame_rate\": 60,                    \"resolutions\": [                             {                                            \"height\": 720,                           \"width\": 1280                          }                                        ],                                       \"rtp_profile\": \"cast\",                 \"store_time\": 100,                       \"time_base\": 90000,                      \"type\": \"video_source\"               }                                       ]                                      }"

    .line 303
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 304
    const-string v2, "sendFormats"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 306
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 307
    const-string v4, "store_time"

    sget v5, Lcom/google/android/gms/cast/media/al;->c:I

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 309
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 310
    const-string v3, "aes-key"

    iget-object v4, p0, Lcom/google/android/gms/cast/media/al;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 311
    const-string v3, "aes-iv-mask"

    iget-object v4, p0, Lcom/google/android/gms/cast/media/al;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 312
    const-string v3, "max_bit_rate"

    sget v4, Lcom/google/android/gms/cast/media/al;->a:I

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 314
    const-string v3, "store_time"

    sget v4, Lcom/google/android/gms/cast/media/al;->c:I

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 316
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 321
    :goto_0
    return-object v0

    .line 318
    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "Malformed config string; ignoring: %s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static f()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x10

    .line 325
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    .line 326
    new-array v1, v4, [B

    .line 327
    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 328
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v0, 0x20

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 329
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 330
    aget-byte v3, v1, v0

    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v3, v3, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    aget-byte v3, v1, v0

    and-int/lit8 v3, v3, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 72
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 74
    invoke-static {}, Lcom/google/android/gms/cast/media/al;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/cast/media/al;->g:Ljava/lang/String;

    .line 75
    invoke-static {}, Lcom/google/android/gms/cast/media/al;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/cast/media/al;->h:Ljava/lang/String;

    .line 76
    iget-wide v2, p0, Lcom/google/android/gms/cast/media/al;->f:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/google/android/gms/cast/media/al;->f:J

    .line 79
    :try_start_0
    const-string v1, "sessionId"

    iget-object v4, p0, Lcom/google/android/gms/cast/media/al;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 80
    const-string v1, "seqNum"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 81
    const-string v1, "type"

    const-string v4, "OFFER"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 82
    const-string v1, "offer"

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/al;->b()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/media/al;->e:Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/gms/cast/media/al;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 88
    return-void

    .line 84
    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v4, "Failed to construct JSONObject for offer!"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "message received: %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 108
    const-string v1, "type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 110
    const-string v2, "ANSWER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/media/al;->a(Lorg/json/JSONObject;)V

    .line 120
    :goto_0
    return-void

    .line 112
    :cond_0
    const-string v0, "STATISTICS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onStatistics"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 118
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Malformed message received; ignoring: %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 115
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/al;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Bad message!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 173
    return-void
.end method
