.class public final Lcom/google/android/gms/smart_profile/card/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/smart_profile/card/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/database/Cursor;)Ljava/util/List;
    .locals 14

    .prologue
    .line 103
    const/4 v0, -0x1

    :try_start_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 104
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 105
    const/4 v0, 0x0

    .line 106
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 107
    const-string v1, "date"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 108
    const-string v1, "new"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 109
    const-string v1, "type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 110
    const-string v1, "number"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 111
    const-string v1, "voicemail_uri"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object v1, v0

    .line 112
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 113
    new-instance v9, Lcom/google/ac/c/a/a/a/ag;

    invoke-direct {v9}, Lcom/google/ac/c/a/a/a/ag;-><init>()V

    .line 114
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    .line 117
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 120
    const-wide/16 v12, 0x3e8

    div-long v12, v10, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    .line 121
    invoke-static {v10, v11, p0}, Lcom/google/android/gms/smart_profile/as;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v10, "tel:"

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    .line 125
    new-instance v10, Lcom/google/ac/c/a/a/a/ae;

    invoke-direct {v10}, Lcom/google/ac/c/a/a/a/ae;-><init>()V

    .line 126
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 127
    packed-switch v11, :pswitch_data_0

    .line 159
    sget v0, Lcom/google/android/gms/p;->wk:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    .line 160
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/google/ac/c/a/a/a/ae;->a:Ljava/lang/Integer;

    .line 165
    :cond_1
    :goto_1
    const/4 v0, 0x4

    if-ne v11, v0, :cond_9

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 168
    :goto_2
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v10, Lcom/google/ac/c/a/a/a/ae;->b:Ljava/lang/Integer;

    .line 169
    invoke-interface {p1, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 170
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v11, 0x1

    if-ne v1, v11, :cond_a

    .line 171
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v10, Lcom/google/ac/c/a/a/a/ae;->b:Ljava/lang/Integer;

    .line 176
    :cond_2
    :goto_3
    iput-object v10, v9, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    .line 178
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 179
    goto :goto_0

    .line 129
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/google/ac/c/a/a/a/ae;->a:Ljava/lang/Integer;

    .line 130
    sget v0, Lcom/google/android/gms/p;->wh:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 182
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_3

    .line 183
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 133
    :pswitch_1
    const/4 v0, 0x2

    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/google/ac/c/a/a/a/ae;->a:Ljava/lang/Integer;

    .line 134
    sget v0, Lcom/google/android/gms/p;->wj:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    goto :goto_1

    .line 137
    :pswitch_2
    invoke-static {p0}, Lcom/google/android/gms/smart_profile/card/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_6

    :cond_4
    const/4 v0, 0x0

    :goto_4
    if-nez v0, :cond_0

    .line 140
    :cond_5
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/google/ac/c/a/a/a/ae;->a:Ljava/lang/Integer;

    .line 143
    sget v0, Lcom/google/android/gms/p;->wi:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    goto :goto_1

    .line 137
    :cond_6
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    const/4 v0, 0x1

    goto :goto_4

    :cond_7
    const-string v12, "us"

    invoke-static {v1, v12}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "us"

    invoke-static {v0, v13}, Landroid/telephony/PhoneNumberUtils;->formatNumberToE164(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v12, :cond_8

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    .line 146
    :pswitch_3
    invoke-static {p0}, Lcom/google/android/gms/smart_profile/card/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/google/ac/c/a/a/a/ae;->c:Ljava/lang/Integer;

    .line 150
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v10, Lcom/google/ac/c/a/a/a/ae;->a:Ljava/lang/Integer;

    .line 151
    sget v0, Lcom/google/android/gms/p;->wl:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    .line 153
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 154
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 155
    iput-object v0, v9, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    goto/16 :goto_1

    .line 165
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 173
    :cond_a
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v10, Lcom/google/ac/c/a/a/a/ae;->b:Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    .line 182
    :cond_b
    if-eqz p1, :cond_c

    .line 183
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_c
    return-object v2

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 236
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.android.voicemail.permission.READ_VOICEMAIL"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
