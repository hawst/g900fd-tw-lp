.class public abstract Lcom/google/android/gms/cast/b/aa;
.super Lcom/google/android/gms/cast/internal/b;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/HashSet;

.field public static final b:Ljava/lang/String;

.field private static c:Ljava/security/cert/CertificateFactory;


# instance fields
.field private d:I

.field private e:I

.field private final f:Ljava/lang/String;

.field private final g:[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 42
    const-string v0, "com.google.cast.tp.deviceauth"

    invoke-static {v0}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/b/aa;->b:Ljava/lang/String;

    .line 119
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/b/aa;->a:Ljava/util/HashSet;

    .line 120
    const-string v0, "X.509"

    invoke-static {v0}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/b/aa;->c:Ljava/security/cert/CertificateFactory;

    .line 121
    const-string v0, "MIIDwzCCAqugAwIBAgIBATANBgkqhkiG9w0BAQUFADB8MQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNTW91bnRhaW4gVmlldzETMBEGA1UECgwKR29vZ2xlIEluYzESMBAGA1UECwwJR29vZ2xlIFRWMRcwFQYDVQQDDA5FdXJla2EgUm9vdCBDQTAeFw0xMjEyMTcyMjM5MzNaFw0zMjEyMTIyMjM5MzNaMHwxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApDYWxpZm9ybmlhMRYwFAYDVQQHDA1Nb3VudGFpbiBWaWV3MRMwEQYDVQQKDApHb29nbGUgSW5jMRIwEAYDVQQLDAlHb29nbGUgVFYxFzAVBgNVBAMMDkV1cmVrYSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuRHQ6hLcMuHfXDNrGXMdnZ7QOXa/pYQJpv1ubencjzZO6YgCvZ/06ET9TPWaAlZqRypjbFhFzHxmJNx5ecMqpLKLoPeitc0Gftu+7AyG8g0kYHSEyikjhALYp+078ewmR1TjsS3mZA/2csXpmFIXwPzyLCDIQPhHyTKeO5exi/WYJHBjZhnBUugEBT1fjbzYS693mG8feNG2UCdN5OwUaWcfWK+poBEmPJQyB3/X6Wkfrj9PY4qPidbyGXhcIY6xtlfYwOHufW7d8ToKavG6//mDL9y1pCAXYzbvyGIZzFbOsuoxiUt4WMG/AxOZ4BLyiKqblNrddnkXHjTRCsQHRQIDAQABo1AwTjAdBgNVHQ4EFgQURE4qR1jYuUiR9k/OdKkdMpqNjekwHwYDVR0jBBgwFoAURE4qR1jYuUiR9k/OdKkdMpqNjekwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOCAQEAP8gmoG5cBUB5oZipM95odIXurrccM1mwEd6f9E/T61EJfUd+blGF9FTNg5glsbqwV+yT2xLi7FFJepZzm8iWbYWM0+E8+jLiWAx3bYcMNAGqMKl24MDn214b6RAwpOAJSSa5WM1aB+VQdd6aO/ZTfrFTXkUnTxfjCDOyUAq79PwllyneQXUw+nc4qmWKc0/qEXvrfBdgJw68PnZS2IvtGvjrN7sR/a5wFwr+4K0Gsx9pinIEwsAzC9YvY0wzERS4YjaIxQNlARmj7wC7bw6S/zQcodYx0Fxen5l9x8q9fHIL9Fylfm4EqNKZLFEBFP6iSPB+voQNtNPi8w593ov1Mw=="

    invoke-static {v0}, Lcom/google/android/gms/cast/b/aa;->b(Ljava/lang/String;)Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 122
    sget-object v1, Lcom/google/android/gms/cast/b/aa;->a:Ljava/util/HashSet;

    new-instance v2, Ljava/security/cert/TrustAnchor;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Ljava/security/cert/TrustAnchor;-><init>(Ljava/security/cert/X509Certificate;[B)V

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 123
    const-string v0, "MIIDxTCCAq2gAwIBAgIBAjANBgkqhkiG9w0BAQUFADB1MQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNTW91bnRhaW4gVmlldzETMBEGA1UECgwKR29vZ2xlIEluYzENMAsGA1UECwwEQ2FzdDEVMBMGA1UEAwwMQ2FzdCBSb290IENBMB4XDTE0MDQwMjE3MzQyNloXDTM0MDMyODE3MzQyNlowdTELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDU1vdW50YWluIFZpZXcxEzARBgNVBAoMCkdvb2dsZSBJbmMxDTALBgNVBAsMBENhc3QxFTATBgNVBAMMDENhc3QgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALrZZZ3aOdPBd/bU0K6PWAhoOUqV7XDP/XkIqarl6binLaBnR4qeyc9wswWHaRHscJiXw+bDw+u9xrA9/E/BXjif2s9zMAZbeTfBXoyHR5SaQZIq1pXEcVwnXQixgMaSvRvjQZeh7HWfVZ4+n48cx2VkB9OzlqEEn5HE3gp7bNnIwHgxoBlCqeiD48788c7CLiRGlQkZysBGsuUButdP87/2aa2ZBPqgBzkO5t9RRwfA5KlcS5TFL7OgMH/nlWuyrzIN8YzVbct7R6cIq8sno03PSlrxBdH4YsUQKnRpquZLlvub2GPkWGbTrYpu/3te+aVWHi2CMVvw4iTmQUofrhMCAwEAAaNgMF4wDwYDVR0TBAgwBgEB/wIBAjAdBgNVHQ4EFgQUfJoefd95VLzXzF7KmYZFeWV0KBkwHwYDVR0jBBgwFoAUfJoefd95VLzXzF7KmYZFeWV0KBkwCwYDVR0PBAQDAgEGMA0GCSqGSIb3DQEBBQUAA4IBAQCA9Fr7PSgZUSDX1PsSl0pl8lg1kncwavHXtlEaf5rNx3sDQq1VagCv8OEGwr1reHXb/kERU0o5u5o6xlk0Lywz47LWXH/deOtxWznag5DFMeI/I+/a6ystd17ew0PSyWtZgsrV7fqhZFvL8Q0aYuGc6KcYcPBfF5b47Ybbrh3gzz5dLu4WbZUrPP2X8wVaJGhNObb45Fi69eAmeFHFW11OCeVsR4t6Wi6JU+bMNlsmPPhyQwKC0ivN8NOj7BM+UtWDPQfcHUNlejMCAaPOt9ZgUTsJwiOKMv6YGWBik4XNNEbb1SMPedp3ACoCbYNYzgN3NeGjIJPCSqKkRhx1LB9N"

    invoke-static {v0}, Lcom/google/android/gms/cast/b/aa;->b(Ljava/lang/String;)Ljava/security/cert/X509Certificate;

    move-result-object v0

    .line 124
    sget-object v1, Lcom/google/android/gms/cast/b/aa;->a:Ljava/util/HashSet;

    new-instance v2, Ljava/security/cert/TrustAnchor;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Ljava/security/cert/TrustAnchor;-><init>(Ljava/security/cert/X509Certificate;[B)V

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 127
    const-string v1, "DeviceAuthChannel"

    const-string v2, "Error parsing built-in cert."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;[BLjava/lang/String;)V
    .locals 2

    .prologue
    .line 136
    sget-object v0, Lcom/google/android/gms/cast/b/aa;->b:Ljava/lang/String;

    const-string v1, "DeviceAuthChannel"

    invoke-direct {p0, v0, v1, p3}, Lcom/google/android/gms/cast/internal/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/b/aa;->d:I

    .line 137
    iput-object p2, p0, Lcom/google/android/gms/cast/b/aa;->g:[B

    .line 138
    iput-object p1, p0, Lcom/google/android/gms/cast/b/aa;->f:Ljava/lang/String;

    .line 139
    return-void
.end method

.method private static b(Ljava/lang/String;)Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/b/aa;->b([B)Ljava/security/cert/X509Certificate;

    move-result-object v0

    return-object v0
.end method

.method private static b([B)Ljava/security/cert/X509Certificate;
    .locals 2

    .prologue
    .line 167
    sget-object v0, Lcom/google/android/gms/cast/b/aa;->c:Ljava/security/cert/CertificateFactory;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method private b(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 153
    iput p1, p0, Lcom/google/android/gms/cast/b/aa;->d:I

    .line 154
    if-nez p1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/cast/b/aa;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Device authentication succeeded."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/cast/b/aa;->a(I)V

    .line 163
    return-void

    .line 156
    :cond_0
    if-ne p1, v5, :cond_1

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/cast/b/aa;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Device authentication failed: %s %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/gms/cast/b/aa;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/google/android/gms/cast/b/aa;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/aa;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Device authentication failed: %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/gms/cast/b/aa;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 146
    new-instance v0, Lcom/google/g/a/e;

    invoke-direct {v0}, Lcom/google/g/a/e;-><init>()V

    .line 147
    new-instance v1, Lcom/google/g/a/b;

    invoke-direct {v1}, Lcom/google/g/a/b;-><init>()V

    .line 148
    invoke-virtual {v0, v1}, Lcom/google/g/a/e;->a(Lcom/google/g/a/b;)Lcom/google/g/a/e;

    .line 149
    invoke-virtual {v0}, Lcom/google/g/a/e;->g()[B

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/b/aa;->f:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/cast/b/aa;->a([BLjava/lang/String;)V

    .line 150
    return-void
.end method

.method protected abstract a(I)V
.end method

.method public final a([B)V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 190
    :try_start_0
    new-instance v0, Lcom/google/g/a/e;

    invoke-direct {v0}, Lcom/google/g/a/e;-><init>()V

    array-length v1, p1

    invoke-virtual {v0, p1, v1}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/g/a/e;
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/cast/b/aa;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "Received a protobuf: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    iget-object v1, v0, Lcom/google/g/a/e;->a:Lcom/google/g/a/b;

    .line 199
    if-eqz v1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/cast/b/aa;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Received DeviceAuthMessage with challenge instead of response (ignored)."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    iget-object v1, p0, Lcom/google/android/gms/cast/b/aa;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "Received an unparseable protobuf (ignored): %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/protobuf/a/e;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 203
    :cond_0
    iget-object v1, v0, Lcom/google/g/a/e;->c:Lcom/google/g/a/c;

    .line 204
    if-eqz v1, :cond_1

    .line 205
    iget v0, v1, Lcom/google/g/a/c;->a:I

    iput v0, p0, Lcom/google/android/gms/cast/b/aa;->e:I

    .line 206
    invoke-direct {p0, v7}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto :goto_0

    .line 209
    :cond_1
    iget-object v2, v0, Lcom/google/g/a/e;->b:Lcom/google/g/a/d;

    .line 210
    if-nez v2, :cond_2

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/cast/b/aa;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Received DeviceAuthMessage with no response (ignored)."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 215
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 217
    :try_start_1
    iget-object v0, v2, Lcom/google/g/a/d;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/b/aa;->b([B)Ljava/security/cert/X509Certificate;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    iget-object v0, v2, Lcom/google/g/a/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/a/a;

    .line 219
    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/b/aa;->b([B)Ljava/security/cert/X509Certificate;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 222
    :catch_1
    move-exception v0

    invoke-direct {p0, v6}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto :goto_0

    .line 225
    :catch_2
    move-exception v0

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto :goto_0

    .line 231
    :cond_3
    :try_start_2
    sget-object v0, Lcom/google/android/gms/cast/b/aa;->c:Ljava/security/cert/CertificateFactory;

    invoke-virtual {v0, v1}, Ljava/security/cert/CertificateFactory;->generateCertPath(Ljava/util/List;)Ljava/security/cert/CertPath;

    move-result-object v0

    .line 232
    new-instance v1, Ljava/security/cert/PKIXParameters;

    sget-object v3, Lcom/google/android/gms/cast/b/aa;->a:Ljava/util/HashSet;

    invoke-direct {v1, v3}, Ljava/security/cert/PKIXParameters;-><init>(Ljava/util/Set;)V

    .line 233
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/security/cert/PKIXParameters;->setRevocationEnabled(Z)V

    .line 234
    const-string v3, "PKIX"

    invoke-static {v3}, Ljava/security/cert/CertPathValidator;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertPathValidator;

    move-result-object v3

    .line 235
    invoke-virtual {v3, v0, v1}, Ljava/security/cert/CertPathValidator;->validate(Ljava/security/cert/CertPath;Ljava/security/cert/CertPathParameters;)Ljava/security/cert/CertPathValidatorResult;

    move-result-object v0

    check-cast v0, Ljava/security/cert/PKIXCertPathValidatorResult;

    .line 237
    invoke-virtual {v0}, Ljava/security/cert/PKIXCertPathValidatorResult;->getPublicKey()Ljava/security/PublicKey;
    :try_end_2
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/security/cert/CertPathValidatorException; {:try_start_2 .. :try_end_2} :catch_6

    move-result-object v3

    .line 256
    iget v0, v2, Lcom/google/g/a/d;->d:I

    packed-switch v0, :pswitch_data_0

    .line 264
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto/16 :goto_0

    .line 239
    :catch_3
    move-exception v0

    invoke-direct {p0, v6}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto/16 :goto_0

    .line 243
    :catch_4
    move-exception v0

    invoke-direct {p0, v6}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto/16 :goto_0

    .line 247
    :catch_5
    move-exception v0

    invoke-direct {p0, v6}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto/16 :goto_0

    .line 250
    :catch_6
    move-exception v0

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto/16 :goto_0

    .line 258
    :pswitch_0
    const-string v0, "SHA1withRSAandMGF1"

    move-object v1, v0

    .line 270
    :goto_2
    :try_start_3
    invoke-static {v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 271
    invoke-virtual {v0, v3}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 272
    iget-object v3, p0, Lcom/google/android/gms/cast/b/aa;->g:[B

    invoke-virtual {v0, v3}, Ljava/security/Signature;->update([B)V

    .line 273
    iget-object v2, v2, Lcom/google/g/a/d;->a:Lcom/google/protobuf/a/a;

    invoke-virtual {v2}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 274
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/aa;->b(I)V
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/security/InvalidKeyException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/security/SignatureException; {:try_start_3 .. :try_end_3} :catch_9

    goto/16 :goto_0

    .line 277
    :catch_7
    move-exception v0

    .line 279
    const-string v2, "DeviceAuthChannel"

    const-string v3, "Could not use algorithm: %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 288
    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto/16 :goto_0

    .line 261
    :pswitch_1
    const-string v0, "SHA1withRSA"

    move-object v1, v0

    .line 262
    goto :goto_2

    .line 281
    :catch_8
    move-exception v0

    invoke-direct {p0, v6}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto/16 :goto_0

    .line 284
    :catch_9
    move-exception v0

    invoke-direct {p0, v8}, Lcom/google/android/gms/cast/b/aa;->b(I)V

    goto/16 :goto_0

    .line 256
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
