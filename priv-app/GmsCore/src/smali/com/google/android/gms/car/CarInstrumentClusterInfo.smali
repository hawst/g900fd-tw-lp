.class public Lcom/google/android/gms/car/CarInstrumentClusterInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/gms/car/et;

    invoke-direct {v0}, Lcom/google/android/gms/car/et;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->a:I

    .line 41
    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput p1, p0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->a:I

    .line 52
    iput p2, p0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->b:I

    .line 53
    iput p3, p0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->c:I

    .line 54
    iput p4, p0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->d:I

    .line 55
    iput p5, p0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->e:I

    .line 56
    iput p6, p0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->f:I

    .line 57
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/gms/car/CarInstrumentClusterInfo;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 66
    invoke-static {p0, p1}, Lcom/google/android/gms/car/et;->a(Lcom/google/android/gms/car/CarInstrumentClusterInfo;Landroid/os/Parcel;)V

    .line 67
    return-void
.end method
