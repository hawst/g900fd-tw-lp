.class public Lcom/google/android/gms/cast/b/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static E:Ljavax/net/ssl/SSLContext;

.field private static final a:I

.field private static final b:[Ljava/nio/ByteBuffer;


# instance fields
.field private final A:Z

.field private B:Ljava/net/Inet4Address;

.field private C:I

.field private D:Landroid/os/Handler;

.field private F:Ljavax/net/ssl/SSLEngine;

.field private G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

.field private H:Ljava/nio/ByteBuffer;

.field private I:Ljava/nio/ByteBuffer;

.field private J:Z

.field private final c:Lcom/google/android/gms/cast/b/t;

.field private final d:I

.field private e:Ljava/nio/channels/SocketChannel;

.field private f:Lcom/google/android/gms/cast/b/w;

.field private g:Lcom/google/protobuf/a/b;

.field private h:Lcom/google/android/gms/cast/b/x;

.field private i:Lcom/google/android/gms/cast/b/w;

.field private j:Lcom/google/protobuf/a/c;

.field private final k:Lcom/google/android/gms/cast/b/u;

.field private final l:I

.field private m:I

.field private n:Ljava/net/InetSocketAddress;

.field private o:J

.field private p:J

.field private q:J

.field private final r:J

.field private s:Z

.field private t:Z

.field private final u:Lcom/google/android/gms/cast/e/h;

.field private final v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:[B

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    sget-object v0, Lcom/google/android/gms/cast/a/b;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/b/o;->a:I

    .line 95
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/cast/b/o;->b:[Ljava/nio/ByteBuffer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/cast/b/t;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 201
    const/high16 v5, 0x20000

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/cast/b/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/cast/b/t;Landroid/os/Handler;Ljava/lang/String;IZ)V

    .line 202
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/cast/b/t;Landroid/os/Handler;Ljava/lang/String;IZ)V
    .locals 6

    .prologue
    const/16 v3, 0x12

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    if-nez p1, :cond_0

    .line 233
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_0
    if-nez p2, :cond_1

    .line 236
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_1
    const/16 v0, 0x400

    if-lt p5, v0, :cond_2

    const/high16 v0, 0x20000

    if-le p5, v0, :cond_3

    .line 239
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize must be between MIN_BUFFER_SIZE and MAX_BUFFER_SIZE"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_3
    const-class v1, Lcom/google/android/gms/cast/b/o;

    monitor-enter v1

    .line 243
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/b/o;->E:Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_5

    .line 244
    :try_start_1
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/b/o;->E:Ljavax/net/ssl/SSLContext;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const/4 v0, 0x0

    :try_start_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v3, :cond_4

    const-string v0, "CastSocket"

    const-string v2, "%d >= %d. Adding new CastClientAuthKeyManager."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/16 v5, 0x12

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    new-array v0, v0, [Ljavax/net/ssl/KeyManager;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/gms/cast/b/d;->a(Landroid/content/Context;)Lcom/google/android/gms/cast/b/d;

    move-result-object v3

    aput-object v3, v0, v2

    :cond_4
    sget-object v2, Lcom/google/android/gms/cast/b/o;->E:Ljavax/net/ssl/SSLContext;

    sget-object v3, Lcom/google/android/gms/cast/b/a;->a:[Lcom/google/android/gms/cast/b/a;

    new-instance v4, Ljava/security/SecureRandom;

    invoke-direct {v4}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v2, v0, v3, v4}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    :try_end_2
    .catch Ljava/security/KeyManagementException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    :cond_5
    :goto_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 248
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastSocket"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v0, p4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;)V

    .line 250
    iput-object p4, p0, Lcom/google/android/gms/cast/b/o;->v:Ljava/lang/String;

    .line 251
    iput-object p2, p0, Lcom/google/android/gms/cast/b/o;->c:Lcom/google/android/gms/cast/b/t;

    .line 252
    iput-object p3, p0, Lcom/google/android/gms/cast/b/o;->D:Landroid/os/Handler;

    .line 253
    iput p5, p0, Lcom/google/android/gms/cast/b/o;->d:I

    .line 254
    iput-boolean p6, p0, Lcom/google/android/gms/cast/b/o;->A:Z

    .line 255
    iget v0, p0, Lcom/google/android/gms/cast/b/o;->d:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lcom/google/android/gms/cast/b/o;->l:I

    .line 256
    invoke-static {p1}, Lcom/google/android/gms/cast/b/u;->a(Landroid/content/Context;)Lcom/google/android/gms/cast/b/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->k:Lcom/google/android/gms/cast/b/u;

    .line 257
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/google/android/gms/cast/b/o;->r:J

    .line 258
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->n()V

    .line 259
    return-void

    .line 244
    :catch_0
    move-exception v0

    :try_start_4
    const-string v2, "CastSocket"

    const-string v3, "Failed SSLContext.init."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/b/o;)Lcom/google/android/gms/cast/b/t;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->c:Lcom/google/android/gms/cast/b/t;

    return-object v0
.end method

.method private a(Ljavax/net/ssl/SSLEngineResult;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 824
    invoke-virtual {p1}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    .line 825
    sget-object v0, Lcom/google/android/gms/cast/b/s;->a:[I

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 875
    :goto_0
    return-void

    .line 827
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->y:[B

    if-eqz v0, :cond_3

    .line 831
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getPeerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    .line 832
    array-length v1, v0

    if-lez v1, :cond_2

    .line 833
    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 834
    const-string v1, "SHA-256"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 835
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/PublicKey;->getEncoded()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 836
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->y:[B

    invoke-static {v0, v1}, Ljava/security/MessageDigest;->isEqual([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v1, "TLS peer PK hash from server does not match the hash from the TLS channel"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 839
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    .line 854
    :catch_0
    move-exception v0

    .line 849
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "SSL Unverified exception"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 850
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 841
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v1, "hashes match valid connection"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    .line 863
    :cond_1
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->J:Z

    goto :goto_0

    .line 845
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v1, "No peer cert available in SSL handshake"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 846
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
    :try_end_2
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1

    .line 851
    :catch_1
    move-exception v0

    .line 852
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "No algorithm available."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 853
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 855
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->x:Z

    if-eqz v0, :cond_1

    .line 858
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v1, "No TLS peer hash for this connection"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 859
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 871
    :goto_1
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getDelegatedTask()Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 872
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 874
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    goto/16 :goto_0

    .line 825
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private declared-synchronized b(Ljava/net/Inet4Address;I)V
    .locals 5

    .prologue
    .line 309
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->k:Lcom/google/android/gms/cast/b/u;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/u;->a()V

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Connecting to %s:%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 313
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->n()V

    .line 315
    iput p2, p0, Lcom/google/android/gms/cast/b/o;->C:I

    .line 316
    iput-object p1, p0, Lcom/google/android/gms/cast/b/o;->B:Ljava/net/Inet4Address;

    .line 318
    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lcom/google/android/gms/cast/b/o;->p:J

    .line 319
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/cast/b/o;->m:I

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->k:Lcom/google/android/gms/cast/b/u;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/cast/b/u;->a(Lcom/google/android/gms/cast/b/o;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    monitor-exit p0

    return-void

    .line 309
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/w;->c()V

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/w;->c()V

    .line 273
    :cond_1
    iput v1, p0, Lcom/google/android/gms/cast/b/o;->m:I

    .line 274
    iput-object v2, p0, Lcom/google/android/gms/cast/b/o;->g:Lcom/google/protobuf/a/b;

    .line 275
    iput-object v2, p0, Lcom/google/android/gms/cast/b/o;->h:Lcom/google/android/gms/cast/b/x;

    .line 276
    iput-object v2, p0, Lcom/google/android/gms/cast/b/o;->j:Lcom/google/protobuf/a/c;

    .line 277
    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/o;->J:Z

    .line 278
    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/o;->s:Z

    .line 279
    iput-object v2, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    .line 280
    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/o;->t:Z

    .line 281
    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/o;->x:Z

    .line 282
    iput-object v2, p0, Lcom/google/android/gms/cast/b/o;->B:Ljava/net/Inet4Address;

    .line 283
    iput v1, p0, Lcom/google/android/gms/cast/b/o;->C:I

    .line 284
    return-void
.end method

.method private o()V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v8, -0x1

    const/4 v1, 0x0

    .line 494
    .line 497
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    iget-boolean v2, v2, Lcom/google/android/gms/cast/b/w;->e:Z

    if-nez v2, :cond_7

    .line 498
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    iget v3, v2, Lcom/google/android/gms/cast/b/w;->b:I

    iput v3, v2, Lcom/google/android/gms/cast/b/w;->d:I

    .line 501
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/w;->e()I

    move-result v3

    const/4 v4, 0x4

    if-ge v3, v4, :cond_3

    const/4 v2, 0x0

    .line 502
    :goto_1
    if-nez v2, :cond_4

    .line 522
    :cond_0
    :goto_2
    if-eqz v0, :cond_6

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    iget v2, v0, Lcom/google/android/gms/cast/b/w;->d:I

    if-eq v2, v8, :cond_2

    iget v2, v0, Lcom/google/android/gms/cast/b/w;->b:I

    iget v3, v0, Lcom/google/android/gms/cast/b/w;->d:I

    if-eq v2, v3, :cond_1

    iget v2, v0, Lcom/google/android/gms/cast/b/w;->d:I

    iput v2, v0, Lcom/google/android/gms/cast/b/w;->b:I

    iput-boolean v1, v0, Lcom/google/android/gms/cast/b/w;->e:Z

    :cond_1
    iput v8, v0, Lcom/google/android/gms/cast/b/w;->d:I

    .line 529
    :cond_2
    :goto_3
    return-void

    .line 501
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/w;->g()B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-long v4, v3

    const/16 v3, 0x18

    shl-long/2addr v4, v3

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/w;->g()B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-long v6, v3

    const/16 v3, 0x10

    shl-long/2addr v6, v3

    or-long/2addr v4, v6

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/w;->g()B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-long v6, v3

    const/16 v3, 0x8

    shl-long/2addr v6, v3

    or-long/2addr v4, v6

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/w;->g()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    or-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_1

    .line 506
    :cond_4
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget v3, p0, Lcom/google/android/gms/cast/b/o;->l:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    .line 507
    new-instance v3, Ljava/io/IOException;

    const-string v4, "invalid message size (%d) received."

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v2, v0, v1

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 512
    :cond_5
    iget-object v3, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/b/w;->e()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    .line 517
    iget-object v3, p0, Lcom/google/android/gms/cast/b/o;->h:Lcom/google/android/gms/cast/b/x;

    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/google/android/gms/cast/b/x;->a(I)V

    .line 518
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->g:Lcom/google/protobuf/a/b;

    new-instance v3, Lcom/google/g/a/g;

    invoke-direct {v3}, Lcom/google/g/a/g;-><init>()V

    invoke-virtual {v3, v2}, Lcom/google/g/a/g;->b(Lcom/google/protobuf/a/b;)Lcom/google/g/a/g;

    move-result-object v2

    .line 519
    iget-object v3, p0, Lcom/google/android/gms/cast/b/o;->c:Lcom/google/android/gms/cast/b/t;

    invoke-interface {v3, v2}, Lcom/google/android/gms/cast/b/t;->a(Lcom/google/g/a/g;)V

    goto/16 :goto_0

    .line 527
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    iput v8, v0, Lcom/google/android/gms/cast/b/w;->d:I

    iget-boolean v2, v0, Lcom/google/android/gms/cast/b/w;->e:Z

    if-eqz v2, :cond_2

    iput v1, v0, Lcom/google/android/gms/cast/b/w;->c:I

    iput v1, v0, Lcom/google/android/gms/cast/b/w;->b:I

    goto :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_2
.end method

.method private p()I
    .locals 3

    .prologue
    .line 664
    const/4 v0, 0x0

    .line 666
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/cast/b/o;->s:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    sget-object v2, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/w;->f()Z

    move-result v1

    if-nez v1, :cond_1

    .line 669
    :cond_0
    const/4 v0, 0x1

    .line 671
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/gms/cast/b/o;->s:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    sget-object v2, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->I:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    if-gtz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    iget-boolean v1, v1, Lcom/google/android/gms/cast/b/w;->e:Z

    if-nez v1, :cond_3

    .line 675
    :cond_2
    or-int/lit8 v0, v0, 0x4

    .line 677
    :cond_3
    return v0
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 884
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isOutboundDone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()Z
    .locals 1

    .prologue
    .line 971
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isOutboundDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->isInboundDone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1052
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->I:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1053
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->I:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 1054
    if-gez v0, :cond_0

    .line 1055
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v1, "writeToChannel: throwing ClosedChannelException"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1056
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 1058
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "writeToChannel: count %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1059
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->I:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    .line 1060
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->D:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/b/p;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast/b/p;-><init>(Lcom/google/android/gms/cast/b/o;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1074
    return-void
.end method

.method private u()V
    .locals 3

    .prologue
    .line 1095
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->z:Z

    .line 1098
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "join\r\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/cast/internal/j;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1102
    return-void

    .line 1101
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/cast/b/n;

    const-string v1, "Unable to connect to the relay server"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/b/n;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method final declared-synchronized a(Ljava/nio/channels/SelectionKey;J)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 598
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v3, "updateSelectionKey when state=%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/gms/cast/b/o;->m:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 599
    iget-boolean v2, p0, Lcom/google/android/gms/cast/b/o;->t:Z

    if-eqz v2, :cond_1

    .line 600
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Socket is no longer connected"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 601
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/o;->t:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 660
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 607
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/google/android/gms/cast/b/o;->m:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 609
    :pswitch_0
    iget-wide v2, p0, Lcom/google/android/gms/cast/b/o;->o:J

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/google/android/gms/cast/b/o;->p:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 611
    const/4 v0, 0x3

    goto :goto_0

    .line 612
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    move-result v1

    if-nez v1, :cond_4

    .line 613
    const/16 v0, 0x8

    .line 659
    :cond_3
    :goto_1
    invoke-virtual {p1, v0}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    .line 660
    const/4 v0, -0x1

    goto :goto_0

    .line 614
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    if-eqz v1, :cond_3

    .line 615
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->p()I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 620
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    if-eqz v2, :cond_5

    .line 621
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->p()I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 624
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/w;->f()Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 628
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    iget-boolean v1, v1, Lcom/google/android/gms/cast/b/w;->e:Z

    if-nez v1, :cond_3

    .line 629
    or-int/lit8 v0, v0, 0x4

    goto :goto_1

    .line 635
    :pswitch_2
    iget-wide v2, p0, Lcom/google/android/gms/cast/b/o;->q:J

    sub-long v2, p2, v2

    iget-wide v4, p0, Lcom/google/android/gms/cast/b/o;->r:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 639
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    if-eqz v1, :cond_7

    .line 640
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->p()I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 644
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    iget-boolean v1, v1, Lcom/google/android/gms/cast/b/w;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    .line 645
    const/4 v0, 0x4

    goto :goto_1

    .line 598
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 607
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method final declared-synchronized a(I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 552
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v3, "shutdown with reason=%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 555
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    .line 557
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 559
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 561
    :goto_0
    const/4 v2, 0x0

    :try_start_2
    iput-object v2, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    .line 564
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    .line 565
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    .line 566
    iget v2, p0, Lcom/google/android/gms/cast/b/o;->m:I

    if-ne v2, v0, :cond_1

    .line 568
    :goto_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/gms/cast/b/o;->m:I

    .line 569
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/gms/cast/b/o;->q:J

    iput-wide v2, p0, Lcom/google/android/gms/cast/b/o;->o:J

    .line 570
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/o;->t:Z

    .line 572
    if-eqz v0, :cond_2

    .line 573
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->D:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/b/q;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast/b/q;-><init>(Lcom/google/android/gms/cast/b/o;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 577
    :goto_2
    monitor-exit p0

    return-void

    :cond_1
    move v0, v1

    .line 566
    goto :goto_1

    .line 575
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->D:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/b/r;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast/b/r;-><init>(Lcom/google/android/gms/cast/b/o;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 552
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/g/a/g;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 472
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/cast/b/o;->m:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "not connected; state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/cast/b/o;->m:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 474
    :cond_0
    if-nez p1, :cond_1

    .line 475
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "message cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 478
    :cond_1
    invoke-virtual {p1}, Lcom/google/g/a/g;->b()I

    move-result v0

    .line 479
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/w;->d()I

    move-result v1

    add-int/lit8 v2, v0, 0x4

    if-ge v1, v2, :cond_2

    .line 480
    new-instance v0, Lcom/google/android/gms/cast/b/c;

    invoke-direct {v0}, Lcom/google/android/gms/cast/b/c;-><init>()V

    throw v0

    .line 482
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    int-to-long v2, v0

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/w;->d()I

    move-result v0

    const/4 v4, 0x4

    if-lt v0, v4, :cond_5

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_3

    const-wide v4, 0xffffffffL

    cmp-long v0, v2, v4

    if-lez v0, :cond_4

    :cond_3
    new-instance v0, Lcom/google/android/gms/cast/b/ad;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid uint32 value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/b/ad;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/16 v0, 0x18

    shr-long v4, v2, v0

    and-long/2addr v4, v6

    long-to-int v0, v4

    int-to-byte v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/b/w;->a(B)V

    const/16 v0, 0x10

    shr-long v4, v2, v0

    and-long/2addr v4, v6

    long-to-int v0, v4

    int-to-byte v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/b/w;->a(B)V

    const/16 v0, 0x8

    shr-long v4, v2, v0

    and-long/2addr v4, v6

    long-to-int v0, v4

    int-to-byte v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/b/w;->a(B)V

    and-long/2addr v2, v6

    long-to-int v0, v2

    int-to-byte v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/b/w;->a(B)V

    .line 483
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->j:Lcom/google/protobuf/a/c;

    invoke-virtual {p1, v0}, Lcom/google/g/a/g;->a(Lcom/google/protobuf/a/c;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->j:Lcom/google/protobuf/a/c;

    invoke-virtual {v0}, Lcom/google/protobuf/a/c;->a()V

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->k:Lcom/google/android/gms/cast/b/u;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/u;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 486
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 437
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/cast/b/o;->w:Ljava/lang/String;

    .line 438
    iput-object p2, p0, Lcom/google/android/gms/cast/b/o;->y:[B

    .line 439
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->x:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 440
    monitor-exit p0

    return-void

    .line 437
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/net/Inet4Address;I)V
    .locals 0

    .prologue
    .line 295
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/cast/b/o;->b(Ljava/net/Inet4Address;I)V

    .line 296
    return-void
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->A:Z

    return v0
.end method

.method final declared-synchronized b()Ljava/nio/channels/SocketChannel;
    .locals 4

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v1, "startConnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 328
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/b/o;->o:J

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->B:Ljava/net/Inet4Address;

    invoke-virtual {v0}, Ljava/net/Inet4Address;->getHostName()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    .line 334
    :try_start_1
    const-string v0, ""

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->B:Ljava/net/Inet4Address;

    invoke-virtual {v1}, Ljava/net/Inet4Address;->getAddress()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/net/InetAddress;->getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;

    move-result-object v0

    check-cast v0, Ljava/net/Inet4Address;

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->B:Ljava/net/Inet4Address;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341
    :cond_0
    :goto_0
    :try_start_2
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->B:Ljava/net/Inet4Address;

    iget v2, p0, Lcom/google/android/gms/cast/b/o;->C:I

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->n:Ljava/net/InetSocketAddress;

    .line 343
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/net/Socket;->setSoLinger(ZI)V

    .line 348
    new-instance v0, Lcom/google/android/gms/cast/b/w;

    iget v1, p0, Lcom/google/android/gms/cast/b/o;->d:I

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/b/w;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    .line 349
    new-instance v0, Lcom/google/android/gms/cast/b/x;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/b/x;-><init>(Lcom/google/android/gms/cast/b/w;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->h:Lcom/google/android/gms/cast/b/x;

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->h:Lcom/google/android/gms/cast/b/x;

    invoke-static {v0}, Lcom/google/protobuf/a/b;->a(Ljava/io/InputStream;)Lcom/google/protobuf/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->g:Lcom/google/protobuf/a/b;

    .line 351
    new-instance v0, Lcom/google/android/gms/cast/b/w;

    iget v1, p0, Lcom/google/android/gms/cast/b/o;->d:I

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/b/w;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    .line 352
    new-instance v0, Lcom/google/android/gms/cast/b/y;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/b/y;-><init>(Lcom/google/android/gms/cast/b/w;)V

    invoke-static {v0}, Lcom/google/protobuf/a/c;->a(Ljava/io/OutputStream;)Lcom/google/protobuf/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->j:Lcom/google/protobuf/a/c;

    .line 355
    sget-object v0, Lcom/google/android/gms/cast/b/o;->E:Ljavax/net/ssl/SSLContext;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->n:Ljava/net/InetSocketAddress;

    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->n:Ljava/net/InetSocketAddress;

    invoke-virtual {v2}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljavax/net/ssl/SSLContext;->createSSLEngine(Ljava/lang/String;I)Ljavax/net/ssl/SSLEngine;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljavax/net/ssl/SSLEngine;->setUseClientMode(Z)V

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    .line 359
    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getPacketBufferSize()I

    move-result v0

    .line 360
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    .line 361
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->I:Ljava/nio/ByteBuffer;

    .line 362
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    .line 363
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->J:Z

    .line 364
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->s:Z

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->n:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->w:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 372
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->u()V

    .line 382
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v1, "startConnect done"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 335
    :catch_0
    move-exception v0

    .line 337
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "call to InetAddress.getByAddress() failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 373
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    if-eqz v0, :cond_3

    .line 374
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->beginHandshake()V

    .line 375
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    goto :goto_1

    .line 377
    :cond_3
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/cast/b/o;->m:I

    .line 378
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->t()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized c()V
    .locals 5

    .prologue
    .line 390
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v1, "disconnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/cast/b/o;->m:I

    .line 392
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/b/o;->q:J

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->k:Lcom/google/android/gms/cast/b/u;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/u;->b()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 402
    :goto_0
    monitor-exit p0

    return-void

    .line 398
    :catch_0
    move-exception v0

    .line 399
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Unable to wake up the muxer: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 2

    .prologue
    .line 408
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/cast/b/o;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 415
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/gms/cast/b/o;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 2

    .prologue
    .line 422
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/cast/b/o;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 426
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/cast/b/o;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()I
    .locals 1

    .prologue
    .line 433
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/cast/b/o;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final i()[B
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 448
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->J:Z

    if-eqz v0, :cond_0

    .line 450
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getPeerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    .line 451
    array-length v1, v0

    if-lez v1, :cond_0

    .line 452
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/security/cert/Certificate;->getEncoded()[B
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 462
    :goto_0
    return-object v0

    .line 454
    :catch_0
    move-exception v0

    .line 455
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error getting peer cert: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLPeerUnverifiedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 462
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 457
    :catch_1
    move-exception v0

    .line 458
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Error getting peer cert: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/security/cert/CertificateEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method final declared-synchronized j()I
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 687
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v3, "onConnectable"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 688
    iget v2, p0, Lcom/google/android/gms/cast/b/o;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 713
    :goto_0
    monitor-exit p0

    return v0

    .line 692
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    .line 695
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->w:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 696
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->u()V

    .line 704
    :goto_1
    const/4 v0, -0x1

    goto :goto_0

    .line 697
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    if-eqz v0, :cond_2

    .line 698
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->beginHandshake()V

    .line 699
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;
    :try_end_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/cast/b/n; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 705
    :catch_0
    move-exception v0

    .line 706
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception in onConnectable"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 707
    const/4 v0, 0x4

    goto :goto_0

    .line 701
    :cond_2
    const/4 v0, 0x2

    :try_start_3
    iput v0, p0, Lcom/google/android/gms/cast/b/o;->m:I

    .line 702
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->t()V
    :try_end_3
    .catch Ljavax/net/ssl/SSLException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/android/gms/cast/b/n; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 708
    :catch_1
    move-exception v0

    .line 709
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception in onConnectable"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 710
    const/4 v0, 0x5

    goto :goto_0

    .line 711
    :catch_2
    move-exception v0

    .line 712
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v3, "exception in onConnectable"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v1

    .line 713
    goto :goto_0

    .line 687
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized k()I
    .locals 14

    .prologue
    const/4 v3, 0x4

    const/4 v13, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 724
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v5, "onReadable when state is %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/google/android/gms/cast/b/o;->m:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, v5, v6}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 726
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    if-eqz v0, :cond_11

    .line 727
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->q()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/cast/b/n; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742
    :catch_0
    move-exception v0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v3, "ClosedChannelException when state was %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/gms/cast/b/o;->m:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 743
    iget v0, p0, Lcom/google/android/gms/cast/b/o;->m:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v0, v13, :cond_1

    .line 745
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    if-eqz v0, :cond_0

    .line 746
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->closeInbound()V
    :try_end_3
    .catch Ljavax/net/ssl/SSLException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_0
    move v1, v2

    .line 762
    :cond_1
    :goto_0
    monitor-exit p0

    return v1

    .line 727
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v5}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    if-gez v0, :cond_3

    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v6, "readFromChannel: count %d; throwing ClosedChannelException"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
    :try_end_4
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/android/gms/cast/b/n; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 754
    :catch_1
    move-exception v0

    .line 755
    :try_start_5
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "CastRelayException encountered. Tearing down the socket."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 756
    const/4 v1, 0x5

    goto :goto_0

    .line 727
    :cond_3
    :try_start_6
    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v6, "readFromChannel: count %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    if-lez v0, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->z:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    new-array v7, v0, [B

    move v0, v2

    move v5, v2

    :goto_2
    if-nez v0, :cond_7

    iget-object v6, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v6

    if-eqz v6, :cond_7

    sget v6, Lcom/google/android/gms/cast/b/o;->a:I

    if-ge v5, v6, :cond_7

    add-int/lit8 v6, v5, 0x1

    iget-object v8, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->get()B

    move-result v8

    aput-byte v8, v7, v5

    invoke-static {v7}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v5

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v7}, Ljava/lang/String;-><init>([B)V

    iget-object v9, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v10, "handleRead: CastNearby join relay response: %s, raw bytes=%s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v8, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    invoke-virtual {v9, v10, v11}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v5, "OK 1\n"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "OK 2\n"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    move v5, v6

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v6, "CastNearby remaining bytes: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/gms/cast/b/o;->z:Z

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/gms/cast/b/o;->w:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->beginHandshake()V

    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 728
    :cond_9
    :goto_3
    iget v0, p0, Lcom/google/android/gms/cast/b/o;->m:I

    if-ne v0, v1, :cond_a

    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/o;->J:Z

    if-eqz v0, :cond_a

    .line 729
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/cast/b/o;->m:I

    .line 730
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->t()V

    .line 735
    :cond_a
    :goto_4
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->o()V

    .line 736
    const/4 v1, -0x1

    goto/16 :goto_0

    .line 727
    :cond_b
    new-instance v0, Lcom/google/android/gms/cast/b/n;

    const-string v5, "Unable to create a connection to the server."

    invoke-direct {v0, v5}, Lcom/google/android/gms/cast/b/n;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/google/android/gms/cast/b/n; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 757
    :catch_2
    move-exception v0

    .line 758
    :try_start_7
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "SSLException encountered. Tearing down the socket."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v1, v3

    .line 759
    goto/16 :goto_0

    .line 727
    :cond_c
    if-lez v6, :cond_d

    :try_start_8
    invoke-virtual {v5, v6}, Lcom/google/android/gms/cast/b/w;->b(I)V

    :cond_d
    sget-object v5, Lcom/google/android/gms/cast/b/s;->b:[I

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/w;->b()[Ljava/nio/ByteBuffer;

    move-result-object v0

    if-nez v0, :cond_f

    new-instance v0, Ljava/io/IOException;

    const-string v5, "Trying to read into a full buffer."

    invoke-direct {v0, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_8
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lcom/google/android/gms/cast/b/n; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 760
    :catch_3
    move-exception v0

    .line 761
    :try_start_9
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "IOException encountered. Tearing down the socket."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v1, v4

    .line 762
    goto/16 :goto_0

    .line 727
    :cond_f
    :try_start_a
    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    iget-object v6, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6, v0}, Ljavax/net/ssl/SSLEngine;->unwrap(Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v6, "handleRead called SSLEngine.unwrap: status=%s handshakeStatus=%s bytesConsumed=%d bytesProduced=%d"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->bytesConsumed()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->bytesProduced()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/b/o;->a(Ljavax/net/ssl/SSLEngineResult;)V

    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->bytesProduced()I

    move-result v6

    if-ltz v6, :cond_10

    iget-object v7, v5, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v7, v7

    if-le v6, v7, :cond_c

    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_a
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_a .. :try_end_a} :catch_0
    .catch Lcom/google/android/gms/cast/b/n; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 724
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 727
    :pswitch_0
    :try_start_b
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->H:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    goto/16 :goto_3

    :pswitch_1
    new-instance v0, Ljava/io/IOException;

    const-string v5, "unexpected buffer overflow condition."

    invoke-direct {v0, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 733
    :cond_11
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->f:Lcom/google/android/gms/cast/b/w;

    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/b/w;->a(Ljava/nio/channels/SocketChannel;)I
    :try_end_b
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lcom/google/android/gms/cast/b/n; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_4

    .line 749
    :catch_4
    move-exception v0

    .line 750
    :try_start_c
    iget-object v2, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Error calling SSLEngine.closeInbound(): %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 727
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method final declared-synchronized l()I
    .locals 11

    .prologue
    const/4 v2, 0x4

    const/4 v10, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 773
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v5, "onWritable when state is %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/google/android/gms/cast/b/o;->m:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 775
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    if-eqz v4, :cond_c

    .line 776
    iget v4, p0, Lcom/google/android/gms/cast/b/o;->m:I

    if-ne v4, v1, :cond_0

    iget-boolean v4, p0, Lcom/google/android/gms/cast/b/o;->J:Z

    if-eqz v4, :cond_0

    .line 777
    const/4 v4, 0x2

    iput v4, p0, Lcom/google/android/gms/cast/b/o;->m:I

    .line 778
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->t()V

    .line 780
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->r()Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 798
    :catch_0
    move-exception v0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "ClosedChannelException when state was %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/gms/cast/b/o;->m:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 805
    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    .line 780
    :cond_2
    :try_start_3
    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->I:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    if-lez v4, :cond_5

    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->s()V

    .line 784
    :goto_1
    iget v4, p0, Lcom/google/android/gms/cast/b/o;->m:I

    if-ne v4, v1, :cond_3

    iget-boolean v4, p0, Lcom/google/android/gms/cast/b/o;->J:Z

    if-eqz v4, :cond_3

    .line 785
    const/4 v4, 0x2

    iput v4, p0, Lcom/google/android/gms/cast/b/o;->m:I

    .line 786
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->t()V

    .line 791
    :cond_3
    :goto_2
    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    iget-boolean v4, v4, Lcom/google/android/gms/cast/b/w;->e:Z

    if-eqz v4, :cond_4

    .line 792
    iget v1, p0, Lcom/google/android/gms/cast/b/o;->m:I

    if-eq v1, v10, :cond_1

    .line 796
    :cond_4
    const/4 v0, -0x1

    goto :goto_0

    .line 780
    :cond_5
    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/b/w;->a()[Ljava/nio/ByteBuffer;

    move-result-object v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "There is nothing to write. disconnecting? "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v6, p0, Lcom/google/android/gms/cast/b/o;->s:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v4, p0, Lcom/google/android/gms/cast/b/o;->s:Z

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngine;->closeOutbound()V
    :try_end_3
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 800
    :catch_1
    move-exception v0

    .line 801
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v3, "SSLException encountered. Tearing down the socket."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v2

    .line 802
    goto :goto_0

    .line 780
    :cond_6
    :try_start_5
    sget-object v4, Lcom/google/android/gms/cast/b/o;->b:[Ljava/nio/ByteBuffer;

    :cond_7
    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->F:Ljavax/net/ssl/SSLEngine;

    iget-object v6, p0, Lcom/google/android/gms/cast/b/o;->I:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v4, v6}, Ljavax/net/ssl/SSLEngine;->wrap([Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v6, "handleWrite called SSLEngine.wrap: status=%s handshakeStatus=%s bytesConsumed=%d bytesProduced=%d"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->bytesConsumed()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->bytesProduced()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v4}, Lcom/google/android/gms/cast/b/o;->a(Ljavax/net/ssl/SSLEngineResult;)V

    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->bytesConsumed()I

    move-result v6

    if-ltz v6, :cond_8

    iget-object v7, v5, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v7, v7

    if-le v6, v7, :cond_9

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_5
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 803
    :catch_2
    move-exception v0

    .line 804
    :try_start_6
    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->u:Lcom/google/android/gms/cast/e/h;

    const-string v2, "IOException encountered. Tearing down the socket."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v0, v3

    .line 805
    goto/16 :goto_0

    .line 780
    :cond_9
    if-lez v6, :cond_a

    :try_start_7
    invoke-virtual {v5, v6}, Lcom/google/android/gms/cast/b/w;->a(I)V

    :cond_a
    sget-object v5, Lcom/google/android/gms/cast/b/s;->b:[I

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v4

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->I:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    if-lez v4, :cond_b

    invoke-direct {p0}, Lcom/google/android/gms/cast/b/o;->s()V

    :cond_b
    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    iget-boolean v4, v4, Lcom/google/android/gms/cast/b/w;->e:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->G:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v4, v5, :cond_5

    goto/16 :goto_1

    :pswitch_0
    new-instance v0, Ljava/io/IOException;

    const-string v4, "unexpected buffer underflow condition."

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 773
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 780
    :pswitch_1
    :try_start_8
    new-instance v0, Ljava/io/IOException;

    const-string v4, "unexpected buffer overflow condition."

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 789
    :cond_c
    iget-object v4, p0, Lcom/google/android/gms/cast/b/o;->i:Lcom/google/android/gms/cast/b/w;

    iget-object v5, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/b/w;->a()[Ljava/nio/ByteBuffer;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v5, v6}, Ljava/nio/channels/SocketChannel;->write([Ljava/nio/ByteBuffer;)J

    move-result-wide v6

    long-to-int v5, v6

    if-lez v5, :cond_d

    invoke-virtual {v4, v5}, Lcom/google/android/gms/cast/b/w;->a(I)V

    goto/16 :goto_2

    :cond_d
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
    :try_end_8
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 780
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method final declared-synchronized m()Ljava/nio/channels/SocketChannel;
    .locals 1

    .prologue
    .line 813
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/b/o;->e:Ljava/nio/channels/SocketChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1064
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CastSocket:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/cast/b/o;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
