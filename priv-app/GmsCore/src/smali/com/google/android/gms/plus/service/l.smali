.class final Lcom/google/android/gms/plus/service/l;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/service/PlusService;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/service/PlusService;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/gms/plus/service/l;->a:Lcom/google/android/gms/plus/service/PlusService;

    .line 176
    invoke-direct {p0, p3}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 177
    iput-object p2, p0, Lcom/google/android/gms/plus/service/l;->d:Ljava/lang/String;

    .line 178
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/common/internal/GetServiceRequest;)V
    .locals 21

    .prologue
    .line 182
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Plus service is expected in the request."

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 188
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a()I

    move-result v1

    if-gtz v1, :cond_1

    .line 189
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "clientVersion too old"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 182
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 192
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->f()Landroid/os/Bundle;

    move-result-object v2

    .line 193
    const/4 v6, 0x0

    .line 194
    if-eqz v2, :cond_2

    .line 195
    const-class v1, Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 196
    const-string v1, "request_visible_actions"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 199
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->c()Ljava/lang/String;

    move-result-object v9

    .line 201
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 202
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "invalid callingPackage"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 204
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/service/l;->a:Lcom/google/android/gms/plus/service/PlusService;

    invoke-static {v1, v9}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 206
    const-string v1, "auth_package"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 207
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/service/l;->a:Lcom/google/android/gms/plus/service/PlusService;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/PlusService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 209
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 220
    :cond_4
    const/4 v1, 0x0

    .line 221
    if-eqz v2, :cond_5

    .line 222
    const-string v1, "skip_oob"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 224
    :cond_5
    if-eqz v1, :cond_7

    .line 227
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 231
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/service/l;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/service/l;->a:Lcom/google/android/gms/plus/service/PlusService;

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/gms/plus/service/i;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/i;-><init>()V

    invoke-static/range {v1 .. v6}, Lcom/google/android/gms/plus/service/PlusService;->a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/i;[Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    .line 234
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    move-object/from16 v0, p1

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_1
    return-void

    .line 210
    :cond_6
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 215
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "invalid authPackage"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 246
    :cond_7
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v14

    .line 248
    const/4 v1, 0x0

    .line 252
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    if-ne v3, v4, :cond_8

    .line 253
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/service/l;->a:Lcom/google/android/gms/plus/service/PlusService;

    invoke-virtual {v1, v10}, Lcom/google/android/gms/plus/service/PlusService;->a(Ljava/lang/String;)I

    move-result v14

    .line 254
    const-string v1, "required_features"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 258
    :cond_8
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->e()[Lcom/google/android/gms/common/api/Scope;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/util/as;->a([Lcom/google/android/gms/common/api/Scope;)[Ljava/lang/String;

    move-result-object v12

    .line 259
    if-nez v1, :cond_c

    .line 260
    invoke-static {v12}, Lcom/google/android/gms/plus/internal/cq;->a([Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 261
    sget-object v15, Lcom/google/android/gms/common/internal/aj;->f:[Ljava/lang/String;

    .line 268
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/service/l;->a:Lcom/google/android/gms/plus/service/PlusService;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/PlusService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1, v9}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 270
    const-string v1, "application_name"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 275
    :goto_3
    invoke-static {v2}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v20

    .line 277
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->d()Landroid/accounts/Account;

    move-result-object v1

    .line 278
    new-instance v7, Lcom/google/android/gms/plus/service/a/as;

    if-nez v1, :cond_b

    const/4 v8, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/plus/service/l;->d:Ljava/lang/String;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/gms/plus/service/f;->a:Lcom/google/android/gms/plus/service/f;

    sget-object v19, Lcom/google/android/gms/plus/service/a/b;->a:Lcom/google/android/gms/plus/service/a/b;

    move-object v13, v6

    move-object/from16 v16, p1

    invoke-direct/range {v7 .. v20}, Lcom/google/android/gms/plus/service/a/as;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Lcom/google/android/gms/plus/service/f;Lcom/google/android/gms/plus/service/a/b;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    .line 283
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/service/l;->a:Lcom/google/android/gms/plus/service/PlusService;

    invoke-static {v1, v7}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    goto :goto_1

    .line 263
    :cond_9
    const/4 v1, 0x0

    new-array v15, v1, [Ljava/lang/String;

    goto :goto_2

    .line 272
    :cond_a
    sget-object v11, Lcom/google/android/gms/common/analytics/a;->c:Ljava/lang/String;

    goto :goto_3

    .line 278
    :cond_b
    iget-object v8, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_4

    .line 238
    :catch_0
    move-exception v1

    goto/16 :goto_1

    :cond_c
    move-object v15, v1

    goto :goto_2
.end method
