.class public Lcom/google/android/gms/fitness/b/c/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/fitness/b/c/an;


# instance fields
.field protected a:Ljava/lang/String;

.field private c:Lcom/google/android/gms/fitness/b/b;

.field private d:Z

.field private e:Lcom/google/android/gms/fitness/b/c/an;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Set;

.field private i:Lcom/google/android/gms/fitness/b/l;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/fitness/b/c/ao;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/b/c/ao;-><init>()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/fitness/b/c/ao;->a:Z

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/b/c/ao;->a()Lcom/google/android/gms/fitness/b/c/an;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/b/c/f;->b:Lcom/google/android/gms/fitness/b/c/an;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-boolean v1, p0, Lcom/google/android/gms/fitness/b/c/f;->d:Z

    .line 44
    sget-object v0, Lcom/google/android/gms/fitness/b/c/f;->b:Lcom/google/android/gms/fitness/b/c/an;

    iput-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->e:Lcom/google/android/gms/fitness/b/c/an;

    .line 47
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->h:Ljava/util/Set;

    .line 48
    sget-object v0, Lcom/google/android/gms/fitness/b/m;->a:Lcom/google/android/gms/fitness/b/l;

    iput-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->i:Lcom/google/android/gms/fitness/b/l;

    .line 49
    iput v1, p0, Lcom/google/android/gms/fitness/b/c/f;->j:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/b/c/f;)Lcom/google/android/gms/fitness/b/b;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->c:Lcom/google/android/gms/fitness/b/b;

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 121
    if-nez p0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "%s is not set"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/fitness/b/c/f;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/gms/fitness/b/c/f;->d:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/fitness/b/c/f;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/fitness/b/c/f;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/fitness/b/c/f;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->h:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/fitness/b/c/f;)Lcom/google/android/gms/fitness/b/c/an;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->e:Lcom/google/android/gms/fitness/b/c/an;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/fitness/b/c/f;)Lcom/google/android/gms/fitness/b/l;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->i:Lcom/google/android/gms/fitness/b/l;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/fitness/b/c/f;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/gms/fitness/b/c/f;->j:I

    return v0
.end method


# virtual methods
.method public a(I)Lcom/google/android/gms/fitness/b/c/f;
    .locals 0

    .prologue
    .line 79
    iput p1, p0, Lcom/google/android/gms/fitness/b/c/f;->j:I

    .line 80
    return-object p0
.end method

.method public a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/f;->c:Lcom/google/android/gms/fitness/b/b;

    .line 55
    return-object p0
.end method

.method public a(Lcom/google/android/gms/fitness/b/c/an;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 2

    .prologue
    .line 84
    if-nez p1, :cond_0

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid sensor comparator specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/f;->e:Lcom/google/android/gms/fitness/b/c/an;

    .line 88
    return-object p0
.end method

.method public a(Lcom/google/android/gms/fitness/b/l;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/f;->i:Lcom/google/android/gms/fitness/b/l;

    .line 106
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/f;->f:Ljava/lang/String;

    .line 65
    return-object p0
.end method

.method public a(Z)Lcom/google/android/gms/fitness/b/c/f;
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/google/android/gms/fitness/b/c/f;->d:Z

    .line 60
    return-object p0
.end method

.method protected final a()V
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->c:Lcom/google/android/gms/fitness/b/b;

    const-string v1, "application"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->f:Ljava/lang/String;

    const-string v1, "outputDataTypeName"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->g:Ljava/lang/String;

    const-string v1, "outputStreamName"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->i:Lcom/google/android/gms/fitness/b/l;

    const-string v1, "valuePredicate"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget v0, p0, Lcom/google/android/gms/fitness/b/c/f;->j:I

    if-gez v0, :cond_0

    .line 115
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid read behind value specified "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/fitness/b/c/f;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_0
    return-void
.end method

.method public b()Lcom/google/android/gms/fitness/b/ad;
    .locals 1

    .prologue
    .line 129
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/f;->g:Ljava/lang/String;

    .line 70
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/f;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 75
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 2

    .prologue
    .line 92
    if-nez p1, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid merged stream name specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/f;->a:Ljava/lang/String;

    .line 96
    return-object p0
.end method
