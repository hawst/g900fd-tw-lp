.class public final Lcom/google/android/gms/googlehelp/c/w;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/c/e;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 50
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 52
    iget-object v0, p3, Lcom/google/ad/a/a/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/w;->setTag(Ljava/lang/Object;)V

    .line 53
    invoke-virtual {p0, v11}, Lcom/google/android/gms/googlehelp/c/w;->setOrientation(I)V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p3, Lcom/google/ad/a/a/h;->e:[Lcom/google/ad/a/a/k;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/w;->a:Ljava/util/List;

    .line 56
    iget-object v8, p3, Lcom/google/ad/a/a/h;->e:[Lcom/google/ad/a/a/k;

    array-length v9, v8

    move v6, v7

    :goto_0
    if-ge v6, v9, :cond_0

    aget-object v3, v8, v6

    .line 57
    iget-object v4, p3, Lcom/google/ad/a/a/h;->s:[Lcom/google/ad/a/a/k;

    new-instance v10, Landroid/widget/LinearLayout;

    invoke-direct {v10, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, v3, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/util/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, v3, Lcom/google/ad/a/a/k;->g:Z

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/googlehelp/common/v;->b(Landroid/content/Context;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    sget v0, Lcom/google/android/gms/p;->oE:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v7}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v10, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/gms/googlehelp/c/t;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/c/t;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/k;[Lcom/google/ad/a/a/k;Landroid/widget/TextView;)V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/w;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, v0}, Lcom/google/android/gms/googlehelp/c/d;->a(Lcom/google/android/gms/googlehelp/c/af;)V

    invoke-virtual {v10, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0, v10}, Lcom/google/android/gms/googlehelp/c/w;->addView(Landroid/view/View;)V

    .line 56
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 59
    :cond_0
    return-void
.end method


# virtual methods
.method public final c()Ljava/util/List;
    .locals 4

    .prologue
    .line 83
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/t;

    .line 85
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/t;->e()Ljava/lang/String;

    move-result-object v3

    .line 86
    if-eqz v3, :cond_0

    .line 87
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/t;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    :cond_1
    return-object v1
.end method

.method public final d()Ljava/util/List;
    .locals 4

    .prologue
    .line 96
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/t;

    .line 98
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/t;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 102
    :cond_1
    return-object v1
.end method
