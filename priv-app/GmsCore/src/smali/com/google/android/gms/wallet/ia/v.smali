.class public final Lcom/google/android/gms/wallet/ia/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/ia/f;


# static fields
.field private static final a:Ljava/text/NumberFormat;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/DecimalFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 40
    sput-object v0, Lcom/google/android/gms/wallet/ia/v;->a:Ljava/text/NumberFormat;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;II)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    if-gtz p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/wallet/ia/v;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;ILjava/lang/String;)V

    .line 75
    return-void

    .line 72
    :cond_0
    packed-switch p2, :pswitch_data_0

    const-string v0, "UsageCartDetailsRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized usage unit: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/google/android/gms/n;->D:I

    :goto_1
    sget v2, Lcom/google/android/gms/p;->CV:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v0, p3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    sget v0, Lcom/google/android/gms/n;->D:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public constructor <init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    .line 51
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/v;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    .line 52
    packed-switch p2, :pswitch_data_0

    .line 57
    const-string v0, "UsageCartDetailsRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized usage unit: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->BZ:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->d:Ljava/lang/String;

    .line 61
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iput-object p3, p0, Lcom/google/android/gms/wallet/ia/v;->e:Ljava/lang/String;

    .line 66
    :goto_1
    return-void

    .line 54
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->BZ:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->d:Ljava/lang/String;

    goto :goto_0

    .line 64
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->e:Ljava/lang/String;

    goto :goto_1

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/checkout/inapp/proto/d;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c()V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b()V

    .line 100
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    aget-object v2, v0, v6

    .line 104
    iget-object v0, v2, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v2, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    iget-object v0, v2, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    sget v3, Lcom/google/android/gms/l;->he:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(I)Landroid/view/View;

    move-result-object v3

    sget v0, Lcom/google/android/gms/j;->kE:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/j;->kH:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/v;->d:Ljava/lang/String;

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v2, "\n"

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->Cn:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/q;->s:I

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    if-lez v2, :cond_1

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    iget-object v6, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    invoke-direct {v5, v6, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/16 v2, 0x21

    invoke-virtual {v3, v5, v4, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    sget v1, Lcom/google/android/gms/l;->hc:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->kL:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/v;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a()V

    goto/16 :goto_0

    :cond_3
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final b(Lcom/google/checkout/inapp/proto/d;)V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 112
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 151
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    aget-object v1, v0, v8

    .line 116
    iget-object v0, v1, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v2

    .line 117
    iget-object v3, p1, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->BH:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    if-eqz v3, :cond_1

    .line 120
    iget v4, v3, Lcom/google/checkout/inapp/proto/at;->c:I

    packed-switch v4, :pswitch_data_0

    .line 131
    :cond_1
    :goto_1
    if-eqz v3, :cond_3

    iget-boolean v3, v3, Lcom/google/checkout/inapp/proto/at;->b:Z

    if-eqz v3, :cond_3

    .line 132
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->CY:I

    new-array v4, v11, [Ljava/lang/Object;

    aput-object v2, v4, v8

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/v;->d:Ljava/lang/String;

    aput-object v2, v4, v9

    aput-object v0, v4, v10

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 142
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    sget v2, Lcom/google/android/gms/j;->kM:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 143
    if-nez v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    sget v2, Lcom/google/android/gms/l;->hd:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(I)Landroid/view/View;

    move-result-object v0

    .line 146
    sget v2, Lcom/google/android/gms/j;->kM:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 148
    :cond_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v9}, Ljava/util/HashMap;-><init>(I)V

    const-string v3, "rate"

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v6, Lcom/google/android/gms/q;->u:I

    invoke-direct {v4, v5, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/ae;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a()V

    goto :goto_0

    .line 122
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->BG:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 125
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->BI:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 134
    :cond_3
    iget-wide v4, v1, Lcom/google/checkout/inapp/proto/f;->j:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    .line 135
    sget-object v3, Lcom/google/android/gms/wallet/ia/v;->a:Ljava/text/NumberFormat;

    iget-wide v4, v1, Lcom/google/checkout/inapp/proto/f;->j:J

    invoke-static {v4, v5}, Lcom/google/android/gms/wallet/common/w;->a(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 137
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->CX:I

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v8

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/v;->d:Ljava/lang/String;

    aput-object v2, v5, v9

    aput-object v1, v5, v10

    aput-object v0, v5, v11

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 139
    goto :goto_2

    .line 140
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/v;->b:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->CW:I

    new-array v3, v10, [Ljava/lang/Object;

    aput-object v2, v3, v8

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/v;->d:Ljava/lang/String;

    aput-object v2, v3, v9

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_2

    .line 120
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
