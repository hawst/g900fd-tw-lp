.class final Lcom/google/android/gms/playlog/store/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/database/sqlite/SQLiteOpenHelper;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteOpenHelper;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteOpenHelper;

    iput-object v0, p0, Lcom/google/android/gms/playlog/store/c;->a:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 76
    return-void
.end method

.method static a(J[B)I
    .locals 2

    .prologue
    .line 69
    array-length v0, p2

    long-to-int v1, p0

    invoke-static {p2, v0, v1}, Lcom/google/android/gms/common/util/aj;->a([BII)I

    move-result v0

    return v0
.end method

.method public static a(J[BI)Z
    .locals 2

    .prologue
    .line 65
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/playlog/store/c;->a(J[B)I

    move-result v0

    if-ne v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/playlog/store/d;
    .locals 3

    .prologue
    .line 236
    new-instance v0, Lcom/google/android/gms/playlog/store/d;

    iget-object v1, p0, Lcom/google/android/gms/playlog/store/c;->a:Landroid/database/sqlite/SQLiteOpenHelper;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/playlog/store/d;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;B)V

    return-object v0
.end method
