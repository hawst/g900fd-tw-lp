.class public final Lcom/google/android/gms/people/d/m;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile Q:[Lcom/google/android/gms/people/d/m;


# instance fields
.field public A:Z

.field public B:I

.field public C:I

.field public D:I

.field public E:I

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:I

.field public M:I

.field public N:I

.field public O:I

.field public P:J

.field public a:Lcom/google/android/gms/people/d/r;

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:I

.field public g:I

.field public h:I

.field public i:Z

.field public j:I

.field public k:I

.field public l:I

.field public m:Z

.field public n:Z

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:I

.field public x:I

.field public y:I

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 737
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 738
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    iput v1, p0, Lcom/google/android/gms/people/d/m;->b:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->c:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->d:I

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->e:Z

    iput v1, p0, Lcom/google/android/gms/people/d/m;->f:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->g:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->h:I

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->i:Z

    iput v1, p0, Lcom/google/android/gms/people/d/m;->j:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->k:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->l:I

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->m:Z

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->n:Z

    iput v1, p0, Lcom/google/android/gms/people/d/m;->o:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->p:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->q:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->r:I

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->s:Z

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->t:Z

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->u:Z

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->v:Z

    iput v1, p0, Lcom/google/android/gms/people/d/m;->w:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->x:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->y:I

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->z:Z

    iput-boolean v1, p0, Lcom/google/android/gms/people/d/m;->A:Z

    iput v1, p0, Lcom/google/android/gms/people/d/m;->B:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->C:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->D:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->E:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->F:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->G:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->H:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->I:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->J:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->K:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->L:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->M:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->N:I

    iput v1, p0, Lcom/google/android/gms/people/d/m;->O:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/m;->P:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/m;->cachedSize:I

    .line 739
    return-void
.end method

.method public static a()[Lcom/google/android/gms/people/d/m;
    .locals 2

    .prologue
    .line 600
    sget-object v0, Lcom/google/android/gms/people/d/m;->Q:[Lcom/google/android/gms/people/d/m;

    if-nez v0, :cond_1

    .line 601
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 603
    :try_start_0
    sget-object v0, Lcom/google/android/gms/people/d/m;->Q:[Lcom/google/android/gms/people/d/m;

    if-nez v0, :cond_0

    .line 604
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/people/d/m;

    sput-object v0, Lcom/google/android/gms/people/d/m;->Q:[Lcom/google/android/gms/people/d/m;

    .line 606
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 608
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/d/m;->Q:[Lcom/google/android/gms/people/d/m;

    return-object v0

    .line 606
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 1116
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1117
    iget-object v1, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    if-eqz v1, :cond_0

    .line 1118
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1121
    :cond_0
    iget v1, p0, Lcom/google/android/gms/people/d/m;->b:I

    if-eqz v1, :cond_1

    .line 1122
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/people/d/m;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1125
    :cond_1
    iget v1, p0, Lcom/google/android/gms/people/d/m;->c:I

    if-eqz v1, :cond_2

    .line 1126
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/people/d/m;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1129
    :cond_2
    iget v1, p0, Lcom/google/android/gms/people/d/m;->d:I

    if-eqz v1, :cond_3

    .line 1130
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/people/d/m;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1133
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->e:Z

    if-eqz v1, :cond_4

    .line 1134
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1137
    :cond_4
    iget v1, p0, Lcom/google/android/gms/people/d/m;->f:I

    if-eqz v1, :cond_5

    .line 1138
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/people/d/m;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1141
    :cond_5
    iget v1, p0, Lcom/google/android/gms/people/d/m;->g:I

    if-eqz v1, :cond_6

    .line 1142
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/people/d/m;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1145
    :cond_6
    iget v1, p0, Lcom/google/android/gms/people/d/m;->h:I

    if-eqz v1, :cond_7

    .line 1146
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/people/d/m;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1149
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->i:Z

    if-eqz v1, :cond_8

    .line 1150
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->i:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1153
    :cond_8
    iget v1, p0, Lcom/google/android/gms/people/d/m;->j:I

    if-eqz v1, :cond_9

    .line 1154
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/gms/people/d/m;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1157
    :cond_9
    iget v1, p0, Lcom/google/android/gms/people/d/m;->k:I

    if-eqz v1, :cond_a

    .line 1158
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/gms/people/d/m;->k:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1161
    :cond_a
    iget v1, p0, Lcom/google/android/gms/people/d/m;->l:I

    if-eqz v1, :cond_b

    .line 1162
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/people/d/m;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1165
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->m:Z

    if-eqz v1, :cond_c

    .line 1166
    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->m:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1169
    :cond_c
    iget v1, p0, Lcom/google/android/gms/people/d/m;->o:I

    if-eqz v1, :cond_d

    .line 1170
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/android/gms/people/d/m;->o:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1173
    :cond_d
    iget v1, p0, Lcom/google/android/gms/people/d/m;->p:I

    if-eqz v1, :cond_e

    .line 1174
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/android/gms/people/d/m;->p:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1177
    :cond_e
    iget v1, p0, Lcom/google/android/gms/people/d/m;->q:I

    if-eqz v1, :cond_f

    .line 1178
    const/16 v1, 0x10

    iget v2, p0, Lcom/google/android/gms/people/d/m;->q:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1181
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->s:Z

    if-eqz v1, :cond_10

    .line 1182
    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->s:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1185
    :cond_10
    iget v1, p0, Lcom/google/android/gms/people/d/m;->w:I

    if-eqz v1, :cond_11

    .line 1186
    const/16 v1, 0x12

    iget v2, p0, Lcom/google/android/gms/people/d/m;->w:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1189
    :cond_11
    iget v1, p0, Lcom/google/android/gms/people/d/m;->x:I

    if-eqz v1, :cond_12

    .line 1190
    const/16 v1, 0x13

    iget v2, p0, Lcom/google/android/gms/people/d/m;->x:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1193
    :cond_12
    iget v1, p0, Lcom/google/android/gms/people/d/m;->y:I

    if-eqz v1, :cond_13

    .line 1194
    const/16 v1, 0x14

    iget v2, p0, Lcom/google/android/gms/people/d/m;->y:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1197
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->z:Z

    if-eqz v1, :cond_14

    .line 1198
    const/16 v1, 0x15

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->z:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1201
    :cond_14
    iget v1, p0, Lcom/google/android/gms/people/d/m;->J:I

    if-eqz v1, :cond_15

    .line 1202
    const/16 v1, 0x16

    iget v2, p0, Lcom/google/android/gms/people/d/m;->J:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1205
    :cond_15
    iget v1, p0, Lcom/google/android/gms/people/d/m;->K:I

    if-eqz v1, :cond_16

    .line 1206
    const/16 v1, 0x17

    iget v2, p0, Lcom/google/android/gms/people/d/m;->K:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1209
    :cond_16
    iget v1, p0, Lcom/google/android/gms/people/d/m;->L:I

    if-eqz v1, :cond_17

    .line 1210
    const/16 v1, 0x18

    iget v2, p0, Lcom/google/android/gms/people/d/m;->L:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1213
    :cond_17
    iget v1, p0, Lcom/google/android/gms/people/d/m;->M:I

    if-eqz v1, :cond_18

    .line 1214
    const/16 v1, 0x19

    iget v2, p0, Lcom/google/android/gms/people/d/m;->M:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1217
    :cond_18
    iget v1, p0, Lcom/google/android/gms/people/d/m;->N:I

    if-eqz v1, :cond_19

    .line 1218
    const/16 v1, 0x1a

    iget v2, p0, Lcom/google/android/gms/people/d/m;->N:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1221
    :cond_19
    iget v1, p0, Lcom/google/android/gms/people/d/m;->O:I

    if-eqz v1, :cond_1a

    .line 1222
    const/16 v1, 0x1b

    iget v2, p0, Lcom/google/android/gms/people/d/m;->O:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1225
    :cond_1a
    iget-wide v2, p0, Lcom/google/android/gms/people/d/m;->P:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1b

    .line 1226
    const/16 v1, 0x1c

    iget-wide v2, p0, Lcom/google/android/gms/people/d/m;->P:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1229
    :cond_1b
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->t:Z

    if-eqz v1, :cond_1c

    .line 1230
    const/16 v1, 0x1d

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->t:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1233
    :cond_1c
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->u:Z

    if-eqz v1, :cond_1d

    .line 1234
    const/16 v1, 0x1e

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->u:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1237
    :cond_1d
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->n:Z

    if-eqz v1, :cond_1e

    .line 1238
    const/16 v1, 0x1f

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->n:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1241
    :cond_1e
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->v:Z

    if-eqz v1, :cond_1f

    .line 1242
    const/16 v1, 0x20

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->v:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1245
    :cond_1f
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->A:Z

    if-eqz v1, :cond_20

    .line 1246
    const/16 v1, 0x21

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->A:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1249
    :cond_20
    iget v1, p0, Lcom/google/android/gms/people/d/m;->B:I

    if-eqz v1, :cond_21

    .line 1250
    const/16 v1, 0x22

    iget v2, p0, Lcom/google/android/gms/people/d/m;->B:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1253
    :cond_21
    iget v1, p0, Lcom/google/android/gms/people/d/m;->C:I

    if-eqz v1, :cond_22

    .line 1254
    const/16 v1, 0x23

    iget v2, p0, Lcom/google/android/gms/people/d/m;->C:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1257
    :cond_22
    iget v1, p0, Lcom/google/android/gms/people/d/m;->E:I

    if-eqz v1, :cond_23

    .line 1258
    const/16 v1, 0x24

    iget v2, p0, Lcom/google/android/gms/people/d/m;->E:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1261
    :cond_23
    iget v1, p0, Lcom/google/android/gms/people/d/m;->F:I

    if-eqz v1, :cond_24

    .line 1262
    const/16 v1, 0x25

    iget v2, p0, Lcom/google/android/gms/people/d/m;->F:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1265
    :cond_24
    iget v1, p0, Lcom/google/android/gms/people/d/m;->H:I

    if-eqz v1, :cond_25

    .line 1266
    const/16 v1, 0x26

    iget v2, p0, Lcom/google/android/gms/people/d/m;->H:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1269
    :cond_25
    iget v1, p0, Lcom/google/android/gms/people/d/m;->I:I

    if-eqz v1, :cond_26

    .line 1270
    const/16 v1, 0x27

    iget v2, p0, Lcom/google/android/gms/people/d/m;->I:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1273
    :cond_26
    iget v1, p0, Lcom/google/android/gms/people/d/m;->r:I

    if-eqz v1, :cond_27

    .line 1274
    const/16 v1, 0x28

    iget v2, p0, Lcom/google/android/gms/people/d/m;->r:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1277
    :cond_27
    iget v1, p0, Lcom/google/android/gms/people/d/m;->D:I

    if-eqz v1, :cond_28

    .line 1278
    const/16 v1, 0x29

    iget v2, p0, Lcom/google/android/gms/people/d/m;->D:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1281
    :cond_28
    iget v1, p0, Lcom/google/android/gms/people/d/m;->G:I

    if-eqz v1, :cond_29

    .line 1282
    const/16 v1, 0x2a

    iget v2, p0, Lcom/google/android/gms/people/d/m;->G:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1285
    :cond_29
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 790
    if-ne p1, p0, :cond_1

    .line 929
    :cond_0
    :goto_0
    return v0

    .line 793
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/m;

    if-nez v2, :cond_2

    move v0, v1

    .line 794
    goto :goto_0

    .line 796
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/m;

    .line 797
    iget-object v2, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    if-nez v2, :cond_3

    .line 798
    iget-object v2, p1, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    if-eqz v2, :cond_4

    move v0, v1

    .line 799
    goto :goto_0

    .line 802
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    iget-object v3, p1, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 803
    goto :goto_0

    .line 806
    :cond_4
    iget v2, p0, Lcom/google/android/gms/people/d/m;->b:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 807
    goto :goto_0

    .line 809
    :cond_5
    iget v2, p0, Lcom/google/android/gms/people/d/m;->c:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 810
    goto :goto_0

    .line 812
    :cond_6
    iget v2, p0, Lcom/google/android/gms/people/d/m;->d:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 813
    goto :goto_0

    .line 815
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->e:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->e:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 816
    goto :goto_0

    .line 818
    :cond_8
    iget v2, p0, Lcom/google/android/gms/people/d/m;->f:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->f:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 819
    goto :goto_0

    .line 821
    :cond_9
    iget v2, p0, Lcom/google/android/gms/people/d/m;->g:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->g:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 822
    goto :goto_0

    .line 824
    :cond_a
    iget v2, p0, Lcom/google/android/gms/people/d/m;->h:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->h:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 825
    goto :goto_0

    .line 827
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->i:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->i:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 828
    goto :goto_0

    .line 830
    :cond_c
    iget v2, p0, Lcom/google/android/gms/people/d/m;->j:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->j:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 831
    goto :goto_0

    .line 833
    :cond_d
    iget v2, p0, Lcom/google/android/gms/people/d/m;->k:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->k:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 834
    goto :goto_0

    .line 836
    :cond_e
    iget v2, p0, Lcom/google/android/gms/people/d/m;->l:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->l:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 837
    goto :goto_0

    .line 839
    :cond_f
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->m:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->m:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 840
    goto :goto_0

    .line 842
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->n:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->n:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 843
    goto/16 :goto_0

    .line 845
    :cond_11
    iget v2, p0, Lcom/google/android/gms/people/d/m;->o:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->o:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 846
    goto/16 :goto_0

    .line 848
    :cond_12
    iget v2, p0, Lcom/google/android/gms/people/d/m;->p:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->p:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 849
    goto/16 :goto_0

    .line 851
    :cond_13
    iget v2, p0, Lcom/google/android/gms/people/d/m;->q:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->q:I

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 852
    goto/16 :goto_0

    .line 854
    :cond_14
    iget v2, p0, Lcom/google/android/gms/people/d/m;->r:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->r:I

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 855
    goto/16 :goto_0

    .line 857
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->s:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->s:Z

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 858
    goto/16 :goto_0

    .line 860
    :cond_16
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->t:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->t:Z

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 861
    goto/16 :goto_0

    .line 863
    :cond_17
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->u:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->u:Z

    if-eq v2, v3, :cond_18

    move v0, v1

    .line 864
    goto/16 :goto_0

    .line 866
    :cond_18
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->v:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->v:Z

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 867
    goto/16 :goto_0

    .line 869
    :cond_19
    iget v2, p0, Lcom/google/android/gms/people/d/m;->w:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->w:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 870
    goto/16 :goto_0

    .line 872
    :cond_1a
    iget v2, p0, Lcom/google/android/gms/people/d/m;->x:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->x:I

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 873
    goto/16 :goto_0

    .line 875
    :cond_1b
    iget v2, p0, Lcom/google/android/gms/people/d/m;->y:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->y:I

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 876
    goto/16 :goto_0

    .line 878
    :cond_1c
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->z:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->z:Z

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 879
    goto/16 :goto_0

    .line 881
    :cond_1d
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/m;->A:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/m;->A:Z

    if-eq v2, v3, :cond_1e

    move v0, v1

    .line 882
    goto/16 :goto_0

    .line 884
    :cond_1e
    iget v2, p0, Lcom/google/android/gms/people/d/m;->B:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->B:I

    if-eq v2, v3, :cond_1f

    move v0, v1

    .line 885
    goto/16 :goto_0

    .line 887
    :cond_1f
    iget v2, p0, Lcom/google/android/gms/people/d/m;->C:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->C:I

    if-eq v2, v3, :cond_20

    move v0, v1

    .line 888
    goto/16 :goto_0

    .line 890
    :cond_20
    iget v2, p0, Lcom/google/android/gms/people/d/m;->D:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->D:I

    if-eq v2, v3, :cond_21

    move v0, v1

    .line 891
    goto/16 :goto_0

    .line 893
    :cond_21
    iget v2, p0, Lcom/google/android/gms/people/d/m;->E:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->E:I

    if-eq v2, v3, :cond_22

    move v0, v1

    .line 894
    goto/16 :goto_0

    .line 896
    :cond_22
    iget v2, p0, Lcom/google/android/gms/people/d/m;->F:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->F:I

    if-eq v2, v3, :cond_23

    move v0, v1

    .line 897
    goto/16 :goto_0

    .line 899
    :cond_23
    iget v2, p0, Lcom/google/android/gms/people/d/m;->G:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->G:I

    if-eq v2, v3, :cond_24

    move v0, v1

    .line 900
    goto/16 :goto_0

    .line 902
    :cond_24
    iget v2, p0, Lcom/google/android/gms/people/d/m;->H:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->H:I

    if-eq v2, v3, :cond_25

    move v0, v1

    .line 903
    goto/16 :goto_0

    .line 905
    :cond_25
    iget v2, p0, Lcom/google/android/gms/people/d/m;->I:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->I:I

    if-eq v2, v3, :cond_26

    move v0, v1

    .line 906
    goto/16 :goto_0

    .line 908
    :cond_26
    iget v2, p0, Lcom/google/android/gms/people/d/m;->J:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->J:I

    if-eq v2, v3, :cond_27

    move v0, v1

    .line 909
    goto/16 :goto_0

    .line 911
    :cond_27
    iget v2, p0, Lcom/google/android/gms/people/d/m;->K:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->K:I

    if-eq v2, v3, :cond_28

    move v0, v1

    .line 912
    goto/16 :goto_0

    .line 914
    :cond_28
    iget v2, p0, Lcom/google/android/gms/people/d/m;->L:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->L:I

    if-eq v2, v3, :cond_29

    move v0, v1

    .line 915
    goto/16 :goto_0

    .line 917
    :cond_29
    iget v2, p0, Lcom/google/android/gms/people/d/m;->M:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->M:I

    if-eq v2, v3, :cond_2a

    move v0, v1

    .line 918
    goto/16 :goto_0

    .line 920
    :cond_2a
    iget v2, p0, Lcom/google/android/gms/people/d/m;->N:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->N:I

    if-eq v2, v3, :cond_2b

    move v0, v1

    .line 921
    goto/16 :goto_0

    .line 923
    :cond_2b
    iget v2, p0, Lcom/google/android/gms/people/d/m;->O:I

    iget v3, p1, Lcom/google/android/gms/people/d/m;->O:I

    if-eq v2, v3, :cond_2c

    move v0, v1

    .line 924
    goto/16 :goto_0

    .line 926
    :cond_2c
    iget-wide v2, p0, Lcom/google/android/gms/people/d/m;->P:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/m;->P:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 927
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 934
    iget-object v0, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 937
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->b:I

    add-int/2addr v0, v3

    .line 938
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->c:I

    add-int/2addr v0, v3

    .line 939
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->d:I

    add-int/2addr v0, v3

    .line 940
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 941
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->f:I

    add-int/2addr v0, v3

    .line 942
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->g:I

    add-int/2addr v0, v3

    .line 943
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->h:I

    add-int/2addr v0, v3

    .line 944
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->i:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 945
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->j:I

    add-int/2addr v0, v3

    .line 946
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->k:I

    add-int/2addr v0, v3

    .line 947
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->l:I

    add-int/2addr v0, v3

    .line 948
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->m:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v3

    .line 949
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->n:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v3

    .line 950
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->o:I

    add-int/2addr v0, v3

    .line 951
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->p:I

    add-int/2addr v0, v3

    .line 952
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->q:I

    add-int/2addr v0, v3

    .line 953
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->r:I

    add-int/2addr v0, v3

    .line 954
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->s:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v3

    .line 955
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->t:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v3

    .line 956
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->u:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v3

    .line 957
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->v:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v3

    .line 958
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->w:I

    add-int/2addr v0, v3

    .line 959
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->x:I

    add-int/2addr v0, v3

    .line 960
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/people/d/m;->y:I

    add-int/2addr v0, v3

    .line 961
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->z:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v3

    .line 962
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/gms/people/d/m;->A:Z

    if-eqz v3, :cond_a

    :goto_a
    add-int/2addr v0, v1

    .line 963
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->B:I

    add-int/2addr v0, v1

    .line 964
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->C:I

    add-int/2addr v0, v1

    .line 965
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->D:I

    add-int/2addr v0, v1

    .line 966
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->E:I

    add-int/2addr v0, v1

    .line 967
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->F:I

    add-int/2addr v0, v1

    .line 968
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->G:I

    add-int/2addr v0, v1

    .line 969
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->H:I

    add-int/2addr v0, v1

    .line 970
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->I:I

    add-int/2addr v0, v1

    .line 971
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->J:I

    add-int/2addr v0, v1

    .line 972
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->K:I

    add-int/2addr v0, v1

    .line 973
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->L:I

    add-int/2addr v0, v1

    .line 974
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->M:I

    add-int/2addr v0, v1

    .line 975
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->N:I

    add-int/2addr v0, v1

    .line 976
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/m;->O:I

    add-int/2addr v0, v1

    .line 977
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/people/d/m;->P:J

    iget-wide v4, p0, Lcom/google/android/gms/people/d/m;->P:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 979
    return v0

    .line 934
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/r;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 940
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 944
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 948
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 949
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 954
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 955
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 956
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 957
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 961
    goto :goto_9

    :cond_a
    move v1, v2

    .line 962
    goto :goto_a
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 594
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/d/r;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/r;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->i:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->j:I

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->k:I

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->l:I

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->m:Z

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->o:I

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->p:I

    goto :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->q:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->s:Z

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->w:I

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->x:I

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->y:I

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->z:Z

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->J:I

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->K:I

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->L:I

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->M:I

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->N:I

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->O:I

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/m;->P:J

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->t:Z

    goto/16 :goto_0

    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->u:Z

    goto/16 :goto_0

    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->n:Z

    goto/16 :goto_0

    :sswitch_20
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->v:Z

    goto/16 :goto_0

    :sswitch_21
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/m;->A:Z

    goto/16 :goto_0

    :sswitch_22
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->B:I

    goto/16 :goto_0

    :sswitch_23
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->C:I

    goto/16 :goto_0

    :sswitch_24
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->E:I

    goto/16 :goto_0

    :sswitch_25
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->F:I

    goto/16 :goto_0

    :sswitch_26
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->H:I

    goto/16 :goto_0

    :sswitch_27
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->I:I

    goto/16 :goto_0

    :sswitch_28
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->r:I

    goto/16 :goto_0

    :sswitch_29
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->D:I

    goto/16 :goto_0

    :sswitch_2a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/m;->G:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xd8 -> :sswitch_1b
        0xe0 -> :sswitch_1c
        0xe8 -> :sswitch_1d
        0xf0 -> :sswitch_1e
        0xf8 -> :sswitch_1f
        0x100 -> :sswitch_20
        0x108 -> :sswitch_21
        0x110 -> :sswitch_22
        0x118 -> :sswitch_23
        0x120 -> :sswitch_24
        0x128 -> :sswitch_25
        0x130 -> :sswitch_26
        0x138 -> :sswitch_27
        0x140 -> :sswitch_28
        0x148 -> :sswitch_29
        0x150 -> :sswitch_2a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 985
    iget-object v0, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    if-eqz v0, :cond_0

    .line 986
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 988
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/d/m;->b:I

    if-eqz v0, :cond_1

    .line 989
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/people/d/m;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 991
    :cond_1
    iget v0, p0, Lcom/google/android/gms/people/d/m;->c:I

    if-eqz v0, :cond_2

    .line 992
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/people/d/m;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 994
    :cond_2
    iget v0, p0, Lcom/google/android/gms/people/d/m;->d:I

    if-eqz v0, :cond_3

    .line 995
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/people/d/m;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 997
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->e:Z

    if-eqz v0, :cond_4

    .line 998
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1000
    :cond_4
    iget v0, p0, Lcom/google/android/gms/people/d/m;->f:I

    if-eqz v0, :cond_5

    .line 1001
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/gms/people/d/m;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1003
    :cond_5
    iget v0, p0, Lcom/google/android/gms/people/d/m;->g:I

    if-eqz v0, :cond_6

    .line 1004
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/people/d/m;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1006
    :cond_6
    iget v0, p0, Lcom/google/android/gms/people/d/m;->h:I

    if-eqz v0, :cond_7

    .line 1007
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/people/d/m;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1009
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->i:Z

    if-eqz v0, :cond_8

    .line 1010
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1012
    :cond_8
    iget v0, p0, Lcom/google/android/gms/people/d/m;->j:I

    if-eqz v0, :cond_9

    .line 1013
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/gms/people/d/m;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1015
    :cond_9
    iget v0, p0, Lcom/google/android/gms/people/d/m;->k:I

    if-eqz v0, :cond_a

    .line 1016
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/gms/people/d/m;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1018
    :cond_a
    iget v0, p0, Lcom/google/android/gms/people/d/m;->l:I

    if-eqz v0, :cond_b

    .line 1019
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/gms/people/d/m;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1021
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->m:Z

    if-eqz v0, :cond_c

    .line 1022
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1024
    :cond_c
    iget v0, p0, Lcom/google/android/gms/people/d/m;->o:I

    if-eqz v0, :cond_d

    .line 1025
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/android/gms/people/d/m;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1027
    :cond_d
    iget v0, p0, Lcom/google/android/gms/people/d/m;->p:I

    if-eqz v0, :cond_e

    .line 1028
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/android/gms/people/d/m;->p:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1030
    :cond_e
    iget v0, p0, Lcom/google/android/gms/people/d/m;->q:I

    if-eqz v0, :cond_f

    .line 1031
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/android/gms/people/d/m;->q:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1033
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->s:Z

    if-eqz v0, :cond_10

    .line 1034
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->s:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1036
    :cond_10
    iget v0, p0, Lcom/google/android/gms/people/d/m;->w:I

    if-eqz v0, :cond_11

    .line 1037
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/android/gms/people/d/m;->w:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1039
    :cond_11
    iget v0, p0, Lcom/google/android/gms/people/d/m;->x:I

    if-eqz v0, :cond_12

    .line 1040
    const/16 v0, 0x13

    iget v1, p0, Lcom/google/android/gms/people/d/m;->x:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1042
    :cond_12
    iget v0, p0, Lcom/google/android/gms/people/d/m;->y:I

    if-eqz v0, :cond_13

    .line 1043
    const/16 v0, 0x14

    iget v1, p0, Lcom/google/android/gms/people/d/m;->y:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1045
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->z:Z

    if-eqz v0, :cond_14

    .line 1046
    const/16 v0, 0x15

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->z:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1048
    :cond_14
    iget v0, p0, Lcom/google/android/gms/people/d/m;->J:I

    if-eqz v0, :cond_15

    .line 1049
    const/16 v0, 0x16

    iget v1, p0, Lcom/google/android/gms/people/d/m;->J:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1051
    :cond_15
    iget v0, p0, Lcom/google/android/gms/people/d/m;->K:I

    if-eqz v0, :cond_16

    .line 1052
    const/16 v0, 0x17

    iget v1, p0, Lcom/google/android/gms/people/d/m;->K:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1054
    :cond_16
    iget v0, p0, Lcom/google/android/gms/people/d/m;->L:I

    if-eqz v0, :cond_17

    .line 1055
    const/16 v0, 0x18

    iget v1, p0, Lcom/google/android/gms/people/d/m;->L:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1057
    :cond_17
    iget v0, p0, Lcom/google/android/gms/people/d/m;->M:I

    if-eqz v0, :cond_18

    .line 1058
    const/16 v0, 0x19

    iget v1, p0, Lcom/google/android/gms/people/d/m;->M:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1060
    :cond_18
    iget v0, p0, Lcom/google/android/gms/people/d/m;->N:I

    if-eqz v0, :cond_19

    .line 1061
    const/16 v0, 0x1a

    iget v1, p0, Lcom/google/android/gms/people/d/m;->N:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1063
    :cond_19
    iget v0, p0, Lcom/google/android/gms/people/d/m;->O:I

    if-eqz v0, :cond_1a

    .line 1064
    const/16 v0, 0x1b

    iget v1, p0, Lcom/google/android/gms/people/d/m;->O:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1066
    :cond_1a
    iget-wide v0, p0, Lcom/google/android/gms/people/d/m;->P:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1b

    .line 1067
    const/16 v0, 0x1c

    iget-wide v2, p0, Lcom/google/android/gms/people/d/m;->P:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1069
    :cond_1b
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->t:Z

    if-eqz v0, :cond_1c

    .line 1070
    const/16 v0, 0x1d

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->t:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1072
    :cond_1c
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->u:Z

    if-eqz v0, :cond_1d

    .line 1073
    const/16 v0, 0x1e

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->u:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1075
    :cond_1d
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->n:Z

    if-eqz v0, :cond_1e

    .line 1076
    const/16 v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->n:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1078
    :cond_1e
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->v:Z

    if-eqz v0, :cond_1f

    .line 1079
    const/16 v0, 0x20

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->v:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1081
    :cond_1f
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/m;->A:Z

    if-eqz v0, :cond_20

    .line 1082
    const/16 v0, 0x21

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/m;->A:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1084
    :cond_20
    iget v0, p0, Lcom/google/android/gms/people/d/m;->B:I

    if-eqz v0, :cond_21

    .line 1085
    const/16 v0, 0x22

    iget v1, p0, Lcom/google/android/gms/people/d/m;->B:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1087
    :cond_21
    iget v0, p0, Lcom/google/android/gms/people/d/m;->C:I

    if-eqz v0, :cond_22

    .line 1088
    const/16 v0, 0x23

    iget v1, p0, Lcom/google/android/gms/people/d/m;->C:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1090
    :cond_22
    iget v0, p0, Lcom/google/android/gms/people/d/m;->E:I

    if-eqz v0, :cond_23

    .line 1091
    const/16 v0, 0x24

    iget v1, p0, Lcom/google/android/gms/people/d/m;->E:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1093
    :cond_23
    iget v0, p0, Lcom/google/android/gms/people/d/m;->F:I

    if-eqz v0, :cond_24

    .line 1094
    const/16 v0, 0x25

    iget v1, p0, Lcom/google/android/gms/people/d/m;->F:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1096
    :cond_24
    iget v0, p0, Lcom/google/android/gms/people/d/m;->H:I

    if-eqz v0, :cond_25

    .line 1097
    const/16 v0, 0x26

    iget v1, p0, Lcom/google/android/gms/people/d/m;->H:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1099
    :cond_25
    iget v0, p0, Lcom/google/android/gms/people/d/m;->I:I

    if-eqz v0, :cond_26

    .line 1100
    const/16 v0, 0x27

    iget v1, p0, Lcom/google/android/gms/people/d/m;->I:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1102
    :cond_26
    iget v0, p0, Lcom/google/android/gms/people/d/m;->r:I

    if-eqz v0, :cond_27

    .line 1103
    const/16 v0, 0x28

    iget v1, p0, Lcom/google/android/gms/people/d/m;->r:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1105
    :cond_27
    iget v0, p0, Lcom/google/android/gms/people/d/m;->D:I

    if-eqz v0, :cond_28

    .line 1106
    const/16 v0, 0x29

    iget v1, p0, Lcom/google/android/gms/people/d/m;->D:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1108
    :cond_28
    iget v0, p0, Lcom/google/android/gms/people/d/m;->G:I

    if-eqz v0, :cond_29

    .line 1109
    const/16 v0, 0x2a

    iget v1, p0, Lcom/google/android/gms/people/d/m;->G:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1111
    :cond_29
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1112
    return-void
.end method
