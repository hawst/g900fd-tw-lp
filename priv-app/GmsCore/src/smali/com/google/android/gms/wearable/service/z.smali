.class final Lcom/google/android/gms/wearable/service/z;
.super Lcom/google/android/gms/wearable/internal/ai;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/d/c;
.implements Lcom/google/android/gms/wearable/internal/ae;


# instance fields
.field private final a:Ljava/util/concurrent/ConcurrentHashMap;

.field private final b:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final c:Landroid/content/pm/PackageManager;

.field private final d:Lcom/google/android/gms/wearable/node/a;

.field private final e:Lcom/google/android/gms/wearable/service/e;

.field private final f:Lcom/google/android/gms/wearable/node/o;

.field private final g:Lcom/google/android/gms/wearable/node/a/a;

.field private final h:Lcom/google/android/gms/wearable/node/au;

.field private final i:Landroid/telecom/TelecomManager;


# direct methods
.method constructor <init>(Landroid/content/pm/PackageManager;Lcom/google/android/gms/wearable/node/o;Lcom/google/android/gms/wearable/node/a;Lcom/google/android/gms/wearable/service/e;Lcom/google/android/gms/wearable/node/a/a;Lcom/google/android/gms/wearable/node/au;Landroid/telecom/TelecomManager;)V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/gms/wearable/internal/ai;-><init>()V

    .line 73
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/service/z;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 78
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/service/z;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 93
    iput-object p1, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    .line 94
    iput-object p2, p0, Lcom/google/android/gms/wearable/service/z;->f:Lcom/google/android/gms/wearable/node/o;

    .line 95
    iput-object p3, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    .line 96
    iput-object p4, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    .line 97
    iput-object p5, p0, Lcom/google/android/gms/wearable/service/z;->g:Lcom/google/android/gms/wearable/node/a/a;

    .line 98
    iput-object p6, p0, Lcom/google/android/gms/wearable/service/z;->h:Lcom/google/android/gms/wearable/node/au;

    .line 101
    iput-object p7, p0, Lcom/google/android/gms/wearable/service/z;->i:Landroid/telecom/TelecomManager;

    .line 102
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wearable/service/z;)Lcom/google/android/gms/wearable/node/a/a;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->g:Lcom/google/android/gms/wearable/node/a/a;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/wearable/service/z;)Lcom/google/android/gms/wearable/node/a;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/gms/wearable/node/am;->a:Lcom/google/android/gms/wearable/node/am;

    iget-object v0, v0, Lcom/google/android/gms/wearable/node/am;->d:Lcom/google/android/gms/wearable/node/ba;

    invoke-interface {v0}, Lcom/google/android/gms/wearable/node/ba;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/wearable/service/z;)Lcom/google/android/gms/wearable/node/o;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->f:Lcom/google/android/gms/wearable/node/o;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wearable/service/z;)Lcom/google/android/gms/wearable/node/au;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->h:Lcom/google/android/gms/wearable/node/au;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wearable/service/z;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/wearable/service/z;)Landroid/content/pm/PackageManager;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/wearable/service/z;)Landroid/telecom/TelecomManager;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->i:Landroid/telecom/TelecomManager;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 995
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/service/x;

    if-eqz v0, :cond_1

    .line 997
    :try_start_0
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wearable/service/x;->a(Lcom/google/android/gms/wearable/internal/ae;)V

    .line 998
    const-string v1, "WearableService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 999
    const-string v1, "WearableService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processEvents: published: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1002
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    goto :goto_0

    .line 1005
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 942
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/service/ay;

    .line 944
    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/wearable/service/ay;->a:Lcom/google/android/gms/wearable/internal/ae;

    invoke-interface {v2, p1}, Lcom/google/android/gms/wearable/internal/ae;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 946
    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/google/android/gms/wearable/service/z;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 947
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/service/ay;->binderDied()V

    goto :goto_0

    .line 950
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/util/ad;ZZ)V
    .locals 4

    .prologue
    .line 1051
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pending Events: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 1052
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1053
    const-string v0, "Listeners"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 1054
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 1055
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1056
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/IBinder;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 1059
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 1061
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/MessageEventParcelable;)V
    .locals 3

    .prologue
    .line 954
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/service/ay;

    .line 956
    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/wearable/service/ay;->a:Lcom/google/android/gms/wearable/internal/ae;

    invoke-interface {v2, p1}, Lcom/google/android/gms/wearable/internal/ae;->a(Lcom/google/android/gms/wearable/internal/MessageEventParcelable;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 958
    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/google/android/gms/wearable/service/z;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 959
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/service/ay;->binderDied()V

    goto :goto_0

    .line 962
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/NodeParcelable;)V
    .locals 3

    .prologue
    .line 966
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/service/ay;

    .line 968
    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/wearable/service/ay;->a:Lcom/google/android/gms/wearable/internal/ae;

    invoke-interface {v2, p1}, Lcom/google/android/gms/wearable/internal/ae;->a(Lcom/google/android/gms/wearable/internal/NodeParcelable;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 970
    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/google/android/gms/wearable/service/z;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 971
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/service/ay;->binderDied()V

    goto :goto_0

    .line 974
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5

    .prologue
    .line 324
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 326
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getConfigs: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/av;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/av;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 348
    return-void

    .line 347
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ab;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 443
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 445
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getDataItem: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/wearable/node/az;->a(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 449
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Called getDataItem with a non-exact uri. Provided: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    new-instance v0, Lcom/google/android/gms/wearable/internal/GetDataItemResponse;

    const/16 v1, 0xd

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/wearable/internal/GetDataItemResponse;-><init>(ILcom/google/android/gms/wearable/internal/DataItemParcelable;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/wearable/internal/ab;->a(Lcom/google/android/gms/wearable/internal/GetDataItemResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 480
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 481
    :goto_0
    return-void

    .line 455
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ac;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/ac;-><init>(Lcom/google/android/gms/wearable/service/z;Landroid/net/Uri;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ab;Lcom/google/android/gms/wearable/Asset;)V
    .locals 5

    .prologue
    .line 636
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 638
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getFdForAsset: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ah;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/ah;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/Asset;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 678
    return-void

    .line 677
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ab;Lcom/google/android/gms/wearable/ConnectionConfiguration;)V
    .locals 5

    .prologue
    .line 253
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 255
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "putConfig: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :cond_0
    if-nez p2, :cond_1

    .line 259
    const-string v0, "WearableService"

    const-string v1, "putConfig: called with null config. Ignoring."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/wearable/internal/ab;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 282
    :goto_0
    return-void

    .line 266
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/at;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/at;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/ConnectionConfiguration;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ab;Lcom/google/android/gms/wearable/PutDataRequest;)V
    .locals 5

    .prologue
    .line 408
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 410
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "putData: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/wearable/node/n;->a(Lcom/google/android/gms/wearable/PutDataRequest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 414
    new-instance v0, Lcom/google/android/gms/wearable/internal/PutDataResponse;

    const/16 v1, 0xfa3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/wearable/internal/PutDataResponse;-><init>(ILcom/google/android/gms/wearable/internal/DataItemParcelable;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/wearable/internal/ab;->a(Lcom/google/android/gms/wearable/internal/PutDataResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 437
    :goto_0
    return-void

    .line 419
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ab;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/ab;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/PutDataRequest;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 436
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ab;Lcom/google/android/gms/wearable/internal/AddListenerRequest;)V
    .locals 5

    .prologue
    .line 746
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 748
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "addListener: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p2, Lcom/google/android/gms/wearable/internal/AddListenerRequest;->b:Lcom/google/android/gms/wearable/internal/ae;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ak;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/ak;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/AddListenerRequest;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 787
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 788
    return-void

    .line 787
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ab;Lcom/google/android/gms/wearable/internal/RemoveListenerRequest;)V
    .locals 5

    .prologue
    .line 794
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 796
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 797
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "removeListener: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p2, Lcom/google/android/gms/wearable/internal/RemoveListenerRequest;->b:Lcom/google/android/gms/wearable/internal/ae;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/am;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/am;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/RemoveListenerRequest;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 819
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 820
    return-void

    .line 819
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ab;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 289
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 291
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "removeConfig: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    const-string v0, "WearableService"

    const-string v1, "removeConfig: called with null config. Ignoring."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/wearable/internal/ab;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 318
    :goto_0
    return-void

    .line 302
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/au;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/au;-><init>(Lcom/google/android/gms/wearable/service/z;Ljava/lang/String;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 317
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/wearable/internal/ab;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 9

    .prologue
    .line 588
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v6

    .line 590
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591
    const-string v1, "WearableService"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "sendMessage: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p4, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    :cond_0
    if-nez p2, :cond_2

    .line 595
    new-instance v0, Lcom/google/android/gms/wearable/internal/SendMessageResponse;

    const/16 v1, 0xfa4

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wearable/internal/SendMessageResponse;-><init>(II)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/wearable/internal/ab;->a(Lcom/google/android/gms/wearable/internal/SendMessageResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 630
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 631
    :goto_1
    return-void

    .line 591
    :cond_1
    :try_start_1
    array-length v0, p4

    goto :goto_0

    .line 601
    :cond_2
    iget-object v8, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v0, Lcom/google/android/gms/wearable/service/ag;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wearable/service/ag;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {v8, p0, v0}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 630
    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v6, v7}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/wearable/service/x;)V
    .locals 1

    .prologue
    .line 989
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 990
    return-void
.end method

.method public final b(Lcom/google/android/gms/wearable/internal/NodeParcelable;)V
    .locals 3

    .prologue
    .line 978
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/service/ay;

    .line 980
    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/wearable/service/ay;->a:Lcom/google/android/gms/wearable/internal/ae;

    invoke-interface {v2, p1}, Lcom/google/android/gms/wearable/internal/ae;->b(Lcom/google/android/gms/wearable/internal/NodeParcelable;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 982
    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/google/android/gms/wearable/service/z;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 983
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/service/ay;->binderDied()V

    goto :goto_0

    .line 986
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5

    .prologue
    .line 486
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 488
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getDataItems: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ad;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/ad;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 518
    return-void

    .line 517
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/wearable/internal/ab;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 523
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 525
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getDataItemsByUri: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ae;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/ae;-><init>(Lcom/google/android/gms/wearable/service/z;Landroid/net/Uri;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 553
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 554
    return-void

    .line 553
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/wearable/internal/ab;Lcom/google/android/gms/wearable/ConnectionConfiguration;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 109
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 111
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "putConnection: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_0
    if-nez p2, :cond_1

    .line 115
    const-string v0, "WearableService"

    const-string v1, "putConnection: called with null config. Ignoring."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/wearable/internal/ab;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 138
    :goto_0
    return-void

    .line 122
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/aa;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/aa;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/ConnectionConfiguration;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/wearable/internal/ab;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 354
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 356
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "enableConnection: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/aw;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/aw;-><init>(Lcom/google/android/gms/wearable/service/z;Ljava/lang/String;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 374
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 375
    return-void

    .line 374
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5

    .prologue
    .line 683
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 685
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 686
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getLocalNode: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ai;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/ai;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 705
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 706
    return-void

    .line 705
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/wearable/internal/ab;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 559
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 561
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "deleteDataItems: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/af;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/af;-><init>(Lcom/google/android/gms/wearable/service/z;Landroid/net/Uri;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 581
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 582
    return-void

    .line 581
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/wearable/internal/ab;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 381
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 383
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "disableConnection: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ax;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/wearable/service/ax;-><init>(Lcom/google/android/gms/wearable/service/z;Ljava/lang/String;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 402
    return-void

    .line 401
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5

    .prologue
    .line 711
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 713
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 714
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getConnectedNodes: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/aj;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/aj;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 740
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 741
    return-void

    .line 740
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5

    .prologue
    .line 825
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 827
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getStorageInformation: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 833
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/an;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/an;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 847
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 848
    return-void

    .line 847
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5

    .prologue
    .line 853
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 855
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "clearStorage: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ao;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/ao;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 872
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 873
    return-void

    .line 872
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final g(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 880
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 882
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 884
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 885
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "endCall: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ap;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/ap;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 904
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 905
    return-void

    .line 904
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final h(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 912
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 914
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 916
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "acceptRingingCall: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/aq;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/aq;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 936
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 937
    return-void

    .line 936
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final i(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 145
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 147
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "getConnection: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/al;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/al;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 178
    return-void

    .line 177
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final j(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 185
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 187
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "enableConnection: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/ar;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/ar;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 212
    return-void

    .line 211
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final k(Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 219
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 221
    :try_start_0
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "disableConnection: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/z;->e:Lcom/google/android/gms/wearable/service/e;

    new-instance v1, Lcom/google/android/gms/wearable/service/as;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wearable/service/as;-><init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/wearable/service/e;->a(Ljava/lang/Object;Lcom/google/android/gms/wearable/service/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 246
    return-void

    .line 245
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1046
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WearableServiceStub["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/z;->d:Lcom/google/android/gms/wearable/node/a;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
