.class public final Lcom/google/android/gms/games/ui/common/players/a;
.super Lcom/google/android/gms/games/ui/d;
.source "SourceFile"


# static fields
.field private static final i:I


# instance fields
.field private h:Ljava/util/HashMap;

.field private final j:Lcom/google/android/gms/games/ui/common/players/b;

.field private k:Ljava/lang/String;

.field private l:Z

.field private final m:I

.field private final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    sget v0, Lcom/google/android/gms/l;->bs:I

    sput v0, Lcom/google/android/gms/games/ui/common/players/a;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;ZI)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d;-><init>(Landroid/content/Context;)V

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->h:Ljava/util/HashMap;

    .line 105
    const-string v0, "PlayerAvatarEventListener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/b;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->j:Lcom/google/android/gms/games/ui/common/players/b;

    .line 107
    iput-boolean p3, p0, Lcom/google/android/gms/games/ui/common/players/a;->l:Z

    .line 108
    iput p4, p0, Lcom/google/android/gms/games/ui/common/players/a;->m:I

    .line 109
    iput v1, p0, Lcom/google/android/gms/games/ui/common/players/a;->n:I

    .line 111
    sget v0, Lcom/google/android/gms/k;->i:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/common/players/a;->e(II)V

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/players/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/players/a;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->l:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/players/a;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->n:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/players/a;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/common/players/a;)Lcom/google/android/gms/games/ui/common/players/b;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->j:Lcom/google/android/gms/games/ui/common/players/b;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/common/players/a;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->m:I

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/players/a;->k:Ljava/lang/String;

    .line 121
    return-void
.end method

.method protected final synthetic b(Landroid/view/ViewGroup;I)Lcom/google/android/gms/games/ui/f;
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->e:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/games/ui/common/players/a;->i:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/common/players/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/common/players/c;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 152
    sget v0, Lcom/google/android/gms/j;->gF:I

    return v0
.end method

.method public final r()I
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/a;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/k;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 116
    sget v0, Lcom/google/android/gms/g;->ax:I

    return v0
.end method
