.class public Lcom/google/android/gms/wallet/address/GoogleTopBarView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 61
    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->setOrientation(I)V

    .line 63
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 64
    sget v1, Lcom/google/android/gms/l;->hx:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 66
    sget v0, Lcom/google/android/gms/j;->jl:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    .line 68
    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    .line 58
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/cq;)V
    .locals 2

    .prologue
    .line 47
    if-eqz p1, :cond_0

    .line 48
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a([Landroid/accounts/Account;)[Lcom/google/android/gms/wallet/common/ui/a;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a([Lcom/google/android/gms/wallet/common/ui/a;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Lcom/google/android/gms/wallet/common/ui/cq;)V

    .line 54
    return-void
.end method
