.class public abstract Lcom/google/android/gms/wallet/common/a/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/a/b;


# instance fields
.field a:Ljava/util/ArrayList;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/HashMap;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/a/s;->b:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/s;->c:Ljava/util/HashMap;

    .line 38
    return-void
.end method

.method private a([C)Ljava/util/ArrayList;
    .locals 8

    .prologue
    .line 64
    if-eqz p1, :cond_1

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([C)V

    move-object v2, v0

    .line 65
    :goto_0
    iget-object v5, p0, Lcom/google/android/gms/wallet/common/a/s;->c:Ljava/util/HashMap;

    monitor-enter v5

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/s;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 68
    if-nez v0, :cond_3

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/s;->a:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/Collection;[C)Ljava/util/ArrayList;

    move-result-object v3

    .line 73
    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v0

    :goto_1
    if-ge v4, v6, :cond_2

    .line 74
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/a/b;

    .line 75
    iget-object v1, v0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    array-length v1, v1

    const/4 v7, 0x1

    if-ne v1, v7, :cond_0

    .line 76
    iget-object v1, v0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    const-string v7, ""

    invoke-static {v1, v7}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/google/t/a/b;->q:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 64
    :cond_1
    const-string v0, "*"

    move-object v2, v0

    goto :goto_0

    .line 79
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/s;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v3

    .line 81
    :cond_3
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 46
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/a/s;->a:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 49
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/a/s;->b()Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 54
    :goto_0
    if-eqz v0, :cond_1

    .line 55
    const/4 v1, 0x0

    :try_start_2
    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/Collection;[C)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/s;->a:Ljava/util/ArrayList;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 60
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 50
    :catch_0
    move-exception v1

    .line 51
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/a/s;->b:Ljava/lang/String;

    const-string v3, "Could not retrieve addresses"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 57
    :cond_1
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/s;->a:Ljava/util/ArrayList;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/t/a/b;
    .locals 3

    .prologue
    .line 181
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/a/s;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not use reference identifiers"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/s;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;C[CILjava/lang/String;)Ljava/util/List;
    .locals 15

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/a/s;->c()V

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/a/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 176
    :goto_0
    return-object v1

    .line 105
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 106
    iget-object v7, p0, Lcom/google/android/gms/wallet/common/a/s;->b:Ljava/lang/String;

    .line 107
    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v8

    .line 108
    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/a/s;->a([C)Ljava/util/ArrayList;

    move-result-object v9

    .line 110
    new-instance v10, Ljava/util/TreeMap;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v10, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    .line 112
    const/16 v1, 0x4e

    move/from16 v0, p2

    if-ne v0, v1, :cond_2

    const/4 v1, 0x1

    move v2, v1

    .line 113
    :goto_1
    const/4 v1, 0x0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v6, v1

    :goto_2
    if-ge v6, v11, :cond_8

    .line 114
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/t/a/b;

    .line 115
    if-eqz v1, :cond_1

    .line 116
    move/from16 v0, p2

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;C)Ljava/lang/String;

    move-result-object v12

    .line 120
    move-object/from16 v0, p1

    invoke-static {v12, v0}, Lcom/google/android/gms/wallet/shared/common/a/a;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 121
    const/4 v3, 0x0

    .line 124
    if-eqz p4, :cond_b

    .line 125
    iget-object v5, v1, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, v1, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v5

    .line 128
    :goto_3
    if-eqz v5, :cond_4

    const/16 v13, 0x35a

    if-eq v5, v13, :cond_4

    .line 130
    move/from16 v0, p4

    if-eq v5, v0, :cond_b

    .line 131
    if-eqz v2, :cond_1

    .line 132
    invoke-virtual {v10, v12}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 133
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v10, v12, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    :cond_1
    :goto_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_2

    .line 112
    :cond_2
    const/4 v1, 0x0

    move v2, v1

    goto :goto_1

    .line 125
    :cond_3
    const/16 v5, 0x35a

    goto :goto_3

    .line 138
    :cond_4
    const/16 v5, 0x35a

    move/from16 v0, p4

    if-eq v0, v5, :cond_b

    .line 140
    invoke-static {v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/t/a/b;

    .line 141
    iput-object v8, v1, Lcom/google/t/a/b;->a:Ljava/lang/String;

    .line 142
    const/4 v3, 0x1

    move v14, v3

    move-object v3, v1

    move v1, v14

    .line 145
    :goto_5
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 146
    iget-object v5, v3, Lcom/google/t/a/b;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 147
    iget-object v1, v3, Lcom/google/t/a/b;->c:Ljava/lang/String;

    .line 148
    move-object/from16 v0, p5

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 149
    if-eqz v2, :cond_1

    .line 150
    invoke-virtual {v10, v12}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 151
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v10, v12, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 158
    :cond_5
    if-eqz v1, :cond_7

    .line 159
    :goto_6
    move-object/from16 v0, p5

    iput-object v0, v3, Lcom/google/t/a/b;->c:Ljava/lang/String;

    .line 160
    :cond_6
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v10, v12, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v1, Lcom/google/android/gms/wallet/common/a/c;->b:Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/wallet/common/a/c;->a:[C

    move-object/from16 v0, p3

    invoke-static {v3, v1, v0, v5}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;Ljava/lang/String;[C[C)Ljava/lang/String;

    move-result-object v1

    .line 167
    new-instance v5, Lcom/google/android/gms/wallet/common/a/c;

    invoke-direct {v5, v12, v3, v1, v7}, Lcom/google/android/gms/wallet/common/a/c;-><init>(Ljava/lang/String;Lcom/google/t/a/b;Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 158
    :cond_7
    invoke-static {v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/t/a/b;

    move-object v3, v1

    goto :goto_6

    .line 170
    :cond_8
    invoke-virtual {v10}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/util/Map$Entry;

    .line 171
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 172
    new-instance v5, Lcom/google/android/gms/wallet/common/a/c;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v5, v1, v7}, Lcom/google/android/gms/wallet/common/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 175
    :cond_a
    sget-object v1, Lcom/google/android/gms/wallet/common/a/c;->c:Ljava/util/Comparator;

    invoke-static {v4, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object v1, v4

    .line 176
    goto/16 :goto_0

    :cond_b
    move v14, v3

    move-object v3, v1

    move v1, v14

    goto/16 :goto_5
.end method

.method protected abstract b()Ljava/util/ArrayList;
.end method
