.class abstract Lcom/google/android/gms/people/sync/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final b:Z

.field protected final c:Z

.field final synthetic d:Lcom/google/android/gms/people/sync/z;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/people/sync/z;ZZ)V
    .locals 0

    .prologue
    .line 2087
    iput-object p1, p0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2088
    iput-boolean p2, p0, Lcom/google/android/gms/people/sync/ag;->c:Z

    .line 2089
    iput-boolean p3, p0, Lcom/google/android/gms/people/sync/ag;->b:Z

    .line 2090
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;)V
    .locals 4

    .prologue
    .line 2095
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 2098
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->b()Ljava/util/List;

    move-result-object v2

    .line 2099
    invoke-static {v2}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v3

    .line 2100
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 2101
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    .line 2103
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/sync/ag;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 2100
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2106
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->f(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->d(Lcom/google/android/gms/people/sync/z;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v2}, Lcom/google/android/gms/people/sync/z;->e(Lcom/google/android/gms/people/sync/z;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x100

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2109
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2111
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 2113
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->g(Lcom/google/android/gms/people/sync/z;)V

    .line 2114
    return-void

    .line 2111
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ag;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0
.end method

.method public abstract a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
.end method

.method public abstract b()V
.end method
