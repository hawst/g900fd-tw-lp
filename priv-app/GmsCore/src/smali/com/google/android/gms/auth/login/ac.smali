.class public final Lcom/google/android/gms/auth/login/ac;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private b:Landroid/os/Handler;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "GLSActivity"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "AuthCheckinReceiver"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/login/ac;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/ac;->c:Z

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/auth/login/ac;->b:Landroid/os/Handler;

    .line 25
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 29
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 30
    :goto_0
    sget-object v1, Lcom/google/android/gms/auth/login/ac;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Processing action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 31
    const-string v1, "com.google.android.checkin.CHECKIN_COMPLETE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ac;->c:Z

    if-nez v0, :cond_1

    .line 32
    iput-boolean v7, p0, Lcom/google/android/gms/auth/login/ac;->c:Z

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ac;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 34
    const-string v1, "success"

    invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 37
    :cond_0
    iput v7, v0, Landroid/os/Message;->arg1:I

    .line 38
    sget-object v1, Lcom/google/android/gms/auth/login/ac;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ID set, checkin status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "success"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 40
    iget-object v1, p0, Lcom/google/android/gms/auth/login/ac;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 47
    :cond_1
    :goto_1
    return-void

    .line 29
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 42
    :cond_3
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 43
    sget-object v1, Lcom/google/android/gms/auth/login/ac;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Checkin completed false."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    .line 44
    iget-object v1, p0, Lcom/google/android/gms/auth/login/ac;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method
