.class public Lcom/google/android/gms/app/settings/DataManagementActivity;
.super Lcom/google/android/gms/common/widget/a/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/common/widget/a/p;


# instance fields
.field private a:Z

.field private b:Lcom/google/android/gms/common/widget/a/o;

.field private c:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/a/b;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/app/settings/DataManagementActivity;)Lcom/google/android/gms/common/widget/a/o;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->b:Lcom/google/android/gms/common/widget/a/o;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/app/settings/DataManagementActivity;Z)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/gms/app/settings/DataManagementActivity;->b(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->a:Z

    .line 103
    if-ne v0, p1, :cond_0

    .line 121
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/app/settings/DataManagementActivity;->b(Z)V

    .line 108
    new-instance v1, Lcom/google/android/gms/drive/DrivePreferences;

    invoke-direct {v1, p1}, Lcom/google/android/gms/drive/DrivePreferences;-><init>(Z)V

    .line 109
    sget-object v2, Lcom/google/android/gms/drive/b;->j:Lcom/google/android/gms/drive/ab;

    iget-object v3, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v2, v3, v1}, Lcom/google/android/gms/drive/ab;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DrivePreferences;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/app/settings/a;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/app/settings/a;-><init>(Lcom/google/android/gms/app/settings/DataManagementActivity;Z)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/app/settings/DataManagementActivity;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/app/settings/DataManagementActivity;->f()V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->a:Z

    .line 128
    iget-object v1, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->b:Lcom/google/android/gms/common/widget/a/o;

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/gms/p;->fg:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/a/o;->d(I)V

    .line 131
    return-void

    .line 128
    :cond_0
    sget v0, Lcom/google/android/gms/p;->ff:I

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 149
    sget v0, Lcom/google/android/gms/p;->gJ:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 150
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->b:Lcom/google/android/gms/common/widget/a/o;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/o;->a(Z)V

    .line 177
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/common/ew;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 178
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/widget/a/l;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/DataManagementActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    new-instance v0, Lcom/google/android/gms/common/widget/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/widget/a/o;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->b:Lcom/google/android/gms/common/widget/a/o;

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->b:Lcom/google/android/gms/common/widget/a/o;

    sget v1, Lcom/google/android/gms/p;->eS:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/o;->c(I)V

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->b:Lcom/google/android/gms/common/widget/a/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/o;->a(Z)V

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->b:Lcom/google/android/gms/common/widget/a/o;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/widget/a/o;->a(Lcom/google/android/gms/common/widget/a/p;)V

    .line 59
    iget-object v0, p1, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    iget-object v1, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->b:Lcom/google/android/gms/common/widget/a/o;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 61
    :cond_0
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 154
    sget-object v0, Lcom/google/android/gms/drive/b;->j:Lcom/google/android/gms/drive/ab;

    iget-object v1, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/ab;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/app/settings/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/app/settings/b;-><init>(Lcom/google/android/gms/app/settings/DataManagementActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 167
    return-void
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->b:Lcom/google/android/gms/common/widget/a/o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/o;->a(Z)V

    .line 172
    return-void
.end method

.method public onClick(Landroid/view/View;Lcom/google/android/gms/common/widget/a/o;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/google/android/gms/app/settings/DataManagementActivity;->f()V

    .line 73
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/app/settings/DataManagementActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 71
    invoke-virtual {p0, p1}, Lcom/google/android/gms/app/settings/DataManagementActivity;->openContextMenu(Landroid/view/View;)V

    .line 72
    invoke-virtual {p0, p1}, Lcom/google/android/gms/app/settings/DataManagementActivity;->unregisterForContextMenu(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 85
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 87
    sget v2, Lcom/google/android/gms/j;->dg:I

    if-ne v2, v1, :cond_0

    .line 88
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gms/app/settings/DataManagementActivity;->a(Z)V

    .line 95
    :goto_0
    return v0

    .line 89
    :cond_0
    sget v2, Lcom/google/android/gms/j;->dh:I

    if-ne v2, v1, :cond_1

    .line 90
    invoke-direct {p0, v0}, Lcom/google/android/gms/app/settings/DataManagementActivity;->a(Z)V

    goto :goto_0

    .line 92
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/support/v7/app/a;->a(Z)V

    .line 45
    invoke-static {p0}, Lcom/google/android/gms/drive/g/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 46
    array-length v1, v0

    if-lez v1, :cond_0

    .line 47
    aget-object v0, v0, v6

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/drive/b;->g:Lcom/google/android/gms/common/api/c;

    new-instance v3, Lcom/google/android/gms/drive/g;

    invoke-direct {v3}, Lcom/google/android/gms/drive/g;-><init>()V

    iget-object v4, v3, Lcom/google/android/gms/drive/g;->a:Landroid/os/Bundle;

    const-string v5, "bypass_initial_sync"

    invoke-virtual {v4, v5, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v3}, Lcom/google/android/gms/drive/g;->a()Lcom/google/android/gms/drive/f;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/b;->d:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/b;->e:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v1, p0, v6, p0}, Lcom/google/android/gms/common/api/w;->a(Landroid/support/v4/app/q;ILcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/app/settings/DataManagementActivity;->c:Lcom/google/android/gms/common/api/v;

    .line 49
    :cond_0
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/widget/a/b;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/app/settings/DataManagementActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 80
    sget v1, Lcom/google/android/gms/m;->b:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 81
    return-void
.end method
