.class final Lcom/google/android/gms/drive/ui/picker/b;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

.field private d:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/drive/ui/picker/b;->b:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/google/android/gms/drive/ui/picker/b;->c:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    .line 57
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/b;->c:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    iget v1, v1, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->f:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-static {v1}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->f(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/drive/g/t;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/g/t;->a(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/b;->c:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->a(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/gms/drive/an;

    invoke-direct {v0}, Lcom/google/android/gms/drive/an;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/an;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/b;->c:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/an;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/an;->a()Lcom/google/android/gms/drive/am;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->a(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CreateFolderActivity"

    const-string v1, "Client is not in connected state"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->b(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-static {v2}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->c(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/h;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/drive/q;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-static {v2}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->e(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/drive/q;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/s;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-static {v2}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->d(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-static {v3}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->b(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/drive/h;->b(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/q;

    move-result-object v0

    goto :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 47
    check-cast p1, Lcom/google/android/gms/drive/s;

    if-nez p1, :cond_0

    const-string v0, "CreateFolderActivity"

    const-string v1, "Failed to create folder: null result"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/b;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/drive/s;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Lcom/google/android/gms/drive/s;->b()Lcom/google/android/gms/drive/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/q;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "driveId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->finish()V

    goto :goto_0

    :cond_2
    const-string v1, "CreateFolderActivity"

    const-string v2, "Failed to create folder (%d): %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/b;->a()V

    goto :goto_0
.end method

.method public final onPreExecute()V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/b;->c:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    iget v1, v1, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->e:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/b;->a:Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-static {v1}, Lcom/google/android/gms/drive/ui/k;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    const-string v2, ""

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/b;->d:Landroid/app/ProgressDialog;

    .line 130
    return-void
.end method
