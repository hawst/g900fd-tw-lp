.class final Lcom/google/android/gms/games/ui/common/leaderboards/f;
.super Lcom/google/android/gms/games/ui/cx;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final k:Landroid/support/v7/widget/CardView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/view/View;

.field private final q:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cx;-><init>(Landroid/view/View;)V

    move-object v0, p1

    .line 126
    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->k:Landroid/support/v7/widget/CardView;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->k:Landroid/support/v7/widget/CardView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->n:Landroid/widget/TextView;

    .line 130
    sget v0, Lcom/google/android/gms/j;->si:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->o:Landroid/widget/TextView;

    .line 132
    sget v0, Lcom/google/android/gms/j;->km:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->p:Landroid/view/View;

    .line 134
    sget v0, Lcom/google/android/gms/j;->nf:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->q:Landroid/view/View;

    .line 135
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/ac;I)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 139
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/cx;->a(Lcom/google/android/gms/games/ui/ac;I)V

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/d;

    .line 142
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->a(Lcom/google/android/gms/games/ui/common/leaderboards/d;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->k:Landroid/support/v7/widget/CardView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->setClickable(Z)V

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->n:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->jt:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->o:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->js:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->p:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->b(Lcom/google/android/gms/games/ui/common/leaderboards/d;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->k:Landroid/support/v7/widget/CardView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->setClickable(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->n:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->jr:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->o:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->jq:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 161
    :cond_2
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->c(Lcom/google/android/gms/games/ui/common/leaderboards/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->k:Landroid/support/v7/widget/CardView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->setClickable(Z)V

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->n:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->jv:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->o:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->ju:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->p:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/d;

    .line 176
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->b(Lcom/google/android/gms/games/ui/common/leaderboards/d;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->d(Lcom/google/android/gms/games/ui/common/leaderboards/d;)Lcom/google/android/gms/games/ui/common/leaderboards/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/e;->a()V

    .line 179
    :cond_0
    return-void
.end method
