.class public final Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;
.super Lcom/google/android/gms/plus/audience/CircleSelectionActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/plus/audience/ch;
.implements Lcom/google/android/gms/plus/f/f;


# instance fields
.field private A:Z

.field private final B:Lcom/google/android/gms/common/api/aq;

.field private final C:Lcom/google/android/gms/common/api/aq;

.field private t:Lcom/google/android/gms/plus/internal/ad;

.field private u:Lcom/google/android/gms/common/people/data/AudienceMember;

.field private v:Z

.field private w:Landroid/graphics/Bitmap;

.field private x:Lcom/google/android/gms/common/api/v;

.field private y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

.field private z:Lcom/google/android/gms/common/api/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;-><init>()V

    .line 453
    new-instance v0, Lcom/google/android/gms/plus/audience/cc;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/audience/cc;-><init>(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->B:Lcom/google/android/gms/common/api/aq;

    .line 480
    new-instance v0, Lcom/google/android/gms/plus/audience/cd;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/audience/cd;-><init>(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->C:Lcom/google/android/gms/common/api/aq;

    .line 90
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->t:Lcom/google/android/gms/plus/internal/ad;

    .line 91
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->w:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Status;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)Lcom/google/android/gms/plus/circles/AddToCircleConsentData;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    return-object p1
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    .line 286
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/audience/a/d;->b(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 289
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 290
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 291
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 293
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 294
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 295
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 296
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->A:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->A:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)Z
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->p()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->q()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->w:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private o()I
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    .line 152
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    const/4 v0, 0x0

    .line 155
    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private p()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    new-instance v0, Lcom/google/android/gms/common/audience/a/d;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/audience/a/d;-><init>(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->b(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/audience/a/d;->b(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->c(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    .line 350
    sget v1, Lcom/google/android/gms/p;->vT:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 351
    invoke-static {v1, v0}, Lcom/google/android/gms/plus/audience/ce;->a(Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/plus/audience/ce;

    move-result-object v0

    .line 353
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "errorDialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    move v0, v6

    .line 367
    :goto_0
    return v0

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->o()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/circles/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v6

    .line 364
    goto :goto_0

    .line 367
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 7

    .prologue
    .line 371
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 372
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 373
    invoke-direct {p0, v3, v4}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 375
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/plus/audience/cf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/cf;

    move-result-object v0

    .line 379
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "updateCircles"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->b()I

    .line 382
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/cf;->a()V

    .line 383
    return-void
.end method


# virtual methods
.method public final Y_()V
    .locals 0

    .prologue
    .line 530
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->c()V

    .line 531
    return-void
.end method

.method protected final synthetic a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/plus/audience/al;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->n()Lcom/google/android/gms/plus/audience/ac;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 121
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->a(Landroid/os/Bundle;)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/audience/a/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    const-string v1, "Update person ID must not be null."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    if-nez p1, :cond_1

    .line 127
    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    .line 144
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->t:Lcom/google/android/gms/plus/internal/ad;

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->o()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 148
    return-void

    .line 129
    :cond_1
    const-string v0, "hasLoggedCircleLoad"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    .line 131
    const-string v0, "addToCircleConsentData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    .line 133
    const-string v0, "addToCircleConsentDataResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "addToCircleConsentDataResultIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const-string v0, "addToCircleConsentDataResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    const-string v0, "addToCircleConsentDataResultIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/a/j;Lcom/google/android/gms/people/model/h;)V
    .locals 6

    .prologue
    .line 398
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->a(Landroid/support/v4/a/j;Lcom/google/android/gms/people/model/h;)V

    .line 400
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    if-nez v0, :cond_0

    .line 401
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/m;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/n;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    .line 406
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 58
    check-cast p2, Lcom/google/android/gms/people/model/h;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Landroid/support/v4/a/j;Lcom/google/android/gms/people/model/h;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 412
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progressDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    .line 416
    if-eqz v0, :cond_0

    .line 417
    invoke-virtual {v0}, Lcom/google/android/gms/plus/f/e;->a()V

    .line 421
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/audience/a/d;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/audience/a/d;-><init>(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/audience/a/d;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/audience/a/d;->b(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/audience/a/d;->b(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->c(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    .line 429
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Landroid/content/Intent;)V

    .line 451
    :goto_0
    return-void

    .line 433
    :cond_1
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    const/16 v2, 0x65

    if-ne v0, v2, :cond_3

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    .line 435
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 436
    sget v0, Lcom/google/android/gms/p;->vU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 445
    :goto_1
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/ce;->a(Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/plus/audience/ce;

    move-result-object v0

    .line 447
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "errorDialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    goto :goto_0

    .line 438
    :cond_2
    sget v2, Lcom/google/android/gms/p;->vV:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 443
    :cond_3
    sget v0, Lcom/google/android/gms/p;->vT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 570
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    .line 571
    return-void
.end method

.method protected final b()V
    .locals 6

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/m;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/n;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 304
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 305
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 306
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 308
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v2, v3

    if-gtz v2, :cond_1

    .line 309
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->b()V

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 314
    sget v0, Lcom/google/android/gms/p;->sD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/plus/f/a;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/f/e;

    move-result-object v0

    .line 324
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "progressDialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    if-eqz v0, :cond_5

    .line 334
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->q()V

    goto :goto_0

    .line 315
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 316
    sget v0, Lcom/google/android/gms/p;->vj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 318
    :cond_4
    sget v0, Lcom/google/android/gms/p;->vW:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 336
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->A:Z

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 545
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-nez v0, :cond_0

    .line 546
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/j;->c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->B:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 550
    :cond_0
    sget v0, Lcom/google/android/gms/j;->aa:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 551
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558
    :goto_0
    return-void

    .line 554
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, v4, v4}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->C:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 387
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/m;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/plus/a/n;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 392
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->c()V

    .line 394
    :cond_0
    return-void
.end method

.method protected final d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 535
    sget-object v0, Lcom/google/android/gms/plus/a/n;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-nez v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 565
    :cond_0
    return-void
.end method

.method protected final n()Lcom/google/android/gms/plus/audience/ac;
    .locals 4

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/plus/audience/ac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/ac;

    move-result-object v0

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x1

    .line 209
    const/16 v0, 0x7d0

    if-ne p1, v0, :cond_4

    .line 210
    if-eq p2, v1, :cond_0

    if-eq p2, v3, :cond_0

    if-ne p2, v3, :cond_2

    .line 213
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->q()V

    .line 240
    :cond_1
    :goto_0
    return-void

    .line 215
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progressDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    .line 217
    if-eqz v0, :cond_3

    .line 218
    invoke-virtual {v0}, Lcom/google/android/gms/plus/f/e;->a()V

    .line 220
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->c()V

    goto :goto_0

    .line 222
    :cond_4
    if-ne p1, v3, :cond_6

    .line 223
    if-eqz p3, :cond_5

    new-instance v0, Lcom/google/android/gms/common/audience/a/h;

    invoke-direct {v0, p3}, Lcom/google/android/gms/common/audience/a/h;-><init>(Landroid/content/Intent;)V

    iget v1, v0, Lcom/google/android/gms/common/audience/a/h;->b:I

    if-ne v1, v3, :cond_5

    iget v1, v0, Lcom/google/android/gms/common/audience/a/h;->c:I

    if-ne v1, v3, :cond_5

    new-instance v1, Lcom/google/android/gms/common/audience/a/d;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/audience/a/d;-><init>(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/audience/a/d;->a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    new-array v3, v3, [Lcom/google/android/gms/common/people/data/AudienceMember;

    const/4 v4, 0x0

    iget-object v0, v0, Lcom/google/android/gms/common/audience/a/h;->a:Lcom/google/android/gms/common/people/data/AudienceMember;

    aput-object v0, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/audience/a/d;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->b(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/audience/a/d;->b(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->c(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->j()V

    goto :goto_0

    .line 224
    :cond_6
    if-nez p1, :cond_1

    .line 226
    if-eq p2, v1, :cond_7

    .line 227
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 230
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 232
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/j;->c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->B:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 237
    :cond_8
    sget-object v0, Lcom/google/android/gms/people/x;->h:Lcom/google/android/gms/people/ai;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/ai;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    goto/16 :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->bb:I

    if-ne v0, v1, :cond_1

    .line 174
    new-instance v0, Lcom/google/android/gms/common/audience/a/f;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/audience/a/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/f;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.gms.common.audience.EXTRA_PAGE_ID"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->u:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/f;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.gms.common.audience.EXTRA_TARGET_PERSON"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->ac:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const-string v3, "com.google.android.gms.common.acl.EXTRA_HEADER_TEXT_COLOR"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/f;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.gms.common.audience.EXTRA_HEADER_TEXT_COLOR"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->aa:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const-string v3, "com.google.android.gms.common.acl.EXTRA_HEADER_BACKGROUND_COLOR"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/f;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.gms.common.audience.EXTRA_HEADER_BACKGROUND_COLOR"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, v0, Lcom/google/android/gms/common/audience/a/f;->a:Landroid/content/Intent;

    if-eqz v0, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_2

    iput-boolean v4, p0, Lcom/google/android/gms/plus/audience/aw;->k:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v1, v4, v4}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->smoothScrollTo(II)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 176
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onClick(Landroid/view/View;)V

    .line 177
    return-void

    .line 174
    :cond_2
    new-instance v1, Lcom/google/android/gms/plus/audience/cb;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/plus/audience/cb;-><init>(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;Landroid/content/Intent;)V

    invoke-super {p0, v1}, Lcom/google/android/gms/plus/audience/aw;->a(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 268
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 269
    const-string v0, "hasLoggedCircleLoad"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 270
    const-string v0, "addToCircleConsentData"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->y:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    if-eqz v0, :cond_0

    .line 272
    const-string v0, "addToCircleConsentDataResultCode"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 274
    const-string v0, "addToCircleConsentDataResultIntent"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->z:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->i()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 278
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onStart()V

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 163
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->x:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 168
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->onStop()V

    .line 169
    return-void
.end method
