.class public Lcom/google/android/gms/auth/login/BrowserActivity;
.super Lcom/google/android/gms/auth/login/g;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/android/setupwizard/navigationbar/a;


# static fields
.field public static q:Ljava/lang/String;

.field public static r:Lcom/google/android/gms/auth/login/BrowserActivity;

.field private static t:Ljava/lang/Object;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/util/Map;

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Landroid/webkit/CookieManager;

.field private volatile H:Z

.field private I:Landroid/content/IntentFilter;

.field private J:Lcom/google/android/gms/auth/login/at;

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Lcom/google/android/gms/auth/login/aw;

.field private P:Z

.field private Q:Ljava/util/ArrayList;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Lcom/google/android/gms/auth/login/ag;

.field private U:Z

.field private V:Lcom/google/android/gms/auth/login/f;

.field private W:Z

.field private X:Lcom/google/android/gms/auth/login/ax;

.field private Y:Landroid/content/ServiceConnection;

.field private Z:Landroid/widget/Button;

.field private aa:Ljava/lang/String;

.field private ab:Lcom/google/android/setupwizard/util/b;

.field private ac:Landroid/view/View;

.field private ad:Landroid/os/AsyncTask;

.field s:Lcom/google/android/gms/auth/login/CustomWebView;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:Z

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const-string v0, "https://accounts.google.com/o/android/auth?"

    sput-object v0, Lcom/google/android/gms/auth/login/BrowserActivity;->q:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/g;-><init>()V

    .line 183
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->D:Z

    .line 186
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->E:Z

    .line 189
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->F:Z

    .line 194
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->H:Z

    .line 197
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->I:Landroid/content/IntentFilter;

    .line 200
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->K:Z

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->L:Z

    .line 202
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->M:Z

    .line 203
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->N:Z

    .line 205
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->P:Z

    .line 206
    iput-object v3, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Q:Ljava/util/ArrayList;

    .line 207
    iput-object v3, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->R:Ljava/lang/String;

    .line 208
    iput-object v3, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->S:Ljava/lang/String;

    .line 210
    iput-object v3, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->T:Lcom/google/android/gms/auth/login/ag;

    .line 211
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->U:Z

    .line 216
    new-instance v0, Lcom/google/android/gms/auth/login/q;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/q;-><init>(Lcom/google/android/gms/auth/login/BrowserActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->X:Lcom/google/android/gms/auth/login/ax;

    .line 223
    new-instance v0, Lcom/google/android/gms/auth/login/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/r;-><init>(Lcom/google/android/gms/auth/login/BrowserActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Y:Landroid/content/ServiceConnection;

    .line 1107
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 152
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/login/BrowserActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 153
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string v1, "access_token"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    const-string v1, "creating_account"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 157
    const-string v1, "is_minute_maid"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 158
    const-string v1, "is_frp"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 159
    const-string v1, "allowed_domains"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 160
    invoke-static {p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 161
    const-string v1, "purchaser_email"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    :cond_0
    invoke-static {p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 164
    const-string v1, "purchaser_name"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    :cond_1
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/BrowserActivity;Lcom/google/android/gms/auth/login/ag;)Lcom/google/android/gms/auth/login/ag;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->T:Lcom/google/android/gms/auth/login/ag;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/BrowserActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->u:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 240
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->u:Ljava/lang/String;

    .line 241
    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->v:Ljava/lang/String;

    .line 242
    const-string v0, "access_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->w:Ljava/lang/String;

    .line 243
    const-string v0, "creating_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->x:Z

    .line 244
    const-string v0, "is_minute_maid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    .line 245
    const-string v0, "is_frp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->N:Z

    .line 246
    const-string v0, "allowed_domains"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Q:Ljava/util/ArrayList;

    .line 247
    const-string v0, "purchaser_email"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->R:Ljava/lang/String;

    .line 248
    const-string v0, "purchaser_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->S:Ljava/lang/String;

    .line 249
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/BrowserActivity;Z)V
    .locals 1

    .prologue
    .line 93
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/auth/login/t;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/login/t;-><init>(Lcom/google/android/gms/auth/login/BrowserActivity;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method static a(Ljava/lang/String;Lcom/google/android/gms/auth/login/f;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 773
    if-nez p0, :cond_1

    .line 782
    :cond_0
    :goto_0
    return v0

    .line 778
    :cond_1
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URI;->getPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 782
    if-eqz v1, :cond_0

    invoke-virtual {p1, p0}, Lcom/google/android/gms/auth/login/f;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "/EmbeddedSetup"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 780
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/login/BrowserActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->v:Ljava/lang/String;

    return-object v0
.end method

.method static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 791
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 792
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Path: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 797
    :goto_0
    return-object p0

    .line 794
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/login/BrowserActivity;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->U:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/gms/auth/login/BrowserActivity;)Landroid/webkit/CookieManager;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->G:Landroid/webkit/CookieManager;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/auth/login/BrowserActivity;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->W:Z

    return p1
.end method

.method public static d(Ljava/lang/String;)Lcom/google/android/gms/auth/login/v;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 968
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 971
    if-eqz v1, :cond_3

    .line 972
    const-string v2, "\\;"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v3, v4

    move-object v1, v0

    :goto_0
    if-ge v3, v6, :cond_4

    aget-object v2, v5, v3

    .line 973
    const/16 v7, 0x3d

    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 974
    if-ltz v7, :cond_2

    .line 975
    invoke-virtual {v2, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 976
    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 977
    const-string v7, "oauth_token"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    move-object v1, v2

    .line 980
    :cond_0
    const-string v7, "user_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    move-object v0, v2

    .line 983
    :cond_1
    if-eqz v1, :cond_2

    if-nez v0, :cond_4

    .line 984
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 989
    :cond_4
    new-instance v2, Lcom/google/android/gms/auth/login/v;

    invoke-direct {v2, v1, v0, v4}, Lcom/google/android/gms/auth/login/v;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    return-object v2
.end method

.method static synthetic d(Lcom/google/android/gms/auth/login/BrowserActivity;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/auth/login/BrowserActivity;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->H:Z

    return p1
.end method

.method static synthetic e(Lcom/google/android/gms/auth/login/BrowserActivity;)Lcom/google/android/gms/auth/login/f;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->V:Lcom/google/android/gms/auth/login/f;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/auth/login/BrowserActivity;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->C:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/auth/login/BrowserActivity;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->U:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/gms/auth/login/BrowserActivity;)Lcom/google/android/gms/auth/login/ag;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->T:Lcom/google/android/gms/auth/login/ag;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/auth/login/BrowserActivity;)Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->K:Z

    return v0
.end method

.method static synthetic j(Lcom/google/android/gms/auth/login/BrowserActivity;)Lcom/google/android/setupwizard/util/b;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ab:Lcom/google/android/setupwizard/util/b;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/auth/login/BrowserActivity;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->K:Z

    return v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ab:Lcom/google/android/setupwizard/util/b;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ab:Lcom/google/android/setupwizard/util/b;

    sget v1, Lcom/google/android/gms/p;->bw:I

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/b;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ac:Landroid/view/View;

    .line 475
    :cond_0
    return-void
.end method

.method static synthetic l(Lcom/google/android/gms/auth/login/BrowserActivity;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->H:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/gms/auth/login/BrowserActivity;)Lcom/google/android/gms/auth/login/aw;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    return-object v0
.end method

.method private m()Z
    .locals 3

    .prologue
    .line 711
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 712
    const-string v1, "com.google.android.androidforwork"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 713
    const-string v1, "android.software.managed_users"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 715
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 510
    const-string v0, "device_country"

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 516
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "&lang="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 517
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&langCountry="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->A:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 518
    iget-boolean v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->B:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 519
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&hl="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 521
    :cond_0
    new-instance v2, Landroid/content/res/Configuration;

    invoke-direct {v2}, Landroid/content/res/Configuration;-><init>()V

    .line 522
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/provider/Settings$System;->getConfiguration(Landroid/content/ContentResolver;Landroid/content/res/Configuration;)V

    .line 523
    iget v3, v2, Landroid/content/res/Configuration;->mcc:I

    if-eqz v3, :cond_1

    .line 524
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&mcc="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v2, Landroid/content/res/Configuration;->mcc:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 526
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&xoauth_display_name=Android%20Phone"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 527
    if-eqz v1, :cond_2

    .line 528
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&cc="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 530
    :cond_2
    if-nez p2, :cond_3

    sget-object p2, Lcom/google/android/gms/auth/login/BrowserActivity;->q:Ljava/lang/String;

    .line 531
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&source=android"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 532
    if-eqz p1, :cond_c

    .line 533
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&Email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 534
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&tmpl=reauth"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 541
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->P:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->n:Z

    if-eqz v1, :cond_4

    .line 542
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&afw=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 544
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Q:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    .line 545
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&domains="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 546
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Q:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 548
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/aw;->hasTelephony()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/aw;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 551
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&ph="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/aw;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 553
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->n:Z

    if-eqz v1, :cond_7

    .line 554
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&is_setup_wizard=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 555
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->N:Z

    if-eqz v1, :cond_7

    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&isFrp=1&return_user_id=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 559
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->o:Z

    if-eqz v1, :cond_8

    .line 560
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&use_immersive_mode=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 564
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->R:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 565
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&pEmail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->R:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 567
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->S:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 568
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&pName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->S:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 571
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v1, :cond_d

    new-instance v1, Lcom/google/android/gms/auth/frp/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/frp/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/frp/a;->a()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 572
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&canFrp=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 575
    :goto_1
    sget-object v0, Lcom/google/android/gms/auth/b/a;->M:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 576
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 577
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&d="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 579
    :cond_b
    return-object v1

    .line 536
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&tmpl=new_account"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_d
    move-object v1, v0

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1075
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->setResult(I)V

    .line 1076
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->finish()V

    .line 1077
    return-void
.end method

.method public final a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 1066
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->o:Z

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a(ZZ)V

    .line 1067
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->n:Z

    if-nez v0, :cond_0

    .line 1068
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1070
    :cond_0
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1071
    return-void
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/k;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 896
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->u:Ljava/lang/String;

    const/4 v1, 0x0

    iget-boolean v3, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->x:Z

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Q:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    move v5, v4

    :goto_0
    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/k;ZZZ)Landroid/content/Intent;

    move-result-object v0

    .line 903
    const/16 v1, 0x3f1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/BrowserActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 904
    return-void

    .line 896
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1082
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 920
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->G:Landroid/webkit/CookieManager;

    invoke-static {p1}, Lcom/google/android/gms/auth/login/BrowserActivity;->d(Ljava/lang/String;)Lcom/google/android/gms/auth/login/v;

    move-result-object v1

    .line 921
    iget-object v0, v1, Lcom/google/android/gms/auth/login/v;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 922
    iput-boolean v3, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->D:Z

    .line 928
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->D:Z

    if-eqz v0, :cond_4

    .line 935
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ab:Lcom/google/android/setupwizard/util/b;

    if-eqz v0, :cond_1

    .line 936
    sget v0, Lcom/google/android/gms/j;->qx:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ab:Lcom/google/android/setupwizard/util/b;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 939
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->E:Z

    if-nez v0, :cond_4

    .line 940
    iput-boolean v3, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->E:Z

    .line 941
    iget-object v0, v1, Lcom/google/android/gms/auth/login/v;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->w:Ljava/lang/String;

    .line 942
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/login/CustomWebView;->setVisibility(I)V

    .line 943
    new-instance v0, Lcom/google/android/gms/auth/login/aa;

    invoke-direct {v0}, Lcom/google/android/gms/auth/login/aa;-><init>()V

    iget-object v2, v1, Lcom/google/android/gms/auth/login/v;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/auth/login/aa;->a:Landroid/os/Bundle;

    const-string v4, "authorization_code"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/android/gms/auth/login/v;->b:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/auth/login/aa;->a:Landroid/os/Bundle;

    const-string v3, "obfuscated_gaia_id"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v1, :cond_3

    .line 947
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/login/aw;->a:Z

    iget-object v2, v0, Lcom/google/android/gms/auth/login/aa;->a:Landroid/os/Bundle;

    const-string v3, "is_terms_of_services_newly_accepted"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 949
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Q:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 950
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Q:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v2, v0, Lcom/google/android/gms/auth/login/aa;->a:Landroid/os/Bundle;

    const-string v3, "allowed_domains"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 952
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/login/aw;->b:Z

    iget-object v2, v0, Lcom/google/android/gms/auth/login/aa;->a:Landroid/os/Bundle;

    const-string v3, "is_new_account"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 955
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->U:Z

    if-eqz v1, :cond_3

    .line 956
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->m:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v2, "GLSActivity"

    const-string v3, "Unbinding MinuteMaidDgService..."

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Y:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/BrowserActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 958
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->U:Z

    .line 961
    :cond_3
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v0, v0, Lcom/google/android/gms/auth/login/aa;->a:Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 962
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->finish()V

    .line 965
    :cond_4
    return-void
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1040
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    if-eqz v1, :cond_2

    .line 1045
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->W:Z

    if-nez v1, :cond_0

    .line 1060
    :goto_0
    return v0

    .line 1049
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/CustomWebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1050
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/CustomWebView;->goBack()V

    goto :goto_0

    .line 1054
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v0, :cond_2

    .line 1055
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->setResult(I)V

    .line 1056
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->finish()V

    .line 1060
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/p;->dp:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/gms/p;->dq:I

    goto :goto_0
.end method

.method protected final h()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 908
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/g;->onActivityResult(IILandroid/content/Intent;)V

    .line 910
    if-ne p2, v0, :cond_0

    .line 911
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->setResult(I)V

    .line 915
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->finish()V

    .line 916
    return-void

    .line 913
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->setResult(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 262
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Z:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 263
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 264
    const-string v1, "errorCode"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 265
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 266
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->finish()V

    .line 268
    :cond_0
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown click action for view "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 462
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ab:Lcom/google/android/setupwizard/util/b;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ab:Lcom/google/android/setupwizard/util/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ac:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/b;->removeView(Landroid/view/View;)V

    .line 466
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->l()V

    .line 468
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 276
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onCreate(Landroid/os/Bundle;)V

    .line 278
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 279
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->z:Ljava/lang/String;

    .line 280
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->A:Ljava/lang/String;

    .line 281
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->B:Ljava/lang/String;

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->B:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->A:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->B:Ljava/lang/String;

    .line 288
    :cond_0
    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 289
    const-string v1, "Accept-Language"

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->B:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->C:Ljava/util/Map;

    .line 295
    :goto_0
    sget-object v0, Lcom/google/android/gms/auth/b/a;->N:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/login/f;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/login/f;-><init>(Ljava/util/regex/Pattern;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->V:Lcom/google/android/gms/auth/login/f;

    .line 296
    sget v0, Lcom/google/android/gms/l;->m:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->setContentView(I)V

    .line 298
    if-eqz p1, :cond_7

    .line 299
    const-string v0, "interactivity_completed"

    invoke-virtual {p1, v0, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->D:Z

    .line 300
    const-string v0, "waiting_for_network"

    invoke-virtual {p1, v0, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->F:Z

    .line 301
    const-string v0, "background_task_started"

    invoke-virtual {p1, v0, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->E:Z

    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/BrowserActivity;->a(Landroid/os/Bundle;)V

    .line 308
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v0, :cond_2

    .line 309
    new-instance v0, Lcom/google/android/setupwizard/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ab:Lcom/google/android/setupwizard/util/b;

    .line 310
    sget v0, Lcom/google/android/gms/j;->qx:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ab:Lcom/google/android/setupwizard/util/b;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 311
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->l()V

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->aa:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 315
    sget v0, Lcom/google/android/gms/p;->bw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->aa:Ljava/lang/String;

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->aa:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 319
    :cond_2
    sget v0, Lcom/google/android/gms/j;->F:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/google/android/gms/j;->uh:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/login/CustomWebView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/auth/login/CustomWebView;->clearCache(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->n:Z

    if-nez v0, :cond_8

    move v0, v7

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/login/CustomWebView;->a(Z)V

    sget v0, Lcom/google/android/gms/j;->sR:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/CustomWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/CustomWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MinuteMaid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/auth/login/u;

    invoke-direct {v4, p0}, Lcom/google/android/gms/auth/login/u;-><init>(Lcom/google/android/gms/auth/login/BrowserActivity;)V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    const/4 v0, 0x6

    iput v0, v1, Landroid/os/Message;->arg1:I

    sget-object v0, Lcom/google/android/gms/auth/b/a;->L:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v4, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    const-string v2, ""

    :goto_3
    new-instance v0, Lcom/google/android/gms/auth/login/aw;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    iget-boolean v5, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->N:Z

    iget-object v6, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->m:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/auth/login/aw;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/webkit/WebView;Landroid/os/Handler;ZLcom/google/android/gms/auth/firstparty/shared/LatencyTracker;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->X:Lcom/google/android/gms/auth/login/ax;

    iput-object v1, v0, Lcom/google/android/gms/auth/login/aw;->c:Lcom/google/android/gms/auth/login/ax;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Q:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/google/android/gms/auth/login/aw;->d:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->m()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->P:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->O:Lcom/google/android/gms/auth/login/aw;

    const-string v2, "mm"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/login/CustomWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    iput-boolean v7, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->H:Z

    new-instance v0, Lcom/google/android/gms/auth/login/at;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/login/at;-><init>(Landroid/webkit/WebView;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->J:Lcom/google/android/gms/auth/login/at;

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->n:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gms/auth/b/a;->I:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/gms/auth/login/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/y;-><init>(Landroid/app/Activity;)V

    :cond_3
    sget-object v0, Lcom/google/android/gms/auth/b/a;->J:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    :cond_4
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->G:Landroid/webkit/CookieManager;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    new-instance v1, Lcom/google/android/gms/auth/login/x;

    invoke-direct {v1, p0, v8}, Lcom/google/android/gms/auth/login/x;-><init>(Lcom/google/android/gms/auth/login/BrowserActivity;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/CustomWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    new-instance v1, Lcom/google/android/gms/auth/login/w;

    invoke-direct {v1, p0, v8}, Lcom/google/android/gms/auth/login/w;-><init>(Lcom/google/android/gms/auth/login/BrowserActivity;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/CustomWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/CustomWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setLightTouchEnabled(Z)V

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setNeedInitialFocus(Z)V

    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/auth/login/CustomWebView;->setMapTrackballToArrowKeys(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/auth/login/CustomWebView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/auth/login/CustomWebView;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/CustomWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    sget-object v0, Lcom/google/android/gms/auth/login/BrowserActivity;->t:Ljava/lang/Object;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->s:Lcom/google/android/gms/auth/login/CustomWebView;

    sget-object v1, Lcom/google/android/gms/auth/login/BrowserActivity;->t:Ljava/lang/Object;

    const-string v2, "gls"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/login/CustomWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_5
    sput-object p0, Lcom/google/android/gms/auth/login/BrowserActivity;->r:Lcom/google/android/gms/auth/login/BrowserActivity;

    .line 321
    new-instance v0, Lcom/google/android/gms/auth/login/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/s;-><init>(Lcom/google/android/gms/auth/login/BrowserActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ad:Landroid/os/AsyncTask;

    .line 356
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ad:Landroid/os/AsyncTask;

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 357
    return-void

    .line 292
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->C:Ljava/util/Map;

    goto/16 :goto_0

    .line 305
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 306
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->a(Landroid/os/Bundle;)V

    goto/16 :goto_1

    :cond_8
    move v0, v8

    .line 319
    goto/16 :goto_2

    :cond_9
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 372
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-nez v1, :cond_0

    .line 373
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 375
    sget v1, Lcom/google/android/gms/p;->bo:I

    invoke-interface {p1, v0, v0, v0, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 377
    sget v2, Lcom/google/android/gms/h;->i:I

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 378
    invoke-static {v1}, Lcom/google/android/gms/auth/a/b;->a(Landroid/view/MenuItem;)V

    .line 381
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 361
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onDestroy()V

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ad:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ad:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 364
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->ad:Landroid/os/AsyncTask;

    .line 366
    :cond_0
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 889
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 386
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 392
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 388
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->setResult(I)V

    .line 389
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/BrowserActivity;->finish()V

    goto :goto_0

    .line 386
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 1015
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onPause()V

    .line 1016
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v0, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->J:Lcom/google/android/gms/auth/login/at;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1019
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 1004
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onResume()V

    .line 1005
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v0, :cond_0

    .line 1006
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->J:Lcom/google/android/gms/auth/login/at;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->I:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/BrowserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1008
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 442
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 443
    const-string v0, "access_token"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v0, "creating_account"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 446
    const-string v0, "is_minute_maid"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 447
    const-string v0, "is_frp"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->N:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 448
    const-string v0, "allowed_domains"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Q:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 449
    const-string v0, "purchaser_email"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->R:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const-string v0, "purchaser_name"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->S:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    const-string v0, "url"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const-string v0, "interactivity_completed"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 454
    const-string v0, "waiting_for_network"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->F:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 455
    const-string v0, "background_task_started"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 456
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 994
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onStart()V

    .line 995
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->y:Z

    if-eqz v0, :cond_0

    .line 996
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->m:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v1, "GLSActivity"

    const-string v2, "Binding MinuteMaidDgService..."

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/MinuteMaidDgService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 998
    iget-object v1, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Y:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/auth/login/BrowserActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 1000
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 1026
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onStop()V

    .line 1027
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->U:Z

    if-eqz v0, :cond_0

    .line 1028
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->m:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v1, "GLSActivity"

    const-string v2, "Unbinding MinuteMaidDgService..."

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1029
    iget-object v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->Y:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/BrowserActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1030
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/BrowserActivity;->U:Z

    .line 1032
    :cond_0
    return-void
.end method
