.class public final Lcom/google/android/gms/appdatasearch/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lcom/google/android/gms/appdatasearch/Feature;)D
    .locals 2

    .prologue
    .line 133
    const-string v0, "factor"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    if-nez v0, :cond_0

    .line 135
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 137
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static a()Lcom/google/android/gms/appdatasearch/Feature;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lcom/google/android/gms/appdatasearch/Feature;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(I)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(I)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D
    .locals 2

    .prologue
    .line 105
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(I)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    .line 106
    if-nez v0, :cond_0

    .line 107
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 109
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aw;->a(Lcom/google/android/gms/appdatasearch/Feature;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static b()Lcom/google/android/gms/appdatasearch/Feature;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/google/android/gms/appdatasearch/Feature;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(I)V

    return-object v0
.end method

.method public static c(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D
    .locals 2

    .prologue
    .line 124
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(I)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    .line 125
    if-nez v0, :cond_0

    .line 126
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 128
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aw;->a(Lcom/google/android/gms/appdatasearch/Feature;)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static c()Lcom/google/android/gms/appdatasearch/Feature;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lcom/google/android/gms/appdatasearch/Feature;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(I)V

    return-object v0
.end method
