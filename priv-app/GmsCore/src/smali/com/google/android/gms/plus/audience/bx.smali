.class final Lcom/google/android/gms/plus/audience/bx;
.super Lcom/google/android/gms/plus/audience/o;
.source "SourceFile"


# instance fields
.field final synthetic e:Lcom/google/android/gms/plus/audience/bw;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/audience/bw;Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 419
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    .line 420
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 423
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 2

    .prologue
    .line 454
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/audience/o;->a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 455
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/bw;->a(Lcom/google/android/gms/plus/audience/bw;)Landroid/widget/CheckBox;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/bw;->d()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 456
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 460
    :cond_0
    :goto_0
    return-object v0

    .line 457
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/bw;->d()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v1

    if-nez v1, :cond_0

    .line 458
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/people/model/d;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
    .locals 3

    .prologue
    .line 429
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/people/model/d;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;

    move-result-object v0

    .line 432
    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->f()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 433
    sget v1, Lcom/google/android/gms/p;->sT:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c(I)V

    .line 439
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/bw;->a(Lcom/google/android/gms/plus/audience/bw;)Landroid/widget/CheckBox;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/bw;->d()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 440
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->setVisibility(I)V

    .line 444
    :cond_0
    :goto_1
    return-object v0

    .line 435
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c()V

    goto :goto_0

    .line 441
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/bw;->d()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v1

    if-nez v1, :cond_0

    .line 442
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/common/data/d;)V
    .locals 4

    .prologue
    .line 465
    new-instance v0, Lcom/google/android/gms/common/data/aa;

    invoke-static {}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->q()Ljava/util/Comparator;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/common/data/aa;-><init>(Lcom/google/android/gms/common/data/d;Ljava/util/Comparator;)V

    invoke-super {p0, v0}, Lcom/google/android/gms/plus/audience/o;->b(Lcom/google/android/gms/common/data/d;)V

    .line 467
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/bw;->a(Lcom/google/android/gms/plus/audience/bw;Ljava/util/HashSet;)Ljava/util/HashSet;

    .line 468
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 469
    invoke-interface {p1, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/d;

    .line 470
    iget-object v2, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    invoke-static {v2}, Lcom/google/android/gms/plus/audience/bw;->b(Lcom/google/android/gms/plus/audience/bw;)Ljava/util/HashSet;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/gms/people/model/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 468
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bx;->e:Lcom/google/android/gms/plus/audience/bw;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/bw;->c(Lcom/google/android/gms/plus/audience/bw;)V

    .line 475
    return-void
.end method
