.class public final Lcom/google/android/gms/googlehelp/c/a;
.super Landroid/widget/TextView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/ad/a/a/h;

.field private final c:Lcom/google/android/gms/googlehelp/c/n;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/ad/a/a/h;Lcom/google/android/gms/googlehelp/c/n;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 50
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 51
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/a;->a:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/a;->b:Lcom/google/ad/a/a/h;

    .line 53
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/c/a;->c:Lcom/google/android/gms/googlehelp/c/n;

    .line 55
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->ba:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 60
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    sget v0, Lcom/google/android/gms/p;->oC:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/a;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->I:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/a;->setTextColor(I)V

    .line 64
    invoke-static {p0, p0}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;Landroid/view/View$OnClickListener;)V

    .line 65
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/gms/googlehelp/c/m;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a;->a:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/c/a;->b:Lcom/google/ad/a/a/h;

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/c/a;->c:Lcom/google/android/gms/googlehelp/c/n;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/c/m;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;Lcom/google/android/gms/googlehelp/c/n;Z)V

    .line 71
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 73
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 74
    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/m;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a;->c:Lcom/google/android/gms/googlehelp/c/n;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/c/n;->a(Landroid/widget/EditText;)V

    .line 77
    return-void
.end method
