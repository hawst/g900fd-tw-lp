.class public Lcom/google/android/gms/reminders/notification/NotificationService;
.super Lcom/google/android/gms/common/app/d;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Landroid/content/ContentResolver;

.field private c:Landroid/app/AlarmManager;

.field private d:Landroid/app/NotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "trigger_time"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "create_time"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "archived"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "deleted"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "due_date_year"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "due_date_month"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "due_date_day"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "due_date_hour"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "due_date_minute"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "due_date_second"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "due_date_period"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "due_date_absolute_time_ms"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "task_list"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/reminders/notification/NotificationService;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 96
    const-string v0, "NotificationService"

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/app/d;-><init>(Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 362
    const-string v0, "com.google.android.gms.reminders.PROVIDER_CHANGE"

    invoke-static {p0, v0}, Lcom/google/android/gms/reminders/notification/NotificationService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 366
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/reminders/notification/NotificationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 367
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 368
    return-object v0
.end method

.method private a()V
    .locals 14

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/notification/NotificationService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/reminders/internal/a/h;->b:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/reminders/notification/NotificationService;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 124
    if-nez v7, :cond_0

    .line 183
    :goto_0
    return-void

    .line 129
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 130
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->c:Landroid/app/AlarmManager;

    invoke-direct {p0, v8, v9}, Lcom/google/android/gms/reminders/notification/NotificationService;->b(J)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 132
    const/4 v0, 0x4

    invoke-static {v7, v0}, Lcom/google/android/gms/reminders/c/c;->b(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    .line 133
    if-nez v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->b:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/gms/reminders/internal/a/h;->a:Landroid/net/Uri;

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 182
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    .line 139
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 140
    const/4 v0, 0x3

    invoke-static {v7, v0}, Lcom/google/android/gms/reminders/c/c;->b(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 147
    :cond_2
    const/16 v0, 0xd

    invoke-static {v7, v0}, Lcom/google/android/gms/reminders/c/c;->b(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    .line 152
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/reminders/c/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 158
    const-string v0, "is_stale"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 159
    const/4 v0, 0x2

    invoke-static {v7, v0}, Lcom/google/android/gms/reminders/c/c;->a(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_3

    .line 160
    const-string v0, "create_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 163
    :cond_3
    invoke-static {v7}, Lcom/google/android/gms/reminders/notification/NotificationService;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 164
    invoke-static {v7}, Lcom/google/android/gms/reminders/notification/NotificationService;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    move-object v1, v0

    .line 165
    :goto_2
    const/4 v0, 0x1

    invoke-static {v7, v0}, Lcom/google/android/gms/reminders/c/c;->a(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v0

    .line 166
    if-eq v0, v1, :cond_4

    .line 167
    const-string v0, "trigger_time"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 171
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/reminders/b/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    sub-long/2addr v4, v12

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    .line 173
    const-string v0, "state"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 174
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v8, v9}, Lcom/google/android/gms/reminders/notification/NotificationService;->b(J)Landroid/app/PendingIntent;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Schedule reminder "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " at time "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    const/16 v3, 0x13

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->c:Landroid/app/AlarmManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 178
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->b:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/gms/reminders/internal/a/h;->a:Landroid/net/Uri;

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v10, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    .line 164
    :cond_6
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    const/4 v1, 0x5

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x6

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x7

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/16 v4, 0x8

    invoke-interface {v7, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_7

    const/16 v4, 0x9

    invoke-interface {v7, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_7

    const/16 v4, 0xa

    invoke-interface {v7, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_7

    const/16 v4, 0x8

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v5, 0x9

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/16 v6, 0xa

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    :goto_4
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_2

    :cond_7
    const/16 v4, 0xb

    invoke-interface {v7, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_8

    const/16 v4, 0xb

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    const/4 v4, 0x0

    :goto_5
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    goto :goto_4

    :pswitch_0
    sget-object v4, Lcom/google/android/gms/reminders/b/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_5

    :pswitch_1
    sget-object v4, Lcom/google/android/gms/reminders/b/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_5

    :pswitch_2
    sget-object v4, Lcom/google/android/gms/reminders/b/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_5

    :pswitch_3
    sget-object v4, Lcom/google/android/gms/reminders/b/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_5

    :cond_8
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    goto :goto_4

    .line 174
    :cond_9
    iget-object v3, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->c:Landroid/app/AlarmManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    .line 182
    :cond_a
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(J)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 286
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 287
    const-string v1, "state"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 289
    iget-object v1, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->b:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/gms/reminders/internal/a/h;->a:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 291
    return-void
.end method

.method private static a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/reminders/model/Task;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 233
    invoke-interface {p1}, Lcom/google/android/gms/reminders/model/Task;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/TaskList;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/reminders/c/a;->b(I)Ljava/lang/String;

    move-result-object v2

    .line 236
    invoke-static {p0, v2}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 277
    :goto_0
    return v0

    .line 241
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.reminders.BIND_LISTENER"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/notification/NotificationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 243
    if-nez v0, :cond_1

    move v0, v1

    .line 246
    goto :goto_0

    .line 249
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Matched services: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    .line 250
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 251
    invoke-static {}, Lcom/google/android/gms/reminders/packagemanager/a;->a()Lcom/google/android/gms/reminders/packagemanager/a;

    move-result-object v3

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->resolvePackageName:Ljava/lang/String;

    invoke-virtual {v3, p0, v4}, Lcom/google/android/gms/reminders/packagemanager/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 253
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 257
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 261
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 262
    const-string v3, "com.google.android.gms.reminders.BIND_LISTENER"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    new-instance v3, Lcom/google/android/gms/common/b;

    invoke-direct {v3}, Lcom/google/android/gms/common/b;-><init>()V

    .line 265
    invoke-virtual {p0, v0, v3, v1}, Lcom/google/android/gms/reminders/notification/NotificationService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 268
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/reminders/internal/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/reminders/internal/e;

    move-result-object v0

    .line 269
    new-instance v4, Lcom/google/android/gms/reminders/model/TaskEntity;

    invoke-direct {v4, p1}, Lcom/google/android/gms/reminders/model/TaskEntity;-><init>(Lcom/google/android/gms/reminders/model/Task;)V

    invoke-interface {v0, v4}, Lcom/google/android/gms/reminders/internal/e;->a(Lcom/google/android/gms/reminders/model/TaskEntity;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    invoke-virtual {p0, v3}, Lcom/google/android/gms/reminders/notification/NotificationService;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_1

    .line 270
    :catch_0
    move-exception v0

    .line 271
    const/4 v4, 0x1

    :try_start_1
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->d()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    invoke-virtual {p0, v3}, Lcom/google/android/gms/reminders/notification/NotificationService;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_1

    .line 272
    :catch_1
    move-exception v0

    .line 273
    const/4 v4, 0x1

    :try_start_2
    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->d()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 275
    invoke-virtual {p0, v3}, Lcom/google/android/gms/reminders/notification/NotificationService;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v3}, Lcom/google/android/gms/reminders/notification/NotificationService;->unbindService(Landroid/content/ServiceConnection;)V

    throw v0

    :cond_3
    move v0, v1

    .line 277
    goto/16 :goto_0
.end method

.method private b(J)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 315
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 316
    const-class v1, Lcom/google/android/gms/reminders/notification/NotificationReceiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 317
    const-string v1, "com.google.android.gms.reminders.POST_NOTIFICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    sget-object v1, Lcom/google/android/gms/reminders/internal/a/h;->a:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 319
    const-string v1, "notification_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 320
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 109
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 110
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Action:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    .line 111
    const-string v3, "com.google.android.gms.reminders.PROVIDER_CHANGE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 112
    invoke-direct {p0}, Lcom/google/android/gms/reminders/notification/NotificationService;->a()V

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    const-string v3, "com.google.android.gms.reminders.POST_NOTIFICATION"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    const-string v2, "notification_id"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Post notification "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-ltz v4, :cond_0

    sget-object v4, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {p0, v4, v0, v0}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/reminders/model/e;

    invoke-direct {v5, v4}, Lcom/google/android/gms/reminders/model/e;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/gms/reminders/model/e;->c()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-gtz v4, :cond_2

    invoke-virtual {v5}, Lcom/google/android/gms/reminders/model/e;->w_()V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {v5, v4}, Lcom/google/android/gms/reminders/model/e;->b(I)Lcom/google/android/gms/reminders/model/Task;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/reminders/model/Task;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/reminders/model/TaskList;->a()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/reminders/c/a;->a(I)Z

    move-result v6

    if-nez v6, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown task list id "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Lcom/google/android/gms/reminders/model/Task;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/reminders/model/TaskList;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/reminders/notification/NotificationService;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v5}, Lcom/google/android/gms/reminders/model/e;->w_()V

    goto :goto_0

    :cond_3
    :try_start_2
    invoke-direct {p0, v4}, Lcom/google/android/gms/reminders/notification/NotificationService;->a(Lcom/google/android/gms/reminders/model/Task;)Z

    move-result v6

    if-eqz v6, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Handled by client "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/reminders/notification/NotificationService;->a(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v5}, Lcom/google/android/gms/reminders/model/e;->w_()V

    goto/16 :goto_0

    :cond_4
    :try_start_3
    invoke-interface {v4}, Lcom/google/android/gms/reminders/model/Task;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/reminders/model/TaskList;->a()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/reminders/c/a;->a(I)Z

    move-result v7

    if-nez v7, :cond_6

    :goto_1
    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->d:Landroid/app/NotificationManager;

    const-string v4, "reminders_service_tag"

    long-to-int v6, v2

    invoke-virtual {v1, v4, v6, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/reminders/notification/NotificationService;->a(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_5
    invoke-virtual {v5}, Lcom/google/android/gms/reminders/model/e;->w_()V

    goto/16 :goto_0

    :cond_6
    :try_start_4
    new-instance v7, Landroid/support/v4/app/bk;

    invoke-direct {v7, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    invoke-interface {v4}, Lcom/google/android/gms/reminders/model/Task;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->pA:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v4, v8

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    sget v0, Lcom/google/android/gms/h;->bR:I

    invoke-virtual {v7, v0}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "market://details?id="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/google/android/gms/reminders/c/a;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v4, 0x8000000

    invoke-static {p0, v1, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, v7, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-virtual {v7}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    invoke-static {p0, v2, v3}, Lcom/google/android/gms/reminders/notification/NotificationActionService;->a(Landroid/content/Context;J)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/support/v4/app/bk;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    invoke-virtual {v7}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    goto :goto_1

    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->pr:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/android/gms/reminders/model/e;->w_()V

    throw v0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Lcom/google/android/gms/common/app/d;->onCreate()V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/notification/NotificationService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->b:Landroid/content/ContentResolver;

    .line 103
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/notification/NotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->c:Landroid/app/AlarmManager;

    .line 104
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/notification/NotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/gms/reminders/notification/NotificationService;->d:Landroid/app/NotificationManager;

    .line 105
    return-void
.end method
