.class public final Lcom/google/android/gms/appstate/d/a;
.super Lcom/google/android/gms/common/server/n;
.source "SourceFile"


# instance fields
.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 66
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 68
    iput-boolean p6, p0, Lcom/google/android/gms/appstate/d/a;->f:Z

    .line 69
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 109
    :try_start_0
    invoke-super/range {p0 .. p5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    .line 111
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Null response received for path "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 112
    :catch_0
    move-exception v0

    move-object v3, v0

    .line 113
    iget-object v0, v3, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v4

    .line 114
    :goto_1
    if-eqz v0, :cond_7

    .line 115
    throw v0

    :pswitch_0
    move v0, v1

    .line 113
    goto :goto_0

    :cond_1
    invoke-static {v3, v4}, Lcom/google/android/gms/common/server/b/c;->b(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/a;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    :cond_2
    move-object v0, v4

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->c()Ljava/lang/String;

    move-result-object v6

    const-string v0, "UnregisteredClientId"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "accessNotConfigured"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "userRateLimitExceeded"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_4
    invoke-static {}, Lcom/google/android/gms/appstate/d/b/a;->c()Lcom/google/android/gms/appstate/d/b/b;

    move-result-object v0

    const/16 v5, 0x3e8

    iput v5, v0, Lcom/google/android/gms/appstate/d/b/b;->a:I

    const/4 v5, 0x7

    iput v5, v0, Lcom/google/android/gms/appstate/d/b/b;->b:I

    move-object v5, v0

    :goto_2
    if-nez v5, :cond_5

    move-object v0, v4

    goto :goto_1

    :cond_5
    iput-object v6, v5, Lcom/google/android/gms/appstate/d/b/b;->c:Ljava/lang/String;

    iget v0, v5, Lcom/google/android/gms/appstate/d/b/b;->b:I

    const/4 v4, -0x1

    if-eq v0, v4, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    new-instance v0, Lcom/google/android/gms/appstate/d/b/a;

    invoke-direct {v0, v5, v2}, Lcom/google/android/gms/appstate/d/b/a;-><init>(Lcom/google/android/gms/appstate/d/b/b;B)V

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_3

    .line 119
    :cond_7
    throw v3

    :cond_8
    move-object v5, v4

    goto :goto_2

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/google/android/gms/appstate/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 73
    new-instance v1, Lcom/google/android/gms/common/server/a/a;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    .line 76
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/appstate/d/a;->a:Landroid/content/Context;

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/server/a/c;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    .line 77
    :catch_0
    move-exception v1

    .line 78
    const-string v2, "AppStateServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 80
    :catch_1
    move-exception v1

    .line 81
    const-string v2, "AppStateServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/q;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/appstate/d/a;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 3

    .prologue
    .line 126
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 127
    const-string v1, "User-Agent"

    const-string v2, "AppState/1.0"

    invoke-static {p1, v2}, Lcom/google/android/gms/common/server/ab;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    return-object v0
.end method

.method protected final a(Lcom/android/volley/p;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 139
    new-instance v0, Lcom/google/android/gms/common/server/b;

    iget-object v1, p0, Lcom/google/android/gms/appstate/d/a;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/android/volley/p;->a()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/16 v3, 0x2710

    :goto_0
    const/4 v4, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/b;-><init>(Landroid/content/Context;Ljava/lang/String;IIF)V

    invoke-virtual {p1, v0}, Lcom/android/volley/p;->a(Lcom/android/volley/z;)Lcom/android/volley/p;

    .line 142
    return-void

    .line 139
    :cond_0
    const/16 v3, 0x1388

    goto :goto_0
.end method

.method protected final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/google/android/gms/appstate/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/gms/appstate/d/a;->f:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/appstate/b/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/appstate/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 95
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/n;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/appstate/d/a;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    .line 100
    throw v0
.end method
