.class public final Lcom/google/android/gms/drive/ui/create/a;
.super Lcom/google/android/gms/drive/ui/i;
.source "SourceFile"


# instance fields
.field private j:Lcom/google/android/gms/drive/DriveId;

.field private k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private l:I

.field private m:I

.field private n:Ljava/lang/String;

.field private o:J

.field private p:Ljava/lang/String;

.field private q:Lcom/google/android/gms/common/api/v;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/EditText;

.field private v:Lcom/google/android/gms/drive/c/d;

.field private w:Landroid/os/Bundle;

.field private x:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/i;-><init>()V

    .line 386
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->v:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    const/4 v1, 0x3

    const/16 v2, 0x1b

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/c/a;->c(I)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->b()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->a()V

    .line 270
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->v:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->b()V

    .line 271
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/aj;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 321
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->q:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/h;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/drive/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/q;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    .line 325
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 326
    :cond_0
    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->j:Lcom/google/android/gms/drive/DriveId;

    .line 327
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/ab;->a:Lcom/google/android/gms/drive/ui/picker/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/ab;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/create/a;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 328
    sget-object v1, Lcom/google/android/gms/drive/ui/picker/ab;->a:Lcom/google/android/gms/drive/ui/picker/ab;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/ui/picker/ab;->a()I

    move-result v1

    move v2, v1

    move-object v1, v0

    .line 341
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/drive/ui/create/a;->t:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 343
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 344
    return-void

    .line 331
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->j:Lcom/google/android/gms/drive/DriveId;

    .line 332
    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->l()Ljava/lang/String;

    move-result-object v1

    .line 333
    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->p()Z

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/String;Z)I

    move-result v2

    .line 335
    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->p()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/gms/p;->go:I

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/create/a;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 338
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 335
    :cond_2
    sget v0, Lcom/google/android/gms/p;->gb:I

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/create/a;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/create/a;->x:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/create/a;->x:Z

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->u:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/drive/ui/create/e;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/drive/ui/create/e;-><init>(Lcom/google/android/gms/drive/ui/create/a;B)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/create/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/create/a;Lcom/google/android/gms/drive/DriveId;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "response_drive_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/gms/drive/ui/create/a;->a(I)V

    iput-boolean v3, p0, Lcom/google/android/gms/drive/ui/create/a;->x:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/create/a;Lcom/google/android/gms/drive/aj;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/create/a;->a(Lcom/google/android/gms/drive/aj;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/drive/ui/create/a;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/create/a;->a(I)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/drive/ui/create/a;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->j:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/drive/ui/create/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->n:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->t:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/gms/drive/ui/create/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/create/c;-><init>(Lcom/google/android/gms/drive/ui/create/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->j:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->q:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->j:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/h;->b(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->q:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/q;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/ui/create/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/create/d;-><init>(Lcom/google/android/gms/drive/ui/create/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 298
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->r:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 299
    return-void

    .line 297
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/create/a;->a(Lcom/google/android/gms/drive/aj;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/drive/ui/create/a;)J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/google/android/gms/drive/ui/create/a;->o:J

    return-wide v0
.end method

.method static synthetic f(Lcom/google/android/gms/drive/ui/create/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/drive/ui/create/a;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/drive/ui/create/a;)I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/gms/drive/ui/create/a;->m:I

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/drive/ui/create/a;)I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/gms/drive/ui/create/a;->l:I

    return v0
.end method

.method static synthetic j(Lcom/google/android/gms/drive/ui/create/a;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/create/a;->a(I)V

    iput-boolean v1, p0, Lcom/google/android/gms/drive/ui/create/a;->x:Z

    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/common/api/v;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/create/a;->q:Lcom/google/android/gms/common/api/v;

    .line 116
    return-void
.end method

.method final b()V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/create/a;->a(I)V

    .line 275
    return-void
.end method

.method final c()V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/create/a;->d()V

    .line 279
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 348
    if-nez p1, :cond_1

    .line 349
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 350
    const-string v0, "response_drive_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->j:Lcom/google/android/gms/drive/DriveId;

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/drive/ui/i;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 120
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/i;->onCreate(Landroid/os/Bundle;)V

    .line 121
    iput-boolean v2, p0, Lcom/google/android/gms/drive/ui/create/a;->x:Z

    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->n:Ljava/lang/String;

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "callerPackagingId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/ui/create/a;->o:J

    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "callerPackageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->p:Ljava/lang/String;

    .line 127
    if-eqz p1, :cond_1

    .line 128
    :goto_0
    const-string v0, "selectedCollectionDriveId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->j:Lcom/google/android/gms/drive/DriveId;

    .line 129
    const-string v0, "metadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 130
    const-string v0, "requestId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/ui/create/a;->l:I

    .line 131
    const-string v0, "isShortcut"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/ui/create/a;->m:I

    .line 132
    const-string v0, "logSessionState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->w:Landroid/os/Bundle;

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-nez v0, :cond_0

    .line 136
    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->fL:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    const-string v2, "application/octet-stream"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 141
    :cond_0
    return-void

    .line 127
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x15

    const/16 v4, 0x8

    .line 167
    sget v0, Lcom/google/android/gms/l;->ao:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 169
    sget v0, Lcom/google/android/gms/j;->eN:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->u:Landroid/widget/EditText;

    .line 171
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->u:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 173
    sget v0, Lcom/google/android/gms/j;->eK:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 175
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    sget v0, Lcom/google/android/gms/j;->eO:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->t:Landroid/widget/TextView;

    .line 180
    sget v0, Lcom/google/android/gms/j;->eJ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->r:Landroid/widget/Button;

    .line 181
    sget v0, Lcom/google/android/gms/j;->eI:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->s:Landroid/widget/Button;

    .line 184
    new-instance v0, Lcom/google/android/gms/drive/ui/create/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/ui/create/b;-><init>(Lcom/google/android/gms/drive/ui/create/a;)V

    .line 199
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->r:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->r:Landroid/widget/Button;

    sget v3, Lcom/google/android/gms/p;->fS:I

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    .line 201
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->r:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 202
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->s:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->s:Landroid/widget/Button;

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 206
    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    sget v0, Lcom/google/android/gms/j;->eP:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 208
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 211
    :cond_0
    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/gms/j;->eM:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->eL:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/g;->s:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setElevation(F)V

    .line 213
    :cond_1
    return-object v1
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 360
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/create/a;->x:Z

    if-nez v0, :cond_1

    .line 361
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 363
    if-eqz v0, :cond_0

    .line 364
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 366
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/i;->onDismiss(Landroid/content/DialogInterface;)V

    .line 368
    :cond_1
    return-void
.end method

.method public final onPause()V
    .locals 3

    .prologue
    .line 156
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/i;->onPause()V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->u:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->v:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->m_()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->w:Landroid/os/Bundle;

    .line 162
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 231
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/i;->onResume()V

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->r:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->q:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->q:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/create/a;->d()V

    .line 238
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/c/a/h;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/g/aw;->b(Landroid/content/Context;)Lcom/google/android/gms/drive/c/a/n;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/c/a/h;-><init>(Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->w:Landroid/os/Bundle;

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/gms/drive/auth/CallingAppInfo;

    iget-wide v2, p0, Lcom/google/android/gms/drive/ui/create/a;->o:J

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/create/a;->p:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/auth/CallingAppInfo;-><init>(JLjava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/a;->n:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/c/c;->b(Lcom/google/android/gms/drive/auth/CallingAppInfo;Ljava/lang/String;)Lcom/google/android/gms/drive/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->v:Lcom/google/android/gms/drive/c/d;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->v:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->a()V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->v:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->b()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    const/4 v1, 0x3

    const/16 v2, 0x1c

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->a()V

    .line 239
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->w:Landroid/os/Bundle;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/c/c;->a(Landroid/os/Bundle;)Lcom/google/android/gms/drive/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/a;->v:Lcom/google/android/gms/drive/c/d;

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/i;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 146
    const-string v0, "selectedCollectionDriveId"

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->j:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 148
    const-string v0, "metadata"

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->k:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 149
    const-string v0, "requestId"

    iget v1, p0, Lcom/google/android/gms/drive/ui/create/a;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 150
    const-string v0, "isShortcut"

    iget v1, p0, Lcom/google/android/gms/drive/ui/create/a;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    const-string v0, "logSessionState"

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/a;->w:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 152
    return-void
.end method
