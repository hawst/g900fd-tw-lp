.class final Lcom/google/android/gms/wallet/common/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v2, Lcom/google/checkout/inapp/proto/d;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/d;

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-direct {v3, v1, v2, v0}, Lcom/google/android/gms/wallet/common/DisplayHints;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/checkout/inapp/proto/d;)V

    return-object v3

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    new-array v0, p1, [Lcom/google/android/gms/wallet/common/DisplayHints;

    return-object v0
.end method
