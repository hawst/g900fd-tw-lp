.class final Lcom/google/android/gms/cast/service/d;
.super Lcom/google/android/gms/cast/internal/f;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/b/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/service/CastService;

.field private final b:Lcom/google/android/gms/cast/b/e;

.field private final c:Lcom/google/android/gms/cast/internal/g;

.field private d:Landroid/os/IBinder$DeathRecipient;

.field private e:Landroid/os/IBinder$DeathRecipient;

.field private final f:Lcom/google/android/gms/cast/CastDevice;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private final i:Lcom/google/android/gms/common/internal/bg;

.field private final j:Ljava/lang/String;

.field private final k:J

.field private final l:I

.field private final m:Lcom/google/android/gms/cast/internal/k;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/service/CastService;Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/cast/internal/g;ILjava/lang/String;JLcom/google/android/gms/cast/internal/k;)V
    .locals 13

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/gms/cast/service/d;->a:Lcom/google/android/gms/cast/service/CastService;

    invoke-direct {p0}, Lcom/google/android/gms/cast/internal/f;-><init>()V

    .line 136
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    .line 137
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/internal/bg;

    iput-object v2, p0, Lcom/google/android/gms/cast/service/d;->i:Lcom/google/android/gms/common/internal/bg;

    .line 138
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/gms/cast/service/d;->l:I

    .line 139
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->f:Lcom/google/android/gms/cast/CastDevice;

    .line 140
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->g:Ljava/lang/String;

    .line 141
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->h:Ljava/lang/String;

    .line 142
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    .line 143
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->j:Ljava/lang/String;

    .line 144
    move-wide/from16 v0, p9

    iput-wide v0, p0, Lcom/google/android/gms/cast/service/d;->k:J

    .line 146
    new-instance v2, Lcom/google/android/gms/cast/service/e;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/cast/service/e;-><init>(Lcom/google/android/gms/cast/service/d;Lcom/google/android/gms/cast/service/CastService;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/service/d;->d:Landroid/os/IBinder$DeathRecipient;

    .line 153
    new-instance v2, Lcom/google/android/gms/cast/service/f;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/cast/service/f;-><init>(Lcom/google/android/gms/cast/service/d;Lcom/google/android/gms/cast/service/CastService;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/service/d;->e:Landroid/os/IBinder$DeathRecipient;

    .line 161
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v2}, Lcom/google/android/gms/cast/internal/g;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/service/d;->e:Landroid/os/IBinder$DeathRecipient;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v3, "acquireDeviceController by %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/cast/service/d;->j:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    invoke-static {p1}, Lcom/google/android/gms/cast/service/CastService;->a(Lcom/google/android/gms/cast/service/CastService;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/cast/service/d;->f:Lcom/google/android/gms/cast/CastDevice;

    iget v6, p0, Lcom/google/android/gms/cast/service/d;->l:I

    iget-object v7, p0, Lcom/google/android/gms/cast/service/d;->j:Ljava/lang/String;

    iget-wide v8, p0, Lcom/google/android/gms/cast/service/d;->k:J

    const-string v11, "API"

    move-object v3, p1

    move-object v10, p0

    invoke-static/range {v3 .. v11}, Lcom/google/android/gms/cast/b/e;->a(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/cast/CastDevice;ILjava/lang/String;JLcom/google/android/gms/cast/b/m;Ljava/lang/String;)Lcom/google/android/gms/cast/b/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    .line 171
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/e;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 174
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->i:Lcom/google/android/gms/common/internal/bg;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/cast/service/d;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 197
    :goto_1
    return-void

    .line 163
    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v3, "client disconnected before listener was set"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/internal/k;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0

    .line 176
    :catch_1
    move-exception v2

    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v3, "client died while brokering service"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_1

    .line 180
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/e;->f()Z

    move-result v2

    if-nez v2, :cond_1

    .line 181
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v3, "connecting to device with applicationId=%s, sessionId=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/cast/service/d;->g:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/gms/cast/service/d;->h:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 185
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    iget-object v3, p0, Lcom/google/android/gms/cast/service/d;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/cast/service/d;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_1
    :goto_2
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->i:Lcom/google/android/gms/common/internal/bg;

    invoke-interface {v2}, Lcom/google/android/gms/common/internal/bg;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/service/d;->d:Landroid/os/IBinder$DeathRecipient;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 194
    :catch_2
    move-exception v2

    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v3, "Unable to link listener reaper"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/internal/k;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_1

    .line 187
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/e;->c()V

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/cast/service/d;)Lcom/google/android/gms/cast/internal/k;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/service/d;)V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    return-void
.end method

.method private declared-synchronized b(Z)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 200
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "Disposing ConnectedClient; controller=%s."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/b/e;->a(Z)V

    .line 210
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->e:Landroid/os/IBinder$DeathRecipient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    .line 212
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0}, Lcom/google/android/gms/cast/internal/g;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/service/d;->e:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 216
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->e:Landroid/os/IBinder$DeathRecipient;

    .line 219
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->d:Landroid/os/IBinder$DeathRecipient;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_4

    .line 221
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->i:Lcom/google/android/gms/common/internal/bg;

    invoke-interface {v0}, Lcom/google/android/gms/common/internal/bg;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/service/d;->d:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 225
    const/4 v0, 0x0

    :try_start_4
    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->d:Landroid/os/IBinder$DeathRecipient;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 228
    :cond_4
    :goto_2
    monitor-exit p0

    return-void

    .line 206
    :cond_5
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->p()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 216
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_6
    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->e:Landroid/os/IBinder$DeathRecipient;

    goto :goto_1

    :catchall_1
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/cast/service/d;->e:Landroid/os/IBinder$DeathRecipient;

    throw v0

    .line 225
    :catch_1
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->d:Landroid/os/IBinder$DeathRecipient;

    goto :goto_2

    :catchall_2
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/cast/service/d;->d:Landroid/os/IBinder$DeathRecipient;

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 489
    sget-object v0, Lcom/google/android/gms/cast/media/al;->b:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/cast/b/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/cast/b/aa;->b:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/cast/b/ae;->b:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/cast/internal/j;->b:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/cast/internal/j;->a:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 300
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    const/16 v1, 0x7d1

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast/internal/g;->c(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :goto_0
    return-void

    .line 302
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final a(DDZ)V
    .locals 7

    .prologue
    .line 485
    iget-object v1, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/cast/b/e;->a(DDZ)V

    .line 486
    return-void
.end method

.method public final a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 252
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->i:Lcom/google/android/gms/common/internal/bg;

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/gms/cast/service/d;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    :goto_0
    return-void

    .line 254
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "client died while brokering service"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 255
    invoke-direct {p0, v4}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 318
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/internal/g;->e(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    :goto_0
    return-void

    .line 320
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 278
    invoke-virtual {p1}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/service/d;->g:Ljava/lang/String;

    .line 279
    iput-object p3, p0, Lcom/google/android/gms/cast/service/d;->h:Ljava/lang/String;

    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/cast/internal/g;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :goto_0
    return-void

    .line 284
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/internal/ApplicationStatus;)V
    .locals 1

    .prologue
    .line 349
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/internal/g;->a(Lcom/google/android/gms/cast/internal/ApplicationStatus;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    :goto_0
    return-void

    .line 351
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/internal/DeviceStatus;)V
    .locals 1

    .prologue
    .line 340
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/internal/g;->a(Lcom/google/android/gms/cast/internal/DeviceStatus;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :goto_0
    return-void

    .line 342
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;)V

    .line 429
    return-void
.end method

.method public final a(Ljava/lang/String;DZ)V
    .locals 2

    .prologue
    .line 326
    iget v0, p0, Lcom/google/android/gms/cast/service/d;->l:I

    invoke-static {v0}, Lcom/google/android/gms/common/util/x;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    :goto_0
    return-void

    .line 333
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/cast/internal/g;->a(Ljava/lang/String;DZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 335
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 385
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/cast/internal/g;->a(Ljava/lang/String;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389
    :goto_0
    return-void

    .line 387
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;JI)V
    .locals 2

    .prologue
    .line 376
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/cast/internal/g;->a(Ljava/lang/String;JI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 380
    :goto_0
    return-void

    .line 378
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    .line 419
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 467
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x80

    if-gt v0, v1, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/cast/service/d;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 473
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 408
    new-instance v0, Lcom/google/android/gms/cast/e;

    invoke-direct {v0}, Lcom/google/android/gms/cast/e;-><init>()V

    invoke-virtual {v0, p2}, Lcom/google/android/gms/cast/e;->a(Z)Lcom/google/android/gms/cast/e;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/cast/e;->a:Lcom/google/android/gms/cast/LaunchOptions;

    .line 412
    iget-object v1, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    .line 413
    return-void
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 367
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/cast/internal/g;->a(Ljava/lang/String;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :goto_0
    return-void

    .line 369
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[BJ)V
    .locals 3

    .prologue
    .line 455
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x80

    if-gt v0, v1, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/cast/service/d;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 462
    :cond_0
    :goto_0
    return-void

    .line 461
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;[BJ)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 238
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/service/d;->i:Lcom/google/android/gms/common/internal/bg;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/cast/service/d;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v4}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "Connected to device. rejoinedApp: %b"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :goto_1
    return-void

    .line 238
    :cond_0
    const/16 v0, 0x3e9

    goto :goto_0

    .line 244
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "client died while brokering service"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 245
    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_1
.end method

.method public final a(ZDZ)V
    .locals 2

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/cast/b/e;->a(ZDZ)V

    .line 480
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "disconnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 396
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    .line 397
    return-void
.end method

.method public final b(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onDisconnected: status=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0}, Lcom/google/android/gms/cast/internal/g;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/internal/g;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :goto_0
    invoke-direct {p0, v4}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    .line 273
    return-void

    .line 266
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "client died while brokering service"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Binder is not alive. Not calling onDisconnected"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 438
    invoke-static {p1}, Lcom/google/android/gms/cast/service/d;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    :goto_0
    return-void

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/b/e;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 358
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/cast/internal/g;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    :goto_0
    return-void

    .line 360
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->i()V

    .line 424
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 291
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/internal/g;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_0
    return-void

    .line 293
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 446
    invoke-static {p1}, Lcom/google/android/gms/cast/service/d;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    :goto_0
    return-void

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/b/e;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/cast/b/e;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->k()V

    .line 434
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 309
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/d;->c:Lcom/google/android/gms/cast/internal/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/internal/g;->d(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :goto_0
    return-void

    .line 311
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/service/d;->b(Z)V

    goto :goto_0
.end method
