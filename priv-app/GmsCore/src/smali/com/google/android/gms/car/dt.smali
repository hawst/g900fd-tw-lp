.class public Lcom/google/android/gms/car/dt;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Object;

.field private g:Lcom/google/android/gms/car/bc;

.field private h:Lcom/google/android/gms/car/gl;

.field private i:Lcom/google/android/gms/car/fr;

.field private j:Lcom/google/android/gms/car/fd;

.field private k:Lcom/google/android/gms/car/cu;

.field private final l:Ljava/util/HashMap;

.field private m:Lcom/google/android/gms/car/fl;

.field private n:Lcom/google/android/gms/car/bp;

.field private final o:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final p:Lcom/google/android/gms/car/t;

.field private final q:Lcom/google/android/gms/car/dx;

.field private final r:Lcom/google/android/gms/common/api/x;

.field private s:Lcom/google/android/gms/car/dw;

.field private t:Lcom/google/android/gms/car/du;

.field private u:Lcom/google/android/gms/car/jx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/car/t;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 127
    new-array v5, v6, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->a:Ljava/lang/Object;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->l:Ljava/util/HashMap;

    .line 79
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 84
    new-instance v0, Lcom/google/android/gms/car/dx;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/dx;-><init>(Lcom/google/android/gms/car/dt;)V

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->q:Lcom/google/android/gms/car/dx;

    .line 87
    new-instance v0, Lcom/google/android/gms/car/dv;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/dv;-><init>(Lcom/google/android/gms/car/dt;)V

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->r:Lcom/google/android/gms/common/api/x;

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->r:Lcom/google/android/gms/common/api/x;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/dt;->a(Lcom/google/android/gms/common/api/x;)V

    .line 129
    iput-object p3, p0, Lcom/google/android/gms/car/dt;->p:Lcom/google/android/gms/car/t;

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->q:Lcom/google/android/gms/car/dx;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/car/dx;->a(Lcom/google/android/gms/car/t;)V

    .line 131
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/dt;)Lcom/google/android/gms/car/dx;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->q:Lcom/google/android/gms/car/dx;

    return-object v0
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 3

    .prologue
    .line 645
    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646
    const-string v0, "CAR.CLIENT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Remote exception from car service:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 649
    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 650
    const-string v0, "CAR.CLIENT"

    const-string v1, "Already handling a remote exception, ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :cond_1
    :goto_0
    return-void

    .line 654
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->q:Lcom/google/android/gms/car/dx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/dx;->a()V

    .line 655
    invoke-virtual {p0}, Lcom/google/android/gms/car/dt;->c_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 656
    invoke-virtual {p0}, Lcom/google/android/gms/car/dt;->b()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/dt;Landroid/os/RemoteException;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/dt;->a(Landroid/os/RemoteException;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/car/dt;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/car/dt;->l()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/dt;)V
    .locals 2

    .prologue
    .line 38
    const-string v0, "CAR.CLIENT"

    const-string v1, "ICar died!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/car/dt;->q:Lcom/google/android/gms/car/dx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/dx;->a()V

    invoke-direct {p0}, Lcom/google/android/gms/car/dt;->m()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/car/dt;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/car/dt;->g()V

    return-void
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 611
    iget-object v1, p0, Lcom/google/android/gms/car/dt;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 612
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/dt;->g:Lcom/google/android/gms/car/bc;

    if-eqz v2, :cond_4

    .line 613
    iget-object v2, p0, Lcom/google/android/gms/car/dt;->g:Lcom/google/android/gms/car/bc;

    const-string v3, "CAR.AUDIO"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "CAR.AUDIO"

    const-string v4, "handleCarDisconnection"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/gms/car/bc;->d:Z

    iget-object v3, v2, Lcom/google/android/gms/car/bc;->a:[Lcom/google/android/gms/car/bm;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    :try_start_1
    iget-object v4, v2, Lcom/google/android/gms/car/bc;->a:[Lcom/google/android/gms/car/bm;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    iget-object v4, v2, Lcom/google/android/gms/car/bc;->a:[Lcom/google/android/gms/car/bm;

    aget-object v4, v4, v0

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/android/gms/car/bc;->a:[Lcom/google/android/gms/car/bm;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/google/android/gms/car/bm;->a()V

    iget-object v4, v2, Lcom/google/android/gms/car/bc;->a:[Lcom/google/android/gms/car/bm;

    const/4 v5, 0x0

    aput-object v5, v4, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v3, v2, Lcom/google/android/gms/car/bc;->b:Ljava/lang/Object;

    monitor-enter v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, v2, Lcom/google/android/gms/car/bc;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bd;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 641
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 613
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v3

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_3
    :try_start_6
    iget-object v0, v2, Lcom/google/android/gms/car/bc;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 614
    const/4 v0, 0x0

    :try_start_7
    iput-object v0, p0, Lcom/google/android/gms/car/dt;->g:Lcom/google/android/gms/car/bc;

    .line 616
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->h:Lcom/google/android/gms/car/gl;

    if-eqz v0, :cond_6

    .line 617
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->h:Lcom/google/android/gms/car/gl;

    const-string v2, "CAR.SENSOR"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "CAR.SENSOR"

    const-string v3, "handleCarDisconnection"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v2, v0, Lcom/google/android/gms/car/gl;->b:Ljava/util/HashMap;

    monitor-enter v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    iget-object v3, v0, Lcom/google/android/gms/car/gl;->b:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/gms/car/gl;->a:Lcom/google/android/gms/car/gm;

    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    iget-object v2, v0, Lcom/google/android/gms/car/gl;->c:Ljava/util/HashMap;

    monitor-enter v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    iget-object v0, v0, Lcom/google/android/gms/car/gl;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 618
    const/4 v0, 0x0

    :try_start_b
    iput-object v0, p0, Lcom/google/android/gms/car/dt;->h:Lcom/google/android/gms/car/gl;

    .line 620
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->m:Lcom/google/android/gms/car/fl;

    if-eqz v0, :cond_8

    .line 621
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->m:Lcom/google/android/gms/car/fl;

    const-string v2, "CAR.MSG"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "CAR.MSG"

    const-string v3, "handleCarDisconnection"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :cond_7
    :try_start_c
    iget-object v2, v0, Lcom/google/android/gms/car/fl;->a:Lcom/google/android/gms/car/lw;

    iget-object v3, v0, Lcom/google/android/gms/car/fl;->b:Lcom/google/android/gms/car/fm;

    invoke-interface {v2, v3}, Lcom/google/android/gms/car/lw;->b(Lcom/google/android/gms/car/lz;)V
    :try_end_c
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :goto_2
    const/4 v2, 0x0

    :try_start_d
    iput-object v2, v0, Lcom/google/android/gms/car/fl;->c:Lcom/google/android/gms/car/fn;

    .line 622
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->m:Lcom/google/android/gms/car/fl;

    .line 624
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->n:Lcom/google/android/gms/car/bp;

    if-eqz v0, :cond_a

    .line 625
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->n:Lcom/google/android/gms/car/bp;

    const-string v2, "CAR.BT"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "CAR.BT"

    const-string v3, "handleCarDisconnection"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-object v0, v0, Lcom/google/android/gms/car/bp;->a:Lcom/google/android/gms/car/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bq;->a()V

    .line 626
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->n:Lcom/google/android/gms/car/bp;

    .line 628
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->i:Lcom/google/android/gms/car/fr;

    if-eqz v0, :cond_d

    .line 629
    iget-object v2, p0, Lcom/google/android/gms/car/dt;->i:Lcom/google/android/gms/car/fr;

    const-string v0, "CAR.SENSOR"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "CAR.SENSOR"

    const-string v3, "handleCarDisconnection"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :cond_b
    :try_start_e
    iget-object v0, v2, Lcom/google/android/gms/car/fr;->a:Lcom/google/android/gms/car/mc;

    iget-object v3, v2, Lcom/google/android/gms/car/fr;->b:Lcom/google/android/gms/car/ft;

    invoke-interface {v0, v3}, Lcom/google/android/gms/car/mc;->b(Lcom/google/android/gms/car/mf;)Z
    :try_end_e
    .catch Ljava/lang/IllegalStateException; {:try_start_e .. :try_end_e} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :cond_c
    :goto_3
    const/4 v0, 0x0

    :try_start_f
    iput-object v0, v2, Lcom/google/android/gms/car/fr;->c:Lcom/google/android/gms/car/fs;

    .line 630
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->i:Lcom/google/android/gms/car/fr;

    .line 632
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->j:Lcom/google/android/gms/car/fd;

    if-eqz v0, :cond_f

    .line 633
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->j:Lcom/google/android/gms/car/fd;

    const-string v2, "CAR.MEDIA"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "CAR.MEDIA"

    const-string v3, "handleCarDisconnection"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/car/fd;->c:Lcom/google/android/gms/car/ff;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :try_start_10
    iget-object v2, v0, Lcom/google/android/gms/car/fd;->a:Lcom/google/android/gms/car/lk;

    iget-object v3, v0, Lcom/google/android/gms/car/fd;->b:Lcom/google/android/gms/car/fe;

    invoke-interface {v2, v3}, Lcom/google/android/gms/car/lk;->b(Lcom/google/android/gms/car/ln;)Z
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :goto_4
    :try_start_11
    iget-object v2, v0, Lcom/google/android/gms/car/fd;->d:Lcom/google/android/gms/car/lq;

    iget-object v0, v0, Lcom/google/android/gms/car/fd;->e:Lcom/google/android/gms/car/fg;

    invoke-interface {v2, v0}, Lcom/google/android/gms/car/lq;->b(Lcom/google/android/gms/car/lt;)Z
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_2
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 634
    :goto_5
    const/4 v0, 0x0

    :try_start_12
    iput-object v0, p0, Lcom/google/android/gms/car/dt;->j:Lcom/google/android/gms/car/fd;

    .line 636
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->k:Lcom/google/android/gms/car/cu;

    if-eqz v0, :cond_10

    .line 637
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->k:Lcom/google/android/gms/car/cu;

    const-string v2, "CAR.TEL.CarCallManager"

    const-string v3, "handleCarDisconnection."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :try_start_13
    iget-object v2, v0, Lcom/google/android/gms/car/cu;->b:Lcom/google/android/gms/car/mi;

    iget-object v3, v0, Lcom/google/android/gms/car/cu;->d:Lcom/google/android/gms/car/di;

    invoke-interface {v2, v3}, Lcom/google/android/gms/car/mi;->b(Lcom/google/android/gms/car/ml;)Z

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->a:Lcom/google/android/gms/car/lb;

    iget-object v0, v0, Lcom/google/android/gms/car/cu;->c:Lcom/google/android/gms/car/dh;

    invoke-interface {v2, v0}, Lcom/google/android/gms/car/lb;->b(Lcom/google/android/gms/car/le;)Z
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_1
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 638
    :goto_6
    const/4 v0, 0x0

    :try_start_14
    iput-object v0, p0, Lcom/google/android/gms/car/dt;->k:Lcom/google/android/gms/car/cu;

    .line 640
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 641
    monitor-exit v1

    return-void

    .line 617
    :catchall_3
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_4
    move-exception v0

    monitor-exit v2

    throw v0

    .line 629
    :catch_0
    move-exception v0

    const-string v3, "CAR.SENSOR"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "CAR.SENSOR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RemoteException from car service:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v2

    goto :goto_4

    :catch_4
    move-exception v0

    goto/16 :goto_3

    :catch_5
    move-exception v2

    goto/16 :goto_2

    .line 621
    :catch_6
    move-exception v2

    goto/16 :goto_2
.end method

.method private declared-synchronized l()V
    .locals 3

    .prologue
    .line 667
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->t:Lcom/google/android/gms/car/du;

    if-nez v0, :cond_0

    .line 668
    new-instance v0, Lcom/google/android/gms/car/du;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/du;-><init>(Lcom/google/android/gms/car/dt;)V

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->t:Lcom/google/android/gms/car/du;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/dt;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/jx;

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->u:Lcom/google/android/gms/car/jx;

    .line 671
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->u:Lcom/google/android/gms/car/jx;

    invoke-interface {v0}, Lcom/google/android/gms/car/jx;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/dt;->t:Lcom/google/android/gms/car/du;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 678
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 673
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "CAR.CLIENT"

    const-string v1, "Unable to link death recipient to ICar."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->t:Lcom/google/android/gms/car/du;

    .line 675
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->u:Lcom/google/android/gms/car/jx;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 667
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized m()V
    .locals 3

    .prologue
    .line 681
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->t:Lcom/google/android/gms/car/du;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/dt;->u:Lcom/google/android/gms/car/jx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 683
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/dt;->u:Lcom/google/android/gms/car/jx;

    invoke-interface {v0}, Lcom/google/android/gms/car/jx;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/dt;->t:Lcom/google/android/gms/car/du;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 686
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/car/dt;->t:Lcom/google/android/gms/car/du;

    .line 687
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->u:Lcom/google/android/gms/car/jx;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 689
    :cond_0
    monitor-exit p0

    return-void

    .line 681
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 38
    invoke-static {p1}, Lcom/google/android/gms/car/jy;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/jx;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 135
    const-string v0, "CAR.CLIENT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    const-string v0, "CAR.CLIENT"

    const-string v1, "connect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->a()V

    .line 139
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 898
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 899
    const-string v1, "client_name"

    const-string v2, "car-1-0"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    const v1, 0x6768a8

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/bj;->b(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 903
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 881
    const-string v0, "com.google.android.gms.car.service.START"

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 143
    const-string v0, "CAR.CLIENT"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const-string v0, "CAR.CLIENT"

    const-string v2, "disconnect"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/dt;->m()V

    .line 148
    invoke-direct {p0}, Lcom/google/android/gms/car/dt;->g()V

    .line 151
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/dt;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/jx;
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3

    .line 156
    :goto_0
    if-eqz v0, :cond_1

    .line 160
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/car/dt;->q:Lcom/google/android/gms/car/dx;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/jx;->b(Lcom/google/android/gms/car/lh;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    .line 163
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/car/dt;->s:Lcom/google/android/gms/car/dw;

    if-eqz v1, :cond_1

    .line 165
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/car/dt;->s:Lcom/google/android/gms/car/dw;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/jx;->b(Lcom/google/android/gms/car/ka;)V

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/dt;->s:Lcom/google/android/gms/car/dw;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 172
    :cond_1
    :goto_2
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->b()V

    .line 173
    return-void

    :catch_0
    move-exception v0

    :goto_3
    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_3
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 886
    const-string v0, "com.google.android.gms.car.ICar"

    return-object v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/car/dt;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 184
    :goto_0
    return v0

    .line 180
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/dt;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/jx;

    invoke-interface {v0}, Lcom/google/android/gms/car/jx;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/dt;->a(Landroid/os/RemoteException;)V

    move v0, v1

    .line 184
    goto :goto_0
.end method

.method public final f()I
    .locals 3

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/google/android/gms/car/dt;->j()V

    .line 190
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/dt;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/jx;

    invoke-interface {v0}, Lcom/google/android/gms/car/jx;->h()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 191
    return v0

    .line 192
    :catch_0
    move-exception v0

    .line 193
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/dt;->a(Landroid/os/RemoteException;)V

    new-instance v0, Lcom/google/android/gms/car/fw;

    invoke-direct {v0}, Lcom/google/android/gms/car/fw;-><init>()V

    throw v0

    .line 196
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CarNotConnected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/gms/car/fw;

    invoke-direct {v0}, Lcom/google/android/gms/car/fw;-><init>()V

    throw v0

    :cond_0
    throw v0
.end method
