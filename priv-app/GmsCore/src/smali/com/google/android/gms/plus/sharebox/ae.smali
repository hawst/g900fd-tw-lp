.class public final Lcom/google/android/gms/plus/sharebox/ae;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Lcom/google/android/gms/plus/sharebox/ag;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ScrollView;

.field private g:Landroid/widget/Button;

.field private h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 61
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/ae;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->e:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/sharebox/ae;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->f:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/sharebox/ae;)Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->c:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/sharebox/ae;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->b:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/ag;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/sharebox/ae;)Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->b:Z

    return v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 308
    return-void
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->c:Z

    return v0
.end method

.method final b()Lcom/google/android/gms/plus/model/posts/Comment;
    .locals 5

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ae;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->c()Lcom/google/android/gms/plus/sharebox/ah;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ah;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 260
    const/4 v0, 0x0

    .line 276
    :goto_0
    return-object v0

    .line 263
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ae;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 265
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ae;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    .line 267
    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->c()Lcom/google/android/gms/plus/sharebox/ah;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ah;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 271
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/bg;->g:Ljava/lang/String;

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v2

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v3, v0, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v4

    .line 276
    new-instance v0, Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/model/posts/Comment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final c()Z
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v4, v0, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    .line 294
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ae;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/ag;->c()Lcom/google/android/gms/plus/sharebox/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/ah;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v5}, Lcom/google/android/gms/plus/sharebox/ag;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v6}, Lcom/google/android/gms/plus/sharebox/ag;->e()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Landroid/support/v4/app/au;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/audience/bg;)V

    .line 300
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->a:Z

    if-nez v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->c()Lcom/google/android/gms/plus/sharebox/ah;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ah;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 302
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->a:Z

    .line 304
    :cond_0
    return-void

    .line 290
    :cond_1
    sget-object v4, Lcom/google/android/gms/common/analytics/a;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 177
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 179
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/plus/sharebox/bg;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    .line 180
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/plus/sharebox/bg;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 184
    :cond_0
    if-nez p1, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/plus/sharebox/bg;->r:Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v2, :cond_4

    :goto_1
    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->r:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    .line 186
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 187
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    const-string v3, "+"

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/ag;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/plus/sharebox/bg;->r:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 191
    new-instance v3, Lcom/google/android/gms/plus/sharebox/MentionSpan;

    invoke-direct {v3, v0}, Lcom/google/android/gms/plus/sharebox/MentionSpan;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/16 v4, 0x21

    invoke-interface {v2, v3, v1, v0, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->requestFocus()Z

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setSelection(I)V

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setSelection(I)V

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->f()V

    .line 204
    return-void

    :cond_3
    move v2, v1

    .line 179
    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 184
    goto/16 :goto_1
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 134
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 135
    instance-of v0, p1, Lcom/google/android/gms/plus/sharebox/ag;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/sharebox/ag;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/sharebox/ag;

    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    .line 140
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->qq:I

    if-ne v0, v1, :cond_0

    .line 235
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ae;->a(Z)V

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->g()V

    .line 238
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 146
    if-eqz p1, :cond_0

    .line 147
    const-string v0, "logged_expand_replybox"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->a:Z

    .line 149
    const-string v0, "logged_comment_added"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->b:Z

    .line 150
    const-string v0, "user_edited"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->c:Z

    .line 152
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 157
    sget v0, Lcom/google/android/gms/l;->eF:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->e:Landroid/view/View;

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->e:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->lN:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->f:Landroid/widget/ScrollView;

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->e:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->qq:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->g:Landroid/widget/Button;

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ae;->g:Landroid/widget/Button;

    if-eqz p3, :cond_0

    const-string v0, "button_enabled"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->e:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->dk:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    new-instance v1, Lcom/google/android/gms/plus/sharebox/af;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/plus/sharebox/af;-><init>(Lcom/google/android/gms/plus/sharebox/ae;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->h:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->e:Landroid/view/View;

    return-object v0

    :cond_0
    move v0, v1

    .line 164
    goto :goto_0
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 242
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->dk:I

    if-ne v0, v1, :cond_0

    .line 243
    packed-switch p2, :pswitch_data_0

    .line 249
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 245
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ae;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 246
    const/4 v0, 0x1

    goto :goto_0

    .line 243
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 217
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 219
    const-string v0, "logged_expand_replybox"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/ae;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 220
    const-string v0, "logged_comment_added"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/ae;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 221
    const-string v0, "user_edited"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/ae;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 222
    const-string v0, "button_enabled"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ae;->g:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 223
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 208
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ae;->d:Lcom/google/android/gms/plus/sharebox/ag;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->c()Lcom/google/android/gms/plus/sharebox/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/ah;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ae;->d()V

    .line 213
    :cond_0
    return-void
.end method
