.class final Lcom/google/android/gms/ads/internal/request/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/g/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/a/p;

.field final synthetic b:Lcom/google/android/gms/ads/internal/util/u;

.field final synthetic c:Lcom/google/android/gms/ads/internal/util/j;

.field final synthetic d:Lcom/google/android/gms/ads/internal/request/ae;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/ae;Lcom/google/android/gms/ads/internal/a/p;Lcom/google/android/gms/ads/internal/util/u;Lcom/google/android/gms/ads/internal/util/j;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/af;->d:Lcom/google/android/gms/ads/internal/request/ae;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/af;->a:Lcom/google/android/gms/ads/internal/a/p;

    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/af;->b:Lcom/google/android/gms/ads/internal/util/u;

    iput-object p4, p0, Lcom/google/android/gms/ads/internal/request/af;->c:Lcom/google/android/gms/ads/internal/util/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 178
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/af;->a:Lcom/google/android/gms/ads/internal/a/p;

    const-string v2, "/nativeAdPreProcess"

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/af;->b:Lcom/google/android/gms/ads/internal/util/u;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/util/u;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/ads/internal/g/m;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/ads/internal/a/p;->b(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 181
    :try_start_0
    const-string v0, "success"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 182
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/af;->c:Lcom/google/android/gms/ads/internal/util/j;

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "ads"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/util/j;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    return-void

    .line 188
    :catch_0
    move-exception v0

    .line 189
    const-string v1, "Malformed native JSON response."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/af;->d:Lcom/google/android/gms/ads/internal/request/ae;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/ads/internal/request/ae;->a(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/af;->d:Lcom/google/android/gms/ads/internal/request/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/request/ae;->a()Z

    move-result v0

    const-string v1, "Unable to set the ad state error!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/af;->c:Lcom/google/android/gms/ads/internal/util/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/util/j;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
