.class final Lcom/google/android/gms/wallet/service/ow/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/cache/e;


# instance fields
.field private final a:Lcom/google/android/gms/wallet/c/i;

.field private final b:[I

.field private final c:Lcom/google/android/gms/wallet/cache/j;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/c/i;[ILcom/google/android/gms/wallet/cache/j;)V
    .locals 0

    .prologue
    .line 1493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1494
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/aj;->a:Lcom/google/android/gms/wallet/c/i;

    .line 1495
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/aj;->b:[I

    .line 1496
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/aj;->c:Lcom/google/android/gms/wallet/cache/j;

    .line 1497
    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;IILcom/google/checkout/inapp/proto/ai;)V
    .locals 4

    .prologue
    .line 1502
    invoke-static {}, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;->a()Lcom/google/android/gms/wallet/firstparty/b;

    move-result-object v1

    .line 1504
    packed-switch p3, :pswitch_data_0

    .line 1513
    sget-object v0, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    .line 1515
    :goto_0
    if-eqz p4, :cond_0

    .line 1516
    iget-object v2, p4, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/aj;->b:[I

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/firstparty/b;[Lcom/google/checkout/inapp/proto/j;[I)V

    .line 1519
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/aj;->a:Lcom/google/android/gms/wallet/c/i;

    iget-object v1, v1, Lcom/google/android/gms/wallet/firstparty/b;->a:Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;

    sget-object v3, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/wallet/c/i;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1523
    :goto_1
    return-void

    .line 1506
    :pswitch_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    .line 1507
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/aj;->c:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {v2, p1, p2, p4}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/ai;)V

    goto :goto_0

    .line 1510
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x7

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    goto :goto_0

    .line 1521
    :catch_0
    move-exception v0

    const-string v0, "NetworkOwService"

    const-string v1, "getInstruments callback: RemoteException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1504
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
