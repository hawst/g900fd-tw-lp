.class public final Lcom/google/android/gms/googlehelp/f/m;
.super Lcom/google/android/gms/googlehelp/f/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/googlehelp/search/a;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/search/a;Ljava/lang/String;Ljava/util/Calendar;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/f/c;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/f/m;->a:Lcom/google/android/gms/googlehelp/search/a;

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/f/m;->b:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/f/m;->c:Ljava/util/Calendar;

    .line 42
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/m;->a:Lcom/google/android/gms/googlehelp/search/a;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/m;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/m;->c:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/googlehelp/search/a;->a(Ljava/lang/String;J)V

    const/4 v0, 0x0

    return-object v0
.end method
