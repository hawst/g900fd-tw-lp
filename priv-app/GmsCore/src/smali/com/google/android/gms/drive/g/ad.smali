.class public final Lcom/google/android/gms/drive/g/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/at;


# instance fields
.field private final a:Lcom/google/android/gms/drive/g/e;

.field private final b:Lcom/google/android/gms/drive/g/ae;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/g/ae;Ljava/lang/String;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;)V
    .locals 8

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/g/ad;->c:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/drive/g/ad;->b:Lcom/google/android/gms/drive/g/ae;

    .line 37
    invoke-interface {p2, p3}, Lcom/google/android/gms/drive/g/ae;->b(Ljava/lang/String;)J

    move-result-wide v6

    .line 39
    invoke-interface {p2, p3}, Lcom/google/android/gms/drive/g/ae;->a(Ljava/lang/String;)I

    move-result v5

    .line 42
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-ltz v0, :cond_0

    if-gez v5, :cond_1

    .line 46
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v6

    .line 47
    invoke-virtual {p4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 49
    :cond_1
    new-instance v1, Lcom/google/android/gms/drive/g/e;

    move-object v2, p1

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/drive/g/e;-><init>(Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;IJ)V

    iput-object v1, p0, Lcom/google/android/gms/drive/g/ad;->a:Lcom/google/android/gms/drive/g/e;

    .line 51
    return-void
.end method

.method private declared-synchronized a()V
    .locals 5

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/g/ad;->a:Lcom/google/android/gms/drive/g/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/e;->b()I

    move-result v0

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/drive/g/ad;->a:Lcom/google/android/gms/drive/g/e;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/g/e;->a()J

    move-result-wide v2

    .line 90
    iget-object v1, p0, Lcom/google/android/gms/drive/g/ad;->b:Lcom/google/android/gms/drive/g/ae;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/ad;->c:Ljava/lang/String;

    invoke-interface {v1, v4, v0, v2, v3}, Lcom/google/android/gms/drive/g/ae;->a(Ljava/lang/String;IJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    monitor-exit p0

    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/g/ad;->a:Lcom/google/android/gms/drive/g/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/e;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/g/ad;->a:Lcom/google/android/gms/drive/g/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/e;->d()Z

    move-result v0

    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/ad;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    monitor-exit p0

    return v0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 1

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/g/ad;->a:Lcom/google/android/gms/drive/g/e;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/e;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/ad;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 59
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/ad;->a()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 56
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 1

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/g/ad;->a:Lcom/google/android/gms/drive/g/e;

    .line 78
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/ad;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    monitor-exit p0

    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 96
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s[%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "PersistentBucketRateLimiter"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/g/ad;->a:Lcom/google/android/gms/drive/g/e;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
