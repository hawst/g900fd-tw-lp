.class public final Lcom/google/android/gms/ads/jams/SystemEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 20
    new-instance v1, Lcom/google/android/gms/ads/jams/d;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/ads/jams/d;-><init>(Landroid/content/Context;)V

    .line 22
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    const-string v0, "[JAMS] Boot completed."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 25
    invoke-virtual {v1}, Lcom/google/android/gms/ads/jams/d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-static {}, Lcom/google/android/gms/ads/jams/NegotiationService;->b()V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    const-string v0, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29
    const-string v0, "[JAMS] Accounts changed"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 31
    invoke-virtual {v1}, Lcom/google/android/gms/ads/jams/d;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    invoke-static {}, Lcom/google/android/gms/ads/jams/NegotiationService;->b()V

    goto :goto_0

    .line 34
    :cond_2
    const-string v0, "com.google.android.checkin.CHECKIN_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    const-string v0, "[JAMS] Gservices updated"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 37
    invoke-virtual {v1}, Lcom/google/android/gms/ads/jams/d;->c()Z

    move-result v2

    .line 38
    sget-object v0, Lcom/google/android/gms/ads/d;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 40
    iget-object v1, v1, Lcom/google/android/gms/ads/jams/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "negotiation_enabled"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 42
    if-eqz v2, :cond_3

    if-nez v0, :cond_3

    .line 44
    const-string v0, "[JAMS] Negotiation disabled"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 45
    invoke-static {}, Lcom/google/android/gms/ads/jams/NegotiationService;->c()V

    goto :goto_0

    .line 46
    :cond_3
    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    .line 48
    const-string v0, "[JAMS] Negotiation enabled"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/google/android/gms/ads/jams/NegotiationService;->b()V

    goto :goto_0
.end method
