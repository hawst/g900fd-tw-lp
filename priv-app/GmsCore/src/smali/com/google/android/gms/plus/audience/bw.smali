.class public final Lcom/google/android/gms/plus/audience/bw;
.super Lcom/google/android/gms/plus/audience/ac;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/plus/audience/bh;


# instance fields
.field private o:Landroid/widget/CheckBox;

.field private p:Landroid/widget/CheckBox;

.field private q:Landroid/view/View;

.field private r:Ljava/util/HashSet;

.field private s:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/ac;-><init>()V

    .line 231
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->r:Ljava/util/HashSet;

    .line 416
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/bw;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/bw;Ljava/util/HashSet;)Ljava/util/HashSet;
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/bw;->r:Ljava/util/HashSet;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/bw;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->r:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/audience/bw;)V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->p()V

    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/people/data/a;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/people/data/a;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/bw;->r:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 378
    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v1}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 387
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->r()V

    .line 388
    return-void
.end method

.method private r()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 479
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->a(Z)V

    .line 480
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->a()Landroid/widget/ListView;

    move-result-object v2

    .line 481
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->s:Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->sZ:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    move v0, v1

    .line 484
    :goto_0
    invoke-virtual {v2}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 485
    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 486
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    if-eq v3, v4, :cond_0

    .line 487
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 484
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 490
    :cond_1
    return-void
.end method

.method private s()V
    .locals 4

    .prologue
    .line 493
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->a(Z)V

    .line 494
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->a()Landroid/widget/ListView;

    move-result-object v1

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->s:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->tb:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 497
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 498
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 499
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    if-eq v2, v3, :cond_0

    .line 500
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 497
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 503
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 392
    if-eq p1, p0, :cond_0

    .line 397
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->r:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 399
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 404
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method protected final c()Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0x8

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    .line 317
    :goto_0
    return-object v0

    .line 313
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->dq:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    .line 316
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->ra:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->cV:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->a(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->dv:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->o:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->o:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->o:Landroid/widget/CheckBox;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->b(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->du:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->c(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->d(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->s()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->eA:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->cU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->ez:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->ra:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 317
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    goto/16 :goto_0

    .line 316
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->du:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->eA:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->e(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->s:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bw;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->s()V

    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->q:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->cU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->r()V

    goto :goto_3
.end method

.method protected final d()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;
    .locals 1

    .prologue
    .line 237
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    return-object v0
.end method

.method protected final synthetic f()Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->i()Lcom/google/android/gms/plus/audience/o;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic g()Lcom/google/android/gms/plus/audience/ae;
    .locals 1

    .prologue
    .line 225
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    return-object v0
.end method

.method protected final i()Lcom/google/android/gms/plus/audience/o;
    .locals 6

    .prologue
    .line 408
    new-instance v0, Lcom/google/android/gms/plus/audience/bx;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/al;->l:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/al;->m:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/bx;-><init>(Lcom/google/android/gms/plus/audience/bw;Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 242
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ac;->onAttach(Landroid/app/Activity;)V

    .line 244
    instance-of v0, p1, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    if-nez v0, :cond_0

    .line 245
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "FaclSelectionFragment may only be used by FaclSelectionActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_0
    return-void
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    .line 354
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    .line 355
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v1

    .line 356
    sget v2, Lcom/google/android/gms/j;->dv:I

    if-ne v1, v2, :cond_1

    .line 357
    invoke-static {v0, p2}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->a(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;Z)V

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    sget v2, Lcom/google/android/gms/j;->cV:I

    if-ne v1, v2, :cond_2

    .line 359
    invoke-static {v0, p2}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->b(Lcom/google/android/gms/plus/audience/FaclSelectionActivity;Z)V

    .line 360
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->p()V

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 362
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->r()V

    goto :goto_0

    .line 365
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 334
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->du:I

    if-ne v0, v1, :cond_1

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->o:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->cU:I

    if-ne v0, v1, :cond_2

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->p:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 338
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->q()V

    goto :goto_0

    .line 339
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->cV:I

    if-ne v0, v1, :cond_3

    .line 340
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->q()V

    goto :goto_0

    .line 341
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->ra:I

    if-ne v0, v1, :cond_0

    .line 342
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 343
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->r()V

    .line 347
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bw;->s:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 345
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bw;->s()V

    goto :goto_1
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 322
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->onStart()V

    .line 323
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/plus/audience/bh;)V

    .line 324
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bw;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->b(Lcom/google/android/gms/plus/audience/bh;)V

    .line 329
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->onStop()V

    .line 330
    return-void
.end method
