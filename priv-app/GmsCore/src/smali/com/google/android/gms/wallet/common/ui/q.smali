.class final Lcom/google/android/gms/wallet/common/ui/q;
.super Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

.field private final c:Landroid/view/View;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2328
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/q;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 2329
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;-><init>(Landroid/content/Context;)V

    .line 2330
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/q;->c:Landroid/view/View;

    .line 2331
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/q;->d:Ljava/lang/String;

    .line 2332
    iget-object v0, p1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/q;->e:Ljava/util/ArrayList;

    .line 2335
    return-void

    .line 2332
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2320
    check-cast p1, [Lcom/google/android/gms/wallet/common/a/c;

    aget-object v3, p1, v0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/a/c;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/q;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/q;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/b;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/a/c;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/a/b;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/a/c;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/q;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/t/a/b;

    move-result-object v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2320
    check-cast p1, Lcom/google/t/a/b;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/q;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/q;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/q;->c:Landroid/view/View;

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Landroid/view/View;Lcom/google/t/a/b;)V

    :cond_0
    return-void
.end method

.method protected final onPreExecute()V
    .locals 0

    .prologue
    .line 2339
    invoke-super {p0}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;->onPreExecute()V

    .line 2342
    return-void
.end method
