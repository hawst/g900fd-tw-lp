.class public final Lcom/google/android/gms/plus/audience/bz;
.super Lcom/google/android/gms/plus/f/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 286
    invoke-direct {p0}, Lcom/google/android/gms/plus/f/d;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/plus/audience/bz;
    .locals 3

    .prologue
    .line 291
    invoke-static {p0}, Lcom/google/android/gms/plus/f/d;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 292
    const-string v1, "resultCode"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 293
    new-instance v1, Lcom/google/android/gms/plus/audience/bz;

    invoke-direct {v1}, Lcom/google/android/gms/plus/audience/bz;-><init>()V

    .line 294
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/bz;->setArguments(Landroid/os/Bundle;)V

    .line 295
    return-object v1
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 300
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/f/d;->onDismiss(Landroid/content/DialogInterface;)V

    .line 301
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;

    .line 302
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bz;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 303
    if-eqz v0, :cond_0

    .line 304
    const-string v2, "resultCode"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a(Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;I)V

    .line 306
    :cond_0
    return-void
.end method
