.class final Lcom/google/android/gms/fitness/l/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/a/u;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/l/af;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/l/af;)V
    .locals 0

    .prologue
    .line 2796
    iput-object p1, p0, Lcom/google/android/gms/fitness/l/ag;->a:Lcom/google/android/gms/fitness/l/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/gms/fitness/internal/ble/b;)Ljava/lang/Void;
    .locals 7

    .prologue
    .line 2800
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/ag;->a:Lcom/google/android/gms/fitness/l/af;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/af;->q_()V
    :try_end_0
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_0

    .line 2802
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/ag;->a:Lcom/google/android/gms/fitness/l/af;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2804
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2805
    iget-object v2, p1, Lcom/google/android/gms/fitness/internal/ble/b;->a:Lcom/google/android/gms/fitness/data/BleDevice;

    .line 2807
    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/BleDevice;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2808
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 2809
    const-string v4, "address"

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/BleDevice;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2810
    const-string v4, "name"

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/BleDevice;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2811
    const-string v4, "type"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2812
    const-string v0, "device_name"

    iget-object v4, p1, Lcom/google/android/gms/fitness/internal/ble/b;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2814
    const-string v0, "model_number"

    iget-object v4, p1, Lcom/google/android/gms/fitness/internal/ble/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2816
    const-string v0, "manufacturer"

    iget-object v4, p1, Lcom/google/android/gms/fitness/internal/ble/b;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2818
    const-string v0, "hardware_revision"

    iget-object v4, p1, Lcom/google/android/gms/fitness/internal/ble/b;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2820
    const-string v0, "firmware_revision"

    iget-object v4, p1, Lcom/google/android/gms/fitness/internal/ble/b;->g:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2822
    const-string v0, "software_revision"

    iget-object v4, p1, Lcom/google/android/gms/fitness/internal/ble/b;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2824
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/ag;->a:Lcom/google/android/gms/fitness/l/af;

    const-string v4, "BleDevices"

    invoke-virtual {v0, v4, v1}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 2826
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/ag;->a:Lcom/google/android/gms/fitness/l/af;

    iget-object v6, p1, Lcom/google/android/gms/fitness/internal/ble/b;->b:Ljava/util/List;

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/gms/fitness/l/af;->a(Lcom/google/android/gms/fitness/l/af;JLjava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2831
    :catchall_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/ag;->a:Lcom/google/android/gms/fitness/l/af;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
    :try_end_2
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_2 .. :try_end_2} :catch_0

    .line 2835
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/k/a/bd;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    .line 2836
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 2829
    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/ag;->a:Lcom/google/android/gms/fitness/l/af;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/af;->r_()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2831
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/ag;->a:Lcom/google/android/gms/fitness/l/af;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/af;->d()V
    :try_end_4
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2796
    check-cast p1, Lcom/google/android/gms/fitness/internal/ble/b;

    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/ag;->a(Lcom/google/android/gms/fitness/internal/ble/b;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
