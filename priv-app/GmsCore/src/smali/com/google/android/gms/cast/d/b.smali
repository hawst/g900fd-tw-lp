.class final Lcom/google/android/gms/cast/d/b;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/d/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/d/a;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/d/a;->a(Lcom/google/android/gms/cast/d/a;)Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "onReceive"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/d/a;->b(Lcom/google/android/gms/cast/d/a;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 205
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PIN"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 206
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "MANUAL"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 207
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "CANCELED"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 208
    iget-object v3, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v3}, Lcom/google/android/gms/cast/d/a;->a(Lcom/google/android/gms/cast/d/a;)Lcom/google/android/gms/cast/e/h;

    move-result-object v3

    const-string v4, "Received pin from dialog pin=%s, manual=%b, canceled=%b"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    if-eqz v2, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/d/a;->c(Lcom/google/android/gms/cast/d/a;)V

    .line 218
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/d/a;->d(Lcom/google/android/gms/cast/d/a;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 219
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/d/a;->d(Lcom/google/android/gms/cast/d/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 220
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/d/a;->e(Lcom/google/android/gms/cast/d/a;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 223
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/d/a;->f(Lcom/google/android/gms/cast/d/a;)I

    .line 224
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 214
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/cast/d/b;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/cast/d/a;->a(Lcom/google/android/gms/cast/d/a;Ljava/lang/String;Z)V

    goto :goto_0

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 224
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
