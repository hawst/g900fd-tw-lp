.class public final Lcom/google/android/gms/location/places/personalized/d;
.super Lcom/google/android/gms/common/data/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ap;


# instance fields
.field private final c:Lcom/google/android/gms/common/api/Status;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/location/places/personalized/d;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/api/Status;)V

    .line 27
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->CREATOR:Lcom/google/android/gms/location/places/personalized/e;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/data/j;-><init>(Lcom/google/android/gms/common/data/DataHolder;Landroid/os/Parcelable$Creator;)V

    .line 37
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/location/places/personalized/d;->c:Lcom/google/android/gms/common/api/Status;

    .line 40
    return-void

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/location/places/personalized/d;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/location/places/personalized/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/location/places/personalized/d;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/api/Status;)V

    return-object v0
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/location/places/personalized/d;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method
