.class public final Lcom/google/android/gms/audiomodem/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/api/j;

.field public static final b:Lcom/google/android/gms/common/api/c;

.field private static final c:Lcom/google/android/gms/common/api/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/d;->a:Lcom/google/android/gms/common/api/j;

    .line 52
    new-instance v0, Lcom/google/android/gms/audiomodem/e;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/d;->c:Lcom/google/android/gms/common/api/i;

    .line 73
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/audiomodem/d;->c:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/audiomodem/d;->a:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/audiomodem/d;->b:Lcom/google/android/gms/common/api/c;

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/Snoop$Params;Lcom/google/android/gms/audiomodem/bc;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/google/android/gms/audiomodem/h;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/audiomodem/h;-><init>(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/Snoop$Params;Lcom/google/android/gms/audiomodem/bc;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bf;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/google/android/gms/audiomodem/j;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/audiomodem/j;-><init>(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bf;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bf;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/google/android/gms/audiomodem/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/audiomodem/i;-><init>(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bf;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bm;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/google/android/gms/audiomodem/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/audiomodem/g;-><init>(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bm;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/android/gms/audiomodem/f;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/audiomodem/f;-><init>(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
