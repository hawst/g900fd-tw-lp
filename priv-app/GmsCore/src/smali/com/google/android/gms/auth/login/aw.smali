.class public final Lcom/google/android/gms/auth/login/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field volatile a:Z

.field volatile b:Z

.field c:Lcom/google/android/gms/auth/login/ax;

.field d:Ljava/util/ArrayList;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/lang/String;

.field private final g:Landroid/webkit/WebView;

.field private final h:Landroid/os/Handler;

.field private final i:Z

.field private j:Z

.field private k:Z

.field private l:Landroid/view/inputmethod/InputMethodManager;

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Landroid/accounts/AccountManager;

.field private p:I

.field private q:Ljava/lang/String;

.field private r:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/webkit/WebView;Landroid/os/Handler;ZLcom/google/android/gms/auth/firstparty/shared/LatencyTracker;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/aw;->a:Z

    .line 64
    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/aw;->b:Z

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/aw;->d:Ljava/util/ArrayList;

    .line 89
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/google/android/gms/auth/login/aw;->m:I

    .line 90
    iget v0, p0, Lcom/google/android/gms/auth/login/aw;->m:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 91
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "You are using SDK: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but the required minimum is: 17"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/auth/login/aw;->e:Landroid/content/Context;

    .line 95
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 97
    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->e:Landroid/content/Context;

    const-string v3, "input_method"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/aw;->l:Landroid/view/inputmethod/InputMethodManager;

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->e:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/auth/login/aw;->n:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/auth/login/aw;->k:Z

    .line 101
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/auth/login/aw;->p:I

    .line 102
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimSerialNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/aw;->q:Ljava/lang/String;

    .line 103
    iput-object p2, p0, Lcom/google/android/gms/auth/login/aw;->f:Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/aw;->j:Z

    .line 105
    iput-object p3, p0, Lcom/google/android/gms/auth/login/aw;->g:Landroid/webkit/WebView;

    .line 106
    iput-object p4, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/aw;->o:Landroid/accounts/AccountManager;

    .line 108
    const-string v0, "Android2JsBrdige"

    invoke-virtual {p6, v0}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/aw;->r:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    .line 109
    iput-boolean p5, p0, Lcom/google/android/gms/auth/login/aw;->i:Z

    .line 110
    return-void

    :cond_1
    move v0, v2

    .line 104
    goto :goto_0
.end method


# virtual methods
.method public final getAccounts()Ljava/lang/String;
    .locals 5
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 227
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 228
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/aw;->i:Z

    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    .line 238
    :goto_0
    return-object v0

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->o:Landroid/accounts/AccountManager;

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 235
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 236
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 238
    :cond_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getAllowedDomains()Ljava/lang/String;
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 128
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 131
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 135
    :cond_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getAndroidId()Ljava/lang/String;
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 260
    const-string v0, "0"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->n:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getBuildVersionSdk()I
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 183
    iget v0, p0, Lcom/google/android/gms/auth/login/aw;->m:I

    return v0
.end method

.method public final getDroidGuardResult(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/auth/login/aw;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/auth/login/aw;->m:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/aw;->getPlayServicesVersionCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 280
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 281
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 282
    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 281
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 285
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SHA-1"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/e;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 286
    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 287
    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 288
    new-instance v2, Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 289
    const-string v3, "dg_minutemaid"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_1
    return-void

    .line 293
    :catch_0
    move-exception v0

    const-string v0, "GLSActivity"

    const-string v1, "Invalid params given to get dg."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final getFactoryResetChallenges()Ljava/lang/String;
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 317
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 318
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 319
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPhoneNumber()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final getPlayServicesVersionCode()I
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 188
    const v0, 0x6768a8

    return v0
.end method

.method public final getSimSerial()Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final getSimState()I
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 243
    iget v0, p0, Lcom/google/android/gms/auth/login/aw;->p:I

    return v0
.end method

.method public final goBack()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 214
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 215
    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 216
    return-void
.end method

.method public final hasPhoneNumber()Z
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/aw;->j:Z

    return v0
.end method

.method public final hasTelephony()Z
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/aw;->k:Z

    return v0
.end method

.method public final hideKeyboard()V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->l:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->g:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 209
    return-void
.end method

.method public final launchEmergencyDialer()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 194
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 195
    const-string v1, "com.android.phone/.EmergencyDialer"

    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 197
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 198
    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 199
    return-void
.end method

.method public final notifyOnTermsOfServiceAccepted()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 339
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/aw;->a:Z

    .line 340
    return-void
.end method

.method public final setBackButtonEnabled(Z)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->c:Lcom/google/android/gms/auth/login/ax;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->c:Lcom/google/android/gms/auth/login/ax;

    invoke-interface {v0, p1}, Lcom/google/android/gms/auth/login/ax;->a(Z)V

    .line 331
    :cond_0
    return-void
.end method

.method public final setNewAccountCreated()V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/aw;->b:Z

    .line 152
    return-void
.end method

.method public final showKeyboard()V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->l:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->g:Landroid/webkit/WebView;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 204
    return-void
.end method

.method public final showView()V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->r:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v1, "GLSActivity"

    const-string v2, "showView invoked."

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 162
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 163
    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 164
    return-void
.end method

.method public final skipLogin()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 221
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 222
    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 223
    return-void
.end method

.method public final startAfw()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 303
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 304
    iget-object v1, p0, Lcom/google/android/gms/auth/login/aw;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 305
    return-void
.end method
