.class public final Lcom/google/android/gms/appstate/service/a/f;
.super Lcom/google/android/gms/appstate/service/a/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/appstate/c/h;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:[B


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;ILjava/lang/String;[B)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    const-string v0, "ResolveStateOp"

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/appstate/service/a/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/appstate/service/a/f;->b:Lcom/google/android/gms/appstate/c/h;

    .line 30
    iput-object p3, p0, Lcom/google/android/gms/appstate/service/a/f;->c:Ljava/lang/String;

    .line 31
    iput p4, p0, Lcom/google/android/gms/appstate/service/a/f;->d:I

    .line 32
    iput-object p5, p0, Lcom/google/android/gms/appstate/service/a/f;->e:Ljava/lang/String;

    .line 33
    if-nez p6, :cond_0

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/appstate/service/a/f;->f:[B

    .line 39
    :goto_0
    return-void

    .line 36
    :cond_0
    array-length v0, p6

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/appstate/service/a/f;->f:[B

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/f;->f:[B

    array-length v1, p6

    invoke-static {p6, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/f;->b:Lcom/google/android/gms/appstate/c/h;

    iget v1, p0, Lcom/google/android/gms/appstate/service/a/f;->d:I

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/appstate/c/h;->a(ILcom/google/android/gms/common/data/DataHolder;)V

    .line 51
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/appstate/a/e;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 43
    iget-object v2, p0, Lcom/google/android/gms/appstate/service/a/f;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/appstate/service/a/f;->c:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/appstate/service/a/f;->d:I

    iget-object v5, p0, Lcom/google/android/gms/appstate/service/a/f;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/appstate/service/a/f;->f:[B

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/appstate/a/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;[B)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
