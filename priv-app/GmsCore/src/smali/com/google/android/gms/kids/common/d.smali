.class public final Lcom/google/android/gms/kids/common/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/List;

.field private static b:Lcom/google/android/gms/kids/common/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/kids/common/e;

    invoke-direct {v0}, Lcom/google/android/gms/kids/common/e;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/d;->a:Ljava/util/List;

    .line 32
    new-instance v0, Lcom/google/android/gms/kids/common/d;

    invoke-direct {v0}, Lcom/google/android/gms/kids/common/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/kids/common/d;->b:Lcom/google/android/gms/kids/common/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 5

    .prologue
    .line 64
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    iget-object v2, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v4, "com.google.android.gms"

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    sget-object v0, Lcom/google/android/gms/kids/common/b/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 67
    return-object v1
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/kids/account/b;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/gms/kids/common/d;->b:Lcom/google/android/gms/kids/common/d;

    new-instance v0, Lcom/google/android/gms/kids/account/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/kids/account/b;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static a()Lcom/google/android/gms/kids/common/d;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/gms/kids/common/d;->b:Lcom/google/android/gms/kids/common/d;

    return-object v0
.end method

.method public static b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/gms/kids/common/d;->b:Lcom/google/android/gms/kids/common/d;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lcom/google/android/gms/kids/common/c;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/gms/kids/common/d;->b:Lcom/google/android/gms/kids/common/d;

    new-instance v0, Lcom/google/android/gms/kids/common/c;

    invoke-direct {v0}, Lcom/google/android/gms/kids/common/c;-><init>()V

    return-object v0
.end method

.method public static d()Lcom/google/android/gms/kids/common/g;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/gms/kids/common/d;->b:Lcom/google/android/gms/kids/common/d;

    new-instance v0, Lcom/google/android/gms/kids/common/g;

    invoke-direct {v0}, Lcom/google/android/gms/kids/common/g;-><init>()V

    return-object v0
.end method

.method public static e()Lcom/google/android/gms/kids/a/a;
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/kids/a/a;->a(Landroid/content/Context;)Lcom/google/android/gms/kids/a/a;

    move-result-object v0

    return-object v0
.end method

.method public static f()Ljava/util/List;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/gms/kids/common/d;->a:Ljava/util/List;

    return-object v0
.end method
