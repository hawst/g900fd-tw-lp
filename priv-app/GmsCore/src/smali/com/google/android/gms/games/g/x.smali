.class public final Lcom/google/android/gms/games/g/x;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[B

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4370
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4371
    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/games/g/x;->a:[B

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/x;->cachedSize:I

    .line 4372
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4439
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4440
    iget-object v1, p0, Lcom/google/android/gms/games/g/x;->a:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4441
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/x;->a:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 4444
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4445
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4448
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 4449
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4452
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4384
    if-ne p1, p0, :cond_1

    .line 4408
    :cond_0
    :goto_0
    return v0

    .line 4387
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/x;

    if-nez v2, :cond_2

    move v0, v1

    .line 4388
    goto :goto_0

    .line 4390
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/x;

    .line 4391
    iget-object v2, p0, Lcom/google/android/gms/games/g/x;->a:[B

    iget-object v3, p1, Lcom/google/android/gms/games/g/x;->a:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 4392
    goto :goto_0

    .line 4394
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 4395
    iget-object v2, p1, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 4396
    goto :goto_0

    .line 4398
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 4399
    goto :goto_0

    .line 4401
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 4402
    iget-object v2, p1, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 4403
    goto :goto_0

    .line 4405
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 4406
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4413
    iget-object v0, p0, Lcom/google/android/gms/games/g/x;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 4415
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 4417
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 4419
    return v0

    .line 4415
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4417
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4344
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/x;->a:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4425
    iget-object v0, p0, Lcom/google/android/gms/games/g/x;->a:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4426
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/x;->a:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 4428
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4429
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/x;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4431
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4432
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/g/x;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4434
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4435
    return-void
.end method
