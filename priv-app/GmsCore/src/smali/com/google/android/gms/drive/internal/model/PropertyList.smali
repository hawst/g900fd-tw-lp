.class public final Lcom/google/android/gms/drive/internal/model/PropertyList;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/drive/internal/model/y;

.field private static final d:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/drive/internal/model/y;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/y;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/model/PropertyList;->CREATOR:Lcom/google/android/gms/drive/internal/model/y;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 59
    sput-object v0, Lcom/google/android/gms/drive/internal/model/PropertyList;->d:Ljava/util/HashMap;

    const-string v1, "items"

    const-string v2, "items"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/drive/internal/model/Property;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 87
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/model/PropertyList;->b:I

    .line 88
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/PropertyList;->a:Ljava/util/Set;

    .line 89
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/PropertyList;->a:Ljava/util/Set;

    .line 98
    iput p2, p0, Lcom/google/android/gms/drive/internal/model/PropertyList;->b:I

    .line 99
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/PropertyList;->c:Ljava/util/List;

    .line 100
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/gms/drive/internal/model/PropertyList;->d:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 214
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 215
    packed-switch v0, :pswitch_data_0

    .line 220
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 217
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/PropertyList;->c:Ljava/util/List;

    .line 224
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/PropertyList;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 225
    return-void

    .line 215
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/PropertyList;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 179
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 183
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/PropertyList;->c:Ljava/util/List;

    return-object v0

    .line 179
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/google/android/gms/drive/internal/model/PropertyList;->CREATOR:Lcom/google/android/gms/drive/internal/model/y;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 242
    instance-of v0, p1, Lcom/google/android/gms/drive/internal/model/PropertyList;

    if-nez v0, :cond_0

    move v0, v1

    .line 273
    :goto_0
    return v0

    .line 247
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 248
    goto :goto_0

    .line 251
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/internal/model/PropertyList;

    .line 252
    sget-object v0, Lcom/google/android/gms/drive/internal/model/PropertyList;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 253
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/PropertyList;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 254
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/PropertyList;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 256
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/PropertyList;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/PropertyList;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 258
    goto :goto_0

    :cond_3
    move v0, v1

    .line 263
    goto :goto_0

    .line 266
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/PropertyList;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 268
    goto :goto_0

    :cond_5
    move v0, v2

    .line 273
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 229
    const/4 v0, 0x0

    .line 230
    sget-object v1, Lcom/google/android/gms/drive/internal/model/PropertyList;->d:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 231
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/PropertyList;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 232
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 233
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/PropertyList;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 235
    goto :goto_0

    .line 236
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/google/android/gms/drive/internal/model/PropertyList;->CREATOR:Lcom/google/android/gms/drive/internal/model/y;

    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/model/y;->a(Lcom/google/android/gms/drive/internal/model/PropertyList;Landroid/os/Parcel;)V

    .line 159
    return-void
.end method
