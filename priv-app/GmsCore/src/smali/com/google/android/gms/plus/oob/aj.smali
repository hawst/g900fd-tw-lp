.class public final Lcom/google/android/gms/plus/oob/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/plus/internal/ax;
.implements Lcom/google/android/gms/plus/oob/ag;


# static fields
.field private static final a:Z


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/plus/internal/ad;

.field private f:Lcom/google/android/gms/plus/internal/ab;

.field private g:Lcom/google/android/gms/plus/oob/ah;

.field private h:Landroid/app/Activity;

.field private i:Lcom/google/android/gms/common/c;

.field private j:Z

.field private k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

.field private l:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

.field private m:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/gms/plus/c/a;->R:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/plus/oob/aj;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->e:Lcom/google/android/gms/plus/internal/ad;

    .line 67
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p2, v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/os/Bundle;)V

    .line 62
    return-object v0
.end method


# virtual methods
.method public final T_()V
    .locals 3

    .prologue
    .line 126
    sget-boolean v0, Lcom/google/android/gms/plus/oob/aj;->a:Z

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "UpgradeAccount"

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-nez v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/aj;->c:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ax;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 134
    :cond_1
    :goto_0
    return-void

    .line 131
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/aj;->j:Z

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/aj;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/aj;->l:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ax;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0
.end method

.method public final W_()V
    .locals 2

    .prologue
    .line 138
    sget-boolean v0, Lcom/google/android/gms/plus/oob/aj;->a:Z

    if-eqz v0, :cond_0

    .line 139
    const-string v0, "UpgradeAccount"

    const-string v1, "onDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/aj;->j:Z

    if-eqz v0, :cond_2

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 144
    :cond_2
    return-void
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    iput-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    .line 80
    iput-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->h:Landroid/app/Activity;

    .line 81
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 70
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/ah;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/oob/ah;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 74
    check-cast v0, Lcom/google/android/gms/plus/oob/ah;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    .line 75
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/aj;->h:Landroid/app/Activity;

    .line 76
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 84
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->b:Ljava/lang/String;

    .line 85
    const-string v0, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->d:Ljava/lang/String;

    .line 86
    invoke-static {p1}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/aj;->c:Ljava/lang/String;

    .line 88
    new-instance v1, Lcom/google/android/gms/plus/internal/cn;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/aj;->h:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/aj;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/aj;->d:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->c:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/f;->e:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/gms/plus/internal/cn;->f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/aj;->e:Lcom/google/android/gms/plus/internal/ad;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/aj;->h:Landroid/app/Activity;

    invoke-interface {v1, v2, v0, p0, p0}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    .line 97
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 3

    .prologue
    .line 115
    sget-boolean v0, Lcom/google/android/gms/plus/oob/aj;->a:Z

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "UpgradeAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConnectionFailed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/aj;->i:Lcom/google/android/gms/common/c;

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/oob/ah;->b(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 122
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 149
    sget-boolean v0, Lcom/google/android/gms/plus/oob/aj;->a:Z

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "UpgradeAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAccountUpgraded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/plus/oob/ah;->b(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 156
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/gms/plus/oob/aj;->j:Z

    .line 170
    :cond_2
    :goto_0
    return-void

    .line 157
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-nez v0, :cond_4

    .line 158
    iput-object p2, p0, Lcom/google/android/gms/plus/oob/aj;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    if-eqz v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/aj;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/oob/ah;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0

    .line 162
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/aj;->j:Z

    if-eqz v0, :cond_2

    .line 163
    iput-boolean v3, p0, Lcom/google/android/gms/plus/oob/aj;->j:Z

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->l:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    .line 165
    iput-object p2, p0, Lcom/google/android/gms/plus/oob/aj;->m:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    if-eqz v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->g:Lcom/google/android/gms/plus/oob/ah;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/aj;->m:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/oob/ah;->b(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 3

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/aj;->j:Z

    if-eqz v0, :cond_0

    .line 226
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Upgrade account already in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :cond_0
    sget-boolean v0, Lcom/google/android/gms/plus/oob/aj;->a:Z

    if-eqz v0, :cond_1

    .line 229
    const-string v0, "UpgradeAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "upgradeAccount: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/aj;->j:Z

    .line 232
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/aj;->l:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 234
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/aj;->T_()V

    .line 238
    :cond_2
    :goto_0
    return-void

    .line 235
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_2

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/aj;->j:Z

    if-eqz v0, :cond_1

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 104
    :cond_1
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 110
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/aj;->f:Lcom/google/android/gms/plus/internal/ab;

    .line 111
    return-void
.end method
