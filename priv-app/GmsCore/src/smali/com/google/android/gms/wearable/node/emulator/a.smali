.class final Lcom/google/android/gms/wearable/node/emulator/a;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;

.field private final b:Z

.field private final c:Lcom/google/android/gms/wearable/node/a/c;

.field private final d:Lcom/google/android/gms/wearable/ConnectionConfiguration;

.field private volatile e:Ljava/net/ServerSocket;

.field private volatile f:Ljava/net/Socket;

.field private volatile g:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;ZLcom/google/android/gms/wearable/ConnectionConfiguration;)V
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    .line 107
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/emulator/a;->a:Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;

    .line 108
    if-eqz p2, :cond_0

    const-string v0, "WearableNetServerThread"

    :goto_0
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->g:Z

    .line 109
    iput-boolean p2, p0, Lcom/google/android/gms/wearable/node/emulator/a;->b:Z

    .line 110
    new-instance v1, Lcom/google/android/gms/wearable/node/a/c;

    const/16 v2, 0x3e8

    const/4 v3, 0x3

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/wearable/node/a/c;-><init>(IIJJ)V

    iput-object v1, p0, Lcom/google/android/gms/wearable/node/emulator/a;->c:Lcom/google/android/gms/wearable/node/a/c;

    .line 112
    iput-object p3, p0, Lcom/google/android/gms/wearable/node/emulator/a;->d:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    .line 113
    return-void

    .line 108
    :cond_0
    const-string v0, "WearableNetClientThread"

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;ZLcom/google/android/gms/wearable/ConnectionConfiguration;B)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wearable/node/emulator/a;-><init>(Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;ZLcom/google/android/gms/wearable/ConnectionConfiguration;)V

    return-void
.end method

.method private static a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 255
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, v1}, Lcom/google/android/gms/wearable/node/a/b;->a(ILjava/lang/CharSequence;Ljava/lang/Throwable;)V

    .line 256
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 259
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/gms/wearable/node/a/b;->a(ILjava/lang/CharSequence;Ljava/lang/Throwable;)V

    .line 260
    return-void
.end method

.method private a(Ljava/net/InetAddress;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 173
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    :try_start_0
    new-instance v0, Ljava/net/ServerSocket;

    const/16 v1, 0x15e1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p1}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->e:Ljava/net/ServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->c:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->a()V

    .line 187
    return-void

    .line 177
    :catch_0
    move-exception v0

    .line 178
    const-string v1, "WearableNetworkSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception while listening for connection: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->b()V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->c:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->b()J

    move-result-wide v0

    .line 182
    const-string v2, "Waiting %.1f seconds to retry listen"

    new-array v3, v7, [Ljava/lang/Object;

    long-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    .line 184
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->e:Ljava/net/ServerSocket;

    .line 234
    if-eqz v0, :cond_0

    .line 236
    :try_start_0
    invoke-virtual {v0}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    const-string v1, "WearableNetworkSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception while closing server socket: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->f:Ljava/net/Socket;

    .line 245
    if-eqz v0, :cond_0

    .line 247
    :try_start_0
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 248
    :catch_0
    move-exception v0

    .line 249
    const-string v1, "WearableNetworkSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception while closing socket: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->g:Z

    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->interrupt()V

    .line 119
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->b()V

    .line 120
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->c()V

    .line 121
    return-void
.end method

.method public final run()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 128
    sget-object v0, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    const-string v1, "goldfish"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    iget-boolean v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->b:Z

    if-eqz v0, :cond_1

    .line 130
    const-string v0, "10.0.2.15"

    .line 140
    :goto_0
    :try_start_0
    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 147
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/emulator/a;->a:Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wearable/node/a/b;->a(Landroid/app/Service;)V

    .line 149
    iget-boolean v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->b:Z

    if-eqz v0, :cond_0

    .line 150
    invoke-direct {p0, v1}, Lcom/google/android/gms/wearable/node/emulator/a;->a(Ljava/net/InetAddress;)V

    .line 152
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_6

    .line 153
    iget-boolean v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "server"
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_2
    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/gms/wearable/node/emulator/a;->b:Z

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Listening via TCP on "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":5601"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v3, v5}, Lcom/google/android/gms/wearable/node/a/b;->a(ILjava/lang/CharSequence;Ljava/lang/Throwable;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/emulator/a;->e:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/wearable/node/emulator/a;->f:Ljava/net/Socket;

    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/emulator/a;->c:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/node/a/c;->a()V

    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Connected, running sync loop as "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->a()Lcom/google/android/gms/wearable/node/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bb;->a()Lcom/google/android/gms/wearable/node/be;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->a()Lcom/google/android/gms/wearable/node/bb;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wearable/node/emulator/a;->f:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wearable/node/emulator/a;->f:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/wearable/node/emulator/a;->d:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v2, v3, v4, v0, v5}, Lcom/google/android/gms/wearable/node/bb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/node/be;Lcom/google/android/gms/wearable/ConnectionConfiguration;)V
    :try_end_2
    .catch Lcom/google/android/gms/wearable/node/bk; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lcom/google/android/gms/wearable/node/i; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->c()V

    :goto_4
    const/4 v0, 0x1

    const-string v2, "Socket closed"

    invoke-static {v0, v2}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->c:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->b()J

    move-result-wide v2

    .line 155
    const/4 v0, 0x1

    const-string v4, "Waiting %.1f seconds to retry connection"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    long-to-float v7, v2

    const/high16 v8, 0x447a0000    # 1000.0f

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    .line 157
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_1

    .line 160
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    :try_start_4
    const-string v1, "Connection interrupted, shutting down"

    invoke-static {v0, v1}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->interrupt()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 165
    const-string v0, "Android Wear NetworkThread finished"

    invoke-static {v9, v0}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->b()V

    .line 167
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/emulator/a;->a:Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    .line 169
    :goto_5
    return-void

    .line 132
    :cond_1
    const-string v0, "10.0.2.2"

    goto/16 :goto_0

    .line 135
    :cond_2
    const-string v0, "127.0.0.1"

    goto/16 :goto_0

    .line 142
    :catch_1
    move-exception v1

    const-string v1, "WearableNetworkSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown host: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 153
    :cond_3
    :try_start_5
    const-string v0, "client"
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_2

    :cond_4
    :try_start_6
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Connecting via TCP to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":5601"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v3, v5}, Lcom/google/android/gms/wearable/node/a/b;->a(ILjava/lang/CharSequence;Ljava/lang/Throwable;)V

    new-instance v2, Ljava/net/Socket;

    const/16 v3, 0x15e1

    invoke-direct {v2, v1, v3}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    iput-object v2, p0, Lcom/google/android/gms/wearable/node/emulator/a;->f:Ljava/net/Socket;
    :try_end_6
    .catch Lcom/google/android/gms/wearable/node/bk; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Lcom/google/android/gms/wearable/node/i; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_3

    :catch_2
    move-exception v0

    const/4 v0, 0x4

    :try_start_7
    const-string v2, "Error: wire protocol version mismatch"

    invoke-static {v0, v2}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->c:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->d()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->c()V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_4

    .line 162
    :catch_3
    move-exception v0

    .line 163
    :try_start_9
    const-string v1, "Unexpected exception, shutting down"

    invoke-static {v1, v0}, Lcom/google/android/gms/wearable/node/emulator/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 165
    const-string v0, "Android Wear NetworkThread finished"

    invoke-static {v9, v0}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->b()V

    .line 167
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/emulator/a;->a:Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    goto :goto_5

    .line 153
    :catch_4
    move-exception v0

    :try_start_a
    iget-boolean v2, p0, Lcom/google/android/gms/wearable/node/emulator/a;->g:Z

    if-eqz v2, :cond_5

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_b
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->c()V

    throw v0
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 165
    :catchall_1
    move-exception v0

    const-string v1, "Android Wear NetworkThread finished"

    invoke-static {v9, v1}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->b()V

    .line 167
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/emulator/a;->a:Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    throw v0

    .line 153
    :cond_5
    :try_start_c
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    const-string v2, "Connection closed, waiting"

    invoke-static {v2, v0}, Lcom/google/android/gms/wearable/node/emulator/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :try_start_d
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->c()V
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_4

    :catch_5
    move-exception v0

    :try_start_e
    const-string v2, "Error writing to device"

    invoke-static {v2, v0}, Lcom/google/android/gms/wearable/node/emulator/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :try_start_f
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->c()V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_4

    :catch_6
    move-exception v0

    const/4 v0, 0x4

    :try_start_10
    const-string v2, "Error: Connection attempted from incorrect client"

    invoke-static {v0, v2}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/emulator/a;->c:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->d()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :try_start_11
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->c()V
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_4

    .line 165
    :cond_6
    const-string v0, "Android Wear NetworkThread finished"

    invoke-static {v9, v0}, Lcom/google/android/gms/wearable/node/emulator/a;->a(ILjava/lang/String;)V

    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/emulator/a;->b()V

    .line 167
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/emulator/a;->a:Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    goto/16 :goto_5
.end method
