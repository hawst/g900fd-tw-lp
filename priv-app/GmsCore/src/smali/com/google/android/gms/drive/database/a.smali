.class public final Lcom/google/android/gms/drive/database/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(ZLcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 56
    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 57
    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v2, "%s %s IN (SELECT %s FROM %s WHERE %s)"

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    if-eqz p0, :cond_0

    const-string v0, "NOT"

    :goto_0
    aput-object v0, v3, v5

    sget-object v0, Lcom/google/android/gms/drive/database/model/s;->b:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->a()Lcom/google/android/gms/drive/database/model/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/r;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/gms/drive/database/model/s;->a:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " IN ("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SELECT "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ar;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ar;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/drive/database/model/as;->j:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 68
    :goto_1
    return-object v0

    .line 57
    :cond_0
    const-string v0, ""

    goto/16 :goto_0

    .line 68
    :cond_1
    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v2, "%s %s IN (SELECT %s FROM %s WHERE %s=?)"

    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    if-eqz p0, :cond_2

    const-string v0, "NOT"

    :goto_2
    aput-object v0, v3, v5

    sget-object v0, Lcom/google/android/gms/drive/database/model/s;->b:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->a()Lcom/google/android/gms/drive/database/model/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/r;->e()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    sget-object v0, Lcom/google/android/gms/drive/database/model/s;->a:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_2
.end method

.method public static a(ZLcom/google/android/gms/drive/metadata/CustomPropertyKey;Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 125
    const-string v0, ""

    .line 127
    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/drive/auth/g;->a()Z

    move-result v1

    if-nez v1, :cond_1

    move v2, v3

    .line 130
    :goto_0
    if-eqz v2, :cond_0

    .line 131
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "AND %s=?"

    new-array v5, v3, [Ljava/lang/Object;

    sget-object v6, Lcom/google/android/gms/drive/database/model/aa;->b:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v0, v1, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 135
    :cond_0
    const-string v5, "%s %s IN (SELECT %s FROM %s WHERE %s=? %s AND %s=? AND %s=?)"

    const/16 v1, 0x8

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v4

    if-eqz p0, :cond_2

    const-string v1, "NOT"

    :goto_1
    aput-object v1, v6, v3

    sget-object v1, Lcom/google/android/gms/drive/database/model/aa;->a:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->a()Lcom/google/android/gms/drive/database/model/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/z;->e()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/aa;->d:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v9

    const/4 v1, 0x5

    aput-object v0, v6, v1

    const/4 v0, 0x6

    sget-object v1, Lcom/google/android/gms/drive/database/model/aa;->c:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x7

    sget-object v1, Lcom/google/android/gms/drive/database/model/aa;->e:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 146
    if-eqz v2, :cond_3

    .line 147
    new-array v1, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    iget-wide v4, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    aput-object p3, v1, v8

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 153
    :goto_2
    return-object v0

    :cond_1
    move v2, v4

    .line 127
    goto/16 :goto_0

    .line 135
    :cond_2
    const-string v1, ""

    goto :goto_1

    .line 153
    :cond_3
    new-array v1, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p3, v1, v7

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    goto :goto_2
.end method
