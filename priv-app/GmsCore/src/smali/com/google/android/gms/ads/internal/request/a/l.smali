.class final Lcom/google/android/gms/ads/internal/request/a/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/g/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/request/a/k;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/a/k;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/a/l;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/l;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/request/a/k;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 46
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/l;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/a/k;->d:Lcom/google/android/gms/ads/internal/util/j;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/util/j;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    monitor-exit v1

    .line 58
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/l;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/a/k;->b:Ljava/lang/String;

    const-string v2, "request_id"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 51
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 54
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/gms/ads/internal/request/a/o;

    const/4 v2, 0x1

    invoke-direct {v0, v2, p2}, Lcom/google/android/gms/ads/internal/request/a/o;-><init>(ILjava/util/Map;)V

    .line 56
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/request/a/o;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " request error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/request/a/o;->a:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 57
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/a/l;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/a/k;->d:Lcom/google/android/gms/ads/internal/util/j;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/ads/internal/util/j;->a(Ljava/lang/Object;)V

    .line 58
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
