.class public final Lcom/google/android/gms/plus/service/a/v;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/c;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Landroid/net/Uri;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/v;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 47
    iput p2, p0, Lcom/google/android/gms/plus/service/a/v;->c:I

    .line 48
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/v;->d:Ljava/lang/String;

    .line 49
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/v;->e:Landroid/net/Uri;

    .line 50
    iput-object p5, p0, Lcom/google/android/gms/plus/service/a/v;->f:Ljava/lang/String;

    .line 51
    iput-object p6, p0, Lcom/google/android/gms/plus/service/a/v;->g:Ljava/lang/String;

    .line 52
    iput-object p7, p0, Lcom/google/android/gms/plus/service/a/v;->h:Ljava/lang/String;

    .line 53
    iput-object p8, p0, Lcom/google/android/gms/plus/service/a/v;->b:Lcom/google/android/gms/plus/internal/c;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 59
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/v;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v2, p0, Lcom/google/android/gms/plus/service/a/v;->c:I

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/v;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/a/v;->e:Landroid/net/Uri;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/v;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/v;->g:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/service/a/v;->h:Ljava/lang/String;

    move-object v0, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/plus/b/b;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/MomentsFeed;

    move-result-object v2

    .line 62
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->e()Ljava/util/List;

    move-result-object v3

    .line 64
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 67
    sget-object v0, Lcom/google/android/gms/plus/internal/z;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v5

    move v1, v8

    .line 69
    :goto_0
    if-ge v1, v4, :cond_0

    .line 70
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;

    .line 71
    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 72
    const-string v7, "momentImpl"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;->af_()[B

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 73
    invoke-virtual {v5, v6}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    .line 69
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 76
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/v;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->m()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v3, v2}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 92
    :goto_1
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 79
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 80
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 81
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/v;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-static {v10, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v9, v9}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 85
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/v;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/v;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-static {v10, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v1, v0, v9, v9}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 89
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/v;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v9, v9}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/v;->b:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/v;->b:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    return-void
.end method
