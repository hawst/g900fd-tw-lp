.class public Lcom/google/android/gms/social/location/TimeShare;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/social/location/aj;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/social/location/aj;

    invoke-direct {v0}, Lcom/google/android/gms/social/location/aj;-><init>()V

    sput-object v0, Lcom/google/android/gms/social/location/TimeShare;->CREATOR:Lcom/google/android/gms/social/location/aj;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lcom/google/android/gms/social/location/TimeShare;->a:I

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/social/location/TimeShare;->b:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/google/android/gms/social/location/TimeShare;->c:Ljava/lang/String;

    .line 47
    iput-object p4, p0, Lcom/google/android/gms/social/location/TimeShare;->d:Ljava/lang/String;

    .line 48
    iput-wide p5, p0, Lcom/google/android/gms/social/location/TimeShare;->e:J

    .line 49
    iput-wide p7, p0, Lcom/google/android/gms/social/location/TimeShare;->f:J

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 10

    .prologue
    .line 54
    const/4 v2, 0x1

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/social/location/TimeShare;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 55
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/gms/social/location/TimeShare;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShare;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShare;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShare;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/google/android/gms/social/location/TimeShare;->e:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 97
    if-ne p0, p1, :cond_0

    .line 98
    const/4 v0, 0x1

    .line 104
    :goto_0
    return v0

    .line 100
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/social/location/TimeShare;

    if-eqz v0, :cond_1

    .line 101
    check-cast p1, Lcom/google/android/gms/social/location/TimeShare;

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShare;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/social/location/TimeShare;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 104
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/google/android/gms/social/location/TimeShare;->f:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 109
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/social/location/TimeShare;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 87
    invoke-static {p0, p1}, Lcom/google/android/gms/social/location/aj;->a(Lcom/google/android/gms/social/location/TimeShare;Landroid/os/Parcel;)V

    .line 88
    return-void
.end method
