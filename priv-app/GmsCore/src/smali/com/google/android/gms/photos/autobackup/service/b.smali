.class final Lcom/google/android/gms/photos/autobackup/service/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/service/a/g;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field final synthetic b:Lcom/google/android/gms/photos/autobackup/service/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/service/a;Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/service/b;->b:Lcom/google/android/gms/photos/autobackup/service/a;

    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/service/b;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/b;->b:Lcom/google/android/gms/photos/autobackup/service/a;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/service/a;->a(Lcom/google/android/gms/photos/autobackup/service/a;)Lcom/google/android/gms/photos/autobackup/service/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a()Z

    move-result v0

    .line 128
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/b;->b:Lcom/google/android/gms/photos/autobackup/service/a;

    invoke-static {v1}, Lcom/google/android/gms/photos/autobackup/service/a;->a(Lcom/google/android/gms/photos/autobackup/service/a;)Lcom/google/android/gms/photos/autobackup/service/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/service/a/b;->b()Ljava/lang/String;

    move-result-object v1

    .line 130
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/service/b;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/4 v3, 0x0

    invoke-interface {v2, v3, v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(IZLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v0

    const-string v0, "AutoBackupService"

    const-string v1, "Failed trying to deliver success"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 141
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/b;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(IZLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :goto_0
    return-void

    .line 145
    :catch_0
    move-exception v0

    const-string v0, "AutoBackupService"

    const-string v1, "Failed trying to deliver failure"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
