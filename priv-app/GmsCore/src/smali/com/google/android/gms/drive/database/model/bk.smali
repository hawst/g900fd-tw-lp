.class public final enum Lcom/google/android/gms/drive/database/model/bk;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/bk;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/bk;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/bk;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/bk;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/bk;

.field public static final enum f:Lcom/google/android/gms/drive/database/model/bk;

.field public static final enum g:Lcom/google/android/gms/drive/database/model/bk;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum h:Lcom/google/android/gms/drive/database/model/bk;

.field public static final enum i:Lcom/google/android/gms/drive/database/model/bk;

.field private static final synthetic k:[Lcom/google/android/gms/drive/database/model/bk;


# instance fields
.field private final j:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x0

    const/4 v10, 0x5

    const/4 v9, 0x1

    const/4 v8, 0x2

    .line 44
    new-instance v0, Lcom/google/android/gms/drive/database/model/bk;

    const-string v1, "CONTENT_HASH"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->d()Lcom/google/android/gms/drive/database/model/bj;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "contentHash"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v9, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/gms/drive/database/model/bk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->a:Lcom/google/android/gms/drive/database/model/bk;

    .line 55
    new-instance v0, Lcom/google/android/gms/drive/database/model/bk;

    const-string v1, "ENTRY_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->d()Lcom/google/android/gms/drive/database/model/bj;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "entryId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v9, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/bk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->b:Lcom/google/android/gms/drive/database/model/bk;

    .line 65
    new-instance v0, Lcom/google/android/gms/drive/database/model/bk;

    const-string v1, "CREATOR_IDENTITY"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->d()Lcom/google/android/gms/drive/database/model/bj;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "creatorIdentity"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v9, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/model/bk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->c:Lcom/google/android/gms/drive/database/model/bk;

    .line 72
    new-instance v0, Lcom/google/android/gms/drive/database/model/bk;

    const-string v1, "WRITE_OPEN_TIME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->d()Lcom/google/android/gms/drive/database/model/bj;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "writeOpenTime"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v9, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, Lcom/google/android/gms/drive/database/model/bk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->d:Lcom/google/android/gms/drive/database/model/bk;

    .line 80
    new-instance v0, Lcom/google/android/gms/drive/database/model/bk;

    const-string v1, "BYTES_SENT"

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->d()Lcom/google/android/gms/drive/database/model/bj;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "bytesSent"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->e:Lcom/google/android/gms/drive/database/model/bk;

    .line 89
    new-instance v0, Lcom/google/android/gms/drive/database/model/bk;

    const-string v1, "UPLOAD_URI"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->d()Lcom/google/android/gms/drive/database/model/bj;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "uploadUri"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/bk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->f:Lcom/google/android/gms/drive/database/model/bk;

    .line 101
    new-instance v0, Lcom/google/android/gms/drive/database/model/bk;

    const-string v1, "PREV_CONTENT_HASH"

    const/4 v2, 0x6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->d()Lcom/google/android/gms/drive/database/model/bj;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/16 v4, 0x14

    new-instance v5, Lcom/google/android/gms/drive/database/model/au;

    const-string v6, "prevContentHash"

    sget-object v7, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    const/16 v4, 0x27

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->g:Lcom/google/android/gms/drive/database/model/bk;

    .line 115
    new-instance v0, Lcom/google/android/gms/drive/database/model/bk;

    const-string v1, "BASE_CONTENT_HASH"

    const/4 v2, 0x7

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->d()Lcom/google/android/gms/drive/database/model/bj;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/16 v4, 0x26

    new-instance v5, Lcom/google/android/gms/drive/database/model/au;

    const-string v6, "baseContentHash"

    sget-object v7, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->h:Lcom/google/android/gms/drive/database/model/bk;

    .line 126
    new-instance v0, Lcom/google/android/gms/drive/database/model/bk;

    const-string v1, "BASE_CONTENT_REVISION_ID"

    const/16 v2, 0x8

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->d()Lcom/google/android/gms/drive/database/model/bj;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/16 v4, 0x26

    new-instance v5, Lcom/google/android/gms/drive/database/model/au;

    const-string v6, "baseContentRevisionId"

    sget-object v7, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bk;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->i:Lcom/google/android/gms/drive/database/model/bk;

    .line 39
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/bk;

    sget-object v1, Lcom/google/android/gms/drive/database/model/bk;->a:Lcom/google/android/gms/drive/database/model/bk;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/gms/drive/database/model/bk;->b:Lcom/google/android/gms/drive/database/model/bk;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/bk;->c:Lcom/google/android/gms/drive/database/model/bk;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/bk;->d:Lcom/google/android/gms/drive/database/model/bk;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/gms/drive/database/model/bk;->e:Lcom/google/android/gms/drive/database/model/bk;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/gms/drive/database/model/bk;->f:Lcom/google/android/gms/drive/database/model/bk;

    aput-object v1, v0, v10

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/drive/database/model/bk;->g:Lcom/google/android/gms/drive/database/model/bk;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/gms/drive/database/model/bk;->h:Lcom/google/android/gms/drive/database/model/bk;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/gms/drive/database/model/bk;->i:Lcom/google/android/gms/drive/database/model/bk;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/bk;->k:[Lcom/google/android/gms/drive/database/model/bk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 133
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bk;->j:Lcom/google/android/gms/drive/database/model/ab;

    .line 134
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/bk;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/gms/drive/database/model/bk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bk;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/bk;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/gms/drive/database/model/bk;->k:[Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/bk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/bk;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bk;->j:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bk;->j:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
