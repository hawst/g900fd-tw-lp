.class final Lcom/google/android/gms/photos/autobackup/ui/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/e;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/e;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "auto_backup"

    const-string v2, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/social/j/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 296
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 297
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 298
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/e;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 299
    const/4 v0, 0x1

    return v0
.end method
