.class public final Lcom/google/android/gms/games/g/w;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/games/g/x;

.field public c:Lcom/google/android/gms/games/g/y;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4635
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4636
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/g/w;->a:I

    iput-object v1, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    iput-object v1, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/w;->cachedSize:I

    .line 4637
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4708
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4709
    iget v1, p0, Lcom/google/android/gms/games/g/w;->a:I

    if-eqz v1, :cond_0

    .line 4710
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/w;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4713
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    if-eqz v1, :cond_1

    .line 4714
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4717
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    if-eqz v1, :cond_2

    .line 4718
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4721
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4649
    if-ne p1, p0, :cond_1

    .line 4677
    :cond_0
    :goto_0
    return v0

    .line 4652
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/w;

    if-nez v2, :cond_2

    move v0, v1

    .line 4653
    goto :goto_0

    .line 4655
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/w;

    .line 4656
    iget v2, p0, Lcom/google/android/gms/games/g/w;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/w;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 4657
    goto :goto_0

    .line 4659
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    if-nez v2, :cond_4

    .line 4660
    iget-object v2, p1, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    if-eqz v2, :cond_5

    move v0, v1

    .line 4661
    goto :goto_0

    .line 4664
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    iget-object v3, p1, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/x;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 4665
    goto :goto_0

    .line 4668
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    if-nez v2, :cond_6

    .line 4669
    iget-object v2, p1, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    if-eqz v2, :cond_0

    move v0, v1

    .line 4670
    goto :goto_0

    .line 4673
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    iget-object v3, p1, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 4674
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4682
    iget v0, p0, Lcom/google/android/gms/games/g/w;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 4684
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 4686
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 4688
    return v0

    .line 4684
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/x;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4686
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    invoke-virtual {v1}, Lcom/google/android/gms/games/g/y;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4604
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/w;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/x;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/x;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/g/y;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4694
    iget v0, p0, Lcom/google/android/gms/games/g/w;->a:I

    if-eqz v0, :cond_0

    .line 4695
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/w;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4697
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    if-eqz v0, :cond_1

    .line 4698
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/w;->b:Lcom/google/android/gms/games/g/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4700
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    if-eqz v0, :cond_2

    .line 4701
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/g/w;->c:Lcom/google/android/gms/games/g/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4703
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4704
    return-void
.end method
