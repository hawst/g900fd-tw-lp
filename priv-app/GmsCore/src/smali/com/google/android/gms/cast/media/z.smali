.class final Lcom/google/android/gms/cast/media/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/cast/CastDevice;

.field final b:Ljava/util/Set;

.field c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039
    iput-object p1, p0, Lcom/google/android/gms/cast/media/z;->a:Lcom/google/android/gms/cast/CastDevice;

    .line 1040
    iput-object p2, p0, Lcom/google/android/gms/cast/media/z;->b:Ljava/util/Set;

    .line 1041
    iput-object p3, p0, Lcom/google/android/gms/cast/media/z;->c:Ljava/lang/String;

    .line 1042
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1074
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/google/android/gms/cast/media/z;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1082
    :cond_1
    :goto_0
    return v0

    .line 1077
    :cond_2
    if-eq p1, p0, :cond_1

    .line 1081
    check-cast p1, Lcom/google/android/gms/cast/media/z;

    .line 1082
    iget-object v2, p0, Lcom/google/android/gms/cast/media/z;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v3, p1, Lcom/google/android/gms/cast/media/z;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v2, v3}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/cast/media/z;->b:Ljava/util/Set;

    iget-object v3, p1, Lcom/google/android/gms/cast/media/z;->b:Ljava/util/Set;

    invoke-static {v2, v3}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/cast/media/z;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/cast/media/z;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1089
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/cast/media/z;->a:Lcom/google/android/gms/cast/CastDevice;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/cast/media/z;->b:Ljava/util/Set;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/cast/media/z;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
