.class public final Lcom/google/android/gms/drive/query/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/query/internal/f;


# instance fields
.field private final a:Lcom/google/android/gms/drive/auth/g;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/auth/g;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/drive/query/b;->a:Lcom/google/android/gms/drive/auth/g;

    .line 36
    return-void
.end method

.method private static b(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;
    .locals 3

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    .line 51
    if-nez v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Field unavailable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 31
    new-instance v0, Lcom/google/android/gms/drive/query/j;

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v2, "1=1"

    invoke-direct {v1, v2, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v3, "1=0"

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/metadata/b;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/gms/drive/query/i;->d:Lcom/google/android/gms/drive/metadata/h;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/gms/drive/DriveId;

    new-instance v0, Lcom/google/android/gms/drive/query/j;

    const/4 v1, 0x0

    invoke-static {v1, p2}, Lcom/google/android/gms/drive/database/a;->a(ZLcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, p2}, Lcom/google/android/gms/drive/database/a;->a(ZLcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported field for contains comparison: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 31
    invoke-static {p1}, Lcom/google/android/gms/drive/query/b;->b(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/gms/drive/metadata/a/ay;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/query/j;

    new-instance v2, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s IS NOT NULL"

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v0, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s IS NULL"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    return-object v1
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 31
    sget-object v0, Lcom/google/android/gms/drive/query/i;->j:Lcom/google/android/gms/drive/metadata/i;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p2, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    const-string v4, "value should have only 1 element"

    invoke-static {v1, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    new-instance v1, Lcom/google/android/gms/drive/query/j;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/drive/query/b;->a:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/gms/drive/database/a;->a(ZLcom/google/android/gms/drive/metadata/CustomPropertyKey;Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/drive/query/b;->a:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v4, v5, v0}, Lcom/google/android/gms/drive/database/a;->a(ZLcom/google/android/gms/drive/metadata/CustomPropertyKey;Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    return-object v1

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported field for has comparison: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/query/internal/Operator;Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 31
    invoke-static {p2}, Lcom/google/android/gms/drive/query/b;->b(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/drive/metadata/a/ay;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->B:Lcom/google/android/gms/drive/metadata/internal/a/j;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/google/android/gms/drive/query/i;->b:Lcom/google/android/gms/drive/metadata/i;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "application/vnd.google-apps.folder"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/android/gms/drive/query/j;

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s=?"

    new-array v4, v7, [Ljava/lang/Object;

    sget-object v5, Lcom/google/android/gms/drive/database/model/as;->l:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "application/vnd.google-apps.folder"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s!=?"

    new-array v5, v7, [Ljava/lang/Object;

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->l:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/vnd.google-apps.folder"

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v0, p3}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->a:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->B:Lcom/google/android/gms/drive/metadata/internal/a/j;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->a:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ap;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/gms/drive/query/j;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s!=?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s=?"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/google/android/gms/drive/query/j;

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s=?"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "0"

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s!=?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "0"

    invoke-direct {v3, v2, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    goto :goto_1

    :cond_3
    new-instance v1, Lcom/google/android/gms/drive/query/j;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s=?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s!=?"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_4
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->b:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Lcom/google/android/gms/drive/query/j;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s<?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s>=?"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_5
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->c:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lcom/google/android/gms/drive/query/j;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s<=?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s>?"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_6
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->d:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Lcom/google/android/gms/drive/query/j;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s>?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s<=?"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_7
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->e:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Lcom/google/android/gms/drive/query/j;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s>=?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s<?"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_8
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->i:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Lcom/google/android/gms/drive/query/j;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s LIKE ?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "%"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s NOT LIKE ?"

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "%"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "%"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/query/j;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported operator: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/internal/Operator;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/query/internal/Operator;Ljava/util/List;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/gms/drive/query/internal/Operator;->f:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/drive/query/e;

    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    sget-object v2, Lcom/google/android/gms/drive/database/x;->b:Lcom/google/android/gms/drive/database/x;

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/gms/drive/query/e;-><init>(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/x;Ljava/util/Collection;)V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/query/internal/Operator;->g:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/drive/query/e;

    sget-object v1, Lcom/google/android/gms/drive/database/x;->b:Lcom/google/android/gms/drive/database/x;

    sget-object v2, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/gms/drive/query/e;-><init>(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/x;Ljava/util/Collection;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not implemented logical operation:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/internal/Operator;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/gms/drive/query/d;

    invoke-interface {p1}, Lcom/google/android/gms/drive/query/d;->a()Lcom/google/android/gms/drive/query/d;

    move-result-object v0

    return-object v0
.end method
