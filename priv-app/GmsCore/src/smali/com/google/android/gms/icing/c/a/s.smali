.class public final Lcom/google/android/gms/icing/c/a/s;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 4708
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4709
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->c:J

    iput-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/s;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/s;->cachedSize:I

    .line 4710
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 4789
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4790
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4791
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4794
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4795
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4798
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 4799
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4802
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 4803
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4806
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 4724
    if-ne p1, p0, :cond_1

    .line 4725
    const/4 v0, 0x1

    .line 4751
    :cond_0
    :goto_0
    return v0

    .line 4727
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/s;

    if-eqz v1, :cond_0

    .line 4730
    check-cast p1, Lcom/google/android/gms/icing/c/a/s;

    .line 4731
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 4732
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4738
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 4739
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4745
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/c/a/s;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 4748
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/c/a/s;->d:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 4751
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/s;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4735
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 4742
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 4756
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4759
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 4761
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/c/a/s;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 4763
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/c/a/s;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 4765
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/s;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4766
    return v0

    .line 4756
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4759
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 4679
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/s;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/s;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/s;->d:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 4772
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4773
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/s;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4775
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4776
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/s;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4778
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/icing/c/a/s;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 4779
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 4781
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/icing/c/a/s;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 4782
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/s;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 4784
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4785
    return-void
.end method
