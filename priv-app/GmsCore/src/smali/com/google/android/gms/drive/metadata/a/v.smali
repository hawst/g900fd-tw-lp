.class final Lcom/google/android/gms/drive/metadata/a/v;
.super Lcom/google/android/gms/drive/metadata/a/an;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 698
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/a/an;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 698
    if-nez p2, :cond_0

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/a;->a()Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, p2}, Lcom/google/android/gms/drive/database/model/ah;->a(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 698
    check-cast p3, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    const-string v0, "app"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    iget-wide v2, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/drive/database/model/ah;->a(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;Ljava/lang/Long;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected final synthetic a(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 698
    check-cast p2, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->a()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p2}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    new-instance v4, Lcom/google/android/gms/drive/internal/model/Property;

    invoke-direct {v4}, Lcom/google/android/gms/drive/internal/model/Property;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/google/android/gms/drive/internal/model/Property;->e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/Property;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "PUBLIC"

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/gms/drive/internal/model/Property;->g(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/Property;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/drive/internal/model/Property;->f(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/Property;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v1, "PRIVATE"

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v2}, Lcom/google/android/gms/drive/internal/model/File;->b(Ljava/util/List;)Lcom/google/android/gms/drive/internal/model/File;

    return-void
.end method
