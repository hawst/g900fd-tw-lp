.class final Lcom/google/android/gms/plus/sharebox/at;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 56
    sput v0, Lcom/google/android/gms/plus/sharebox/at;->a:I

    .line 67
    sput v0, Lcom/google/android/gms/plus/sharebox/at;->b:I

    return-void
.end method

.method public static a(Ljava/util/ArrayList;)Lcom/google/android/gms/common/people/data/Audience;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 255
    if-nez p0, :cond_0

    move-object v0, v1

    .line 271
    :goto_0
    return-object v0

    .line 258
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 259
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 261
    sget-object v0, Lcom/google/android/gms/plus/c/a;->I:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 262
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    if-ge v2, v5, :cond_1

    .line 263
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v1, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 266
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v3, v0, :cond_2

    .line 267
    const-string v0, "GooglePlusPlatform"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Only "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " recipients can be pre-populated in a post.  Some recipients were removed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/plus/sharebox/a/b;)Lcom/google/android/gms/plus/data/a/a;
    .locals 3

    .prologue
    .line 279
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 280
    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string v1, "url"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v1, "type"

    const-string v2, "article"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    if-nez p0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/a/b;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 287
    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/a/b;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 288
    const-string v1, "title"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/a/b;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/a/b;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 292
    const-string v1, "thumbnailUrl"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/a/b;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/a/b;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 296
    const-string v1, "description"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/a/b;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_2
    new-instance v1, Lcom/google/android/gms/plus/data/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/data/a/a;-><init>(Landroid/content/ContentValues;)V

    return-object v1
.end method

.method public static a(Landroid/app/Activity;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 143
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 144
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 148
    const-string v0, "com.google.android.gms"

    .line 158
    :cond_0
    :goto_0
    return-object v0

    .line 152
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v2

    .line 154
    if-eqz v2, :cond_0

    const-string v2, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 155
    const-string v0, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 320
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    :goto_0
    return-object p1

    .line 325
    :cond_0
    array-length v0, p3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 326
    aget-object p1, p3, v3

    goto :goto_0

    .line 330
    :cond_1
    invoke-static {p2}, Lcom/google/android/gms/plus/sharebox/at;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 331
    const-string p1, "<<default account>>"

    goto :goto_0

    .line 335
    :cond_2
    const-string v0, "pref_com.google.android.gms.plus.sharebox"

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 336
    const-string v1, "pref_global_account_name"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 339
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    array-length v1, p3

    if-eqz v1, :cond_3

    .line 340
    aget-object v0, p3, v3

    .line 341
    invoke-static {p0, v0, p2}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_3
    move-object p1, v0

    .line 344
    goto :goto_0
.end method

.method public static a(Landroid/text/Spannable;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 78
    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-interface {p0, v2, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 79
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 82
    if-nez v0, :cond_1

    move v1, v2

    :goto_0
    move v4, v2

    .line 83
    :goto_1
    if-ge v4, v1, :cond_4

    .line 84
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v2

    .line 85
    :goto_2
    add-int/lit8 v7, v6, 0x1

    if-ge v3, v7, :cond_0

    .line 86
    aget-object v7, v0, v4

    invoke-static {v7}, Lcom/google/android/gms/plus/sharebox/MentionSpan;->a(Landroid/text/style/URLSpan;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 87
    if-gt v6, v3, :cond_2

    .line 88
    aget-object v3, v0, v4

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_0
    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 82
    :cond_1
    array-length v1, v0

    goto :goto_0

    .line 90
    :cond_2
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {p0, v7}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    aget-object v8, v0, v4

    invoke-interface {p0, v8}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    if-le v7, v8, :cond_3

    .line 92
    aget-object v6, v0, v4

    invoke-virtual {v5, v3, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_3

    .line 85
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 99
    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 100
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 101
    new-array v4, v1, [I

    .line 102
    new-array v6, v1, [I

    .line 104
    :goto_4
    if-ge v2, v1, :cond_7

    .line 105
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/style/URLSpan;

    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 107
    const-string v7, "g:"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 109
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "+"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v8, Lcom/google/android/gms/plus/sharebox/at;->a:I

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    :goto_5
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    aput v0, v4, v2

    .line 121
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    aput v0, v6, v2

    .line 104
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 111
    :cond_5
    const-string v7, "e:"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 113
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "+"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v8, Lcom/google/android/gms/plus/sharebox/at;->b:I

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 117
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "+"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 124
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 125
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    :goto_6
    if-ltz v1, :cond_8

    .line 126
    aget v5, v4, v1

    aget v7, v6, v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v5, v7, v0}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_6

    .line 129
    :cond_8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Landroid/view/View;IZ)V
    .locals 1

    .prologue
    .line 482
    if-eqz p2, :cond_0

    .line 483
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 486
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 487
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 488
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 489
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 491
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 493
    :cond_0
    return-void
.end method

.method protected static a(Landroid/app/Activity;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 391
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 392
    const-string v2, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 413
    :cond_0
    :goto_0
    return v0

    .line 395
    :cond_1
    const-string v2, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 397
    const-string v1, "ShareBox"

    const-string v2, "Cannot override the calling package when using the action: com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 401
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 402
    const-string v1, "ShareBox"

    const-string v2, "Must use startActivityForResult when using the action: com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 407
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    .line 409
    if-eqz v1, :cond_0

    .line 413
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 356
    invoke-static {p2}, Lcom/google/android/gms/plus/sharebox/at;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    :goto_0
    return v0

    .line 360
    :cond_0
    const-string v1, "pref_com.google.android.gms.plus.sharebox"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 362
    const-string v1, "pref_global_account_name"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 363
    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 365
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 208
    const-string v0, "com.google.android.apps.plus.GOOGLE_INTERACTIVE_POST"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 373
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 382
    :cond_0
    :goto_0
    return v0

    .line 378
    :cond_1
    const-string v1, "com.google.android.gms"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 382
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/app/Activity;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 166
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 167
    const-string v2, "com.google.android.apps.plus.GOOGLE_INTERACTIVE_POST"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 201
    :goto_0
    return v0

    .line 171
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    .line 172
    invoke-static {p0}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    .line 177
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 178
    :cond_1
    const-string v1, "GooglePlusPlatform"

    const-string v2, "Must use startActivityForResult when creating an interactive post."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 184
    :cond_2
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/a/b;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/b;

    move-result-object v2

    .line 185
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/a/b;->b()Z

    move-result v2

    if-nez v2, :cond_4

    .line 186
    :cond_3
    const-string v1, "GooglePlusPlatform"

    const-string v2, "The contentUrl is required when creating an interactive post."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 193
    :cond_4
    const-string v2, "com.google.android.apps.plus.CALL_TO_ACTION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/a/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/a;

    move-result-object v1

    .line 195
    if-nez v1, :cond_5

    .line 196
    const-string v1, "GooglePlusPlatform"

    const-string v2, "Call to action data is required when creating an interactive post."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 201
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static b(Landroid/app/Activity;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 422
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 423
    const-string v2, "com.google.android.gms.plus.action.REPLY_INTERNAL_GOOGLE"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 445
    :cond_0
    :goto_0
    return v0

    .line 426
    :cond_1
    const-string v2, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 428
    const-string v1, "ReplyBox"

    const-string v2, "Cannot override the calling package when using the action: com.google.android.gms.plus.action.REPLY_INTERNAL_GOOGLE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 432
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 433
    const-string v1, "ReplyBox"

    const-string v2, "Must use startActivityForResult when using the action: com.google.android.gms.plus.action.REPLY_INTERNAL_GOOGLE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 439
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    .line 441
    if-eqz v1, :cond_0

    .line 445
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 239
    const-string v1, "com.google.android.apps.plus.GOOGLE_INTERACTIVE_POST"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 248
    :cond_0
    :goto_0
    return v0

    .line 243
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/a/b;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/b;

    move-result-object v1

    .line 244
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/a/b;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(Landroid/app/Activity;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 218
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 219
    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/at;->b(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 232
    :goto_0
    return v0

    .line 223
    :cond_0
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/a/b;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/b;

    move-result-object v1

    .line 224
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/a/b;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/a/b;->b()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/a/b;->e()Z

    move-result v1

    if-nez v1, :cond_2

    .line 227
    :cond_1
    const-string v1, "GooglePlusPlatform"

    const-string v2, "Mobile deep-link IDs must specify metadata."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 232
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static c(Landroid/app/Activity;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 473
    if-nez p0, :cond_1

    .line 476
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v0

    :goto_1
    if-eqz v1, :cond_0

    :cond_2
    const-string v1, "youTubeComments"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const-string v2, "com.google.android.gms.plus.action.REPLY_INTERNAL_GOOGLE"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1
.end method

.method protected static c(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 452
    if-nez p0, :cond_0

    .line 453
    const/4 v0, 0x0

    .line 455
    :goto_0
    return v0

    :cond_0
    const-string v0, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
