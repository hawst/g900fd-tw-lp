.class final Lcom/google/android/gms/fitness/sensors/g/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field final b:Lcom/google/android/gms/fitness/data/Device;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/gms/fitness/data/Device;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/g/i;->a:Ljava/util/List;

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/g/i;->b:Lcom/google/android/gms/fitness/data/Device;

    .line 25
    return-void
.end method

.method public static a(Lcom/google/android/gms/wearable/m;)Lcom/google/android/gms/fitness/sensors/g/i;
    .locals 5

    .prologue
    .line 52
    const-string v0, "local_device"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/m;->i(Ljava/lang/String;)Lcom/google/android/gms/wearable/m;

    move-result-object v0

    .line 53
    sget-object v1, Lcom/google/android/gms/fitness/sensors/g/b;->e:Lcom/google/android/gms/fitness/sensors/g/a;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/sensors/g/a;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Device;

    .line 54
    const-string v1, "all_data_sources"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wearable/m;->k(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 57
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/m;

    .line 58
    sget-object v4, Lcom/google/android/gms/fitness/sensors/g/b;->c:Lcom/google/android/gms/fitness/sensors/g/a;

    invoke-interface {v4, v1}, Lcom/google/android/gms/fitness/sensors/g/a;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 60
    :cond_0
    new-instance v1, Lcom/google/android/gms/fitness/sensors/g/i;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/fitness/sensors/g/i;-><init>(Ljava/util/List;Lcom/google/android/gms/fitness/data/Device;)V

    return-object v1
.end method
