.class final Lcom/google/android/gms/wearable/node/bc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wearable/node/bb;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/concurrent/Callable;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wearable/node/bb;Ljava/lang/String;Ljava/util/concurrent/Callable;)V
    .locals 0

    .prologue
    .line 474
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/bc;->a:Lcom/google/android/gms/wearable/node/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475
    iput-object p2, p0, Lcom/google/android/gms/wearable/node/bc;->b:Ljava/lang/String;

    .line 476
    iput-object p3, p0, Lcom/google/android/gms/wearable/node/bc;->c:Ljava/util/concurrent/Callable;

    .line 477
    return-void
.end method

.method private a()Ljava/lang/Void;
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 484
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/ax;

    .line 485
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/ax;->b()V

    .line 486
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 487
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bc;->c:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Void;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    const-string v1, "wearable"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 501
    const-string v1, "wearable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CallableWrapper ending for thread "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bc;->a:Lcom/google/android/gms/wearable/node/bb;

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bc;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wearable/node/bb;->a(Ljava/lang/String;)V

    .line 506
    :goto_0
    return-object v0

    .line 488
    :catch_0
    move-exception v0

    .line 489
    :try_start_1
    const-string v1, "wearable"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 490
    const-string v1, "wearable"

    const-string v2, "Reader or writer threw an IOException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500
    :cond_1
    const-string v0, "wearable"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 501
    const-string v0, "wearable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallableWrapper ending for thread "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bc;->a:Lcom/google/android/gms/wearable/node/bb;

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bc;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/bb;->a(Ljava/lang/String;)V

    .line 506
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 492
    :catch_1
    move-exception v0

    .line 493
    :try_start_2
    const-string v1, "wearable"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 494
    const-string v1, "wearable"

    const-string v2, "Reader or writer was interrupted"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 496
    :cond_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 500
    const-string v0, "wearable"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 501
    const-string v0, "wearable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallableWrapper ending for thread "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bc;->a:Lcom/google/android/gms/wearable/node/bb;

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bc;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/bb;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 497
    :catch_2
    move-exception v0

    .line 498
    :try_start_3
    const-string v1, "wearable"

    const-string v2, "Unexpected exception in reader or writer:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 500
    const-string v0, "wearable"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 501
    const-string v0, "wearable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CallableWrapper ending for thread "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bc;->a:Lcom/google/android/gms/wearable/node/bb;

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bc;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/bb;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 500
    :catchall_0
    move-exception v0

    const-string v1, "wearable"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 501
    const-string v1, "wearable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CallableWrapper ending for thread "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bc;->a:Lcom/google/android/gms/wearable/node/bb;

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bc;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wearable/node/bb;->a(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 471
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bc;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
