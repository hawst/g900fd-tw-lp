.class public final Lcom/google/android/gms/reminders/sync/b;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/reminders/sync/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 47
    new-instance v0, Lcom/google/android/gms/reminders/sync/d;

    new-instance v1, Lcom/google/android/gms/reminders/sync/c;

    invoke-direct {v1, p1}, Lcom/google/android/gms/reminders/sync/c;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/reminders/sync/d;-><init>(Lcom/google/android/gms/common/server/q;)V

    iput-object v0, p0, Lcom/google/android/gms/reminders/sync/b;->a:Lcom/google/android/gms/reminders/sync/d;

    .line 48
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/f/a/a/t;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Lcom/google/f/a/a/t;

    invoke-direct {v0}, Lcom/google/f/a/a/t;-><init>()V

    .line 173
    const-string v1, "Reminders-Android"

    invoke-static {p0, v1}, Lcom/google/android/gms/common/server/ab;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    .line 174
    return-object v0
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 10

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/sync/b;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 55
    if-nez p2, :cond_0

    .line 56
    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->c()I

    .line 134
    :goto_0
    return-void

    .line 60
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "onPerformSync extras: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " authority: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " syncResult: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    .line 62
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 63
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "extra key: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    goto :goto_1

    .line 67
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "feed"

    sget-object v0, Lcom/google/android/gms/reminders/b/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "periodic"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v3, "interval_seconds"

    sget-object v0, Lcom/google/android/gms/reminders/b/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v3, "com.google.android.gms.reminders"

    sget-object v0, Lcom/google/android/gms/reminders/b/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p1, v3, v2, v4, v5}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 69
    const-string v0, "initialize"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 70
    const-string v0, "upload"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 71
    const-string v0, "periodic"

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 72
    const-string v0, "feed"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "feed"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    :goto_2
    sget-object v2, Lcom/google/android/gms/reminders/b/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 76
    if-nez v6, :cond_3

    if-nez v7, :cond_3

    if-nez v8, :cond_3

    if-nez v9, :cond_3

    .line 77
    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->b()I

    goto/16 :goto_0

    .line 72
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 81
    :cond_3
    if-eqz v6, :cond_4

    .line 82
    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    .line 83
    const/4 v0, 0x1

    invoke-static {p1, p3, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 84
    const/4 v0, 0x1

    invoke-static {p1, p3, v0}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 88
    :cond_4
    if-eqz v8, :cond_5

    .line 89
    const-string v0, "interval_seconds"

    const-wide/16 v2, -0x1

    invoke-virtual {p2, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Performing periodic sync on interval "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " seconds."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    .line 94
    :cond_5
    new-instance v3, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v0, v2, v4, v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget-object v0, Lcom/google/android/gms/reminders/b/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 98
    invoke-static {v1, p1}, Lcom/google/android/gms/reminders/a/a;->a(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/gms/reminders/a/d;

    move-result-object v4

    .line 100
    if-nez v4, :cond_6

    .line 101
    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->c()I

    goto/16 :goto_0

    .line 105
    :cond_6
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com.google.android.gms.reminders"

    sget-object v0, Lcom/google/android/gms/reminders/b/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/reminders/b/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    invoke-static {v1, p1, v2, v0, v5}, Lcom/google/android/gsf/n;->a(Landroid/content/ContentResolver;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Z

    .line 110
    :try_start_0
    new-instance v0, Lcom/google/android/gms/reminders/sync/e;

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/sync/b;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/reminders/sync/b;->a:Lcom/google/android/gms/reminders/sync/d;

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/reminders/sync/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/reminders/sync/d;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/reminders/a/d;Landroid/content/SyncResult;)V

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/sync/e;->a()I

    move-result v0

    .line 113
    if-eqz v7, :cond_7

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    .line 115
    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 125
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->d()I

    .line 126
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    goto/16 :goto_0

    .line 117
    :cond_7
    if-eqz v7, :cond_8

    .line 118
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    .line 120
    :cond_8
    if-eqz v6, :cond_a

    const/4 v5, 0x4

    .line 122
    :goto_3
    new-instance v0, Lcom/google/android/gms/reminders/sync/a;

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/sync/b;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/reminders/sync/b;->a:Lcom/google/android/gms/reminders/sync/d;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/reminders/sync/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/reminders/sync/d;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/reminders/a/d;I)V

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/sync/a;->a()Z
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 127
    :catch_1
    move-exception v0

    .line 128
    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->d()I

    .line 129
    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v1, :cond_9

    .line 130
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Volley Error Status:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->d()I

    .line 132
    :cond_9
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_0

    .line 120
    :cond_a
    if-eqz v7, :cond_b

    const/4 v5, 0x3

    goto :goto_3

    :cond_b
    if-eqz v8, :cond_c

    const/4 v5, 0x2

    goto :goto_3

    :cond_c
    if-eqz v9, :cond_d

    const/4 v5, 0x1

    goto :goto_3

    :cond_d
    const/4 v5, 0x0

    goto :goto_3
.end method
