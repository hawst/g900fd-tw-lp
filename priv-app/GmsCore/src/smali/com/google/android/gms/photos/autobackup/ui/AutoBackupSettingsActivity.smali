.class public Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;
.super Lcom/google/android/libraries/social/a/a/c;
.source "SourceFile"


# static fields
.field private static final c:Landroid/net/Uri;

.field private static final d:[I


# instance fields
.field private e:Lcom/google/android/gms/common/api/v;

.field private f:Landroid/preference/Preference;

.field private g:Landroid/app/ProgressDialog;

.field private h:Lcom/google/android/gms/photos/autobackup/ui/aj;

.field private i:Lcom/google/android/gms/photos/PhotosListPreference;

.field private j:Landroid/preference/ListPreference;

.field private k:Lcom/google/android/gms/photos/PhotosListPreference;

.field private l:Landroid/preference/CheckBoxPreference;

.field private m:Landroid/preference/CheckBoxPreference;

.field private n:[Ljava/lang/String;

.field private o:[Ljava/lang/String;

.field private p:[Ljava/lang/String;

.field private q:Ljava/util/Map;

.field private final r:Lcom/google/android/gms/photos/autobackup/l;

.field private final s:Lcom/google/android/gms/photos/autobackup/ui/ae;

.field private final t:Lcom/google/android/gms/photos/autobackup/ui/af;

.field private final u:Lcom/google/android/gms/common/api/aq;

.field private final v:Lcom/google/android/gms/common/api/aq;

.field private final w:Lcom/google/android/gms/common/api/aq;

.field private final x:Lcom/google/android/gms/common/api/aq;

.field private final y:Lcom/google/android/gms/common/api/aq;

.field private final z:Landroid/preference/Preference$OnPreferenceChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57
    const-string v0, "https://www.google.com/settings/storage/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->c:Landroid/net/Uri;

    .line 58
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/gms/p;->so:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/gms/p;->dC:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/gms/p;->dB:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->d:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/libraries/social/a/a/c;-><init>()V

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->q:Ljava/util/Map;

    .line 77
    new-instance v0, Lcom/google/android/gms/photos/autobackup/l;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    .line 78
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/ui/ae;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->s:Lcom/google/android/gms/photos/autobackup/ui/ae;

    .line 79
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/af;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->b:Lcom/google/android/libraries/social/i/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/photos/autobackup/ui/af;-><init>(Landroid/app/Activity;Lcom/google/android/libraries/social/i/w;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->t:Lcom/google/android/gms/photos/autobackup/ui/af;

    .line 83
    new-instance v0, Lcom/google/android/gms/photos/autobackup/m;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->b:Lcom/google/android/libraries/social/i/i;

    new-instance v2, Lcom/google/android/gms/photos/autobackup/ui/a;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/autobackup/ui/a;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/m;-><init>(Lcom/google/android/libraries/social/i/w;Lcom/google/android/gms/photos/autobackup/n;)V

    .line 94
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/t;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->b:Lcom/google/android/libraries/social/i/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/photos/autobackup/ui/t;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/i/w;)V

    .line 97
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/j;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->u:Lcom/google/android/gms/common/api/aq;

    .line 113
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/k;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->v:Lcom/google/android/gms/common/api/aq;

    .line 121
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/l;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->w:Lcom/google/android/gms/common/api/aq;

    .line 141
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/m;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->x:Lcom/google/android/gms/common/api/aq;

    .line 153
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/n;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->y:Lcom/google/android/gms/common/api/aq;

    .line 163
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/o;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->z:Landroid/preference/Preference$OnPreferenceChangeListener;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/photos/autobackup/l;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/photos/autobackup/l;->a(Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->h:Lcom/google/android/gms/photos/autobackup/ui/aj;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->b()Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/google/android/gms/photos/autobackup/ui/aj;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->f:Landroid/preference/Preference;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->j()Lcom/google/android/gms/photos/autobackup/model/UserQuota;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v3}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->f:Landroid/preference/Preference;

    sget v3, Lcom/google/android/gms/p;->ds:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->i:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/photos/PhotosListPreference;->a(I)V

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->i:Lcom/google/android/gms/photos/PhotosListPreference;

    iget-object v4, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->p:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    aget-object v0, v4, v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/photos/PhotosListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->j:Landroid/preference/ListPreference;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    :goto_3
    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->j:Landroid/preference/ListPreference;

    iget-object v4, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->n:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    :goto_4
    aget-object v0, v4, v0

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->k:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->f()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    :goto_5
    invoke-virtual {v3, v0}, Lcom/google/android/gms/photos/PhotosListPreference;->a(I)V

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->k:Lcom/google/android/gms/photos/PhotosListPreference;

    iget-object v4, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->o:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    :goto_6
    aget-object v0, v4, v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/photos/PhotosListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->k:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/PhotosListPreference;->setEnabled(Z)V

    :goto_7
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->l:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->d()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->m:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->e()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    sget-object v0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->d:[I

    array-length v2, v0

    :goto_8
    if-ge v1, v2, :cond_b

    aget v3, v0, v1

    invoke-virtual {p0, v3}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->b()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->f:Landroid/preference/Preference;

    sget v3, Lcom/google/android/gms/p;->dv:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->a()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->f:Landroid/preference/Preference;

    const-string v3, ""

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->b()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-int v0, v4

    invoke-static {p0, v0}, Lcom/google/android/libraries/social/autobackup/c/b;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->f:Landroid/preference/Preference;

    sget v4, Lcom/google/android/gms/p;->du:I

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v0, v5, v1

    invoke-virtual {p0, v4, v5}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_4

    :cond_8
    move v0, v1

    goto/16 :goto_5

    :cond_9
    move v0, v1

    goto/16 :goto_6

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->k:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/photos/PhotosListPreference;->setEnabled(Z)V

    goto/16 :goto_7

    :cond_b
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;Ljava/util/List;)V
    .locals 5

    .prologue
    .line 49
    sget v0, Lcom/google/android/gms/p;->sk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->q:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    new-instance v3, Landroid/preference/CheckBoxPreference;

    invoke-direct {v3, p0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->q:Ljava/util/Map;

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->c()Z

    move-result v1

    invoke-virtual {v3, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->z:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v3, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->c:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V
    .locals 3

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/gms/photos/autobackup/d;->c:Lcom/google/android/gms/photos/autobackup/f;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v2}, Lcom/google/android/gms/photos/autobackup/l;->c()Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/f;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->w:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->d()V

    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->d()V

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->g:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->g:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/common/api/aq;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->x:Lcom/google/android/gms/common/api/aq;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 434
    sget-object v0, Lcom/google/android/gms/photos/autobackup/d;->c:Lcom/google/android/gms/photos/autobackup/f;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/f;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->u:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 437
    sget-object v0, Lcom/google/android/gms/photos/autobackup/d;->c:Lcom/google/android/gms/photos/autobackup/f;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v2}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/f;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->v:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 440
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->g:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 470
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->c()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->d()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->q:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/common/api/aq;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->y:Lcom/google/android/gms/common/api/aq;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 49
    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "com.google"

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/common/a;->a([Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v2}, Lcom/google/android/gms/photos/autobackup/l;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/photos/PhotosListPreference;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->i:Lcom/google/android/gms/photos/PhotosListPreference;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Landroid/preference/ListPreference;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->j:Landroid/preference/ListPreference;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/photos/PhotosListPreference;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->k:Lcom/google/android/gms/photos/PhotosListPreference;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 179
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/a/a/c;->a(Landroid/os/Bundle;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->a:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/ae;

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->s:Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->a:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/autobackup/l;

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->a:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/s;

    new-instance v2, Lcom/google/android/gms/photos/autobackup/ui/x;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/autobackup/ui/x;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 184
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 489
    if-nez p2, :cond_1

    if-ne p1, v1, :cond_1

    .line 493
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->h:Lcom/google/android/gms/photos/autobackup/ui/aj;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/ui/aj;->a(Z)V

    .line 502
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eq p1, v1, :cond_2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 496
    :cond_2
    const-string v0, "authAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 498
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 499
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->r:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/photos/autobackup/l;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 207
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/a/a/c;->onCreate(Landroid/os/Bundle;)V

    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "plusone:autobackup_allow_migration"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->finish()V

    .line 349
    :goto_0
    return-void

    .line 215
    :cond_0
    sget v0, Lcom/google/android/gms/s;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->addPreferencesFromResource(I)V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 218
    sget v1, Lcom/google/android/gms/c;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->n:[Ljava/lang/String;

    .line 220
    sget v1, Lcom/google/android/gms/c;->o:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->o:[Ljava/lang/String;

    .line 222
    sget v1, Lcom/google/android/gms/c;->n:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->p:[Ljava/lang/String;

    .line 225
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 226
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/ah;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/ah;-><init>(Landroid/preference/PreferenceActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->h:Lcom/google/android/gms/photos/autobackup/ui/aj;

    .line 230
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->h:Lcom/google/android/gms/photos/autobackup/ui/aj;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/p;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/p;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/ui/aj;->a(Lcom/google/android/gms/photos/autobackup/ui/ak;)V

    .line 243
    sget v0, Lcom/google/android/gms/p;->so:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->f:Landroid/preference/Preference;

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->f:Landroid/preference/Preference;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/q;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/q;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 253
    sget v0, Lcom/google/android/gms/p;->sq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/PhotosListPreference;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->i:Lcom/google/android/gms/photos/PhotosListPreference;

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->i:Lcom/google/android/gms/photos/PhotosListPreference;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/b;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/PhotosListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 263
    sget v0, Lcom/google/android/gms/p;->sn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/c;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 278
    sget v0, Lcom/google/android/gms/p;->sf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 280
    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/d;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 290
    sget v0, Lcom/google/android/gms/p;->sh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 292
    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/e;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 303
    sget v0, Lcom/google/android/gms/p;->sg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->j:Landroid/preference/ListPreference;

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->j:Landroid/preference/ListPreference;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/f;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 314
    sget v0, Lcom/google/android/gms/p;->zp:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/PhotosListPreference;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->k:Lcom/google/android/gms/photos/PhotosListPreference;

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->k:Lcom/google/android/gms/photos/PhotosListPreference;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/g;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/PhotosListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 326
    sget v0, Lcom/google/android/gms/p;->sl:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->l:Landroid/preference/CheckBoxPreference;

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->l:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/h;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/h;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 337
    sget v0, Lcom/google/android/gms/p;->sj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->m:Landroid/preference/CheckBoxPreference;

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->m:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/i;-><init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 348
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/photos/autobackup/d;->b:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->s:Lcom/google/android/gms/photos/autobackup/ui/ae;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/autobackup/ui/ae;->a(Lcom/google/android/gms/common/api/v;)V

    goto/16 :goto_0

    .line 228
    :cond_1
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/y;-><init>(Landroid/preference/PreferenceActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->h:Lcom/google/android/gms/photos/autobackup/ui/aj;

    goto/16 :goto_1
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 188
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 189
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 190
    sget v2, Lcom/google/android/gms/p;->yN:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->rw:I

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 193
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 197
    :cond_0
    :goto_0
    return-object v0

    .line 194
    :cond_1
    if-ne p1, v3, :cond_0

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->t:Lcom/google/android/gms/photos/autobackup/ui/af;

    iget v2, v1, Lcom/google/android/gms/photos/autobackup/ui/af;->b:I

    if-ne p1, v2, :cond_0

    iput-boolean v3, v1, Lcom/google/android/gms/photos/autobackup/ui/af;->c:Z

    new-instance v2, Lcom/google/android/gms/photos/autobackup/ui/ab;

    iget-object v1, v1, Lcom/google/android/gms/photos/autobackup/ui/af;->a:Landroid/app/Activity;

    invoke-direct {v2, v1}, Lcom/google/android/gms/photos/autobackup/ui/ab;-><init>(Landroid/content/Context;)V

    const-string v1, "PromptEnableSyncMixin.account_name"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "PromptEnableSyncMixin.master_sync_enabled"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "PromptEnableSyncMixin.account_sync_enabled"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/gms/photos/autobackup/ui/ab;->a(Ljava/lang/String;ZZ)Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 414
    invoke-super {p0}, Lcom/google/android/libraries/social/a/a/c;->onPause()V

    .line 415
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->d()V

    .line 416
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 406
    invoke-super {p0}, Lcom/google/android/libraries/social/a/a/c;->onResume()V

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 408
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->c()V

    .line 410
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 398
    invoke-super {p0}, Lcom/google/android/libraries/social/a/a/c;->onStart()V

    .line 399
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 402
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 420
    invoke-super {p0}, Lcom/google/android/libraries/social/a/a/c;->onStop()V

    .line 421
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 424
    :cond_0
    return-void
.end method
