.class public Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;
.super Lcom/google/android/gms/plus/oob/x;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/oob/aa;
.implements Lcom/google/android/gms/plus/oob/ac;
.implements Lcom/google/android/gms/plus/oob/ah;
.implements Lcom/google/android/gms/plus/oob/e;
.implements Lcom/google/android/gms/plus/oob/w;


# instance fields
.field private d:Lcom/google/android/gms/plus/oob/ag;

.field private e:Landroid/support/v4/app/m;

.field private f:Landroid/support/v4/app/Fragment;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

.field private k:Landroid/view/View;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/x;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 100
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.common.oob.EXTRAS_CLIENT_CALLING_APP_PACKAGE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.OVERRIDE_THEME"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 105
    invoke-virtual {p3, v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/content/Intent;)V

    .line 106
    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/oob/ab;)V
    .locals 2

    .prologue
    .line 239
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 240
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/x;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/m;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v0, v1}, Lcom/google/android/gms/plus/oob/x;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 242
    invoke-direct {p0, p2}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/plus/oob/ab;)V

    .line 243
    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;)V
    .locals 1

    .prologue
    .line 214
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 215
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 226
    :goto_0
    return-void

    .line 218
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;->f()Ljava/lang/String;

    move-result-object v0

    .line 219
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    .line 223
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e()Z

    move-result v0

    invoke-static {v0, p2}, Lcom/google/android/gms/plus/oob/ab;->a(ZLcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;)Lcom/google/android/gms/plus/oob/ab;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/oob/ab;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/plus/oob/ab;)V
    .locals 3

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    .line 248
    const-string v2, "content_fragment"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 249
    sget v0, Lcom/google/android/gms/j;->dD:I

    const-string v2, "content_fragment"

    invoke-virtual {v1, v0, p1, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 253
    :goto_0
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->b()I

    .line 255
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    .line 256
    return-void

    .line 251
    :cond_0
    sget v0, Lcom/google/android/gms/j;->dD:I

    const-string v2, "content_fragment"

    invoke-virtual {v1, v0, p1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    invoke-static {p1, p2}, Lcom/google/android/gms/plus/oob/u;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/oob/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e:Landroid/support/v4/app/m;

    .line 451
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e:Landroid/support/v4/app/m;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "progress_dialog_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 452
    return-void

    .line 447
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/plus/f/e;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/f/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e:Landroid/support/v4/app/m;

    .line 448
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e:Landroid/support/v4/app/m;

    const/4 v1, 0x1

    sget v2, Lcom/google/android/gms/q;->C:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/m;->a(II)V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 229
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->l()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    .line 230
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 235
    :goto_2
    return-void

    .line 229
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 233
    :cond_2
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e()Z

    move-result v1

    invoke-static {v1, p1}, Lcom/google/android/gms/plus/oob/ab;->a(ZLjava/util/ArrayList;)Lcom/google/android/gms/plus/oob/ab;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/oob/ab;)V

    goto :goto_2
.end method

.method private c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 205
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e()Z

    move-result v0

    sget v1, Lcom/google/android/gms/p;->uN:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1, v2}, Lcom/google/android/gms/plus/oob/ab;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/oob/ab;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/oob/ab;)V

    .line 210
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 399
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 421
    :goto_0
    return-void

    .line 402
    :cond_0
    iput v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->m:I

    .line 406
    iget v1, p0, Lcom/google/android/gms/plus/oob/x;->c:I

    if-nez v1, :cond_3

    :goto_1
    if-eqz v0, :cond_2

    .line 408
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->l:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 409
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->l:Landroid/graphics/drawable/Drawable;

    .line 411
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 413
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 415
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 416
    sget v0, Lcom/google/android/gms/p;->uV:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->uW:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 406
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 419
    :cond_4
    sget v0, Lcom/google/android/gms/p;->va:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 424
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    :goto_0
    return-void

    .line 427
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->m:I

    .line 429
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    sget v0, Lcom/google/android/gms/p;->uX:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->uY:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 433
    :cond_1
    sget v0, Lcom/google/android/gms/p;->ve:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private j()Z
    .locals 2

    .prologue
    .line 438
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progress_dialog_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/m;

    .line 440
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 458
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->m:I

    .line 459
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->l()V

    .line 460
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 463
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e:Landroid/support/v4/app/m;

    if-eqz v0, :cond_2

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e:Landroid/support/v4/app/m;

    invoke-virtual {v0}, Landroid/support/v4/app/m;->a()V

    .line 465
    iput-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e:Landroid/support/v4/app/m;

    .line 475
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 476
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 477
    iput-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->l:Landroid/graphics/drawable/Drawable;

    .line 479
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 480
    return-void

    .line 467
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progress_dialog_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/m;

    .line 469
    if-eqz v0, :cond_0

    .line 470
    invoke-virtual {v0}, Landroid/support/v4/app/m;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/plus/oob/y;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/plus/oob/y;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/y;->c()V

    .line 386
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 2

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->k()V

    .line 278
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_2

    .line 279
    :cond_0
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 298
    :cond_1
    :goto_0
    return-void

    .line 282
    :cond_2
    invoke-static {p2}, Lcom/google/android/gms/plus/oob/af;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 283
    sget-object v1, Lcom/google/android/gms/common/analytics/l;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/fk;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;)V

    goto :goto_0

    .line 287
    :cond_3
    invoke-static {p2}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 288
    const-string v0, "UpgradeAccount"

    const-string v1, "Account is already upgraded to G+"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v0}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 290
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f()V

    goto :goto_0

    .line 295
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/plus/oob/y;

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/plus/oob/y;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/oob/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 1

    .prologue
    .line 360
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->i()V

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->d:Lcom/google/android/gms/plus/oob/ag;

    invoke-interface {v0, p1}, Lcom/google/android/gms/plus/oob/ag;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 362
    return-void
.end method

.method public final bridge synthetic b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 41
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/x;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 303
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->k()V

    .line 305
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 306
    :cond_0
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 356
    :goto_0
    return-void

    .line 310
    :cond_1
    invoke-static {p2}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 311
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v0}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f()V

    goto :goto_0

    .line 319
    :cond_2
    invoke-static {p2}, Lcom/google/android/gms/plus/oob/af;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 320
    sget-object v1, Lcom/google/android/gms/common/analytics/l;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/fk;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;)V

    goto :goto_0

    .line 325
    :cond_3
    invoke-static {p2}, Lcom/google/android/gms/plus/oob/af;->d(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_a

    .line 326
    invoke-static {p2}, Lcom/google/android/gms/plus/oob/af;->d(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    .line 327
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->d()Ljava/util/List;

    move-result-object v3

    .line 329
    invoke-static {v0}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 330
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    .line 331
    :goto_2
    if-ge v2, v4, :cond_8

    .line 332
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;

    .line 333
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->e()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 334
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->d()Ljava/lang/String;

    move-result-object v1

    const-string v2, "invalidNameHardFail"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v1, Lcom/google/android/gms/common/analytics/l;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v1}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 335
    :goto_3
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/m;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v1}, Lcom/google/android/gms/plus/oob/x;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e()Z

    move-result v1

    invoke-static {v1, p2, v0}, Lcom/google/android/gms/plus/oob/v;->a(ZLcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;Ljava/lang/String;)Lcom/google/android/gms/plus/oob/v;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->dD:I

    const-string v3, "content_fragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/aj;

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 325
    goto :goto_1

    .line 334
    :cond_5
    const-string v2, "invalidNameAppealable"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lcom/google/android/gms/common/analytics/l;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v1}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_3

    :cond_6
    sget-object v1, Lcom/google/android/gms/common/analytics/l;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-super {p0, v1}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_3

    .line 331
    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 342
    :cond_8
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 344
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 345
    :goto_4
    if-ge v1, v4, :cond_9

    .line 346
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ErrorsEntity;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 348
    :cond_9
    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 352
    :cond_a
    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 353
    const-string v0, "UpgradeAccount"

    const-string v1, "Unhandled error result"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_b
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto/16 :goto_0
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/x;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g()V

    .line 367
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 372
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->onBackPressed()V

    .line 374
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 375
    const-string v1, "content_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/gms/plus/oob/y;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/gms/plus/oob/y;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/y;->c()V

    .line 379
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/x;->onCreate(Landroid/os/Bundle;)V

    .line 113
    if-eqz p1, :cond_0

    .line 114
    const-string v0, "UpgradeAccountActivity.dialogStateKey"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->m:I

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 118
    const-string v1, "com.google.android.gms.common.oob.EXTRAS_PROMO_APP_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g:Ljava/lang/String;

    .line 119
    const-string v1, "com.google.android.gms.common.oob.EXTRAS_PROMO_APP_TEXT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->h:Ljava/lang/String;

    .line 120
    const-string v1, "com.google.android.gms.common.oob.EXTRA_BACK_BUTTON_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->i:Ljava/lang/String;

    .line 121
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->j:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->j:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    const-string v1, ""

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/plus/k;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->k:Landroid/view/View;

    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 126
    sget v0, Lcom/google/android/gms/l;->ey:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->setContentView(I)V

    .line 128
    invoke-static {p0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "debug_no_connection"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e()Z

    move-result v0

    sget v1, Lcom/google/android/gms/p;->vd:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->vc:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/plus/oob/ab;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/oob/ab;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Lcom/google/android/gms/plus/oob/ab;)V

    .line 140
    :cond_2
    :goto_0
    return-void

    .line 138
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "content_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->e()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->i:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/gms/plus/oob/y;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/oob/y;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->dD:I

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->f:Landroid/support/v4/app/Fragment;

    const-string v3, "content_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->h()V

    .line 139
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v0, "upgrade_account_fragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/oob/ai;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->j:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/plus/oob/ai;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)Lcom/google/android/gms/plus/oob/ai;

    move-result-object v0

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "upgrade_account_fragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->d:Lcom/google/android/gms/plus/oob/ag;

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/ai;->a()Lcom/google/android/gms/plus/oob/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->d:Lcom/google/android/gms/plus/oob/ag;

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 164
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/x;->onPause()V

    .line 165
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->l()V

    .line 166
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/x;->onResume()V

    .line 152
    iget v0, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->m:I

    packed-switch v0, :pswitch_data_0

    .line 160
    :goto_0
    return-void

    .line 154
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->h()V

    goto :goto_0

    .line 157
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->i()V

    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/x;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 145
    const-string v0, "UpgradeAccountActivity.dialogStateKey"

    iget v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    return-void
.end method
