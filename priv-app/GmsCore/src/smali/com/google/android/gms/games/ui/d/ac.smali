.class public final Lcom/google/android/gms/games/ui/d/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/ui/q;

.field public b:Landroid/support/v7/widget/SearchView;

.field public c:I

.field public d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field public f:Z

.field g:Landroid/os/Handler;

.field public h:Z

.field i:Z

.field protected j:Ljava/lang/Runnable;

.field private k:Lcom/google/android/gms/games/ui/d/ag;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/d/ag;I)V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/ui/d/ac;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/d/ag;IB)V

    .line 98
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/d/ag;IB)V
    .locals 2

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Lcom/google/android/gms/games/ui/d/ad;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/d/ad;-><init>(Lcom/google/android/gms/games/ui/d/ac;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/ac;->j:Ljava/lang/Runnable;

    .line 118
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    .line 119
    iput-object p2, p0, Lcom/google/android/gms/games/ui/d/ac;->k:Lcom/google/android/gms/games/ui/d/ag;

    .line 120
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/ac;->g:Landroid/os/Handler;

    .line 121
    iput p3, p0, Lcom/google/android/gms/games/ui/d/ac;->c:I

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d/ac;->h:Z

    .line 123
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d/ac;->f:Z

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/ac;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/ac;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 160
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 328
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/ac;->d:Ljava/lang/String;

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/ac;->k:Lcom/google/android/gms/games/ui/d/ag;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/d/ag;->a(Ljava/lang/String;)V

    .line 330
    return-void
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/ac;->a:Lcom/google/android/gms/games/ui/q;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/q;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 318
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v1}, Landroid/support/v7/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 320
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/ac;->b:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 395
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 396
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/d/ac;->a(Ljava/lang/String;)V

    .line 397
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/d/ac;->b()V

    .line 399
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
