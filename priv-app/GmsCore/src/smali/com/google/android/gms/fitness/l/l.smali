.class interface abstract Lcom/google/android/gms/fitness/l/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field public static final e:Ljava/lang/String;

.field public static final f:Ljava/lang/String;

.field public static final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 325
    const-string v0, "CREATE TABLE %s (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "BleDevices"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "_id INTEGER PRIMARY KEY AUTOINCREMENT"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "type TEXT"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "name TEXT"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "address NOT NULL"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "device_name TEXT"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "model_number TEXT"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "manufacturer TEXT"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "hardware_revision TEXT"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "firmware_revision TEXT"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "software_revision TEXT"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/l;->a:Ljava/lang/String;

    .line 339
    const-string v0, "BleDevices"

    const-string v1, "device_name"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/l;->b:Ljava/lang/String;

    .line 340
    const-string v0, "BleDevices"

    const-string v1, "model_number"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/l;->c:Ljava/lang/String;

    .line 341
    const-string v0, "BleDevices"

    const-string v1, "manufacturer"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/l;->d:Ljava/lang/String;

    .line 342
    const-string v0, "BleDevices"

    const-string v1, "hardware_revision"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/l;->e:Ljava/lang/String;

    .line 343
    const-string v0, "BleDevices"

    const-string v1, "firmware_revision"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/l;->f:Ljava/lang/String;

    .line 344
    const-string v0, "BleDevices"

    const-string v1, "software_revision"

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/h;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/l;->g:Ljava/lang/String;

    return-void
.end method
