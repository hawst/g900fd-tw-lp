.class public Lcom/google/android/gms/photos/autobackup/model/UserQuota;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field private b:J

.field private c:J

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/i;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/model/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->a:I

    .line 56
    return-void
.end method

.method public constructor <init>(IJJZZ)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->a:I

    .line 48
    iput-wide p2, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->b:J

    .line 49
    iput-wide p4, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->c:J

    .line 50
    iput-boolean p6, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->d:Z

    .line 51
    iput-boolean p7, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->e:Z

    .line 52
    return-void
.end method

.method public constructor <init>(JJZZ)V
    .locals 9

    .prologue
    .line 60
    const/4 v1, 0x1

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/photos/autobackup/model/UserQuota;-><init>(IJJZZ)V

    .line 62
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->b:J

    return-wide v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->c:J

    return-wide v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->d:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/UserQuota;->e:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 99
    invoke-static {p0, p1}, Lcom/google/android/gms/photos/autobackup/model/i;->a(Lcom/google/android/gms/photos/autobackup/model/UserQuota;Landroid/os/Parcel;)V

    .line 100
    return-void
.end method
