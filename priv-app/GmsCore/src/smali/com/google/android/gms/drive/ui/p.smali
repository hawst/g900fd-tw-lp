.class public final Lcom/google/android/gms/drive/ui/p;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/app/Activity;)V
    .locals 12

    .prologue
    .line 27
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/g/r;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10102

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v4

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v3

    sget v5, Lcom/google/android/gms/g;->t:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sget v6, Lcom/google/android/gms/g;->u:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sget v7, Lcom/google/android/gms/g;->v:I

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const-wide v8, 0x3fb999999999999aL    # 0.1

    int-to-double v10, v4

    mul-double/2addr v8, v10

    double-to-int v7, v8

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    mul-int/lit8 v7, v7, 0x2

    sub-int v7, v4, v7

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    mul-int/lit8 v1, v5, 0x2

    sub-int v1, v3, v1

    invoke-static {v6, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    iget v1, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int v1, v4, v1

    shr-int/lit8 v1, v1, 0x1

    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    const/16 v1, 0x31

    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->alpha:F

    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    invoke-virtual {v0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 30
    :cond_0
    return-void
.end method
