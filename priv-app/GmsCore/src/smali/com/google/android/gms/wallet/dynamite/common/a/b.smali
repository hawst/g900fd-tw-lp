.class public final Lcom/google/android/gms/wallet/dynamite/common/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "[_-]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 21
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 23
    new-instance v1, Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, p0, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILjava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 62
    :goto_0
    return v0

    .line 58
    :cond_0
    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    .line 59
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 60
    goto :goto_0

    .line 58
    :sswitch_0
    const-string v0, "hy"

    goto :goto_1

    :sswitch_1
    const-string v0, "zh"

    goto :goto_1

    :sswitch_2
    const-string v0, "ja"

    goto :goto_1

    :sswitch_3
    const-string v0, "ko"

    goto :goto_1

    :sswitch_4
    const-string v0, "th"

    goto :goto_1

    :sswitch_5
    const-string v0, "vi"

    goto :goto_1

    .line 62
    :cond_1
    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 58
    nop

    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_0
        0x6e -> :sswitch_1
        0x10b -> :sswitch_1
        0x150 -> :sswitch_2
        0x170 -> :sswitch_3
        0x172 -> :sswitch_3
        0x1af -> :sswitch_1
        0x288 -> :sswitch_4
        0x297 -> :sswitch_1
        0x2ce -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v0

    .line 40
    :cond_1
    sget-object v1, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v0

    .line 41
    sget-object v2, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v0

    .line 42
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
