.class public final Lcom/google/android/gms/drive/b/k;
.super Ljava/io/FilterOutputStream;
.source "SourceFile"


# instance fields
.field private final a:Ljava/security/MessageDigest;

.field private b:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 19
    const-string v0, "SHA-256"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/k;->a:Ljava/security/MessageDigest;

    .line 28
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/google/android/gms/drive/b/k;->b:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/drive/b/k;->flush()V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/drive/b/k;->close()V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/drive/b/k;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/e;->a([BZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final write(I)V
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/drive/b/k;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 50
    iget-wide v0, p0, Lcom/google/android/gms/drive/b/k;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/drive/b/k;->b:J

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/drive/b/k;->a:Ljava/security/MessageDigest;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update(B)V

    .line 52
    return-void
.end method

.method public final write([B)V
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/drive/b/k;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 57
    iget-wide v0, p0, Lcom/google/android/gms/drive/b/k;->b:J

    array-length v2, p1

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/drive/b/k;->b:J

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/drive/b/k;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 59
    return-void
.end method

.method public final write([BII)V
    .locals 4

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/drive/b/k;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 64
    iget-wide v0, p0, Lcom/google/android/gms/drive/b/k;->b:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/drive/b/k;->b:J

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/drive/b/k;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1, p2, p3}, Ljava/security/MessageDigest;->update([BII)V

    .line 66
    return-void
.end method
