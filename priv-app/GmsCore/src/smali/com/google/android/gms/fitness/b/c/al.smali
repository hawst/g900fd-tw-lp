.class public Lcom/google/android/gms/fitness/b/c/al;
.super Lcom/google/android/gms/fitness/b/c/f;
.source "SourceFile"


# instance fields
.field protected b:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/fitness/b/c/f;-><init>()V

    .line 25
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/fitness/b/c/al;->b:J

    return-void
.end method


# virtual methods
.method public final a(J)Lcom/google/android/gms/fitness/b/c/al;
    .locals 1

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/google/android/gms/fitness/b/c/al;->b:J

    .line 29
    return-object p0
.end method

.method public final bridge synthetic a(I)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/b/c/f;->a(I)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/b/c/f;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/fitness/b/c/an;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/b/c/f;->a(Lcom/google/android/gms/fitness/b/c/an;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/fitness/b/l;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/b/c/f;->a(Lcom/google/android/gms/fitness/b/l;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/b/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Z)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/b/c/f;->a(Z)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Lcom/google/android/gms/fitness/b/ad;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/b/c/al;->c()Lcom/google/android/gms/fitness/b/c/ak;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/b/c/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/android/gms/fitness/b/c/ak;
    .locals 4

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/b/c/al;->a()V

    .line 35
    new-instance v0, Lcom/google/android/gms/fitness/b/c/ak;

    iget-wide v2, p0, Lcom/google/android/gms/fitness/b/c/al;->b:J

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/gms/fitness/b/c/ak;-><init>(Lcom/google/android/gms/fitness/b/c/al;J)V

    return-object v0
.end method

.method public final bridge synthetic c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/b/c/f;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic d(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/b/c/f;->d(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/c/f;

    move-result-object v0

    return-object v0
.end method
