.class final Lcom/google/android/gms/cast/b/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/internal/n;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/b/e;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/cast/b/e;)V
    .locals 0

    .prologue
    .line 1256
    iput-object p1, p0, Lcom/google/android/gms/cast/b/l;->a:Lcom/google/android/gms/cast/b/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/cast/b/e;B)V
    .locals 0

    .prologue
    .line 1256
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/b/l;-><init>(Lcom/google/android/gms/cast/b/e;)V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1302
    invoke-static {}, Lcom/google/android/gms/cast/b/e;->r()Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 1261
    if-nez p5, :cond_0

    .line 1262
    iget-object v0, p0, Lcom/google/android/gms/cast/b/l;->a:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->o()Ljava/lang/String;

    move-result-object v6

    .line 1265
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/l;->a:Lcom/google/android/gms/cast/b/e;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 1266
    return-void

    :cond_0
    move-object v6, p5

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[BJLjava/lang/String;)V
    .locals 7

    .prologue
    .line 1271
    if-nez p5, :cond_0

    .line 1272
    iget-object v0, p0, Lcom/google/android/gms/cast/b/l;->a:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->o()Ljava/lang/String;

    move-result-object v6

    .line 1275
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/l;->a:Lcom/google/android/gms/cast/b/e;

    const-wide/16 v4, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;[BJLjava/lang/String;)V

    .line 1276
    return-void

    :cond_0
    move-object v6, p5

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 7

    .prologue
    .line 1281
    iget-object v0, p0, Lcom/google/android/gms/cast/b/l;->a:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->o()Ljava/lang/String;

    move-result-object v6

    .line 1285
    iget-object v0, p0, Lcom/google/android/gms/cast/b/l;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->y(Lcom/google/android/gms/cast/b/e;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/b/l;->a:Lcom/google/android/gms/cast/b/e;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 1287
    return-void
.end method
