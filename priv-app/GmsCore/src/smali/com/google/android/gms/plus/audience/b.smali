.class public final Lcom/google/android/gms/plus/audience/b;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Z

.field private i:Lcom/google/android/gms/people/n;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 78
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/b;Lcom/google/android/gms/people/n;)Lcom/google/android/gms/people/n;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/b;->i:Lcom/google/android/gms/people/n;

    return-object p1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/b;
    .locals 2

    .prologue
    .line 62
    const-string v0, "Account name must not be empty."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 63
    const-string v0, "Update person qualifiedId must not be empty."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 64
    const-string v0, "Circle to add must not be empty."

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 66
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 67
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v1, "plusPageId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v1, "updatePersonId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v1, "circleIdToAdd"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v1, "clientApplicationId"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    new-instance v1, Lcom/google/android/gms/plus/audience/b;

    invoke-direct {v1}, Lcom/google/android/gms/plus/audience/b;-><init>()V

    .line 73
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/b;->setArguments(Landroid/os/Bundle;)V

    .line 74
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/b;)Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->h:Z

    return v0
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 106
    iput-boolean v5, p0, Lcom/google/android/gms/plus/audience/b;->h:Z

    .line 108
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/b;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/b;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/b;->d:Ljava/lang/String;

    new-array v5, v5, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/b;->e:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/gms/plus/a/n;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/people/j;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/audience/d;

    invoke-direct {v1, p0, v8}, Lcom/google/android/gms/plus/audience/d;-><init>(Lcom/google/android/gms/plus/audience/b;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 113
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/b;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/b;->c()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/b;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/c;

    .line 156
    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/b;->i:Lcom/google/android/gms/people/n;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/b;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/audience/c;->a(Lcom/google/android/gms/people/n;)V

    .line 159
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->i:Lcom/google/android/gms/people/n;

    if-eqz v0, :cond_1

    .line 86
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "AddToCircleFragment should only be used once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->h:Z

    if-nez v0, :cond_2

    .line 92
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/b;->b()V

    .line 103
    :cond_2
    :goto_0
    return-void

    .line 99
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->g:Z

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/b;->i:Lcom/google/android/gms/people/n;

    .line 151
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/b;->c()V

    .line 152
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->g:Z

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->g:Z

    .line 165
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/b;->b()V

    .line 167
    :cond_0
    return-void
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->h:Z

    if-eqz v0, :cond_1

    .line 172
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/b;->g:Z

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 175
    :cond_1
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 118
    instance-of v0, p1, Lcom/google/android/gms/plus/audience/c;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement AddToCircleFragmentHost."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 125
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 126
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/b;->setRetainInstance(Z)V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/b;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 128
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/b;->b:Ljava/lang/String;

    .line 129
    const-string v1, "plusPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/b;->c:Ljava/lang/String;

    .line 130
    const-string v1, "updatePersonId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/b;->d:Ljava/lang/String;

    .line 131
    const-string v1, "circleIdToAdd"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/b;->e:Ljava/lang/String;

    .line 132
    const-string v1, "clientApplicationId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/b;->f:Ljava/lang/String;

    .line 134
    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/b;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-direct {v1, v0, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    sget-object v2, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    new-instance v3, Lcom/google/android/gms/people/ad;

    invoke-direct {v3}, Lcom/google/android/gms/people/ad;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, v3, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v3}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 140
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->f:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/b;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 146
    return-void
.end method
