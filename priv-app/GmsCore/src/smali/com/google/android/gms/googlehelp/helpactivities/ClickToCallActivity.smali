.class public Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;
.super Lcom/google/android/gms/googlehelp/helpactivities/r;
.source "SourceFile"


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/EditText;

.field private f:Lcom/google/android/gms/googlehelp/common/m;

.field private g:Landroid/widget/ProgressBar;

.field private h:Landroid/view/MenuItem;

.field private i:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;-><init>()V

    return-void
.end method

.method private a(Landroid/widget/EditText;I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x3
    .end annotation

    .prologue
    .line 249
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {p0, p2}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 256
    :goto_0
    return-void

    .line 254
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 215
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->a(Z)V

    .line 218
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    new-instance v5, Lcom/google/android/gms/googlehelp/helpactivities/e;

    invoke-direct {v5, p0}, Lcom/google/android/gms/googlehelp/helpactivities/e;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;)V

    new-instance v6, Lcom/google/android/gms/googlehelp/helpactivities/f;

    invoke-direct {v6, p0, p1, p2, p3}, Lcom/google/android/gms/googlehelp/helpactivities/f;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 239
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 242
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->b:Landroid/view/View;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->g:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->h:Landroid/view/MenuItem;

    if-nez p1, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 245
    return-void

    :cond_2
    move v0, v2

    .line 242
    goto :goto_0
.end method

.method private e()Ljava/util/Map;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 100
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/gms/c;->k:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 103
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 104
    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 105
    array-length v6, v5

    const/4 v7, 0x2

    if-lt v6, v7, :cond_0

    .line 106
    aget-object v6, v5, v1

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 109
    const/4 v7, 0x1

    aget-object v5, v5, v7

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 110
    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    :cond_1
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/android/volley/ac;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 260
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-nez v0, :cond_0

    const/16 v0, 0x1f4

    move v4, v0

    .line 264
    :goto_0
    const/16 v0, 0x196

    if-ne v4, v0, :cond_1

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->c:Landroid/widget/EditText;

    sget v1, Lcom/google/android/gms/p;->or:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->a(Landroid/widget/EditText;I)V

    .line 305
    :goto_1
    return-void

    .line 260
    :cond_0
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    move v4, v0

    goto :goto_0

    .line 272
    :cond_1
    sget v3, Lcom/google/android/gms/p;->oo:I

    .line 273
    const/high16 v1, 0x1040000

    .line 274
    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/g;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/google/android/gms/googlehelp/helpactivities/g;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    sparse-switch v4, :sswitch_data_0

    .line 295
    sget v4, Lcom/google/android/gms/p;->pm:I

    move v6, v3

    move v3, v2

    move v2, v6

    .line 299
    :goto_2
    new-instance v5, Lcom/google/android/gms/googlehelp/helpactivities/b;

    invoke-direct {v5}, Lcom/google/android/gms/googlehelp/helpactivities/b;-><init>()V

    invoke-virtual {v5, v3}, Lcom/google/android/gms/googlehelp/helpactivities/b;->a(I)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/google/android/gms/googlehelp/helpactivities/b;->b(I)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/google/android/gms/googlehelp/helpactivities/b;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/b;->c(I)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "error_handler_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/helpactivities/b;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_1

    .line 283
    :sswitch_0
    sget v4, Lcom/google/android/gms/p;->ox:I

    move v6, v3

    move v3, v2

    move v2, v6

    .line 284
    goto :goto_2

    .line 287
    :sswitch_1
    sget v4, Lcom/google/android/gms/p;->os:I

    .line 288
    sget v3, Lcom/google/android/gms/p;->ot:I

    .line 289
    const v1, 0x104000a

    .line 291
    sget-object v0, Lcom/google/android/gms/googlehelp/helpactivities/b;->j:Landroid/content/DialogInterface$OnClickListener;

    move v6, v2

    move v2, v1

    move v1, v6

    .line 292
    goto :goto_2

    .line 281
    nop

    :sswitch_data_0
    .sparse-switch
        0x193 -> :sswitch_1
        0x198 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onCreate(Landroid/os/Bundle;)V

    .line 63
    sget v0, Lcom/google/android/gms/p;->oq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 66
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 67
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v1

    .line 68
    invoke-virtual {v1, v0}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 70
    sget v0, Lcom/google/android/gms/l;->cc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->setContentView(I)V

    .line 71
    sget v0, Lcom/google/android/gms/j;->hH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->b:Landroid/view/View;

    .line 72
    sget v0, Lcom/google/android/gms/j;->jj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->c:Landroid/widget/EditText;

    .line 73
    sget v0, Lcom/google/android/gms/j;->ji:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->d:Landroid/widget/EditText;

    .line 74
    sget v0, Lcom/google/android/gms/j;->iJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->e:Landroid/widget/EditText;

    .line 76
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 78
    sget v0, Lcom/google/android/gms/j;->jh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 79
    const-string v2, "display_country"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v1, v2, v3}, Lcom/google/android/gms/googlehelp/common/i;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 83
    new-instance v3, Lcom/google/android/gms/googlehelp/common/m;

    invoke-direct {v3, p0, v0, v2}, Lcom/google/android/gms/googlehelp/common/m;-><init>(Landroid/content/Context;Landroid/widget/Spinner;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->f:Lcom/google/android/gms/googlehelp/common/m;

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->c:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/gms/googlehelp/common/e;->b(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->c:Landroid/widget/EditText;

    new-instance v2, Landroid/telephony/PhoneNumberFormattingTextWatcher;

    invoke-direct {v2}, Landroid/telephony/PhoneNumberFormattingTextWatcher;-><init>()V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->d:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/gms/googlehelp/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 92
    sget v0, Lcom/google/android/gms/j;->iL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->g:Landroid/widget/ProgressBar;

    .line 95
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->e()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->i:Ljava/util/Map;

    .line 96
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    .line 124
    sget v0, Lcom/google/android/gms/m;->t:I

    sget v1, Lcom/google/android/gms/m;->s:I

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/gms/googlehelp/helpactivities/a;->a(IILandroid/view/Menu;Landroid/view/MenuInflater;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 132
    sget v0, Lcom/google/android/gms/j;->hG:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->h:Landroid/view/MenuItem;

    .line 133
    new-instance v0, Lcom/google/android/gms/googlehelp/c/ab;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/EditText;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->d:Landroid/widget/EditText;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->c:Landroid/widget/EditText;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->h:Landroid/view/MenuItem;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/googlehelp/c/ab;-><init>(Ljava/util/List;Landroid/view/MenuItem;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/ab;->a()V

    .line 136
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 142
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 143
    sget v1, Lcom/google/android/gms/j;->hG:I

    if-ne v0, v1, :cond_8

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->convertKeypadLettersToDigits(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->c:Landroid/widget/EditText;

    sget v1, Lcom/google/android/gms/p;->or:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->a(Landroid/widget/EditText;I)V

    :cond_1
    :goto_0
    move v0, v2

    .line 148
    :goto_1
    return v0

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->d:Landroid/widget/EditText;

    sget v1, Lcom/google/android/gms/p;->op:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->a(Landroid/widget/EditText;I)V

    goto :goto_0

    :cond_3
    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->f:Lcom/google/android/gms/googlehelp/common/m;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/m;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->i:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_9

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "+"

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->f:Lcom/google/android/gms/googlehelp/common/m;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/m;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {p0, v6, v5, v0, v4}, Lcom/google/android/gms/googlehelp/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Lcom/google/android/gms/common/util/ak;->d(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/google/android/gms/common/util/ak;->c(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    :goto_4
    if-nez v0, :cond_7

    :cond_4
    move v0, v3

    :goto_5
    if-nez v0, :cond_1

    invoke-direct {p0, v1, v5, v4}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    move v0, v3

    goto :goto_4

    :cond_7
    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/b;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/helpactivities/b;-><init>()V

    sget v3, Lcom/google/android/gms/p;->ov:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/helpactivities/b;->a(I)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->ou:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/helpactivities/b;->b(I)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->oo:I

    new-instance v6, Lcom/google/android/gms/googlehelp/helpactivities/d;

    invoke-direct {v6, p0, v1, v5, v4}, Lcom/google/android/gms/googlehelp/helpactivities/d;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v6}, Lcom/google/android/gms/googlehelp/helpactivities/b;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v0

    const/high16 v3, 0x1040000

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/helpactivities/b;->c(I)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v3

    const-string v6, "roaming_handler_dialog"

    invoke-virtual {v0, v3, v6}, Lcom/google/android/gms/googlehelp/helpactivities/b;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    move v0, v2

    goto :goto_5

    .line 148
    :cond_8
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_1

    :cond_9
    move-object v0, v1

    goto/16 :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 117
    const-string v0, "EXTRA_HELP_CONFIG"

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 118
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 119
    return-void
.end method
