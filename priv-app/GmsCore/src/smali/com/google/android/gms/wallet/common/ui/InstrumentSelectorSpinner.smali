.class public Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cc;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/ce;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 31
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ce;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/wallet/common/ui/ce;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/gms/wallet/common/ui/ce;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/gms/wallet/common/ui/ce;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/common/ui/cd;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/android/gms/wallet/common/ui/cd;)V

    .line 129
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 53
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Z)V

    .line 65
    return-void
.end method

.method public final a([I)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a([I)V

    .line 81
    return-void
.end method

.method public final a([Lcom/google/checkout/inapp/proto/j;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a([Lcom/google/checkout/inapp/proto/j;)V

    .line 58
    return-void
.end method

.method public final a([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->a([Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ce;->a()Z

    move-result v0

    return v0
.end method

.method public final b([I)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->b([I)V

    .line 86
    return-void
.end method

.method public final b_(Z)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->b(Z)V

    .line 70
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ce;->c(Z)V

    .line 75
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->isEnabled()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 107
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    .line 109
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cg;->notifyDataSetChanged()V

    .line 113
    :cond_0
    return-void
.end method
