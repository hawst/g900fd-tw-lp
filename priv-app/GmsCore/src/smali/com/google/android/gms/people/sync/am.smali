.class final Lcom/google/android/gms/people/sync/am;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Z

.field final b:Z

.field final c:Z

.field final synthetic d:Lcom/google/android/gms/people/sync/z;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/sync/z;ZZZ)V
    .locals 0

    .prologue
    .line 1488
    iput-object p1, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1489
    iput-boolean p2, p0, Lcom/google/android/gms/people/sync/am;->a:Z

    .line 1490
    iput-boolean p4, p0, Lcom/google/android/gms/people/sync/am;->b:Z

    .line 1491
    iput-boolean p3, p0, Lcom/google/android/gms/people/sync/am;->c:Z

    .line 1492
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const-wide/16 v10, 0x1

    .line 1520
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 1522
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->b()Ljava/util/List;

    move-result-object v4

    .line 1523
    invoke-static {v4}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v5

    move v3, v2

    .line 1524
    :goto_0
    if-ge v3, v5, :cond_7

    .line 1525
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    .line 1531
    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 1532
    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->m(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v6

    .line 1534
    if-nez v1, :cond_0

    if-eqz v6, :cond_5

    .line 1535
    :cond_0
    if-nez v6, :cond_3

    invoke-static {v0}, Lcom/google/android/gms/people/sync/ap;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1536
    sget-object v1, Lcom/google/android/gms/people/a/a;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1537
    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->g(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1548
    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1551
    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->p(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v0

    .line 1553
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/sync/ap;->j(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1562
    iget-object v1, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/sync/ap;->m(Ljava/lang/String;)V

    .line 1607
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->c(Lcom/google/android/gms/people/sync/z;)Landroid/content/SyncResult;

    move-result-object v0

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v6, v10

    iput-wide v6, v0, Landroid/content/SyncStats;->numDeletes:J

    .line 1608
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/people/sync/y;->v:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/people/sync/y;->v:I

    .line 1524
    :cond_1
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 1531
    goto :goto_1

    .line 1571
    :cond_3
    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->p(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v1

    .line 1573
    if-eqz v1, :cond_1

    .line 1580
    iget-object v7, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v7}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/google/android/gms/people/sync/ap;->j(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1584
    iget-object v1, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/sync/ap;->d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1586
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->c(Lcom/google/android/gms/people/sync/z;)Landroid/content/SyncResult;

    move-result-object v0

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v0, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v8, v10

    iput-wide v8, v0, Landroid/content/SyncStats;->numUpdates:J

    .line 1587
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/people/sync/y;->u:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/people/sync/y;->u:I

    .line 1588
    if-eqz v6, :cond_1

    .line 1589
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/people/sync/y;->w:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/people/sync/y;->w:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 1621
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    .line 1592
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/sync/ap;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1594
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->c(Lcom/google/android/gms/people/sync/z;)Landroid/content/SyncResult;

    move-result-object v0

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numInserts:J

    add-long/2addr v6, v10

    iput-wide v6, v0, Landroid/content/SyncStats;->numInserts:J

    .line 1595
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/people/sync/y;->t:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/people/sync/y;->t:I

    goto :goto_3

    .line 1598
    :cond_5
    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->p(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v1

    .line 1600
    if-eqz v1, :cond_6

    .line 1601
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/sync/ap;->m(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1604
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/sync/ap;->n(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1612
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->v()V

    .line 1614
    if-lez v5, :cond_8

    .line 1615
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->f(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->d(Lcom/google/android/gms/people/sync/z;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v2}, Lcom/google/android/gms/people/sync/z;->e(Lcom/google/android/gms/people/sync/z;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1619
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1621
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 1623
    iget-object v0, p0, Lcom/google/android/gms/people/sync/am;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->g(Lcom/google/android/gms/people/sync/z;)V

    .line 1624
    return-void
.end method
