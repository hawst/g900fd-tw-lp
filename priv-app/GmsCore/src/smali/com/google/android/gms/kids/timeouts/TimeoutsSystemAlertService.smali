.class public Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->a:Landroid/view/View;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;)V
    .locals 4

    .prologue
    .line 22
    const-class v1, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->a:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v0, "TimeoutsSystemAlertService"

    const-string v2, "Timeout already deactivated"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v0, "TimeoutsSystemAlertService"

    const-string v2, "deactivating timeout"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    sget-object v2, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->a:Landroid/view/View;

    invoke-interface {v0, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->a:Landroid/view/View;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/kids/timeouts/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/kids/timeouts/h;-><init>(Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;Landroid/os/Handler;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/kids/timeouts/g;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/gms/kids/timeouts/g;-><init>(Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;Ljava/lang/String;Z)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-static {p0}, Lcom/google/android/gms/gcm/ab;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "TimeoutsSystemAlertService"

    const-string v1, "Gcm registration has not done yet"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    const-class v3, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;

    monitor-enter v3

    :try_start_0
    sget-object v0, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    const-string v0, "TimeoutsSystemAlertService"

    const-string v1, "Timeout already activated"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_1
    :try_start_1
    const-string v0, "TimeoutsSystemAlertService"

    const-string v1, "activating timeout"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v4, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v4}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    const/16 v0, 0x11

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/16 v0, 0x7d3

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->type:I

    const/4 v0, -0x1

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    const/4 v0, -0x1

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    const/4 v0, -0x3

    iput v0, v4, Landroid/view/WindowManager$LayoutParams;->format:I

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/l;->cv:I

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    sput-object v1, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->a:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->kQ:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/google/android/gms/p;->pD:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->a:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->kR:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/google/android/gms/p;->pF:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/google/android/gms/p;->pC:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_2
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;->a:Landroid/view/View;

    invoke-interface {v0, v1, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 37
    const-string v2, "TimeoutsSystemAlertService"

    const-string v3, "Timeout start service"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 40
    if-eqz p1, :cond_1

    .line 41
    const-string v2, "timeout_theme_key"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 43
    :goto_0
    if-ne v2, v1, :cond_0

    move v0, v1

    .line 44
    :cond_0
    new-instance v2, Ljava/lang/Thread;

    new-instance v4, Lcom/google/android/gms/kids/timeouts/f;

    invoke-direct {v4, p0, v3, v0}, Lcom/google/android/gms/kids/timeouts/f;-><init>(Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;Landroid/os/Handler;Z)V

    invoke-direct {v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 57
    return v1

    :cond_1
    move v2, v0

    goto :goto_0
.end method
