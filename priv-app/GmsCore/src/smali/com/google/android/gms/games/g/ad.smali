.class public final Lcom/google/android/gms/games/g/ad;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8544
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8545
    iput v0, p0, Lcom/google/android/gms/games/g/ad;->a:I

    iput v0, p0, Lcom/google/android/gms/games/g/ad;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/ad;->cachedSize:I

    .line 8546
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 8595
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8596
    iget v1, p0, Lcom/google/android/gms/games/g/ad;->a:I

    if-eqz v1, :cond_0

    .line 8597
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/ad;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8600
    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/g/ad;->b:I

    if-eqz v1, :cond_1

    .line 8601
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/games/g/ad;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8604
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8557
    if-ne p1, p0, :cond_1

    .line 8570
    :cond_0
    :goto_0
    return v0

    .line 8560
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/ad;

    if-nez v2, :cond_2

    move v0, v1

    .line 8561
    goto :goto_0

    .line 8563
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/ad;

    .line 8564
    iget v2, p0, Lcom/google/android/gms/games/g/ad;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/ad;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 8565
    goto :goto_0

    .line 8567
    :cond_3
    iget v2, p0, Lcom/google/android/gms/games/g/ad;->b:I

    iget v3, p1, Lcom/google/android/gms/games/g/ad;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 8568
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 8575
    iget v0, p0, Lcom/google/android/gms/games/g/ad;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 8577
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/ad;->b:I

    add-int/2addr v0, v1

    .line 8578
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8517
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/ad;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/ad;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 8584
    iget v0, p0, Lcom/google/android/gms/games/g/ad;->a:I

    if-eqz v0, :cond_0

    .line 8585
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/ad;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8587
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/g/ad;->b:I

    if-eqz v0, :cond_1

    .line 8588
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/g/ad;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8590
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8591
    return-void
.end method
