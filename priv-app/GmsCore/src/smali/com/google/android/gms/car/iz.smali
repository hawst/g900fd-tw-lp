.class final Lcom/google/android/gms/car/iz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/gms/car/ExceptionParcel;
    .locals 2

    .prologue
    .line 41
    :try_start_0
    new-instance v1, Lcom/google/android/gms/car/ExceptionParcel;

    invoke-virtual {p0}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v0}, Lcom/google/android/gms/car/ExceptionParcel;-><init>(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 46
    :goto_0
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 46
    new-instance v0, Lcom/google/android/gms/car/ExceptionParcel;

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ExceptionParcel;-><init>(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    invoke-static {p1}, Lcom/google/android/gms/car/iz;->a(Landroid/os/Parcel;)Lcom/google/android/gms/car/ExceptionParcel;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    new-array v0, p1, [Lcom/google/android/gms/car/ExceptionParcel;

    return-object v0
.end method
