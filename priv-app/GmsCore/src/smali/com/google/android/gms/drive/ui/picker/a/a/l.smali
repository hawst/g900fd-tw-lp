.class public final Lcom/google/android/gms/drive/ui/picker/a/a/l;
.super Lcom/google/android/gms/drive/ui/picker/a/a/i;
.source "SourceFile"


# instance fields
.field private h:I

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/drawable/NinePatchDrawable;

.field private k:I

.field private l:Landroid/graphics/RectF;

.field private m:Lcom/google/android/gms/drive/ui/picker/a/a/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;Lcom/google/android/gms/drive/ui/picker/a/a/j;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/drive/ui/picker/a/a/i;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;Lcom/google/android/gms/drive/ui/picker/a/a/j;)V

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->i:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/gms/h;->R:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->j:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->i:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->l:Landroid/graphics/RectF;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 130
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->g:I

    .line 131
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a(I)V

    .line 132
    if-ne p1, v0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    if-eq p1, v1, :cond_2

    if-ne v0, v1, :cond_0

    .line 137
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->m:Lcom/google/android/gms/drive/ui/picker/a/a/m;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->m:Lcom/google/android/gms/drive/ui/picker/a/a/m;

    goto :goto_0
.end method

.method public final a(IILandroid/content/res/Resources;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a:Landroid/graphics/drawable/Drawable;

    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->c(I)I

    move-result v1

    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->b(I)I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->c:I

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->d:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->e:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->e:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    div-int/lit8 v1, p2, 0xa

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->e:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/i;->b:Landroid/graphics/drawable/Drawable;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->l:Landroid/graphics/RectF;

    .line 99
    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->h:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 100
    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->h:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 101
    div-int/lit8 v1, p2, 0xa

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 102
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 103
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->i:Landroid/graphics/Paint;

    const-string v3, "W"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v5, v4, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 104
    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->k:I

    .line 105
    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->k:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 106
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->j:Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v1, :cond_2

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->j:Landroid/graphics/drawable/NinePatchDrawable;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 110
    :cond_2
    return-void
.end method

.method public final a(ILandroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 150
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 151
    const/4 v1, 0x1

    const/high16 v2, 0x43960000    # 300.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->h:I

    .line 154
    return-void
.end method

.method final a(Landroid/graphics/Canvas;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->j:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->i:Landroid/graphics/Paint;

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->l:Landroid/graphics/RectF;

    .line 90
    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    .line 91
    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v3

    .line 92
    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget v5, v1, Landroid/graphics/RectF;->top:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v5

    add-float/2addr v2, v3

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, p2, v4, v1, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 94
    return-void
.end method

.method public final a(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a(Landroid/widget/AbsListView;III)V

    .line 116
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/ui/picker/a/a/m;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->m:Lcom/google/android/gms/drive/ui/picker/a/a/m;

    .line 162
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 65
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->b()V

    .line 66
    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 125
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->c()Z

    move-result v0

    return v0
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->i:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 158
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 165
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/l;->f:Ljava/lang/String;

    .line 166
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->d()V

    .line 167
    return-void
.end method
