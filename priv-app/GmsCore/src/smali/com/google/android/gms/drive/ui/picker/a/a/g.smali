.class public abstract Lcom/google/android/gms/drive/ui/picker/a/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/android/gms/drive/ui/picker/a/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method


# virtual methods
.method public abstract a()Lcom/google/android/gms/drive/query/SortOrder;
.end method

.method public final a(Lcom/google/android/gms/drive/ui/picker/a/l;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/g;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 62
    return-void
.end method

.method public abstract b()Lcom/google/android/gms/drive/ui/picker/a/a/r;
.end method

.method public abstract c()Landroid/widget/SectionIndexer;
.end method

.method public d()Lcom/google/android/gms/drive/ui/picker/a/a/a;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;->a:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/g;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/g;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->close()V

    .line 71
    :cond_0
    return-void
.end method
