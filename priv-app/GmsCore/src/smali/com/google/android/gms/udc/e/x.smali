.class public final Lcom/google/android/gms/udc/e/x;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/udc/e/y;

.field public apiHeader:Lcom/google/android/gms/udc/e/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 258
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 259
    iput-object v0, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/x;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/x;->cachedSize:I

    .line 260
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 325
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 326
    iget-object v1, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    if-eqz v1, :cond_0

    .line 327
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-eqz v1, :cond_1

    .line 331
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 272
    if-ne p1, p0, :cond_1

    .line 273
    const/4 v0, 0x1

    .line 297
    :cond_0
    :goto_0
    return v0

    .line 275
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/x;

    if-eqz v1, :cond_0

    .line 278
    check-cast p1, Lcom/google/android/gms/udc/e/x;

    .line 279
    iget-object v1, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-nez v1, :cond_4

    .line 280
    iget-object v1, p1, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-nez v1, :cond_0

    .line 288
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    if-nez v1, :cond_5

    .line 289
    iget-object v1, p1, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    if-nez v1, :cond_0

    .line 297
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/x;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 284
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 293
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 302
    iget-object v0, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 305
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 307
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/x;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    return v0

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/a;->hashCode()I

    move-result v0

    goto :goto_0

    .line 305
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    invoke-virtual {v1}, Lcom/google/android/gms/udc/e/y;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/x;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/udc/e/y;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/udc/e/a;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    if-eqz v0, :cond_0

    .line 315
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    if-eqz v0, :cond_1

    .line 318
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/udc/e/x;->apiHeader:Lcom/google/android/gms/udc/e/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 320
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 321
    return-void
.end method
