.class public final Lcom/google/android/gms/fitness/apiary/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/fitness/apiary/b;

.field static final b:Lcom/google/android/gms/fitness/apiary/b;

.field static final c:Lcom/google/android/gms/fitness/apiary/b;

.field static final d:Lcom/google/android/gms/fitness/apiary/b;

.field static final e:Lcom/google/android/gms/fitness/apiary/b;

.field public static final f:Lcom/google/android/gms/fitness/apiary/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/fitness/apiary/d;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/c;->a:Lcom/google/android/gms/fitness/apiary/b;

    .line 95
    new-instance v0, Lcom/google/android/gms/fitness/apiary/e;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/c;->b:Lcom/google/android/gms/fitness/apiary/b;

    .line 123
    new-instance v0, Lcom/google/android/gms/fitness/apiary/f;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/c;->c:Lcom/google/android/gms/fitness/apiary/b;

    .line 140
    new-instance v0, Lcom/google/android/gms/fitness/apiary/g;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/c;->d:Lcom/google/android/gms/fitness/apiary/b;

    .line 156
    new-instance v0, Lcom/google/android/gms/fitness/apiary/h;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/c;->e:Lcom/google/android/gms/fitness/apiary/b;

    .line 182
    new-instance v0, Lcom/google/android/gms/fitness/apiary/i;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/c;->f:Lcom/google/android/gms/fitness/apiary/b;

    return-void
.end method

.method public static a(Lcom/google/android/gms/fitness/data/DataPoint;ZZ)Lcom/google/af/a/b/a/a/c;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 281
    new-instance v2, Lcom/google/af/a/b/a/a/c;

    invoke-direct {v2}, Lcom/google/af/a/b/a/a/c;-><init>()V

    .line 285
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 286
    if-eqz p1, :cond_0

    cmp-long v3, v0, v4

    if-gtz v3, :cond_0

    .line 287
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 289
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/af/a/b/a/a/c;->a:Ljava/lang/Long;

    .line 290
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/af/a/b/a/a/c;->b:Ljava/lang/Long;

    .line 291
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->f()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 292
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/af/a/b/a/a/c;->f:Ljava/lang/Long;

    .line 294
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->e()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_2

    .line 295
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/af/a/b/a/a/c;->g:Ljava/lang/Long;

    .line 298
    :cond_2
    if-eqz p2, :cond_3

    .line 299
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/af/a/b/a/a/c;->c:Ljava/lang/String;

    .line 300
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->d()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_3

    .line 302
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/af/a/b/a/a/c;->d:Ljava/lang/String;

    .line 306
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->a()[Lcom/google/android/gms/fitness/data/Value;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lcom/google/af/a/b/a/a/j;

    iput-object v0, v2, Lcom/google/af/a/b/a/a/c;->e:[Lcom/google/af/a/b/a/a/j;

    .line 307
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->a()[Lcom/google/android/gms/fitness/data/Value;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 308
    iget-object v1, v2, Lcom/google/af/a/b/a/a/c;->e:[Lcom/google/af/a/b/a/a/j;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(I)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v3

    new-instance v4, Lcom/google/af/a/b/a/a/j;

    invoke-direct {v4}, Lcom/google/af/a/b/a/a/j;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Value;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x2

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Value;->b()I

    move-result v6

    if-ne v5, v6, :cond_5

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Value;->d()F

    move-result v3

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    iput-object v3, v4, Lcom/google/af/a/b/a/a/j;->b:Ljava/lang/Double;

    :cond_4
    :goto_1
    aput-object v4, v1, v0

    .line 307
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 308
    :cond_5
    const/4 v5, 0x1

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Value;->b()I

    move-result v6

    if-ne v5, v6, :cond_6

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Value;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v4, Lcom/google/af/a/b/a/a/j;->a:Ljava/lang/Integer;

    goto :goto_1

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Value;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 310
    :cond_7
    return-object v2
.end method

.method public static a(Lcom/google/af/a/b/a/a/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;
    .locals 7

    .prologue
    .line 228
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    .line 229
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v1

    .line 230
    iget-object v2, p0, Lcom/google/af/a/b/a/a/c;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/af/a/b/a/a/c;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 232
    iget-object v2, p0, Lcom/google/af/a/b/a/a/c;->g:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 233
    iget-object v2, p0, Lcom/google/af/a/b/a/a/c;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->a(J)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 235
    :cond_0
    iget-object v2, p0, Lcom/google/af/a/b/a/a/c;->f:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 236
    iget-object v2, p0, Lcom/google/af/a/b/a/a/c;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->b(J)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 239
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataType;->b()Ljava/util/List;

    move-result-object v3

    .line 240
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 241
    iget-object v0, p0, Lcom/google/af/a/b/a/a/c;->e:[Lcom/google/af/a/b/a/a/j;

    aget-object v4, v0, v2

    .line 242
    invoke-virtual {v1, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->a(I)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v5

    .line 243
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Field;->b()I

    move-result v0

    .line 244
    packed-switch v0, :pswitch_data_0

    .line 256
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected format: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 246
    :pswitch_0
    iget-object v0, v4, Lcom/google/af/a/b/a/a/j;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 247
    iget-object v0, v4, Lcom/google/af/a/b/a/a/j;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/fitness/data/Value;->a(I)V

    .line 240
    :cond_2
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 251
    :pswitch_1
    iget-object v0, v4, Lcom/google/af/a/b/a/a/j;->b:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 252
    iget-object v0, v4, Lcom/google/af/a/b/a/a/j;->b:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/fitness/data/Value;->a(F)V

    goto :goto_1

    .line 260
    :cond_3
    if-eqz p2, :cond_4

    .line 261
    invoke-virtual {v1, p2}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 263
    :cond_4
    return-object v1

    .line 244
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a([Lcom/google/af/a/b/a/a/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/l/z;)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 333
    new-instance v3, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 334
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 335
    array-length v5, p0

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_2

    aget-object v6, p0, v1

    .line 337
    :try_start_0
    iget-object v7, v6, Lcom/google/af/a/b/a/a/c;->d:Ljava/lang/String;

    invoke-interface {v4, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 338
    :goto_1
    invoke-static {v6, p1, v0}, Lcom/google/android/gms/fitness/apiary/c;->a(Lcom/google/af/a/b/a/a/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 337
    :cond_0
    invoke-interface {p2, v7}, Lcom/google/android/gms/fitness/l/z;->c(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    invoke-interface {v4, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 339
    :catch_0
    move-exception v0

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unable to convert: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v0, v6, v7}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 343
    :cond_2
    return-object v3
.end method
