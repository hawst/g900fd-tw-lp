.class public final Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
.super Lcom/google/android/gms/plus/audience/ao;
.source "SourceFile"


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/audience/ao;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/google/android/gms/h;->bf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b()V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c()V

    .line 45
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->a()V

    .line 46
    return-void
.end method

.method public final a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->tj:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/plus/audience/ap;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->a(Lcom/google/android/gms/plus/audience/ap;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    return-void
.end method

.method public final bridge synthetic a(Z)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->a(Z)V

    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 68
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 72
    return-void
.end method

.method public final bridge synthetic b(Z)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->b(Z)V

    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 86
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    return-void
.end method

.method public final bridge synthetic c(Z)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->c(Z)V

    return-void
.end method

.method public final bridge synthetic d(Z)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->d(Z)V

    return-void
.end method

.method public final bridge synthetic d()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->d()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic e()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->e()V

    return-void
.end method

.method public final bridge synthetic f()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->f()V

    return-void
.end method

.method public final bridge synthetic isChecked()Z
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->isChecked()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/audience/ao;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public final bridge synthetic onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->onClick(Landroid/view/View;)V

    return-void
.end method

.method protected final onFinishInflate()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->onFinishInflate()V

    .line 33
    sget v0, Lcom/google/android/gms/j;->aZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b:Landroid/widget/TextView;

    .line 34
    sget v0, Lcom/google/android/gms/j;->aW:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->c:Landroid/widget/TextView;

    .line 35
    sget v0, Lcom/google/android/gms/j;->aY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->d:Landroid/widget/ImageView;

    .line 36
    sget v0, Lcom/google/android/gms/j;->aX:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->e:Landroid/widget/TextView;

    .line 37
    return-void
.end method

.method public final bridge synthetic setChecked(Z)V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->setChecked(Z)V

    return-void
.end method

.method public final bridge synthetic toggle()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ao;->toggle()V

    return-void
.end method
