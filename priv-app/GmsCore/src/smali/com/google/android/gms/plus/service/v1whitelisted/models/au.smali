.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/au;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public final b:Ljava/util/Set;

.field private c:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$ActorEntity;

.field private d:Ljava/util/List;

.field private e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$PlusonersEntity;

.field private f:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$RepliesEntity;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 898
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->b:Ljava/util/Set;

    .line 899
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ag;
    .locals 7

    .prologue
    .line 954
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->b:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$ActorEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->d:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$PlusonersEntity;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->f:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$RepliesEntity;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$ActorEntity;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$PlusonersEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$RepliesEntity;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/au;
    .locals 2

    .prologue
    .line 916
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->d:Ljava/util/List;

    .line 917
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->b:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 918
    return-object p0
.end method
