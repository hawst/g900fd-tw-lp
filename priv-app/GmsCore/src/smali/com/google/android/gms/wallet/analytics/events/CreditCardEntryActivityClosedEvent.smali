.class public Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->j:Ljava/lang/String;

    .line 26
    iput p1, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->c:I

    .line 27
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->c:I

    .line 32
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;-><init>(ILjava/lang/String;)V

    .line 38
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 39
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 50
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    return-void
.end method
