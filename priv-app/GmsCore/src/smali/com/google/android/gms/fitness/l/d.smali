.class final Lcom/google/android/gms/fitness/l/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Ljava/util/Map;

.field private c:J

.field private final d:Lcom/google/android/gms/fitness/l/e;

.field private final e:Lcom/google/android/gms/common/util/p;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/l/e;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/gms/fitness/l/d;->d:Lcom/google/android/gms/fitness/l/e;

    .line 54
    new-instance v0, Lcom/google/android/gms/common/util/r;

    invoke-direct {v0}, Lcom/google/android/gms/common/util/r;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/d;->e:Lcom/google/android/gms/common/util/p;

    .line 55
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/d;->a:Ljava/lang/Object;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/d;->b:Ljava/util/Map;

    .line 57
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 8

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/d;->b()Ljava/util/Map;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    .line 86
    :goto_0
    return-object v0

    .line 71
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/d;->d:Lcom/google/android/gms/fitness/l/e;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/l/e;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 78
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/d;->b:Ljava/util/Map;

    if-nez v2, :cond_1

    .line 79
    iput-object v0, p0, Lcom/google/android/gms/fitness/l/d;->b:Ljava/util/Map;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/d;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/gms/fitness/l/d;->c:J

    .line 82
    const-string v0, "Loaded data source stats for cache. Next expiration: %tT"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/gms/fitness/l/d;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/d;->b:Ljava/util/Map;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    const-class v1, Lcom/google/android/gms/fitness/l/ae;

    invoke-static {v0, v1}, Lcom/google/k/a/bd;->a(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 74
    invoke-static {v0}, Lcom/google/k/a/bd;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method final b()Ljava/util/Map;
    .locals 6

    .prologue
    .line 121
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/d;->b:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/l/d;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/fitness/l/d;->c:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 123
    const-string v0, "Cached stats expired, clearing cache"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/d;->c()V

    .line 125
    const/4 v0, 0x0

    monitor-exit v1

    .line 127
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/d;->b:Ljava/util/Map;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 136
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 137
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/gms/fitness/l/d;->b:Ljava/util/Map;

    .line 138
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
