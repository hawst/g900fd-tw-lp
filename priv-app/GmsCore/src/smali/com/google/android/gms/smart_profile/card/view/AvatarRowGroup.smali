.class public Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->a(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->a(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->a(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->setOrientation(I)V

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->bP:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->a:I

    .line 44
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 106
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 107
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 51
    .line 52
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    sub-int v0, p4, p2

    iget v2, p0, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->b:I

    sub-int/2addr v0, v2

    :goto_0
    move v2, v1

    move v3, v0

    .line 55
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 56
    invoke-virtual {p0, v2}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 57
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 58
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 59
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 60
    iget v7, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    add-int/2addr v3, v7

    .line 61
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->b:I

    sub-int/2addr v7, v3

    sub-int/2addr v7, v5

    add-int v8, v7, v5

    invoke-virtual {v4, v7, v1, v8, v6}, Landroid/view/View;->layout(IIII)V

    .line 62
    :goto_2
    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 55
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 61
    :cond_0
    add-int v7, v3, v5

    invoke-virtual {v4, v3, v1, v7, v6}, Landroid/view/View;->layout(IIII)V

    goto :goto_2

    .line 64
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->b:I

    .line 79
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->b:I

    iget v2, p0, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->a:I

    mul-int/lit8 v2, v2, 0x4

    sub-int/2addr v0, v2

    .line 81
    div-int/lit8 v2, v0, 0x5

    .line 85
    mul-int/lit8 v3, v2, 0x5

    sub-int/2addr v0, v3

    .line 86
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 87
    invoke-virtual {p0, v1}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    div-int/lit8 v0, v0, 0x2

    invoke-static {v3, v0}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->a(Landroid/view/View;I)V

    .line 91
    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 92
    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    iget v4, p0, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->a:I

    invoke-static {v3, v4}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->a(Landroid/view/View;I)V

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_1
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    move v0, v1

    .line 97
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 98
    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/view/View;->measure(II)V

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 101
    :cond_2
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->b:I

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/smart_profile/card/view/AvatarRowGroup;->setMeasuredDimension(II)V

    .line 102
    return-void
.end method
