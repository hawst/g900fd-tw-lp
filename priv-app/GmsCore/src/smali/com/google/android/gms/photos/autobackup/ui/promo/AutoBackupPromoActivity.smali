.class public Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;
.super Lcom/google/android/libraries/social/a/a/c/e;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/photos/autobackup/l;

.field private final d:Lcom/google/android/gms/photos/autobackup/ui/ae;

.field private e:Lcom/google/android/gms/photos/e;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/libraries/social/a/a/c/e;-><init>()V

    .line 23
    new-instance v0, Lcom/google/android/gms/photos/autobackup/l;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->c:Lcom/google/android/gms/photos/autobackup/l;

    .line 24
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/ui/ae;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->d:Lcom/google/android/gms/photos/autobackup/ui/ae;

    .line 28
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/promo/l;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->b:Lcom/google/android/libraries/social/i/i;

    new-instance v2, Lcom/google/android/gms/photos/autobackup/ui/promo/a;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/photos/autobackup/ui/promo/a;-><init>(Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;B)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/photos/autobackup/ui/promo/l;-><init>(Landroid/app/Activity;Lcom/google/android/libraries/social/i/w;Lcom/google/android/gms/common/api/x;)V

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, ".photos.autobackup.ui.Extras.Account.NAME"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/promo/b;

    invoke-direct {v1}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;-><init>()V

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    const v2, 0x1020002

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    return-void
.end method


# virtual methods
.method protected final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/a/a/c/e;->a(Landroid/os/Bundle;)V

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->a:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/e;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/e;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->e:Lcom/google/android/gms/photos/e;

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->a:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/ae;

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->d:Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->a:Lcom/google/android/libraries/social/a/a;

    const-class v1, Lcom/google/android/gms/photos/autobackup/l;

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->c:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->c:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/l;->b()V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->c:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, ".photos.autobackup.ui.Extras.Account.NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No account name specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/autobackup/l;->a(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/a/a/c/e;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->requestWindowFeature(I)Z

    .line 55
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/promo/a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/photos/autobackup/ui/promo/a;-><init>(Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/photos/autobackup/d;->b:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->c:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    .line 59
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 61
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->d:Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/photos/autobackup/ui/ae;->a(Lcom/google/android/gms/common/api/v;)V

    .line 62
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/google/android/libraries/social/a/a/c/e;->onStop()V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/AutoBackupPromoActivity;->e:Lcom/google/android/gms/photos/e;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/e;->a()V

    .line 68
    return-void
.end method
