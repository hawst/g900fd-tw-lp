.class public final Lcom/google/android/gms/googlehelp/f/g;
.super Lcom/google/android/gms/googlehelp/f/a;
.source "SourceFile"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private final d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private final e:Lcom/google/android/gms/googlehelp/d/b;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/util/Map;

.field private h:Lcom/google/android/gms/googlehelp/common/t;

.field private final i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/util/Map;Lcom/google/android/gms/googlehelp/common/t;Z)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/f/a;-><init>(Lcom/google/android/gms/googlehelp/f/n;)V

    .line 64
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->b:Landroid/content/Context;

    .line 65
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/f/g;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d()Lcom/google/android/gms/googlehelp/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->e:Lcom/google/android/gms/googlehelp/d/b;

    .line 68
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/f/g;->f:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/f/g;->g:Ljava/util/Map;

    .line 70
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/f/g;->h:Lcom/google/android/gms/googlehelp/common/t;

    .line 71
    iput-boolean p5, p0, Lcom/google/android/gms/googlehelp/f/g;->i:Z

    .line 72
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(Lcom/google/android/gms/googlehelp/f/a;)V

    .line 77
    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/gms/googlehelp/common/t;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/g;->h:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/f/g;->b()Z

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/gms/googlehelp/f/g;->i:Z

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/common/t;Lcom/google/android/gms/googlehelp/common/t;ZZ)V

    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/g;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/g;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/googlehelp/a/n;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/t;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/f/g;->e:Lcom/google/android/gms/googlehelp/d/b;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v2, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/k;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/g;->g:Ljava/util/Map;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/googlehelp/d/b;->a(Ljava/util/List;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/common/t;->a(Ljava/util/Map;)V

    :cond_2
    return-object v2
.end method
