.class final Lcom/google/android/gms/photos/autobackup/ui/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/ui/t;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/ui/t;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/v;->a:Lcom/google/android/gms/photos/autobackup/ui/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 51
    check-cast p1, Lcom/google/android/gms/photos/autobackup/h;

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/v;->a:Lcom/google/android/gms/photos/autobackup/ui/t;

    iget-boolean v0, v0, Lcom/google/android/gms/photos/autobackup/ui/t;->e:Z

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/google/android/gms/photos/autobackup/h;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/photos/autobackup/h;->b()Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/v;->a:Lcom/google/android/gms/photos/autobackup/ui/t;

    iget-object v1, v1, Lcom/google/android/gms/photos/autobackup/ui/t;->d:Lcom/google/android/gms/photos/autobackup/ui/r;

    invoke-interface {p1}, Lcom/google/android/gms/photos/autobackup/h;->b()Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;

    move-result-object v2

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iput v0, v1, Lcom/google/android/gms/photos/autobackup/ui/r;->b:I

    const/4 v0, 0x0

    iput-boolean v0, v1, Lcom/google/android/gms/photos/autobackup/ui/r;->a:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/v;->a:Lcom/google/android/gms/photos/autobackup/ui/t;

    iget-object v0, v0, Lcom/google/android/gms/photos/autobackup/ui/t;->a:Lcom/google/android/libraries/social/b/a;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/v;->a:Lcom/google/android/gms/photos/autobackup/ui/t;

    iget-object v1, v1, Lcom/google/android/gms/photos/autobackup/ui/t;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/social/b/a;->a(Ljava/lang/Runnable;J)Lcom/google/android/libraries/social/b/b;

    :cond_2
    return-void

    :pswitch_0
    iget v3, v1, Lcom/google/android/gms/photos/autobackup/ui/r;->b:I

    if-eq v3, v4, :cond_3

    iget-boolean v3, v1, Lcom/google/android/gms/photos/autobackup/ui/r;->a:Z

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, v1, Lcom/google/android/gms/photos/autobackup/ui/r;->c:Lcom/google/android/gms/photos/autobackup/ui/s;

    invoke-interface {v3, v2}, Lcom/google/android/gms/photos/autobackup/ui/s;->b(Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V

    goto :goto_0

    :cond_4
    iget-object v3, v1, Lcom/google/android/gms/photos/autobackup/ui/r;->c:Lcom/google/android/gms/photos/autobackup/ui/s;

    invoke-interface {v3, v2}, Lcom/google/android/gms/photos/autobackup/ui/s;->a(Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V

    goto :goto_0

    :pswitch_1
    iget v2, v1, Lcom/google/android/gms/photos/autobackup/ui/r;->b:I

    if-ne v2, v4, :cond_0

    iget-object v2, v1, Lcom/google/android/gms/photos/autobackup/ui/r;->c:Lcom/google/android/gms/photos/autobackup/ui/s;

    invoke-interface {v2}, Lcom/google/android/gms/photos/autobackup/ui/s;->a()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
