.class public abstract Lcom/google/android/gms/games/ui/ce;
.super Lcom/google/android/gms/games/ui/cb;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/ui/cb;-><init>(Landroid/content/Context;I)V

    .line 25
    return-void
.end method


# virtual methods
.method public varargs a([Lcom/google/android/gms/common/data/d;)V
    .locals 4

    .prologue
    .line 34
    new-instance v1, Lcom/google/android/gms/common/data/c;

    invoke-direct {v1}, Lcom/google/android/gms/common/data/c;-><init>()V

    .line 35
    const/4 v0, 0x0

    array-length v2, p1

    :goto_0
    if-ge v0, v2, :cond_0

    .line 36
    aget-object v3, p1, v0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/data/c;->a(Lcom/google/android/gms/common/data/d;)V

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39
    :cond_0
    invoke-super {p0, v1}, Lcom/google/android/gms/games/ui/cb;->a(Lcom/google/android/gms/common/data/d;)V

    .line 40
    return-void
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ce;->c()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/common/data/c;

    if-eqz v1, :cond_0

    .line 47
    check-cast v0, Lcom/google/android/gms/common/data/c;

    .line 48
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/c;->a()V

    .line 50
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/cb;->notifyDataSetChanged()V

    .line 51
    return-void
.end method
