.class public final Lcom/google/android/gms/drive/ui/picker/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/drive/g/av;

.field final c:Lcom/google/android/gms/drive/ui/picker/a/s;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/ui/picker/a/s;)V
    .locals 4

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/d;->a:Landroid/content/Context;

    .line 44
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/s;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/d;->c:Lcom/google/android/gms/drive/ui/picker/a/s;

    .line 45
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 46
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->b()Lcom/google/android/gms/drive/g/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 47
    new-instance v1, Lcom/google/android/gms/drive/g/av;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/d;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/g/av;-><init>(Landroid/content/Context;Landroid/text/format/Time;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/d;->b:Lcom/google/android/gms/drive/g/av;

    .line 48
    return-void
.end method
