.class public final Lcom/google/android/gms/notifications/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/notifications/f;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/server/n;

.field private final d:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/notifications/f;->b:Landroid/content/Context;

    .line 40
    new-instance v0, Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/notifications/f;->c:Lcom/google/android/gms/common/server/n;

    .line 43
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/notifications/f;->d:Landroid/graphics/BitmapFactory$Options;

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/notifications/f;->d:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 45
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/notifications/f;
    .locals 2

    .prologue
    .line 51
    const-class v1, Lcom/google/android/gms/notifications/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/notifications/f;->a:Lcom/google/android/gms/notifications/f;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/google/android/gms/notifications/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/notifications/f;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/notifications/f;->a:Lcom/google/android/gms/notifications/f;

    .line 55
    :cond_0
    sget-object v0, Lcom/google/android/gms/notifications/f;->a:Lcom/google/android/gms/notifications/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a([B)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 94
    if-eqz p1, :cond_0

    .line 99
    :try_start_0
    const-string v1, "icon"

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/notifications/f;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-static {v1, v2, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    .line 100
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 103
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 107
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 116
    :cond_0
    :goto_0
    return-object v0

    .line 104
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 105
    :goto_1
    :try_start_3
    const-string v3, "GnotsImageLoader"

    const-string v4, "Failed to create temp file."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 107
    if-eqz v2, :cond_0

    .line 109
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 111
    :catch_1
    move-exception v1

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v2, :cond_1

    .line 109
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 111
    :cond_1
    :goto_3
    throw v0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_3

    .line 107
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 104
    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)[B
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/notifications/f;->b:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 82
    :goto_1
    return-object v0

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 70
    :cond_1
    if-eqz p1, :cond_2

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/notifications/f;->c:Lcom/google/android/gms/common/server/n;

    iget-object v2, p0, Lcom/google/android/gms/notifications/f;->b:Landroid/content/Context;

    invoke-virtual {v0, v2, p1}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Ljava/lang/String;)[B
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_1

    .line 74
    :catch_0
    move-exception v0

    .line 76
    const-string v2, "GnotsImageLoader"

    const-string v3, "Failed to fetch the image."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_2
    move-object v0, v1

    .line 82
    goto :goto_1

    .line 77
    :catch_1
    move-exception v0

    .line 78
    const-string v2, "GnotsImageLoader"

    const-string v3, "Failed to allocate memory for fetched image."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 127
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-object v0

    .line 133
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/notifications/f;->d:Landroid/graphics/BitmapFactory$Options;

    invoke-static {p1, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_0

    .line 136
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/google/android/gms/notifications/f;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 138
    const v2, 0x1050006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 140
    const v3, 0x1050005

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 142
    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 146
    :catch_0
    move-exception v1

    .line 148
    const-string v2, "GnotsImageLoader"

    const-string v3, "Failed to scale the image to proper size."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 149
    :catch_1
    move-exception v1

    .line 150
    const-string v2, "GnotsImageLoader"

    const-string v3, "Failed to allocate memory to decode image."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
