.class final Lcom/google/android/gms/cast/media/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast_mirroring/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/media/o;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/o;)V
    .locals 0

    .prologue
    .line 2175
    iput-object p1, p0, Lcom/google/android/gms/cast/media/u;->a:Lcom/google/android/gms/cast/media/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2178
    packed-switch p1, :pswitch_data_0

    .line 2192
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2180
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/cast/media/u;->a:Lcom/google/android/gms/cast/media/o;

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->e(Lcom/google/android/gms/cast/media/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/aa;

    iget-object v3, v1, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-interface {v0, v3, p2}, Lcom/google/android/gms/cast/media/aa;->a(Lcom/google/android/gms/cast/CastDevice;I)V

    goto :goto_1

    .line 2187
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/cast/media/u;->a:Lcom/google/android/gms/cast/media/o;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Mirroring failed due to an error: major=%d, minor=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2189
    iget-object v0, p0, Lcom/google/android/gms/cast/media/u;->a:Lcom/google/android/gms/cast/media/o;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/media/o;->a(Z)V

    goto :goto_0

    .line 2178
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
