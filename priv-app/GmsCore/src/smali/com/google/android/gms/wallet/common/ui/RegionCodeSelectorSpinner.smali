.class public Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;
.super Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cv;


# instance fields
.field private b:Lcom/google/android/gms/wallet/common/ui/cw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;-><init>(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;)Lcom/google/android/gms/wallet/common/ui/cw;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->b:Lcom/google/android/gms/wallet/common/ui/cw;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/common/ui/cw;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->b:Lcom/google/android/gms/wallet/common/ui/cw;

    .line 97
    return-void
.end method

.method public final a([I)V
    .locals 5

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cy;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->gV:I

    sget v3, Lcom/google/android/gms/j;->eg:I

    invoke-static {p1}, Lcom/google/android/gms/common/util/h;->a([I)[Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/wallet/common/ui/cy;-><init>(Landroid/content/Context;II[Ljava/lang/Integer;)V

    .line 66
    sget v1, Lcom/google/android/gms/l;->hC:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cy;->setDropDownViewResource(I)V

    .line 67
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 68
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cx;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/cx;-><init>(Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 87
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->e()Z

    move-result v0

    return v0
.end method

.method public final g_(I)V
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    const-string v1, "Populate selector with region codes before setting the selected Region Code"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cy;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cy;->getPosition(Ljava/lang/Object;)I

    move-result v0

    .line 54
    if-ltz v0, :cond_0

    .line 55
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;->a(I)V

    .line 58
    :cond_0
    return-void
.end method
