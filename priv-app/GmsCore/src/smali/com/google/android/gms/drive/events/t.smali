.class public final Lcom/google/android/gms/drive/events/t;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(J)Lcom/google/android/gms/drive/database/model/bl;
    .locals 2

    .prologue
    .line 77
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    .line 78
    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/drive/database/r;->g(J)Lcom/google/android/gms/drive/database/model/bl;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/google/android/gms/drive/events/f;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/bl;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/drive/events/f;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/drive/DriveId;

    .line 53
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->g()Lcom/google/android/gms/drive/database/i;

    move-result-object v1

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/drive/events/f;->k()Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    .line 56
    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/database/r;->c(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v3

    .line 58
    new-instance v0, Lcom/google/android/gms/drive/database/model/bl;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    iget-wide v3, v3, Lcom/google/android/gms/drive/database/model/ad;->f:J

    const-wide/16 v6, 0x0

    move v9, v8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/drive/database/model/bl;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;JLjava/lang/String;JII)V

    .line 61
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 63
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bl;->i()V

    .line 64
    iget-wide v2, v0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/drive/events/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/gms/drive/events/t;->a(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/drive/events/f;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/gms/drive/events/t;->a(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;)V

    .line 69
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0
.end method

.method private static a(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 132
    if-eqz p3, :cond_0

    .line 133
    new-instance v0, Lcom/google/android/gms/drive/database/model/bm;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/drive/database/model/bm;-><init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;)V

    .line 135
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bm;->i()V

    .line 137
    :cond_0
    return-void
.end method

.method static b(J)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 89
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/events/t;->a(J)Lcom/google/android/gms/drive/database/model/bl;

    move-result-object v1

    .line 90
    if-nez v1, :cond_0

    .line 91
    const-string v1, "PersistedEventStore"

    const-string v2, "Trying to delete an already deleted PersistedEvent"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :goto_0
    return v0

    .line 95
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bl;->j()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    const/4 v0, 0x1

    goto :goto_0

    .line 96
    :catch_0
    move-exception v1

    .line 97
    const-string v2, "PersistedEventStore"

    const-string v3, "Error deleting PersistedEvent"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
