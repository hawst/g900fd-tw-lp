.class public final enum Lcom/google/android/gms/drive/database/model/q;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/q;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/q;

.field private static final synthetic d:[Lcom/google/android/gms/drive/database/model/q;


# instance fields
.field private final c:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 48
    new-instance v0, Lcom/google/android/gms/drive/database/model/q;

    const-string v1, "APP_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/p;->d()Lcom/google/android/gms/drive/database/model/p;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "appId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/q;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/q;->a:Lcom/google/android/gms/drive/database/model/q;

    .line 58
    new-instance v0, Lcom/google/android/gms/drive/database/model/q;

    const-string v1, "SCOPE"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/p;->d()Lcom/google/android/gms/drive/database/model/p;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "scope"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    new-array v4, v6, [Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/drive/database/model/q;->a:Lcom/google/android/gms/drive/database/model/q;

    iget-object v5, v5, Lcom/google/android/gms/drive/database/model/q;->c:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/database/model/q;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/q;->b:Lcom/google/android/gms/drive/database/model/q;

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/q;

    sget-object v1, Lcom/google/android/gms/drive/database/model/q;->a:Lcom/google/android/gms/drive/database/model/q;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/q;->b:Lcom/google/android/gms/drive/database/model/q;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/drive/database/model/q;->d:[Lcom/google/android/gms/drive/database/model/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/q;->c:Lcom/google/android/gms/drive/database/model/ab;

    .line 68
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/q;
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/google/android/gms/drive/database/model/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/q;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/q;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/gms/drive/database/model/q;->d:[Lcom/google/android/gms/drive/database/model/q;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/q;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/q;->c:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/q;->c:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
