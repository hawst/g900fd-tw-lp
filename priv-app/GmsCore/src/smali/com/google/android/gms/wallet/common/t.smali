.class public final Lcom/google/android/gms/wallet/common/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Z

.field private c:Ljava/util/Locale;

.field private d:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/t;->a:Z

    .line 116
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/t;->b:Z

    .line 117
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/t;->c:Ljava/util/Locale;

    .line 118
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/t;->d:I

    .line 119
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/t;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wallet/common/s;
    .locals 6

    .prologue
    .line 162
    new-instance v0, Lcom/google/android/gms/wallet/common/s;

    new-instance v1, Lcom/google/android/gms/wallet/common/u;

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/t;->a:Z

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/common/t;->b:Z

    iget v4, p0, Lcom/google/android/gms/wallet/common/t;->d:I

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/t;->c:Ljava/util/Locale;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/wallet/common/u;-><init>(ZZILjava/util/Locale;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/common/s;-><init>(Lcom/google/android/gms/wallet/common/u;B)V

    return-object v0
.end method
