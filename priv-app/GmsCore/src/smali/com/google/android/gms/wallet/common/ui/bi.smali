.class public final Lcom/google/android/gms/wallet/common/ui/bi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/am;
.implements Lcom/google/android/gms/wallet/common/ui/dn;


# instance fields
.field private final a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private final b:Lcom/google/android/gms/wallet/common/ui/dn;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/dn;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bi;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 19
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/bi;->b:Lcom/google/android/gms/wallet/common/ui/dn;

    .line 20
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bi;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bi;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bi;->b:Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bi;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bi;->b:Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 35
    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bi;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 36
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bi;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 38
    :cond_0
    return v0

    .line 34
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
