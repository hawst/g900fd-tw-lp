.class public final Lcom/google/android/gms/notifications/GunsBrowserActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field private final a:Ljava/util/Set;

.field private b:Lcom/google/c/e/a/a/a/a/c;

.field private c:Landroid/webkit/WebView;

.field private d:Z

.field private e:Z

.field private f:Landroid/widget/ProgressBar;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 63
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    sget-object v0, Lcom/google/android/gms/notifications/a/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/notifications/a/a;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    sget-object v0, Lcom/google/android/gms/notifications/a/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/notifications/a/a;->a(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Landroid/util/Patterns;->DOMAIN_NAME:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/android/gms/notifications/a/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/notifications/a/a;->a(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    iput-object v2, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a:Ljava/util/Set;

    .line 64
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/notifications/GunsBrowserActivity;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->f:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 303
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/notifications/g;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    .line 304
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 305
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->g:Ljava/lang/String;

    .line 135
    invoke-direct {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b()Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-direct {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c()Ljava/lang/String;

    move-result-object v2

    .line 138
    invoke-static {p1}, Lcom/google/android/gms/notifications/k;->a(Landroid/content/Intent;)Lcom/google/c/e/a/a/a/a/c;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    .line 139
    const-string v3, "com.google.android.gms.notifications.intents.accountName"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->g:Ljava/lang/String;

    .line 140
    invoke-direct {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b()Ljava/lang/String;

    move-result-object v3

    .line 141
    invoke-direct {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c()Ljava/lang/String;

    move-result-object v4

    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->g:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/gms/common/util/a;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 145
    const-string v0, "GnotsBrowserActivity"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "GnotsBrowserActivity"

    const-string v1, "Intended account doesn\'t exist on device"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a()V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->finish()V

    .line 180
    :cond_1
    :goto_0
    return-void

    .line 156
    :cond_2
    iget-boolean v5, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->e:Z

    if-eqz v5, :cond_3

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    iget-object v5, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    :cond_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 161
    const-string v0, "GnotsBrowserActivity"

    const-string v1, "Trying to navigate to null/empty url/accountname"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->finish()V

    goto :goto_0

    .line 167
    :cond_4
    iput-boolean v7, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->e:Z

    .line 168
    iput-boolean v7, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->d:Z

    .line 170
    const-string v0, "GnotsBrowserActivity"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 171
    const-string v0, "GnotsBrowserActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loading "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/support/v4/app/au;->a(I)V

    .line 178
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/notifications/GunsBrowserActivity;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 35
    const-string v0, "GnotsBrowserActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GnotsBrowserActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error loading url. ErrorCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Description : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FailingUrl :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/f;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->i:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->finish()V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 211
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/h;->a:Ljava/lang/String;

    .line 215
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    iget-object v1, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 228
    :goto_0
    return-void

    .line 218
    :cond_0
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "http"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "accounts.google.com"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "MergeSession"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "uberauth"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "source"

    const-string v3, "gms.gnots"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "continue"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 226
    iget-object v1, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    iget-boolean v0, v0, Lcom/google/c/e/a/a/a/a/h;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/h;->a:Ljava/lang/String;

    .line 316
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/notifications/GunsBrowserActivity;)Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->d:Z

    return v0
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/b;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->b:Lcom/google/c/e/a/a/a/a/c;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/b;->b:Ljava/lang/String;

    .line 325
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/notifications/GunsBrowserActivity;)V
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->e:Z

    invoke-direct {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a()V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/notifications/GunsBrowserActivity;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 2

    .prologue
    .line 184
    if-eqz p1, :cond_0

    .line 185
    const/4 v0, 0x0

    .line 188
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/notifications/d;

    iget-object v1, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->g:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/notifications/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 35
    check-cast p2, Landroid/os/Bundle;

    iget v0, p1, Landroid/support/v4/a/j;->m:I

    if-nez v0, :cond_0

    const-string v0, "GnotsAuthLoader.hadError"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "GnotsAuthLoader.uberAuthToken"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/q;->onBackPressed()V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 68
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 70
    sget v0, Lcom/google/android/gms/l;->cr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->setContentView(I)V

    .line 71
    sget v0, Lcom/google/android/gms/j;->jv:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c:Landroid/webkit/WebView;

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    .line 75
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    .line 76
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 77
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 78
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 79
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    .line 80
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->c:Landroid/webkit/WebView;

    new-instance v1, Lcom/google/android/gms/notifications/b;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/notifications/b;-><init>(Lcom/google/android/gms/notifications/GunsBrowserActivity;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 84
    sget v0, Lcom/google/android/gms/j;->ju:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->f:Landroid/widget/ProgressBar;

    .line 86
    if-eqz p1, :cond_0

    .line 87
    const-string v0, "isAlreadyCancelled"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->e:Z

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a(Landroid/content/Intent;)V

    .line 93
    return-void
.end method

.method protected final onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onNewIntent(Landroid/content/Intent;)V

    .line 99
    invoke-virtual {p0, p1}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->setIntent(Landroid/content/Intent;)V

    .line 100
    invoke-direct {p0, p1}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->a(Landroid/content/Intent;)V

    .line 101
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 113
    const-string v0, "isAlreadyCancelled"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->e:Z

    .line 114
    return-void
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    const-string v0, "isAlreadyCancelled"

    iget-boolean v1, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 108
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 128
    iget-boolean v0, p0, Lcom/google/android/gms/notifications/GunsBrowserActivity;->d:Z

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsBrowserActivity;->finish()V

    .line 131
    :cond_0
    return-void
.end method
