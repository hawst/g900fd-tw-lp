.class public final Lcom/google/android/gms/auth/trustagent/at;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/ComponentName;

.field private static final b:Landroid/content/Intent;

.field private static c:Lcom/google/android/gms/auth/trustagent/at;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/auth/g/a;

.field private final f:Ljava/util/List;

.field private final g:Ljava/lang/Object;

.field private h:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gms"

    const-string v2, "com.google.android.gms.auth.trustagent.trustlet.TrustStateMonitorService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/trustagent/at;->a:Landroid/content/ComponentName;

    .line 56
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/trustagent/at;->a:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/trustagent/at;->b:Landroid/content/Intent;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/gms/auth/g/a;

    invoke-direct {v0}, Lcom/google/android/gms/auth/g/a;-><init>()V

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/auth/trustagent/at;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/g/a;Ljava/util/List;)V

    .line 96
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/g/a;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->d:Landroid/content/Context;

    .line 104
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/g/a;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->e:Lcom/google/android/gms/auth/g/a;

    .line 105
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->f:Ljava/util/List;

    .line 106
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->g:Ljava/lang/Object;

    .line 107
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/auth/trustagent/at;
    .locals 3

    .prologue
    .line 81
    const-class v1, Lcom/google/android/gms/auth/trustagent/at;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/auth/trustagent/at;->c:Lcom/google/android/gms/auth/trustagent/at;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/trustagent/at;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/auth/trustagent/at;->c:Lcom/google/android/gms/auth/trustagent/at;

    .line 85
    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/trustagent/at;->c:Lcom/google/android/gms/auth/trustagent/at;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/google/android/gms/auth/trustagent/ax;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 202
    new-instance v4, Lcom/google/android/gms/common/b;

    invoke-direct {v4}, Lcom/google/android/gms/common/b;-><init>()V

    .line 203
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/at;->d:Landroid/content/Context;

    const-string v2, "TrustStateTracker"

    sget-object v3, Lcom/google/android/gms/auth/trustagent/at;->b:Landroid/content/Intent;

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    new-instance v0, Lcom/google/android/gms/auth/trustagent/az;

    const-string v1, "Cannot connect to TrustStateMonitorService."

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/trustagent/az;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_0
    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/s;->a(Landroid/os/IBinder;)Lcom/google/android/gms/auth/trustagent/trustlet/r;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 221
    :try_start_1
    invoke-interface {p1, v0}, Lcom/google/android/gms/auth/trustagent/ax;->a(Lcom/google/android/gms/auth/trustagent/trustlet/r;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 223
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/at;->d:Landroid/content/Context;

    invoke-virtual {v1, v2, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    return-object v0

    .line 216
    :catch_0
    move-exception v0

    .line 217
    :try_start_2
    new-instance v1, Lcom/google/android/gms/auth/trustagent/az;

    const-string v2, "Cannot connect to TrustStateMonitorService."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/trustagent/az;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 223
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/at;->d:Landroid/content/Context;

    invoke-virtual {v1, v2, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/at;Z)V
    .locals 3

    .prologue
    .line 39
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/at;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/aw;

    invoke-interface {v0}, Lcom/google/android/gms/auth/trustagent/aw;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/at;Z)V
    .locals 3

    .prologue
    .line 39
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/at;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/aw;

    invoke-interface {v0}, Lcom/google/android/gms/auth/trustagent/aw;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/trustagent/aw;)V
    .locals 4

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/at;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lcom/google/android/gms/auth/trustagent/az;

    const-string v1, "TrustStateMonitorService is not supported."

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/trustagent/az;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/at;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->h:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 118
    new-instance v0, Lcom/google/android/gms/auth/trustagent/ay;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/ay;-><init>(Lcom/google/android/gms/auth/trustagent/at;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->h:Landroid/content/BroadcastReceiver;

    .line 119
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 120
    const-string v2, "com.google.android.gms.auth.trustagent.TRUST_AGENT_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 121
    const-string v2, "com.google.android.gms.auth.trustagent.TRUST_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 122
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/at;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/at;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 124
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->e:Lcom/google/android/gms/auth/g/a;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/auth/trustagent/aw;)V
    .locals 3

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/at;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Lcom/google/android/gms/auth/trustagent/az;

    const-string v1, "TrustStateMonitorService is not supported."

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/trustagent/az;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/at;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->h:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/at;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/at;->h:Landroid/content/BroadcastReceiver;

    .line 139
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/at;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Lcom/google/android/gms/auth/trustagent/az;

    const-string v1, "TrustStateMonitorService is not supported."

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/trustagent/az;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/trustagent/au;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/au;-><init>(Lcom/google/android/gms/auth/trustagent/at;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/at;->a(Lcom/google/android/gms/auth/trustagent/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/at;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Lcom/google/android/gms/auth/trustagent/az;

    const-string v1, "TrustStateMonitorService is not supported."

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/trustagent/az;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/trustagent/av;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/av;-><init>(Lcom/google/android/gms/auth/trustagent/at;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/at;->a(Lcom/google/android/gms/auth/trustagent/ax;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
