.class final Lcom/google/android/gms/cast/activity/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/activity/l;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/activity/l;I)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/gms/cast/activity/q;->a:Lcom/google/android/gms/cast/activity/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p2, p0, Lcom/google/android/gms/cast/activity/q;->b:I

    .line 51
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 55
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/gms/cast/activity/q;->b:I

    iget-object v1, p0, Lcom/google/android/gms/cast/activity/q;->a:Lcom/google/android/gms/cast/activity/l;

    invoke-static {v1}, Lcom/google/android/gms/cast/activity/l;->a(Lcom/google/android/gms/cast/activity/l;)[Landroid/widget/EditText;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/q;->a:Lcom/google/android/gms/cast/activity/l;

    invoke-static {v0}, Lcom/google/android/gms/cast/activity/l;->a(Lcom/google/android/gms/cast/activity/l;)[Landroid/widget/EditText;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/cast/activity/q;->b:I

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 60
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/q;->a:Lcom/google/android/gms/cast/activity/l;

    invoke-static {v0}, Lcom/google/android/gms/cast/activity/l;->c(Lcom/google/android/gms/cast/activity/l;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/activity/q;->a:Lcom/google/android/gms/cast/activity/l;

    invoke-static {v1}, Lcom/google/android/gms/cast/activity/l;->b(Lcom/google/android/gms/cast/activity/l;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 61
    return-void

    .line 57
    :cond_1
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/activity/q;->b:I

    if-lez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/q;->a:Lcom/google/android/gms/cast/activity/l;

    invoke-static {v0}, Lcom/google/android/gms/cast/activity/l;->a(Lcom/google/android/gms/cast/activity/l;)[Landroid/widget/EditText;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/cast/activity/q;->b:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method
