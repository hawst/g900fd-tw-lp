.class final Lcom/google/android/gms/people/service/bg/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/service/bg/e;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/google/android/gms/people/service/bg/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 272
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    .line 273
    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->n()Z

    .line 274
    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 275
    invoke-static {p1}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->e(Landroid/content/Context;)V

    .line 276
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->m()Lcom/google/android/gms/people/sync/c;

    move-result-object v4

    iget-object v0, v4, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-virtual {v0}, Lcom/google/android/gms/people/f/d;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/f/e;

    iget-object v1, v0, Lcom/google/android/gms/people/f/e;->a:Ljava/lang/String;

    iget-object v6, v4, Lcom/google/android/gms/people/sync/c;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/google/android/gms/people/a/c;->c(Z)J

    move-result-wide v8

    invoke-static {v8, v9, v3}, Lcom/google/android/gms/people/sync/c;->a(JZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v2}, Lcom/google/android/gms/people/a/c;->c(Z)J

    move-result-wide v8

    invoke-static {v8, v9, v2}, Lcom/google/android/gms/people/sync/c;->a(JZ)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v7, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    move v1, v3

    :goto_1
    if-nez v1, :cond_0

    iget-object v1, v4, Lcom/google/android/gms/people/sync/c;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v1

    iget-object v6, v0, Lcom/google/android/gms/people/f/e;->a:Ljava/lang/String;

    iget-object v7, v0, Lcom/google/android/gms/people/f/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    cmp-long v1, v10, v6

    if-nez v1, :cond_0

    iget-object v1, v4, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    iget-object v6, v0, Lcom/google/android/gms/people/f/e;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/people/f/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v6, v0}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 277
    :cond_3
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->n()Lcom/google/android/gms/people/sync/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/d;->a()V

    iget-object v0, v1, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/f/f;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/f/g;

    iget-object v3, v1, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/people/f/g;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/gms/people/f/g;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v3, v10, v4

    if-nez v3, :cond_4

    iget-object v3, v1, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    iget-object v4, v0, Lcom/google/android/gms/people/f/g;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/people/f/g;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/people/f/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 279
    :cond_5
    return-void
.end method
