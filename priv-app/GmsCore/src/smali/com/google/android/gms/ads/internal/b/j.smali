.class public final Lcom/google/android/gms/ads/internal/b/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lcom/google/android/gms/ads/internal/b/i;

.field private d:Landroid/util/Base64OutputStream;

.field private e:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/google/android/gms/ads/internal/b/m;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/b/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/b/j;->c:Lcom/google/android/gms/ads/internal/b/i;

    .line 54
    iput p1, p0, Lcom/google/android/gms/ads/internal/b/j;->b:I

    .line 59
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/ads/internal/b/j;->a:I

    .line 60
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 132
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 133
    if-eqz v1, :cond_0

    array-length v0, v1

    if-nez v0, :cond_1

    .line 134
    :cond_0
    const-string v0, ""

    .line 171
    :goto_0
    return-object v0

    .line 136
    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/b/j;->e:Ljava/io/ByteArrayOutputStream;

    .line 137
    new-instance v0, Landroid/util/Base64OutputStream;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b/j;->e:Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0xa

    invoke-direct {v0, v2, v3}, Landroid/util/Base64OutputStream;-><init>(Ljava/io/OutputStream;I)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/b/j;->d:Landroid/util/Base64OutputStream;

    .line 138
    new-instance v2, Ljava/util/PriorityQueue;

    iget v0, p0, Lcom/google/android/gms/ads/internal/b/j;->b:I

    new-instance v3, Lcom/google/android/gms/ads/internal/b/k;

    invoke-direct {v3, p0}, Lcom/google/android/gms/ads/internal/b/k;-><init>(Lcom/google/android/gms/ads/internal/b/j;)V

    invoke-direct {v2, v0, v3}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 145
    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_3

    .line 146
    aget-object v3, v1, v0

    .line 147
    invoke-static {v3}, Lcom/google/android/gms/ads/internal/b/l;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 149
    array-length v4, v3

    iget v5, p0, Lcom/google/android/gms/ads/internal/b/j;->a:I

    if-lt v4, v5, :cond_2

    .line 151
    iget v4, p0, Lcom/google/android/gms/ads/internal/b/j;->b:I

    iget v5, p0, Lcom/google/android/gms/ads/internal/b/j;->a:I

    invoke-static {v3, v4, v5, v2}, Lcom/google/android/gms/ads/internal/b/n;->a([Ljava/lang/String;IILjava/util/PriorityQueue;)V

    .line 145
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 158
    :cond_3
    invoke-virtual {v2}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/b/o;

    .line 160
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/b/j;->d:Landroid/util/Base64OutputStream;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/b/j;->c:Lcom/google/android/gms/ads/internal/b/i;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/b/o;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/b/i;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/util/Base64OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 161
    :catch_0
    move-exception v0

    .line 162
    const-string v2, "Error while writing hash to byteStream"

    invoke-static {v2, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 166
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/j;->d:Landroid/util/Base64OutputStream;

    invoke-virtual {v0}, Landroid/util/Base64OutputStream;->flush()V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/j;->d:Landroid/util/Base64OutputStream;

    invoke-virtual {v0}, Landroid/util/Base64OutputStream;->close()V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/j;->e:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 169
    :catch_1
    move-exception v0

    .line 170
    const-string v1, "HashManager: unable to convert to base 64"

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 171
    const-string v0, ""

    goto :goto_0
.end method
