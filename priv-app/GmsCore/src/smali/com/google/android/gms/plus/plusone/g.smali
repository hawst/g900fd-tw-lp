.class final Lcom/google/android/gms/plus/plusone/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/plusone/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/plusone/e;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/b;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 146
    const-string v0, "PlusOneButtonView"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    const-string v0, "PlusOneButtonView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPlusOneLoaded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/plusone/e;->a:Z

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    iput-boolean v3, v0, Lcom/google/android/gms/plus/plusone/e;->a:Z

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    iget-object v0, v0, Lcom/google/android/gms/plus/plusone/e;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    .line 153
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    iput-object p2, v0, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    iget-object v1, v1, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/plusone/e;->a(Lcom/google/android/gms/plus/data/a/b;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/e;->a()V

    .line 161
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    iput-boolean v3, v0, Lcom/google/android/gms/plus/plusone/e;->m:Z

    .line 162
    return-void

    .line 158
    :cond_2
    const-string v0, "PlusOneButtonView"

    const-string v1, "PlusOne failed to load"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/g;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/e;->e()V

    goto :goto_0
.end method
