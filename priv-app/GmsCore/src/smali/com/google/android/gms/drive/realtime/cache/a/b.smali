.class public final enum Lcom/google/android/gms/drive/realtime/cache/a/b;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/realtime/cache/a/b;

.field public static final enum b:Lcom/google/android/gms/drive/realtime/cache/a/b;

.field private static final synthetic d:[Lcom/google/android/gms/drive/realtime/cache/a/b;


# instance fields
.field private final c:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 52
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/a/b;

    const-string v1, "KEY"

    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->d()Lcom/google/android/gms/drive/realtime/cache/a/a;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "key"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    new-array v4, v7, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/realtime/cache/a/b;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/b;->a:Lcom/google/android/gms/drive/realtime/cache/a/b;

    .line 56
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/a/b;

    const-string v1, "VALUE"

    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->d()Lcom/google/android/gms/drive/realtime/cache/a/a;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "value"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/realtime/cache/a/b;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/b;->b:Lcom/google/android/gms/drive/realtime/cache/a/b;

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/realtime/cache/a/b;

    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/b;->a:Lcom/google/android/gms/drive/realtime/cache/a/b;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/b;->b:Lcom/google/android/gms/drive/realtime/cache/a/b;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/b;->d:[Lcom/google/android/gms/drive/realtime/cache/a/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/a/b;->c:Lcom/google/android/gms/drive/database/model/ab;

    .line 64
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/realtime/cache/a/b;
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/gms/drive/realtime/cache/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/realtime/cache/a/b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/realtime/cache/a/b;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/a/b;->d:[Lcom/google/android/gms/drive/realtime/cache/a/b;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/realtime/cache/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/realtime/cache/a/b;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/a/b;->c:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
