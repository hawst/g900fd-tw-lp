.class final Lcom/google/android/gms/cast/c/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/c/d;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/c/n;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/c/n;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/gms/cast/c/o;->a:Lcom/google/android/gms/cast/c/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/cast/c/o;->a:Lcom/google/android/gms/cast/c/n;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onFilteringFinished"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/cast/c/o;->a:Lcom/google/android/gms/cast/c/n;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/h;->b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/cast/c/p;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast/c/p;-><init>(Lcom/google/android/gms/cast/c/o;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 188
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/cast/c/o;->a:Lcom/google/android/gms/cast/c/n;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onAppIdApproved: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/cast/c/o;->a:Lcom/google/android/gms/cast/c/n;

    iget-object v1, v0, Lcom/google/android/gms/cast/c/n;->b:Lcom/google/android/gms/cast/c/j;

    monitor-enter v1

    .line 174
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/o;->a:Lcom/google/android/gms/cast/c/n;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/n;->b:Lcom/google/android/gms/cast/c/j;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/j;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 175
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
