.class public Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;
.super Lcom/google/android/gms/common/audience/b/b;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/DynamiteApi;
.end annotation

.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 435
    invoke-direct {p0}, Lcom/google/android/gms/common/audience/b/b;-><init>()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "call initialize() first"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 495
    return-void

    .line 494
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 463
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    .line 464
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 465
    const-string v1, "state"

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 466
    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 477
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    .line 478
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(I)V

    .line 479
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 457
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    .line 458
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    const-string v1, "state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 459
    return-void
.end method

.method public final a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/b/l;Lcom/google/android/gms/common/audience/b/d;)V
    .locals 3

    .prologue
    .line 449
    new-instance v2, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1, p3}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;-><init>(Landroid/content/Context;Landroid/content/Context;Lcom/google/android/gms/common/audience/b/d;)V

    iput-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    .line 453
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 1

    .prologue
    .line 471
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    .line 472
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 473
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    .line 490
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b(Z)V

    .line 491
    return-void
.end method

.method public final b()Lcom/google/android/gms/b/l;
    .locals 1

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 483
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->c()V

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl$DynamiteHost;->a:Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Z)V

    .line 485
    return-void
.end method
