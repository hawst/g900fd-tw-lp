.class public final Lcom/google/android/gms/ads/internal/k/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/k/j;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/ads/internal/k/c;

.field final b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

.field final c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field final d:Landroid/content/Context;

.field public final e:Ljava/lang/Object;

.field final f:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field public g:Lcom/google/android/gms/ads/internal/k/a/d;

.field public h:I

.field private final i:Ljava/lang/String;

.field private final j:Lcom/google/android/gms/ads/internal/k/a/a;

.field private final k:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/k/d;Lcom/google/android/gms/ads/internal/k/c;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 4

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/k/g;->e:Ljava/lang/Object;

    .line 45
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/ads/internal/k/g;->h:I

    .line 57
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/k/g;->d:Landroid/content/Context;

    .line 59
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/k/g;->j:Lcom/google/android/gms/ads/internal/k/a/a;

    .line 60
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    .line 69
    const-string v0, "com.google.ads.mediation.customevent.CustomEventAdapter"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/k/g;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/k/g;->i:Ljava/lang/String;

    .line 74
    :goto_0
    iget-wide v0, p4, Lcom/google/android/gms/ads/internal/k/d;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p4, Lcom/google/android/gms/ads/internal/k/d;->b:J

    :goto_1
    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/k/g;->k:J

    .line 78
    iput-object p6, p0, Lcom/google/android/gms/ads/internal/k/g;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    .line 79
    iput-object p7, p0, Lcom/google/android/gms/ads/internal/k/g;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 80
    iput-object p8, p0, Lcom/google/android/gms/ads/internal/k/g;->f:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    .line 81
    return-void

    .line 72
    :cond_0
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/k/g;->i:Ljava/lang/String;

    goto :goto_0

    .line 74
    :cond_1
    const-wide/16 v0, 0x2710

    goto :goto_1
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/k/c;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/g;->j:Lcom/google/android/gms/ads/internal/k/a/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/k/c;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/k/a/a;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter"

    .line 107
    :goto_0
    return-object v0

    .line 101
    :cond_0
    const-string v0, "com.google.ads.mediation.customevent.CustomEventAdapter"
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    const-string v0, "Fail to determine the custom event\'s version, assuming the old one."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 107
    :cond_1
    const-string v0, "com.google.ads.mediation.customevent.CustomEventAdapter"

    goto :goto_0
.end method


# virtual methods
.method final a()Lcom/google/android/gms/ads/internal/k/a/d;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Instantiating mediation adapter: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/g;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 171
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/g;->j:Lcom/google/android/gms/ads/internal/k/a/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/g;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/k/a/a;->a(Ljava/lang/String;)Lcom/google/android/gms/ads/internal/k/a/d;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 174
    :goto_0
    return-object v0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not instantiate mediation adapter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/k/g;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JJ)Lcom/google/android/gms/ads/internal/k/i;
    .locals 15

    .prologue
    .line 121
    iget-object v6, p0, Lcom/google/android/gms/ads/internal/k/g;->e:Ljava/lang/Object;

    monitor-enter v6

    .line 122
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 123
    new-instance v4, Lcom/google/android/gms/ads/internal/k/f;

    invoke-direct {v4}, Lcom/google/android/gms/ads/internal/k/f;-><init>()V

    .line 126
    sget-object v2, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/ads/internal/k/h;

    invoke-direct {v3, p0, v4}, Lcom/google/android/gms/ads/internal/k/h;-><init>(Lcom/google/android/gms/ads/internal/k/g;Lcom/google/android/gms/ads/internal/k/f;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 152
    iget-wide v2, p0, Lcom/google/android/gms/ads/internal/k/g;->k:J

    :goto_0
    iget v5, p0, Lcom/google/android/gms/ads/internal/k/g;->h:I

    const/4 v7, -0x2

    if-ne v5, v7, :cond_2

    const-wide/32 v8, 0xea60

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    sub-long v12, v10, v0

    sub-long v12, v2, v12

    sub-long v10, v10, p1

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v5, v12, v10

    if-lez v5, :cond_0

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-gtz v5, :cond_1

    :cond_0
    const-string v5, "Timed out waiting for adapter."

    invoke-static {v5}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    const/4 v5, 0x3

    iput v5, p0, Lcom/google/android/gms/ads/internal/k/g;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 152
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/google/android/gms/ads/internal/k/g;->e:Ljava/lang/Object;

    invoke-static {v12, v13, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v5

    const/4 v5, -0x1

    :try_start_2
    iput v5, p0, Lcom/google/android/gms/ads/internal/k/g;->h:I

    goto :goto_0

    .line 155
    :cond_2
    new-instance v0, Lcom/google/android/gms/ads/internal/k/i;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/k/g;->g:Lcom/google/android/gms/ads/internal/k/a/d;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/k/g;->i:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/ads/internal/k/g;->h:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/k/i;-><init>(Lcom/google/android/gms/ads/internal/k/c;Lcom/google/android/gms/ads/internal/k/a/d;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/f;I)V

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 113
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/g;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 114
    :try_start_0
    iput p1, p0, Lcom/google/android/gms/ads/internal/k/g;->h:I

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/g;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 116
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
