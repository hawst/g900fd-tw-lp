.class public final Lcom/google/android/gms/photos/a/a/g;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/photos/a/a/e;

.field public b:Lcom/google/android/gms/photos/a/a/b;

.field public c:Lcom/google/android/gms/photos/a/a/a/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/photos/a/a/g;->cachedSize:I

    .line 36
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 65
    iget-object v1, p0, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    if-eqz v1, :cond_0

    .line 66
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    if-eqz v1, :cond_1

    .line 70
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    if-eqz v1, :cond_2

    .line 74
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/photos/a/a/g;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/photos/a/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/photos/a/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/photos/a/a/b;

    invoke-direct {v0}, Lcom/google/android/gms/photos/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/photos/a/a/a/d;

    invoke-direct {v0}, Lcom/google/android/gms/photos/a/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x2a -> :sswitch_2
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    if-eqz v0, :cond_0

    .line 51
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    if-eqz v0, :cond_1

    .line 54
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    if-eqz v0, :cond_2

    .line 57
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 59
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 60
    return-void
.end method
