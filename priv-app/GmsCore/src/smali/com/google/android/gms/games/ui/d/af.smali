.class public final Lcom/google/android/gms/games/ui/d/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/dm;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/SearchView;

.field final synthetic b:Lcom/google/android/gms/games/ui/d/ac;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/d/ac;Landroid/support/v7/widget/SearchView;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/af;->b:Lcom/google/android/gms/games/ui/d/ac;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/d/af;->a:Landroid/support/v7/widget/SearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/af;->a:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/af;->a:Landroid/support/v7/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 282
    :cond_0
    return v2
.end method
