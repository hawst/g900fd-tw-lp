.class final Lcom/google/android/gms/wallet/address/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/cache/e;


# instance fields
.field private final a:Lcom/google/android/gms/identity/intents/UserAddressRequest;

.field private final b:Lcom/google/android/gms/wallet/address/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/identity/intents/UserAddressRequest;Lcom/google/android/gms/wallet/address/c;)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/google/android/gms/wallet/address/b;->a:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    .line 137
    iput-object p2, p0, Lcom/google/android/gms/wallet/address/b;->b:Lcom/google/android/gms/wallet/address/c;

    .line 138
    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;IILcom/google/checkout/inapp/proto/ai;)V
    .locals 2

    .prologue
    .line 143
    if-eqz p4, :cond_0

    .line 144
    new-instance v0, Lcom/google/aa/a/a/a/b;

    invoke-direct {v0}, Lcom/google/aa/a/a/a/b;-><init>()V

    .line 147
    iget-object v1, p4, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    iput-object v1, v0, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    .line 148
    iget-object v1, p4, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    iput-object v1, v0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    .line 149
    iget-object v1, p0, Lcom/google/android/gms/wallet/address/b;->a:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/aa/a/a/a/b;Lcom/google/android/gms/identity/intents/UserAddressRequest;)[Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    .line 151
    array-length v1, v0

    if-lez v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/google/android/gms/wallet/address/b;->b:Lcom/google/android/gms/wallet/address/c;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/address/c;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    .line 157
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/b;->b:Lcom/google/android/gms/wallet/address/c;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/address/c;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0
.end method
