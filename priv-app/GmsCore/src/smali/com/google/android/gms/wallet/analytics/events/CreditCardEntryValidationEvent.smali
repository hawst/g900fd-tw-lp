.class public Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;
.super Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Z

.field public final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/d;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>(Landroid/os/Parcel;)V

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->a:Z

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->b:Z

    .line 35
    return-void

    :cond_0
    move v0, v2

    .line 33
    goto :goto_0

    :cond_1
    move v1, v2

    .line 34
    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(ZZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>()V

    .line 26
    iput-object p3, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->j:Ljava/lang/String;

    .line 27
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->a:Z

    .line 28
    iput-boolean p2, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->b:Z

    .line 29
    return-void
.end method

.method public static a(Landroid/content/Context;ZZLjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;-><init>(ZZLjava/lang/String;)V

    .line 41
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 42
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 53
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 54
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->b:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 55
    return-void

    :cond_0
    move v0, v2

    .line 53
    goto :goto_0

    :cond_1
    move v1, v2

    .line 54
    goto :goto_1
.end method
