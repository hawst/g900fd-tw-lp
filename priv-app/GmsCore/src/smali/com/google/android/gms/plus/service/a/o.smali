.class public final Lcom/google/android/gms/plus/service/a/o;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/c;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/c;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/o;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 41
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/o;->b:Lcom/google/android/gms/plus/internal/c;

    .line 42
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/a/o;->c:I

    .line 43
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/o;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v1, p0, Lcom/google/android/gms/plus/service/a/o;->c:I

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;I)Lcom/google/android/gms/plus/data/a/c;

    move-result-object v0

    .line 51
    if-nez v0, :cond_0

    .line 52
    const-string v0, "GetSignUpStateOperation"

    const-string v1, "Unable to load the user\'s sign-up state"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/o;->b:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 69
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/o;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v0, v0, Lcom/google/android/gms/plus/data/a/c;->a:Landroid/os/Bundle;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 60
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v5, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 61
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/o;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-interface {v0, v6, v1, v4}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    .line 64
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/o;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/o;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-interface {v1, v6, v0, v4}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    .line 67
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/o;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v4, v4}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/o;->b:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/o;->b:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 77
    :cond_0
    return-void
.end method
