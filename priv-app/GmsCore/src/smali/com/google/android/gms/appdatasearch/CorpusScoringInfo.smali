.class public Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/h;


# instance fields
.field final a:I

.field public final b:Lcom/google/android/gms/appdatasearch/CorpusId;

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/appdatasearch/h;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/h;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/appdatasearch/CorpusId;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->a:I

    .line 42
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->b:Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 43
    iput p3, p0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->c:I

    .line 44
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/h;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/h;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/h;->a(Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;Landroid/os/Parcel;I)V

    .line 77
    return-void
.end method
