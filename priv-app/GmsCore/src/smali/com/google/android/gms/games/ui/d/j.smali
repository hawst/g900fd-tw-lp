.class public final Lcom/google/android/gms/games/ui/d/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/ui/common/players/h;


# instance fields
.field private a:Z

.field private b:Lcom/google/android/gms/games/ui/d/k;

.field private c:Lcom/google/android/gms/games/ui/q;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/d/k;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/d/k;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->b:Lcom/google/android/gms/games/ui/d/k;

    .line 54
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/q;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->c:Lcom/google/android/gms/games/ui/q;

    .line 55
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 24
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->c:Lcom/google/android/gms/games/ui/q;

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogUpdatingProfileVisibility"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->c:Lcom/google/android/gms/games/ui/q;

    sget v1, Lcom/google/android/gms/p;->kp:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->b:Lcom/google/android/gms/games/ui/d/k;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d/j;->a:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/d/k;->a(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->c:Lcom/google/android/gms/games/ui/q;

    sget v1, Lcom/google/android/gms/p;->ko:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->c:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    const-string v0, "ProfileVisiHelp"

    const-string v1, "Trying to set visibility when not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :goto_0
    return-void

    .line 70
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v1, v0, p1}, Lcom/google/android/gms/games/y;->c(Lcom/google/android/gms/common/api/v;Z)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 71
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/d/j;->a:Z

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->c:Lcom/google/android/gms/games/ui/q;

    sget v1, Lcom/google/android/gms/p;->kq:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/q;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-static {v0}, Lcom/google/android/gms/games/ui/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;

    move-result-object v0

    .line 76
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/j;->c:Lcom/google/android/gms/games/ui/q;

    const-string v2, "com.google.android.gms.games.ui.dialog.progressDialogUpdatingProfileVisibility"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/j;->b:Lcom/google/android/gms/games/ui/d/k;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/d/k;->M()Z

    move-result v0

    return v0
.end method
