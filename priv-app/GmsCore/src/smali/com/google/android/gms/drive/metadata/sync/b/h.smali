.class public final Lcom/google/android/gms/drive/metadata/sync/b/h;
.super Lcom/google/android/gms/drive/metadata/sync/b/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/drive/g/aw;

.field private final c:Lcom/google/android/gms/drive/database/model/a;

.field private final d:Ljava/util/List;

.field private final e:J

.field private final f:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

.field private final g:Landroid/content/SyncResult;

.field private final h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/a;Ljava/util/List;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;Landroid/content/SyncResult;)V
    .locals 10

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/b/a;-><init>()V

    .line 51
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/aw;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->b:Lcom/google/android/gms/drive/g/aw;

    .line 52
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->c:Lcom/google/android/gms/drive/database/model/a;

    .line 53
    const-wide/high16 v0, -0x8000000000000000L

    .line 54
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    .line 55
    iget-object v1, v0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 56
    iget-object v1, v0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 57
    iget-object v1, v0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 58
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 59
    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-gtz v1, :cond_0

    .line 60
    const-string v1, "PreparedSyncMore"

    const-string v5, "Unexpectedly low clipTime in %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {v1, v5, v6}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    move-wide v0, v2

    move-wide v2, v0

    .line 63
    goto :goto_0

    .line 56
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 64
    :cond_2
    iput-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->e:J

    .line 65
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->d:Ljava/util/List;

    .line 66
    iput-object p4, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->f:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    .line 67
    iput-object p5, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->g:Landroid/content/SyncResult;

    .line 68
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->p()Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    .line 69
    return-void
.end method

.method private static a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 128
    const-string v0, "PreparedSyncMore"

    const-string v1, "Error syncing more."

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 129
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    .line 84
    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    const/4 v0, 0x0

    .line 88
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->c:Lcom/google/android/gms/drive/database/model/a;

    iget-object v3, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;)V

    .line 96
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    .line 98
    new-instance v6, Lcom/google/android/gms/drive/metadata/sync/b/i;

    iget-object v7, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->c:Lcom/google/android/gms/drive/database/model/a;

    iget-object v8, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->f:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    invoke-direct {v6, v7, v8, v0, p1}, Lcom/google/android/gms/drive/metadata/sync/b/i;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;Lcom/google/android/gms/drive/database/model/bb;I)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/af/c/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/af/c/a/c; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/drive/metadata/sync/syncadapter/o; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/sync/b/h;->a(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b(Ljava/lang/String;)V

    :goto_1
    move v0, v2

    .line 124
    :goto_2
    return v0

    .line 101
    :cond_0
    :try_start_2
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;

    .line 102
    :goto_3
    new-instance v4, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;

    iget-object v5, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->b:Lcom/google/android/gms/drive/g/aw;

    invoke-direct {v4, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    .line 103
    iget-object v5, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->g:Landroid/content/SyncResult;

    invoke-interface {v0, v4, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Landroid/content/SyncResult;)V

    .line 104
    iget-object v5, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->g:Landroid/content/SyncResult;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a(Landroid/content/SyncResult;)V

    .line 105
    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->g:Landroid/content/SyncResult;

    const/4 v5, 0x1

    invoke-interface {v0, v4, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;->a(Landroid/content/SyncResult;Z)V
    :try_end_2
    .catch Lcom/google/af/c/a/a; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/af/c/a/c; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/android/gms/drive/metadata/sync/syncadapter/o; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b(Ljava/lang/String;)V

    move v0, v1

    goto :goto_2

    .line 101
    :cond_1
    :try_start_3
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/i;

    const/4 v5, 0x0

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/i;-><init>(Ljava/util/Collection;B)V
    :try_end_3
    .catch Lcom/google/af/c/a/a; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/af/c/a/c; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/gms/drive/metadata/sync/syncadapter/o; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 109
    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/sync/b/h;->a(Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 111
    :catch_2
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/sync/b/h;->a(Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 113
    :catch_3
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/sync/b/h;->a(Ljava/lang/Exception;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 118
    :catch_4
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b(Ljava/lang/String;)V

    move v0, v1

    goto :goto_2

    .line 119
    :catch_5
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/sync/b/h;->a(Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b(Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->b(Ljava/lang/String;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 141
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "PreparedSyncMore[%s%d feeds, clipTime=%s, %s]"

    const/4 v0, 0x4

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/a;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    const-string v0, "running, "

    :goto_1
    aput-object v0, v5, v2

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v0, 0x2

    iget-wide v6, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/h;->c:Lcom/google/android/gms/drive/database/model/a;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    aput-object v1, v5, v0

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
