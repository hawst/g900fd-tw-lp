.class public final Lcom/google/android/gms/photos/autobackup/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 313
    new-instance v0, Lcom/google/android/gms/photos/autobackup/a/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/photos/autobackup/a/g;-><init>(Lcom/google/android/gms/photos/autobackup/a/b;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 331
    new-instance v0, Lcom/google/android/gms/photos/autobackup/a/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/a/i;-><init>(Lcom/google/android/gms/photos/autobackup/a/b;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 352
    new-instance v0, Lcom/google/android/gms/photos/autobackup/a/k;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/a/k;-><init>(Lcom/google/android/gms/photos/autobackup/a/b;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 373
    new-instance v0, Lcom/google/android/gms/photos/autobackup/a/m;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/photos/autobackup/a/m;-><init>(Lcom/google/android/gms/photos/autobackup/a/b;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 470
    new-instance v0, Lcom/google/android/gms/photos/autobackup/a/e;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/photos/autobackup/a/e;-><init>(Lcom/google/android/gms/photos/autobackup/a/b;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 390
    new-instance v0, Lcom/google/android/gms/photos/autobackup/a/o;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/a/o;-><init>(Lcom/google/android/gms/photos/autobackup/a/b;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 422
    new-instance v0, Lcom/google/android/gms/photos/autobackup/a/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/a/c;-><init>(Lcom/google/android/gms/photos/autobackup/a/b;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
