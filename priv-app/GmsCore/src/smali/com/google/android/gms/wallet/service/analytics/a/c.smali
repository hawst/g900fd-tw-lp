.class public Lcom/google/android/gms/wallet/service/analytics/a/c;
.super Lcom/google/android/gms/wallet/service/analytics/a/h;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/k/f/a/a/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>()V

    .line 30
    new-instance v0, Lcom/google/k/f/a/a/f;

    invoke-direct {v0}, Lcom/google/k/f/a/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/c;->a:Lcom/google/k/f/a/a/f;

    .line 31
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/wallet/common/ag;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    .line 35
    const-class v0, Lcom/google/k/f/a/a/f;

    new-instance v1, Lcom/google/k/f/a/a/f;

    invoke-direct {v1}, Lcom/google/k/f/a/a/f;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/k/f/a/a/f;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/c;->a:Lcom/google/k/f/a/a/f;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V
    .locals 3

    .prologue
    .line 58
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;

    if-eqz v0, :cond_0

    .line 59
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;

    .line 60
    iget-object v0, p2, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/c;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/content/Context;)V

    .line 61
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/c;->d:Ljava/lang/String;

    .line 69
    :goto_0
    return-void

    .line 62
    :cond_0
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;

    if-eqz v0, :cond_1

    .line 63
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/c;->a:Lcom/google/k/f/a/a/f;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->c:I

    iput v1, v0, Lcom/google/k/f/a/a/f;->a:I

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/c;->a:Lcom/google/k/f/a/a/f;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->d:I

    iput v1, v0, Lcom/google/k/f/a/a/f;->b:I

    goto :goto_0

    .line 67
    :cond_1
    const-string v0, "IaPaymentSessionState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to fill data for event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/wallet/common/ah;)V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Lcom/google/android/gms/wallet/common/ah;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/c;->a:Lcom/google/k/f/a/a/f;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;

    .line 43
    return-void
.end method

.method protected final a(Lcom/google/k/f/a/a/j;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/c;->a:Lcom/google/k/f/a/a/f;

    iput-object v0, p1, Lcom/google/k/f/a/a/j;->q:Lcom/google/k/f/a/a/f;

    .line 48
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/c;->a:Lcom/google/k/f/a/a/f;

    iget v0, v0, Lcom/google/k/f/a/a/f;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
