.class public final Lcom/google/android/gms/plus/service/a/ao;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

.field private final c:Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/ao;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 36
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/ao;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    .line 37
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/ao;->c:Lcom/google/android/gms/plus/internal/f;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ao;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/ao;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    iget-object v2, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/plus/b/n;->b(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ao;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "81"

    const-string v2, "SetAuthCategoryFacl"

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/people/pub/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ao;->c:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 60
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 51
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 52
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ao;->c:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v0, v4, v1}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 55
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ao;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/ao;->c:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v1, v4, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 58
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ao;->c:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v5}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ao;->c:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ao;->c:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;)V

    .line 67
    :cond_0
    return-void
.end method
