.class public final Lcom/google/android/gms/plus/service/v1/MomentsFeed;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v1/b;

.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final b:Ljava/util/Set;

.field private final c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/util/List;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/plus/service/v1/b;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->CREATOR:Lcom/google/android/gms/plus/service/v1/b;

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 79
    sput-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    const-string v1, "etag"

    const-string v2, "etag"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    const-string v1, "items"

    const-string v2, "items"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    const-string v1, "nextLink"

    const-string v2, "nextLink"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    const-string v1, "nextPageToken"

    const-string v2, "nextPageToken"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    const-string v1, "selfLink"

    const-string v2, "selfLink"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    const-string v1, "updated"

    const-string v2, "updated"

    const/16 v3, 0x9

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 150
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->c:I

    .line 151
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->b:Ljava/util/Set;

    .line 152
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 166
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->b:Ljava/util/Set;

    .line 167
    iput p2, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->c:I

    .line 168
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->d:Ljava/lang/String;

    .line 169
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->e:Ljava/util/List;

    .line 170
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->f:Ljava/lang/String;

    .line 171
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->g:Ljava/lang/String;

    .line 172
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->h:Ljava/lang/String;

    .line 173
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->i:Ljava/lang/String;

    .line 174
    iput-object p9, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->j:Ljava/lang/String;

    .line 175
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 399
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 400
    packed-switch v0, :pswitch_data_0

    .line 420
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 402
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->d:Ljava/lang/String;

    .line 423
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 424
    return-void

    .line 405
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->f:Ljava/lang/String;

    goto :goto_0

    .line 408
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->g:Ljava/lang/String;

    goto :goto_0

    .line 411
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->h:Ljava/lang/String;

    goto :goto_0

    .line 414
    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->i:Ljava/lang/String;

    goto :goto_0

    .line 417
    :pswitch_6
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->j:Ljava/lang/String;

    goto :goto_0

    .line 400
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 429
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 430
    packed-switch v0, :pswitch_data_0

    .line 435
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 432
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->e:Ljava/util/List;

    .line 439
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 440
    return-void

    .line 430
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 352
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 368
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->d:Ljava/lang/String;

    .line 366
    :goto_0
    return-object v0

    .line 356
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->e:Ljava/util/List;

    goto :goto_0

    .line 358
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->f:Ljava/lang/String;

    goto :goto_0

    .line 360
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->g:Ljava/lang/String;

    goto :goto_0

    .line 362
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->h:Ljava/lang/String;

    goto :goto_0

    .line 364
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->i:Ljava/lang/String;

    goto :goto_0

    .line 366
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->j:Ljava/lang/String;

    goto :goto_0

    .line 352
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    return-object v0
.end method

.method final b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->b:Ljava/util/Set;

    return-object v0
.end method

.method final c()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->c:I

    return v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->CREATOR:Lcom/google/android/gms/plus/service/v1/b;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->e:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 457
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v1/MomentsFeed;

    if-nez v0, :cond_0

    move v0, v1

    .line 488
    :goto_0
    return v0

    .line 462
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 463
    goto :goto_0

    .line 466
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v1/MomentsFeed;

    .line 467
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 468
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 469
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 471
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 473
    goto :goto_0

    :cond_3
    move v0, v1

    .line 478
    goto :goto_0

    .line 481
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 483
    goto :goto_0

    :cond_5
    move v0, v2

    .line 488
    goto :goto_0
.end method

.method final f()Ljava/util/List;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->e:Ljava/util/List;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 444
    const/4 v0, 0x0

    .line 445
    sget-object v1, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 446
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 447
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 448
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 450
    goto :goto_0

    .line 451
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 331
    sget-object v0, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->CREATOR:Lcom/google/android/gms/plus/service/v1/b;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/v1/b;->a(Lcom/google/android/gms/plus/service/v1/MomentsFeed;Landroid/os/Parcel;)V

    .line 332
    return-void
.end method
