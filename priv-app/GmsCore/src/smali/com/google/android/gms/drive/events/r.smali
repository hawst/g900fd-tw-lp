.class final Lcom/google/android/gms/drive/events/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private c:Landroid/os/IBinder;

.field private d:Landroid/content/ServiceConnection;

.field private e:Lcom/google/android/gms/drive/internal/cd;

.field private f:Z

.field private final g:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v2, p0, Lcom/google/android/gms/drive/events/r;->f:Z

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/events/r;->g:Ljava/util/List;

    .line 49
    iput-object p1, p0, Lcom/google/android/gms/drive/events/r;->a:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/drive/events/r;->b:Ljava/lang/String;

    .line 51
    const-string v0, "PackageEventRouter"

    const-string v1, "Starting new event router for %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/events/r;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 52
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/r;->c()V

    .line 53
    return-void
.end method

.method private declared-synchronized a(Landroid/os/IBinder;)V
    .locals 5

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    const-string v0, "PackageEventRouter"

    const-string v1, "Connected to event service for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/events/r;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :try_start_1
    iput-object p1, p0, Lcom/google/android/gms/drive/events/r;->c:Landroid/os/IBinder;

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->c:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    :goto_0
    :try_start_2
    invoke-static {p1}, Lcom/google/android/gms/drive/internal/ce;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/internal/cd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/events/r;->e:Lcom/google/android/gms/drive/internal/cd;

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/events/DriveEvent;

    .line 146
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/events/r;->b(Lcom/google/android/gms/drive/events/DriveEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    :try_start_3
    const-string v1, "PackageEventRouter"

    const-string v2, "Unable to link to event service death"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 149
    monitor-exit p0

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/events/r;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/r;->d()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/events/r;Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/events/r;->a(Landroid/os/IBinder;)V

    return-void
.end method

.method private declared-synchronized b(Lcom/google/android/gms/drive/events/DriveEvent;)V
    .locals 2

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->e:Lcom/google/android/gms/drive/internal/cd;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->e:Lcom/google/android/gms/drive/internal/cd;

    new-instance v1, Lcom/google/android/gms/drive/internal/OnEventResponse;

    invoke-direct {v1, p1}, Lcom/google/android/gms/drive/internal/OnEventResponse;-><init>(Lcom/google/android/gms/drive/events/DriveEvent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/internal/cd;->a(Lcom/google/android/gms/drive/internal/OnEventResponse;)V

    .line 181
    const-string v0, "PackageEventRouter"

    const-string v1, "Event sent OK."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/internal/be;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :goto_0
    monitor-exit p0

    return-void

    .line 183
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "PackageEventRouter"

    const-string v1, "Could not send event"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/internal/be;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Event that cant be sent: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/drive/internal/be;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 5

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    const-string v0, "PackageEventRouter"

    const-string v1, "Connecting event router to %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/events/r;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/drive/events/g;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 114
    new-instance v1, Lcom/google/android/gms/drive/events/s;

    invoke-direct {v1, p0, p0}, Lcom/google/android/gms/drive/events/s;-><init>(Lcom/google/android/gms/drive/events/r;Lcom/google/android/gms/drive/events/r;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/events/r;->d:Landroid/content/ServiceConnection;

    .line 125
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/events/r;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/drive/events/r;->d:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 157
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/gms/drive/events/r;->c:Landroid/os/IBinder;

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/events/r;->d:Landroid/content/ServiceConnection;

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/events/r;->e:Lcom/google/android/gms/drive/internal/cd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    monitor-exit p0

    return-void

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final declared-synchronized a(Lcom/google/android/gms/drive/events/DriveEvent;)V
    .locals 1

    .prologue
    .line 64
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/drive/events/r;->f:Z

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->e:Lcom/google/android/gms/drive/internal/cd;

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :goto_0
    monitor-exit p0

    return-void

    .line 68
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/events/r;->b(Lcom/google/android/gms/drive/events/DriveEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 81
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/gms/drive/events/r;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/drive/events/r;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    const-string v1, "PackageEventRouter"

    const-string v2, "Stopping idle event router for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/drive/events/r;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/drive/events/r;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :goto_0
    monitor-exit p0

    return v0

    .line 86
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/drive/events/r;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 87
    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b()V
    .locals 5

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    const-string v0, "PackageEventRouter"

    const-string v1, "Stopping event router for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/events/r;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->d:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 100
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/events/r;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/drive/events/r;->d:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :cond_0
    monitor-exit p0

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized binderDied()V
    .locals 2

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/events/r;->c:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 170
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/r;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    monitor-exit p0

    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
