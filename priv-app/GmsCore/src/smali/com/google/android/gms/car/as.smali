.class final Lcom/google/android/gms/car/as;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/hardware/display/VirtualDisplay;

.field private b:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Landroid/hardware/display/DisplayManager;Ljava/lang/String;IIILandroid/view/Surface;)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 1379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1380
    iput-object p6, p0, Lcom/google/android/gms/car/as;->b:Landroid/view/Surface;

    .line 1381
    const/16 v6, 0xa

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v6}, Landroid/hardware/display/DisplayManager;->createVirtualDisplay(Ljava/lang/String;IIILandroid/view/Surface;I)Landroid/hardware/display/VirtualDisplay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/as;->a:Landroid/hardware/display/VirtualDisplay;

    .line 1384
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 1412
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/as;->a:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->release()V

    .line 1413
    iget-object v0, p0, Lcom/google/android/gms/car/as;->b:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 1414
    iget-object v0, p0, Lcom/google/android/gms/car/as;->b:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1415
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/as;->b:Landroid/view/Surface;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1417
    :cond_0
    monitor-exit p0

    return-void

    .line 1412
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
