.class public Lcom/google/android/gms/photos/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/libraries/social/account/b;

.field private final c:Lcom/google/android/gms/clearcut/a;

.field private d:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/google/android/gms/photos/e;->a:Landroid/content/Context;

    .line 87
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/gms/photos/e;->b:Lcom/google/android/libraries/social/account/b;

    .line 88
    new-instance v0, Lcom/google/android/gms/clearcut/a;

    const/16 v1, 0x2a

    invoke-direct {v0, p1, v1, v2, v2}, Lcom/google/android/gms/clearcut/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/e;->c:Lcom/google/android/gms/clearcut/a;

    .line 92
    return-void
.end method

.method private a(Z)Lcom/google/android/gms/photos/a/a/g;
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 228
    new-instance v2, Lcom/google/android/gms/photos/a/a/g;

    invoke-direct {v2}, Lcom/google/android/gms/photos/a/a/g;-><init>()V

    .line 229
    new-instance v0, Lcom/google/android/gms/photos/a/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/photos/a/a/e;-><init>()V

    iput-object v0, v2, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    .line 230
    iget-object v0, v2, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    const/16 v3, 0x50

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/gms/photos/a/a/e;->b:Ljava/lang/Integer;

    .line 231
    iget-object v3, v2, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    iget-object v0, p0, Lcom/google/android/gms/photos/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/b/a/c;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/photos/a/a/e;->a:Ljava/lang/Integer;

    .line 232
    iget-object v0, v2, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/photos/a/a/e;->c:Ljava/lang/Integer;

    .line 233
    iget-object v1, v2, Lcom/google/android/gms/photos/a/a/g;->a:Lcom/google/android/gms/photos/a/a/e;

    if-eqz p1, :cond_1

    const/16 v0, 0x32

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/photos/a/a/e;->d:Ljava/lang/Integer;

    .line 235
    return-object v2

    :cond_0
    move v0, v1

    .line 231
    goto :goto_0

    .line 233
    :cond_1
    const/16 v0, 0x64

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/photos/e;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/photos/e;->b()V

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/google/android/gms/photos/a/a/g;)V
    .locals 2

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/e;->c:Lcom/google/android/gms/clearcut/a;

    new-instance v1, Lcom/google/android/gms/photos/h;

    invoke-direct {v1, p0, p2}, Lcom/google/android/gms/photos/h;-><init>(Lcom/google/android/gms/photos/e;Lcom/google/android/gms/photos/a/a/g;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/a;->a(Lcom/google/android/gms/clearcut/d;)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    iput-object p1, v0, Lcom/google/android/gms/clearcut/c;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/photos/e;->d:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    monitor-exit p0

    return-void

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/photos/e;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/photos/e;->d:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method private declared-synchronized b()V
    .locals 5

    .prologue
    .line 185
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/photos/e;->c()V

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/photos/e;->c:Lcom/google/android/gms/clearcut/a;

    iget-object v1, p0, Lcom/google/android/gms/photos/e;->d:Lcom/google/android/gms/common/api/v;

    const-wide/16 v2, 0x32

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/clearcut/a;->a(Lcom/google/android/gms/common/api/v;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    const-string v0, "PhotosClearcutTransmit"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "PhotosClearcutTransmit"

    const-string v1, "Failed flushing logger."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :cond_0
    monitor-exit p0

    return-void

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/String;Lcom/google/android/gms/photos/j;Ljava/util/List;Lcom/google/android/gms/photos/i;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 131
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/photos/e;->c()V

    .line 133
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gms/photos/e;->a(Z)Lcom/google/android/gms/photos/a/a/g;

    move-result-object v2

    .line 134
    new-instance v1, Lcom/google/android/gms/photos/a/a/b;

    invoke-direct {v1}, Lcom/google/android/gms/photos/a/a/b;-><init>()V

    iput-object v1, v2, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    .line 135
    iget-object v1, v2, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    new-instance v3, Lcom/google/android/gms/photos/a/a/c;

    invoke-direct {v3}, Lcom/google/android/gms/photos/a/a/c;-><init>()V

    iput-object v3, v1, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    .line 136
    iget-object v1, v2, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    iget-object v1, v1, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    invoke-static {p2}, Lcom/google/android/gms/photos/j;->a(Lcom/google/android/gms/photos/j;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/gms/photos/a/a/c;->a:Ljava/lang/Integer;

    .line 137
    if-eqz p4, :cond_0

    .line 138
    iget-object v1, v2, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    iget-object v1, v1, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    invoke-static {p4}, Lcom/google/android/gms/photos/i;->a(Lcom/google/android/gms/photos/i;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/gms/photos/a/a/c;->d:Ljava/lang/Integer;

    .line 140
    :cond_0
    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 141
    iget-object v1, v2, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    iget-object v1, v1, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [I

    iput-object v3, v1, Lcom/google/android/gms/photos/a/a/c;->c:[I

    move v1, v0

    .line 142
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 143
    iget-object v0, v2, Lcom/google/android/gms/photos/a/a/g;->b:Lcom/google/android/gms/photos/a/a/b;

    iget-object v0, v0, Lcom/google/android/gms/photos/a/a/b;->a:Lcom/google/android/gms/photos/a/a/c;

    iget-object v3, v0, Lcom/google/android/gms/photos/a/a/c;->c:[I

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/j;

    invoke-static {v0}, Lcom/google/android/gms/photos/j;->a(Lcom/google/android/gms/photos/j;)I

    move-result v0

    aput v0, v3, v1

    .line 142
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 146
    :cond_1
    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/photos/e;->a(Ljava/lang/String;Lcom/google/android/gms/photos/a/a/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    monitor-exit p0

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/e;->d:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 201
    new-instance v0, Lcom/google/android/gms/common/api/w;

    iget-object v1, p0, Lcom/google/android/gms/photos/e;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/clearcut/a;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/photos/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/g;-><init>(Lcom/google/android/gms/photos/e;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/photos/e;->d:Lcom/google/android/gms/common/api/v;

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/e;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/photos/e;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 217
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/photos/e;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/photos/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/f;-><init>(Lcom/google/android/gms/photos/e;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    monitor-exit p0

    return-void

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ILcom/google/android/gms/photos/a/a/a/c;)V
    .locals 3

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Lcom/google/android/libraries/b/a/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    invoke-direct {p0}, Lcom/google/android/gms/photos/e;->c()V

    .line 162
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/photos/e;->a(Z)Lcom/google/android/gms/photos/a/a/g;

    move-result-object v1

    .line 163
    new-instance v0, Lcom/google/android/gms/photos/a/a/a/d;

    invoke-direct {v0}, Lcom/google/android/gms/photos/a/a/a/d;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    .line 164
    iget-object v0, v1, Lcom/google/android/gms/photos/a/a/g;->c:Lcom/google/android/gms/photos/a/a/a/d;

    iput-object p2, v0, Lcom/google/android/gms/photos/a/a/a/d;->a:Lcom/google/android/gms/photos/a/a/a/c;

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/photos/e;->b:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/e;->b:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/photos/e;->a(Ljava/lang/String;Lcom/google/android/gms/photos/a/a/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    monitor-exit p0

    return-void

    .line 165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/google/android/gms/photos/j;)V
    .locals 2

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Lcom/google/android/libraries/b/a/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/photos/e;->b(Ljava/lang/String;Lcom/google/android/gms/photos/j;Ljava/util/List;Lcom/google/android/gms/photos/i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    monitor-exit p0

    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/google/android/gms/photos/j;Ljava/util/List;Lcom/google/android/gms/photos/i;)V
    .locals 1

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Lcom/google/android/libraries/b/a/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    invoke-static {p4}, Lcom/google/android/libraries/b/a/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/photos/e;->b(Ljava/lang/String;Lcom/google/android/gms/photos/j;Ljava/util/List;Lcom/google/android/gms/photos/i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    monitor-exit p0

    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
