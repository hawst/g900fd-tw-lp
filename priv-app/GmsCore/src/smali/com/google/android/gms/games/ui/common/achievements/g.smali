.class final Lcom/google/android/gms/games/ui/common/achievements/g;
.super Lcom/google/android/gms/games/ui/cx;
.source "SourceFile"


# instance fields
.field private final k:Landroid/widget/TextView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cx;-><init>(Landroid/view/View;)V

    .line 64
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->k:Landroid/widget/TextView;

    .line 65
    sget v0, Lcom/google/android/gms/j;->tB:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->n:Landroid/widget/TextView;

    .line 66
    sget v0, Lcom/google/android/gms/j;->pU:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->o:Landroid/widget/ProgressBar;

    .line 67
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/ac;I)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 71
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/cx;->a(Lcom/google/android/gms/games/ui/ac;I)V

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/achievements/f;

    .line 75
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 78
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->a(Lcom/google/android/gms/games/ui/common/achievements/f;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 80
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->k:Landroid/widget/TextView;

    const/high16 v4, 0x3fa00000    # 1.25f

    invoke-static {v1, v4}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/widget/TextView;F)V

    .line 82
    sget v1, Lcom/google/android/gms/p;->hC:I

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->b(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->c(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 86
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->k:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->c(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 95
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->n:Landroid/widget/TextView;

    sget v5, Lcom/google/android/gms/p;->hz:I

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v2

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->o:Landroid/widget/ProgressBar;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->c(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->o:Landroid/widget/ProgressBar;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->b(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 112
    sget v1, Lcom/google/android/gms/p;->hA:I

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->b(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->c(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 116
    return-void

    .line 92
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->b(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->c(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v4

    div-int/2addr v1, v4

    goto :goto_0

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->k:Landroid/widget/TextView;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v1, v4}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/widget/TextView;F)V

    .line 102
    sget v1, Lcom/google/android/gms/p;->hB:I

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->b(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/achievements/f;->c(Lcom/google/android/gms/games/ui/common/achievements/f;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 106
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/achievements/g;->n:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
