.class public Lcom/google/android/gms/people/service/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/util/p;

.field private final d:Lcom/google/android/gms/common/server/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/gms/people/service/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/service/m;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/n;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/util/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/service/m;->c:Lcom/google/android/gms/common/util/p;

    .line 48
    iput-object p2, p0, Lcom/google/android/gms/people/service/m;->d:Lcom/google/android/gms/common/server/n;

    .line 49
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/people/service/m;
    .locals 1

    .prologue
    .line 52
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->g()Lcom/google/android/gms/people/service/m;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Lcom/google/android/gms/people/service/m;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 61
    new-instance v0, Lcom/google/android/gms/common/server/n;

    sget-object v1, Lcom/google/android/gms/people/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 65
    new-instance v1, Lcom/google/android/gms/people/service/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/people/service/m;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/n;)V

    return-object v1
.end method

.method private c(Ljava/lang/String;)[B
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/people/service/m;->d:Lcom/google/android/gms/common/server/n;

    iget-object v1, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B

    move-result-object v0

    .line 139
    if-nez v0, :cond_1

    .line 140
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    const-string v1, "PeopleIS"

    const-string v2, "Unable to load image from the server."

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    :goto_0
    return-object v0

    .line 144
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/a/c;->a(I)V

    iget-object v1, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/people/a/c;->a(J)V

    goto :goto_0
.end method

.method private d(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 188
    new-instance v0, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    .line 190
    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bs;->b()Lcom/google/android/gms/common/internal/br;

    .line 191
    iget-object v1, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/f/h;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/bs;->a(I)Lcom/google/android/gms/common/internal/br;

    .line 193
    iput-boolean p2, v0, Lcom/google/android/gms/common/internal/bs;->d:Z

    .line 194
    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bs;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)[B
    .locals 4

    .prologue
    .line 99
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/m;->d:Lcom/google/android/gms/common/server/n;

    iget-object v1, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    const-string v1, "PeopleIS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error retrieving image at URL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 104
    sget-object v0, Lcom/google/android/gms/people/service/a/a/e;->j:[B

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)[B
    .locals 1

    .prologue
    .line 80
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/people/service/m;->b(Ljava/lang/String;Z)[B
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/people/service/a/a/e;->j:[B

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)[B
    .locals 4

    .prologue
    .line 174
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/service/m;->d(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 177
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/people/service/m;->c(Ljava/lang/String;)[B
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 183
    :goto_0
    return-object v0

    .line 178
    :catch_0
    move-exception v0

    .line 179
    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    sget-object v0, Lcom/google/android/gms/people/service/a/a/e;->i:[B

    goto :goto_0

    .line 182
    :cond_0
    const-string v1, "PeopleIS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error retrieving image at URL "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 183
    sget-object v0, Lcom/google/android/gms/people/service/a/a/e;->j:[B

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Z)[B
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v6, 0x3

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/google/android/gms/people/service/m;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    div-long/2addr v2, v10

    invoke-virtual {v0}, Lcom/google/android/gms/people/a/c;->b()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    .line 116
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "No request was sent, since we are currently backing off. A request at this time would likely fail."

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    const-string v0, "PeopleIS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requesting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/m;->c(Ljava/lang/String;)[B
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    .line 126
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 127
    invoke-static {v2}, Lcom/google/android/gms/people/f/h;->b(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v4

    iget-object v0, v4, Lcom/google/android/gms/people/a/c;->a:Landroid/content/SharedPreferences;

    const-string v1, "avatar_fetch_backoff_sec"

    const/4 v5, 0x0

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    sget-object v0, Lcom/google/android/gms/people/a/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v0, Lcom/google/android/gms/people/a/a;->F:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "PeopleIS"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "avatar backoff: lb="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " bos="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exp="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-lez v1, :cond_5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-eqz v5, :cond_5

    mul-int/lit8 v0, v0, 0x2

    move v1, v0

    :goto_1
    sget-object v0, Lcom/google/android/gms/people/a/a;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/people/a/c;->a(I)V

    if-lez v0, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/people/service/m;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    div-long/2addr v4, v10

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-virtual {v3}, Lcom/google/android/gms/people/a/c;->b()J

    move-result-wide v6

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/people/a/c;->a(J)V

    iget-object v1, p0, Lcom/google/android/gms/people/service/m;->b:Landroid/content/Context;

    const-string v3, "PeopleIS"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "avatar backoff="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " delay until="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v3, v0}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_3
    const-string v0, "PeopleIS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Error retrieving image at URL "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 133
    throw v2

    .line 129
    :cond_4
    if-eqz p2, :cond_3

    invoke-static {v2}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130
    sget-object v0, Lcom/google/android/gms/people/service/a/a/e;->i:[B

    goto/16 :goto_0

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;Z)[B
    .locals 2

    .prologue
    .line 157
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/people/service/m;->d(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 159
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/service/m;->a(Ljava/lang/String;Z)[B

    move-result-object v0

    return-object v0
.end method
