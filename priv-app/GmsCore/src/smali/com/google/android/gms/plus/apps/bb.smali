.class final Lcom/google/android/gms/plus/apps/bb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/apps/ba;

.field private final b:Ljava/lang/String;

.field private c:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/apps/ba;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p2, p0, Lcom/google/android/gms/plus/apps/bb;->b:Ljava/lang/String;

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/apps/bb;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bb;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/apps/bb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bb;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/ba;->a(Lcom/google/android/gms/plus/apps/ba;)Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->bv:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bb;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bp;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/ba;->c(Lcom/google/android/gms/plus/apps/ba;)Lcom/google/android/gms/common/server/n;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v2}, Lcom/google/android/gms/plus/apps/ba;->b(Lcom/google/android/gms/plus/apps/ba;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v0

    .line 103
    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/ba;->d(Lcom/google/android/gms/plus/apps/ba;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/ba;->d(Lcom/google/android/gms/plus/apps/ba;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 118
    :goto_0
    return-void

    .line 108
    :cond_0
    const/4 v1, 0x0

    array-length v2, v0

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v3}, Lcom/google/android/gms/plus/apps/ba;->e(Lcom/google/android/gms/plus/apps/ba;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bb;->c:Landroid/graphics/Bitmap;

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bb;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/ba;->d(Lcom/google/android/gms/plus/apps/ba;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/ba;->d(Lcom/google/android/gms/plus/apps/ba;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 116
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/ba;->d(Lcom/google/android/gms/plus/apps/ba;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/ba;->d(Lcom/google/android/gms/plus/apps/ba;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 114
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/ba;->d(Lcom/google/android/gms/plus/apps/ba;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bb;->a:Lcom/google/android/gms/plus/apps/ba;

    invoke-static {v1}, Lcom/google/android/gms/plus/apps/ba;->d(Lcom/google/android/gms/plus/apps/ba;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
