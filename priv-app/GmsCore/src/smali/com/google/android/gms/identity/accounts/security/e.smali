.class public final Lcom/google/android/gms/identity/accounts/security/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljavax/crypto/Mac;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/identity/accounts/security/e;->a:Ljavax/crypto/Mac;

    .line 25
    return-void
.end method

.method public constructor <init>(Ljavax/crypto/Mac;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/gms/identity/accounts/security/e;->a:Ljavax/crypto/Mac;

    .line 20
    return-void
.end method


# virtual methods
.method final a(Ljavax/crypto/SecretKey;)V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/e;->a:Ljavax/crypto/Mac;

    invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 32
    return-void
.end method

.method final a([B)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/e;->a:Ljavax/crypto/Mac;

    invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->update([B)V

    .line 39
    return-void
.end method

.method final b([B)[B
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/security/e;->a:Ljavax/crypto/Mac;

    invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    return-object v0
.end method
