.class public final Lcom/google/android/gms/drive/ui/picker/a/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/view/LayoutInflater;

.field final b:Landroid/view/View$OnClickListener;

.field final c:Landroid/database/DataSetObserver;

.field final d:I

.field e:Z

.field f:Z

.field g:Ljava/lang/String;

.field h:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/View$OnClickListener;ILandroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->e:Z

    .line 34
    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->f:Z

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->g:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->a:Landroid/view/LayoutInflater;

    .line 42
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->b:Landroid/view/View$OnClickListener;

    .line 43
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->c:Landroid/database/DataSetObserver;

    .line 44
    iput p3, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->d:I

    .line 45
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->h:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->h:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/google/android/gms/drive/ui/picker/a/m;->f:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 88
    :cond_0
    return-void
.end method
