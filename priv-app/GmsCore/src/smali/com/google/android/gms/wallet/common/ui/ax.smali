.class public final Lcom/google/android/gms/wallet/common/ui/ax;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/widget/ImageView;

.field b:Lcom/google/android/gms/wallet/common/ui/ay;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 32
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->c:I

    .line 37
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->d:I

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 110
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->c:I

    packed-switch v0, :pswitch_data_0

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->dE:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 112
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->dD:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/ax;->c:I

    .line 66
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ax;->a()V

    .line 67
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    :cond_0
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/ax;->d:I

    .line 79
    return-void
.end method

.method public final c(I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;I)V

    .line 91
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->b:Lcom/google/android/gms/wallet/common/ui/ay;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ax;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ax;->b:Lcom/google/android/gms/wallet/common/ui/ay;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->c:I

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ay;->a(I)Lcom/google/android/gms/wallet/common/ui/ay;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->b:Lcom/google/android/gms/wallet/common/ui/ay;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->b:Lcom/google/android/gms/wallet/common/ui/ay;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ax;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "CvcImageFragment.CvcInfo"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/ay;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 62
    :cond_1
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 42
    sget v0, Lcom/google/android/gms/l;->gz:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 43
    sget v0, Lcom/google/android/gms/j;->dW:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ax;->a()V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/ax;->d:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 47
    return-object v1
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ax;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "CvcImageFragment.CvcInfo"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ay;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ax;->b:Lcom/google/android/gms/wallet/common/ui/ay;

    .line 55
    return-void
.end method
