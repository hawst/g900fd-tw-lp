.class public final Lcom/google/android/gms/auth/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static A:Lcom/google/android/gms/common/a/d;

.field public static B:Lcom/google/android/gms/common/a/d;

.field public static C:Lcom/google/android/gms/common/a/d;

.field public static D:Lcom/google/android/gms/common/a/d;

.field public static E:Lcom/google/android/gms/common/a/d;

.field public static F:Lcom/google/android/gms/common/a/d;

.field public static G:Lcom/google/android/gms/common/a/d;

.field public static H:Lcom/google/android/gms/common/a/d;

.field public static I:Lcom/google/android/gms/common/a/d;

.field public static J:Lcom/google/android/gms/common/a/d;

.field public static K:Lcom/google/android/gms/common/a/d;

.field public static L:Lcom/google/android/gms/common/a/d;

.field public static M:Lcom/google/android/gms/common/a/d;

.field public static N:Lcom/google/android/gms/common/a/d;

.field public static O:Lcom/google/android/gms/common/a/d;

.field public static P:Lcom/google/android/gms/common/a/d;

.field public static Q:Lcom/google/android/gms/common/a/d;

.field public static R:Lcom/google/android/gms/common/a/d;

.field public static S:Lcom/google/android/gms/common/a/d;

.field public static T:Lcom/google/android/gms/common/a/d;

.field public static U:Lcom/google/android/gms/common/a/d;

.field public static V:Lcom/google/android/gms/common/a/d;

.field public static W:Lcom/google/android/gms/common/a/d;

.field public static X:Lcom/google/android/gms/common/a/d;

.field public static Y:Lcom/google/android/gms/common/a/d;

.field public static Z:Lcom/google/android/gms/common/a/d;

.field public static a:Lcom/google/android/gms/common/a/d;

.field public static aa:Lcom/google/android/gms/common/a/d;

.field public static ab:Lcom/google/android/gms/common/a/d;

.field public static ac:Lcom/google/android/gms/common/a/d;

.field public static ad:Lcom/google/android/gms/common/a/d;

.field public static ae:Lcom/google/android/gms/common/a/d;

.field public static af:Lcom/google/android/gms/common/a/d;

.field public static ag:Lcom/google/android/gms/common/a/d;

.field public static ah:Lcom/google/android/gms/common/a/d;

.field public static ai:Lcom/google/android/gms/common/a/d;

.field public static aj:Lcom/google/android/gms/common/a/d;

.field public static ak:Lcom/google/android/gms/common/a/d;

.field public static al:Lcom/google/android/gms/common/a/d;

.field public static am:Lcom/google/android/gms/common/a/d;

.field public static an:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;

.field public static l:Lcom/google/android/gms/common/a/d;

.field public static m:Lcom/google/android/gms/common/a/d;

.field public static n:Lcom/google/android/gms/common/a/d;

.field public static o:Lcom/google/android/gms/common/a/d;

.field public static p:Lcom/google/android/gms/common/a/d;

.field public static q:Lcom/google/android/gms/common/a/d;

.field public static r:Lcom/google/android/gms/common/a/d;

.field public static s:Lcom/google/android/gms/common/a/d;

.field public static t:Lcom/google/android/gms/common/a/d;

.field public static u:Lcom/google/android/gms/common/a/d;

.field public static v:Lcom/google/android/gms/common/a/d;

.field public static w:Lcom/google/android/gms/common/a/d;

.field public static x:Lcom/google/android/gms/common/a/d;

.field public static y:Lcom/google/android/gms/common/a/d;

.field public static z:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const-wide/32 v10, 0x36ee80

    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 19
    const-string v0, "auth_servlet_path"

    const-string v1, "https://android.clients.google.com/auth"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->a:Lcom/google/android/gms/common/a/d;

    .line 22
    const-string v0, "setup_servlet_path"

    const-string v1, "https://android.clients.google.com/setup"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->b:Lcom/google/android/gms/common/a/d;

    .line 25
    const-string v0, "checkname_servlet_path"

    const-string v1, "https://android.clients.google.com/setup/checkname"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->c:Lcom/google/android/gms/common/a/d;

    .line 28
    const-string v0, "create_profile_path"

    const-string v1, "https://android.clients.google.com/setup/createprofile"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->d:Lcom/google/android/gms/common/a/d;

    .line 31
    const-string v0, "auth_reauth_settings_url"

    const-string v1, "https://android.clients.google.com/auth/reauthsettings"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->e:Lcom/google/android/gms/common/a/d;

    .line 34
    const-string v0, "auth_verify_pin_url"

    const-string v1, "https://android.clients.google.com/auth/verifypin"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->f:Lcom/google/android/gms/common/a/d;

    .line 37
    const-string v0, "auth_exchange_bootstrap_credentials_url"

    const-string v1, "https://android.clients.google.com/auth/exchange_bootstrap_credentials"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->g:Lcom/google/android/gms/common/a/d;

    .line 41
    const-string v0, "auth_get_bootstrap_challenges_url"

    const-string v1, "https://android.clients.google.com/auth/get_bootstrap_challenges"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->h:Lcom/google/android/gms/common/a/d;

    .line 45
    const-string v0, "auth_factory_reset_protection_validation_url"

    const-string v1, "https://android.clients.google.com/auth/frp/validation"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->i:Lcom/google/android/gms/common/a/d;

    .line 50
    const-string v0, "oauth2_account_manager_client_id"

    const-string v1, "1070009224336-sdh77n7uot3oc99ais00jmuft6sk2fg9.apps.googleusercontent.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->j:Lcom/google/android/gms/common/a/d;

    .line 64
    const-string v0, "auth_channel_id_enabled_gms_core_version"

    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->k:Lcom/google/android/gms/common/a/d;

    .line 68
    const-string v0, "auth_channel_id_enabled_sdk_version"

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->l:Lcom/google/android/gms/common/a/d;

    .line 71
    const-string v0, "enable_auth_generic_account_recovery"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->m:Lcom/google/android/gms/common/a/d;

    .line 74
    const-string v0, "enable_auth_generic_login_challange"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->n:Lcom/google/android/gms/common/a/d;

    .line 77
    const-string v0, "enable_auth_generic_two_step_verification"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->o:Lcom/google/android/gms/common/a/d;

    .line 80
    const-string v0, "enable_auth_generic_reauth"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->p:Lcom/google/android/gms/common/a/d;

    .line 83
    const-string v0, "enable_auth_generic_unknown"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->q:Lcom/google/android/gms/common/a/d;

    .line 86
    const-string v0, "auth_enable_clearcut"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->r:Lcom/google/android/gms/common/a/d;

    .line 89
    const-string v0, "auth_get_token_sample_percentage"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->s:Lcom/google/android/gms/common/a/d;

    .line 92
    const-string v0, "auth_grant_credential_screen_sample_percentage"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->t:Lcom/google/android/gms/common/a/d;

    .line 95
    const-string v0, "auth_auth_zen_sample_percentage"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->u:Lcom/google/android/gms/common/a/d;

    .line 98
    const-string v0, "auth_auth_server_sample_percentage"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->v:Lcom/google/android/gms/common/a/d;

    .line 101
    const-string v0, "auth_trust_agent_sample_percentage"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->w:Lcom/google/android/gms/common/a/d;

    .line 104
    const-string v0, "auth_trust_agent_geofence_sample_percentage"

    const v1, 0x3a83126f    # 0.001f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->x:Lcom/google/android/gms/common/a/d;

    .line 107
    const-string v0, "auth_trust_agent_user_present_sample_percentage"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->y:Lcom/google/android/gms/common/a/d;

    .line 110
    const-string v0, "enable_droidguard"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->z:Lcom/google/android/gms/common/a/d;

    .line 113
    const-string v0, "auth_enable_droidguard_token_on_request_probability"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Double;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->A:Lcom/google/android/gms/common/a/d;

    .line 119
    const-string v0, "auth_is_proximity_features_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->B:Lcom/google/android/gms/common/a/d;

    .line 122
    const-string v0, "auth_supported_protocol_versions_for_proximity_unlock"

    const-string v1, "3"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->C:Lcom/google/android/gms/common/a/d;

    .line 125
    const-string v0, "auth_is_encryption_supported_for_protocol_v3"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->D:Lcom/google/android/gms/common/a/d;

    .line 128
    const-string v0, "auth_proximity_features_proactive_sync_period"

    const-wide v2, 0x9a7ec800L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->E:Lcom/google/android/gms/common/a/d;

    .line 132
    const-string v0, "auth_proximity_features_sync_on_authentication_failure_period"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->F:Lcom/google/android/gms/common/a/d;

    .line 137
    const-string v0, "auth_proximity_features_bluetooth_server_uuid"

    const-string v1, "704EE561-3782-405A-A14B-2D47A2DDCDDF"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->G:Lcom/google/android/gms/common/a/d;

    .line 145
    const-string v0, "GmsCoreAccountSetupWorkflow"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->H:Lcom/google/android/gms/common/a/d;

    .line 148
    const-string v0, "auth_enable_minutemaid_resize_hack"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->I:Lcom/google/android/gms/common/a/d;

    .line 152
    const-string v0, "auth_minutemaid_debuggable"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->J:Lcom/google/android/gms/common/a/d;

    .line 155
    const-string v0, "minutemaid_url_override"

    const-string v1, "https://accounts.google.com/EmbeddedSetup?"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->K:Lcom/google/android/gms/common/a/d;

    .line 159
    const-string v0, "auth_minute_maid_timeout"

    const-wide/16 v2, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->L:Lcom/google/android/gms/common/a/d;

    .line 162
    const-string v0, "auth_minute_maid_device_type"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->M:Lcom/google/android/gms/common/a/d;

    .line 189
    const-string v0, "auth_valid_minutemaid_url_pattern"

    const-string v1, "^https:\\/\\/[\\d\\w\\.]+\\.google\\.com\\/[\\d\\w\\/]+[\\/\\?]?"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->N:Lcom/google/android/gms/common/a/d;

    .line 197
    const-string v0, "auth_minutemaid_sms_capture_pattern"

    const-string v1, "(?:^|\\W)(?:MMOTP|G-)(\\d{4,})"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->O:Lcom/google/android/gms/common/a/d;

    .line 200
    const-string v0, "d2d_account_setup_enabled"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->P:Lcom/google/android/gms/common/a/d;

    .line 203
    const-string v0, "d2d_source_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->Q:Lcom/google/android/gms/common/a/d;

    .line 206
    const-string v0, "d2d_notification_uri_format"

    const-string v1, "https://support.google.com/mobile/?p=tap_and_go"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->R:Lcom/google/android/gms/common/a/d;

    .line 210
    const-string v0, "d2d_client_logs_enabled"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->S:Lcom/google/android/gms/common/a/d;

    .line 213
    const-string v0, "d2d_cancel_duration"

    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->T:Lcom/google/android/gms/common/a/d;

    .line 216
    const-string v0, "auth_email_check_on_token_request_interval_sec"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->U:Lcom/google/android/gms/common/a/d;

    .line 219
    const-string v0, "auth_coffee_is_place_trustlet_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->V:Lcom/google/android/gms/common/a/d;

    .line 222
    const-string v0, "auth_coffee_is_bluetooth_trustlet_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->W:Lcom/google/android/gms/common/a/d;

    .line 225
    const-string v0, "auth_coffee_is_nfc_trustlet_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->X:Lcom/google/android/gms/common/a/d;

    .line 228
    const-string v0, "auth_coffee_is_face_unlock_trustlet_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->Y:Lcom/google/android/gms/common/a/d;

    .line 231
    const-string v0, "auth_magic_is_phone_position_trustlet_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->Z:Lcom/google/android/gms/common/a/d;

    .line 234
    const-string v0, "auth_magic_off_person_duration_threshold_seconds"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->aa:Lcom/google/android/gms/common/a/d;

    .line 237
    const-string v0, "auth_cron_period_secs"

    const-wide/32 v2, 0x127500

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->ab:Lcom/google/android/gms/common/a/d;

    .line 240
    const-string v0, "auth_enable_factory_reset_protection"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->ac:Lcom/google/android/gms/common/a/d;

    .line 245
    const-string v0, "auth_is_factory_reset_protection_data_block_available"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->ad:Lcom/google/android/gms/common/a/d;

    .line 248
    const-string v0, "auth_coffee_fetch_home_work_address_interval_millis"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->ae:Lcom/google/android/gms/common/a/d;

    .line 252
    const-string v0, "authzen_enable"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->af:Lcom/google/android/gms/common/a/d;

    .line 256
    const-string v0, "authzen_ui_test_enable"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->ag:Lcom/google/android/gms/common/a/d;

    .line 260
    const-string v0, "authzen_cryptauth_server_url"

    const-string v1, "https://www.googleapis.com/"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->ah:Lcom/google/android/gms/common/a/d;

    .line 264
    const-string v0, "authzen_cryptauth_scope"

    const-string v1, "https://www.googleapis.com/auth/cryptauth"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->ai:Lcom/google/android/gms/common/a/d;

    .line 268
    const-string v0, "authzen_gcm_upstream_address"

    const-string v1, "authzen"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->aj:Lcom/google/android/gms/common/a/d;

    .line 273
    const-string v0, "authzen_encryption_key_validity_period_months"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->ak:Lcom/google/android/gms/common/a/d;

    .line 277
    const-string v0, "authzen_send_methods"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->al:Lcom/google/android/gms/common/a/d;

    .line 284
    const-string v0, "auth_reauth_check_package"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->am:Lcom/google/android/gms/common/a/d;

    .line 288
    const-string v0, "auth_reauth_sync_period"

    const-wide/32 v2, 0x89fd0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/b/a;->an:Lcom/google/android/gms/common/a/d;

    return-void
.end method
