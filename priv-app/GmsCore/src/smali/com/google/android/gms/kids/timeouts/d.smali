.class public final Lcom/google/android/gms/kids/timeouts/d;
.super Lcom/google/android/gms/kids/common/sync/d;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/kids/timeouts/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/gms/kids/timeouts/d;

    invoke-direct {v0}, Lcom/google/android/gms/kids/timeouts/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/kids/timeouts/d;->a:Lcom/google/android/gms/kids/timeouts/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/kids/common/sync/d;-><init>()V

    return-void
.end method

.method private a(Lcom/google/s/a/i;Landroid/content/ContentProviderClient;)V
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0x5
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 102
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->c()Lcom/google/android/gms/kids/common/c;

    invoke-static {}, Lcom/google/android/gms/kids/common/c;->a()J

    move-result-wide v2

    .line 103
    iget-object v4, p1, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    array-length v5, v4

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    .line 104
    invoke-static {v6}, Lcom/google/android/gms/kids/timeouts/a;->b(Lcom/google/s/a/z;)Landroid/content/ContentValues;

    move-result-object v6

    .line 105
    const-string v7, "update_time_stamp"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 106
    const-string v7, "is_deleted"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/gms/kids/timeouts/d;->d()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {p2, v7, v6}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 112
    const-string v4, "is_deleted"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/kids/timeouts/d;->d()Landroid/net/Uri;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "update_time_stamp<"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p2, v4, v0, v2, v3}, Landroid/content/ContentProviderClient;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 119
    const-string v0, "TimeoutsSyncInfoProvider"

    const-string v2, "Updating %d timeouts."

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/google/s/a/i;->a:[Lcom/google/s/a/z;

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    return-void
.end method

.method public static e()Lcom/google/android/gms/kids/timeouts/d;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/gms/kids/timeouts/d;->a:Lcom/google/android/gms/kids/timeouts/d;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.kids.timeouts.SYNCED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/ContentProviderClient;)V
    .locals 7

    .prologue
    .line 74
    new-instance v3, Lcom/google/s/a/h;

    invoke-direct {v3}, Lcom/google/s/a/h;-><init>()V

    .line 75
    const-string v0, "me"

    iput-object v0, v3, Lcom/google/s/a/h;->a:Ljava/lang/String;

    .line 76
    new-instance v0, Lcom/google/s/a/d;

    invoke-direct {v0}, Lcom/google/s/a/d;-><init>()V

    iput-object v0, v3, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    .line 79
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->a()Lcom/google/android/gms/kids/common/d;

    invoke-static {p2}, Lcom/google/android/gms/kids/common/d;->a(Landroid/accounts/Account;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 80
    new-instance v0, Lcom/google/android/gms/kids/common/a/a;

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->a()Lcom/google/android/gms/kids/common/d;

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->e()Lcom/google/android/gms/kids/a/a;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/kids/common/a/a;-><init>(Lcom/google/android/gms/common/server/q;)V

    .line 82
    iget-object v0, v0, Lcom/google/android/gms/kids/common/a/a;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/people/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/s/a/h;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/timeouts?alt=proto"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    if-eqz v5, :cond_0

    iget-object v5, v3, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    iget-object v5, v5, Lcom/google/s/a/d;->a:Ljava/lang/String;

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&apiHeader.consistencyTokenJar="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    iget-object v6, v6, Lcom/google/s/a/d;->a:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v5, v3, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    if-eqz v5, :cond_1

    iget-object v5, v3, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    iget-object v5, v5, Lcom/google/s/a/d;->b:Ljava/lang/String;

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&apiHeader.locale="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/s/a/h;->apiHeader:Lcom/google/s/a/d;

    iget-object v6, v6, Lcom/google/s/a/d;->b:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v5, v3, Lcom/google/s/a/h;->b:Ljava/lang/Boolean;

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&includeDisabled="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/s/a/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/google/s/a/i;

    invoke-direct {v5}, Lcom/google/s/a/i;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/s/a/i;

    .line 83
    iget-object v1, v0, Lcom/google/s/a/i;->b:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 84
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->c()Lcom/google/android/gms/kids/common/c;

    .line 85
    iget-object v1, v0, Lcom/google/s/a/i;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {}, Lcom/google/android/gms/kids/common/c;->c()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/kids/common/h;->a(J)V

    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, v0, Lcom/google/s/a/i;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/kids/common/h;->b(J)V

    .line 90
    :cond_3
    invoke-direct {p0, v0, p4}, Lcom/google/android/gms/kids/timeouts/d;->a(Lcom/google/s/a/i;Landroid/content/ContentProviderClient;)V

    .line 91
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 124
    const-string v0, "to"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string v0, "timeouts"

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/kids/timeouts/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/kids/timeouts/e;-><init>(Lcom/google/android/gms/kids/timeouts/d;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
