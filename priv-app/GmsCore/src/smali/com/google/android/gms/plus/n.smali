.class public final Lcom/google/android/gms/plus/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field public c:Landroid/content/Intent;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    iput-object p1, p0, Lcom/google/android/gms/plus/n;->d:Landroid/content/Context;

    .line 236
    iput-object p2, p0, Lcom/google/android/gms/plus/n;->e:Lcom/google/android/gms/common/server/ClientContext;

    .line 237
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/n;->a:Z

    .line 238
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/plus/n;->b:I

    .line 239
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 279
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 280
    iget-object v1, p0, Lcom/google/android/gms/plus/n;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/n;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->j()Ljava/lang/String;

    move-result-object v5

    .line 282
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/gms/plus/n;->a:Z

    if-eqz v1, :cond_2

    move-object v1, v7

    .line 286
    :goto_1
    iget v2, p0, Lcom/google/android/gms/plus/n;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/n;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->h()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/plus/internal/cq;->a([Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 290
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/gms/plus/n;->b:I

    .line 293
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/plus/n;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/n;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/gms/plus/n;->b:I

    iget-object v6, p0, Lcom/google/android/gms/plus/n;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v6}, Lcom/google/android/gms/common/server/ClientContext;->k()[Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/gms/plus/n;->d:Landroid/content/Context;

    iget-object v9, p0, Lcom/google/android/gms/plus/n;->c:Landroid/content/Intent;

    if-nez v9, :cond_3

    :goto_2
    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 303
    iget-object v1, p0, Lcom/google/android/gms/plus/n;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->l()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/os/Bundle;)V

    .line 305
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.plus.action.SIGN_UP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 306
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 307
    return-object v1

    :cond_1
    move-object v5, v7

    .line 280
    goto :goto_0

    .line 282
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/n;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 293
    :cond_3
    const/high16 v7, 0x8000000

    invoke-static {v8, v9, v7}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    goto :goto_2
.end method
