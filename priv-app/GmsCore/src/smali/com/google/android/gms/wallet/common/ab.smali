.class public final Lcom/google/android/gms/wallet/common/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/content/IntentFilter;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/wallet/common/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/wallet/common/ab;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/gms/wallet/common/ad;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ad;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ab;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ad;)V

    .line 65
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ad;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ab;->b:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ab;->c:Lcom/google/android/gms/wallet/common/ad;

    .line 71
    return-void
.end method

.method private static a(Lcom/google/android/gms/location/m;Lcom/google/android/gms/wallet/common/ac;)Lg/a/e;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 261
    .line 262
    invoke-virtual {p0}, Lcom/google/android/gms/location/m;->a()V

    .line 264
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/ac;->a()V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/gms/location/m;->c_()Z

    move-result v1

    if-nez v1, :cond_0

    .line 266
    const-string v1, "RiskAdvisoryDataProvider"

    const-string v2, "failed to connect to location client"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/gms/location/m;->b()V

    .line 281
    :goto_0
    return-object v0

    .line 269
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v1}, Lcom/google/android/gms/location/internal/w;->f()Landroid/location/Location;

    move-result-object v1

    .line 270
    if-eqz v1, :cond_1

    .line 271
    new-instance v0, Lg/a/e;

    invoke-direct {v0}, Lg/a/e;-><init>()V

    .line 272
    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iput-wide v2, v0, Lg/a/e;->b:D

    .line 273
    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iput-wide v2, v0, Lg/a/e;->c:D

    .line 274
    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    long-to-double v2, v2

    iput-wide v2, v0, Lg/a/e;->e:D

    .line 275
    invoke-virtual {v1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    iput-wide v2, v0, Lg/a/e;->a:D

    .line 276
    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    iput v1, v0, Lg/a/e;->d:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/m;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/location/m;->b()V

    throw v0
.end method

.method private a(Lg/a/d;)V
    .locals 4

    .prologue
    .line 287
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ab;->c:Lcom/google/android/gms/wallet/common/ad;

    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    .line 289
    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 290
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 291
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    .line 292
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 294
    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_1

    .line 295
    iget-object v3, p1, Lg/a/d;->i:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p1, Lg/a/d;->i:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 300
    :catch_0
    move-exception v0

    .line 301
    const-string v1, "RiskAdvisoryDataProvider"

    const-string v2, "Unable to retrieve network interfaces"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 303
    :cond_2
    return-void
.end method

.method private static a(Lg/a/d;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 228
    if-eqz p2, :cond_2

    .line 229
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, p2, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_2

    .line 232
    iget-object v1, p0, Lg/a/d;->a:[Lg/a/f;

    new-instance v2, Lg/a/f;

    invoke-direct {v2}, Lg/a/f;-><init>()V

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v3, v2, Lg/a/f;->a:Ljava/lang/String;

    :cond_0
    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lg/a/f;->b:Ljava/lang/String;

    iget-wide v4, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iput-wide v4, v2, Lg/a/f;->d:J

    iget-wide v4, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    iput-wide v4, v2, Lg/a/f;->c:J

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_1

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v0, v2, Lg/a/f;->e:Ljava/lang/String;

    :cond_1
    invoke-static {v1, v2}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lg/a/f;

    iput-object v0, p0, Lg/a/d;->a:[Lg/a/f;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :cond_2
    :goto_0
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    const-string v1, "RiskAdvisoryDataProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Package info not found for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    new-instance v3, Lcom/google/android/gms/wallet/common/ac;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    invoke-direct {v3, v0}, Lcom/google/android/gms/wallet/common/ac;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    new-instance v4, Lcom/google/android/gms/location/m;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ab;->b:Landroid/content/Context;

    invoke-direct {v4, v0, v3, v3}, Lcom/google/android/gms/location/m;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)V

    new-instance v5, Lg/a/b;

    invoke-direct {v5}, Lg/a/b;-><init>()V

    new-instance v6, Lg/a/c;

    invoke-direct {v6}, Lg/a/c;-><init>()V

    iput v2, v6, Lg/a/c;->a:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ab;->b:Landroid/content/Context;

    const-string v7, "phone"

    invoke-virtual {v0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    iput-object v0, v6, Lg/a/c;->e:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ab;->b:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v8

    iput-wide v8, v6, Lg/a/c;->f:J

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, v6, Lg/a/c;->g:Ljava/lang/String;

    :cond_2
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v0, v6, Lg/a/c;->h:Ljava/lang/String;

    :cond_3
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, v6, Lg/a/c;->i:Ljava/lang/String;

    :cond_4
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, v6, Lg/a/c;->j:Ljava/lang/String;

    :cond_5
    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v0, v6, Lg/a/c;->k:Ljava/lang/String;

    :cond_6
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, v6, Lg/a/c;->l:Ljava/lang/String;

    :cond_7
    iput-object v6, v5, Lg/a/b;->a:Lg/a/c;

    new-instance v6, Lg/a/d;

    invoke-direct {v6}, Lg/a/d;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ab;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/gms/wallet/common/ab;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v0, v7}, Lcom/google/android/gms/wallet/common/ab;->a(Lg/a/d;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    if-eqz p2, :cond_8

    invoke-static {v6, v0, p2}, Lcom/google/android/gms/wallet/common/ab;->a(Lg/a/d;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    :cond_8
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    iput-object v0, v6, Lg/a/d;->b:[Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/google/android/gms/wallet/common/ab;->a(Lcom/google/android/gms/location/m;Lcom/google/android/gms/wallet/common/ac;)Lg/a/e;

    move-result-object v0

    if-eqz v0, :cond_9

    iput-object v0, v6, Lg/a/d;->e:Lg/a/e;

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ab;->b:Landroid/content/Context;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/gms/wallet/common/ab;->a:Landroid/content/IntentFilter;

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_a

    const-string v3, "level"

    invoke-virtual {v0, v3, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "scale"

    invoke-virtual {v0, v4, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_a

    mul-int/lit8 v3, v3, 0x64

    div-int v0, v3, v0

    iput v0, v6, Lg/a/d;->c:I

    :cond_a
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    int-to-long v8, v0

    iput-wide v8, v6, Lg/a/d;->d:J

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ab;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-ge v0, v4, :cond_e

    const-string v0, "adb_enabled"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_c

    move v0, v1

    :goto_1
    iput-boolean v0, v6, Lg/a/d;->f:Z

    const-string v0, "install_non_market_apps"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_d

    :goto_2
    iput-boolean v1, v6, Lg/a/d;->g:Z

    :goto_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lg/a/d;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lg/a/d;->j:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/google/android/gms/wallet/common/ab;->a(Lg/a/d;)V

    iput-object v6, v5, Lg/a/b;->b:Lg/a/d;

    new-instance v0, Lg/a/a;

    invoke-direct {v0}, Lg/a/a;-><init>()V

    iput-object v5, v0, Lg/a/a;->a:Lg/a/b;

    new-instance v1, Lg/b/a;

    invoke-direct {v1}, Lg/b/a;-><init>()V

    iput-object v0, v1, Lg/b/a;->a:Lg/a/a;

    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    const/16 v9, 0x8

    if-gt v8, v9, :cond_b

    iput-object v7, v6, Lg/a/c;->d:Ljava/lang/String;

    goto/16 :goto_0

    :cond_b
    iput-object v7, v6, Lg/a/c;->c:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1
    iput-object v7, v6, Lg/a/c;->b:Ljava/lang/String;

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto :goto_1

    :cond_d
    move v1, v2

    goto :goto_2

    :cond_e
    const-string v0, "adb_enabled"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_f

    move v0, v1

    :goto_4
    iput-boolean v0, v6, Lg/a/d;->f:Z

    const-string v0, "install_non_market_apps"

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_10

    :goto_5
    iput-boolean v1, v6, Lg/a/d;->g:Z

    goto :goto_3

    :cond_f
    move v0, v2

    goto :goto_4

    :cond_10
    move v1, v2

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
