.class public Lcom/google/android/gms/kids/account/BootCompletedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 20
    const-string v0, "BootCompletedReceiver"

    const-string v1, "onReceive: action=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->c()Lcom/google/android/gms/kids/common/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/kids/common/c;->d()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/kids/common/h;->a(J)V

    .line 25
    new-instance v0, Lcom/google/android/gms/kids/timeouts/c;

    invoke-direct {v0}, Lcom/google/android/gms/kids/timeouts/c;-><init>()V

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/service/KidsApiService;->a(Landroid/content/Context;Lcom/google/android/gms/kids/common/c/b;)V

    .line 28
    new-instance v0, Lcom/google/android/gms/kids/account/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/kids/account/c;-><init>(Lcom/google/android/gms/kids/account/BootCompletedReceiver;)V

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/service/KidsApiService;->a(Landroid/content/Context;Lcom/google/android/gms/kids/common/c/b;)V

    .line 34
    return-void
.end method
