.class public final Lcom/google/android/gms/googlehelp/f/f;
.super Lcom/google/android/gms/googlehelp/f/a;
.source "SourceFile"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private final d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private final e:Lcom/google/android/gms/googlehelp/d/b;

.field private final f:Lcom/google/android/gms/googlehelp/common/k;

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/lang/String;Ljava/util/Calendar;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/f/a;-><init>(Lcom/google/android/gms/googlehelp/f/n;)V

    .line 87
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->b:Landroid/content/Context;

    .line 88
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/f/f;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d()Lcom/google/android/gms/googlehelp/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->e:Lcom/google/android/gms/googlehelp/d/b;

    .line 93
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    .line 94
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/f/f;->g:Ljava/lang/String;

    .line 95
    iput p4, p0, Lcom/google/android/gms/googlehelp/f/f;->h:I

    .line 96
    iput-object p5, p0, Lcom/google/android/gms/googlehelp/f/f;->i:Ljava/lang/String;

    .line 97
    iput-object p6, p0, Lcom/google/android/gms/googlehelp/f/f;->j:Ljava/util/Calendar;

    .line 98
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/util/Calendar;)V
    .locals 7

    .prologue
    .line 67
    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/f/f;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/lang/String;Ljava/util/Calendar;)V

    .line 68
    return-void
.end method

.method private a(Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/f;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/googlehelp/a/m;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/f/a;)V

    .line 103
    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 39
    check-cast p1, Landroid/util/Pair;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/googlehelp/common/k;

    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/f/f;->g:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/googlehelp/f/f;->h:I

    iget-object v6, p0, Lcom/google/android/gms/googlehelp/f/f;->i:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/common/k;Lcom/google/android/gms/googlehelp/common/k;ZLjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 39
    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->r()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/f/f;->a(Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/google/android/gms/googlehelp/common/k;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    move v3, v2

    :goto_0
    iget-object v5, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v5, v2}, Lcom/google/android/gms/googlehelp/common/k;->a(Z)V

    move v6, v3

    move-object v3, v4

    move v4, v6

    :goto_1
    if-eqz v3, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/f/f;->e:Lcom/google/android/gms/googlehelp/d/b;

    iget-object v5, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v5}, Lcom/google/android/gms/googlehelp/common/k;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/gms/googlehelp/d/b;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v3

    if-eqz v4, :cond_1

    if-nez v3, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/f/f;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v0, "GOOGLEHELP_GetLeafAnswerTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to read the latest leaf answer from database: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v4}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/common/k;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/f/f;->a(Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/f/f;->e:Lcom/google/android/gms/googlehelp/d/b;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/googlehelp/d/b;->a(Lcom/google/android/gms/googlehelp/common/k;)V

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/googlehelp/common/k;->d(Ljava/lang/String;)V

    move-object v3, v0

    move v0, v2

    :cond_1
    :goto_2
    if-eqz v3, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/f/f;->j:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/googlehelp/common/k;->a(J)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->g:Ljava/lang/String;

    const-string v4, "ARTICLE_HELP_LINK_CLICKED"

    if-ne v0, v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/k;->j()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/k;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/googlehelp/common/k;->c(Ljava/lang/String;)V

    :cond_2
    :goto_3
    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->e:Lcom/google/android/gms/googlehelp/d/b;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/d/b;->a(Lcom/google/android/gms/googlehelp/common/k;)V

    :cond_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->e:Lcom/google/android/gms/googlehelp/d/b;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/googlehelp/d/b;->a(Lcom/google/android/gms/googlehelp/common/k;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/f;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v4}, Lcom/google/android/gms/googlehelp/common/k;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/common/k;->d(Ljava/lang/String;)V

    move v0, v2

    move v3, v1

    goto/16 :goto_0

    :cond_5
    move v0, v1

    move-object v3, v4

    move v4, v1

    move v1, v2

    goto/16 :goto_1

    :cond_6
    move v1, v2

    move-object v3, v0

    move v0, v2

    goto :goto_2

    :cond_7
    move v2, v0

    goto :goto_3

    :cond_8
    move v0, v1

    move v4, v1

    goto/16 :goto_1
.end method
