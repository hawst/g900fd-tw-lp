.class final Lcom/google/android/gms/audiomodem/av;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z


# direct methods
.method static declared-synchronized a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 20
    const-class v1, Lcom/google/android/gms/audiomodem/av;

    monitor-enter v1

    :try_start_0
    sget-boolean v2, Lcom/google/android/gms/audiomodem/av;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 32
    :goto_0
    monitor-exit v1

    return v0

    .line 25
    :cond_0
    :try_start_1
    const-string v2, "Whisper"

    invoke-static {v2}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 26
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/gms/audiomodem/av;->a:Z
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 29
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "audioModem"

    const/4 v2, 0x6

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    const-string v0, "NativeLibrary: Unable to load libWhisper.so"

    const-string v2, "audioModem"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 32
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
