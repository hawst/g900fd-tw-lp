.class public final Lcom/google/android/gms/wearable/node/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/d/c;
.implements Lcom/google/android/gms/wearable/node/aa;
.implements Ljava/util/concurrent/Callable;


# static fields
.field public static final a:Lcom/google/android/gms/wearable/c/m;

.field private static final b:Lcom/google/android/gms/wearable/c/n;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private volatile f:Ljava/io/OutputStream;

.field private final g:Lcom/google/android/gms/wearable/c/i;

.field private final h:Lcom/google/android/gms/wearable/node/be;

.field private final i:Ljava/util/concurrent/locks/Lock;

.field private j:I

.field private final k:Lcom/google/android/gms/wearable/node/bi;

.field private final l:Landroid/util/SparseArray;

.field private final m:Landroid/util/SparseArray;

.field private final n:Landroid/util/SparseIntArray;

.field private volatile o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/gms/wearable/c/m;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/c/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/node/ab;->a:Lcom/google/android/gms/wearable/c/m;

    .line 42
    new-instance v0, Lcom/google/android/gms/wearable/c/m;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/c/m;-><init>()V

    .line 43
    new-instance v1, Lcom/google/android/gms/wearable/c/l;

    invoke-direct {v1}, Lcom/google/android/gms/wearable/c/l;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/wearable/c/m;->k:Lcom/google/android/gms/wearable/c/l;

    .line 44
    invoke-static {v0}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/c/m;)Lcom/google/android/gms/wearable/c/n;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/node/ab;->b:Lcom/google/android/gms/wearable/c/n;

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/c/i;Lcom/google/android/gms/wearable/node/be;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->i:Ljava/util/concurrent/locks/Lock;

    .line 60
    iput v1, p0, Lcom/google/android/gms/wearable/node/ab;->j:I

    .line 61
    invoke-static {}, Lcom/google/android/gms/wearable/node/bh;->a()Lcom/google/android/gms/wearable/node/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->k:Lcom/google/android/gms/wearable/node/bi;

    .line 63
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    .line 65
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    .line 67
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->n:Landroid/util/SparseIntArray;

    .line 72
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/ab;->c:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/google/android/gms/wearable/node/ab;->d:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    .line 75
    iput-object p4, p0, Lcom/google/android/gms/wearable/node/ab;->g:Lcom/google/android/gms/wearable/c/i;

    .line 76
    iput-object p5, p0, Lcom/google/android/gms/wearable/node/ab;->h:Lcom/google/android/gms/wearable/node/be;

    .line 77
    iput-boolean v1, p0, Lcom/google/android/gms/wearable/node/ab;->o:Z

    .line 78
    const/16 v0, 0x3000

    iput v0, p0, Lcom/google/android/gms/wearable/node/ab;->e:I

    .line 79
    return-void
.end method

.method private a(I)I
    .locals 6

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 202
    if-nez v0, :cond_0

    move v0, v2

    .line 229
    :goto_0
    return v0

    .line 207
    :cond_0
    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/node/aq;

    .line 210
    if-nez v1, :cond_1

    move v0, v2

    .line 211
    goto :goto_0

    .line 214
    :cond_1
    invoke-interface {v1}, Lcom/google/android/gms/wearable/node/aq;->a()Lcom/google/android/gms/wearable/c/m;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/wearable/node/ab;->a:Lcom/google/android/gms/wearable/c/m;

    if-ne v2, v4, :cond_2

    .line 215
    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 216
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->n:Landroid/util/SparseIntArray;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->delete(I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v3

    .line 217
    goto :goto_0

    .line 216
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 220
    :cond_2
    invoke-interface {v1}, Lcom/google/android/gms/wearable/node/aq;->c()Lcom/google/android/gms/wearable/c/n;

    move-result-object v2

    iput-boolean v3, p0, Lcom/google/android/gms/wearable/node/ab;->o:Z

    iget-object v3, p0, Lcom/google/android/gms/wearable/node/ab;->k:Lcom/google/android/gms/wearable/node/bi;

    iget-object v4, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    iget-object v5, p0, Lcom/google/android/gms/wearable/node/ab;->h:Lcom/google/android/gms/wearable/node/be;

    invoke-static {v3, v4, v2, v5}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/node/bi;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/c/n;Lcom/google/android/gms/wearable/node/be;)I

    move-result v2

    .line 225
    invoke-interface {v1}, Lcom/google/android/gms/wearable/node/aq;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 226
    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    :cond_3
    move v0, v2

    .line 229
    goto :goto_0
.end method

.method private static a(Landroid/util/SparseArray;Lcom/google/android/gms/common/util/ad;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 378
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 379
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_2

    .line 380
    invoke-virtual {p0, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 381
    invoke-virtual {p0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 382
    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 383
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ": size="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 384
    if-eqz p2, :cond_1

    .line 385
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 386
    new-array v1, v2, [Lcom/google/android/gms/wearable/node/aq;

    invoke-interface {v0, v1}, Ljava/util/Queue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/wearable/node/aq;

    .line 387
    array-length v5, v0

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_0

    aget-object v6, v0, v1

    .line 388
    invoke-interface {v6}, Lcom/google/android/gms/wearable/node/aq;->a()Lcom/google/android/gms/wearable/c/m;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/wearable/node/bb;->a(Lcom/google/android/gms/wearable/c/m;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 387
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 390
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 379
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 394
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 395
    return-void
.end method

.method private c()Ljava/lang/Void;
    .locals 14

    .prologue
    .line 84
    const/16 v0, 0xa

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 85
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/ax;

    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MultiQueueWriterCallable["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/ab;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/ax;->setName(Ljava/lang/String;)V

    .line 87
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-wide v2, v4

    .line 88
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/ax;->a()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    if-nez v1, :cond_10

    .line 90
    :try_start_1
    const-string v1, "wearable"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "WearableVerbose"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    :cond_0
    const-string v1, "wearable"

    const-string v4, "waiting for change"

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/ab;->e()I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-wide v4, v2

    .line 105
    :goto_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    monitor-enter v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 106
    const/4 v1, 0x0

    :try_start_3
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    :goto_2
    if-ge v1, v3, :cond_2

    .line 107
    iget-object v6, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 108
    iget-object v7, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    iget-object v8, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-virtual {v8, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v6, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 106
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    .line 190
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    .line 191
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/ab;->d()V

    .line 193
    :try_start_4
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 197
    :goto_3
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 110
    :cond_2
    const/4 v1, 0x0

    :try_start_5
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/ab;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v3}, Landroid/util/SparseIntArray;->size()I

    move-result v3

    :goto_5
    if-ge v1, v3, :cond_3

    .line 111
    iget-object v6, p0, Lcom/google/android/gms/wearable/node/ab;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v6

    .line 112
    iget-object v7, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    invoke-virtual {v7, v6}, Landroid/util/SparseArray;->remove(I)V

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 114
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 115
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    .line 116
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 118
    :try_start_6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 119
    const/4 v3, 0x0

    .line 120
    const/4 v2, 0x0

    .line 124
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/google/android/gms/wearable/node/ab;->a(I)I

    move-result v1

    .line 125
    if-lez v1, :cond_8

    .line 126
    add-int/lit8 v2, v1, 0x0

    .line 127
    const/4 v1, 0x1

    move v3, v2

    .line 138
    :goto_6
    if-nez v1, :cond_a

    .line 139
    const-string v1, "wearable"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "WearableVerbose"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 141
    :cond_4
    const-string v1, "wearable"

    const-string v2, "no message found"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/wearable/node/ab;->o:Z

    if-eqz v1, :cond_11

    .line 173
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/wearable/node/ab;->o:Z

    .line 174
    const-string v1, "wearable"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "WearableVerbose"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 176
    :cond_6
    const-string v1, "wearable"

    const-string v2, "sending heartbeat"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->k:Lcom/google/android/gms/wearable/node/bi;

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    sget-object v3, Lcom/google/android/gms/wearable/node/ab;->b:Lcom/google/android/gms/wearable/c/n;

    iget-object v6, p0, Lcom/google/android/gms/wearable/node/ab;->h:Lcom/google/android/gms/wearable/node/be;

    invoke-static {v1, v2, v3, v6}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/node/bi;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/c/n;Lcom/google/android/gms/wearable/node/be;)I

    move-wide v2, v4

    goto/16 :goto_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 184
    :catch_1
    move-exception v0

    .line 186
    :try_start_7
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->h:Lcom/google/android/gms/wearable/node/be;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "writer threw IOException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wearable/node/be;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    .line 190
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    .line 191
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/ab;->d()V

    .line 193
    :try_start_8
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 197
    :goto_7
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 129
    :cond_8
    const/4 v1, 0x0

    :try_start_9
    iget-object v6, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v6

    move v13, v1

    move v1, v2

    move v2, v3

    move v3, v13

    :goto_8
    if-ge v3, v6, :cond_12

    .line 130
    iget-object v7, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    invoke-virtual {v7, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v7

    .line 131
    invoke-direct {p0, v7}, Lcom/google/android/gms/wearable/node/ab;->a(I)I

    move-result v7

    .line 132
    if-ltz v7, :cond_9

    .line 133
    add-int/2addr v2, v7

    .line 134
    add-int/lit8 v1, v1, 0x1

    .line 129
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 145
    :cond_a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 147
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 148
    const-string v1, "wearable"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "WearableVerbose"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 150
    :cond_b
    if-lez v3, :cond_e

    .line 151
    sub-long v8, v10, v8

    .line 152
    sub-long v4, v6, v4

    .line 153
    const-wide/16 v10, 0x1

    cmp-long v1, v8, v10

    if-ltz v1, :cond_c

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-long v10, v3

    div-long/2addr v10, v8

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " KBps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 157
    :goto_9
    const-wide/16 v10, 0x1

    cmp-long v1, v4, v10

    if-ltz v1, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v10, ", total "

    invoke-direct {v1, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-long v10, v3

    div-long/2addr v10, v4

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, " KBps"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 161
    :goto_a
    iget-object v10, p0, Lcom/google/android/gms/wearable/node/ab;->c:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "wrote data: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms, total time "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v10, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v4, v6

    .line 164
    goto/16 :goto_1

    .line 153
    :cond_c
    const-string v1, ""

    move-object v2, v1

    goto :goto_9

    .line 157
    :cond_d
    const-string v1, ""

    goto :goto_a

    .line 165
    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->c:Ljava/lang/String;

    const-string v2, "0 byte sent"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    move-wide v4, v6

    .line 168
    goto/16 :goto_1

    .line 182
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->h:Lcom/google/android/gms/wearable/node/be;

    const-string v1, "writer was stopped"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/be;->a(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    .line 190
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    .line 191
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/ab;->d()V

    .line 193
    :try_start_a
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 197
    :goto_b
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 189
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    .line 190
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    .line 191
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/ab;->d()V

    .line 193
    :try_start_b
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    .line 197
    :goto_c
    throw v0

    :catch_2
    move-exception v0

    goto/16 :goto_3

    :catch_3
    move-exception v0

    goto :goto_b

    :catch_4
    move-exception v0

    goto/16 :goto_7

    :catch_5
    move-exception v1

    goto :goto_c

    :cond_11
    move-wide v2, v4

    goto/16 :goto_0

    :cond_12
    move v3, v2

    goto/16 :goto_6
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 299
    .line 300
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v4, :cond_1

    .line 301
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 302
    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/node/aq;

    .line 303
    :goto_1
    if-eqz v1, :cond_0

    .line 304
    add-int/lit8 v2, v2, 0x1

    .line 305
    invoke-interface {v1}, Lcom/google/android/gms/wearable/node/aq;->d()V

    .line 306
    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/node/aq;

    goto :goto_1

    .line 300
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->c:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "purged "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " messages from writer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :cond_2
    return-void
.end method

.method private e()I
    .locals 4

    .prologue
    .line 325
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->i:Ljava/util/concurrent/locks/Lock;

    monitor-enter v1

    .line 326
    :goto_0
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/wearable/node/ab;->j:I

    if-nez v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->i:Ljava/util/concurrent/locks/Lock;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 330
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/wearable/node/ab;->j:I

    .line 331
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/gms/wearable/node/ab;->j:I

    .line 332
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 337
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->i:Ljava/util/concurrent/locks/Lock;

    monitor-enter v1

    .line 338
    const/4 v0, 0x1

    :try_start_0
    iput v0, p0, Lcom/google/android/gms/wearable/node/ab;->j:I

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->i:Ljava/util/concurrent/locks/Lock;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 340
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wearable/c/i;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->g:Lcom/google/android/gms/wearable/c/i;

    return-object v0
.end method

.method public final a(ILcom/google/android/gms/wearable/c/m;Lcom/google/android/gms/wearable/node/y;)V
    .locals 4

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->f:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    .line 236
    new-instance v0, Ljava/io/IOException;

    const-string v1, "writer is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_0
    const-string v0, "message was null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->g:Lcom/google/android/gms/wearable/c/i;

    iget v0, v0, Lcom/google/android/gms/wearable/c/i;->d:I

    iget v1, p0, Lcom/google/android/gms/wearable/node/ab;->e:I

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/wearable/node/ar;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/gms/wearable/node/ar;-><init>(ILcom/google/android/gms/wearable/c/m;Lcom/google/android/gms/wearable/node/y;I)V

    move-object v1, v0

    .line 241
    :goto_0
    invoke-interface {v1}, Lcom/google/android/gms/wearable/node/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    const-string v0, "wearable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MultiQueueWriterCallable: dropping message from queue because the target node cannot read it: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :goto_1
    return-void

    .line 239
    :cond_1
    new-instance v0, Lcom/google/android/gms/wearable/node/as;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/gms/wearable/node/as;-><init>(ILcom/google/android/gms/wearable/c/m;Lcom/google/android/gms/wearable/node/y;I)V

    move-object v1, v0

    goto :goto_0

    .line 254
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    monitor-enter v2

    .line 257
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 258
    if-nez v0, :cond_4

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 262
    if-nez v0, :cond_4

    .line 266
    const/4 v0, 0x4

    if-eq p1, v0, :cond_3

    const/16 v0, 0x8

    if-ne p1, v0, :cond_5

    .line 268
    :cond_3
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v3, 0xa

    invoke-direct {v0, v3}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    .line 272
    :goto_2
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 274
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/ab;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseIntArray;->delete(I)V

    .line 277
    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    instance-of v2, v0, Ljava/util/concurrent/LinkedBlockingQueue;

    if-eqz v2, :cond_6

    .line 282
    check-cast v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V

    .line 286
    :goto_3
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/ab;->f()V

    goto :goto_1

    .line 270
    :cond_5
    :try_start_1
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 284
    :cond_6
    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/common/util/ad;ZZ)V
    .locals 4

    .prologue
    .line 351
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    monitor-enter v1

    .line 352
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "message queues: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->l:Landroid/util/SparseArray;

    invoke-static {v0, p1, p3}, Lcom/google/android/gms/wearable/node/ab;->a(Landroid/util/SparseArray;Lcom/google/android/gms/common/util/ad;Z)V

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 355
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "pending message queues: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->m:Landroid/util/SparseArray;

    invoke-static {v0, p1, p3}, Lcom/google/android/gms/wearable/node/ab;->a(Landroid/util/SparseArray;Lcom/google/android/gms/common/util/ad;Z)V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/ab;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    .line 359
    if-lez v0, :cond_1

    .line 360
    const-string v2, "queues to remove: "

    invoke-virtual {p1, v2}, Lcom/google/android/gms/common/util/ad;->print(Ljava/lang/String;)V

    .line 361
    if-nez v0, :cond_2

    .line 362
    const-string v0, "none"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 373
    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    .line 364
    :cond_2
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/ab;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    :goto_1
    if-ge v0, v2, :cond_4

    .line 365
    if-lez v0, :cond_3

    .line 366
    const-string v3, ", "

    invoke-virtual {p1, v3}, Lcom/google/android/gms/common/util/ad;->print(Ljava/lang/String;)V

    .line 368
    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/ab;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/gms/common/util/ad;->print(I)V

    .line 364
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 370
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->println()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 406
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/ab;->o:Z

    .line 407
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/ab;->f()V

    .line 408
    return-void
.end method

.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/ab;->c()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
