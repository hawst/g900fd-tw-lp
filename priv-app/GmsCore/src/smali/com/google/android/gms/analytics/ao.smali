.class final Lcom/google/android/gms/analytics/ao;
.super Ljava/lang/Thread;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/analytics/i;


# static fields
.field private static e:Lcom/google/android/gms/analytics/ao;


# instance fields
.field private final a:Ljava/util/concurrent/LinkedBlockingQueue;

.field private volatile b:Z

.field private volatile c:Z

.field private volatile d:Ljava/lang/String;

.field private volatile f:Lcom/google/android/gms/analytics/bt;

.field private final g:Landroid/content/Context;

.field private final h:Ljava/util/concurrent/locks/Lock;

.field private final i:Ljava/util/List;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 69
    const-string v0, "GAThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 46
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/ao;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 49
    iput-boolean v1, p0, Lcom/google/android/gms/analytics/ao;->b:Z

    .line 50
    iput-boolean v1, p0, Lcom/google/android/gms/analytics/ao;->c:Z

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/ao;->i:Ljava/util/List;

    .line 70
    if-eqz p1, :cond_0

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/ao;->g:Landroid/content/Context;

    .line 77
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->i:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/analytics/internal/Command;

    const-string v2, "appendVersion"

    const-string v3, "&_v"

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ma4.0.5"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/analytics/internal/Command;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/ao;->h:Ljava/util/concurrent/locks/Lock;

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/ao;->start()V

    .line 83
    return-void

    .line 73
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/analytics/ao;->g:Landroid/content/Context;

    goto :goto_0
.end method

.method static a(Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 159
    const/4 v0, 0x1

    .line 160
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 166
    const/4 v1, 0x0

    .line 167
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    move v0, v1

    move v1, v4

    :goto_0
    if-ltz v1, :cond_1

    .line 168
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 169
    shl-int/lit8 v0, v0, 0x6

    const v3, 0xfffffff

    and-int/2addr v0, v3

    add-int/2addr v0, v2

    shl-int/lit8 v2, v2, 0xe

    add-int/2addr v0, v2

    .line 170
    const/high16 v2, 0xfe00000

    and-int/2addr v2, v0

    .line 171
    if-eqz v2, :cond_0

    shr-int/lit8 v2, v2, 0x15

    xor-int/2addr v0, v2

    .line 167
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 175
    :cond_1
    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/analytics/ao;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->g:Landroid/content/Context;

    return-object v0
.end method

.method static a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ao;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/gms/analytics/ao;->e:Lcom/google/android/gms/analytics/ao;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/google/android/gms/analytics/ao;

    invoke-direct {v0, p0}, Lcom/google/android/gms/analytics/ao;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/analytics/ao;->e:Lcom/google/android/gms/analytics/ao;

    .line 64
    :cond_0
    sget-object v0, Lcom/google/android/gms/analytics/ao;->e:Lcom/google/android/gms/analytics/ao;

    return-object v0
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 140
    new-instance v1, Ljava/io/PrintStream;

    invoke-direct {v1, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    .line 141
    invoke-virtual {p0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 142
    invoke-virtual {v1}, Ljava/io/PrintStream;->flush()V

    .line 143
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 136
    return-void
.end method

.method private static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 187
    :try_start_0
    const-string v1, "gaInstallData"

    invoke-virtual {p0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    .line 192
    const/16 v2, 0x2000

    new-array v2, v2, [B

    .line 193
    const/4 v3, 0x0

    const/16 v4, 0x2000

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/FileInputStream;->read([BII)I

    move-result v3

    .line 194
    invoke-virtual {v1}, Ljava/io/FileInputStream;->available()I

    move-result v4

    if-lez v4, :cond_0

    .line 197
    const-string v2, "Too much campaign data, ignoring it."

    invoke-static {v2}, Lcom/google/android/gms/analytics/bl;->a(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 199
    const-string v1, "gaInstallData"

    invoke-virtual {p0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 221
    :goto_0
    return-object v0

    .line 202
    :cond_0
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 203
    const-string v1, "gaInstallData"

    invoke-virtual {p0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 204
    if-gtz v3, :cond_1

    .line 205
    const-string v1, "Campaign file is empty."

    invoke-static {v1}, Lcom/google/android/gms/analytics/bl;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 214
    :catch_0
    move-exception v1

    const-string v1, "No campaign data found."

    invoke-static {v1}, Lcom/google/android/gms/analytics/bl;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_1
    :try_start_1
    new-instance v1, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3}, Ljava/lang/String;-><init>([BII)V

    .line 209
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Campaign found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/analytics/bl;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 210
    goto :goto_0

    .line 219
    :catch_1
    move-exception v1

    const-string v1, "Error reading campaign data."

    invoke-static {v1}, Lcom/google/android/gms/analytics/bl;->a(Ljava/lang/String;)V

    .line 220
    const-string v1, "gaInstallData"

    invoke-virtual {p0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/analytics/ao;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/analytics/ao;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/analytics/ao;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/analytics/ao;)Ljava/util/List;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/analytics/ao;)Lcom/google/android/gms/analytics/bt;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->f:Lcom/google/android/gms/analytics/bt;

    return-object v0
.end method

.method private declared-synchronized f()V
    .locals 2

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->f:Lcom/google/android/gms/analytics/bt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 112
    :goto_0
    monitor-exit p0

    return-void

    .line 110
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/analytics/ag;

    iget-object v1, p0, Lcom/google/android/gms/analytics/ao;->g:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/analytics/ag;-><init>(Landroid/content/Context;Lcom/google/android/gms/analytics/i;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/ao;->f:Lcom/google/android/gms/analytics/bt;

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->f:Lcom/google/android/gms/analytics/bt;

    invoke-interface {v0}, Lcom/google/android/gms/analytics/bt;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/gms/analytics/ap;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/analytics/ap;-><init>(Lcom/google/android/gms/analytics/ao;B)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/ao;->a(Ljava/lang/Runnable;)V

    .line 122
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/google/android/gms/analytics/ar;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/analytics/ar;-><init>(Lcom/google/android/gms/analytics/ao;Ljava/util/Map;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/ao;->a(Ljava/lang/Runnable;)V

    .line 117
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 131
    new-instance v0, Lcom/google/android/gms/analytics/aq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/analytics/aq;-><init>(Lcom/google/android/gms/analytics/ao;B)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/ao;->a(Ljava/lang/Runnable;)V

    .line 132
    return-void
.end method

.method public final c()Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method

.method public final d()Ljava/lang/Thread;
    .locals 0

    .prologue
    .line 293
    return-object p0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 316
    invoke-direct {p0}, Lcom/google/android/gms/analytics/ao;->f()V

    .line 322
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 323
    iget-object v1, p0, Lcom/google/android/gms/analytics/ao;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->drainTo(Ljava/util/Collection;)I

    .line 328
    iget-object v1, p0, Lcom/google/android/gms/analytics/ao;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 330
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/gms/analytics/ao;->b:Z

    .line 331
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333
    :try_start_1
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 334
    :catch_0
    move-exception v0

    .line 335
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error dispatching all events on exit, giving up: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/analytics/ao;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 341
    return-void

    .line 340
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/analytics/ao;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 227
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 236
    const-wide/16 v0, 0x1388

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 245
    :goto_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/analytics/ao;->f()V

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/analytics/ao;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/ao;->d:Ljava/lang/String;

    .line 248
    const-string v0, "Initialized GA Thread"

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3

    .line 256
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ao;->c:Z

    if-nez v0, :cond_1

    .line 262
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->a:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 265
    iget-object v1, p0, Lcom/google/android/gms/analytics/ao;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 267
    :try_start_3
    iget-boolean v1, p0, Lcom/google/android/gms/analytics/ao;->b:Z

    if-nez v1, :cond_0

    .line 268
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 271
    :cond_0
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/analytics/ao;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 275
    :catch_0
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->b(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 276
    :catch_1
    move-exception v0

    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error on GAThread: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/analytics/ao;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->a(Ljava/lang/String;)V

    .line 280
    const-string v0, "Google Analytics is shutting down."

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->a(Ljava/lang/String;)V

    .line 281
    iput-boolean v3, p0, Lcom/google/android/gms/analytics/ao;->b:Z

    goto :goto_1

    .line 239
    :catch_2
    move-exception v0

    const-string v0, "sleep interrupted in GAThread initialize"

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :catch_3
    move-exception v0

    .line 250
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error initializing the GAThread: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/analytics/ao;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->a(Ljava/lang/String;)V

    .line 253
    const-string v0, "Google Analytics will not start up."

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->a(Ljava/lang/String;)V

    .line 254
    iput-boolean v3, p0, Lcom/google/android/gms/analytics/ao;->b:Z

    goto :goto_1

    .line 271
    :catchall_0
    move-exception v0

    :try_start_6
    iget-object v1, p0, Lcom/google/android/gms/analytics/ao;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 284
    :cond_1
    return-void
.end method
