.class public final Lcom/google/android/gms/security/verifier/u;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/gms/security/verifier/p;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 735
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 736
    invoke-static {}, Lcom/google/android/gms/security/verifier/p;->a()[Lcom/google/android/gms/security/verifier/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/verifier/u;->cachedSize:I

    .line 737
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 798
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 799
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 800
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 801
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    aget-object v2, v2, v0

    .line 802
    if-eqz v2, :cond_0

    .line 803
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 800
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 808
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 809
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 812
    :cond_2
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 748
    if-ne p1, p0, :cond_1

    .line 766
    :cond_0
    :goto_0
    return v0

    .line 751
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/verifier/u;

    if-nez v2, :cond_2

    move v0, v1

    .line 752
    goto :goto_0

    .line 754
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/verifier/u;

    .line 755
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 757
    goto :goto_0

    .line 759
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    if-nez v2, :cond_4

    .line 760
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    move v0, v1

    .line 761
    goto :goto_0

    .line 763
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 764
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 771
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 774
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 776
    return v0

    .line 774
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 712
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/security/verifier/p;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/security/verifier/p;

    invoke-direct {v3}, Lcom/google/android/gms/security/verifier/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/security/verifier/p;

    invoke-direct {v3}, Lcom/google/android/gms/security/verifier/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 782
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 783
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 784
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/u;->a:[Lcom/google/android/gms/security/verifier/p;

    aget-object v1, v1, v0

    .line 785
    if-eqz v1, :cond_0

    .line 786
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 783
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 790
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 791
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/u;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 793
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 794
    return-void
.end method
