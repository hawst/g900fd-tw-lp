.class public final Lcom/google/android/gms/drive/ui/picker/a/a/n;
.super Lcom/google/android/gms/drive/ui/picker/a/a/g;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/text/Collator;

.field private d:Landroid/util/Pair;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/android/gms/drive/query/SortOrder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 80
    sget v0, Lcom/google/android/gms/p;->fK:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Locale;)V

    .line 81
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/a/g;-><init>()V

    .line 90
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->e:Landroid/content/Context;

    .line 91
    iput-object p2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->b:Ljava/lang/String;

    .line 94
    invoke-static {p3}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->c:Ljava/text/Collator;

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->c:Ljava/text/Collator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 97
    new-instance v0, Lcom/google/android/gms/drive/query/k;

    invoke-direct {v0}, Lcom/google/android/gms/drive/query/k;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/drive/query/k;->b:Z

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/query/k;->a(Lcom/google/android/gms/drive/metadata/k;)Lcom/google/android/gms/drive/query/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/query/k;->a()Lcom/google/android/gms/drive/query/SortOrder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->f:Lcom/google/android/gms/drive/query/SortOrder;

    .line 101
    return-void
.end method

.method private static b(Lcom/google/android/gms/drive/ui/picker/a/l;)Lcom/google/android/gms/drive/ui/picker/a/a/o;
    .locals 2

    .prologue
    .line 173
    invoke-interface {p0}, Lcom/google/android/gms/drive/ui/picker/a/l;->b()Ljava/lang/String;

    move-result-object v0

    .line 174
    const-string v1, "application/vnd.google-apps.folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->a:Lcom/google/android/gms/drive/ui/picker/a/a/o;

    .line 177
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->b:Lcom/google/android/gms/drive/ui/picker/a/a/o;

    goto :goto_0
.end method

.method private f()Landroid/util/Pair;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->d:Landroid/util/Pair;

    if-nez v0, :cond_2

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 110
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4, v1, v0}, Ljava/lang/String;->codePointCount(II)I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    move v0, v1

    move v3, v1

    .line 114
    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 115
    invoke-virtual {v4, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    .line 116
    add-int/lit8 v2, v0, 0x1

    add-int v7, v3, v6

    invoke-virtual {v4, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v0

    .line 117
    add-int v0, v3, v6

    move v3, v0

    move v0, v2

    .line 118
    goto :goto_0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->c:Ljava/text/Collator;

    invoke-static {v5, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 124
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 125
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/w;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->e:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->gx:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const-string v6, ""

    invoke-direct {v0, v3, v4, v6}, Lcom/google/android/gms/drive/ui/picker/a/w;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    array-length v3, v5

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v5, v0

    .line 127
    new-instance v6, Lcom/google/android/gms/drive/ui/picker/a/w;

    invoke-direct {v6, v4, v1, v4}, Lcom/google/android/gms/drive/ui/picker/a/w;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 129
    :cond_1
    new-array v0, v1, [Lcom/google/android/gms/drive/ui/picker/a/w;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/ui/picker/a/w;

    .line 134
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v0, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->d:Landroid/util/Pair;

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->d:Landroid/util/Pair;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/SortOrder;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->f:Lcom/google/android/gms/drive/query/SortOrder;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/ui/picker/a/a/r;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 147
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->k()Z

    move-result v4

    .line 150
    if-eqz v4, :cond_2

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/a/a/n;->b(Lcom/google/android/gms/drive/ui/picker/a/l;)Lcom/google/android/gms/drive/ui/picker/a/a/o;

    move-result-object v0

    .line 152
    iget-object v5, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v5}, Lcom/google/android/gms/drive/ui/picker/a/l;->l()Z

    .line 158
    :goto_0
    iget-object v5, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v5}, Lcom/google/android/gms/drive/ui/picker/a/l;->l()Z

    move-result v5

    .line 159
    if-eqz v5, :cond_3

    .line 160
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-static {v1}, Lcom/google/android/gms/drive/ui/picker/a/a/n;->b(Lcom/google/android/gms/drive/ui/picker/a/l;)Lcom/google/android/gms/drive/ui/picker/a/a/o;

    move-result-object v1

    .line 161
    iget-object v6, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v6}, Lcom/google/android/gms/drive/ui/picker/a/l;->k()Z

    .line 166
    :goto_1
    iget-object v6, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-static {v6}, Lcom/google/android/gms/drive/ui/picker/a/a/n;->b(Lcom/google/android/gms/drive/ui/picker/a/l;)Lcom/google/android/gms/drive/ui/picker/a/a/o;

    move-result-object v6

    .line 167
    if-eqz v4, :cond_0

    if-eq v6, v0, :cond_4

    :cond_0
    move v4, v3

    .line 168
    :goto_2
    if-eqz v5, :cond_1

    if-eq v6, v1, :cond_5

    :cond_1
    move v0, v3

    .line 169
    :goto_3
    invoke-virtual {v6, v4, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/o;->a(ZZ)Lcom/google/android/gms/drive/ui/picker/a/a/r;

    move-result-object v0

    return-object v0

    .line 154
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/ui/picker/a/l;->a(I)Z

    move-object v0, v1

    goto :goto_0

    .line 163
    :cond_3
    iget-object v6, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v6}, Lcom/google/android/gms/drive/ui/picker/a/l;->m()V

    goto :goto_1

    :cond_4
    move v4, v2

    .line 167
    goto :goto_2

    :cond_5
    move v0, v2

    .line 168
    goto :goto_3
.end method

.method public final c()Landroid/widget/SectionIndexer;
    .locals 4

    .prologue
    .line 187
    new-instance v1, Lcom/google/android/gms/drive/ui/picker/a/u;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/a/a/n;->c:Ljava/text/Collator;

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/a/n;->f()Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [Lcom/google/android/gms/drive/ui/picker/a/w;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/drive/ui/picker/a/u;-><init>(Lcom/google/android/gms/drive/ui/picker/a/l;Ljava/text/Collator;[Lcom/google/android/gms/drive/ui/picker/a/w;)V

    return-object v1
.end method
