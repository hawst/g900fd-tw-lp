.class public final Lcom/google/android/gms/drive/metadata/a/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 3

    .prologue
    .line 35
    new-instance v1, Lcom/google/android/gms/drive/internal/model/File;

    invoke-direct {v1}, Lcom/google/android/gms/drive/internal/model/File;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/f;

    .line 37
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    .line 38
    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/internal/model/File;)V

    goto :goto_0

    .line 40
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 3

    .prologue
    .line 55
    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v1

    .line 56
    invoke-virtual {p2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/f;

    .line 57
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    .line 58
    invoke-virtual {v0, p0, p2, p1, v1}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    .line 59
    invoke-virtual {v0, p2, p0, p1}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)V

    goto :goto_0

    .line 61
    :cond_0
    return-object v1
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 6

    .prologue
    .line 89
    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v1

    .line 90
    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .line 91
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 93
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/f;

    move-result-object v3

    .line 94
    if-eqz v3, :cond_0

    .line 95
    invoke-static {v3}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    .line 96
    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Lorg/json/JSONObject;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    goto :goto_0

    .line 98
    :cond_0
    const-string v3, "MetadataBufferConversion"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Ignored unknown metadata field in JSON: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 101
    :cond_1
    return-object v1
.end method

.method public static b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 77
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/f;

    .line 79
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    .line 80
    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lorg/json/JSONObject;)V

    goto :goto_0

    .line 82
    :cond_0
    return-object v1
.end method
