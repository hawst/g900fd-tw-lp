.class final Lcom/google/android/gms/drive/api/a/aa;
.super Lcom/google/android/gms/drive/internal/ce;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/concurrent/CountDownLatch;

.field final synthetic b:Lcom/google/android/gms/drive/api/a/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/api/a/y;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/gms/drive/api/a/aa;->b:Lcom/google/android/gms/drive/api/a/y;

    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/aa;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/ce;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/internal/OnEventResponse;)V
    .locals 3

    .prologue
    .line 146
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnEventResponse;->a()Lcom/google/android/gms/drive/events/DriveEvent;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/events/ChangeEvent;

    .line 147
    invoke-virtual {v0}, Lcom/google/android/gms/drive/events/ChangeEvent;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aa;->b:Lcom/google/android/gms/drive/api/a/y;

    iget-object v0, v0, Lcom/google/android/gms/drive/api/a/y;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/aa;->b:Lcom/google/android/gms/drive/api/a/y;

    invoke-static {v1}, Lcom/google/android/gms/drive/api/a/y;->c(Lcom/google/android/gms/drive/api/a/y;)Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, p0}, Lcom/google/android/gms/drive/api/d;->b(Lcom/google/android/gms/drive/DriveId;ILcom/google/android/gms/drive/internal/cd;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aa;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 152
    :cond_0
    return-void
.end method
