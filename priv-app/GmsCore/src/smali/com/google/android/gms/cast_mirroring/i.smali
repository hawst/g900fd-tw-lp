.class final Lcom/google/android/gms/cast_mirroring/i;
.super Lcom/google/android/gms/cast_mirroring/b/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast_mirroring/m;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/os/IBinder$DeathRecipient;

.field private c:Lcom/google/android/gms/cast_mirroring/b/a;

.field private d:Lcom/google/android/gms/cast_mirroring/JGCastService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/gms/cast_mirroring/b/e;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/google/android/gms/cast_mirroring/i;->a:Landroid/content/Context;

    .line 118
    new-instance v0, Lcom/google/android/gms/cast_mirroring/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast_mirroring/j;-><init>(Lcom/google/android/gms/cast_mirroring/i;)V

    iput-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->b:Landroid/os/IBinder$DeathRecipient;

    .line 125
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 285
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 296
    :goto_0
    return v0

    .line 290
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast_mirroring/i;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 291
    iget-object v1, p0, Lcom/google/android/gms/cast_mirroring/i;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    const/4 v0, 0x1

    goto :goto_0

    .line 294
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static c()V
    .locals 4

    .prologue
    .line 250
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a()Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v0

    if-nez v0, :cond_0

    .line 252
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->b()Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a()Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    return-void

    .line 253
    :catch_0
    move-exception v0

    .line 254
    const-string v1, "CastMirroringService"

    const-string v2, "InterruptedException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 267
    sget-object v0, Lcom/google/android/gms/cast_mirroring/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    :cond_0
    return-void

    .line 272
    :cond_1
    sget-object v0, Lcom/google/android/gms/cast_mirroring/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast_mirroring/i;->a(Ljava/lang/String;)Z

    move-result v1

    .line 276
    sget-object v0, Lcom/google/android/gms/cast_mirroring/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast_mirroring/i;->a(Ljava/lang/String;)Z

    move-result v0

    or-int/2addr v0, v1

    .line 279
    if-nez v0, :cond_0

    .line 280
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Cast mirroring is not enabled."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->d:Lcom/google/android/gms/cast_mirroring/JGCastService;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->d:Lcom/google/android/gms/cast_mirroring/JGCastService;

    invoke-virtual {v0}, Lcom/google/android/gms/cast_mirroring/JGCastService;->disconnect()V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->d:Lcom/google/android/gms/cast_mirroring/JGCastService;

    .line 154
    :cond_0
    return-void
.end method

.method public final a(III)V
    .locals 2

    .prologue
    .line 206
    packed-switch p1, :pswitch_data_0

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 208
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v0, p2}, Lcom/google/android/gms/cast_mirroring/b/a;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    const-string v0, "CastMirroringService"

    const-string v1, "client died - unable to notify."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a()Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 237
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a()Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d()V

    goto :goto_0

    .line 215
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v0}, Lcom/google/android/gms/cast_mirroring/b/a;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 242
    :catch_1
    move-exception v0

    const-string v0, "CastMirroringService"

    const-string v1, "client already disconnected - unable to notify."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a()Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 244
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a()Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d()V

    goto :goto_0

    .line 222
    :pswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->d:Lcom/google/android/gms/cast_mirroring/JGCastService;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->d:Lcom/google/android/gms/cast_mirroring/JGCastService;

    invoke-virtual {v0}, Lcom/google/android/gms/cast_mirroring/JGCastService;->release()V

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->d:Lcom/google/android/gms/cast_mirroring/JGCastService;

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v0}, Lcom/google/android/gms/cast_mirroring/b/a;->b()V

    .line 228
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 206
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/cast_mirroring/b/a;)V
    .locals 2

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/google/android/gms/cast_mirroring/i;->d()V

    .line 188
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/i;->c()V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a()Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V

    .line 191
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast_mirroring/b/a;Lcom/google/android/gms/cast_mirroring/b/f;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/gms/cast_mirroring/i;->d()V

    .line 179
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/i;->c()V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a()Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;Lcom/google/android/gms/cast_mirroring/b/f;Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 183
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast_mirroring/b/a;ZLjava/lang/String;Landroid/view/Surface;I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 130
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    if-eqz p2, :cond_0

    .line 132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "creating source not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v0}, Lcom/google/android/gms/cast_mirroring/b/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast_mirroring/i;->b:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    new-instance v0, Lcom/google/android/gms/cast_mirroring/JGCastService;

    iget-object v1, p0, Lcom/google/android/gms/cast_mirroring/i;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/cast_mirroring/JGCastService;-><init>(Landroid/content/Context;Lcom/google/android/gms/cast_mirroring/m;)V

    iput-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->d:Lcom/google/android/gms/cast_mirroring/JGCastService;

    const/16 v0, 0x3a

    invoke-virtual {p3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p3, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/cast_mirroring/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    and-int/lit8 v0, p5, 0x1

    if-eqz v0, :cond_1

    const/high16 v0, -0x80000000

    or-int/2addr v6, v0

    :cond_1
    const-string v0, "CastMirroringService"

    const-string v1, "createSourceOrSink, flags = 0x%x"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->d:Lcom/google/android/gms/cast_mirroring/JGCastService;

    const-string v2, "0.0.0.0"

    if-eqz p2, :cond_2

    const/4 v5, 0x0

    :goto_0
    move v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/cast_mirroring/JGCastService;->createSourceOrSink(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/Surface;I)V

    .line 146
    :goto_1
    return-void

    .line 140
    :catch_0
    move-exception v0

    const-string v0, "CastMirroringService"

    const-string v1, "unable to link cast mirroring reaper"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/cast_mirroring/i;->a()V

    goto :goto_1

    :cond_2
    move-object v5, p4

    .line 145
    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/cast_mirroring/i;->a()V

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    if-eqz v0, :cond_0

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v0}, Lcom/google/android/gms/cast_mirroring/b/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast_mirroring/i;->b:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    iput-object v3, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    .line 169
    iput-object v3, p0, Lcom/google/android/gms/cast_mirroring/i;->b:Landroid/os/IBinder$DeathRecipient;

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v0

    iput-object v3, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    .line 169
    iput-object v3, p0, Lcom/google/android/gms/cast_mirroring/i;->b:Landroid/os/IBinder$DeathRecipient;

    goto :goto_0

    .line 168
    :catchall_0
    move-exception v0

    iput-object v3, p0, Lcom/google/android/gms/cast_mirroring/i;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    .line 169
    iput-object v3, p0, Lcom/google/android/gms/cast_mirroring/i;->b:Landroid/os/IBinder$DeathRecipient;

    throw v0
.end method

.method public final b(Lcom/google/android/gms/cast_mirroring/b/a;)V
    .locals 3

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/android/gms/cast_mirroring/i;->d()V

    .line 196
    invoke-static {}, Lcom/google/android/gms/cast_mirroring/i;->c()V

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/i;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->c()I

    move-result v1

    invoke-static {}, Lcom/google/android/gms/cast_mirroring/CastMirroringService;->a()Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v2

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V

    .line 200
    return-void
.end method
