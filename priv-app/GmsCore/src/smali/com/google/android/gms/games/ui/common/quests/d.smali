.class public final Lcom/google/android/gms/games/ui/common/quests/d;
.super Lcom/google/android/gms/games/ui/d;
.source "SourceFile"


# instance fields
.field private final h:Lcom/google/android/gms/games/ui/q;

.field private final i:Lcom/google/android/gms/games/ui/common/quests/g;

.field private final j:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/quests/g;Z)V
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/q;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/d;-><init>(Landroid/content/Context;)V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/quests/d;->h:Lcom/google/android/gms/games/ui/q;

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/quests/d;->i:Lcom/google/android/gms/games/ui/common/quests/g;

    .line 27
    if-eqz p3, :cond_0

    sget v0, Lcom/google/android/gms/l;->bV:I

    :goto_0
    iput v0, p0, Lcom/google/android/gms/games/ui/common/quests/d;->j:I

    .line 29
    return-void

    .line 27
    :cond_0
    sget v0, Lcom/google/android/gms/l;->bW:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/quests/d;)Lcom/google/android/gms/games/ui/q;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/d;->h:Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/quests/d;)Lcom/google/android/gms/games/ui/common/quests/g;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/d;->i:Lcom/google/android/gms/games/ui/common/quests/g;

    return-object v0
.end method


# virtual methods
.method protected final b(Landroid/view/ViewGroup;I)Lcom/google/android/gms/games/ui/f;
    .locals 4

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/games/ui/common/quests/e;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/quests/d;->e:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/google/android/gms/games/ui/common/quests/d;->j:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/quests/e;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/google/android/gms/j;->gG:I

    return v0
.end method

.method public final r()I
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/d;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/k;->n:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method
