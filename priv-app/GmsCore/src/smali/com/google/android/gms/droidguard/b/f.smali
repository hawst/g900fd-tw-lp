.class public final Lcom/google/android/gms/droidguard/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/gms/droidguard/b/f;->a:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/google/android/gms/droidguard/b/f;->b:Landroid/content/Context;

    .line 52
    iput-object p3, p0, Lcom/google/android/gms/droidguard/b/f;->c:Ljava/lang/Throwable;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;[B)[B
    .locals 19

    .prologue
    .line 56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 59
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_3

    aget-object v5, v3, v2

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "em.V"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "j"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "d.g"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    const-string v6, "a"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "r.e"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/16 v6, 0x28

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    const/16 v6, 0x1e

    if-ne v5, v6, :cond_2

    :cond_1
    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v2

    invoke-virtual {v6}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v2

    invoke-virtual {v6}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 66
    new-instance v3, Lcom/google/android/gms/droidguard/b/m;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "$"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/droidguard/b/m;-><init>(Ljava/lang/String;)V

    .line 68
    const/16 v4, 0x66

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    .line 76
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    const/16 v7, 0x4d

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3, v4}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 83
    new-instance v9, Lcom/google/android/gms/droidguard/b/m;

    invoke-direct {v9, v8}, Lcom/google/android/gms/droidguard/b/m;-><init>(Ljava/lang/String;)V

    .line 84
    new-instance v12, Lcom/google/android/gms/droidguard/b/m;

    const-class v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v2}, Lcom/google/android/gms/droidguard/b/m;-><init>(Ljava/lang/String;)V

    .line 87
    new-instance v2, Lcom/google/android/gms/droidguard/b/g;

    invoke-direct {v2, v9}, Lcom/google/android/gms/droidguard/b/g;-><init>(Lcom/google/android/gms/droidguard/b/m;)V

    invoke-virtual {v2}, Lcom/google/android/gms/droidguard/b/g;->a()Ljava/lang/String;

    move-result-object v13

    .line 89
    const-string v5, ""

    .line 90
    const-wide/16 v6, 0x0

    .line 91
    const-string v4, ""

    .line 92
    const-string v3, ""

    .line 93
    const-string v2, ""

    .line 94
    if-nez p2, :cond_4

    .line 95
    new-instance v2, Lcom/google/android/gms/droidguard/b/i;

    invoke-direct {v2}, Lcom/google/android/gms/droidguard/b/i;-><init>()V

    invoke-static {v9}, Lcom/google/android/gms/droidguard/b/i;->a(Lcom/google/android/gms/droidguard/b/m;)Ljava/lang/String;

    move-result-object v5

    .line 96
    new-instance v2, Lcom/google/android/gms/droidguard/b/n;

    invoke-direct {v2}, Lcom/google/android/gms/droidguard/b/n;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    .line 97
    new-instance v2, Lcom/google/android/gms/droidguard/b/a;

    invoke-direct {v2, v12}, Lcom/google/android/gms/droidguard/b/a;-><init>(Lcom/google/android/gms/droidguard/b/m;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/gms/droidguard/b/a;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v4

    .line 98
    new-instance v2, Lcom/google/android/gms/droidguard/b/b;

    invoke-direct {v2}, Lcom/google/android/gms/droidguard/b/b;-><init>()V

    invoke-static {}, Lcom/google/android/gms/droidguard/b/b;->a()Ljava/lang/String;

    move-result-object v3

    .line 99
    new-instance v2, Lcom/google/android/gms/droidguard/b/c;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/droidguard/b/f;->b:Landroid/content/Context;

    invoke-direct {v2, v14}, Lcom/google/android/gms/droidguard/b/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/android/gms/droidguard/b/c;->a()Ljava/lang/String;

    move-result-object v2

    .line 107
    :cond_4
    const/4 v14, 0x7

    new-array v14, v14, [I

    fill-array-data v14, :array_1

    .line 108
    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v12, v14}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v8, v14}, Lcom/google/android/gms/common/util/al;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 110
    new-instance v14, Lcom/google/android/gms/droidguard/b/e;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/droidguard/b/f;->b:Landroid/content/Context;

    invoke-direct {v14, v9, v15}, Lcom/google/android/gms/droidguard/b/e;-><init>(Lcom/google/android/gms/droidguard/b/m;Landroid/content/Context;)V

    .line 114
    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    const/16 v16, 0x2

    aput-object v2, v15, v16

    const/16 v16, 0x3

    aput-object v4, v15, v16

    .line 118
    if-nez p2, :cond_5

    .line 120
    invoke-virtual {v14, v8, v15}, Lcom/google/android/gms/droidguard/b/e;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/droidguard/b/d;

    move-result-object v8

    .line 127
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 131
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v18, "5="

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n7="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v8, Lcom/google/android/gms/droidguard/b/d;->a:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n8="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v8, Lcom/google/android/gms/droidguard/b/d;->b:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n9="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v8, v8, Lcom/google/android/gms/droidguard/b/d;->c:Ljava/lang/String;

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v13, "\n"

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 136
    if-nez p2, :cond_b

    .line 137
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/droidguard/b/f;->c:Ljava/lang/Throwable;

    invoke-virtual {v13}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v13

    .line 138
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v14, "0="

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/droidguard/b/f;->a:Ljava/lang/String;

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v14, "\n1="

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\n2="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n3="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n4="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n6="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n10="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v4, v16, v10

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n11="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/f;->c:Ljava/lang/Throwable;

    invoke-virtual {v3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n12="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v13, :cond_6

    const-string v2, ""

    :goto_2
    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n13="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/droidguard/b/f;->c:Ljava/lang/Throwable;

    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n14="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v13, :cond_7

    const-string v2, ""

    :goto_3
    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 152
    :try_start_0
    new-instance v4, Lcom/google/android/gms/droidguard/b/o;

    invoke-direct {v4, v9}, Lcom/google/android/gms/droidguard/b/o;-><init>(Lcom/google/android/gms/droidguard/b/m;)V

    const-string v2, ""

    invoke-static {v2, v15}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sget-object v2, Lcom/google/android/gms/droidguard/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, Lcom/google/android/gms/droidguard/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_9

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x12

    if-lt v2, v6, :cond_8

    new-instance v2, Lcom/google/android/gms/droidguard/b/p;

    iget-object v4, v4, Lcom/google/android/gms/droidguard/b/o;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-direct {v2, v4}, Lcom/google/android/gms/droidguard/b/p;-><init>(Lcom/google/android/gms/droidguard/b/m;)V

    invoke-virtual {v2, v5}, Lcom/google/android/gms/droidguard/b/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 154
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "15="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/droidguard/b/q; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 162
    :goto_4
    if-eqz p2, :cond_a

    .line 163
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 179
    :goto_5
    return-object v2

    .line 124
    :cond_5
    move-object/from16 v0, p2

    invoke-virtual {v14, v8, v0}, Lcom/google/android/gms/droidguard/b/e;->a(Ljava/lang/String;[B)Lcom/google/android/gms/droidguard/b/d;

    move-result-object v8

    goto/16 :goto_1

    .line 138
    :cond_6
    invoke-virtual {v13}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_7
    invoke-static {v13}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 152
    :cond_8
    :try_start_1
    new-instance v2, Lcom/google/android/gms/droidguard/b/q;

    const/4 v4, 0x3

    invoke-direct {v2, v4}, Lcom/google/android/gms/droidguard/b/q;-><init>(I)V

    throw v2
    :try_end_1
    .catch Lcom/google/android/gms/droidguard/b/q; {:try_start_1 .. :try_end_1} :catch_0

    .line 155
    :catch_0
    move-exception v2

    .line 156
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "16="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/droidguard/b/q;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n17="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/droidguard/b/q;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n18="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/droidguard/b/q;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 152
    :cond_9
    :try_start_2
    new-instance v2, Lcom/google/android/gms/droidguard/b/q;

    const/4 v4, 0x4

    invoke-direct {v2, v4}, Lcom/google/android/gms/droidguard/b/q;-><init>(I)V

    throw v2
    :try_end_2
    .catch Lcom/google/android/gms/droidguard/b/q; {:try_start_2 .. :try_end_2} :catch_0

    .line 166
    :cond_a
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/util/al;->a([B)[B

    move-result-object v3

    .line 169
    const/16 v2, 0x38

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    .line 173
    invoke-virtual {v12, v2}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 175
    :try_start_3
    new-instance v4, Lcom/google/android/gms/droidguard/b/j;

    invoke-direct {v4, v12}, Lcom/google/android/gms/droidguard/b/j;-><init>(Lcom/google/android/gms/droidguard/b/m;)V

    invoke-virtual {v4, v3}, Lcom/google/android/gms/droidguard/b/j;->a([B)[B
    :try_end_3
    .catch Lcom/google/android/gms/droidguard/b/k; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v2

    goto/16 :goto_5

    :catch_1
    move-exception v3

    goto/16 :goto_5

    :cond_b
    move-object v2, v8

    goto/16 :goto_4

    .line 68
    :array_0
    .array-data 4
        0x49
        0xc4
        0xaa
        0x9e
        0xb4
        0xb6
        0xc5
        0xaa
        0x98
        0x25
        0xb0
        0xa1
        0x8f
        0xcb
        0xcc
        0xc5
        0x98
        0xc0
        0xb1
        0x6b
        0xcd
        0xb9
        0xce
        0xb2
        0xc1
        0xb1
        0x25
        0x84
        0x84
        0xca
        0xa9
        0xb2
        0xb0
        0x98
        0xba
        0x72
        0x7d
        0xa9
        0xc7
        0xb1
        0xd1
        0xb9
        0x25
        0xc3
        0xb9
        0xc1
        0xb0
        0xca
        0xc7
        0x6b
        0xbe
        0x84
        0x7f
        0xbe
        0xa5
        0xf
        0x25
        0xc0
        0xb2
        0xca
        0xc3
        0xc5
        0xd0
        0xb4
        0xcc
        0xb2
        0xb5
        0xaa
        0x55
        0x8f
        0xa9
        0xc1
        0xc5
        0xd0
        0xc0
        0xc3
        0xc2
        0x78
        0xbf
        0xc2
        0x8b
        0xc9
        0xd0
        0xcb
        0xaa
        0xc5
        0xb9
        0x4
        0xc0
        0xb0
        0xb9
        0xa9
        0x84
        0x98
        0xb1
        0xaa
        0xc2
        0xbc
        0xcd
        0x8f
        0xd1
        0x8f
    .end array-data

    .line 107
    :array_1
    .array-data 4
        0x32
        0x1b
        0x2d
        0x30
        0x5f
        0x74
        0x56
    .end array-data

    .line 169
    :array_2
    .array-data 4
        0x1d
        0x71
        0x61
        0x58
        0x9
        0x75
        0x9
        0x55
        0x5b
        0x9
        0x4a
        0xe
        0x5d
        0x4a
        0x72
        0x68
        0x6b
        0x5a
        0x5a
        0x49
        0x39
        0x24
        0x4d
        0x3e
        0x62
        0x4c
        0x24
        0x73
        0x6e
        0x39
        0x5c
        0x4a
        0x66
        0x39
        0x72
        0x4c
        0x68
        0x45
        0x6b
        0x3e
        0x1e
        0x6d
        0x48
        0x4c
        0x5c
        0x5a
        0x35
        0x6e
        0x4a
        0x7
        0xf
        0x48
        0x4b
        0x6c
        0x39
        0x1e
    .end array-data
.end method
