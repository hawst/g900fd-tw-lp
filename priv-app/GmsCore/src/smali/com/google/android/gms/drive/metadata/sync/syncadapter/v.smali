.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;


# instance fields
.field private final a:Landroid/content/SyncResult;

.field private final b:Lcom/google/android/gms/drive/database/model/a;

.field private final c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

.field private final d:Lcom/google/android/gms/drive/database/r;

.field private final e:Ljava/lang/Boolean;

.field private f:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/a;Landroid/content/SyncResult;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->b:Lcom/google/android/gms/drive/database/model/a;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->a:Landroid/content/SyncResult;

    .line 37
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->d:Lcom/google/android/gms/drive/database/r;

    .line 38
    iput-object p4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->e:Ljava/lang/Boolean;

    .line 39
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->d:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->b:Lcom/google/android/gms/drive/database/model/a;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/c;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->f:J

    .line 72
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/d/g;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->d:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->b:Lcom/google/android/gms/drive/database/model/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/database/model/a;)V

    .line 50
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/d/g;Lcom/google/android/gms/drive/d/d;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    .line 55
    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->b:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->e:Ljava/lang/Boolean;

    invoke-static {v1}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {p2}, Lcom/google/android/gms/drive/d/d;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->h()V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numDeletes:J

    .line 66
    :goto_0
    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->b:Lcom/google/android/gms/drive/database/model/a;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->e:Ljava/lang/Boolean;

    iget-wide v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->f:J

    const/4 v6, 0x1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/d/d;Ljava/lang/Boolean;JZ)V

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numInserts:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numInserts:J

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/d/g;)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method
