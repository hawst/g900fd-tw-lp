.class public Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ap;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/corpora/k;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:Z

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/google/android/gms/search/corpora/k;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->CREATOR:Lcom/google/android/gms/search/corpora/k;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->c:I

    .line 118
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;Z)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput p1, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->c:I

    .line 135
    iput-object p2, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 136
    iput-boolean p3, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->b:Z

    .line 137
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->CREATOR:Lcom/google/android/gms/search/corpora/k;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->CREATOR:Lcom/google/android/gms/search/corpora/k;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/corpora/k;->a(Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;Landroid/os/Parcel;I)V

    .line 149
    return-void
.end method
