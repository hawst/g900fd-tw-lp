.class public final enum Lcom/google/android/gms/drive/database/model/bh;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/bh;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/bh;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/bh;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/bh;

.field private static final synthetic f:[Lcom/google/android/gms/drive/database/model/bh;


# instance fields
.field private final e:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 42
    new-instance v0, Lcom/google/android/gms/drive/database/model/bh;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->d()Lcom/google/android/gms/drive/database/model/bg;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "accountId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v4

    invoke-virtual {v3, v4, v10}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/model/bh;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bh;->a:Lcom/google/android/gms/drive/database/model/bh;

    .line 47
    new-instance v0, Lcom/google/android/gms/drive/database/model/bh;

    const-string v1, "PAYLOAD"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->d()Lcom/google/android/gms/drive/database/model/bg;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "payload"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/database/model/bh;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bh;->b:Lcom/google/android/gms/drive/database/model/bh;

    .line 51
    new-instance v0, Lcom/google/android/gms/drive/database/model/bh;

    const-string v1, "ATTEMPT_COUNT"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->d()Lcom/google/android/gms/drive/database/model/bg;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "retryCount"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/bh;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bh;->c:Lcom/google/android/gms/drive/database/model/bh;

    .line 58
    new-instance v0, Lcom/google/android/gms/drive/database/model/bh;

    const-string v1, "REQUIRED_ENTRY_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->d()Lcom/google/android/gms/drive/database/model/bg;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "requiredEntryId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v4

    invoke-virtual {v3, v4, v10}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/bh;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bh;->d:Lcom/google/android/gms/drive/database/model/bh;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/bh;

    sget-object v1, Lcom/google/android/gms/drive/database/model/bh;->a:Lcom/google/android/gms/drive/database/model/bh;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/bh;->b:Lcom/google/android/gms/drive/database/model/bh;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/drive/database/model/bh;->c:Lcom/google/android/gms/drive/database/model/bh;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/bh;->d:Lcom/google/android/gms/drive/database/model/bh;

    aput-object v1, v0, v9

    sput-object v0, Lcom/google/android/gms/drive/database/model/bh;->f:[Lcom/google/android/gms/drive/database/model/bh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 66
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bh;->e:Lcom/google/android/gms/drive/database/model/ab;

    .line 67
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/bh;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/gms/drive/database/model/bh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bh;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/bh;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/gms/drive/database/model/bh;->f:[Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/bh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/bh;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bh;->e:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bh;->e:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
