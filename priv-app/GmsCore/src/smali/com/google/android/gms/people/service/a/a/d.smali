.class final Lcom/google/android/gms/people/service/a/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/f/m;

.field final synthetic b:[B


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/f/m;[B)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/people/service/a/a/d;->a:Lcom/google/android/gms/people/f/m;

    iput-object p2, p0, Lcom/google/android/gms/people/service/a/a/d;->b:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 74
    const/4 v2, 0x0

    .line 76
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/d;->a:Lcom/google/android/gms/people/f/m;

    invoke-virtual {v0}, Lcom/google/android/gms/people/f/m;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/d;->b:[B

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/a/d;->b:[B

    array-length v3, v3

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 82
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/d;->a:Lcom/google/android/gms/people/f/m;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 84
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 80
    :goto_1
    :try_start_2
    const-string v2, "PeopleService"

    const-string v3, "Failed to write to pipe"

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 82
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/d;->a:Lcom/google/android/gms/people/f/m;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 83
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/d;->a:Lcom/google/android/gms/people/f/m;

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    .line 82
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 79
    :catch_1
    move-exception v0

    goto :goto_1
.end method
