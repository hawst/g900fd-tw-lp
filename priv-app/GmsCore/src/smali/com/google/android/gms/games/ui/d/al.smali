.class public final Lcom/google/android/gms/games/ui/d/al;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/res/Resources;I)I
    .locals 3

    .prologue
    .line 1626
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The level must be higher or equal to 1"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 1628
    sget v0, Lcom/google/android/gms/c;->j:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 1629
    add-int/lit8 v1, p1, -0x1

    array-length v2, v0

    rem-int/2addr v1, v2

    .line 1630
    aget v0, v0, v1

    return v0

    .line 1626
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/ui/cn;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2037
    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2039
    invoke-interface {p0}, Lcom/google/android/gms/games/ui/cn;->m()Landroid/app/Activity;

    move-result-object v1

    .line 2040
    instance-of v0, v1, Lcom/google/android/gms/games/ui/q;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2041
    check-cast v0, Lcom/google/android/gms/games/ui/q;

    .line 2042
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    .line 2043
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2044
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->W:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2052
    :goto_0
    return v0

    .line 2049
    :cond_0
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2050
    invoke-interface {p0}, Lcom/google/android/gms/games/ui/cn;->m()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lcom/google/android/gms/d;->b:I

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2052
    iget v0, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    .prologue
    .line 1144
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.play.games"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156
    :goto_0
    return-object p0

    .line 1152
    :cond_0
    :try_start_0
    const-string v0, "com.google.android.play.games"

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 1156
    :catch_0
    move-exception v0

    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 671
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 672
    const-string v1, "com.google.android.play.games"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 677
    invoke-static {p1}, Lcom/google/android/gms/common/internal/ay;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 679
    if-eqz p2, :cond_1

    .line 680
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "pcampaignid"

    invoke-virtual {v1, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 683
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 686
    :cond_1
    return-object v0

    .line 672
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 906
    invoke-static {}, Lcom/google/android/gms/common/audience/a/i;->a()Lcom/google/android/gms/common/audience/a/k;

    move-result-object v1

    .line 907
    invoke-interface {v1, p1}, Lcom/google/android/gms/common/audience/a/k;->m(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;

    .line 908
    invoke-static {p2}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/audience/a/k;->j(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;

    .line 911
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 912
    invoke-interface {v1, p3}, Lcom/google/android/gms/common/audience/a/k;->d(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/k;

    .line 915
    sget v0, Lcom/google/android/gms/p;->ie:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 921
    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/common/audience/a/k;->k(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;

    .line 923
    invoke-interface {v1}, Lcom/google/android/gms/common/audience/a/k;->a()Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 919
    :cond_0
    sget v0, Lcom/google/android/gms/p;->id:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)Landroid/view/View;
    .locals 4

    .prologue
    .line 1561
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/google/android/gms/l;->aI:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->gL:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v2
.end method

.method public static a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/cn;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2001
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 2002
    const/4 v1, 0x0

    .line 2004
    instance-of v2, v0, Lcom/google/android/gms/games/ui/cn;

    if-eqz v2, :cond_6

    .line 2005
    check-cast v0, Lcom/google/android/gms/games/ui/cn;

    .line 2006
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cn;->f()Z

    move-result v1

    .line 2009
    :goto_0
    if-nez v1, :cond_5

    move v2, v1

    move-object v1, p0

    .line 2011
    :cond_0
    instance-of v4, v1, Lcom/google/android/gms/games/ui/cn;

    if-eqz v4, :cond_1

    move-object v0, v1

    .line 2012
    check-cast v0, Lcom/google/android/gms/games/ui/cn;

    .line 2013
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cn;->f()Z

    move-result v2

    .line 2014
    if-nez v2, :cond_3

    .line 2015
    :cond_1
    if-eqz v1, :cond_2

    .line 2019
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2021
    :cond_2
    if-nez v1, :cond_0

    .line 2024
    :cond_3
    :goto_1
    if-eqz v2, :cond_4

    .line 2027
    :goto_2
    return-object v0

    :cond_4
    move-object v0, v3

    goto :goto_2

    :cond_5
    move v2, v1

    goto :goto_1

    :cond_6
    move-object v0, v3

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1316
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1317
    instance-of v0, v1, Lcom/google/android/gms/common/data/u;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1318
    check-cast v0, Lcom/google/android/gms/common/data/u;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/u;->z_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1322
    :cond_0
    :goto_0
    return-object v1

    .line 1318
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 969
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, p1

    const-wide/32 v4, 0xea60

    cmp-long v4, v0, v4

    if-gtz v4, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->jP:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-wide/32 v4, 0x240c8400

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    const-wide/16 v4, 0x3e8

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(JJJ)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {p0, p1, p2}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 873
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 874
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 877
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 878
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    .line 879
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 880
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 877
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 882
    :cond_0
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 886
    :cond_1
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 887
    return-object v2
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Lcom/google/android/gms/games/ui/d/aq;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 538
    sget v0, Lcom/google/android/gms/p;->iF:I

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 541
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 542
    sget v2, Lcom/google/android/gms/l;->aH:I

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 543
    sget v0, Lcom/google/android/gms/j;->gK:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 545
    sget v3, Lcom/google/android/gms/p;->iE:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 547
    new-instance v0, Lcom/google/android/gms/games/ui/d/am;

    invoke-direct {v0, p2, p1, p0}, Lcom/google/android/gms/games/ui/d/am;-><init>(Lcom/google/android/gms/games/ui/d/aq;Lcom/google/android/gms/games/multiplayer/Invitation;Landroid/app/Activity;)V

    .line 569
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 570
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 571
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 572
    const v1, 0x104000a

    invoke-virtual {v3, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 573
    const/high16 v1, 0x1040000

    invoke-virtual {v3, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 576
    sget v1, Lcom/google/android/gms/p;->mk:I

    invoke-virtual {v3, v1, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 577
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 578
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 579
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    .line 580
    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    .line 581
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/app/Dialog;)V
    .locals 4

    .prologue
    .line 1505
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "android:id/titleDivider"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1506
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1507
    if-eqz v0, :cond_0

    .line 1508
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1513
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 387
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "UiUtils"

    const-string v1, "googleApiClient not connected, ignoring request for participant list"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 387
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v1, p3

    if-lez v1, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v1, p3

    new-array v2, v1, [Lcom/google/android/gms/games/multiplayer/ParticipantEntity;

    const/4 v4, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v0

    move v1, v0

    :goto_1
    if-ge v5, v6, :cond_4

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    add-int/lit8 v3, v1, 0x1

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/ParticipantEntity;

    aput-object v0, v2, v1

    move v0, v3

    move-object v1, v4

    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v4, v1

    move v1, v0

    goto :goto_1

    :cond_3
    move v8, v1

    move-object v1, v0

    move v0, v8

    goto :goto_2

    :cond_4
    add-int/2addr v1, p3

    if-eqz v4, :cond_5

    invoke-interface {v4}, Lcom/google/android/gms/games/multiplayer/Participant;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/ParticipantEntity;

    aput-object v0, v2, v1

    :cond_5
    invoke-interface {p6}, Lcom/google/android/gms/games/Game;->m()Landroid/net/Uri;

    move-result-object v6

    invoke-interface {p6}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v7

    sget-object v0, Lcom/google/android/gms/games/d;->n:Lcom/google/android/gms/games/multiplayer/f;

    invoke-interface {p6}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v3

    move-object v1, p1

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/games/multiplayer/f;->a(Lcom/google/android/gms/common/api/v;[Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 837
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 840
    if-nez p2, :cond_0

    .line 841
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 843
    :cond_0
    const-string v0, "com.google.android.gms.games.GAME"

    new-instance v1, Lcom/google/android/gms/games/GameEntity;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/GameEntity;-><init>(Lcom/google/android/gms/games/Game;)V

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 844
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.LAUNCH_GAME"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 845
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 852
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 855
    const-string v1, "package"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 856
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 858
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 859
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 333
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->b(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 334
    invoke-static {p0}, Landroid/support/v4/app/cv;->a(Landroid/content/Context;)Landroid/support/v4/app/cv;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.games.ui.destination.main.MainActivity"

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    new-instance v3, Landroid/content/ComponentName;

    const-string v4, "com.google.android.play.games"

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/cv;->a(Landroid/content/Intent;)Landroid/support/v4/app/cv;

    .line 337
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 338
    if-eqz p2, :cond_1

    .line 339
    invoke-static {v2, p2}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 343
    :cond_1
    new-instance v3, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_GAME_DETAIL"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 346
    const-string v0, "The given game cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "com.google.android.gms.games.GAME"

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/GameEntity;

    invoke-static {p0}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v4, v6, v0, v5, v7}, Lcom/google/android/gms/common/internal/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "GamesDestApi"

    const-string v4, "Failed to downgrade game safely! Aborting."

    invoke-static {v0, v4}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    .line 347
    const-string v0, "UiUtils"

    const-string v1, "viewGameDetail - Failed to add the game to the Intent"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :goto_1
    return-void

    .line 346
    :cond_2
    invoke-virtual {v3, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v0, 0x1

    goto :goto_0

    .line 351
    :cond_3
    invoke-virtual {v3, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 352
    invoke-virtual {v1, v3}, Landroid/support/v4/app/cv;->a(Landroid/content/Intent;)Landroid/support/v4/app/cv;

    .line 353
    invoke-virtual {v1}, Landroid/support/v4/app/cv;->a()V

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 694
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 695
    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 697
    const-string v0, "GPG_clientUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/ui/n;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 698
    :goto_0
    return-void

    .line 697
    :catch_0
    move-exception v0

    const-string v1, "UiUtils"

    const-string v2, "Unable to launch play store intent"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 1488
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1489
    if-eqz v0, :cond_0

    .line 1490
    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1494
    :goto_0
    return-void

    .line 1492
    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/support/v7/widget/bn;)V
    .locals 1

    .prologue
    .line 1778
    instance-of v0, p0, Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 1779
    check-cast p0, Landroid/widget/ImageView;

    .line 1780
    sget v0, Lcom/google/android/gms/h;->an:I

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1781
    new-instance v0, Lcom/google/android/gms/games/ui/d/ao;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/d/ao;-><init>(Landroid/widget/ImageView;)V

    iput-object v0, p1, Landroid/support/v7/widget/bn;->d:Landroid/support/v7/widget/bo;

    .line 1788
    iget-object v0, p1, Landroid/support/v7/widget/bn;->b:Landroid/support/v7/internal/view/menu/v;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/v;->b()V

    .line 1789
    return-void
.end method

.method public static a(Landroid/widget/TextView;F)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1815
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    .line 1817
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1818
    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    .line 1819
    aget-object v0, v0, v5

    .line 1820
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v1

    div-float/2addr v1, p1

    .line 1821
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 1822
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v1

    float-to-int v2, v2

    .line 1823
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v1

    float-to-int v1, v1

    .line 1824
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1825
    invoke-static {v0, v1, v2, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1826
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1827
    invoke-virtual {p0, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1830
    :cond_0
    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/Boolean;)V
    .locals 5

    .prologue
    .line 1607
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setElegantTextHeight"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 1608
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1616
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 1615
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Landroid/content/Context;Lcom/google/android/gms/games/ui/bh;Lcom/google/android/gms/games/achievement/Achievement;)V
    .locals 3

    .prologue
    .line 503
    invoke-interface {p3}, Lcom/google/android/gms/games/achievement/Achievement;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/achievement/AchievementEntity;

    .line 504
    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 505
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.games.destination.ACTION_VIEW_ACHIEVEMENT_DESCRIPTION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 506
    const-string v2, "com.google.android.gms.games.ACHIEVEMENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object v0, v1

    .line 511
    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 512
    return-void

    .line 508
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-interface {v1, p0, v0}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/achievement/AchievementEntity;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/ui/q;)V
    .locals 3

    .prologue
    .line 362
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.VIEW_SHOP_GAMES_LIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 364
    const-string v1, "com.google.android.gms.games.TAB"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 366
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/q;->startActivity(Landroid/content/Intent;)V

    .line 371
    :goto_0
    return-void

    .line 368
    :cond_0
    const-string v0, "UiUtils"

    const-string v1, "showPopularMultiplayer: Trying to show Popular Multiplayer screen when not in the destination app"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static varargs a(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 3

    .prologue
    .line 648
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 649
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_PROFILE_COMPARISON"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 650
    const-string v2, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 651
    const-string v0, "com.google.android.gms.games.ANIMATION"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 653
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    .line 654
    invoke-static {p0, p2}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;

    move-result-object v0

    .line 656
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/q;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 660
    :goto_0
    return-void

    .line 658
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/q;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;)V
    .locals 1

    .prologue
    .line 1756
    invoke-static {p0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 1757
    sget v0, Lcom/google/android/gms/g;->ai:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 1759
    sget v0, Lcom/google/android/gms/g;->aj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 1761
    return-void
.end method

.method public static varargs a(ZI[Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1677
    if-eqz p0, :cond_0

    move p1, v0

    .line 1678
    :cond_0
    array-length v1, p2

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p2, v0

    .line 1679
    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1678
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1681
    :cond_1
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 1334
    packed-switch p0, :pswitch_data_0

    .line 1344
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1342
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1334
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z
    .locals 2

    .prologue
    .line 1449
    invoke-static {p0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 1450
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 1451
    invoke-interface {p0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1452
    const-string v0, "UiUtils"

    const-string v1, "googleApiClient not connected...calling activity.finish()"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1453
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1454
    const-string v0, "UiUtils"

    const-string v1, "calling setResult RESULT_RECONNECT_REQUIRED on activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1455
    const/16 v0, 0x2711

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/q;->setResult(I)V

    .line 1457
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/q;->finish()V

    .line 1458
    const/4 v0, 0x1

    .line 1460
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 523
    invoke-interface {p0}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;J)Ljava/lang/String;
    .locals 9

    .prologue
    const-wide/32 v4, 0x5265c00

    const/4 v8, 0x0

    .line 1028
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1029
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V

    .line 1030
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V

    .line 1031
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V

    .line 1032
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V

    .line 1036
    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1038
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    move-object v1, p0

    move-wide v2, p1

    move-wide v6, v4

    .line 1040
    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->getRelativeDateTimeString(Landroid/content/Context;JJJI)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1046
    :goto_0
    return-object v0

    .line 1044
    :cond_0
    invoke-static {p0, p1, p2}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1546
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v1, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1547
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1548
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 1549
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1550
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1551
    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1167
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1170
    :try_start_0
    const-string v2, "com.google.android.play.games"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1173
    const/4 v0, 0x1

    .line 1175
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1697
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1700
    :cond_0
    return-void
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1187
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1189
    :try_start_0
    const-string v2, "com.google.android.play.games"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1191
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1192
    const v2, 0x1312d00

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1196
    :cond_0
    :goto_0
    return v0

    .line 1194
    :catch_0
    move-exception v1

    const-string v1, "UiUtils"

    const-string v2, "Cannot find the installed destination app. Something is clearly wrong."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1838
    sget-object v0, Lcom/google/android/gms/games/ui/n;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1839
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1840
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1841
    return-void
.end method
