.class public final Lcom/google/android/gms/plus/oob/s;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private final c:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 26
    check-cast p3, [Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/google/android/gms/plus/oob/s;->c:[Ljava/lang/CharSequence;

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/plus/oob/s;->a:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/s;->b:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/oob/s;->a:I

    .line 70
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 64
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/plus/oob/s;->b:Ljava/lang/String;

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/oob/s;->a:I

    .line 66
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/gms/plus/oob/s;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 40
    check-cast v0, Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/gms/plus/oob/s;->a:I

    packed-switch v2, :pswitch_data_0

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/s;->c:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/s;->c:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 41
    :goto_0
    return-object v1

    .line 40
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/gms/plus/oob/s;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/s;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/gms/plus/oob/s;->c:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/s;->c:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/s;->notifyDataSetChanged()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/plus/oob/s;->a:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
