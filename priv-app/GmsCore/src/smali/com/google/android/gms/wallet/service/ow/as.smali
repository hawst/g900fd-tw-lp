.class public final Lcom/google/android/gms/wallet/service/ow/as;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Landroid/accounts/Account;

.field public e:Lcom/google/aa/b/a/a/a/a/u;

.field public f:Z

.field public g:Z

.field public h:[Ljava/lang/String;

.field public i:Z

.field public j:Z

.field public k:Lcom/google/aa/a/a/a/d;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;ZZ[Ljava/lang/String;ZZLcom/google/aa/a/a/a/d;)V
    .locals 14

    .prologue
    .line 170
    const-wide/16 v12, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v13}, Lcom/google/android/gms/wallet/service/ow/as;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;ZZ[Ljava/lang/String;ZZLcom/google/aa/a/a/a/d;J)V

    .line 173
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;ZZ[Ljava/lang/String;ZZLcom/google/aa/a/a/a/d;J)V
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-wide p11, p0, Lcom/google/android/gms/wallet/service/ow/as;->a:J

    .line 183
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    .line 184
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    .line 185
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    .line 186
    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    .line 187
    iput-boolean p5, p0, Lcom/google/android/gms/wallet/service/ow/as;->f:Z

    .line 188
    iput-boolean p6, p0, Lcom/google/android/gms/wallet/service/ow/as;->g:Z

    .line 189
    iput-object p7, p0, Lcom/google/android/gms/wallet/service/ow/as;->h:[Ljava/lang/String;

    .line 190
    iput-boolean p8, p0, Lcom/google/android/gms/wallet/service/ow/as;->i:Z

    .line 191
    iput-boolean p9, p0, Lcom/google/android/gms/wallet/service/ow/as;->j:Z

    .line 192
    iput-object p10, p0, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    .line 193
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 264
    if-ne p0, p1, :cond_0

    move v0, v1

    .line 271
    :goto_0
    return v0

    .line 267
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/wallet/service/ow/as;

    if-nez v0, :cond_1

    move v0, v2

    .line 268
    goto :goto_0

    .line 270
    :cond_1
    check-cast p1, Lcom/google/android/gms/wallet/service/ow/as;

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    iget-object v3, p1, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    iget-object v3, p1, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->f:Z

    iget-boolean v3, p1, Lcom/google/android/gms/wallet/service/ow/as;->f:Z

    if-ne v0, v3, :cond_a

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->g:Z

    iget-boolean v3, p1, Lcom/google/android/gms/wallet/service/ow/as;->g:Z

    if-ne v0, v3, :cond_a

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ow/as;->h:[Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/gms/wallet/service/ow/as;->h:[Ljava/lang/String;

    if-eq v4, v5, :cond_9

    if-nez v4, :cond_3

    move v3, v2

    :goto_1
    if-nez v5, :cond_4

    move v0, v2

    :goto_2
    if-nez v3, :cond_2

    if-eqz v0, :cond_9

    :cond_2
    if-eq v3, v0, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->i:Z

    iget-boolean v3, p1, Lcom/google/android/gms/wallet/service/ow/as;->i:Z

    if-ne v0, v3, :cond_a

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->j:Z

    iget-boolean v3, p1, Lcom/google/android/gms/wallet/service/ow/as;->j:Z

    if-ne v0, v3, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-object v3, p1, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    goto :goto_0

    :cond_3
    array-length v0, v4

    move v3, v0

    goto :goto_1

    :cond_4
    array-length v0, v5

    goto :goto_2

    :cond_5
    new-instance v6, Lcom/google/android/gms/common/util/i;

    invoke-direct {v6, v3}, Lcom/google/android/gms/common/util/i;-><init>(I)V

    array-length v3, v4

    move v0, v2

    :goto_4
    if-ge v0, v3, :cond_6

    aget-object v7, v4, v0

    invoke-virtual {v6, v7}, Lcom/google/android/gms/common/util/i;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/util/j;

    move-result-object v7

    iget v8, v7, Lcom/google/android/gms/common/util/j;->a:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/google/android/gms/common/util/j;->a:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    array-length v3, v5

    move v0, v2

    :goto_5
    if-ge v0, v3, :cond_7

    aget-object v4, v5, v0

    invoke-virtual {v6, v4}, Lcom/google/android/gms/common/util/i;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/util/j;

    move-result-object v4

    iget v7, v4, Lcom/google/android/gms/common/util/j;->a:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v4, Lcom/google/android/gms/common/util/j;->a:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    iget-object v0, v6, Lcom/google/android/gms/common/util/i;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/util/j;

    iget v0, v0, Lcom/google/android/gms/common/util/j;->a:I

    if-eqz v0, :cond_8

    move v0, v2

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_3

    :cond_a
    move v0, v2

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 248
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 250
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    if-nez v4, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 252
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    invoke-static {v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->c(Lcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->f:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v1

    .line 254
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->g:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v1

    .line 255
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->h:[Ljava/lang/String;

    if-eqz v0, :cond_5

    array-length v4, v0

    if-lez v4, :cond_5

    :goto_5
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v1

    .line 256
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->i:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v1

    .line 257
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/service/ow/as;->j:Z

    if-eqz v1, :cond_7

    :goto_7
    add-int/2addr v0, v2

    .line 258
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    invoke-static {v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->c(Lcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    return v0

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    invoke-virtual {v0}, Landroid/accounts/Account;->hashCode()I

    move-result v0

    goto :goto_0

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 250
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    move v0, v3

    .line 253
    goto :goto_3

    :cond_4
    move v0, v3

    .line 254
    goto :goto_4

    .line 255
    :cond_5
    const/4 v0, 0x0

    goto :goto_5

    :cond_6
    move v0, v3

    .line 256
    goto :goto_6

    :cond_7
    move v2, v3

    .line 257
    goto :goto_7
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 197
    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->a()Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/wallet/service/ow/as;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ah;->a(J)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ah;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/service/ow/as;->f:Z

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ah;->a(Z)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/service/ow/as;->g:Z

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ah;->a(Z)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/as;->h:[Ljava/lang/String;

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/service/ow/as;->i:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Z)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/service/ow/as;->j:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Z)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ah;->a:Ljava/util/ArrayList;

    const-string v3, "\u203d"

    const/4 v4, 0x1

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;[Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method
