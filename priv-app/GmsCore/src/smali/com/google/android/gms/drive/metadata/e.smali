.class public final Lcom/google/android/gms/drive/metadata/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Z
    .locals 13

    .prologue
    const/16 v12, 0x1e

    const/16 v11, 0xa

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 30
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->c:Lcom/google/android/gms/drive/metadata/internal/a/c;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    if-eqz v0, :cond_9

    invoke-virtual {p1, p0}, Lcom/google/android/gms/drive/database/model/ah;->a(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->b()Ljava/util/Map;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->g()J

    move-result-wide v6

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v2, v3

    move v4, v3

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v1

    if-ne v1, v5, :cond_0

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    move v1, v5

    :goto_2
    if-eqz v10, :cond_3

    if-eqz v1, :cond_a

    const/4 v1, -0x1

    :goto_3
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v0

    if-ne v0, v5, :cond_5

    add-int/2addr v2, v1

    :goto_4
    int-to-long v0, v1

    add-long/2addr v0, v6

    move-wide v6, v0

    goto :goto_1

    :cond_2
    move v1, v3

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_4

    new-instance v1, Lcom/google/android/gms/common/service/h;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Cannot delete non-existent property: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v11, v0, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1

    :cond_4
    move v1, v5

    goto :goto_3

    :cond_5
    add-int/2addr v4, v1

    goto :goto_4

    :cond_6
    if-le v4, v12, :cond_7

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "A resource cannot have more than %d public properties"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v11, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_7
    if-le v2, v12, :cond_8

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "A resource cannot have more than %d private properties"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v11, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_8
    const-wide/16 v0, 0x64

    cmp-long v0, v6, v0

    if-lez v0, :cond_9

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "A resource cannot have more than %d total properties"

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v11, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_9
    return v5

    :cond_a
    move v1, v3

    goto :goto_3
.end method
