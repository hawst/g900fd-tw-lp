.class public final Lcom/google/android/gms/googlehelp/search/r;
.super Landroid/support/v4/widget/f;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final j:[Ljava/lang/String;


# instance fields
.field private final k:Landroid/app/Activity;

.field private final l:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private final m:Lcom/google/android/gms/googlehelp/search/a;

.field private final n:Lcom/google/android/gms/googlehelp/search/SearchView;

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_intent_query"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/googlehelp/search/r;->j:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 3

    .prologue
    .line 71
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/widget/f;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/search/r;->o:I

    .line 73
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/r;->k:Landroid/app/Activity;

    .line 74
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/r;->l:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 75
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->f()Lcom/google/android/gms/googlehelp/search/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/r;->m:Lcom/google/android/gms/googlehelp/search/a;

    .line 76
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/search/r;->n:Lcom/google/android/gms/googlehelp/search/SearchView;

    .line 77
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 182
    new-instance v3, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/google/android/gms/googlehelp/search/r;->j:[Ljava/lang/String;

    invoke-direct {v3, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 183
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    .line 184
    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/r;->m:Lcom/google/android/gms/googlehelp/search/a;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/a;->d()Landroid/database/Cursor;

    move-result-object v0

    .line 187
    :goto_0
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 191
    :try_start_0
    const-string v1, "suggest_intent_query"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move v1, v4

    .line 192
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 193
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 194
    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 195
    const/4 v2, 0x3

    new-array v9, v2, [Ljava/lang/Object;

    const/4 v10, 0x0

    add-int/lit8 v2, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v9, v10

    const/4 v1, 0x1

    aput-object v8, v9, v1

    const/4 v1, 0x2

    aput-object v8, v9, v1

    invoke-virtual {v3, v9}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v2

    .line 196
    goto :goto_1

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/r;->m:Lcom/google/android/gms/googlehelp/search/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/search/a;->a(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 198
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 201
    if-nez v5, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/r;->l:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/r;->k:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move-object v0, v3

    .line 216
    :goto_2
    return-object v0

    .line 198
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1

    .line 207
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/r;->k:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/r;->l:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, v2, p1}, Lcom/google/android/gms/googlehelp/a/h;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 209
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 210
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 211
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 212
    new-array v8, v13, [Ljava/lang/Object;

    add-int/lit8 v2, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v4

    aput-object v0, v8, v11

    aput-object v7, v8, v12

    invoke-virtual {v3, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v0, v2

    :goto_4
    move v1, v0

    .line 214
    goto :goto_3

    :cond_4
    move-object v0, v3

    .line 216
    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_4
.end method

.method private static a(Landroid/database/Cursor;I)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 161
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 168
    :goto_0
    return-object v0

    .line 165
    :cond_0
    :try_start_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 166
    :catch_0
    move-exception v1

    .line 167
    const-string v2, "GOOGLEHELP_SuggestionsAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 157
    invoke-static {p0, v0}, Lcom/google/android/gms/googlehelp/search/r;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 109
    if-nez p1, :cond_1

    const-string v0, ""

    .line 112
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/r;->n:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/search/SearchView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/r;->n:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/search/SearchView;->getWindowVisibility()I

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move-object v0, v1

    .line 121
    :goto_1
    return-object v0

    .line 109
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 118
    :cond_2
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/search/r;->a(Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 119
    :catch_0
    move-exception v0

    .line 120
    const-string v2, "GOOGLEHELP_SuggestionsAdapter"

    const-string v3, "Search suggestions query threw an exception."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 121
    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/r;->k:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->cn:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 83
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/f;->a(Landroid/database/Cursor;)V

    .line 85
    if-eqz p1, :cond_0

    .line 86
    const-string v0, "suggest_text_1"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/search/r;->o:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 89
    const-string v1, "GOOGLEHELP_SuggestionsAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 133
    sget v0, Lcom/google/android/gms/j;->jg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 134
    iget v1, p0, Lcom/google/android/gms/googlehelp/search/r;->o:I

    invoke-static {p2, v1}, Lcom/google/android/gms/googlehelp/search/r;->a(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 135
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    :goto_0
    sget v1, Lcom/google/android/gms/j;->hY:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 143
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 144
    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    return-void

    .line 138
    :cond_0
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 176
    instance-of v1, v0, Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/r;->n:Lcom/google/android/gms/googlehelp/search/SearchView;

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 179
    :cond_0
    return-void
.end method
