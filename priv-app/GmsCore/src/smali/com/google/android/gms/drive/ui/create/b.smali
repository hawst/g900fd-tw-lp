.class final Lcom/google/android/gms/drive/ui/create/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/ui/create/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/ui/create/a;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/create/b;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 187
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 188
    sget v1, Lcom/google/android/gms/j;->eJ:I

    if-ne v0, v1, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/b;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/create/a;->a(Lcom/google/android/gms/drive/ui/create/a;)V

    .line 197
    :goto_0
    return-void

    .line 190
    :cond_0
    sget v1, Lcom/google/android/gms/j;->eI:I

    if-ne v0, v1, :cond_1

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/b;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/q;->setResult(I)V

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/b;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/b;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/create/a;->b(Lcom/google/android/gms/drive/ui/create/a;)V

    goto :goto_0

    .line 195
    :cond_1
    const-string v1, "CreateFileDialogFragment"

    const-string v2, "Unknown view clicked: %s, %s."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object p1, v3, v0

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
