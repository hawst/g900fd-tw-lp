.class public Lcom/google/android/gms/wallet/common/ui/TosActivity;
.super Lcom/google/android/gms/wallet/common/ui/dq;
.source "SourceFile"


# instance fields
.field a:Landroid/view/ViewGroup;

.field private b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dq;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 34
    const-string v1, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 35
    const-string v1, "legalDocs"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 36
    const-string v1, "com.google.android.gms"

    const-class v2, Lcom/google/android/gms/wallet/common/ui/TosActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 45
    const-string v1, "legalDocs"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    const-string v3, "Activity requires legalDocs extra"

    invoke-static {v1, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 47
    const-string v1, "legalDocs"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    .line 50
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onCreate(Landroid/os/Bundle;)V

    .line 51
    sget v1, Lcom/google/android/gms/l;->go:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->setContentView(I)V

    .line 53
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->b:Landroid/view/LayoutInflater;

    .line 54
    sget v1, Lcom/google/android/gms/j;->tf:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->a:Landroid/view/ViewGroup;

    .line 55
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b()[Lcom/google/aa/b/a/a/a/a/s;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    if-eqz v3, :cond_1

    array-length v0, v3

    if-lez v0, :cond_1

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    iget-object v6, v0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->b:Landroid/view/LayoutInflater;

    sget v7, Lcom/google/android/gms/l;->gY:I

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/LinkButton;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/LinkButton;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/common/ui/LinkButton;->a(Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/j;->rl:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->a:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-nez v5, :cond_0

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 57
    :cond_1
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 58
    if-eqz v0, :cond_2

    .line 59
    invoke-virtual {v0}, Landroid/support/v7/app/a;->a()V

    .line 61
    sget v1, Lcom/google/android/gms/p;->CK:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->c(I)V

    .line 67
    :goto_2
    return-void

    .line 63
    :cond_2
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    .line 64
    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    .line 65
    sget v1, Lcom/google/android/gms/p;->CK:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    goto :goto_2
.end method
