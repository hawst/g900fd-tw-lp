.class public final Lcom/google/android/gms/appstate/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final a:Lcom/google/android/gms/common/api/j;

.field public static final b:Lcom/google/android/gms/common/api/Scope;

.field public static final c:Lcom/google/android/gms/common/api/c;

.field private static final d:Lcom/google/android/gms/common/api/i;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/appstate/a;->a:Lcom/google/android/gms/common/api/j;

    .line 48
    new-instance v0, Lcom/google/android/gms/appstate/b;

    invoke-direct {v0}, Lcom/google/android/gms/appstate/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/appstate/a;->d:Lcom/google/android/gms/common/api/i;

    .line 69
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/appstate"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/appstate/a;->b:Lcom/google/android/gms/common/api/Scope;

    .line 72
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/appstate/a;->d:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/appstate/a;->a:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/gms/appstate/a;->b:Lcom/google/android/gms/common/api/Scope;

    aput-object v5, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/appstate/a;->c:Lcom/google/android/gms/common/api/c;

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 563
    new-instance v0, Lcom/google/android/gms/appstate/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appstate/c;-><init>(Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
