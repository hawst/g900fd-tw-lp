.class public final Lcom/google/android/gms/identity/accounts/service/h;
.super Lcom/google/android/gms/identity/accounts/service/p;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/identity/accounts/service/i;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/identity/accounts/service/i;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/identity/accounts/service/p;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/identity/accounts/service/h;->a:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/identity/accounts/service/h;->b:Lcom/google/android/gms/identity/accounts/service/i;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/identity/accounts/service/l;Lcom/google/android/gms/identity/accounts/api/AccountData;)V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/h;->b:Lcom/google/android/gms/identity/accounts/service/i;

    iget-object v1, p0, Lcom/google/android/gms/identity/accounts/service/h;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/identity/accounts/service/k;

    invoke-direct {v2, p1, p2}, Lcom/google/android/gms/identity/accounts/service/k;-><init>(Lcom/google/android/gms/identity/accounts/service/l;Lcom/google/android/gms/identity/accounts/api/AccountData;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/identity/accounts/service/i;->a(Landroid/content/Context;Lcom/google/android/gms/identity/accounts/service/d;)V

    .line 39
    return-void
.end method

.method public final a(Lcom/google/android/gms/identity/accounts/service/l;[B)V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/h;->b:Lcom/google/android/gms/identity/accounts/service/i;

    iget-object v1, p0, Lcom/google/android/gms/identity/accounts/service/h;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/identity/accounts/service/j;

    invoke-direct {v2, p1, p2}, Lcom/google/android/gms/identity/accounts/service/j;-><init>(Lcom/google/android/gms/identity/accounts/service/l;[B)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/identity/accounts/service/i;->a(Landroid/content/Context;Lcom/google/android/gms/identity/accounts/service/d;)V

    .line 45
    return-void
.end method
