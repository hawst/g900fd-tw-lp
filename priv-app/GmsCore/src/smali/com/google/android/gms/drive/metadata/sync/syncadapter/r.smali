.class final Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 370
    check-cast p2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;

    .line 373
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;

    invoke-direct {v0, p0, p2, p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/s;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;Lcom/google/android/gms/drive/metadata/sync/syncadapter/i;Landroid/content/ServiceConnection;)V

    .line 403
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-static {v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->c(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 408
    :goto_0
    return-void

    .line 405
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/view/h;->a:Lcom/google/android/gms/drive/ui/picker/view/h;

    sget-object v3, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;Lcom/google/android/gms/drive/ui/picker/view/h;)Z

    goto :goto_0

    .line 406
    :catch_1
    move-exception v0

    .line 407
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/r;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/view/h;)V

    .line 366
    return-void
.end method
