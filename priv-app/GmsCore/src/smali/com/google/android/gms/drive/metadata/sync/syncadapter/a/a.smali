.class final Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;


# instance fields
.field private a:I

.field private b:J

.field private d:J

.field private final e:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a:I

    .line 14
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->d:J

    .line 16
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->e:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 3

    .prologue
    .line 20
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 30
    :cond_0
    monitor-exit p0

    return-void

    .line 24
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a:I

    if-eq p1, v0, :cond_0

    .line 25
    iput p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a:I

    .line 26
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;

    .line 27
    iget v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a:I

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(JJ)V
    .locals 3

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 43
    :cond_0
    return-void

    .line 38
    :cond_1
    iput-wide p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->b:J

    .line 39
    iput-wide p3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->d:J

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;

    .line 41
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;->a(JJ)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;)V
    .locals 4

    .prologue
    .line 46
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->b:J

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->d:J

    invoke-interface {p1, v0, v1, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;->a(JJ)V

    .line 47
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a:I

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;->a(I)V

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    monitor-exit p0

    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
