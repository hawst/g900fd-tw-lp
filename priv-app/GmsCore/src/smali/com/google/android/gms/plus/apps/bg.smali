.class public final Lcom/google/android/gms/plus/apps/bg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/plus/internal/ah;


# static fields
.field private static a:Lcom/google/android/gms/plus/apps/bg;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/LinkedList;

.field private e:Lcom/google/android/gms/plus/internal/ad;

.field private f:Lcom/google/android/gms/plus/internal/ab;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/apps/bg;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/internal/ad;)V

    .line 89
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/internal/ad;)V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->c:Ljava/util/HashMap;

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->b:Landroid/content/Context;

    .line 95
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->d:Ljava/util/LinkedList;

    .line 96
    iput-object p2, p0, Lcom/google/android/gms/plus/apps/bg;->e:Lcom/google/android/gms/plus/internal/ad;

    .line 97
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/bg;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/google/android/gms/plus/apps/bg;->a:Lcom/google/android/gms/plus/apps/bg;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lcom/google/android/gms/plus/apps/bg;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/apps/bg;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/plus/apps/bg;->a:Lcom/google/android/gms/plus/apps/bg;

    .line 106
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/bg;->a:Lcom/google/android/gms/plus/apps/bg;

    return-object v0
.end method


# virtual methods
.method public final T_()V
    .locals 3

    .prologue
    .line 144
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    sget-object v0, Lcom/google/android/gms/plus/c/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x0

    invoke-interface {v1, p0, v0, v2}, Lcom/google/android/gms/plus/internal/ab;->b(Lcom/google/android/gms/plus/internal/ah;ILjava/lang/String;)V

    .line 146
    return-void
.end method

.method public final W_()V
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    .line 151
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    .line 156
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/a/b;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 161
    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 162
    invoke-virtual {p2}, Lcom/google/android/gms/plus/model/a/b;->c()I

    move-result v5

    move v4, v3

    .line 163
    :goto_0
    if-ge v4, v5, :cond_2

    .line 164
    invoke-virtual {p2, v4}, Lcom/google/android/gms/plus/model/a/b;->b(I)Lcom/google/android/gms/plus/model/a/a;

    move-result-object v0

    .line 165
    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/bg;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/bg;->c:Ljava/util/HashMap;

    new-instance v6, Lcom/google/android/gms/plus/apps/bh;

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->d()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->e()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v1, v7, v8, v3}, Lcom/google/android/gms/plus/apps/bh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    invoke-virtual {v2, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bg;->c:Ljava/util/HashMap;

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/bh;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bg;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bg;->d:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/apps/bi;

    invoke-interface {v1, v0}, Lcom/google/android/gms/plus/apps/bi;->a(Lcom/google/android/gms/plus/apps/bh;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 163
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 168
    :cond_2
    if-eqz p3, :cond_3

    .line 170
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    sget-object v0, Lcom/google/android/gms/plus/c/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, p0, v0, p3}, Lcom/google/android/gms/plus/internal/ab;->b(Lcom/google/android/gms/plus/internal/ah;ILjava/lang/String;)V

    .line 180
    :goto_2
    return-void

    .line 177
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/plus/apps/bi;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 116
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    .line 118
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/apps/bi;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/bh;

    .line 127
    if-nez v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    if-nez v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->e:Lcom/google/android/gms/plus/internal/ad;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/bg;->b:Landroid/content/Context;

    invoke-static {v0, v1, p0, p0, p3}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bg;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 140
    :cond_1
    :goto_0
    return-void

    .line 138
    :cond_2
    invoke-interface {p1, v0}, Lcom/google/android/gms/plus/apps/bi;->a(Lcom/google/android/gms/plus/apps/bh;)V

    goto :goto_0
.end method
