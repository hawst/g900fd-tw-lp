.class public final Lcom/google/android/gms/ads/internal/e/a;
.super Lcom/google/android/gms/ads/internal/e/a/b;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/ads/internal/l;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/l;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/e/a/b;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/e/a;->a:Lcom/google/android/gms/ads/internal/l;

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/e/a;->b:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/e/a;->c:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/e/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/b/l;)V
    .locals 2

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/e/a;->a:Lcom/google/android/gms/ads/internal/l;

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v1, v0}, Lcom/google/android/gms/ads/internal/l;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/e/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/e/a;->a:Lcom/google/android/gms/ads/internal/l;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/l;->t()V

    .line 64
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/e/a;->a:Lcom/google/android/gms/ads/internal/l;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/l;->u()V

    .line 71
    return-void
.end method
