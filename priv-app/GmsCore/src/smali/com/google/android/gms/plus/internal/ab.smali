.class public interface abstract Lcom/google/android/gms/plus/internal/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/er;


# static fields
.field public static final a:Lcom/google/android/gms/plus/internal/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/gms/plus/internal/ac;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/ac;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/google/android/gms/plus/internal/ae;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/af;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/ag;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZ)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/ah;ILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/ai;Lcom/google/android/gms/plus/model/posts/Comment;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/aj;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/ak;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/al;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/am;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/an;Landroid/net/Uri;I)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/ao;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/ap;IILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/ar;Lcom/google/android/gms/plus/model/posts/Post;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/as;Lcom/google/android/gms/plus/model/posts/Post;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/at;Lcom/google/android/gms/plus/model/posts/Post;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/au;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/av;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/aw;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/ax;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/ah;ILjava/lang/String;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/am;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V
.end method

.method public abstract e()Ljava/lang/String;
.end method
