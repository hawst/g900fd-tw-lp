.class public Lcom/google/android/gms/smart_profile/SmartProfileActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/bp;
.implements Lcom/google/android/gms/smart_profile/bj;
.implements Lcom/google/android/gms/smart_profile/card/g;
.implements Lcom/google/android/gms/smart_profile/header/d;
.implements Lcom/google/android/gms/smart_profile/header/view/b;


# instance fields
.field private a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

.field private b:Lcom/google/android/gms/smart_profile/bg;

.field private c:Lcom/google/android/gms/smart_profile/card/f;

.field private d:Landroid/widget/LinearLayout;

.field private e:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

.field private f:Lcom/google/android/gms/smart_profile/header/b;

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/Integer;

.field private l:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;)Landroid/support/v7/widget/bn;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->s()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/b;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v4

    .line 300
    :goto_0
    return-object v0

    .line 255
    :cond_2
    new-instance v3, Landroid/support/v7/widget/bn;

    invoke-direct {v3, p0, p1}, Landroid/support/v7/widget/bn;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 256
    iput-object p0, v3, Landroid/support/v7/widget/bn;->c:Landroid/support/v7/widget/bp;

    .line 257
    iget-object v5, v3, Landroid/support/v7/widget/bn;->a:Landroid/support/v7/internal/view/menu/i;

    .line 258
    invoke-virtual {v3}, Landroid/support/v7/widget/bn;->a()Landroid/view/MenuInflater;

    move-result-object v0

    .line 259
    sget v6, Lcom/google/android/gms/m;->C:I

    invoke-virtual {v0, v6, v5}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->n()Z

    move-result v0

    if-nez v0, :cond_6

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v6}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v7}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, p0, v6, v7}, Lcom/google/android/gms/smart_profile/aj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 270
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_6

    move v0, v1

    .line 275
    :goto_1
    sget v6, Lcom/google/android/gms/j;->tS:I

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 276
    sget v7, Lcom/google/android/gms/p;->xu:I

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 277
    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v6}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v7}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, p0, v6, v7}, Lcom/google/android/gms/smart_profile/aj;->b(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 289
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 297
    :goto_2
    sget v0, Lcom/google/android/gms/j;->fl:I

    invoke-interface {v5, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 298
    sget v0, Lcom/google/android/gms/j;->dL:I

    invoke-interface {v5, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 300
    invoke-interface {v5}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v3

    goto/16 :goto_0

    :cond_3
    move v8, v1

    move v1, v2

    move v2, v8

    .line 294
    goto :goto_2

    :cond_4
    move-object v0, v4

    .line 300
    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/SmartProfileActivity;)Lcom/google/android/gms/smart_profile/bg;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/smart_profile/SmartProfileActivity;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f()V

    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 562
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/gms/j;->tC:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 564
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/gms/j;->rZ:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 566
    return-void

    :cond_0
    move v0, v2

    .line 562
    goto :goto_0

    :cond_1
    move v2, v1

    .line 564
    goto :goto_1
.end method

.method private c(Z)Lcom/google/android/gms/common/api/aq;
    .locals 1

    .prologue
    .line 801
    new-instance v0, Lcom/google/android/gms/smart_profile/av;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/smart_profile/av;-><init>(Lcom/google/android/gms/smart_profile/SmartProfileActivity;Z)V

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->c()V

    .line 214
    :cond_0
    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    .line 347
    if-nez v0, :cond_0

    move-object v0, v1

    .line 364
    :goto_0
    return-object v0

    .line 350
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/Person;->q()Ljava/util/List;

    move-result-object v0

    .line 352
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move-object v0, v1

    .line 353
    goto :goto_0

    .line 355
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/k;

    .line 356
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 357
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->e()Lcom/google/android/gms/people/identity/models/l;

    move-result-object v0

    .line 358
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "cp2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 360
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 364
    goto :goto_0
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 523
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v1

    .line 524
    invoke-static {v1}, Lcom/google/android/gms/smart_profile/m;->a(Lcom/google/android/gms/people/identity/models/Person;)I

    move-result v2

    .line 525
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Lcom/google/android/gms/people/identity/models/Person;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_4

    invoke-interface {v1}, Lcom/google/android/gms/people/identity/models/Person;->q()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move v0, v7

    :goto_0
    if-eqz v0, :cond_1

    if-nez v2, :cond_5

    :cond_1
    if-eq v2, v6, :cond_5

    .line 551
    :goto_1
    return-void

    .line 525
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/k;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->c()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->e()Lcom/google/android/gms/people/identity/models/l;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->c()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->b()Ljava/lang/String;

    move-result-object v4

    const-string v5, "contact"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v6

    goto :goto_0

    :cond_4
    move v0, v7

    goto :goto_0

    .line 540
    :cond_5
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 541
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->i()Z

    move-result v0

    .line 550
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b(Z)V

    goto :goto_1

    .line 543
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->v()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v4, "starred"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v6, :cond_7

    move v0, v6

    goto :goto_2

    :cond_7
    move v0, v7

    goto :goto_2

    :cond_8
    move v0, v7

    goto :goto_2
.end method

.method private g()V
    .locals 2

    .prologue
    .line 678
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->h:Z

    .line 679
    sget v0, Lcom/google/android/gms/p;->wX:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 680
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d()V

    .line 681
    return-void
.end method

.method private h()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 718
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v4}, Lcom/google/android/gms/smart_profile/bg;->g()I

    move-result v4

    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gms/smart_profile/ContactsPickerActivity;

    invoke-direct {v5, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "com.google.android.gms.people.smart_profile.QUALIFIED_ID"

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.people.smart_profile.VIEWER_ACCOUNT_NAME"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.people.smart_profile.VIEWER_PAGE_ID"

    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.people.smart_profile.APPLICATION_ID"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v5
.end method

.method private i()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    .line 728
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/Person;->q()Ljava/util/List;

    move-result-object v0

    .line 730
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/k;

    .line 731
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 732
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->e()Lcom/google/android/gms/people/identity/models/l;

    move-result-object v0

    .line 733
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "cp2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 734
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/aj;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 738
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 742
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->t()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/smart_profile/aj;->a(Lcom/google/android/gms/people/identity/models/Person;Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 785
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p0, v1, v2}, Lcom/google/android/gms/smart_profile/aj;->b(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 789
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 790
    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->startActivity(Landroid/content/Intent;)V

    .line 792
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->e:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b()V

    .line 643
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-static {v1, v0}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/graphics/drawable/GradientDrawable$Orientation;Landroid/content/res/Resources;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    .line 644
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 645
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 649
    :goto_0
    return-void

    .line 647
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 305
    sget v0, Lcom/google/android/gms/j;->nc:I

    if-ne p2, v0, :cond_1

    .line 306
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a(Landroid/view/View;)Landroid/support/v7/widget/bn;

    move-result-object v0

    .line 307
    if-eqz v0, :cond_0

    .line 308
    iget-object v0, v0, Landroid/support/v7/widget/bn;->b:Landroid/support/v7/internal/view/menu/v;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/v;->b()V

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    sget v0, Lcom/google/android/gms/j;->rZ:I

    if-ne p2, v0, :cond_3

    .line 311
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 312
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/j;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-direct {p0, v4}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->c(Z)Lcom/google/android/gms/common/api/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 319
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->v()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/smart_profile/m;->a(Landroid/content/Context;ZLjava/lang/String;)V

    .line 322
    invoke-direct {p0, v4}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b(Z)V

    goto :goto_0

    .line 324
    :cond_3
    sget v0, Lcom/google/android/gms/j;->tC:I

    if-ne p2, v0, :cond_5

    .line 325
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 326
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/j;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-direct {p0, v5}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->c(Z)Lcom/google/android/gms/common/api/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 333
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->v()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/smart_profile/m;->a(Landroid/content/Context;ZLjava/lang/String;)V

    .line 336
    invoke-direct {p0, v5}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b(Z)V

    goto/16 :goto_0

    .line 338
    :cond_5
    sget v0, Lcom/google/android/gms/j;->fj:I

    if-ne p2, v0, :cond_8

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/m;->a(Lcom/google/android/gms/people/identity/models/Person;)I

    move-result v0

    if-ne v0, v4, :cond_7

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->startActivity(Landroid/content/Intent;)V

    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/smart_profile/b;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto/16 :goto_0

    :cond_7
    if-le v0, v4, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->h()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 340
    :cond_8
    sget v0, Lcom/google/android/gms/j;->P:I

    if-ne p2, v0, :cond_0

    .line 341
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->j()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/smart_profile/b;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 432
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v5

    .line 433
    if-nez v5, :cond_1

    if-eqz p1, :cond_1

    .line 434
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->g()V

    .line 471
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    new-instance v3, Lcom/google/ac/c/a/a/a/w;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/w;-><init>()V

    if-eqz v0, :cond_5

    const-string v6, "com.google.android.gms.people.smart_profile.DISPLAY_NAME"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "com.google.android.gms.people.smart_profile.DISPLAY_NAME"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    :cond_2
    const-string v6, "com.google.android.gms.people.smart_profile.AVATAR_URL"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "com.google.android.gms.people.smart_profile.AVATAR_URL"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    :cond_3
    const-string v6, "com.google.android.gms.people.smart_profile.COVER_PHOTO_URL"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "com.google.android.gms.people.smart_profile.COVER_PHOTO_URL"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    :cond_4
    const-string v6, "com.google.android.gms.people.smart_profile.TAGLINE"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "com.google.android.gms.people.smart_profile.TAGLINE"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/ac/c/a/a/a/w;->d:Ljava/lang/String;

    :cond_5
    new-instance v0, Lcom/google/android/gms/smart_profile/header/b;

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->e:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-direct {v0, v3, v6, p0}, Lcom/google/android/gms/smart_profile/header/b;-><init>(Lcom/google/ac/c/a/a/a/w;Lcom/google/android/gms/smart_profile/header/view/HeaderView;Lcom/google/android/gms/smart_profile/header/d;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    :cond_6
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    iput-object v0, v3, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    iput-boolean v2, v3, Lcom/google/android/gms/smart_profile/header/b;->a:Z

    iget-object v6, v3, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    if-eqz v6, :cond_b

    iget-object v6, v3, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v6, v6, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, v3, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v7, v3, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v7}, Lcom/google/android/gms/smart_profile/bg;->j()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    :cond_7
    iget-object v6, v3, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v6, v6, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, v3, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v7, v3, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v7}, Lcom/google/android/gms/smart_profile/bg;->l()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    :cond_8
    iget-object v6, v3, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v6, v6, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    iget-object v6, v3, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v7, v3, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v7}, Lcom/google/android/gms/smart_profile/bg;->m()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    :cond_9
    iget-object v6, v3, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->b()Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    move-result-object v0

    iget-object v7, v3, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v7, v7, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    iget-object v8, v3, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v8, v8, Lcom/google/ac/c/a/a/a/w;->d:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/header/b;->a()Z

    move-result v9

    invoke-static {v6, v0, v7, v8, v9}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, v3, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    if-eqz v0, :cond_b

    iget-boolean v0, v3, Lcom/google/android/gms/smart_profile/header/b;->f:Z

    if-eqz v0, :cond_a

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/header/b;->a()Z

    move-result v0

    iget-boolean v6, v3, Lcom/google/android/gms/smart_profile/header/b;->g:Z

    if-eq v0, v6, :cond_b

    :cond_a
    iget-object v6, v3, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    iget-object v0, v3, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v7

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/header/b;->a()Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    :goto_1
    invoke-static {v6, v7, v0}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;IZ)V

    iput-boolean v1, v3, Lcom/google/android/gms/smart_profile/header/b;->f:Z

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/header/b;->a()Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/smart_profile/header/b;->g:Z

    :cond_b
    iget-object v0, v3, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->e(Z)V

    iget-object v0, v3, Lcom/google/android/gms/smart_profile/header/b;->d:Lcom/google/android/gms/smart_profile/header/d;

    if-eqz v0, :cond_d

    const/4 v0, 0x5

    new-array v6, v0, [I

    sget v0, Lcom/google/android/gms/j;->rZ:I

    aput v0, v6, v2

    sget v0, Lcom/google/android/gms/j;->tC:I

    aput v0, v6, v1

    const/4 v0, 0x2

    sget v7, Lcom/google/android/gms/j;->P:I

    aput v7, v6, v0

    const/4 v0, 0x3

    sget v7, Lcom/google/android/gms/j;->fj:I

    aput v7, v6, v0

    sget v0, Lcom/google/android/gms/j;->nc:I

    aput v0, v6, v11

    array-length v7, v6

    move v0, v2

    :goto_2
    if-ge v0, v7, :cond_d

    aget v8, v6, v0

    iget-object v9, v3, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v9, v8}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    new-instance v10, Lcom/google/android/gms/smart_profile/header/c;

    invoke-direct {v10, v3, v8}, Lcom/google/android/gms/smart_profile/header/c;-><init>(Lcom/google/android/gms/smart_profile/header/b;I)V

    invoke-virtual {v9, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_c
    move v0, v2

    goto :goto_1

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/smart_profile/header/b;->a(Landroid/support/v4/app/au;)V

    .line 439
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/b;->c()Z

    move-result v0

    if-nez v0, :cond_e

    if-eqz p1, :cond_e

    .line 440
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->g()V

    goto/16 :goto_0

    .line 444
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/b;->c()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->b()V

    .line 447
    :cond_f
    if-eqz p1, :cond_0

    .line 453
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 454
    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a(Landroid/view/View;)Landroid/support/v7/widget/bn;

    move-result-object v6

    .line 455
    if-nez v6, :cond_10

    .line 456
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/gms/j;->nc:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 460
    :cond_10
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f()V

    .line 461
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/m;->a(Lcom/google/android/gms/people/identity/models/Person;)I

    move-result v0

    if-ne v0, v1, :cond_15

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_14

    move v0, v1

    :goto_3
    move v3, v0

    :goto_4
    if-nez v3, :cond_25

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->D:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_25

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->j()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v0, v7, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_17

    move v0, v1

    :goto_5
    iget-object v7, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    sget v8, Lcom/google/android/gms/j;->fj:I

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    if-eqz v3, :cond_18

    move v3, v2

    :goto_6
    invoke-virtual {v7, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    sget v7, Lcom/google/android/gms/j;->P:I

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v0, :cond_11

    move v4, v2

    :cond_11
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 464
    if-nez v6, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/gms/j;->rZ:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_19

    move v0, v1

    :goto_7
    if-nez v0, :cond_12

    .line 465
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 468
    :cond_12
    iget-object v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->c:Lcom/google/android/gms/smart_profile/card/f;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i:Ljava/lang/String;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i:Ljava/lang/String;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->n()Z

    move-result v0

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->p()Z

    move-result v0

    if-nez v0, :cond_1d

    :cond_13
    move-object v0, v3

    :goto_8
    invoke-virtual {v4, v5, v6, v7, v0}, Lcom/google/android/gms/smart_profile/card/f;->a(Lcom/google/android/gms/people/identity/models/Person;Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 470
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->supportInvalidateOptionsMenu()V

    goto/16 :goto_0

    :cond_14
    move v0, v2

    .line 461
    goto/16 :goto_3

    :cond_15
    if-le v0, v1, :cond_26

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->d()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->h()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_16

    move v0, v1

    :goto_9
    move v3, v0

    goto/16 :goto_4

    :cond_16
    move v0, v2

    goto :goto_9

    :cond_17
    move v0, v2

    goto/16 :goto_5

    :cond_18
    move v3, v4

    goto :goto_6

    .line 464
    :cond_19
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/gms/j;->tC:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1a

    move v0, v1

    goto :goto_7

    :cond_1a
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/gms/j;->P:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1b

    move v0, v1

    goto :goto_7

    :cond_1b
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/gms/j;->fj:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1c

    move v0, v1

    goto/16 :goto_7

    :cond_1c
    move v0, v2

    goto/16 :goto_7

    .line 468
    :cond_1d
    new-instance v8, Lcom/google/ac/c/a/a/a/o;

    invoke-direct {v8}, Lcom/google/ac/c/a/a/a/o;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v9, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v9}, Lcom/google/android/gms/smart_profile/bg;->n()Z

    move-result v9

    if-eqz v9, :cond_21

    iget-object v9, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v9}, Lcom/google/android/gms/smart_profile/bg;->o()Z

    move-result v9

    if-eqz v9, :cond_20

    sget v9, Lcom/google/android/gms/p;->xb:I

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_a
    iput-object v0, v8, Lcom/google/ac/c/a/a/a/o;->b:Ljava/lang/String;

    :cond_1e
    :goto_b
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v9, "android.resource"

    invoke-virtual {v0, v9}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v9, "com.google.android.gms"

    invoke-virtual {v0, v9}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    sget v9, Lcom/google/android/gms/h;->cR:I

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/google/ac/c/a/a/a/o;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->n()Z

    move-result v0

    if-nez v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->p()Z

    move-result v0

    if-eqz v0, :cond_22

    :cond_1f
    :goto_c
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->n()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->p()Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v9, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v9}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v10}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v1, v9, v10}, Lcom/google/android/gms/smart_profile/aj;->b(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    :goto_d
    iput-object v0, v8, Lcom/google/ac/c/a/a/a/o;->e:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/ac/c/a/a/a/f;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/f;-><init>()V

    new-instance v2, Lcom/google/ac/c/a/a/a/n;

    invoke-direct {v2}, Lcom/google/ac/c/a/a/a/n;-><init>()V

    iput-object v2, v1, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    iget-object v2, v1, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    new-array v8, v8, [Lcom/google/ac/c/a/a/a/o;

    invoke-interface {v0, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ac/c/a/a/a/o;

    iput-object v0, v2, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v3

    goto/16 :goto_8

    :cond_20
    sget v9, Lcom/google/android/gms/p;->xa:I

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_a

    :cond_21
    iget-object v9, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v9}, Lcom/google/android/gms/smart_profile/bg;->p()Z

    move-result v9

    if-eqz v9, :cond_1e

    sget v9, Lcom/google/android/gms/p;->xu:I

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/google/ac/c/a/a/a/o;->b:Ljava/lang/String;

    goto/16 :goto_b

    :cond_22
    move v1, v2

    goto :goto_c

    :cond_23
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/aj;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_d

    :cond_24
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v9, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v9}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v10}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v1, v9, v10}, Lcom/google/android/gms/smart_profile/aj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_d

    :cond_25
    move v0, v2

    goto/16 :goto_5

    :cond_26
    move v3, v2

    goto/16 :goto_4
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 369
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 370
    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 371
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d()V

    .line 389
    :goto_0
    return v0

    .line 373
    :cond_0
    sget v2, Lcom/google/android/gms/j;->tS:I

    if-ne v1, v2, :cond_2

    .line 374
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, p0, v2, v3}, Lcom/google/android/gms/smart_profile/aj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->startActivity(Landroid/content/Intent;)V

    .line 375
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, p0, v2, v3}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    .line 378
    :cond_2
    sget v2, Lcom/google/android/gms/j;->fl:I

    if-ne v1, v2, :cond_3

    .line 379
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->k()V

    .line 380
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, p0, v2, v3}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    .line 383
    :cond_3
    sget v2, Lcom/google/android/gms/j;->dL:I

    if-ne v1, v2, :cond_4

    .line 384
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/gms/smart_profile/aj;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 385
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, p0, v2, v3}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    .line 389
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->b()Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 623
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->b()Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 624
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->b()Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/bg;->b()Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v4}, Lcom/google/android/gms/smart_profile/bg;->g()I

    move-result v4

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/gms/smart_profile/aj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 628
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/smart_profile/b;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 633
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 634
    return-void

    .line 626
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->b()Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->u()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v4}, Lcom/google/android/gms/smart_profile/bg;->g()I

    move-result v4

    invoke-static {v2, v3, v0, v4, v1}, Lcom/google/android/gms/smart_profile/aj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 653
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a()V

    .line 655
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->g:Z

    if-eqz v0, :cond_1

    .line 675
    :goto_0
    return-void

    .line 659
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->b()Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    new-instance v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/du;

    invoke-direct {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/du;-><init>()V

    iput-boolean v0, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/du;->a:Z

    iget-object v0, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/du;->b:Ljava/util/Set;

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedContactDataEntity;

    iget-object v5, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/du;->b:Ljava/util/Set;

    iget-boolean v4, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/du;->a:Z

    invoke-direct {v0, v5, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedContactDataEntity;-><init>(Ljava/util/Set;Z)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedContactDataEntity;

    new-instance v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/dx;

    invoke-direct {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/dx;-><init>()V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedContactDataEntity;

    iput-object v0, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/dx;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedContactDataEntity;

    iget-object v0, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/dx;->b:Ljava/util/Set;

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedProfileEntity;

    iget-object v5, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/dx;->b:Ljava/util/Set;

    iget-object v4, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/dx;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedContactDataEntity;

    invoke-direct {v0, v5, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedProfileEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedContactDataEntity;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedProfileEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/ak;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v4

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedProfileEntity;

    iput-object v0, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedProfileEntity;

    iget-object v0, v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->d:Ljava/util/Set;

    const/16 v5, 0x40

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    sget-object v4, Lcom/google/android/gms/smart_profile/b;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v5, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v2, p0, v4, v5, v0}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    .line 665
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->c:Lcom/google/android/gms/smart_profile/card/f;

    if-eqz v0, :cond_8

    .line 666
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->c:Lcom/google/android/gms/smart_profile/card/f;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/f;->c()Ljava/util/List;

    move-result-object v0

    .line 667
    if-eqz v0, :cond_8

    .line 668
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/a/b;

    .line 669
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/a/b;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/a/b;->c()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v5}, Lcom/google/android/gms/smart_profile/ak;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    sget-object v2, Lcom/google/android/gms/smart_profile/c;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->w:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_3
    if-eqz v2, :cond_2

    invoke-virtual {v5, p0, v2, v6, v0}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    goto :goto_2

    .line 659
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 669
    :cond_4
    sget-object v2, Lcom/google/android/gms/smart_profile/c;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->s:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_3

    :cond_5
    sget-object v2, Lcom/google/android/gms/smart_profile/c;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->u:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_3

    :cond_6
    sget-object v2, Lcom/google/android/gms/smart_profile/c;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->v:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_3

    :cond_7
    sget-object v2, Lcom/google/android/gms/smart_profile/c;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v6, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->t:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_3

    .line 674
    :cond_8
    iput-boolean v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->g:Z

    goto/16 :goto_0

    :cond_9
    move-object v2, v3

    goto :goto_3
.end method

.method public finish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 179
    invoke-super {p0}, Landroid/support/v4/app/q;->finish()V

    .line 181
    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->overridePendingTransition(II)V

    .line 182
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 394
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 395
    const/4 v0, 0x0

    .line 396
    packed-switch p1, :pswitch_data_0

    .line 415
    :goto_0
    if-eqz v0, :cond_1

    .line 416
    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/j;->g()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/j;->g()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 418
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->z:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, p0, v2, v3}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 421
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/j;->h()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/j;->h()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/smart_profile/b;->A:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 428
    :cond_1
    return-void

    .line 400
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->k()V

    goto :goto_0

    .line 403
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/smart_profile/bg;->a(Landroid/content/Intent;)V

    .line 404
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/b;->b()V

    .line 405
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/i;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/audience/a/j;

    move-result-object v0

    goto :goto_0

    .line 408
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/smart_profile/bg;->b(Landroid/content/Intent;)V

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/b;->b()V

    .line 410
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/i;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/audience/a/j;

    move-result-object v0

    goto :goto_0

    .line 396
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d()V

    .line 208
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0, v3, v3}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->overridePendingTransition(II)V

    .line 89
    sget v0, Lcom/google/android/gms/l;->eX:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->setContentView(I)V

    .line 91
    sget v0, Lcom/google/android/gms/j;->rN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->a:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    .line 92
    sget v0, Lcom/google/android/gms/j;->jI:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->e:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->e:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(Lcom/google/android/gms/smart_profile/header/view/b;)V

    .line 94
    sget v0, Lcom/google/android/gms/j;->jK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->l:Landroid/view/View;

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->e:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    sget v1, Lcom/google/android/gms/j;->ta:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d:Landroid/widget/LinearLayout;

    .line 98
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->j:Ljava/lang/String;

    .line 99
    invoke-static {}, Lcom/google/android/gms/common/ey;->a()Lcom/google/android/gms/common/ey;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/ey;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Unsupported caller."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/os/Bundle;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->k:Ljava/lang/Integer;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->k:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->as:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->k:Ljava/lang/Integer;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->e:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d(I)V

    .line 110
    if-nez p1, :cond_1

    move v0, v3

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->g:Z

    .line 112
    if-eqz p1, :cond_2

    const-string v0, "isError"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->h:Z

    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "smartProfileUtilFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/bg;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "cardsFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/f;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->c:Lcom/google/android/gms/smart_profile/card/f;

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 121
    const-string v0, "com.google.android.gms.people.smart_profile.VIEWER_ACCOUNT_NAME"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i:Ljava/lang/String;

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    if-nez v0, :cond_5

    .line 123
    const-string v0, "com.google.android.gms.people.smart_profile.VIEWER_PAGE_ID"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 124
    if-eqz v1, :cond_3

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 127
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->g()V

    .line 167
    :goto_2
    return-void

    .line 110
    :cond_1
    const-string v0, "impressionsLogged"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v3

    .line 112
    goto :goto_1

    .line 133
    :cond_3
    const-string v0, "com.google.android.gms.people.smart_profile.QUALIFIED_ID"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 134
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135
    const-string v0, "SmartProfile"

    const-string v1, "Must supply a people qualified id."

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->setResult(I)V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->finish()V

    goto :goto_2

    .line 142
    :cond_4
    const/16 v0, 0x64

    .line 143
    invoke-static {}, Lcom/google/android/gms/common/ey;->a()Lcom/google/android/gms/common/ey;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->j:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/common/ey;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 145
    const-string v0, "com.google.android.gms.people.smart_profile.APPLICATION_ID"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 146
    const-string v0, "com.google.android.gms.people.smart_profile.APPLICATION_ID"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 157
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->k:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->j:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/smart_profile/bg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Lcom/google/android/gms/smart_profile/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    .line 160
    invoke-static {}, Lcom/google/android/gms/smart_profile/card/f;->a()Lcom/google/android/gms/smart_profile/card/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->c:Lcom/google/android/gms/smart_profile/card/f;

    .line 161
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    const-string v2, "smartProfileUtilFragment"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->rV:I

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->c:Lcom/google/android/gms/smart_profile/card/f;

    const-string v3, "cardsFragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 166
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->b:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/smart_profile/bg;->a(Lcom/google/android/gms/smart_profile/bj;)V

    goto :goto_2

    .line 149
    :cond_6
    const-string v0, "SmartProfile"

    const-string v1, "Must supply application id."

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0, v3}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->setResult(I)V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->finish()V

    goto/16 :goto_2

    :cond_7
    move v3, v0

    goto :goto_3
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 197
    packed-switch p1, :pswitch_data_0

    .line 202
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/q;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 199
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->d()V

    .line 200
    const/4 v0, 0x1

    goto :goto_0

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 186
    invoke-super {p0}, Landroid/support/v4/app/q;->onResume()V

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    iget-boolean v0, v0, Lcom/google/android/gms/smart_profile/header/b;->a:Z

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->f:Lcom/google/android/gms/smart_profile/header/b;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/header/b;->a(Landroid/support/v4/app/au;)V

    .line 192
    :cond_0
    invoke-virtual {p0, v2, v2}, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->overridePendingTransition(II)V

    .line 193
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 171
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 173
    const-string v0, "impressionsLogged"

    iget-boolean v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 174
    const-string v0, "isError"

    iget-boolean v1, p0, Lcom/google/android/gms/smart_profile/SmartProfileActivity;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 175
    return-void
.end method
