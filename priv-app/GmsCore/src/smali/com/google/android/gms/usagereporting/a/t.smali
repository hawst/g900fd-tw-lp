.class final Lcom/google/android/gms/usagereporting/a/t;
.super Lcom/google/android/gms/usagereporting/a/p;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/usagereporting/a/g;

.field private final b:Lcom/google/android/gms/common/api/m;

.field private final c:Ljava/util/concurrent/atomic/AtomicReference;

.field private final d:Lcom/google/android/gms/usagereporting/a/u;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/usagereporting/a/g;Lcom/google/android/gms/common/api/m;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/gms/usagereporting/a/u;)V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/a/p;-><init>(B)V

    .line 223
    iput-object p1, p0, Lcom/google/android/gms/usagereporting/a/t;->a:Lcom/google/android/gms/usagereporting/a/g;

    .line 224
    iput-object p2, p0, Lcom/google/android/gms/usagereporting/a/t;->b:Lcom/google/android/gms/common/api/m;

    .line 225
    iput-object p3, p0, Lcom/google/android/gms/usagereporting/a/t;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 226
    iput-object p4, p0, Lcom/google/android/gms/usagereporting/a/t;->d:Lcom/google/android/gms/usagereporting/a/u;

    .line 227
    return-void
.end method


# virtual methods
.method public final b(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 247
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/t;->c:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/t;->b:Lcom/google/android/gms/common/api/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 253
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/t;->b:Lcom/google/android/gms/common/api/m;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/t;->c:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 233
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/t;->b:Lcom/google/android/gms/common/api/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 243
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/t;->d:Lcom/google/android/gms/usagereporting/a/u;

    if-nez v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/t;->b:Lcom/google/android/gms/common/api/m;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/t;->c:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p0, Lcom/google/android/gms/usagereporting/a/t;->d:Lcom/google/android/gms/usagereporting/a/u;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/t;->a:Lcom/google/android/gms/usagereporting/a/g;

    iget-object v1, p0, Lcom/google/android/gms/usagereporting/a/t;->d:Lcom/google/android/gms/usagereporting/a/u;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/usagereporting/a/g;->a(Lcom/google/android/gms/usagereporting/a/d;Lcom/google/android/gms/usagereporting/a/a;)V

    goto :goto_0
.end method
