.class public final Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/plus/service/pos/m;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/pos/r;

.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final b:Ljava/util/Set;

.field private final c:I

.field private d:D

.field private e:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 518
    new-instance v0, Lcom/google/android/gms/plus/service/pos/r;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/pos/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->CREATOR:Lcom/google/android/gms/plus/service/pos/r;

    .line 533
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 536
    sput-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a:Ljava/util/HashMap;

    const-string v1, "count"

    const-string v2, "count"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->d(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a:Ljava/util/HashMap;

    const-string v1, "person"

    const-string v2, "person"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 570
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 571
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->c:I

    .line 572
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b:Ljava/util/Set;

    .line 573
    return-void
.end method

.method constructor <init>(Ljava/util/Set;IDLjava/util/List;)V
    .locals 1

    .prologue
    .line 581
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 582
    iput-object p1, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b:Ljava/util/Set;

    .line 583
    iput p2, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->c:I

    .line 584
    iput-wide p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->d:D

    .line 585
    iput-object p5, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->e:Ljava/util/List;

    .line 586
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 543
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;D)V
    .locals 5

    .prologue
    .line 1070
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1071
    packed-switch v0, :pswitch_data_0

    .line 1076
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a double."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1073
    :pswitch_0
    iput-wide p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->d:D

    .line 1079
    iget-object v1, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1080
    return-void

    .line 1071
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 1085
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1086
    packed-switch v0, :pswitch_data_0

    .line 1091
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1088
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->e:Ljava/util/List;

    .line 1095
    iget-object v1, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1096
    return-void

    .line 1086
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1033
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1039
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1035
    :pswitch_0
    iget-wide v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->d:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 1037
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->e:Ljava/util/List;

    goto :goto_0

    .line 1033
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1018
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 514
    return-object p0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1023
    const/4 v0, 0x0

    return v0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 620
    iget-wide v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->d:D

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1007
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->CREATOR:Lcom/google/android/gms/plus/service/pos/r;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->e:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1124
    instance-of v0, p1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 1155
    :goto_0
    return v0

    .line 1129
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 1130
    goto :goto_0

    .line 1133
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    .line 1134
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1135
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1136
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1138
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1140
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1145
    goto :goto_0

    .line 1148
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1150
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1155
    goto :goto_0
.end method

.method final f()Ljava/util/Set;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b:Ljava/util/Set;

    return-object v0
.end method

.method final g()I
    .locals 1

    .prologue
    .line 611
    iget v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->c:I

    return v0
.end method

.method final h()Ljava/util/List;
    .locals 1

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->e:Ljava/util/List;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1111
    const/4 v0, 0x0

    .line 1112
    sget-object v1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1113
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1114
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 1115
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 1117
    goto :goto_0

    .line 1118
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1012
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;->CREATOR:Lcom/google/android/gms/plus/service/pos/r;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/pos/r;->a(Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;Landroid/os/Parcel;)V

    .line 1013
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 1106
    const/4 v0, 0x1

    return v0
.end method
