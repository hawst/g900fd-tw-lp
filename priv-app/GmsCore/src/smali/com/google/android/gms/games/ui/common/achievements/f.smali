.class public final Lcom/google/android/gms/games/ui/common/achievements/f;
.super Lcom/google/android/gms/games/ui/cw;
.source "SourceFile"


# static fields
.field private static final c:I

.field private static final f:I


# instance fields
.field private h:I

.field private i:I

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/google/android/gms/l;->bQ:I

    sput v0, Lcom/google/android/gms/games/ui/common/achievements/f;->c:I

    .line 19
    sget v0, Lcom/google/android/gms/l;->bR:I

    sput v0, Lcom/google/android/gms/games/ui/common/achievements/f;->f:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cw;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->j:Z

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/achievements/f;)Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->j:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/achievements/f;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->h:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/achievements/f;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->i:I

    return v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/cx;
    .locals 4

    .prologue
    .line 50
    new-instance v1, Lcom/google/android/gms/games/ui/common/achievements/g;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->e:Landroid/view/LayoutInflater;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->j:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/games/ui/common/achievements/f;->c:I

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v2, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/common/achievements/g;-><init>(Landroid/view/View;)V

    return-object v1

    :cond_0
    sget v0, Lcom/google/android/gms/games/ui/common/achievements/f;->f:I

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 32
    sget v0, Lcom/google/android/gms/j;->gw:I

    return v0
.end method

.method public final e(II)V
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->h:I

    .line 43
    iput p2, p0, Lcom/google/android/gms/games/ui/common/achievements/f;->i:I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/f;->c()V

    .line 46
    return-void
.end method
