.class public final Lcom/google/android/gms/people/d/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/gms/people/d/e;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/android/gms/people/d/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 271
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 272
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/e;->cachedSize:I

    .line 273
    return-void
.end method

.method public static a()[Lcom/google/android/gms/people/d/e;
    .locals 2

    .prologue
    .line 251
    sget-object v0, Lcom/google/android/gms/people/d/e;->d:[Lcom/google/android/gms/people/d/e;

    if-nez v0, :cond_1

    .line 252
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 254
    :try_start_0
    sget-object v0, Lcom/google/android/gms/people/d/e;->d:[Lcom/google/android/gms/people/d/e;

    if-nez v0, :cond_0

    .line 255
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/people/d/e;

    sput-object v0, Lcom/google/android/gms/people/d/e;->d:[Lcom/google/android/gms/people/d/e;

    .line 257
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/d/e;->d:[Lcom/google/android/gms/people/d/e;

    return-object v0

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 347
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 348
    iget-object v1, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 349
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 352
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 353
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 356
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    if-eqz v1, :cond_2

    .line 357
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 360
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 285
    if-ne p1, p0, :cond_1

    .line 315
    :cond_0
    :goto_0
    return v0

    .line 288
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/e;

    if-nez v2, :cond_2

    move v0, v1

    .line 289
    goto :goto_0

    .line 291
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/e;

    .line 292
    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 293
    iget-object v2, p1, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 294
    goto :goto_0

    .line 296
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 297
    goto :goto_0

    .line 299
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 300
    iget-object v2, p1, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 301
    goto :goto_0

    .line 303
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 304
    goto :goto_0

    .line 306
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    if-nez v2, :cond_7

    .line 307
    iget-object v2, p1, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    if-eqz v2, :cond_0

    move v0, v1

    .line 308
    goto :goto_0

    .line 311
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    iget-object v3, p1, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 312
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 323
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 325
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 327
    return v0

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 325
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    invoke-virtual {v1}, Lcom/google/android/gms/people/d/f;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 245
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/d/f;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 337
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/people/d/e;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    if-eqz v0, :cond_2

    .line 340
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 342
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 343
    return-void
.end method
