.class public final Lcom/google/android/gms/fitness/b/c/ah;
.super Lcom/google/android/gms/fitness/b/c/ag;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/b/b;Lcom/google/android/gms/fitness/b/c/an;)V
    .locals 2

    .prologue
    .line 21
    const/4 v0, 0x1

    const-string v1, "overlay_user_input"

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/google/android/gms/fitness/b/c/ag;-><init>(ZLcom/google/android/gms/fitness/b/b;Lcom/google/android/gms/fitness/b/c/an;Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/o;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/gms/fitness/b/c/ai;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/fitness/b/c/ai;-><init>(Lcom/google/android/gms/fitness/b/c/ah;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/o;Z)Lcom/google/android/gms/fitness/b/u;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/gms/fitness/b/v;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/b/v;-><init>()V

    const-string v1, "com.google.activity.segment"

    iput-object v1, v0, Lcom/google/android/gms/fitness/b/v;->a:Ljava/lang/String;

    iput-object p0, v0, Lcom/google/android/gms/fitness/b/v;->b:Lcom/google/android/gms/fitness/b/o;

    iput-boolean p1, v0, Lcom/google/android/gms/fitness/b/v;->e:Z

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/b/v;->a()Lcom/google/android/gms/fitness/b/u;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/util/List;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    const-string v1, "user_input"

    invoke-direct {p0, v1}, Lcom/google/android/gms/fitness/b/c/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/o;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/b/c/ah;->a(Lcom/google/android/gms/fitness/b/o;Z)Lcom/google/android/gms/fitness/b/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    const-string v1, "session_activity_segment"

    invoke-direct {p0, v1}, Lcom/google/android/gms/fitness/b/c/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/o;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/b/c/ah;->a(Lcom/google/android/gms/fitness/b/o;Z)Lcom/google/android/gms/fitness/b/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    new-instance v1, Lcom/google/android/gms/fitness/b/c/aj;

    invoke-direct {v1, p0}, Lcom/google/android/gms/fitness/b/c/aj;-><init>(Lcom/google/android/gms/fitness/b/c/ah;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/b/c/ah;->a(Lcom/google/android/gms/fitness/b/o;Z)Lcom/google/android/gms/fitness/b/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    const-string v1, "default_activity_segments"

    invoke-direct {p0, v1}, Lcom/google/android/gms/fitness/b/c/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/o;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/b/c/ah;->a(Lcom/google/android/gms/fitness/b/o;Z)Lcom/google/android/gms/fitness/b/u;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    return-object v0
.end method
