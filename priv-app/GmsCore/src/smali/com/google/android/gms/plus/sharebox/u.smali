.class public final Lcom/google/android/gms/plus/sharebox/u;
.super Lcom/google/android/gms/plus/audience/e;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/google/android/gms/plus/audience/f;


# instance fields
.field private A:Lcom/google/android/gms/plus/sharebox/y;

.field private B:Lcom/google/android/gms/plus/audience/a/b;

.field private C:Lcom/google/android/gms/plus/sharebox/w;

.field private final r:Landroid/content/Context;

.field private final s:Landroid/support/v4/app/au;

.field private final t:Ljava/lang/String;

.field private final u:Ljava/lang/String;

.field private v:Landroid/widget/Filter;

.field private w:Ljava/lang/String;

.field private x:Lcom/google/android/gms/plus/audience/a/h;

.field private y:Lcom/google/android/gms/plus/sharebox/x;

.field private z:Lcom/google/android/gms/plus/audience/a/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/au;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/audience/bg;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 140
    move-object v0, p0

    move-object v1, p1

    move-object v2, p7

    move-object v3, p5

    move-object v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 129
    new-instance v0, Lcom/google/android/gms/plus/sharebox/x;

    invoke-direct {v0, p0, v5}, Lcom/google/android/gms/plus/sharebox/x;-><init>(Lcom/google/android/gms/plus/sharebox/u;B)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->y:Lcom/google/android/gms/plus/sharebox/x;

    .line 131
    new-instance v0, Lcom/google/android/gms/plus/sharebox/y;

    invoke-direct {v0, p0, v5}, Lcom/google/android/gms/plus/sharebox/y;-><init>(Lcom/google/android/gms/plus/sharebox/u;B)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->A:Lcom/google/android/gms/plus/sharebox/y;

    .line 134
    new-instance v0, Lcom/google/android/gms/plus/sharebox/w;

    invoke-direct {v0, p0, v5}, Lcom/google/android/gms/plus/sharebox/w;-><init>(Lcom/google/android/gms/plus/sharebox/u;B)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->C:Lcom/google/android/gms/plus/sharebox/w;

    .line 142
    iput-object p0, p0, Lcom/google/android/gms/plus/audience/e;->e:Lcom/google/android/gms/plus/audience/f;

    .line 143
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/u;->r:Landroid/content/Context;

    .line 144
    iput-object p2, p0, Lcom/google/android/gms/plus/sharebox/u;->s:Landroid/support/v4/app/au;

    .line 145
    iput-object p3, p0, Lcom/google/android/gms/plus/sharebox/u;->t:Ljava/lang/String;

    .line 146
    iput-object p4, p0, Lcom/google/android/gms/plus/sharebox/u;->u:Ljava/lang/String;

    .line 147
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/u;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/sharebox/u;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->r:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/sharebox/u;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/sharebox/u;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/sharebox/u;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/sharebox/u;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/sharebox/u;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/plus/sharebox/u;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final X_()V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->z:Lcom/google/android/gms/plus/audience/a/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/u;->w:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/audience/a/i;->a(Ljava/lang/String;I)Lcom/google/android/gms/plus/audience/a/i;

    .line 189
    return-void
.end method

.method protected final a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
    .locals 2

    .prologue
    .line 196
    invoke-super/range {p0 .. p12}, Lcom/google/android/gms/plus/audience/e;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    move-result-object v0

    .line 201
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->d(Z)V

    .line 202
    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->x:Lcom/google/android/gms/plus/audience/a/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->z:Lcom/google/android/gms/plus/audience/a/i;

    if-nez v0, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/u;->w:Ljava/lang/String;

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->B:Lcom/google/android/gms/plus/audience/a/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/u;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/a/b;->a(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->x:Lcom/google/android/gms/plus/audience/a/h;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/u;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/a/h;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/audience/a/e;

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->z:Lcom/google/android/gms/plus/audience/a/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/u;->w:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/audience/a/i;->a(Ljava/lang/String;I)Lcom/google/android/gms/plus/audience/a/i;

    goto :goto_0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->v:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 162
    new-instance v0, Lcom/google/android/gms/plus/sharebox/v;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/sharebox/v;-><init>(Lcom/google/android/gms/plus/sharebox/u;B)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->v:Landroid/widget/Filter;

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->v:Landroid/widget/Filter;

    return-object v0
.end method

.method public final j()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 169
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/e;->j()V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->s:Landroid/support/v4/app/au;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/u;->y:Lcom/google/android/gms/plus/sharebox/x;

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/a/h;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->x:Lcom/google/android/gms/plus/audience/a/h;

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->s:Landroid/support/v4/app/au;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/u;->A:Lcom/google/android/gms/plus/sharebox/y;

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/a/i;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->z:Lcom/google/android/gms/plus/audience/a/i;

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->s:Landroid/support/v4/app/au;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/u;->C:Lcom/google/android/gms/plus/sharebox/w;

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/a/b;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->B:Lcom/google/android/gms/plus/audience/a/b;

    .line 176
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->s:Landroid/support/v4/app/au;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/au;->a(I)V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->s:Landroid/support/v4/app/au;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/app/au;->a(I)V

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/u;->s:Landroid/support/v4/app/au;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/app/au;->a(I)V

    .line 183
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/e;->k()V

    .line 184
    return-void
.end method
