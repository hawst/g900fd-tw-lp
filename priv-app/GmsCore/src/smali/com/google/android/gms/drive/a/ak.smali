.class final Lcom/google/android/gms/drive/a/ak;
.super Lcom/google/android/gms/drive/a/b;
.source "SourceFile"


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:J

.field private final g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private final h:J

.field private final i:J


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JJJ)V
    .locals 9

    .prologue
    .line 54
    sget-object v3, Lcom/google/android/gms/drive/a/e;->e:Lcom/google/android/gms/drive/a/e;

    sget-object v7, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;)V

    .line 56
    iput-object p4, p0, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    .line 57
    iput-object p5, p0, Lcom/google/android/gms/drive/a/ak;->e:Ljava/lang/String;

    .line 58
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-object v2, p0, Lcom/google/android/gms/drive/a/ak;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 59
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/ak;->f:J

    .line 60
    move-wide/from16 v0, p9

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/ak;->h:J

    .line 61
    move-wide/from16 v0, p11

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/ak;->i:J

    .line 62
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    sget-object v0, Lcom/google/android/gms/drive/a/e;->d:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 67
    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/ak;->h:J

    .line 68
    const-string v0, "contentHash"

    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    .line 69
    const-string v0, "modifiedContentHash"

    invoke-virtual {p2, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/ak;->e:Ljava/lang/String;

    .line 70
    const-string v0, "writeOpenTime"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/ak;->f:J

    .line 71
    const-string v0, "metadataDelta"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/ak;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 73
    const-string v0, "fileSize"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/ak;->i:J

    .line 74
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/ak;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 4

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/google/android/gms/drive/a/ak;->h:J

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/drive/database/r;->b(J)Lcom/google/android/gms/drive/database/model/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bi;->j()V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->p()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/ak;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/database/model/ah;->i(Ljava/lang/String;)V

    .line 101
    iget-wide v0, p0, Lcom/google/android/gms/drive/a/ak;->i:J

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(J)V

    .line 113
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/ak;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/ak;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {p2, v0, v1}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 115
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->r()V

    .line 116
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 120
    new-instance v0, Lcom/google/android/gms/drive/a/z;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v3, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/a/z;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    return-object v0

    .line 107
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/database/model/ah;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 148
    if-ne p0, p1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return v0

    .line 151
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/a/ak;

    if-nez v2, :cond_2

    move v0, v1

    .line 152
    goto :goto_0

    .line 154
    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/a/ak;

    .line 155
    iget-object v2, p0, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/c/a/a/b/h/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ak;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/ak;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/c/a/a/b/h/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->i:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/a/ak;->i:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ak;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/ak;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->h:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/a/ak;->h:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->f:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/a/ak;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 78
    invoke-super {p0}, Lcom/google/android/gms/drive/a/b;->f()Lorg/json/JSONObject;

    move-result-object v0

    .line 79
    const-string v1, "pendingUploadSqlId"

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->h:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 80
    const-string v1, "contentHash"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 81
    const-string v1, "modifiedContentHash"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ak;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 82
    const-string v1, "writeOpenTime"

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 83
    const-string v1, "metadataDelta"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ak;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/a/ax;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 84
    const-string v1, "fileSize"

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->i:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 85
    return-object v0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 136
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/a/ak;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 138
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->i:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/a/ak;->i:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 139
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ak;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 141
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->h:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/a/ak;->h:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 142
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->f:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/a/ak;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 143
    return v0

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ak;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ak;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/drive/a/ak;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UndoContentAndMetadataAction [mWriteOpenTime="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMetadataChangeSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/ak;->g:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPendingUploadSqlId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFileSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/ak;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
