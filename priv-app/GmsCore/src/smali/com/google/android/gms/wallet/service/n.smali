.class public abstract Lcom/google/android/gms/wallet/service/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/n;->a:Landroid/accounts/Account;

    .line 34
    return-void
.end method

.method private static a([I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 95
    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    array-length v2, p0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    .line 99
    sparse-switch v3, :sswitch_data_0

    .line 98
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 102
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 99
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x9 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/google/android/gms/wallet/a/b;
    .locals 3

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/n;->a()Ljava/lang/String;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/n;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 44
    new-instance v2, Lcom/google/android/gms/wallet/a/b;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/wallet/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public abstract a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)Z
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 72
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 50
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/n;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/n;->a([I)Z

    move-result v0

    goto :goto_0

    .line 56
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/p;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/n;->a([I)Z

    move-result v0

    goto :goto_0

    .line 60
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/l;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/n;->a([I)Z

    move-result v0

    goto :goto_0

    .line 64
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/j;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/n;->a([I)Z

    move-result v0

    goto :goto_0

    .line 68
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/h;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/h;->a:[I

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/n;->a([I)Z

    move-result v0

    goto :goto_0

    .line 48
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xd -> :sswitch_1
        0xe -> :sswitch_3
        0xf -> :sswitch_2
        0x10 -> :sswitch_4
        0x1e -> :sswitch_5
    .end sparse-switch
.end method
