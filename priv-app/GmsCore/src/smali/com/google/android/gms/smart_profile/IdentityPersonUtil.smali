.class public Lcom/google/android/gms/smart_profile/IdentityPersonUtil;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# static fields
.field private static final b:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;


# instance fields
.field a:Ljava/util/List;

.field private c:Landroid/content/Context;

.field private d:Ljava/lang/ref/WeakReference;

.field private e:Ljava/lang/ref/WeakReference;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Z

.field private n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

.field private o:Z

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Ljava/util/Map;

.field private s:Ljava/util/Map;

.field private t:Lcom/google/android/gms/common/api/v;

.field private u:Lcom/google/android/gms/smart_profile/ae;

.field private final v:Lcom/google/android/gms/smart_profile/be;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;-><init>()V

    const-string v1, "starred"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;->g(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->b:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->c:Landroid/content/Context;

    .line 331
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    .line 332
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f:Ljava/lang/String;

    .line 333
    iput-object p4, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->h:Ljava/lang/String;

    .line 334
    iput-object p5, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i:Ljava/lang/String;

    .line 335
    iput p6, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->j:I

    .line 336
    new-instance v0, Lcom/google/android/gms/smart_profile/be;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/be;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->v:Lcom/google/android/gms/smart_profile/be;

    .line 337
    return-void
.end method

.method private A()V
    .locals 1

    .prologue
    .line 758
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/ac;

    .line 760
    :goto_0
    if-eqz v0, :cond_0

    .line 761
    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/ac;->aj_()V

    .line 763
    :cond_0
    return-void

    .line 758
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Lcom/google/android/gms/people/identity/models/m;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 660
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 672
    :cond_1
    :goto_0
    return-object v0

    .line 663
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 664
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    .line 665
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/m;

    .line 666
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/m;->e()Lcom/google/android/gms/people/identity/models/l;

    move-result-object v4

    .line 668
    if-eqz v4, :cond_3

    invoke-interface {v4}, Lcom/google/android/gms/people/identity/models/l;->e()Z

    move-result v4

    if-nez v4, :cond_1

    .line 664
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 672
    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Lcom/google/android/gms/smart_profile/SmartProfilePerson;)Lcom/google/android/gms/smart_profile/SmartProfilePerson;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->r:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Z)Z
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->o:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->k:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/util/List;)Ljava/util/List;
    .locals 4

    .prologue
    .line 806
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 807
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 808
    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 810
    :cond_0
    return-object v1
.end method

.method static synthetic b(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->s:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->v()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->q:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->w()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->p:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->r:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Lcom/google/android/gms/smart_profile/SmartProfilePerson;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->l:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->x()V

    return-void
.end method

.method static synthetic n(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->s:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->m:Z

    return v0
.end method

.method static synthetic p(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->A()V

    return-void
.end method

.method static synthetic q(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->z()V

    return-void
.end method

.method static synthetic r(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->l:Z

    return v0
.end method

.method static synthetic s(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u:Lcom/google/android/gms/smart_profile/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u:Lcom/google/android/gms/smart_profile/ae;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/x;)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u:Lcom/google/android/gms/smart_profile/ae;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/y;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u:Lcom/google/android/gms/smart_profile/ae;

    :cond_0
    return-void
.end method

.method private v()V
    .locals 3

    .prologue
    .line 384
    new-instance v0, Lcom/google/android/gms/people/f;

    invoke-direct {v0}, Lcom/google/android/gms/people/f;-><init>()V

    .line 385
    invoke-virtual {v0}, Lcom/google/android/gms/people/f;->b()Lcom/google/android/gms/people/f;

    .line 386
    sget-object v1, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/f;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    .line 388
    new-instance v1, Lcom/google/android/gms/smart_profile/ah;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/smart_profile/ah;-><init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 389
    return-void
.end method

.method private w()V
    .locals 5

    .prologue
    .line 392
    new-instance v0, Lcom/google/android/gms/people/d;

    invoke-direct {v0}, Lcom/google/android/gms/people/d;-><init>()V

    .line 393
    sget-object v1, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->h:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    .line 395
    new-instance v1, Lcom/google/android/gms/smart_profile/ag;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/smart_profile/ag;-><init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 396
    return-void
.end method

.method private x()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 399
    new-instance v3, Lcom/google/android/gms/people/identity/f;

    invoke-direct {v3}, Lcom/google/android/gms/people/identity/f;-><init>()V

    iput-boolean v1, v3, Lcom/google/android/gms/people/identity/f;->b:Z

    iput-boolean v1, v3, Lcom/google/android/gms/people/identity/f;->d:Z

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/google/android/gms/people/identity/f;->c:Z

    new-instance v0, Lcom/google/android/gms/people/identity/d;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/d;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/people/identity/d;->a:Ljava/lang/String;

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->h:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/people/identity/d;->b:Ljava/lang/String;

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/d;->a()Lcom/google/android/gms/people/identity/c;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/c;

    iput-object v0, v3, Lcom/google/android/gms/people/identity/f;->a:Lcom/google/android/gms/people/identity/c;

    new-instance v0, Lcom/google/android/gms/people/identity/e;

    invoke-direct {v0, v3, v2}, Lcom/google/android/gms/people/identity/e;-><init>(Lcom/google/android/gms/people/identity/f;B)V

    .line 405
    sget-object v3, Lcom/google/android/gms/people/x;->d:Lcom/google/android/gms/people/identity/a;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->v:Lcom/google/android/gms/smart_profile/be;

    new-array v1, v1, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i:Ljava/lang/String;

    aput-object v6, v1, v2

    invoke-interface {v3, v4, v0, v5, v1}, Lcom/google/android/gms/people/identity/a;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/identity/e;Lcom/google/android/gms/people/identity/g;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    .line 408
    new-instance v1, Lcom/google/android/gms/smart_profile/ai;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/smart_profile/ai;-><init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 409
    return-void

    :cond_1
    move v0, v2

    .line 399
    goto :goto_0

    :cond_2
    const-string v4, ""

    iput-object v4, v0, Lcom/google/android/gms/people/identity/d;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method private y()I
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->q()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    move v1, v2

    .line 535
    :cond_1
    :goto_0
    return v1

    .line 528
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->q()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 529
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->q()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/k;

    .line 530
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->f()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "starred"

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/k;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 528
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    move v1, v2

    .line 535
    goto :goto_0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 750
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/ad;

    .line 752
    :goto_0
    if-eqz v0, :cond_0

    .line 753
    iget-boolean v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->l:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/smart_profile/ad;->a(Z)V

    .line 755
    :cond_0
    return-void

    .line 750
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->r:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->r:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->l:Z

    if-eqz v0, :cond_0

    .line 344
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->z()V

    .line 366
    :goto_0
    return-void

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 347
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->x()V

    goto :goto_0

    .line 350
    :cond_1
    new-instance v0, Lcom/google/android/gms/smart_profile/ae;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/smart_profile/ae;-><init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;B)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u:Lcom/google/android/gms/smart_profile/ae;

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u:Lcom/google/android/gms/smart_profile/ae;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u:Lcom/google/android/gms/smart_profile/ae;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 355
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 356
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->v()V

    goto :goto_0

    .line 357
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->r:Ljava/util/Map;

    if-nez v0, :cond_3

    .line 358
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->w()V

    goto :goto_0

    .line 360
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->x()V

    goto :goto_0

    .line 363
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 769
    if-nez p1, :cond_1

    .line 776
    :cond_0
    :goto_0
    return-void

    .line 772
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/common/audience/a/i;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/audience/a/j;

    move-result-object v0

    .line 773
    if-eqz v0, :cond_0

    .line 774
    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/j;->i()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a:Ljava/util/List;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/smart_profile/ac;)V
    .locals 1

    .prologue
    .line 746
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->e:Ljava/lang/ref/WeakReference;

    .line 747
    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/ad;)V
    .locals 1

    .prologue
    .line 737
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->d:Ljava/lang/ref/WeakReference;

    .line 738
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 503
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->y()I

    move-result v0

    .line 507
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    if-eqz v1, :cond_1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-ltz v0, :cond_2

    if-eqz p1, :cond_2

    .line 518
    :cond_1
    :goto_0
    return-void

    .line 513
    :cond_2
    if-eqz p1, :cond_3

    .line 514
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    sget-object v1, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->b:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    goto :goto_0

    .line 516
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->q()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->m:Z

    if-eqz v0, :cond_0

    .line 374
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->A()V

    .line 381
    :goto_0
    return-void

    .line 376
    :cond_0
    new-instance v0, Lcom/google/android/gms/people/h;

    invoke-direct {v0}, Lcom/google/android/gms/people/h;-><init>()V

    .line 377
    sget-object v1, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->h:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/h;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    .line 379
    new-instance v1, Lcom/google/android/gms/smart_profile/af;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/smart_profile/af;-><init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 782
    if-nez p1, :cond_1

    .line 792
    :cond_0
    :goto_0
    return-void

    .line 785
    :cond_1
    const-string v0, "EXTRA_TARGET_CIRCLE_ID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 787
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 788
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 789
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 790
    invoke-direct {p0, v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a:Ljava/util/List;

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->r:Ljava/util/Map;

    return-object v0
.end method

.method public final e()Ljava/util/Map;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->s:Ljava/util/Map;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/smart_profile/SmartProfilePerson;
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    return-object v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->y()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 544
    const/4 v0, 0x0

    .line 545
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 546
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 549
    :cond_0
    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 556
    iget v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->j:I

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 564
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    .line 565
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->v()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 581
    :cond_1
    :goto_0
    return-object v0

    .line 568
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->u()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Ljava/util/List;)Lcom/google/android/gms/people/identity/models/m;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/n;

    .line 569
    if-eqz v0, :cond_3

    .line 570
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/n;->b()Ljava/lang/String;

    move-result-object v0

    .line 571
    if-nez v0, :cond_1

    .line 575
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/n;

    .line 576
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/n;->b()Ljava/lang/String;

    move-result-object v0

    .line 577
    if-eqz v0, :cond_4

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 581
    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 589
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    .line 590
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->n()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 606
    :cond_1
    :goto_0
    return-object v0

    .line 593
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->m()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Ljava/util/List;)Lcom/google/android/gms/people/identity/models/m;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/i;

    .line 594
    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/i;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/i;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 595
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/i;->f()Ljava/lang/String;

    move-result-object v0

    .line 596
    if-nez v0, :cond_1

    .line 600
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;

    .line 601
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;->f()Ljava/lang/String;

    move-result-object v0

    .line 602
    if-eqz v0, :cond_4

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 606
    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 614
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    .line 615
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->f()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 619
    :goto_0
    return-object v0

    .line 618
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->e()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/e;

    .line 619
    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/e;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/e;->b()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/e;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public final n()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 628
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    .line 629
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->v()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 643
    :goto_0
    return v0

    .line 632
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->u()Ljava/util/List;

    move-result-object v3

    .line 633
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    .line 634
    :goto_1
    if-ge v2, v4, :cond_3

    .line 635
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/n;

    .line 636
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/n;->e()Lcom/google/android/gms/people/identity/models/l;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/n;->e()Lcom/google/android/gms/people/identity/models/l;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/people/identity/models/l;->g()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 637
    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/n;->e()Lcom/google/android/gms/people/identity/models/l;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/l;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 640
    const/4 v0, 0x1

    goto :goto_0

    .line 634
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 643
    goto :goto_0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 650
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    .line 651
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->t()Z

    move-result v1

    if-nez v1, :cond_1

    .line 652
    :cond_0
    const/4 v0, 0x0

    .line 654
    :goto_0
    return v0

    :cond_1
    const-string v1, "page"

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/r;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 681
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->p:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 690
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->o:Z

    return v0
.end method

.method public final r()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 699
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    .line 700
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->t()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/people/identity/models/r;->h()Z

    move-result v2

    if-nez v2, :cond_1

    .line 704
    :cond_0
    :goto_0
    return v0

    .line 703
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/people/identity/models/r;->g()Ljava/util/List;

    move-result-object v1

    .line 704
    const-string v2, "googlePlus"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "googlePlusDisabledByAdmin"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 715
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/r;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 726
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u()Ljava/util/List;

    move-result-object v0

    .line 727
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->q:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Ljava/util/List;
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/r;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n:Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->s()Lcom/google/android/gms/people/identity/models/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/r;->c()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 802
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a:Ljava/util/List;

    goto :goto_0
.end method
