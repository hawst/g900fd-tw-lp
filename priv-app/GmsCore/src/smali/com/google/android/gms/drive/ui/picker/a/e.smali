.class public final Lcom/google/android/gms/drive/ui/picker/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Z

.field final b:Z

.field final c:Z

.field final d:Lcom/google/android/gms/drive/ui/picker/view/i;

.field final e:Lcom/google/android/gms/drive/ui/picker/a/a/g;

.field final f:Lcom/google/android/gms/drive/g/av;

.field final g:I

.field h:Landroid/widget/SectionIndexer;

.field final i:Lcom/google/android/gms/drive/ui/picker/a/t;

.field j:Lcom/google/android/gms/drive/ui/picker/a/l;

.field private final k:Lcom/google/android/gms/drive/ui/picker/a/s;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/ui/picker/a/l;Lcom/google/android/gms/drive/ui/picker/a/a/g;Lcom/google/android/gms/drive/ui/picker/a/q;Lcom/google/android/gms/drive/ui/picker/view/i;Lcom/google/android/gms/drive/g/av;Lcom/google/android/gms/drive/ui/picker/a/t;Lcom/google/android/gms/drive/ui/picker/a/s;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->d:Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-virtual {p3, v0}, Lcom/google/android/gms/drive/ui/picker/a/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->a:Z

    .line 74
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->c:Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-virtual {p3, v0}, Lcom/google/android/gms/drive/ui/picker/a/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->b:Z

    .line 75
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->b:Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-virtual {p3, v0}, Lcom/google/android/gms/drive/ui/picker/a/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->c:Z

    .line 76
    iput-object p4, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->d:Lcom/google/android/gms/drive/ui/picker/view/i;

    .line 77
    iput-object p2, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->e:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    .line 78
    iput-object p5, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->f:Lcom/google/android/gms/drive/g/av;

    .line 79
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/t;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->i:Lcom/google/android/gms/drive/ui/picker/a/t;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->e:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/a/g;->d()Lcom/google/android/gms/drive/ui/picker/a/a/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/g;->a:[I

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/a/a;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/gms/p;->fV:I

    :goto_0
    iput v0, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->g:I

    .line 81
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/s;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->k:Lcom/google/android/gms/drive/ui/picker/a/s;

    .line 82
    invoke-virtual {p2}, Lcom/google/android/gms/drive/ui/picker/a/a/g;->c()Landroid/widget/SectionIndexer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->h:Landroid/widget/SectionIndexer;

    .line 83
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 84
    return-void

    .line 80
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->fV:I

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->fX:I

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/gms/p;->fU:I

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/google/android/gms/p;->fY:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static a(Lcom/google/android/gms/drive/ui/picker/a/h;Landroid/widget/ImageView;IZLjava/util/List;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->m:Z

    if-nez v0, :cond_2

    .line 301
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 302
    sget v1, Lcom/google/android/gms/f;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 304
    if-eqz p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 309
    :cond_0
    :goto_1
    return-void

    .line 304
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 305
    :cond_2
    if-eqz p3, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->a:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/h;->a:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method final a(Lcom/google/android/gms/drive/ui/picker/a/l;Lcom/google/android/gms/drive/ui/picker/a/h;Lcom/google/android/gms/drive/DriveId;)V
    .locals 3

    .prologue
    .line 338
    invoke-interface {p1}, Lcom/google/android/gms/drive/ui/picker/a/l;->j()I

    move-result v0

    .line 339
    iget-object v1, p2, Lcom/google/android/gms/drive/ui/picker/a/h;->l:Ljava/util/List;

    .line 340
    new-instance v2, Lcom/google/android/gms/drive/ui/picker/a/f;

    invoke-direct {v2, p0, v0, p3}, Lcom/google/android/gms/drive/ui/picker/a/f;-><init>(Lcom/google/android/gms/drive/ui/picker/a/e;ILcom/google/android/gms/drive/DriveId;)V

    .line 346
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 347
    if-eqz v0, :cond_0

    .line 348
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 351
    :cond_1
    return-void
.end method

.method final a()Z
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->b()Ljava/lang/String;

    move-result-object v0

    .line 355
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/e;->k:Lcom/google/android/gms/drive/ui/picker/a/s;

    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/ui/picker/a/s;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
