.class public Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/text/SimpleDateFormat;


# instance fields
.field private volatile b:Ljava/util/concurrent/atomic/AtomicReference;

.field private c:Ljava/util/concurrent/ConcurrentHashMap;

.field private d:Lcom/google/android/gms/wearable/node/bluetooth/b;

.field private e:Lcom/google/android/gms/wearable/node/bluetooth/c;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ssZ"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->a:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 43
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 221
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 146
    const/4 v0, 0x0

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/bluetooth/a;

    .line 148
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 151
    goto :goto_0

    .line 153
    :cond_0
    if-lez v1, :cond_1

    .line 154
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wearable/node/a/b;->a(Landroid/app/Service;)V

    .line 158
    :goto_2
    return-void

    .line 156
    :cond_1
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;)V
    .locals 2

    .prologue
    .line 34
    const-string v0, "WearableBluetooth"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WearableBluetooth"

    const-string v1, "onBluetoothStateChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/bluetooth/a;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/a;->b()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;Landroid/bluetooth/BluetoothDevice;)V
    .locals 3

    .prologue
    .line 34
    const-string v0, "WearableBluetooth"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WearableBluetooth"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onBluetoothDeviceBondStateChanged for device:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/bluetooth/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/a;->b()V

    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->a()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 34
    const-string v0, "WearableBluetooth"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "WearableBluetooth"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", received connection retry intent  for device address "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/bluetooth/a;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/wearable/node/bluetooth/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/wearable/node/bluetooth/a;->b:Lcom/google/android/gms/wearable/node/bluetooth/d;

    if-nez v2, :cond_1

    const-string v2, "WearableBluetooth"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received connection retry alarm but ignoring because the connection was stopped by the user; device: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/wearable/node/bluetooth/a;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/wearable/node/bluetooth/a;->b:Lcom/google/android/gms/wearable/node/bluetooth/d;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->b()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const-string v0, "WearableBluetooth"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received connection retry alarm but the device is no longer configured; address: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 132
    if-eqz v0, :cond_0

    .line 133
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "First started: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/bluetooth/a;

    .line 137
    iget-object v2, v0, Lcom/google/android/gms/wearable/node/bluetooth/a;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "-- Connection : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/google/android/gms/wearable/node/bluetooth/a;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v4}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v3, "Config: "

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/wearable/node/bluetooth/a;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "---- bt connection health ----"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/wearable/node/bluetooth/a;->d:Lcom/google/android/gms/wearable/node/bluetooth/h;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/io/PrintWriter;)V

    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 139
    :cond_1
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 54
    const-string v0, "WearableBluetooth"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const-string v0, "WearableBluetooth"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wearable/service/y;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 71
    :goto_0
    return-void

    .line 65
    :cond_1
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    .line 67
    new-instance v0, Lcom/google/android/gms/wearable/node/bluetooth/b;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/wearable/node/bluetooth/b;-><init>(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;B)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->d:Lcom/google/android/gms/wearable/node/bluetooth/b;

    .line 68
    new-instance v0, Lcom/google/android/gms/wearable/node/bluetooth/c;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/wearable/node/bluetooth/c;-><init>(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;B)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->e:Lcom/google/android/gms/wearable/node/bluetooth/c;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->d:Lcom/google/android/gms/wearable/node/bluetooth/b;

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->d:Lcom/google/android/gms/wearable/node/bluetooth/b;

    invoke-static {}, Lcom/google/android/gms/wearable/node/bluetooth/b;->a()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->e:Lcom/google/android/gms/wearable/node/bluetooth/c;

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->e:Lcom/google/android/gms/wearable/node/bluetooth/c;

    invoke-static {}, Lcom/google/android/gms/wearable/node/bluetooth/c;->a()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 75
    const-string v0, "WearableBluetooth"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const-string v0, "WearableBluetooth"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->d:Lcom/google/android/gms/wearable/node/bluetooth/b;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->e:Lcom/google/android/gms/wearable/node/bluetooth/c;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/bluetooth/a;

    .line 81
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/a;->c()V

    goto :goto_0

    .line 83
    :cond_1
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    .line 84
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 85
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 94
    const-string v0, "WearableBluetooth"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "WearableBluetooth"

    const-string v1, "onStartCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 101
    const-string v0, "connection_config"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/ConnectionConfiguration;

    .line 103
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->c()I

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->d()I

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 107
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid config: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 110
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/node/bluetooth/a;

    .line 111
    const-string v2, "connection_remove"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 113
    if-eqz v1, :cond_3

    .line 114
    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/bluetooth/a;->c()V

    .line 115
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->a()V

    .line 126
    return v4

    .line 119
    :cond_4
    if-nez v1, :cond_5

    .line 120
    new-instance v1, Lcom/google/android/gms/wearable/node/bluetooth/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wearable/node/bluetooth/a;-><init>(Landroid/content/Context;)V

    .line 121
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    :cond_5
    iget-object v2, v1, Lcom/google/android/gms/wearable/node/bluetooth/a;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/ConnectionConfiguration;

    iput-object v0, v1, Lcom/google/android/gms/wearable/node/bluetooth/a;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/bluetooth/a;->c()V

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/bluetooth/a;->b()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
