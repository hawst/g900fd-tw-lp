.class public final Lcom/google/android/gms/games/g/ag;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public D:Z

.field public E:I

.field public F:Lcom/google/android/gms/games/g/ah;

.field public a:Ljava/lang/String;

.field public b:I

.field public c:J

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public i:I

.field public j:J

.field public k:J

.field public l:I

.field public m:Z

.field public n:J

.field public o:J

.field public p:Z

.field public q:I

.field public r:I

.field public s:J

.field public t:J

.field public u:I

.field public v:J

.field public w:J

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 7272
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7273
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->b:I

    iput-wide v2, p0, Lcom/google/android/gms/games/g/ag;->c:J

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->g:I

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->h:I

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->i:I

    iput-wide v2, p0, Lcom/google/android/gms/games/g/ag;->j:J

    iput-wide v2, p0, Lcom/google/android/gms/games/g/ag;->k:J

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->l:I

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->m:Z

    iput-wide v2, p0, Lcom/google/android/gms/games/g/ag;->n:J

    iput-wide v2, p0, Lcom/google/android/gms/games/g/ag;->o:J

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->p:Z

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->q:I

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->r:I

    iput-wide v2, p0, Lcom/google/android/gms/games/g/ag;->s:J

    iput-wide v2, p0, Lcom/google/android/gms/games/g/ag;->t:J

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->u:I

    iput-wide v2, p0, Lcom/google/android/gms/games/g/ag;->v:J

    iput-wide v2, p0, Lcom/google/android/gms/games/g/ag;->w:J

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->x:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->y:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->z:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->A:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->B:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->C:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->D:Z

    iput v1, p0, Lcom/google/android/gms/games/g/ag;->E:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/ag;->cachedSize:I

    .line 7274
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 7594
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7595
    iget-object v1, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 7596
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7599
    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->b:I

    if-eqz v1, :cond_1

    .line 7600
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7603
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 7604
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7607
    :cond_2
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->d:I

    if-eqz v1, :cond_3

    .line 7608
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7611
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 7612
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7615
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 7616
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7619
    :cond_5
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->g:I

    if-eqz v1, :cond_6

    .line 7620
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7623
    :cond_6
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->h:I

    if-eqz v1, :cond_7

    .line 7624
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7627
    :cond_7
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->i:I

    if-eqz v1, :cond_8

    .line 7628
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7631
    :cond_8
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->j:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 7632
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->j:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7635
    :cond_9
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->k:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_a

    .line 7636
    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->k:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7639
    :cond_a
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->l:I

    if-eqz v1, :cond_b

    .line 7640
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7643
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->m:Z

    if-eqz v1, :cond_c

    .line 7644
    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->m:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7647
    :cond_c
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->n:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_d

    .line 7648
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->n:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7651
    :cond_d
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->o:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_e

    .line 7652
    const/16 v1, 0xf

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->o:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7655
    :cond_e
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->p:Z

    if-eqz v1, :cond_f

    .line 7656
    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->p:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7659
    :cond_f
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->q:I

    if-eqz v1, :cond_10

    .line 7660
    const/16 v1, 0x11

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->q:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7663
    :cond_10
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->r:I

    if-eqz v1, :cond_11

    .line 7664
    const/16 v1, 0x12

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->r:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7667
    :cond_11
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->s:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_12

    .line 7668
    const/16 v1, 0x13

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->s:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7671
    :cond_12
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->t:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_13

    .line 7672
    const/16 v1, 0x14

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->t:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7675
    :cond_13
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->u:I

    if-eqz v1, :cond_14

    .line 7676
    const/16 v1, 0x15

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->u:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7679
    :cond_14
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->v:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_15

    .line 7680
    const/16 v1, 0x16

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->v:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7683
    :cond_15
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->w:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_16

    .line 7684
    const/16 v1, 0x17

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->w:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7687
    :cond_16
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->x:Z

    if-eqz v1, :cond_17

    .line 7688
    const/16 v1, 0x18

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->x:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7691
    :cond_17
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->y:Z

    if-eqz v1, :cond_18

    .line 7692
    const/16 v1, 0x19

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->y:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7695
    :cond_18
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->z:Z

    if-eqz v1, :cond_19

    .line 7696
    const/16 v1, 0x1a

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->z:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7699
    :cond_19
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->A:Z

    if-eqz v1, :cond_1a

    .line 7700
    const/16 v1, 0x1b

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->A:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7703
    :cond_1a
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->B:Z

    if-eqz v1, :cond_1b

    .line 7704
    const/16 v1, 0x1c

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->B:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7707
    :cond_1b
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->C:Z

    if-eqz v1, :cond_1c

    .line 7708
    const/16 v1, 0x1d

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->C:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7711
    :cond_1c
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->D:Z

    if-eqz v1, :cond_1d

    .line 7712
    const/16 v1, 0x1e

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->D:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7715
    :cond_1d
    iget v1, p0, Lcom/google/android/gms/games/g/ag;->E:I

    if-eqz v1, :cond_1e

    .line 7716
    const/16 v1, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->E:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7719
    :cond_1e
    iget-object v1, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    if-eqz v1, :cond_1f

    .line 7720
    const/16 v1, 0x20

    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7723
    :cond_1f
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7315
    if-ne p1, p0, :cond_1

    .line 7436
    :cond_0
    :goto_0
    return v0

    .line 7318
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/ag;

    if-nez v2, :cond_2

    move v0, v1

    .line 7319
    goto :goto_0

    .line 7321
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/ag;

    .line 7322
    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 7323
    iget-object v2, p1, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 7324
    goto :goto_0

    .line 7326
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 7327
    goto :goto_0

    .line 7329
    :cond_4
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->b:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 7330
    goto :goto_0

    .line 7332
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/ag;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 7333
    goto :goto_0

    .line 7335
    :cond_6
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->d:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 7336
    goto :goto_0

    .line 7338
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 7339
    iget-object v2, p1, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 7340
    goto :goto_0

    .line 7342
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 7343
    goto :goto_0

    .line 7345
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 7346
    iget-object v2, p1, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 7347
    goto :goto_0

    .line 7349
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 7350
    goto :goto_0

    .line 7352
    :cond_b
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->g:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->g:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 7353
    goto :goto_0

    .line 7355
    :cond_c
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->h:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->h:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 7356
    goto :goto_0

    .line 7358
    :cond_d
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->i:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->i:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 7359
    goto :goto_0

    .line 7361
    :cond_e
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->j:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/ag;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    move v0, v1

    .line 7362
    goto/16 :goto_0

    .line 7364
    :cond_f
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->k:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/ag;->k:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    move v0, v1

    .line 7365
    goto/16 :goto_0

    .line 7367
    :cond_10
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->l:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->l:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 7368
    goto/16 :goto_0

    .line 7370
    :cond_11
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->m:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ag;->m:Z

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 7371
    goto/16 :goto_0

    .line 7373
    :cond_12
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->n:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/ag;->n:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_13

    move v0, v1

    .line 7374
    goto/16 :goto_0

    .line 7376
    :cond_13
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->o:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/ag;->o:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_14

    move v0, v1

    .line 7377
    goto/16 :goto_0

    .line 7379
    :cond_14
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->p:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ag;->p:Z

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 7380
    goto/16 :goto_0

    .line 7382
    :cond_15
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->q:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->q:I

    if-eq v2, v3, :cond_16

    move v0, v1

    .line 7383
    goto/16 :goto_0

    .line 7385
    :cond_16
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->r:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->r:I

    if-eq v2, v3, :cond_17

    move v0, v1

    .line 7386
    goto/16 :goto_0

    .line 7388
    :cond_17
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->s:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/ag;->s:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_18

    move v0, v1

    .line 7389
    goto/16 :goto_0

    .line 7391
    :cond_18
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->t:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/ag;->t:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_19

    move v0, v1

    .line 7392
    goto/16 :goto_0

    .line 7394
    :cond_19
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->u:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->u:I

    if-eq v2, v3, :cond_1a

    move v0, v1

    .line 7395
    goto/16 :goto_0

    .line 7397
    :cond_1a
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->v:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/ag;->v:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1b

    move v0, v1

    .line 7398
    goto/16 :goto_0

    .line 7400
    :cond_1b
    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->w:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/ag;->w:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1c

    move v0, v1

    .line 7401
    goto/16 :goto_0

    .line 7403
    :cond_1c
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->x:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ag;->x:Z

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 7404
    goto/16 :goto_0

    .line 7406
    :cond_1d
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->y:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ag;->y:Z

    if-eq v2, v3, :cond_1e

    move v0, v1

    .line 7407
    goto/16 :goto_0

    .line 7409
    :cond_1e
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->z:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ag;->z:Z

    if-eq v2, v3, :cond_1f

    move v0, v1

    .line 7410
    goto/16 :goto_0

    .line 7412
    :cond_1f
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->A:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ag;->A:Z

    if-eq v2, v3, :cond_20

    move v0, v1

    .line 7413
    goto/16 :goto_0

    .line 7415
    :cond_20
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->B:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ag;->B:Z

    if-eq v2, v3, :cond_21

    move v0, v1

    .line 7416
    goto/16 :goto_0

    .line 7418
    :cond_21
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->C:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ag;->C:Z

    if-eq v2, v3, :cond_22

    move v0, v1

    .line 7419
    goto/16 :goto_0

    .line 7421
    :cond_22
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/ag;->D:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/ag;->D:Z

    if-eq v2, v3, :cond_23

    move v0, v1

    .line 7422
    goto/16 :goto_0

    .line 7424
    :cond_23
    iget v2, p0, Lcom/google/android/gms/games/g/ag;->E:I

    iget v3, p1, Lcom/google/android/gms/games/g/ag;->E:I

    if-eq v2, v3, :cond_24

    move v0, v1

    .line 7425
    goto/16 :goto_0

    .line 7427
    :cond_24
    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    if-nez v2, :cond_25

    .line 7428
    iget-object v2, p1, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    if-eqz v2, :cond_0

    move v0, v1

    .line 7429
    goto/16 :goto_0

    .line 7432
    :cond_25
    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/ah;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 7433
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/16 v8, 0x20

    .line 7441
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 7444
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/g/ag;->b:I

    add-int/2addr v0, v4

    .line 7445
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/g/ag;->c:J

    iget-wide v6, p0, Lcom/google/android/gms/games/g/ag;->c:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7447
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/g/ag;->d:I

    add-int/2addr v0, v4

    .line 7448
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 7450
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 7452
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/g/ag;->g:I

    add-int/2addr v0, v4

    .line 7453
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/g/ag;->h:I

    add-int/2addr v0, v4

    .line 7454
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/g/ag;->i:I

    add-int/2addr v0, v4

    .line 7455
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/g/ag;->j:J

    iget-wide v6, p0, Lcom/google/android/gms/games/g/ag;->j:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7457
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/g/ag;->k:J

    iget-wide v6, p0, Lcom/google/android/gms/games/g/ag;->k:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7459
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/g/ag;->l:I

    add-int/2addr v0, v4

    .line 7460
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->m:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v4

    .line 7461
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/g/ag;->n:J

    iget-wide v6, p0, Lcom/google/android/gms/games/g/ag;->n:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7463
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/g/ag;->o:J

    iget-wide v6, p0, Lcom/google/android/gms/games/g/ag;->o:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7465
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->p:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    .line 7466
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/g/ag;->q:I

    add-int/2addr v0, v4

    .line 7467
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/g/ag;->r:I

    add-int/2addr v0, v4

    .line 7468
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/g/ag;->s:J

    iget-wide v6, p0, Lcom/google/android/gms/games/g/ag;->s:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7470
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/g/ag;->t:J

    iget-wide v6, p0, Lcom/google/android/gms/games/g/ag;->t:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7472
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/games/g/ag;->u:I

    add-int/2addr v0, v4

    .line 7473
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/g/ag;->v:J

    iget-wide v6, p0, Lcom/google/android/gms/games/g/ag;->v:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7475
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/games/g/ag;->w:J

    iget-wide v6, p0, Lcom/google/android/gms/games/g/ag;->w:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 7477
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->x:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v4

    .line 7478
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->y:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v4

    .line 7479
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->z:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    .line 7480
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->A:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_8
    add-int/2addr v0, v4

    .line 7481
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->B:Z

    if-eqz v0, :cond_9

    move v0, v2

    :goto_9
    add-int/2addr v0, v4

    .line 7482
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->C:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_a
    add-int/2addr v0, v4

    .line 7483
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/android/gms/games/g/ag;->D:Z

    if-eqz v4, :cond_b

    :goto_b
    add-int/2addr v0, v2

    .line 7484
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/g/ag;->E:I

    add-int/2addr v0, v2

    .line 7485
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    if-nez v2, :cond_c

    :goto_c
    add-int/2addr v0, v1

    .line 7487
    return v0

    .line 7441
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 7448
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 7450
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_3
    move v0, v3

    .line 7460
    goto/16 :goto_3

    :cond_4
    move v0, v3

    .line 7465
    goto/16 :goto_4

    :cond_5
    move v0, v3

    .line 7477
    goto :goto_5

    :cond_6
    move v0, v3

    .line 7478
    goto :goto_6

    :cond_7
    move v0, v3

    .line 7479
    goto :goto_7

    :cond_8
    move v0, v3

    .line 7480
    goto :goto_8

    :cond_9
    move v0, v3

    .line 7481
    goto :goto_9

    :cond_a
    move v0, v3

    .line 7482
    goto :goto_a

    :cond_b
    move v2, v3

    .line 7483
    goto :goto_b

    .line 7485
    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    invoke-virtual {v1}, Lcom/google/android/gms/games/g/ah;->hashCode()I

    move-result v1

    goto :goto_c
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 6429
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/ag;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/ag;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/android/gms/games/g/ag;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/ag;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/ag;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/ag;->i:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/ag;->j:J

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/ag;->k:J

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/ag;->l:I

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->m:Z

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/ag;->n:J

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/ag;->o:J

    goto :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->p:Z

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/ag;->q:I

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/ag;->r:I

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/ag;->s:J

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/ag;->t:J

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/android/gms/games/g/ag;->u:I

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/ag;->v:J

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/ag;->w:J

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->x:Z

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->y:Z

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->z:Z

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->A:Z

    goto/16 :goto_0

    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->B:Z

    goto/16 :goto_0

    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->C:Z

    goto/16 :goto_0

    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->D:Z

    goto/16 :goto_0

    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/ag;->E:I

    goto/16 :goto_0

    :sswitch_20
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/ah;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ah;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xd8 -> :sswitch_1b
        0xe0 -> :sswitch_1c
        0xe8 -> :sswitch_1d
        0xf0 -> :sswitch_1e
        0xf8 -> :sswitch_1f
        0x102 -> :sswitch_20
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 7493
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7494
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7496
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->b:I

    if-eqz v0, :cond_1

    .line 7497
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7499
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/games/g/ag;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 7500
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7502
    :cond_2
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->d:I

    if-eqz v0, :cond_3

    .line 7503
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7505
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 7506
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7508
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 7509
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7511
    :cond_5
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->g:I

    if-eqz v0, :cond_6

    .line 7512
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7514
    :cond_6
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->h:I

    if-eqz v0, :cond_7

    .line 7515
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7517
    :cond_7
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->i:I

    if-eqz v0, :cond_8

    .line 7518
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7520
    :cond_8
    iget-wide v0, p0, Lcom/google/android/gms/games/g/ag;->j:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 7521
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7523
    :cond_9
    iget-wide v0, p0, Lcom/google/android/gms/games/g/ag;->k:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_a

    .line 7524
    const/16 v0, 0xb

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7526
    :cond_a
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->l:I

    if-eqz v0, :cond_b

    .line 7527
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7529
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->m:Z

    if-eqz v0, :cond_c

    .line 7530
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7532
    :cond_c
    iget-wide v0, p0, Lcom/google/android/gms/games/g/ag;->n:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_d

    .line 7533
    const/16 v0, 0xe

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->n:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7535
    :cond_d
    iget-wide v0, p0, Lcom/google/android/gms/games/g/ag;->o:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_e

    .line 7536
    const/16 v0, 0xf

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->o:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7538
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->p:Z

    if-eqz v0, :cond_f

    .line 7539
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7541
    :cond_f
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->q:I

    if-eqz v0, :cond_10

    .line 7542
    const/16 v0, 0x11

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->q:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7544
    :cond_10
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->r:I

    if-eqz v0, :cond_11

    .line 7545
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->r:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7547
    :cond_11
    iget-wide v0, p0, Lcom/google/android/gms/games/g/ag;->s:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_12

    .line 7548
    const/16 v0, 0x13

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->s:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7550
    :cond_12
    iget-wide v0, p0, Lcom/google/android/gms/games/g/ag;->t:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_13

    .line 7551
    const/16 v0, 0x14

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->t:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7553
    :cond_13
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->u:I

    if-eqz v0, :cond_14

    .line 7554
    const/16 v0, 0x15

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->u:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7556
    :cond_14
    iget-wide v0, p0, Lcom/google/android/gms/games/g/ag;->v:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_15

    .line 7557
    const/16 v0, 0x16

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->v:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7559
    :cond_15
    iget-wide v0, p0, Lcom/google/android/gms/games/g/ag;->w:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_16

    .line 7560
    const/16 v0, 0x17

    iget-wide v2, p0, Lcom/google/android/gms/games/g/ag;->w:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 7562
    :cond_16
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->x:Z

    if-eqz v0, :cond_17

    .line 7563
    const/16 v0, 0x18

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->x:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7565
    :cond_17
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->y:Z

    if-eqz v0, :cond_18

    .line 7566
    const/16 v0, 0x19

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->y:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7568
    :cond_18
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->z:Z

    if-eqz v0, :cond_19

    .line 7569
    const/16 v0, 0x1a

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->z:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7571
    :cond_19
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->A:Z

    if-eqz v0, :cond_1a

    .line 7572
    const/16 v0, 0x1b

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->A:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7574
    :cond_1a
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->B:Z

    if-eqz v0, :cond_1b

    .line 7575
    const/16 v0, 0x1c

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->B:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7577
    :cond_1b
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->C:Z

    if-eqz v0, :cond_1c

    .line 7578
    const/16 v0, 0x1d

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->C:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7580
    :cond_1c
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/ag;->D:Z

    if-eqz v0, :cond_1d

    .line 7581
    const/16 v0, 0x1e

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/ag;->D:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 7583
    :cond_1d
    iget v0, p0, Lcom/google/android/gms/games/g/ag;->E:I

    if-eqz v0, :cond_1e

    .line 7584
    const/16 v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/ag;->E:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7586
    :cond_1e
    iget-object v0, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    if-eqz v0, :cond_1f

    .line 7587
    const/16 v0, 0x20

    iget-object v1, p0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 7589
    :cond_1f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7590
    return-void
.end method
