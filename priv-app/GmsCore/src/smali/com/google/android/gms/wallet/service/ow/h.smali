.class public final Lcom/google/android/gms/wallet/service/ow/h;
.super Lcom/google/android/gms/wallet/c/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/c/b;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/c/b;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/wallet/c/b;-><init>()V

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    .line 50
    return-void
.end method

.method private a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;)Lcom/google/android/gms/wallet/c/i;
    .locals 3

    .prologue
    .line 230
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/google/android/gms/wallet/service/ow/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;B)V

    move-object p1, v0

    .line 233
    :cond_0
    return-object p1
.end method

.method private c(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    if-eqz p1, :cond_0

    .line 217
    const-string v0, "androidPackageName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 220
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 225
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 198
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/c/b;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 3

    .prologue
    .line 130
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    const-string v1, "check_preauth"

    invoke-direct {p0, p2, v1}, Lcom/google/android/gms/wallet/service/ow/h;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;)Lcom/google/android/gms/wallet/c/i;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/wallet/c/b;->a(Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 141
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "checkForPreAuthorization: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 136
    :catch_1
    move-exception v0

    .line 137
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 138
    const/16 v0, 0x8

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p2, v0, v1, v2}, Lcom/google/android/gms/wallet/c/i;->a(IZLandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 3

    .prologue
    .line 147
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    const-string v1, "create_wallet_objects"

    invoke-direct {p0, p3, v1}, Lcom/google/android/gms/wallet/service/ow/h;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;)Lcom/google/android/gms/wallet/c/i;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/wallet/c/b;->a(Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 157
    :goto_0
    return-void

    .line 152
    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "createWalletObjects: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 153
    :catch_1
    move-exception v0

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 155
    const/16 v0, 0x8

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v0, v1}, Lcom/google/android/gms/wallet/c/i;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/FullWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 3

    .prologue
    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    const-string v1, "load_full_wallet"

    invoke-direct {p0, p3, v1}, Lcom/google/android/gms/wallet/service/ow/h;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;)Lcom/google/android/gms/wallet/c/i;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/wallet/c/b;->a(Lcom/google/android/gms/wallet/FullWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 93
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "getFullWallet: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 84
    :catch_1
    move-exception v0

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    const/16 v0, 0x8

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lcom/google/android/gms/wallet/f;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/f;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/f;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v0, v1, v2}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/f;)V
    .locals 3

    .prologue
    .line 163
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/c/b;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/f;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 173
    :goto_0
    return-void

    .line 167
    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "checkOwServerState: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 168
    :catch_1
    move-exception v0

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170
    const/16 v0, 0x8

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v0, v1, v2}, Lcom/google/android/gms/wallet/c/f;->a(IILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 3

    .prologue
    .line 57
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    const-string v1, "load_masked_wallet"

    invoke-direct {p0, p3, v1}, Lcom/google/android/gms/wallet/service/ow/h;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;)Lcom/google/android/gms/wallet/c/i;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/wallet/c/b;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 71
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "getMaskedWalletForPreauthorizedBuyer: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 63
    :catch_1
    move-exception v0

    .line 64
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    const/16 v0, 0x8

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lcom/google/android/gms/wallet/o;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/o;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v0, v1, v2}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/c/b;->a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    const-string v1, "get_instruments"

    invoke-direct {p0, p3, v1}, Lcom/google/android/gms/wallet/service/ow/h;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;)Lcom/google/android/gms/wallet/c/i;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/wallet/c/b;->a(Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 193
    :goto_0
    return-void

    .line 184
    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "getInstruments: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 185
    :catch_1
    move-exception v0

    .line 186
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 187
    invoke-static {}, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;->a()Lcom/google/android/gms/wallet/firstparty/b;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/firstparty/b;->a([Ljava/lang/String;)Lcom/google/android/gms/wallet/firstparty/b;

    move-result-object v0

    new-array v1, v3, [[B

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/firstparty/b;->a([[B)Lcom/google/android/gms/wallet/firstparty/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/firstparty/b;->a:Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;

    .line 191
    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v1, v0, v2}, Lcom/google/android/gms/wallet/c/i;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/wallet/firstparty/GetInstrumentsResponse;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 3

    .prologue
    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    const-string v1, "change_masked_wallet"

    invoke-direct {p0, p4, v1}, Lcom/google/android/gms/wallet/service/ow/h;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;)Lcom/google/android/gms/wallet/c/i;

    move-result-object v1

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/gms/wallet/c/b;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 114
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "changeMaskedWallet: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 105
    :catch_1
    move-exception v0

    .line 106
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p3}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 107
    const/16 v0, 0x8

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lcom/google/android/gms/wallet/o;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/wallet/o;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/wallet/o;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p4, v0, v1, v2}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/h;->a:Lcom/google/android/gms/wallet/c/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/c/b;->b(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :goto_0
    return-void

    .line 208
    :catch_0
    move-exception v0

    .line 209
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/h;->b:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/ow/h;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
