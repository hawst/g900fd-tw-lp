.class final Lcom/google/android/gms/wallet/dynamite/ui/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/dynamite/ui/a;


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:I

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Lcom/google/android/gms/wallet/dynamite/ui/i;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->b:I

    .line 23
    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->c:I

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->d:Ljava/lang/String;

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->a:Landroid/graphics/Paint;

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 31
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 34
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->b:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->e:Lcom/google/android/gms/wallet/dynamite/ui/i;

    if-eqz v0, :cond_0

    .line 39
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->c:I

    .line 54
    :goto_0
    return v0

    .line 41
    :cond_0
    iput p1, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->b:I

    .line 43
    int-to-float v0, p1

    const v1, 0x3ecccccd    # 0.4f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 44
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->a:Landroid/graphics/Paint;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->a:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->c:I

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->e:Lcom/google/android/gms/wallet/dynamite/ui/i;

    if-nez v0, :cond_1

    .line 48
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/ui/i;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/dynamite/ui/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->e:Lcom/google/android/gms/wallet/dynamite/ui/i;

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->e:Lcom/google/android/gms/wallet/dynamite/ui/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/wallet/dynamite/ui/i;->a:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->e:Lcom/google/android/gms/wallet/dynamite/ui/i;

    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->a:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, v0, Lcom/google/android/gms/wallet/dynamite/ui/i;->b:Landroid/graphics/Paint;

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->e:Lcom/google/android/gms/wallet/dynamite/ui/i;

    iput p1, v0, Lcom/google/android/gms/wallet/dynamite/ui/i;->c:I

    .line 54
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->c:I

    goto :goto_0
.end method

.method public final a()Lcom/google/android/gms/wallet/dynamite/ui/b;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->e:Lcom/google/android/gms/wallet/dynamite/ui/i;

    .line 63
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/h;->e:Lcom/google/android/gms/wallet/dynamite/ui/i;

    .line 64
    return-object v0
.end method
