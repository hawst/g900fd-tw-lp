.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private final c:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->c:Ljava/util/Set;

    .line 46
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/cc;
    .locals 4

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedCircleMemberEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->c:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedCircleMemberEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;
    .locals 2

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->a:Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->c:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;
    .locals 2

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->b:Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->c:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 66
    return-object p0
.end method
