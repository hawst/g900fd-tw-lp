.class public final Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/view/View;

.field private final e:Ljava/util/LinkedHashMap;

.field private final f:Lcom/google/android/gms/common/internal/a/a;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Lcom/google/android/gms/common/audience/b/d;

.field private j:Lcom/google/android/gms/common/people/data/Audience;

.field private k:I

.field private l:I

.field private m:Z

.field private final n:Ljava/lang/String;

.field private final o:Landroid/content/Context;

.field private final p:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Context;Lcom/google/android/gms/common/audience/b/d;)V
    .locals 4

    .prologue
    .line 124
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 83
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    .line 86
    new-instance v0, Lcom/google/android/gms/common/internal/a/a;

    const/high16 v1, 0x200000

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/a/a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->f:Lcom/google/android/gms/common/internal/a/a;

    .line 126
    iput-object p3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->i:Lcom/google/android/gms/common/audience/b/d;

    .line 127
    iput-object p2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->o:Landroid/content/Context;

    .line 128
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a:Landroid/view/LayoutInflater;

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->ad:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 132
    sget v0, Lcom/google/android/gms/j;->bl:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    .line 133
    sget v0, Lcom/google/android/gms/j;->bk:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->d:Landroid/view/View;

    .line 135
    sget v0, Lcom/google/android/gms/j;->bm:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    .line 137
    sget v0, Lcom/google/android/gms/p;->vv:I

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->n:Ljava/lang/String;

    .line 140
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    new-instance v2, Lcom/google/android/gms/people/ad;

    invoke-direct {v2}, Lcom/google/android/gms/people/ad;-><init>()V

    const/16 v3, 0x50

    iput v3, v2, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v2}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->p:Lcom/google/android/gms/common/api/v;

    .line 145
    invoke-virtual {p0, p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setSaveEnabled(Z)V

    .line 148
    return-void
.end method

.method private a(Landroid/view/View;Lcom/google/android/gms/common/people/data/AudienceMember;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 285
    sget v0, Lcom/google/android/gms/j;->cG:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 286
    sget v0, Lcom/google/android/gms/j;->cJ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 287
    sget v1, Lcom/google/android/gms/j;->cI:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 288
    sget v2, Lcom/google/android/gms/j;->cH:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 291
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "checkboxEnabled"

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v6

    .line 294
    :goto_0
    if-eqz v3, :cond_1

    iget v3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->l:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_1

    move v5, v6

    .line 295
    :goto_1
    if-eqz v5, :cond_2

    move v3, v4

    :goto_2
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 297
    if-eqz v5, :cond_3

    move-object v2, p0

    :goto_3
    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    invoke-virtual {p1, v5}, Landroid/view/View;->setClickable(Z)V

    .line 299
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 302
    iget-object v3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->o:Landroid/content/Context;

    iget-boolean v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->m:Z

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown audience member type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v3, v4

    .line 291
    goto :goto_0

    :cond_1
    move v5, v4

    .line 294
    goto :goto_1

    .line 295
    :cond_2
    const/16 v3, 0x8

    goto :goto_2

    .line 297
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 302
    :pswitch_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    :pswitch_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown circle type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    if-eqz v2, :cond_6

    sget v2, Lcom/google/android/gms/h;->y:I

    :goto_4
    sget v5, Lcom/google/android/gms/p;->em:I

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v5, Lcom/google/android/gms/h;->F:I

    move-object v9, v3

    move v3, v5

    move v5, v2

    move-object v2, v9

    :goto_5
    new-instance v8, Lcom/google/android/gms/plus/audience/widgets/d;

    invoke-direct {v8, v5, v2, v3, v4}, Lcom/google/android/gms/plus/audience/widgets/d;-><init>(ILjava/lang/String;IB)V

    .line 305
    iget v2, v8, Lcom/google/android/gms/plus/audience/widgets/d;->a:I

    invoke-virtual {v7, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 306
    iget-object v2, v8, Lcom/google/android/gms/plus/audience/widgets/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    const/16 v0, 0x7f

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 308
    iget v0, v8, Lcom/google/android/gms/plus/audience/widgets/d;->c:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 310
    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 311
    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->f:Lcom/google/android/gms/common/internal/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/internal/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    :cond_4
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->p:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v3, v2, v6, v6}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/plus/audience/widgets/a;

    invoke-direct {v3, p0, v2, v1}, Lcom/google/android/gms/plus/audience/widgets/a;-><init>(Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-interface {v0, v3}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 314
    :cond_5
    return-object p1

    .line 302
    :cond_6
    sget v2, Lcom/google/android/gms/h;->w:I

    goto :goto_4

    :pswitch_3
    if-eqz v2, :cond_7

    sget v2, Lcom/google/android/gms/h;->y:I

    :goto_6
    sget v5, Lcom/google/android/gms/p;->ek:I

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v5, Lcom/google/android/gms/h;->C:I

    move-object v9, v3

    move v3, v5

    move v5, v2

    move-object v2, v9

    goto :goto_5

    :cond_7
    sget v2, Lcom/google/android/gms/h;->w:I

    goto :goto_6

    :pswitch_4
    sget v5, Lcom/google/android/gms/h;->v:I

    sget v2, Lcom/google/android/gms/p;->en:I

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->A:I

    goto :goto_5

    :pswitch_5
    sget v5, Lcom/google/android/gms/h;->w:I

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->B:I

    goto :goto_5

    :pswitch_6
    sget v5, Lcom/google/android/gms/h;->v:I

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->A:I

    goto :goto_5

    :pswitch_7
    sget v5, Lcom/google/android/gms/h;->v:I

    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->E:I

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;)Lcom/google/android/gms/common/internal/a/a;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->f:Lcom/google/android/gms/common/internal/a/a;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iput v3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->l:I

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->setVisibility(I)V

    .line 232
    :goto_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->l:I

    packed-switch v0, :pswitch_data_0

    .line 252
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b()V

    .line 253
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->invalidate()V

    .line 254
    return-void

    .line 227
    :cond_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->k:I

    iput v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->l:I

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->setVisibility(I)V

    goto :goto_0

    .line 234
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 235
    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setClickable(Z)V

    .line 236
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 240
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 241
    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setClickable(Z)V

    .line 242
    invoke-virtual {p0, p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 246
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 247
    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setClickable(Z)V

    .line 248
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 232
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 258
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Landroid/view/View;Lcom/google/android/gms/common/people/data/AudienceMember;)Landroid/view/View;

    goto :goto_0

    .line 260
    :cond_0
    return-void
.end method

.method private static b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 343
    const/16 v0, 0xff

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 344
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 345
    invoke-virtual {p1, v1, v1, v1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 346
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 210
    iput p1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->k:I

    .line 211
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a()V

    .line 212
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 5

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    .line 196
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    .line 198
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 199
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 200
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AudienceMember can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/gms/l;->ae:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Landroid/view/View;Lcom/google/android/gms/common/people/data/AudienceMember;)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v0, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->invalidate()V

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->h:Ljava/lang/String;

    goto :goto_1

    .line 202
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    if-eqz v0, :cond_4

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->ej:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 206
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a()V

    .line 207
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->m:Z

    if-eq v0, p1, :cond_0

    .line 216
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->m:Z

    .line 217
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->b()V

    .line 219
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 263
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    .line 265
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->c:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->ej:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 268
    :cond_1
    return-void
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 152
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->p:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 154
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 358
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->l:I

    packed-switch v0, :pswitch_data_0

    .line 371
    :goto_0
    return-void

    .line 360
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->i:Lcom/google/android/gms/common/audience/b/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/b/d;->a()V

    goto :goto_0

    .line 371
    :catch_0
    move-exception v0

    goto :goto_0

    .line 364
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 365
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->i:Lcom/google/android/gms/common/audience/b/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/audience/b/d;->a(Lcom/google/android/gms/common/people/data/AudienceMember;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 358
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->p:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 159
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 160
    return-void
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 375
    check-cast p1, Landroid/os/Bundle;

    .line 376
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 377
    const-string v0, "parent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 378
    const-string v0, "showEmptyText"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    .line 379
    const-string v0, "underage"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->m:Z

    .line 380
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 381
    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 385
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 386
    const-string v1, "parent"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 387
    const-string v1, "showEmptyText"

    iget-boolean v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->g:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 388
    const-string v1, "underage"

    iget-boolean v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->m:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 389
    const-string v1, "audience"

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/widgets/AudienceViewImpl;->j:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 390
    return-object v0
.end method
