.class public final enum Lcom/google/android/gms/drive/database/model/s;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/s;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/s;

.field private static final synthetic d:[Lcom/google/android/gms/drive/database/model/s;


# instance fields
.field private final c:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 44
    new-instance v0, Lcom/google/android/gms/drive/database/model/s;

    const-string v1, "PARENT_ENTRY_SQL_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->d()Lcom/google/android/gms/drive/database/model/r;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "parentEntryId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v4

    invoke-virtual {v3, v4, v8}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/s;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/s;->a:Lcom/google/android/gms/drive/database/model/s;

    .line 48
    new-instance v0, Lcom/google/android/gms/drive/database/model/s;

    const-string v1, "CHILD_ENTRY_SQL_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->d()Lcom/google/android/gms/drive/database/model/r;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "childEntryId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v4

    invoke-virtual {v3, v4, v8}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/database/model/s;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/s;->b:Lcom/google/android/gms/drive/database/model/s;

    .line 43
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/s;

    sget-object v1, Lcom/google/android/gms/drive/database/model/s;->a:Lcom/google/android/gms/drive/database/model/s;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/s;->b:Lcom/google/android/gms/drive/database/model/s;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/drive/database/model/s;->d:[Lcom/google/android/gms/drive/database/model/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/s;->c:Lcom/google/android/gms/drive/database/model/ab;

    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/s;
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/gms/drive/database/model/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/s;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/s;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/gms/drive/database/model/s;->d:[Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/s;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/s;->c:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/s;->c:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
