.class public final Lcom/google/android/gms/droidguard/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final a:Ljava/util/UUID;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 27
    new-instance v0, Ljava/util/UUID;

    const-wide v2, -0x121074568629b532L    # -3.563403477674908E221

    const-wide v4, -0x5c37d8232ae2de13L

    invoke-direct {v0, v2, v3, v4, v5}, Ljava/util/UUID;-><init>(JJ)V

    sput-object v0, Lcom/google/android/gms/droidguard/e/a;->a:Ljava/util/UUID;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/google/android/gms/droidguard/e/a;->b:Landroid/content/Context;

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/droidguard/e/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/gms/droidguard/e/a;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/droidguard/e/a;[BLandroid/media/MediaDrm;)V
    .locals 3

    .prologue
    .line 23
    :try_start_0
    invoke-virtual {p2, p1}, Landroid/media/MediaDrm;->provideProvisionResponse([B)V
    :try_end_0
    .catch Landroid/media/DeniedByServerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/droidguard/e/a;->d:Z

    const-string v1, "DG.WV"

    const-string v2, "Provisioning denied by remote server"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/droidguard/e/a;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/google/android/gms/droidguard/e/a;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/droidguard/e/a;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/google/android/gms/droidguard/e/a;->e:Z

    return p1
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/16 v3, 0x12

    const/4 v0, 0x0

    .line 81
    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v3, :cond_1

    const-string v0, "DG.WV"

    const-string v1, "Running unsupported SDK."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/e/a;->d:Z

    if-eqz v1, :cond_2

    const-string v1, "DG.WV"

    const-string v2, "Provisioning request denied by server."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/gms/droidguard/e/c; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    if-eqz v0, :cond_0

    :try_start_1
    new-instance v0, Landroid/media/MediaDrm;

    sget-object v1, Lcom/google/android/gms/droidguard/e/a;->a:Ljava/util/UUID;

    invoke-direct {v0, v1}, Landroid/media/MediaDrm;-><init>(Ljava/util/UUID;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/gms/droidguard/e/c; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {v0}, Landroid/media/MediaDrm;->openSession()[B
    :try_end_2
    .catch Landroid/media/MediaDrmException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v0, v1}, Landroid/media/MediaDrm;->closeSession([B)V
    :try_end_3
    .catch Lcom/google/android/gms/droidguard/e/c; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    throw v0

    .line 81
    :cond_2
    :try_start_4
    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/e/a;->e:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/e/a;->c:Z

    if-nez v1, :cond_3

    const-string v1, "DG.WV"

    const-string v2, "Waiting for provisioning response from server."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Lcom/google/android/gms/droidguard/e/c; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 84
    :catch_1
    move-exception v0

    .line 85
    const-string v1, "DG.WV"

    const-string v2, "Unexpected exception."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 81
    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_5
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_4

    instance-of v1, v0, Landroid/media/UnsupportedSchemeException;

    if-eqz v1, :cond_4

    const-string v1, "DG.WV"

    const-string v2, "Widevine DRM not supported on this device"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_4
    const-string v1, "DG.WV"

    const-string v2, "Unexpected exception while creating MediaDrm."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catch Lcom/google/android/gms/droidguard/e/c; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    :catch_3
    move-exception v1

    :try_start_6
    instance-of v1, v1, Landroid/media/NotProvisionedException;

    if-eqz v1, :cond_5

    new-instance v1, Lcom/google/android/gms/droidguard/e/b;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/droidguard/e/b;-><init>(Lcom/google/android/gms/droidguard/e/a;Landroid/media/MediaDrm;)V

    invoke-virtual {v1}, Lcom/google/android/gms/droidguard/e/b;->start()V

    new-instance v0, Lcom/google/android/gms/droidguard/e/c;

    const-string v1, "Waiting for provisioning response from server."

    invoke-direct {v0, v1}, Lcom/google/android/gms/droidguard/e/c;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catch Lcom/google/android/gms/droidguard/e/c; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    :cond_5
    :try_start_8
    new-instance v0, Lcom/google/android/gms/droidguard/e/c;

    const-string v1, "Unable to trigger provisioning right now."

    invoke-direct {v0, v1}, Lcom/google/android/gms/droidguard/e/c;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_4
    move-exception v0

    new-instance v1, Lcom/google/android/gms/droidguard/e/c;

    const-string v2, "Exception while opening mediaDrm session."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/droidguard/e/c;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method
