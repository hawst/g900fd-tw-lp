.class public final Lcom/google/android/gms/people/sync/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/Map;

.field static final b:Ljava/util/Map;

.field static final c:Ljava/util/Map;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/people/sync/f;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/sync/e;->a:Ljava/util/Map;

    .line 73
    new-instance v0, Lcom/google/android/gms/people/sync/g;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/sync/e;->b:Ljava/util/Map;

    .line 85
    new-instance v0, Lcom/google/android/gms/people/sync/h;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/sync/e;->c:Ljava/util/Map;

    .line 236
    const-string v0, "@profile@"

    sput-object v0, Lcom/google/android/gms/people/sync/e;->d:Ljava/lang/String;

    .line 239
    const-string v0, "@circle@"

    sput-object v0, Lcom/google/android/gms/people/sync/e;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Ljava/lang/String;)D
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 652
    if-nez p0, :cond_0

    move-wide v0, v2

    .line 661
    :goto_0
    return-wide v0

    .line 656
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;

    .line 657
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 658
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->d()D

    move-result-wide v0

    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 661
    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)D
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 619
    if-nez p0, :cond_0

    move-wide v0, v2

    .line 627
    :goto_0
    return-wide v0

    .line 622
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$Affinities;

    .line 623
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$Affinities;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 624
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$Affinities;->d()D

    move-result-wide v0

    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 627
    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;)I
    .locals 2

    .prologue
    .line 722
    sget-object v0, Lcom/google/android/gms/people/internal/at;->a:Ljava/util/Map;

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;)I
    .locals 2

    .prologue
    .line 715
    sget-object v0, Lcom/google/android/gms/people/sync/e;->c:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;)I
    .locals 2

    .prologue
    .line 708
    sget-object v0, Lcom/google/android/gms/people/sync/e;->a:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;
    .locals 2

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/people/sync/m;->a:Lcom/google/android/gms/people/sync/m;

    invoke-static {v0, v1}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;Lcom/google/android/gms/people/sync/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 0

    .prologue
    .line 450
    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    sget-object p0, Lcom/google/android/gms/people/internal/at;->b:Ljava/lang/Iterable;

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lcom/google/android/gms/people/sync/j;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 242
    if-nez p0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-object v0

    .line 243
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 244
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    .line 245
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 246
    invoke-interface {p1, v1}, Lcom/google/android/gms/people/sync/j;->a(Ljava/lang/Object;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, v1

    .line 247
    goto :goto_0

    .line 244
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method private static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 690
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 691
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 693
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 255
    if-nez p0, :cond_1

    .line 258
    :cond_0
    :goto_0
    return v0

    .line 256
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 258
    const-string v0, "profile"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Z
    .locals 1

    .prologue
    .line 532
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Z
    .locals 1

    .prologue
    .line 439
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 280
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    :goto_0
    return v0

    .line 283
    :cond_0
    if-nez p0, :cond_1

    move v1, v0

    :goto_1
    if-eqz v1, :cond_2

    .line 284
    const/4 v0, 0x2

    goto :goto_0

    .line 283
    :cond_1
    const-string v1, "circle"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    .line 286
    :cond_2
    if-nez p0, :cond_3

    :goto_2
    if-eqz v0, :cond_4

    .line 287
    const/4 v0, 0x1

    goto :goto_0

    .line 286
    :cond_3
    const-string v0, "contact"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2

    .line 289
    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)I
    .locals 2

    .prologue
    .line 701
    sget-object v0, Lcom/google/android/gms/people/sync/e;->b:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static b(Ljava/util/List;)I
    .locals 1

    .prologue
    .line 446
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;
    .locals 2

    .prologue
    .line 349
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->k(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 350
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 352
    :goto_0
    return-object v0

    .line 350
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->p()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    goto :goto_0

    .line 352
    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 668
    if-nez p0, :cond_0

    move-object v0, v1

    .line 677
    :goto_0
    return-object v0

    .line 672
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;

    .line 673
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 674
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 677
    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 634
    if-nez p0, :cond_0

    move-object v0, v1

    .line 642
    :goto_0
    return-object v0

    .line 637
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$Affinities;

    .line 638
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$Affinities;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 639
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$Affinities;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 642
    goto :goto_0
.end method

.method public static c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;
    .locals 2

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->l()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/people/sync/l;->a:Lcom/google/android/gms/people/sync/l;

    invoke-static {v0, v1}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;Lcom/google/android/gms/people/sync/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    return-object v0
.end method

.method public static c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 310
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 301
    :pswitch_0
    sget-object v0, Lcom/google/android/gms/people/sync/e;->d:Ljava/lang/String;

    goto :goto_0

    .line 303
    :pswitch_1
    sget-object v0, Lcom/google/android/gms/people/sync/e;->e:Ljava/lang/String;

    goto :goto_0

    .line 307
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 299
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;
    .locals 2

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->c()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z
    .locals 1

    .prologue
    .line 317
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Taglines;
    .locals 2

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->t()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/people/sync/p;->a:Lcom/google/android/gms/people/sync/p;

    invoke-static {v0, v1}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;Lcom/google/android/gms/people/sync/j;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Taglines;

    return-object v0
.end method

.method public static final f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)I
    .locals 2

    .prologue
    .line 380
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    .line 381
    if-eqz v0, :cond_0

    const-string v1, "page"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    const/4 v0, 0x2

    .line 385
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static g(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 393
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v3

    .line 394
    if-nez v3, :cond_0

    move v0, v1

    .line 417
    :goto_0
    return v0

    .line 397
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 398
    goto :goto_0

    .line 401
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/a/a;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 402
    goto :goto_0

    .line 409
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->r()Ljava/util/List;

    move-result-object v0

    .line 410
    if-nez v0, :cond_4

    .line 411
    sget-object v0, Lcom/google/android/gms/people/a/a;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 413
    goto :goto_0

    :cond_3
    move v0, v1

    .line 415
    goto :goto_0

    .line 417
    :cond_4
    const-string v1, "googlePlus"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static h(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 474
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    if-nez v1, :cond_1

    move v1, v0

    :goto_0
    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v1

    goto :goto_0
.end method

.method public static j(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/util/List;
    .locals 4

    .prologue
    .line 483
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 485
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->i(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 486
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 487
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 488
    const-string v0, "PeopleProtoHelper"

    const-string v3, "Empty contact ID detected"

    invoke-static {v0, v3}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 491
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 494
    :cond_1
    return-object v1
.end method

.method public static k(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 1

    .prologue
    .line 508
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->l(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 517
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    .line 518
    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->g(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v0

    if-lez v0, :cond_3

    .line 522
    sget-object v5, Lcom/google/android/gms/people/sync/i;->a:Lcom/google/android/gms/people/sync/i;

    invoke-interface {v5, p0}, Lcom/google/android/gms/people/sync/j;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v7

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_2

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v5, v0}, Lcom/google/android/gms/people/sync/j;->a(Ljava/lang/Object;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_1

    :goto_2
    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    .line 524
    :goto_3
    return-object v0

    :cond_0
    move v2, v3

    .line 522
    goto :goto_1

    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 524
    goto :goto_3
.end method

.method public static m(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 2

    .prologue
    .line 543
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "menagerie"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 554
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->p(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v0

    .line 555
    if-nez v0, :cond_0

    .line 556
    new-instance v0, Lcom/google/android/gms/people/sync/t;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to get qualifed ID.  v2id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/t;-><init>(Ljava/lang/String;)V

    throw v0

    .line 558
    :cond_0
    return-object v0
.end method

.method public static o(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 562
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 573
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->g(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->o(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v0

    .line 575
    if-eqz v0, :cond_0

    .line 576
    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 590
    :goto_0
    return-object v0

    .line 580
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/people/sync/e;->l(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    move-result-object v0

    .line 581
    if-eqz v0, :cond_2

    .line 582
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v1

    .line 583
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 584
    new-instance v0, Lcom/google/android/gms/people/sync/t;

    const-string v1, "Email empty."

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/t;-><init>(Ljava/lang/String;)V

    throw v0

    .line 586
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 590
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 1

    .prologue
    .line 601
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 1

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 684
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->m()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$LegacyFields;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
