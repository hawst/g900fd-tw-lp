.class final Lcom/google/android/gms/fitness/b/c/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/b/o;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/b/c/l;

.field private final b:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/b/c/l;Z)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/n;->a:Lcom/google/android/gms/fitness/b/c/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-boolean p2, p0, Lcom/google/android/gms/fitness/b/c/n;->b:Z

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/b/c/l;ZB)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/b/c/n;-><init>(Lcom/google/android/gms/fitness/b/c/l;Z)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/b/e;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 47
    sget-object v2, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->e()Lcom/google/android/gms/fitness/b/h;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/fitness/b/c/n;->a:Lcom/google/android/gms/fitness/b/c/l;

    iget-object v2, v2, Lcom/google/android/gms/fitness/b/c/b;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->a()Lcom/google/android/gms/fitness/b/b;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "com.google.android.gms"

    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/fitness/b/c/n;->a:Lcom/google/android/gms/fitness/b/c/l;

    iget-object v2, v2, Lcom/google/android/gms/fitness/b/c/l;->g:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/fitness/b/c/n;->b:Z

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/b/c/v;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
