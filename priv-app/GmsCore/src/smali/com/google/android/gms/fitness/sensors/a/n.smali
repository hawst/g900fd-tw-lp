.class final Lcom/google/android/gms/fitness/sensors/a/n;
.super Lcom/google/android/gms/fitness/sensors/a/o;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/sensors/a/e;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/sensors/a/e;)V
    .locals 1

    .prologue
    .line 794
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/n;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/a/o;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/sensors/a/e;B)V
    .locals 0

    .prologue
    .line 794
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/sensors/a/n;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 799
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/n;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    const-string v1, "discoverProperties"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/fitness/sensors/a/ae;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 800
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/n;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v1, v0, Lcom/google/android/gms/fitness/sensors/a/e;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 801
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/n;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    .line 802
    const-string v0, "Not running discover command, no Gatt."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 803
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/n;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/ae;->a()V

    .line 804
    monitor-exit v1

    .line 810
    :goto_0
    return-void

    .line 806
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/n;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->m:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    move-result v0

    if-nez v0, :cond_1

    .line 807
    const-string v0, "Failed to start discover services"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 808
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/n;->a:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/a/e;->p:Lcom/google/android/gms/fitness/sensors/a/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/ae;->a()V

    .line 810
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
