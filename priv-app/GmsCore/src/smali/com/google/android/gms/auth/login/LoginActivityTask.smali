.class public Lcom/google/android/gms/auth/login/LoginActivityTask;
.super Lcom/google/android/gms/auth/login/g;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/setupwizard/navigationbar/a;


# static fields
.field private static final r:Ljava/lang/String;

.field private static final s:Ljava/lang/String;

.field private static final t:Ljava/lang/String;

.field private static final u:Ljava/lang/String;

.field private static final v:Ljava/lang/String;

.field private static final w:Ljava/lang/String;

.field private static final x:Ljava/lang/String;

.field private static final y:Ljava/lang/String;


# instance fields
.field private A:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Ljava/lang/String;

.field private H:Z

.field private I:Lcom/google/android/gms/auth/login/ba;

.field private J:Lcom/google/android/setupwizard/util/b;

.field private K:Landroid/view/View;

.field q:Z

.field private z:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/gms/auth/login/LoginActivityTask;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->r:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "auth_code"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->s:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".token_request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->t:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".backup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->u:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->v:Ljava/lang/String;

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " .browser_request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->w:Ljava/lang/String;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".confirming_credentials"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->x:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".allow_credit_card"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->y:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/g;-><init>()V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->q:Z

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/LoginActivityTask;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "password"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->w:Ljava/lang/String;

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/login/LoginActivityTask;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->A:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    .line 113
    const-string v0, "password"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->C:Ljava/lang/String;

    .line 114
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->B:Ljava/lang/String;

    .line 115
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->D:Z

    .line 116
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->G:Ljava/lang/String;

    .line 117
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->E:Z

    .line 118
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->F:Z

    .line 119
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->H:Z

    .line 120
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/LoginActivityTask;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->H:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/login/LoginActivityTask;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->A:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->J:Lcom/google/android/setupwizard/util/b;

    sget v1, Lcom/google/android/gms/p;->bv:I

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/b;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->K:Landroid/view/View;

    .line 189
    return-void
.end method

.method private m()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->I:Lcom/google/android/gms/auth/login/ba;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/login/ba;->cancel(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    const-string v0, "GLSActivity"

    const-string v1, "LoginActivityTask.BackgroudTask failed to cancel."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 278
    const-string v1, "errorCode"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 279
    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setResult(ILandroid/content/Intent;)V

    .line 280
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->finish()V

    .line 281
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->m()V

    .line 293
    return-void
.end method

.method public final a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->o:Z

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a(ZZ)V

    .line 286
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 287
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 288
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 298
    return-void
.end method

.method public final c()V
    .locals 9

    .prologue
    .line 199
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->c()V

    .line 201
    const-string v0, "GLSActivity"

    const-string v1, "Starting TokenRequestAsyncTask..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    new-instance v0, Lcom/google/android/gms/auth/login/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->A:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->C:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->B:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->D:Z

    iget-boolean v7, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->E:Z

    iget-boolean v8, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->F:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/auth/login/am;-><init>(Lcom/google/android/gms/auth/login/LoginActivityTask;Landroid/content/Context;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->I:Lcom/google/android/gms/auth/login/ba;

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->I:Lcom/google/android/gms/auth/login/ba;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/ba;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 259
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->m()V

    .line 264
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->J:Lcom/google/android/setupwizard/util/b;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->J:Lcom/google/android/setupwizard/util/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->K:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/b;->removeView(Landroid/view/View;)V

    .line 182
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->l()V

    .line 184
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/16 v1, 0x15

    .line 137
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onCreate(Landroid/os/Bundle;)V

    .line 139
    if-nez p1, :cond_1

    .line 140
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 141
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->a(Landroid/os/Bundle;)V

    .line 147
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->f()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 148
    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    new-instance v0, Lcom/google/android/setupwizard/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->J:Lcom/google/android/setupwizard/util/b;

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->J:Lcom/google/android/setupwizard/util/b;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setContentView(Landroid/view/View;)V

    .line 156
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->c()V

    .line 158
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 159
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->l()V

    .line 171
    :goto_2
    return-void

    .line 143
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/LoginActivityTask;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 152
    :cond_2
    sget v0, Lcom/google/android/gms/l;->E:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setContentView(I)V

    .line 153
    sget v0, Lcom/google/android/gms/j;->cs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->z:Landroid/widget/Button;

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->z:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 164
    :cond_3
    sget v0, Lcom/google/android/gms/j;->tN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->bV:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->G:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 167
    sget v0, Lcom/google/android/gms/p;->bE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->G:Ljava/lang/String;

    .line 169
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->G:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivityTask;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 193
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onPause()V

    .line 194
    sget-boolean v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->j:Z

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPause(), class="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 125
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->t:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->A:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 126
    const-string v0, "password"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->s:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->u:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 129
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->w:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 130
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->x:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->F:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 131
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->v:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->G:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    sget-object v0, Lcom/google/android/gms/auth/login/LoginActivityTask;->y:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/LoginActivityTask;->H:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 133
    return-void
.end method
