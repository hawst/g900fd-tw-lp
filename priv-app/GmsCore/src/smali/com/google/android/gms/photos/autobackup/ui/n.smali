.class final Lcom/google/android/gms/photos/autobackup/ui/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/n;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 3

    .prologue
    .line 154
    check-cast p1, Lcom/google/android/gms/photos/autobackup/i;

    const-string v0, "AutoBackupSettingsAct"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AutoBackupSettingsAct"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "folder "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/photos/autobackup/i;->b()Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state changed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
