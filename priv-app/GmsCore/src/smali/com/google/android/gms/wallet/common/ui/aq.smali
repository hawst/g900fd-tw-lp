.class final Lcom/google/android/gms/wallet/common/ui/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/an;

.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/an;)V
    .locals 1

    .prologue
    .line 811
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/aq;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/aq;->b:I

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 844
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 840
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aq;->a:Lcom/google/android/gms/wallet/common/ui/an;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;)I

    move-result v0

    .line 819
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/aq;->b:I

    if-eq v1, v0, :cond_1

    .line 820
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aq;->a:Lcom/google/android/gms/wallet/common/ui/an;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/ax;->a(I)V

    .line 822
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    .line 824
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/aq;->a:Lcom/google/android/gms/wallet/common/ui/an;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/text/InputFilter;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 825
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aq;->a:Lcom/google/android/gms/wallet/common/ui/an;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 826
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aq;->a:Lcom/google/android/gms/wallet/common/ui/an;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/an;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/aw;->f()Z

    .line 828
    :cond_0
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/aq;->b:I

    .line 831
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aq;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/an;->e(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/lang/String;

    move-result-object v0

    .line 832
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aq;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 833
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aq;->a:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/an;->a(Lcom/google/android/gms/wallet/common/ui/an;Ljava/lang/String;)V

    .line 835
    :cond_3
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aq;->c:Ljava/lang/String;

    .line 836
    return-void
.end method
