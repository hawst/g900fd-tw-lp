.class public final Lcom/google/android/gms/ads/settings/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/16 v9, 0x100

    const/4 v2, 0x0

    .line 48
    move v3, v2

    .line 49
    :goto_0
    const/4 v1, 0x2

    if-ge v3, v1, :cond_0

    .line 52
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "android_id"

    invoke-static {v1, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    if-nez v1, :cond_1

    .line 72
    :cond_0
    :goto_1
    return-object v0

    .line 59
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v4, v4

    const/16 v5, 0x100

    new-array v5, v5, [B

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, Lcom/google/android/gms/ads/settings/e/b;->a(I[BI)V

    const/4 v6, 0x4

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/ads/settings/e/b;->a(I[BI)V

    const-string v4, "MD5"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4

    const-string v6, "UTF-8"

    invoke-virtual {v1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v4

    const/16 v1, 0x10

    new-array v6, v1, [C

    fill-array-data v6, :array_0

    new-instance v7, Ljava/lang/StringBuilder;

    array-length v1, v4

    mul-int/lit8 v1, v1, 0x2

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    move v1, v2

    :goto_2
    array-length v8, v4

    if-ge v1, v8, :cond_2

    aget-byte v8, v4, v1

    and-int/lit16 v8, v8, 0xf0

    ushr-int/lit8 v8, v8, 0x4

    aget-char v8, v6, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    aget-byte v8, v4, v1

    and-int/lit8 v8, v8, 0xf

    aget-char v8, v6, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "UTF-8"

    invoke-virtual {v1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    move v1, v2

    :goto_3
    array-length v6, v4

    if-ge v1, v6, :cond_3

    add-int/lit8 v6, v1, 0x8

    aget-byte v7, v4, v1

    aput-byte v7, v5, v6

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    const/16 v4, 0x28

    invoke-static {v1, v5, v4}, Lcom/google/android/gms/ads/settings/e/b;->a(I[BI)V

    invoke-static {v5}, Lcom/google/android/gms/ads/settings/e/b;->a([B)V

    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    const/16 v1, 0x30

    :goto_4
    if-ge v1, v9, :cond_4

    const/16 v6, 0x100

    invoke-virtual {v4, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    aput-byte v6, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    const/16 v1, 0x100

    new-array v1, v1, [B

    new-instance v4, Lcom/google/android/gms/ads/settings/e/c;

    invoke-direct {v4}, Lcom/google/android/gms/ads/settings/e/c;-><init>()V

    invoke-virtual {v4, v5, v1}, Lcom/google/android/gms/ads/settings/e/c;->a([B[B)V

    .line 61
    const/16 v4, 0xb

    invoke-static {v1, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_1

    .line 69
    :catch_0
    move-exception v1

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_0

    .line 65
    :catch_1
    move-exception v1

    goto/16 :goto_1

    .line 59
    nop

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/google/android/gms/ads/settings/a/c;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 38
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/google/android/gms/ads/settings/e/a;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 39
    if-nez v1, :cond_0

    .line 40
    const/4 v0, 0x0

    .line 44
    :goto_0
    return-object v0

    .line 42
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "?sig="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&vv=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
