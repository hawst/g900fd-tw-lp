.class public Lcom/google/android/gms/plus/oob/y;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/ui/n;
.implements Lcom/google/android/gms/common/ui/o;


# instance fields
.field protected a:Lcom/google/android/gms/plus/oob/e;

.field protected b:Landroid/widget/LinearLayout;

.field protected c:Landroid/widget/Button;

.field protected d:Landroid/widget/Button;

.field private e:Lcom/google/android/gms/plus/oob/aa;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

.field private h:Landroid/view/View;

.field private i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 67
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/oob/y;)Lcom/google/android/gms/common/ui/ScrollViewWithEvents;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    return-object v0
.end method

.method public static a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/oob/y;
    .locals 4

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/gms/plus/oob/y;

    invoke-direct {v0}, Lcom/google/android/gms/plus/oob/y;-><init>()V

    .line 116
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 117
    const-string v2, "is_setup_wizard_theme"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 118
    const-string v2, "promo_app_package"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v2, "promo_app_text"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v2, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v2, "back_button_name"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v2, "upgrade_account"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 123
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/y;->setArguments(Landroid/os/Bundle;)V

    .line 124
    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 603
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 604
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 605
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 611
    :goto_0
    return-object v0

    .line 608
    :catch_0
    move-exception v0

    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    const-string v0, "UpgradeAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAppName can\'t find a package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/widget/LinearLayout;)Ljava/util/List;
    .locals 5

    .prologue
    .line 432
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 434
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    .line 435
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 436
    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 437
    instance-of v4, v0, Lcom/google/android/gms/plus/oob/d;

    if-eqz v4, :cond_1

    .line 438
    check-cast v0, Lcom/google/android/gms/plus/oob/d;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 435
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 439
    :cond_1
    instance-of v4, v0, Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    .line 441
    check-cast v0, Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/y;->a(Landroid/widget/LinearLayout;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 444
    :cond_2
    return-object v2
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 515
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 519
    const/4 v1, 0x0

    move v7, v1

    move-object v6, v4

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v7, v1, :cond_6

    .line 520
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    .line 522
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/gms/plus/oob/y;->j:Z

    invoke-static {v2, v7, v1, v3}, Lcom/google/android/gms/plus/oob/d;->a(Landroid/content/Context;ILcom/google/android/gms/plus/service/v1whitelisted/models/b;Z)Lcom/google/android/gms/plus/oob/d;

    move-result-object v2

    .line 524
    if-eqz v2, :cond_2

    .line 525
    iget-object v3, p0, Lcom/google/android/gms/plus/oob/y;->a:Lcom/google/android/gms/plus/oob/e;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/plus/oob/d;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V

    .line 526
    instance-of v3, v2, Lcom/google/android/gms/plus/oob/FieldViewGender;

    if-eqz v3, :cond_3

    .line 527
    check-cast v2, Lcom/google/android/gms/plus/oob/FieldViewGender;

    .line 535
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 536
    if-eqz v4, :cond_0

    .line 537
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 539
    :cond_0
    if-eqz v6, :cond_1

    .line 540
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 542
    :cond_1
    move-object v0, v4

    check-cast v0, Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

    move-object v3, v0

    move-object v0, v6

    check-cast v0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;

    move-object v5, v0

    invoke-virtual {v2, v3, v5}, Lcom/google/android/gms/plus/oob/FieldViewGender;->a(Lcom/google/android/gms/plus/oob/FieldViewCustomGender;Lcom/google/android/gms/plus/oob/FieldViewPronoun;)V

    .line 519
    :cond_2
    :goto_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_0

    .line 544
    :cond_3
    instance-of v3, v2, Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

    if-eqz v3, :cond_4

    move-object v4, v2

    .line 545
    goto :goto_1

    .line 546
    :cond_4
    instance-of v3, v2, Lcom/google/android/gms/plus/oob/FieldViewPronoun;

    if-eqz v3, :cond_5

    move-object v6, v2

    .line 547
    goto :goto_1

    .line 549
    :cond_5
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 552
    :catch_0
    move-exception v2

    .line 553
    const-string v3, "UpgradeAccount"

    const/4 v5, 0x5

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 554
    const-string v3, "UpgradeAccount"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "Failed to add field: type="

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->s()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " id="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 559
    :cond_6
    return-object v8
.end method

.method private a(Lcom/google/android/gms/plus/oob/d;)V
    .locals 2

    .prologue
    .line 563
    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/oob/d;->setVisibility(I)V

    .line 566
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/d;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 567
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->a:Lcom/google/android/gms/plus/oob/e;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/d;->g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/oob/e;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 569
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/plus/oob/y;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/gms/plus/oob/y;->l:I

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 396
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/y;->j:Z

    if-eqz v0, :cond_0

    .line 408
    :goto_0
    return-void

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-static {v0}, Lcom/google/android/gms/plus/oob/af;->c(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->c:Landroid/widget/Button;

    sget v1, Lcom/google/android/gms/p;->vg:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 403
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->c:Landroid/widget/Button;

    sget v1, Lcom/google/android/gms/p;->uZ:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 406
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->c:Landroid/widget/Button;

    sget v1, Lcom/google/android/gms/p;->vb:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 318
    iput p1, p0, Lcom/google/android/gms/plus/oob/y;->l:I

    .line 319
    return-void
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 427
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_setup_wizard_theme"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/l;->dX:I

    move v1, v0

    :goto_0
    sget v0, Lcom/google/android/gms/j;->cn:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 429
    return-void

    .line 427
    :cond_0
    sget v0, Lcom/google/android/gms/l;->dW:I

    move v1, v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 331
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->b:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 332
    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    const-string v0, "UpgradeAccount"

    const-string v1, "Form build requested but view has not yet been created"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/y;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 344
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 345
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->d()Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fm;->d()Ljava/util/List;

    move-result-object v0

    .line 346
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/y;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    move v1, v4

    .line 347
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 348
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/oob/d;

    .line 350
    iget-boolean v2, p0, Lcom/google/android/gms/plus/oob/y;->m:Z

    if-eqz v2, :cond_2

    instance-of v2, v0, Lcom/google/android/gms/plus/oob/FieldViewName;

    if-eqz v2, :cond_2

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/gms/plus/oob/FieldViewGender;

    if-eqz v2, :cond_2

    .line 352
    add-int/lit8 v5, v1, 0x1

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/oob/d;

    .line 353
    iget-object v7, p0, Lcom/google/android/gms/plus/oob/y;->b:Landroid/widget/LinearLayout;

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/plus/oob/FieldViewName;

    move-object v3, v1

    check-cast v3, Lcom/google/android/gms/plus/oob/FieldViewGender;

    new-instance v8, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getActivity()Landroid/support/v4/app/q;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v9, Landroid/view/ViewGroup$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v9, v10, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v9, 0x50

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v2}, Lcom/google/android/gms/plus/oob/FieldViewName;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v9, 0x3f800000    # 1.0f

    iput v9, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v8, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v3}, Lcom/google/android/gms/plus/oob/FieldViewGender;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, 0x0

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 355
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/y;->a(Lcom/google/android/gms/plus/oob/d;)V

    .line 356
    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/oob/y;->a(Lcom/google/android/gms/plus/oob/d;)V

    move v1, v5

    .line 347
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 358
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/oob/y;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 359
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/y;->a(Lcom/google/android/gms/plus/oob/d;)V

    goto :goto_2

    .line 366
    :cond_3
    invoke-static {p1}, Lcom/google/android/gms/plus/oob/af;->c(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->f:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->vf:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 370
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->c()V

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->b()V

    .line 324
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->c()V

    .line 325
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 412
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/y;->j:Z

    if-eqz v0, :cond_0

    .line 424
    :goto_0
    return-void

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->d:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 377
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->b:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 378
    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    const-string v0, "UpgradeAccount"

    const-string v1, "Next button update requested but form view has not yet been created"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/y;->c:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->b:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/y;->a(Landroid/widget/LinearLayout;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/oob/d;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/d;->b()Z

    move-result v4

    if-nez v4, :cond_2

    const-string v1, "UpgradeAccount"

    const/4 v3, 0x2

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "UpgradeAccount"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid field: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 384
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->a()V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 383
    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 256
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 262
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 130
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/aa;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/oob/aa;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 134
    check-cast v0, Lcom/google/android/gms/plus/oob/aa;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->e:Lcom/google/android/gms/plus/oob/aa;

    .line 135
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/e;

    if-nez v0, :cond_1

    .line 136
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/oob/e;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/oob/e;

    iput-object p1, p0, Lcom/google/android/gms/plus/oob/y;->a:Lcom/google/android/gms/plus/oob/e;

    .line 140
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 293
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 294
    sget v1, Lcom/google/android/gms/j;->mw:I

    if-ne v0, v1, :cond_e

    .line 295
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->b:Landroid/widget/LinearLayout;

    if-nez v0, :cond_3

    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    const-string v1, "FieldView values requested but form view has not yet been created"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_0
    :goto_0
    invoke-static {v2}, Lcom/google/android/gms/plus/oob/af;->c(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->a:Lcom/google/android/gms/plus/oob/e;

    sget-object v1, Lcom/google/android/gms/common/analytics/l;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/common/analytics/m;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/plus/oob/e;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->e:Lcom/google/android/gms/plus/oob/aa;

    invoke-interface {v0, v2}, Lcom/google/android/gms/plus/oob/aa;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 314
    :cond_2
    :goto_1
    return-void

    .line 299
    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->b:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/y;->a(Landroid/widget/LinearLayout;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v2

    move-object v4, v2

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/oob/d;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/d;->c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v3

    if-eqz v3, :cond_a

    instance-of v7, v0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;

    if-eqz v7, :cond_6

    if-eqz v1, :cond_4

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v4, :cond_5

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    move-object v1, v2

    move-object v4, v2

    goto :goto_2

    :cond_6
    instance-of v7, v0, Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

    if-eqz v7, :cond_7

    move-object v1, v3

    goto :goto_2

    :cond_7
    instance-of v0, v0, Lcom/google/android/gms/plus/oob/FieldViewGender;

    if-eqz v0, :cond_8

    move-object v4, v3

    goto :goto_2

    :cond_8
    if-eqz v4, :cond_9

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v4, v2

    :cond_9
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    move-object v3, v4

    move-object v4, v3

    goto :goto_2

    :cond_b
    if-eqz v4, :cond_c

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;-><init>()V

    const-string v0, "upgrade"

    iput-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;->c:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;->d:Ljava/util/Set;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/fn;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fn;-><init>()V

    iput-object v5, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/fn;->a:Ljava/util/List;

    iget-object v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/fn;->b:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;

    iget-object v3, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/fn;->b:Ljava/util/Set;

    iget-object v2, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/fn;->a:Ljava/util/List;

    invoke-direct {v0, v3, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;-><init>(Ljava/util/Set;Ljava/util/List;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;

    iput-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;->d:Ljava/util/Set;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iget-object v2, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;->d:Ljava/util/Set;

    iget-object v3, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;

    iget-object v4, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;

    iget-object v1, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/fj;->c:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    move-object v2, v0

    goto/16 :goto_0

    .line 307
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->pageScroll(I)Z

    .line 308
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->a:Lcom/google/android/gms/plus/oob/e;

    sget-object v1, Lcom/google/android/gms/common/analytics/l;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/oob/e;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto/16 :goto_1

    .line 310
    :cond_e
    sget v1, Lcom/google/android/gms/j;->bO:I

    if-ne v0, v1, :cond_2

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->a:Lcom/google/android/gms/plus/oob/e;

    sget-object v1, Lcom/google/android/gms/common/analytics/l;->w:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/oob/e;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->e:Lcom/google/android/gms/plus/oob/aa;

    invoke-interface {v0}, Lcom/google/android/gms/plus/oob/aa;->c()V

    goto/16 :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 159
    if-nez p1, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "upgrade_account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    .line 161
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/oob/y;->l:I

    .line 166
    :goto_0
    return-void

    .line 163
    :cond_0
    const-string v0, "state_upgrade_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    .line 164
    const-string v0, "state_scroll_y"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/oob/y;->l:I

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 171
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 172
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_setup_wizard_theme"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/y;->j:Z

    .line 174
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->bH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    if-lez v0, :cond_5

    const/4 v4, -0x2

    if-ne v2, v4, :cond_5

    iget-boolean v2, p0, Lcom/google/android/gms/plus/oob/y;->j:Z

    if-nez v2, :cond_5

    iput-boolean v8, p0, Lcom/google/android/gms/plus/oob/y;->m:Z

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xd

    if-lt v5, v6, :cond_4

    invoke-virtual {v2, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    :goto_0
    invoke-virtual {v1}, Landroid/support/v4/app/q;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget v2, v4, Landroid/graphics/Point;->x:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v1, v0, v3}, Landroid/view/Window;->setLayout(II)V

    .line 176
    :goto_1
    sget v0, Lcom/google/android/gms/l;->ez:I

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 179
    sget v0, Lcom/google/android/gms/j;->go:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->b:Landroid/widget/LinearLayout;

    .line 181
    sget v0, Lcom/google/android/gms/j;->qM:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a(Lcom/google/android/gms/common/ui/o;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a(Lcom/google/android/gms/common/ui/n;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/plus/oob/z;

    invoke-direct {v2, p0}, Lcom/google/android/gms/plus/oob/z;-><init>(Lcom/google/android/gms/plus/oob/y;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 192
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/y;->j:Z

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/plus/oob/ae;->a(Landroid/view/LayoutInflater;Landroid/view/View;Z)V

    .line 193
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->f:Landroid/widget/TextView;

    .line 195
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/y;->j:Z

    if-nez v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "promo_app_package"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 200
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/q;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 202
    sget v0, Lcom/google/android/gms/j;->sU:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 203
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :cond_0
    :goto_2
    sget v0, Lcom/google/android/gms/j;->pX:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->h:Landroid/view/View;

    .line 211
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/y;->j:Z

    if-eqz v0, :cond_6

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->h:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 231
    :cond_1
    :goto_3
    invoke-virtual {p0, p1, v1}, Lcom/google/android/gms/plus/oob/y;->a(Landroid/view/LayoutInflater;Landroid/view/View;)V

    .line 232
    sget v0, Lcom/google/android/gms/j;->mw:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->c:Landroid/widget/Button;

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->c:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 235
    sget v0, Lcom/google/android/gms/j;->bO:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->d:Landroid/widget/Button;

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "back_button_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->k:Ljava/lang/String;

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->k:Ljava/lang/String;

    .line 243
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/y;->j:Z

    if-nez v0, :cond_3

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->d:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/y;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->d:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 251
    :cond_3
    :goto_4
    return-object v1

    .line 174
    :cond_4
    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v5

    iput v5, v4, Landroid/graphics/Point;->x:I

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    iput v2, v4, Landroid/graphics/Point;->y:I

    goto/16 :goto_0

    :cond_5
    iput-boolean v7, p0, Lcom/google/android/gms/plus/oob/y;->m:Z

    goto/16 :goto_1

    .line 214
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "promo_app_text"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 215
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 217
    sget v0, Lcom/google/android/gms/j;->pY:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 218
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 219
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 220
    :cond_7
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 222
    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/oob/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 223
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 224
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/y;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->uL:I

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v2, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 248
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/y;->d:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_4

    :catch_0
    move-exception v0

    goto/16 :goto_2
.end method

.method public onDetach()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 144
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 145
    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->e:Lcom/google/android/gms/plus/oob/aa;

    .line 146
    iput-object v0, p0, Lcom/google/android/gms/plus/oob/y;->a:Lcom/google/android/gms/plus/oob/e;

    .line 147
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 151
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 152
    const-string v0, "state_upgrade_account"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/y;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 153
    const-string v0, "state_scroll_y"

    iget v1, p0, Lcom/google/android/gms/plus/oob/y;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 154
    return-void
.end method
