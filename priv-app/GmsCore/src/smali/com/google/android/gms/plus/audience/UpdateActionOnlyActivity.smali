.class public final Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/plus/audience/c;
.implements Lcom/google/android/gms/plus/f/f;


# instance fields
.field private a:Landroid/support/v4/app/q;

.field private b:Lcom/google/android/gms/common/api/v;

.field private c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

.field private d:Lcom/google/android/gms/people/m;

.field private e:Lcom/google/android/gms/common/people/data/AudienceMember;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 75
    iput-object p0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a:Landroid/support/v4/app/q;

    .line 76
    return-void
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 149
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->setResult(ILandroid/content/Intent;)V

    .line 150
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->finish()V

    .line 151
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 349
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_1

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->e:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    .line 351
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 352
    sget v0, Lcom/google/android/gms/p;->vU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 360
    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/bz;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/audience/bz;

    move-result-object v0

    .line 362
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "errorDialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 365
    return-void

    .line 354
    :cond_0
    sget v1, Lcom/google/android/gms/p;->vV:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 358
    :cond_1
    sget v0, Lcom/google/android/gms/p;->vT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;I)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;Lcom/google/android/gms/people/m;)V
    .locals 6

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->d:Lcom/google/android/gms/people/m;

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->b()Z

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->b()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/circles/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c()V

    goto :goto_0
.end method

.method private b()I
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const/4 v0, 0x0

    .line 165
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->h:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->g:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/plus/a/m;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    new-instance v4, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->k:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->l:I

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;-><init>(Ljava/lang/String;I)V

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->i:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 209
    sget v0, Lcom/google/android/gms/p;->sG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/f/a;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/f/e;

    move-result-object v0

    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "progressDialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 218
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "AddToCircle"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/b;

    .line 220
    if-nez v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->e:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->j:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->h:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/audience/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/b;

    move-result-object v0

    .line 224
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "AddToCircle"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->b()I

    .line 227
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/b;->a()V

    .line 229
    :cond_0
    return-void
.end method


# virtual methods
.method public final Y_()V
    .locals 2

    .prologue
    .line 311
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->setResult(ILandroid/content/Intent;)V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->finish()V

    .line 313
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    .line 341
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->d()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 344
    new-instance v1, Lcom/google/android/gms/plus/audience/ca;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/plus/audience/ca;-><init>(Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;Lcom/google/android/gms/common/api/Status;)V

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->d:Lcom/google/android/gms/people/m;

    .line 345
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/n;)V
    .locals 4

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a:Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progressDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    .line 262
    if-eqz v0, :cond_0

    .line 263
    invoke-virtual {v0}, Lcom/google/android/gms/plus/f/e;->a()V

    .line 266
    :cond_0
    if-nez p1, :cond_1

    .line 267
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 281
    :goto_0
    return-void

    .line 270
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/people/n;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 271
    invoke-interface {p1}, Lcom/google/android/gms/people/n;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    .line 275
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/audience/a/n;

    invoke-direct {v0}, Lcom/google/android/gms/common/audience/a/n;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->j:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/n;->a:Landroid/content/Intent;

    const-string v3, "EXTRA_TARGET_CIRCLE_ID"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->e:Lcom/google/android/gms/common/people/data/AudienceMember;

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/n;->a:Landroid/content/Intent;

    const-string v3, "EXTRA_UPDATE_PERSON"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, v0, Lcom/google/android/gms/common/audience/a/n;->a:Landroid/content/Intent;

    .line 280
    const/4 v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-eqz v0, :cond_0

    .line 329
    :goto_0
    return-void

    .line 320
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->b:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/j;->c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/audience/by;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/audience/by;-><init>(Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 336
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 182
    const/16 v0, 0x7d0

    if-ne p1, v0, :cond_3

    .line 183
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    if-eq p2, v1, :cond_0

    if-ne p2, v1, :cond_1

    .line 186
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c()V

    .line 198
    :goto_0
    return-void

    .line 188
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progressDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    .line 190
    if-eqz v0, :cond_2

    .line 191
    invoke-virtual {v0}, Lcom/google/android/gms/plus/f/e;->a()V

    .line 193
    :cond_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 196
    :cond_3
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/q;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 79
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 82
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    const-string v0, "UpdateActionOnlyActivity"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->i:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    const-string v0, "EXTRA_ACCOUNT_NAME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->f:Ljava/lang/String;

    .line 96
    const-string v0, "EXTRA_PLUS_PAGE_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->g:Ljava/lang/String;

    .line 97
    const-string v0, "EXTRA_CLIENT_APPLICATION_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->h:Ljava/lang/String;

    .line 98
    const-string v0, "EXTRA_TARGET_CIRCLE_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->j:Ljava/lang/String;

    .line 99
    const-string v0, "EXTRA_UPDATE_PERSON"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->e:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 100
    const-string v0, "EXTRA_START_VIEW_NAMESPACE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "sg"

    :cond_2
    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->k:Ljava/lang/String;

    .line 101
    const-string v0, "EXTRA_START_VIEW_TYPE_NUM"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->l:I

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->f:Ljava/lang/String;

    const-string v1, "Account name must not be empty."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->e:Lcom/google/android/gms/common/people/data/AudienceMember;

    const-string v1, "Update person must not be null."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->j:Ljava/lang/String;

    const-string v1, "Target circleId must not be null."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 107
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    sget-object v1, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    new-instance v2, Lcom/google/android/gms/people/ad;

    invoke-direct {v2}, Lcom/google/android/gms/people/ad;-><init>()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->b()I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v2}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->b:Lcom/google/android/gms/common/api/v;

    .line 113
    iput-object v4, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    .line 114
    iput-object v4, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->d:Lcom/google/android/gms/people/m;

    .line 116
    if-eqz p1, :cond_0

    .line 120
    const-string v0, "addToCircleConsentData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    .line 123
    const-string v0, "addToCircleConsentDataResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "addToCircleConsentDataResultIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const-string v0, "addToCircleConsentDataResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "addToCircleConsentDataResultIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v4, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 131
    new-instance v0, Lcom/google/android/gms/plus/audience/ca;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/audience/ca;-><init>(Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;Lcom/google/android/gms/common/api/Status;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->d:Lcom/google/android/gms/people/m;

    goto/16 :goto_0

    .line 91
    :catch_0
    move-exception v0

    invoke-direct {p0, v3, v4}, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->a(ILandroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 138
    const-string v0, "addToCircleConsentData"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->c:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->d:Lcom/google/android/gms/people/m;

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "addToCircleConsentDataResultCode"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->d:Lcom/google/android/gms/people/m;

    invoke-interface {v1}, Lcom/google/android/gms/people/m;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    const-string v0, "addToCircleConsentDataResultIntent"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->d:Lcom/google/android/gms/people/m;

    invoke-interface {v1}, Lcom/google/android/gms/people/m;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->i()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 146
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 170
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 172
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/UpdateActionOnlyActivity;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 177
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 178
    return-void
.end method
