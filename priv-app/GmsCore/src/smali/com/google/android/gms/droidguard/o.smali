.class final Lcom/google/android/gms/droidguard/o;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/droidguard/DroidGuardService;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/droidguard/DroidGuardService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/droidguard/o;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    .line 70
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/droidguard/DroidGuardService;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/droidguard/o;-><init>(Lcom/google/android/gms/droidguard/DroidGuardService;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final k(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/droidguard/o;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/droidguard/o;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-virtual {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 83
    sget-object v0, Lcom/google/android/gms/droidguard/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/droidguard/o;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Lcom/google/android/gms/droidguard/e/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/o;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/DroidGuardService;->a(Lcom/google/android/gms/droidguard/DroidGuardService;)Lcom/google/android/gms/droidguard/e/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/droidguard/e/a;->a()V
    :try_end_0
    .catch Lcom/google/android/gms/droidguard/e/c; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/gms/droidguard/l;

    iget-object v1, p0, Lcom/google/android/gms/droidguard/o;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-direct {v0, v1, p3}, Lcom/google/android/gms/droidguard/l;-><init>(Lcom/google/android/gms/droidguard/DroidGuardService;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/droidguard/l;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 95
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_1
    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 99
    :goto_1
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string v1, "DroidGuardService"

    const-string v2, "Waiting for Widevine device certificate provisioning."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 99
    :catch_1
    move-exception v0

    goto :goto_1
.end method
