.class public final Lcom/google/android/gms/udc/e/q;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/udc/e/q;


# instance fields
.field public a:Lcom/google/android/gms/udc/e/p;

.field public b:Lcom/google/android/gms/udc/e/s;

.field public c:Lcom/google/android/gms/udc/e/k;

.field public d:Lcom/google/android/gms/udc/e/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    iput-object v0, p0, Lcom/google/android/gms/udc/e/q;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/q;->cachedSize:I

    .line 37
    return-void
.end method

.method public static a()[Lcom/google/android/gms/udc/e/q;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/gms/udc/e/q;->e:[Lcom/google/android/gms/udc/e/q;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/gms/udc/e/q;->e:[Lcom/google/android/gms/udc/e/q;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/udc/e/q;

    sput-object v0, Lcom/google/android/gms/udc/e/q;->e:[Lcom/google/android/gms/udc/e/q;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/gms/udc/e/q;->e:[Lcom/google/android/gms/udc/e/q;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 132
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 133
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    if-eqz v1, :cond_0

    .line 134
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    if-eqz v1, :cond_1

    .line 138
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    if-eqz v1, :cond_2

    .line 142
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    if-eqz v1, :cond_3

    .line 146
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 52
    const/4 v0, 0x1

    .line 94
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/q;

    if-eqz v1, :cond_0

    .line 57
    check-cast p1, Lcom/google/android/gms/udc/e/q;

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    if-nez v1, :cond_6

    .line 59
    iget-object v1, p1, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    if-nez v1, :cond_0

    .line 67
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_7

    .line 68
    iget-object v1, p1, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 76
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    if-nez v1, :cond_8

    .line 77
    iget-object v1, p1, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    if-nez v1, :cond_0

    .line 85
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_9

    .line 86
    iget-object v1, p1, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 94
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/q;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 63
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 72
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 81
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 90
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 102
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 104
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 106
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 108
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/q;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    return v0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/p;->hashCode()I

    move-result v0

    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_1

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/k;->hashCode()I

    move-result v0

    goto :goto_2

    .line 106
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/q;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/udc/e/p;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/udc/e/k;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    if-eqz v0, :cond_0

    .line 116
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_1

    .line 119
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    if-eqz v0, :cond_2

    .line 122
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->c:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 124
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_3

    .line 125
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/udc/e/q;->d:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 127
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 128
    return-void
.end method
