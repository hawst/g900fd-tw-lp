.class final Lcom/google/android/gms/cast/media/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/ApplicationMetadata;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Z

.field final synthetic e:Lcom/google/android/gms/cast/media/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/d;Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 795
    iput-object p1, p0, Lcom/google/android/gms/cast/media/j;->e:Lcom/google/android/gms/cast/media/d;

    iput-object p2, p0, Lcom/google/android/gms/cast/media/j;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    iput-object p3, p0, Lcom/google/android/gms/cast/media/j;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/cast/media/j;->c:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/gms/cast/media/j;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 798
    iget-object v0, p0, Lcom/google/android/gms/cast/media/j;->e:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/y;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 804
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/j;->e:Lcom/google/android/gms/cast/media/d;

    iget-object v1, v1, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    iget-object v1, v1, Lcom/google/android/gms/cast/media/y;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 806
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 807
    iget-object v5, p0, Lcom/google/android/gms/cast/media/j;->a:Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v6, p0, Lcom/google/android/gms/cast/media/j;->b:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/cast/media/j;->c:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/google/android/gms/cast/media/j;->d:Z

    iget-object v8, v0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v9, "onApplicationConnected: sessionId=%s"

    new-array v10, v3, [Ljava/lang/Object;

    aput-object v7, v10, v2

    invoke-virtual {v8, v9, v10}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v8, v0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v9, "mSession = %s"

    new-array v10, v3, [Ljava/lang/Object;

    iget-object v11, v0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    aput-object v11, v10, v2

    invoke-virtual {v8, v9, v10}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v8, v0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    if-eqz v8, :cond_2

    iget-object v8, v0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    invoke-virtual {v8, v5, v7}, Lcom/google/android/gms/cast/media/ae;->a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;)V

    :cond_2
    iget-object v5, v0, Lcom/google/android/gms/cast/media/o;->b:Ljava/lang/String;

    invoke-static {v6, v5}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iput-object v6, v0, Lcom/google/android/gms/cast/media/o;->b:Ljava/lang/String;

    move v0, v3

    :goto_2
    or-int/2addr v0, v1

    move v1, v0

    .line 810
    goto :goto_1

    :cond_3
    move v0, v2

    .line 807
    goto :goto_2

    .line 812
    :cond_4
    if-eqz v1, :cond_0

    .line 813
    iget-object v0, p0, Lcom/google/android/gms/cast/media/j;->e:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->f(Lcom/google/android/gms/cast/media/a;)V

    goto :goto_0
.end method
