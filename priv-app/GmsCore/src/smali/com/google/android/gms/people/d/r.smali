.class public final Lcom/google/android/gms/people/d/r;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 199
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 200
    iput v0, p0, Lcom/google/android/gms/people/d/r;->a:I

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/r;->b:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/r;->cachedSize:I

    .line 201
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 250
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 251
    iget v1, p0, Lcom/google/android/gms/people/d/r;->a:I

    if-eqz v1, :cond_0

    .line 252
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/people/d/r;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/r;->b:Z

    if-eqz v1, :cond_1

    .line 256
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/r;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 259
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 212
    if-ne p1, p0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return v0

    .line 215
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 216
    goto :goto_0

    .line 218
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/r;

    .line 219
    iget v2, p0, Lcom/google/android/gms/people/d/r;->a:I

    iget v3, p1, Lcom/google/android/gms/people/d/r;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 220
    goto :goto_0

    .line 222
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/r;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/r;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 223
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 230
    iget v0, p0, Lcom/google/android/gms/people/d/r;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 232
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/r;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 233
    return v0

    .line 232
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 176
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/r;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/r;->b:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 239
    iget v0, p0, Lcom/google/android/gms/people/d/r;->a:I

    if-eqz v0, :cond_0

    .line 240
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/people/d/r;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 242
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/r;->b:Z

    if-eqz v0, :cond_1

    .line 243
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/r;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 245
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 246
    return-void
.end method
