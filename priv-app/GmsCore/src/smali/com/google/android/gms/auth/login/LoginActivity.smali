.class public Lcom/google/android/gms/auth/login/LoginActivity;
.super Lcom/google/android/gms/auth/login/g;
.source "SourceFile"


# instance fields
.field private q:Lcom/google/android/gms/auth/login/al;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Z

.field private v:Z

.field private w:Lcom/google/android/gms/auth/login/ak;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/g;-><init>()V

    .line 95
    return-void
.end method

.method private a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 145
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 146
    if-eqz p1, :cond_1

    .line 147
    invoke-static {p1}, Lcom/google/android/gms/auth/a/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v1

    .line 148
    invoke-static {v1}, Lcom/google/android/gms/auth/a/h;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/a/h;->b(Landroid/content/Intent;)V

    .line 149
    const-string v2, "authtoken"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 150
    if-eqz v2, :cond_0

    .line 151
    const-string v3, "authtoken"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    :cond_0
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/al;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    const-string v1, "booleanResult"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 157
    :cond_1
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/LoginActivity;)Lcom/google/android/gms/auth/login/ak;
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->w:Lcom/google/android/gms/auth/login/ak;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 550
    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/al;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const/16 v2, 0x15

    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/al;->j()I

    move-result v2

    if-ne v2, v0, :cond_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    .line 551
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->b()V

    .line 555
    :goto_1
    return-void

    :cond_0
    move v2, v0

    .line 550
    goto :goto_0

    .line 553
    :cond_1
    new-instance v2, Lcom/google/android/gms/auth/login/be;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/login/be;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/al;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/gms/auth/login/be;->a:Landroid/content/Intent;

    const-string v5, "account_name"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/al;->i()Z

    move-result v3

    iget-object v4, v2, Lcom/google/android/gms/auth/login/be;->a:Landroid/content/Intent;

    const-string v5, "is_confirming_credentials"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/al;->j()I

    move-result v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v1, v2, Lcom/google/android/gms/auth/login/be;->a:Landroid/content/Intent;

    const-string v3, "is_adding_account"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, v2, Lcom/google/android/gms/auth/login/be;->a:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/16 v1, 0x402

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/dataservice/bg;Z)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->c()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v0

    const-string v2, "Calling app cannot be null!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/al;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v3, "facl"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/al;->g()Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v2, v2, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v3, "options"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->u:Z

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->b(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->j()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/al;->c()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v2

    .line 245
    if-eqz p2, :cond_0

    .line 246
    invoke-virtual {v2, p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/dataservice/bg;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->w:Lcom/google/android/gms/auth/login/ak;

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->w:Lcom/google/android/gms/auth/login/ak;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/ak;->cancel(Z)Z

    .line 253
    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/login/ak;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->s:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->t:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/auth/login/LoginActivity;->v:Z

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/al;->i()Z

    move-result v7

    move-object v1, p0

    move v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/auth/login/ak;-><init>(Lcom/google/android/gms/auth/login/LoginActivity;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->w:Lcom/google/android/gms/auth/login/ak;

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->w:Lcom/google/android/gms/auth/login/ak;

    new-array v1, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/ak;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 282
    :goto_1
    return-void

    :cond_2
    move v0, v8

    .line 235
    goto :goto_0

    .line 264
    :cond_3
    const-string v0, "GLSActivity"

    const-string v3, "Starting LoginActivityTask for user: %s..."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v8

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->s:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->t:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/auth/login/LoginActivity;->v:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->i()Z

    move-result v7

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "allow_credit_card"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    move-object v1, p0

    move v6, p3

    invoke-static/range {v1 .. v9}, Lcom/google/android/gms/auth/login/LoginActivityTask;->a(Landroid/content/Context;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 278
    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/k;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 589
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->e()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->u:Z

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/al;->j()I

    move-result v1

    if-nez v1, :cond_0

    move v4, v5

    :goto_0
    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/k;ZZZ)Landroid/content/Intent;

    move-result-object v0

    .line 596
    const/16 v1, 0x3f1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 597
    return-void

    .line 589
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 610
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/al;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->t:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/al;->j()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/login/al;->l()Z

    move-result v4

    iget-object v6, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v6, v6, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v7, "allowed_domains"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v7, v7, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v8, "purchaser_email"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v8, v8, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v9, "purchaser_name"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/auth/login/BrowserActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 620
    const/16 v1, 0x3ec

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 621
    return-void

    :cond_0
    move v3, v5

    .line 610
    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 558
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v2, "grant_credential_response_status"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 561
    invoke-static {p1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/login/af;

    move-result-object v3

    .line 563
    iget-object v0, v3, Lcom/google/android/gms/auth/login/af;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    .line 565
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-ne v0, v2, :cond_2

    .line 566
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v2, v3, Lcom/google/android/gms/auth/login/af;->c:Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/login/al;->a(Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)Lcom/google/android/gms/auth/login/al;

    .line 568
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->g()Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    move-result-object v0

    .line 569
    iget-object v2, v3, Lcom/google/android/gms/auth/login/af;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 570
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;->a()Ljava/lang/String;

    move-result-object v0

    .line 571
    :goto_0
    new-instance v2, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    iget-object v4, v3, Lcom/google/android/gms/auth/login/af;->b:Ljava/lang/String;

    invoke-direct {v2, v0, v4}, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 575
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/login/al;->a(Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;)Lcom/google/android/gms/auth/login/al;

    .line 576
    iget-object v0, v3, Lcom/google/android/gms/auth/login/af;->d:Lcom/google/android/gms/auth/firstparty/dataservice/bg;

    invoke-direct {p0, v1, v0, v5}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/dataservice/bg;Z)V

    .line 586
    :goto_2
    return-void

    :cond_0
    move-object v0, v1

    .line 570
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 573
    goto :goto_1

    .line 579
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "errorCode"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "errorMessage"

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 582
    invoke-static {v0}, Lcom/google/android/gms/auth/a/h;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/h;->b(Landroid/content/Intent;)V

    .line 583
    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v5, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 584
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto :goto_2
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x2

    const/4 v9, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 337
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/g;->onActivityResult(IILandroid/content/Intent;)V

    .line 338
    const-string v0, "GLSActivity"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    if-nez p3, :cond_1

    const-string v0, "NO_INTENT"

    .line 344
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/gms/auth/login/LoginActivity;->k:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " #onActivityResult(requestCode: %s, resultCode %s, )"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 346
    const-string v5, "GLSActivity"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    aput-object v0, v6, v8

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_0
    if-ne p2, v1, :cond_3

    .line 351
    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(I)V

    .line 352
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    .line 499
    :goto_1
    return-void

    .line 339
    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "NO_EXTRAS"

    goto :goto_0

    :cond_2
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 356
    :cond_3
    if-nez p2, :cond_5

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->j()I

    move-result v0

    if-nez v0, :cond_4

    packed-switch p1, :pswitch_data_0

    :cond_4
    :pswitch_0
    invoke-direct {p0, p3}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "is_browser_only"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->l()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->a()V

    goto :goto_1

    .line 361
    :cond_5
    sparse-switch p1, :sswitch_data_0

    .line 496
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/login/g;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1

    .line 363
    :sswitch_0
    const/4 v0, 0x6

    if-ne p2, v0, :cond_6

    .line 364
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->b()V

    goto :goto_1

    .line 367
    :cond_6
    invoke-static {p3}, Lcom/google/android/gms/auth/login/UsernamePasswordActivity;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/login/bf;

    move-result-object v0

    .line 369
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v5, v0, Lcom/google/android/gms/auth/login/bf;->a:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/google/android/gms/auth/login/al;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;

    .line 370
    iget-object v0, v0, Lcom/google/android/gms/auth/login/bf;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->s:Ljava/lang/String;

    .line 373
    :sswitch_1
    const/16 v0, 0xb

    if-ne p2, v0, :cond_7

    .line 374
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(I)V

    .line 375
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    .line 377
    :cond_7
    if-ne p2, v9, :cond_9

    .line 378
    invoke-static {p3}, Lcom/google/android/gms/auth/login/aa;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/login/aa;

    move-result-object v0

    .line 379
    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/aa;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->t:Ljava/lang/String;

    .line 380
    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/aa;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 381
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/aa;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/gms/auth/login/al;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;

    .line 383
    :cond_8
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v5, v0, Lcom/google/android/gms/auth/login/aa;->a:Landroid/os/Bundle;

    const-string v6, "is_terms_of_services_newly_accepted"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iget-object v3, v3, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v6, "is_terms_of_services_newly_accepted"

    invoke-virtual {v3, v6, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 385
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/aa;->a:Landroid/os/Bundle;

    const-string v5, "is_new_account"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v3, v3, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v5, "is_new_account"

    invoke-virtual {v3, v5, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 391
    :cond_9
    :sswitch_2
    if-ne p2, v9, :cond_b

    .line 392
    invoke-static {p3}, Lcom/google/android/gms/auth/login/CaptchaActivity;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->r:Ljava/lang/String;

    if-eqz v0, :cond_1c

    if-eqz v3, :cond_1c

    .line 395
    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/LoginActivity;->r:Ljava/lang/String;

    invoke-direct {v0, v5, v3}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v0

    .line 400
    :goto_2
    const/16 v0, 0x3ec

    if-ne p1, v0, :cond_a

    move v0, v1

    :goto_3
    invoke-direct {p0, v3, v4, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/dataservice/bg;Z)V

    goto/16 :goto_1

    :cond_a
    move v0, v2

    goto :goto_3

    .line 402
    :cond_b
    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(I)V

    .line 403
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto/16 :goto_1

    .line 407
    :sswitch_3
    invoke-direct {p0, p3}, Lcom/google/android/gms/auth/login/LoginActivity;->b(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 410
    :sswitch_4
    invoke-static {p3}, Lcom/google/android/gms/auth/login/ba;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v5

    .line 416
    sget-object v0, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v3

    if-eq v0, v3, :cond_c

    sget-object v0, Lcom/google/android/gms/auth/firstparty/shared/k;->p:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v3

    if-ne v0, v3, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->j()I

    move-result v0

    if-nez v0, :cond_13

    .line 420
    :cond_c
    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a()Ljava/lang/String;

    move-result-object v0

    .line 421
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/al;->e()Ljava/lang/String;

    move-result-object v3

    .line 424
    if-eqz v0, :cond_12

    .line 426
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/auth/login/al;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;

    .line 430
    :goto_4
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v3, v3, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v4, "is_terms_of_services_newly_accepted"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 431
    const-string v3, "GLSActivity"

    const-string v4, "Sending Google play TOS broadcast..."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.vending.TOS_ACKED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 433
    const-string v4, "com.android.vending"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 434
    const-string v4, "TosAckedReceiver.account"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 441
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 442
    const-string v4, "com.android.vending.TOS_ACKED"

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/auth/login/LoginActivity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 444
    const-string v3, "GLSActivity"

    const-string v4, "Sending Chrome TOS broadcast..."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.chrome.TOS_ACKED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 446
    const-string v4, "com.android.chrome"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 447
    const-string v4, "TosAckedReceiver.account"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    const-string v4, "com.android.chrome.TOS_ACKED"

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/auth/login/LoginActivity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 451
    :cond_d
    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/login/al;->i()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v6, v6, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v7, "is_new_account"

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    if-nez v4, :cond_e

    sget-object v8, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-ne v3, v8, :cond_f

    :cond_e
    move v2, v1

    :cond_f
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    invoke-static {v3}, Lcom/google/android/gms/auth/a/h;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/google/android/gms/auth/a/h;->b(Landroid/content/Intent;)V

    if-eqz v4, :cond_10

    const-string v2, "authtoken"

    invoke-virtual {v7, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_10
    if-eqz v5, :cond_11

    const-string v2, "booleanResult"

    invoke-virtual {v7, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_11
    const-string v1, "accountType"

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "authAccount"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "is_new_account"

    invoke-virtual {v7, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 457
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/google/android/gms/auth/login/a/b;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    .line 458
    invoke-virtual {p0, v9, v7}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(ILandroid/content/Intent;)V

    .line 459
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto/16 :goto_1

    .line 427
    :cond_12
    if-eqz v3, :cond_1b

    move-object v0, v3

    .line 428
    goto/16 :goto_4

    .line 462
    :cond_13
    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/al;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;

    .line 468
    :cond_14
    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->b:Lcom/google/android/gms/auth/firstparty/shared/k;

    if-ne v0, v1, :cond_15

    .line 469
    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/k;)V

    goto/16 :goto_1

    .line 471
    :cond_15
    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v0

    const-string v1, "GLSActivity"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_16

    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " #handleTokenResponse - status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_16
    sget-object v1, Lcom/google/android/gms/auth/login/aj;->a:[I

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/k;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    :cond_17
    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/k;)V

    goto/16 :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->a()V

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->r()I

    move-result v6

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->c()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/login/al;->c()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->d()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/al;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/al;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->p()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->q()Z

    move-result v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/Collection;ZI)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x403

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    :pswitch_4
    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->o()Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    move-result-object v1

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->r:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->o()Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->a(Landroid/graphics/Bitmap;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 477
    :sswitch_5
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->a()V

    goto/16 :goto_1

    .line 481
    :sswitch_6
    const/4 v0, 0x6

    if-ne p2, v0, :cond_18

    .line 482
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->b()V

    goto/16 :goto_1

    .line 483
    :cond_18
    const/4 v0, 0x5

    if-ne p2, v0, :cond_19

    .line 484
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->a()V

    goto/16 :goto_1

    .line 485
    :cond_19
    if-ne p2, v9, :cond_1a

    .line 487
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->b()V

    goto/16 :goto_1

    .line 490
    :cond_1a
    invoke-virtual {p0, p2}, Lcom/google/android/gms/auth/login/LoginActivity;->setResult(I)V

    .line 491
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->finish()V

    goto/16 :goto_1

    :cond_1b
    move-object v0, v4

    goto/16 :goto_4

    :cond_1c
    move-object v3, v4

    goto/16 :goto_2

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 361
    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_2
        0x3ea -> :sswitch_4
        0x3ec -> :sswitch_1
        0x3f1 -> :sswitch_6
        0x402 -> :sswitch_0
        0x403 -> :sswitch_3
        0x409 -> :sswitch_5
    .end sparse-switch

    .line 471
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 180
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onCreate(Landroid/os/Bundle;)V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    const-string v0, "GLSActivity"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/LoginActivity;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " #onCreate - isFinishing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    if-nez p1, :cond_c

    .line 189
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 191
    if-nez v3, :cond_2

    .line 193
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LoginActivity requires extra data"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_2
    new-instance v4, Lcom/google/android/gms/auth/login/al;

    invoke-direct {v4, v3}, Lcom/google/android/gms/auth/login/al;-><init>(Landroid/os/Bundle;)V

    iput-object v4, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    .line 196
    iget-object v3, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/login/al;->j()I

    move-result v3

    if-eq v3, v1, :cond_b

    .line 198
    invoke-static {v0}, Lcom/google/android/gms/auth/a/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v0

    .line 199
    if-nez v0, :cond_3

    .line 200
    sget-object v0, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    .line 203
    :cond_3
    sget-object v3, Lcom/google/android/gms/auth/login/aj;->a:[I

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/k;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 213
    invoke-direct {p0, v8, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/k;)V

    goto :goto_0

    .line 205
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v3, "is_setup_wizard"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p0}, Lcom/google/android/gms/auth/login/a/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    invoke-static {p0}, Lcom/google/android/gms/auth/login/a/a;->a(Landroid/content/Context;)Z

    move-result v3

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v5, "com.google.android.setupwizard"

    const-string v6, "com.google.android.setupwizard.GoogleServicesActivity"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "agreePlayEmail"

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/gms/c;->c:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    const-string v7, "device_country"

    invoke-static {p0, v7, v8}, Lcom/google/android/gms/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "google_setup:play_email_opt_in"

    invoke-static {v8, v9}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v6, "google_setup:play_email_opt_in"

    invoke-static {v2, v6, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    :goto_2
    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz v0, :cond_4

    const-string v1, "agreeBackup"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_4
    if-eqz v3, :cond_5

    const-string v0, "agreeRestore"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_5
    const/16 v0, 0x411

    invoke-virtual {p0, v4, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    if-eqz v7, :cond_8

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    if-nez v6, :cond_a

    :cond_8
    const-string v1, "GLSActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Problem looking up Google Play email default; countryCode="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " optInCountries="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    move v1, v2

    goto :goto_2

    :cond_a
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    goto :goto_2

    .line 208
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/LoginActivity;->a()V

    goto/16 :goto_0

    .line 218
    :cond_b
    invoke-direct {p0, v8, v8, v2}, Lcom/google/android/gms/auth/login/LoginActivity;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/dataservice/bg;Z)V

    goto/16 :goto_0

    .line 221
    :cond_c
    new-instance v0, Lcom/google/android/gms/auth/login/al;

    invoke-direct {v0, p1}, Lcom/google/android/gms/auth/login/al;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->h()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->h()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/LoginActivity;->b(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 203
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->w:Lcom/google/android/gms/auth/login/ak;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->w:Lcom/google/android/gms/auth/login/ak;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/ak;->cancel(Z)Z

    .line 175
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onDestroy()V

    .line 176
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/auth/login/LoginActivity;->q:Lcom/google/android/gms/auth/login/al;

    new-instance v1, Landroid/os/Bundle;

    iget-object v0, v0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 168
    return-void
.end method
