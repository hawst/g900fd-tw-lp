.class public final Lcom/google/android/gms/auth/f/k;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field private c:Z

.field private d:I

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:I

.field private i:Z

.field private j:J

.field private k:Z

.field private l:I

.field private m:Z

.field private n:J

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Ljava/util/List;

.field private v:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1196
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 1226
    iput v1, p0, Lcom/google/android/gms/auth/f/k;->b:I

    .line 1243
    iput v1, p0, Lcom/google/android/gms/auth/f/k;->d:I

    .line 1260
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/f/k;->f:Ljava/lang/String;

    .line 1277
    iput v1, p0, Lcom/google/android/gms/auth/f/k;->h:I

    .line 1294
    iput-wide v2, p0, Lcom/google/android/gms/auth/f/k;->j:J

    .line 1311
    iput v1, p0, Lcom/google/android/gms/auth/f/k;->l:I

    .line 1328
    iput-wide v2, p0, Lcom/google/android/gms/auth/f/k;->n:J

    .line 1345
    iput-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->p:Z

    .line 1362
    iput-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->r:Z

    .line 1379
    iput-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->t:Z

    .line 1395
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/f/k;->u:Ljava/util/List;

    .line 1484
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/f/k;->v:I

    .line 1196
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1487
    iget v0, p0, Lcom/google/android/gms/auth/f/k;->v:I

    if-gez v0, :cond_0

    .line 1489
    invoke-virtual {p0}, Lcom/google/android/gms/auth/f/k;->b()I

    .line 1491
    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/f/k;->v:I

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->a:Z

    .line 1231
    iput p1, p0, Lcom/google/android/gms/auth/f/k;->b:I

    .line 1232
    return-object p0
.end method

.method public final a(J)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1298
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->i:Z

    .line 1299
    iput-wide p1, p0, Lcom/google/android/gms/auth/f/k;->j:J

    .line 1300
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/f/g;)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1412
    if-nez p1, :cond_0

    .line 1413
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1415
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/f/k;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1416
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/f/k;->u:Ljava/util/List;

    .line 1418
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/f/k;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1419
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1349
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->o:Z

    .line 1350
    iput-boolean p1, p0, Lcom/google/android/gms/auth/f/k;->p:Z

    .line 1351
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 1193
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/k;->a(I)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/k;->b(I)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->e:Z

    iput-object v0, p0, Lcom/google/android/gms/auth/f/k;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/k;->c(I)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/f/k;->a(J)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/k;->d(I)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/f/k;->b(J)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/k;->a(Z)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/k;->b(Z)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/k;->c(Z)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_b
    new-instance v0, Lcom/google/android/gms/auth/f/g;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/g;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/k;->a(Lcom/google/android/gms/auth/f/g;)Lcom/google/android/gms/auth/f/k;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 1449
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->a:Z

    if-eqz v0, :cond_0

    .line 1450
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/auth/f/k;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1452
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->c:Z

    if-eqz v0, :cond_1

    .line 1453
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/auth/f/k;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1455
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->e:Z

    if-eqz v0, :cond_2

    .line 1456
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/auth/f/k;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1458
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->g:Z

    if-eqz v0, :cond_3

    .line 1459
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/auth/f/k;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1461
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->i:Z

    if-eqz v0, :cond_4

    .line 1462
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/auth/f/k;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 1464
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->k:Z

    if-eqz v0, :cond_5

    .line 1465
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/gms/auth/f/k;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1467
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->m:Z

    if-eqz v0, :cond_6

    .line 1468
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/android/gms/auth/f/k;->n:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 1470
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->o:Z

    if-eqz v0, :cond_7

    .line 1471
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 1473
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->q:Z

    if-eqz v0, :cond_8

    .line 1474
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->r:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 1476
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->s:Z

    if-eqz v0, :cond_9

    .line 1477
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->t:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 1479
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/auth/f/k;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/f/g;

    .line 1480
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    goto :goto_0

    .line 1482
    :cond_a
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 1496
    const/4 v0, 0x0

    .line 1497
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->a:Z

    if-eqz v1, :cond_0

    .line 1498
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/auth/f/k;->b:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1501
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->c:Z

    if-eqz v1, :cond_1

    .line 1502
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/auth/f/k;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1505
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->e:Z

    if-eqz v1, :cond_2

    .line 1506
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/auth/f/k;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1509
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->g:Z

    if-eqz v1, :cond_3

    .line 1510
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/auth/f/k;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1513
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->i:Z

    if-eqz v1, :cond_4

    .line 1514
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/auth/f/k;->j:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1517
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->k:Z

    if-eqz v1, :cond_5

    .line 1518
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/auth/f/k;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1521
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->m:Z

    if-eqz v1, :cond_6

    .line 1522
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/gms/auth/f/k;->n:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1525
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->o:Z

    if-eqz v1, :cond_7

    .line 1526
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/k;->p:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1529
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->q:Z

    if-eqz v1, :cond_8

    .line 1530
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/k;->r:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1533
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/k;->s:Z

    if-eqz v1, :cond_9

    .line 1534
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/k;->t:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1537
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/auth/f/k;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/f/g;

    .line 1538
    const/16 v3, 0xb

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1540
    goto :goto_0

    .line 1541
    :cond_a
    iput v1, p0, Lcom/google/android/gms/auth/f/k;->v:I

    .line 1542
    return v1
.end method

.method public final b(I)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1247
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->c:Z

    .line 1248
    iput p1, p0, Lcom/google/android/gms/auth/f/k;->d:I

    .line 1249
    return-object p0
.end method

.method public final b(J)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1332
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->m:Z

    .line 1333
    iput-wide p1, p0, Lcom/google/android/gms/auth/f/k;->n:J

    .line 1334
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1366
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->q:Z

    .line 1367
    iput-boolean p1, p0, Lcom/google/android/gms/auth/f/k;->r:Z

    .line 1368
    return-object p0
.end method

.method public final c(I)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->g:Z

    .line 1282
    iput p1, p0, Lcom/google/android/gms/auth/f/k;->h:I

    .line 1283
    return-object p0
.end method

.method public final c(Z)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1383
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->s:Z

    .line 1384
    iput-boolean p1, p0, Lcom/google/android/gms/auth/f/k;->t:Z

    .line 1385
    return-object p0
.end method

.method public final d(I)Lcom/google/android/gms/auth/f/k;
    .locals 1

    .prologue
    .line 1315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/k;->k:Z

    .line 1316
    iput p1, p0, Lcom/google/android/gms/auth/f/k;->l:I

    .line 1317
    return-object p0
.end method
