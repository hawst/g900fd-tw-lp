.class public Lcom/google/android/gms/gcm/http/GoogleHttpService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/gcm/http/a;

.field private final b:Lcom/google/android/gms/http/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 42
    new-instance v0, Lcom/google/android/gms/gcm/http/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/gcm/http/c;-><init>(Lcom/google/android/gms/gcm/http/GoogleHttpService;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/http/GoogleHttpService;->b:Lcom/google/android/gms/http/h;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/gcm/http/GoogleHttpService;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 26
    invoke-static {p0}, Lcom/google/android/gms/gcm/http/d;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/http/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/http/d;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/http/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/http/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-ne v1, p1, :cond_1

    const-string v0, "GCM.VHTTP"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCM.VHTTP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HTTP: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_4

    const-string v1, "block"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "GCM.HTTP"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HTTP request blocked due to server rule: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v0

    :cond_1
    const-string v0, "GCM.HTTP"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GCM.HTTP"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http rule: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-nez v1, :cond_3

    const-string v1, "block"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v2, "rewrite"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_5

    const-string v1, "rewrite"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "rewrite"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a:Lcom/google/android/gms/gcm/http/a;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/gcm/http/a;->a(Ljava/lang/String;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "temporary_blocked"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GCM.HTTP"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HTTP request blocked due to http moratorium: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "temporary_blocked"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GCM.HTTP"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HTTP request blocked due to automatic backoff: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 144
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 145
    const-string v1, "name"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v1, "block"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/gcm/http/GoogleHttpService;Ljava/lang/String;I)V
    .locals 10

    .prologue
    const/4 v5, 0x3

    .line 26
    iget-object v1, p0, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a:Lcom/google/android/gms/gcm/http/a;

    iget-boolean v0, v1, Lcom/google/android/gms/gcm/http/a;->d:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/gcm/http/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, v1, Lcom/google/android/gms/gcm/http/a;->c:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v3

    if-lez p2, :cond_2

    const/16 v0, 0x1f4

    if-ge p2, v0, :cond_2

    const/16 v0, 0x1ad

    if-eq p2, v0, :cond_2

    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/gcm/http/a;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    if-lez p2, :cond_1

    invoke-static {p0}, Lcom/google/android/gms/gcm/nts/n;->b(Landroid/content/Context;)V

    :cond_1
    return-void

    :cond_2
    :try_start_1
    iget-object v0, v1, Lcom/google/android/gms/gcm/http/a;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/http/b;

    if-nez v0, :cond_4

    iget-object v0, v1, Lcom/google/android/gms/gcm/http/a;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    const/16 v4, 0x64

    if-lt v0, v4, :cond_3

    const-string v0, "GCM.HTTP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Backoff lookup map has grown too big. Not considering for backoff newly failing url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    :try_start_2
    new-instance v0, Lcom/google/android/gms/gcm/http/b;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/gcm/http/b;-><init>(Lcom/google/android/gms/gcm/http/a;B)V

    iget-object v1, v1, Lcom/google/android/gms/gcm/http/a;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    iget v1, v0, Lcom/google/android/gms/gcm/http/b;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/gcm/http/b;->a:I

    iget v1, v0, Lcom/google/android/gms/gcm/http/b;->a:I

    if-ne v1, v5, :cond_6

    const-wide v4, 0x408f400000000000L    # 1000.0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide v8, 0x40c1940000000000L    # 9000.0

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v1, v4

    iput v1, v0, Lcom/google/android/gms/gcm/http/b;->b:I

    :cond_5
    :goto_1
    iput p2, v0, Lcom/google/android/gms/gcm/http/b;->d:I

    monitor-exit v3

    goto :goto_0

    :cond_6
    iget v1, v0, Lcom/google/android/gms/gcm/http/b;->a:I

    if-le v1, v5, :cond_5

    iget v1, v0, Lcom/google/android/gms/gcm/http/b;->b:I

    int-to-long v4, v1

    iget v1, v0, Lcom/google/android/gms/gcm/http/b;->a:I

    int-to-long v6, v1

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const-wide/32 v8, 0x5265c00

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    add-long/2addr v4, v6

    long-to-int v1, v4

    iput v1, v0, Lcom/google/android/gms/gcm/http/b;->b:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget v1, v0, Lcom/google/android/gms/gcm/http/b;->b:I

    int-to-long v6, v1

    add-long/2addr v4, v6

    iput-wide v4, v0, Lcom/google/android/gms/gcm/http/b;->c:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 189
    const-string v0, "\nGoogleHttpService stats:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a:Lcom/google/android/gms/gcm/http/a;

    const-string v1, "BackoffStatuses:"

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/gcm/http/a;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "["

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v1, "failureCount: "

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/gcm/http/b;

    iget v1, v1, Lcom/google/android/gms/gcm/http/b;->a:I

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(I)V

    const-string v1, ", currentBackoff: "

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/http/b;

    iget v0, v0, Lcom/google/android/gms/gcm/http/b;->b:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, "]"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 191
    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 78
    const-string v0, "GCM.HTTP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "GCM.HTTP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Binding: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/http/GoogleHttpService;->b:Lcom/google/android/gms/http/h;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 86
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 87
    const-string v0, "GCM.HTTP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "GCM.HTTP"

    const-string v1, "onCreate called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    invoke-static {}, Lcom/google/android/gms/gcm/http/a;->a()Lcom/google/android/gms/gcm/http/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a:Lcom/google/android/gms/gcm/http/a;

    .line 92
    iget-object v1, p0, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a:Lcom/google/android/gms/gcm/http/a;

    const-string v0, "gcm_hc_backoff"

    invoke-static {p0, v0, v4, v5}, Lcom/google/android/gms/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-boolean v2, v1, Lcom/google/android/gms/gcm/http/a;->d:Z

    if-eq v2, v0, :cond_1

    iput-boolean v0, v1, Lcom/google/android/gms/gcm/http/a;->d:Z

    iget-object v0, v1, Lcom/google/android/gms/gcm/http/a;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a:Lcom/google/android/gms/gcm/http/a;

    const-string v1, "gcm_hc_duration"

    const-wide/16 v2, -0x1

    invoke-static {p0, v1, v2, v3}, Lcom/google/android/gms/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/gcm/http/a;->e:J

    .line 96
    return-void

    .line 92
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 101
    const-string v0, "GCM.HTTP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "GCM.HTTP"

    const-string v1, "onDestroy called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x2

    return v0
.end method
