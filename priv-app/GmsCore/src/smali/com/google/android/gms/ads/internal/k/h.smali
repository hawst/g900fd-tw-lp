.class final Lcom/google/android/gms/ads/internal/k/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/k/f;

.field final synthetic b:Lcom/google/android/gms/ads/internal/k/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/k/g;Lcom/google/android/gms/ads/internal/k/f;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/k/h;->b:Lcom/google/android/gms/ads/internal/k/g;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/k/h;->a:Lcom/google/android/gms/ads/internal/k/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/h;->b:Lcom/google/android/gms/ads/internal/k/g;

    iget-object v13, v0, Lcom/google/android/gms/ads/internal/k/g;->e:Ljava/lang/Object;

    monitor-enter v13

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/h;->b:Lcom/google/android/gms/ads/internal/k/g;

    iget v0, v0, Lcom/google/android/gms/ads/internal/k/g;->h:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    .line 133
    monitor-exit v13

    .line 147
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/h;->b:Lcom/google/android/gms/ads/internal/k/g;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/h;->b:Lcom/google/android/gms/ads/internal/k/g;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/k/g;->a()Lcom/google/android/gms/ads/internal/k/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/k/g;->g:Lcom/google/android/gms/ads/internal/k/a/d;

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/h;->b:Lcom/google/android/gms/ads/internal/k/g;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/k/g;->g:Lcom/google/android/gms/ads/internal/k/a/d;

    if-nez v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/h;->b:Lcom/google/android/gms/ads/internal/k/g;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/k/g;->a(I)V

    .line 141
    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v13

    throw v0

    .line 145
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/h;->a:Lcom/google/android/gms/ads/internal/k/f;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/h;->b:Lcom/google/android/gms/ads/internal/k/g;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/k/f;->a(Lcom/google/android/gms/ads/internal/k/j;)V

    .line 146
    iget-object v14, p0, Lcom/google/android/gms/ads/internal/k/h;->b:Lcom/google/android/gms/ads/internal/k/g;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/k/h;->a:Lcom/google/android/gms/ads/internal/k/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, v14, Lcom/google/android/gms/ads/internal/k/g;->f:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget v0, v0, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->d:I

    const v1, 0x3e8fa0

    if-ge v0, v1, :cond_3

    iget-object v0, v14, Lcom/google/android/gms/ads/internal/k/g;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, v14, Lcom/google/android/gms/ads/internal/k/g;->g:Lcom/google/android/gms/ads/internal/k/a/d;

    iget-object v1, v14, Lcom/google/android/gms/ads/internal/k/g;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v1

    iget-object v2, v14, Lcom/google/android/gms/ads/internal/k/g;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v3, v14, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/k/c;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v5}, Lcom/google/android/gms/ads/internal/k/a/d;->a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/g;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 147
    :goto_1
    :try_start_3
    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 146
    :cond_2
    :try_start_4
    iget-object v0, v14, Lcom/google/android/gms/ads/internal/k/g;->g:Lcom/google/android/gms/ads/internal/k/a/d;

    iget-object v1, v14, Lcom/google/android/gms/ads/internal/k/g;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v1

    iget-object v2, v14, Lcom/google/android/gms/ads/internal/k/g;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v3, v14, Lcom/google/android/gms/ads/internal/k/g;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v4, v14, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/k/c;->g:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/k/a/d;->a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/g;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_5
    const-string v1, "Could not request ad from mediation adapter."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x5

    invoke-virtual {v14, v0}, Lcom/google/android/gms/ads/internal/k/g;->a(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :cond_3
    :try_start_6
    iget-object v0, v14, Lcom/google/android/gms/ads/internal/k/g;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_4

    iget-object v0, v14, Lcom/google/android/gms/ads/internal/k/g;->g:Lcom/google/android/gms/ads/internal/k/a/d;

    iget-object v1, v14, Lcom/google/android/gms/ads/internal/k/g;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v1

    iget-object v2, v14, Lcom/google/android/gms/ads/internal/k/g;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v3, v14, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/k/c;->g:Ljava/lang/String;

    iget-object v4, v14, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v4, v4, Lcom/google/android/gms/ads/internal/k/c;->a:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/k/a/d;->a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/g;)V

    goto :goto_1

    :cond_4
    iget-object v6, v14, Lcom/google/android/gms/ads/internal/k/g;->g:Lcom/google/android/gms/ads/internal/k/a/d;

    iget-object v0, v14, Lcom/google/android/gms/ads/internal/k/g;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v7

    iget-object v8, v14, Lcom/google/android/gms/ads/internal/k/g;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v9, v14, Lcom/google/android/gms/ads/internal/k/g;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v0, v14, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v10, v0, Lcom/google/android/gms/ads/internal/k/c;->g:Ljava/lang/String;

    iget-object v0, v14, Lcom/google/android/gms/ads/internal/k/g;->a:Lcom/google/android/gms/ads/internal/k/c;

    iget-object v11, v0, Lcom/google/android/gms/ads/internal/k/c;->a:Ljava/lang/String;

    move-object v12, v5

    invoke-interface/range {v6 .. v12}, Lcom/google/android/gms/ads/internal/k/a/d;->a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/g;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1
.end method
