.class public final Lcom/google/android/gms/photos/autobackup/ui/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/ui/s;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/widget/Toast;

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->e:Z

    .line 20
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->a:Landroid/content/Context;

    .line 21
    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->b:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->b:Landroid/widget/Toast;

    .line 61
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->b:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 62
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->b:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->e()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->c:I

    .line 66
    iput v1, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->d:I

    .line 67
    iput-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->e:Z

    .line 68
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->dy:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/x;->a(Ljava/lang/CharSequence;)V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->e:Z

    .line 53
    return-void
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V
    .locals 2

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->e:Z

    if-eqz v0, :cond_0

    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/photos/autobackup/ui/x;->c(Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->dA:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/x;->a(Ljava/lang/CharSequence;)V

    .line 30
    return-void
.end method

.method public final b(Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 34
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->e:Z

    if-eqz v0, :cond_0

    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/photos/autobackup/ui/x;->c(Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;)V

    .line 37
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->e()I

    move-result v3

    .line 38
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->d()F

    move-result v0

    float-to-double v4, v0

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    .line 39
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupStatus;->f()I

    move-result v4

    add-int/2addr v4, v3

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    iget v4, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->c:I

    sub-int/2addr v0, v4

    .line 41
    iget v4, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->d:I

    if-le v0, v4, :cond_1

    .line 42
    iput v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->d:I

    .line 44
    :cond_1
    iget v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->c:I

    sub-int v0, v3, v0

    add-int/lit8 v0, v0, 0x1

    .line 45
    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->dz:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    iget v0, p0, Lcom/google/android/gms/photos/autobackup/ui/x;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/photos/autobackup/ui/x;->a(Ljava/lang/CharSequence;)V

    .line 47
    return-void

    :cond_2
    move v0, v2

    .line 38
    goto :goto_0

    :cond_3
    move v0, v2

    .line 39
    goto :goto_1
.end method
