.class public final Lcom/google/android/gms/plus/service/a/am;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;

.field private c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

.field private final d:Ljava/util/List;

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZLcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/am;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 47
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/am;->b:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/am;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 49
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/am;->d:Ljava/util/List;

    .line 50
    iput-boolean p5, p0, Lcom/google/android/gms/plus/service/a/am;->e:Z

    .line 51
    iput-boolean p6, p0, Lcom/google/android/gms/plus/service/a/am;->f:Z

    .line 52
    iput-boolean p7, p0, Lcom/google/android/gms/plus/service/a/am;->g:Z

    .line 53
    iput-boolean p8, p0, Lcom/google/android/gms/plus/service/a/am;->h:Z

    .line 54
    iput-object p9, p0, Lcom/google/android/gms/plus/service/a/am;->i:Lcom/google/android/gms/plus/internal/f;

    .line 55
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x4

    const/4 v10, 0x0

    .line 60
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/am;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/am;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/a/am;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/am;->d:Ljava/util/List;

    iget-boolean v6, p0, Lcom/google/android/gms/plus/service/a/am;->e:Z

    iget-boolean v7, p0, Lcom/google/android/gms/plus/service/a/am;->f:Z

    iget-boolean v8, p0, Lcom/google/android/gms/plus/service/a/am;->g:Z

    iget-boolean v9, p0, Lcom/google/android/gms/plus/service/a/am;->h:Z

    iget-object v0, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gms/plus/b/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZ)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/am;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "81"

    const-string v3, "SetAppFacl"

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/people/pub/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 66
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/am;->i:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 78
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 69
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v10, v0, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 70
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/am;->i:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v0, v11, v1}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 73
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/am;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/am;->i:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v1, v11, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 76
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/am;->i:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v12}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/am;->i:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/am;->i:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;)V

    .line 85
    :cond_0
    return-void
.end method
