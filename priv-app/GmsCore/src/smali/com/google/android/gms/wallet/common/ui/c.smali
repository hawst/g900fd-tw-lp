.class final Lcom/google/android/gms/wallet/common/ui/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 187
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->f()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 188
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;Z)V

    .line 190
    new-instance v1, Lcom/google/checkout/inapp/proto/z;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/z;-><init>()V

    .line 191
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h()Lcom/google/t/a/b;

    move-result-object v2

    iput-object v2, v1, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    .line 192
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->a(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 194
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    .line 196
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 197
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->b(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    .line 199
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 200
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->c(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Lcom/google/checkout/inapp/proto/q;

    move-result-object v2

    iput-object v2, v1, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    .line 202
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d(Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 203
    iget-object v2, v1, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/t/a/b;->t:Ljava/lang/String;

    .line 206
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d:Landroid/widget/CheckBox;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 209
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/z;Z)V

    .line 214
    :goto_1
    return-void

    .line 206
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 212
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/c;->a:Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddAddressActivity;->g()Z

    goto :goto_1
.end method
