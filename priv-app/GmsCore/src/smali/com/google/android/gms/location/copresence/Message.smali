.class public final Lcom/google/android/gms/location/copresence/Message;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/gms/location/copresence/n;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/n;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput p1, p0, Lcom/google/android/gms/location/copresence/Message;->a:I

    .line 88
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/Message;->b:Ljava/lang/String;

    .line 89
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/Message;->c:Ljava/lang/String;

    .line 90
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/Message;->d:[B

    .line 91
    array-length v0, p4

    const/16 v1, 0x3e8

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Payloads may be at most 1000 bytes"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 93
    return-void

    .line 91
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/location/copresence/Message;->a:I

    .line 118
    iput-object p1, p0, Lcom/google/android/gms/location/copresence/Message;->b:Ljava/lang/String;

    .line 119
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/Message;->c:Ljava/lang/String;

    .line 120
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/Message;->d:[B

    .line 121
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 4

    .prologue
    .line 103
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/gms/location/copresence/Message;-><init>(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 107
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/gms/location/copresence/Message;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/Message;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/Message;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/Message;->d:[B

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 161
    if-ne p0, p1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 164
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/location/copresence/Message;

    if-nez v2, :cond_2

    move v0, v1

    .line 165
    goto :goto_0

    .line 167
    :cond_2
    check-cast p1, Lcom/google/android/gms/location/copresence/Message;

    .line 168
    iget-object v2, p0, Lcom/google/android/gms/location/copresence/Message;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/Message;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/Message;->d:[B

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/Message;->d:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 152
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/Message;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/Message;->d:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Message["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/Message;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 147
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/n;->a(Lcom/google/android/gms/location/copresence/Message;Landroid/os/Parcel;)V

    .line 148
    return-void
.end method
