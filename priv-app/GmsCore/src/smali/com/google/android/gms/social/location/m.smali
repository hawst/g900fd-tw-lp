.class final Lcom/google/android/gms/social/location/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/social/location/j;


# direct methods
.method constructor <init>(Lcom/google/android/gms/social/location/j;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/google/android/gms/social/location/m;->a:Lcom/google/android/gms/social/location/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, 0x10000000

    .line 425
    check-cast p1, Lcom/google/android/gms/location/reporting/h;

    iget-object v0, p0, Lcom/google/android/gms/social/location/m;->a:Lcom/google/android/gms/social/location/j;

    iput-object p1, v0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    iget-object v0, p0, Lcom/google/android/gms/social/location/m;->a:Lcom/google/android/gms/social/location/j;

    iget-object v0, v0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    invoke-interface {v0}, Lcom/google/android/gms/location/reporting/h;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/social/location/m;->a:Lcom/google/android/gms/social/location/j;

    iput-object v1, v0, Lcom/google/android/gms/social/location/j;->e:Landroid/content/Intent;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/m;->a:Lcom/google/android/gms/social/location/j;

    iget-object v0, v0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    invoke-interface {v0}, Lcom/google/android/gms/location/reporting/h;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/social/location/m;->a:Lcom/google/android/gms/social/location/j;

    iget-object v0, p0, Lcom/google/android/gms/social/location/m;->a:Lcom/google/android/gms/social/location/j;

    iget-object v2, v0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/social/location/j;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.MANAGE_NETWORK_USAGE"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.apps.maps"

    const-string v5, "com.google.android.maps.MapsActivity"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :goto_1
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v4, 0x10000

    invoke-virtual {v2, v0, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v5, :cond_2

    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    if-nez v5, :cond_4

    :cond_2
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_5

    :goto_3
    iput-object v0, v3, Lcom/google/android/gms/social/location/j;->e:Landroid/content/Intent;

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.google.android.apps.maps.LOCATION_SETTINGS"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.apps.maps"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_1

    :cond_4
    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v2

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_3
.end method
