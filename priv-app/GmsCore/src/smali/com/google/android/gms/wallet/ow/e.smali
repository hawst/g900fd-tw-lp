.class final Lcom/google/android/gms/wallet/ow/e;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ow/d;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    const/16 v1, 0x19b

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;I)V

    .line 598
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/d;)V
    .locals 3

    .prologue
    .line 537
    iget v0, p1, Lcom/google/aa/b/a/a/a/a/d;->a:I

    packed-switch v0, :pswitch_data_0

    .line 549
    const-string v0, "AuthenticateInstrumentF"

    const-string v1, "Unexpected authenticate instrument response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;I)V

    .line 552
    :goto_0
    return-void

    .line 539
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/d;->h:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;)Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/aa/b/a/a/a/a/i;Z)V

    goto :goto_0

    .line 543
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Bq:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/q;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 545
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/d;->b(Lcom/google/android/gms/wallet/ow/d;)V

    goto :goto_0

    .line 537
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/j;J)V
    .locals 4

    .prologue
    .line 557
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v0, v0

    .line 558
    const/4 v1, 0x0

    .line 561
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 562
    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    aget v1, v1, v0

    packed-switch v1, :pswitch_data_0

    .line 568
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    const/16 v1, 0x19d

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;I)V

    .line 593
    :goto_1
    return-void

    .line 564
    :pswitch_0
    const/4 v1, 0x1

    .line 561
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 572
    :cond_0
    if-eqz v1, :cond_1

    .line 573
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Bq:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/q;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/d;->b(Lcom/google/android/gms/wallet/ow/d;)V

    goto :goto_1

    .line 578
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/d;->c(Lcom/google/android/gms/wallet/ow/d;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 580
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;)Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/d;->d(Lcom/google/android/gms/wallet/ow/d;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/j;Lcom/google/aa/b/a/a/a/a/i;Ljava/lang/String;J)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v0

    .line 586
    :goto_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 587
    const-string v2, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 588
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/d;->d(Lcom/google/android/gms/wallet/ow/d;)Landroid/accounts/Account;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gms/wallet/w;->a:Landroid/accounts/Account;

    .line 591
    :goto_3
    const-string v2, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 592
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    const/4 v2, -0x1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;ILandroid/content/Intent;)V

    goto :goto_1

    .line 584
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;Lcom/google/aa/b/a/a/a/a/j;)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v0

    goto :goto_2

    .line 588
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/d;->d(Lcom/google/android/gms/wallet/ow/d;)Landroid/accounts/Account;

    move-result-object v0

    goto :goto_3

    .line 562
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/b/a/c;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 607
    invoke-static {p1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/b/a/c;)I

    move-result v0

    .line 608
    invoke-static {p1, v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/b/a/c;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    iget-object v1, p1, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    iget-object v1, v1, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    .line 610
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    iget-object v3, v1, Lcom/google/aa/b/a/k;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/aa/b/a/k;->b:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;Ljava/lang/String;Ljava/lang/String;I)V

    .line 618
    :goto_0
    return-void

    .line 612
    :cond_0
    const/16 v1, 0x198

    if-ne v0, v1, :cond_1

    .line 613
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;ILandroid/content/Intent;)V

    goto :goto_0

    .line 615
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;I)V

    .line 628
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/d;->e(Lcom/google/android/gms/wallet/ow/d;)V

    .line 603
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 622
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/e;->a:Lcom/google/android/gms/wallet/ow/d;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/ow/d;I)V

    .line 623
    return-void
.end method
