.class public final Lcom/google/android/gms/auth/be/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;

.field private static b:Ljava/lang/ref/WeakReference;


# instance fields
.field private final c:Ljava/lang/Object;

.field private final d:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "FactoryReset"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "DataBlockManagerHelper"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/be/a/a;->a:Lcom/google/android/gms/auth/d/a;

    .line 31
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/gms/auth/be/a/a;->b:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    const-string v0, "persistent_data_block"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/be/a/a;-><init>(Ljava/lang/Object;)V

    .line 48
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/gms/auth/be/a/a;->c:Ljava/lang/Object;

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/be/a/a;->d:Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/a/a;
    .locals 3

    .prologue
    .line 34
    const-class v1, Lcom/google/android/gms/auth/be/a/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/auth/be/a/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/a/a;

    .line 35
    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/google/android/gms/auth/be/a/a;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/be/a/a;-><init>(Landroid/content/Context;)V

    .line 38
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lcom/google/android/gms/auth/be/a/a;->b:Ljava/lang/ref/WeakReference;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    :cond_0
    monitor-exit v1

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/be/a/a/a;)J
    .locals 11

    .prologue
    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Ljava/io/IOException;

    const-string v1, "DataBlockManager is not supported on current device."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    if-nez p1, :cond_1

    .line 108
    sget-object v0, Lcom/google/android/gms/auth/be/a/a;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Supplied DataBlockContainer is null. Proceeding to erase stored data."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 109
    new-array v0, v4, [B

    move-object v1, v0

    .line 113
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/auth/be/a/a;->d:Ljava/lang/Object;

    monitor-enter v4

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/a;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v5, "getMaximumDataBlockSize"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v0, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 117
    iget-object v5, p0, Lcom/google/android/gms/auth/be/a/a;->c:Ljava/lang/Object;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 118
    cmp-long v0, v6, v2

    if-nez v0, :cond_2

    .line 119
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error when getting data block maximum capacity."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :catch_0
    move-exception v0

    .line 132
    :try_start_1
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unable to write data to DataBlockManager."

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 111
    :cond_1
    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 121
    :cond_2
    :try_start_2
    array-length v0, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    int-to-long v8, v0

    cmp-long v0, v8, v6

    if-lez v0, :cond_3

    .line 122
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-wide v0, v2

    .line 130
    :goto_1
    return-wide v0

    .line 124
    :cond_3
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/a;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v5, "write"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, [B

    aput-object v10, v8, v9

    invoke-virtual {v0, v5, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 126
    iget-object v5, p0, Lcom/google/android/gms/auth/be/a/a;->c:Ljava/lang/Object;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    invoke-virtual {v0, v5, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->longValue()J

    move-result-wide v0

    .line 127
    cmp-long v2, v0, v2

    if-nez v2, :cond_4

    .line 128
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error when writing to data block."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 130
    :cond_4
    sub-long v0, v6, v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/a;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/auth/be/a/a/a;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 84
    :goto_0
    return-object v0

    .line 73
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/a;->d:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/a;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v3, "read"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/be/a/a;->c:Ljava/lang/Object;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 76
    if-eqz v0, :cond_1

    array-length v3, v0

    if-nez v3, :cond_2

    .line 77
    :cond_1
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 79
    :cond_2
    new-instance v3, Lcom/google/android/gms/auth/be/a/a/a;

    invoke-direct {v3}, Lcom/google/android/gms/auth/be/a/a/a;-><init>()V

    .line 80
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/a/a/a;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    sget-object v2, Lcom/google/android/gms/auth/be/a/a;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Failed to read data from DataBlockManager."

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 84
    goto :goto_0
.end method
