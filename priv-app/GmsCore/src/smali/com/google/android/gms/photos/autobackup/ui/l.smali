.class final Lcom/google/android/gms/photos/autobackup/ui/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/l;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 3

    .prologue
    .line 122
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/l;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->a(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/photos/autobackup/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/l;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/photos/autobackup/d;->c:Lcom/google/android/gms/photos/autobackup/f;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/l;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->d(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/l;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v2}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->a(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/photos/autobackup/l;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/f;->c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/l;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->c(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/common/api/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/l;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->e(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/l;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->f(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/l;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    sget v1, Lcom/google/android/gms/p;->dr:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
