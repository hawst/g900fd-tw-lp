.class public final Lcom/google/android/gms/drive/b/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field final b:Landroid/support/v4/g/t;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/d;->a:I

    .line 35
    new-instance v0, Landroid/support/v4/g/t;

    invoke-direct {v0}, Landroid/support/v4/g/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/b/a/d;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/d;->a:I

    .line 39
    iget-object v0, p1, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v0}, Landroid/support/v4/g/t;->a()Landroid/support/v4/g/t;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/g/t;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    .line 40
    iget v0, p1, Lcom/google/android/gms/drive/b/a/d;->a:I

    iput v0, p0, Lcom/google/android/gms/drive/b/a/d;->a:I

    .line 41
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v1, p0, Lcom/google/android/gms/drive/b/a/d;->a:I

    .line 49
    new-instance v0, Landroid/support/v4/g/t;

    invoke-direct {v0}, Landroid/support/v4/g/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    .line 50
    const-string v0, "map"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 51
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 52
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 54
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 56
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    move v0, v1

    .line 57
    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v0, v7, :cond_0

    .line 58
    new-instance v7, Lcom/google/android/gms/drive/b/a/c;

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/gms/drive/b/a/c;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v5, v4}, Landroid/support/v4/g/t;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 62
    :cond_1
    const-string v0, "size"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/d;->a:I

    .line 63
    return-void
.end method

.method static a(Ljava/util/List;Lcom/google/android/gms/drive/b/a/c;)Z
    .locals 3

    .prologue
    .line 124
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/a/c;

    .line 125
    iget-object v0, v0, Lcom/google/android/gms/drive/b/a/c;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/drive/b/a/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Ljava/util/List;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/t;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 137
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public final a()Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 77
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 78
    const-string v0, "size"

    iget v1, p0, Lcom/google/android/gms/drive/b/a/d;->a:I

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 79
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 80
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v0}, Landroid/support/v4/g/t;->b()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 81
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/t;->e(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/a/c;

    .line 83
    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/a/c;->a()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/t;->d(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 80
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 88
    :cond_1
    const-string v0, "map"

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 89
    return-object v2
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/t;->f(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 178
    if-ne p0, p1, :cond_1

    move v2, v3

    .line 193
    :cond_0
    :goto_0
    return v2

    .line 181
    :cond_1
    instance-of v0, p1, Lcom/google/android/gms/drive/b/a/d;

    if-eqz v0, :cond_0

    .line 182
    check-cast p1, Lcom/google/android/gms/drive/b/a/d;

    .line 183
    iget v0, p1, Lcom/google/android/gms/drive/b/a/d;->a:I

    iget v1, p0, Lcom/google/android/gms/drive/b/a/d;->a:I

    if-ne v0, v1, :cond_0

    move v1, v2

    .line 186
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v0}, Landroid/support/v4/g/t;->b()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/t;->e(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v4, p1, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    iget-object v5, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v5, v1}, Landroid/support/v4/g/t;->d(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/support/v4/g/t;->a(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v2, v3

    .line 191
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 198
    iget v1, p0, Lcom/google/android/gms/drive/b/a/d;->a:I

    .line 199
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 200
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v3}, Landroid/support/v4/g/t;->b()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 201
    iget-object v3, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v3, v0}, Landroid/support/v4/g/t;->d(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_0
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 204
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 205
    mul-int/lit8 v1, v1, 0x25

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v1, v3

    .line 206
    mul-int/lit8 v1, v1, 0x25

    iget-object v3, p0, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/support/v4/g/t;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 207
    goto :goto_1

    .line 208
    :cond_1
    return v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/b/a/d;->a()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 69
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
