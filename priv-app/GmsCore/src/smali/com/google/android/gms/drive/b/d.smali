.class public final Lcom/google/android/gms/drive/b/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/database/r;

.field final b:Lcom/google/android/gms/drive/g/w;

.field final c:Lcom/google/android/gms/drive/c/b;

.field final d:Lcom/google/android/gms/drive/g/i;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:Ljava/lang/Object;

.field volatile g:Ljava/util/Collection;

.field h:J

.field private final i:Landroid/content/Context;

.field private final j:Lcom/google/android/gms/drive/b/f;

.field private k:Lcom/google/android/gms/drive/g/al;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/w;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/c/b;Lcom/google/android/gms/drive/g/i;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->e:Ljava/util/concurrent/ExecutorService;

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->f:Ljava/lang/Object;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->g:Ljava/util/Collection;

    .line 57
    iput-object p1, p0, Lcom/google/android/gms/drive/b/d;->i:Landroid/content/Context;

    .line 58
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    .line 59
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/w;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    .line 60
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->j:Lcom/google/android/gms/drive/b/f;

    .line 61
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/c/b;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->c:Lcom/google/android/gms/drive/c/b;

    .line 62
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->d:Lcom/google/android/gms/drive/g/i;

    .line 63
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/gms/drive/g/al;
    .locals 5

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->k:Lcom/google/android/gms/drive/g/al;

    if-nez v0, :cond_0

    .line 67
    sget-object v0, Lcom/google/android/gms/drive/ai;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 68
    const-string v1, "ContentMaintenance"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContentMaintenance interval "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    sget-object v1, Lcom/google/android/gms/drive/g/an;->a:Lcom/google/android/gms/drive/g/am;

    new-instance v2, Lcom/google/android/gms/drive/b/e;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/b/e;-><init>(Lcom/google/android/gms/drive/b/d;)V

    iget-object v3, p0, Lcom/google/android/gms/drive/b/d;->e:Ljava/util/concurrent/ExecutorService;

    const-string v4, "ContentMaintenance"

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/google/android/gms/drive/g/am;->a(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)Lcom/google/android/gms/drive/g/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->k:Lcom/google/android/gms/drive/g/al;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->k:Lcom/google/android/gms/drive/g/al;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Lcom/google/android/gms/drive/database/model/ax;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 270
    iget-object v2, p1, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 271
    const-string v2, "ContentMaintenance"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Evicting from internal storage (will remain in shared storage): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 278
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/drive/b/d;->j:Lcom/google/android/gms/drive/b/f;

    iget-object v5, p1, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    iget-object v2, v4, Lcom/google/android/gms/drive/b/f;->d:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/g/w;->f()Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v0, "ContentManager"

    const-string v1, "Shared storage is not available; not moving content with hash: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 279
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_0
    .catch Lcom/google/android/gms/drive/g/s; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 286
    :goto_2
    return-void

    .line 274
    :cond_1
    const-string v2, "ContentMaintenance"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Moving from internal to shared storage: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 278
    :cond_2
    :try_start_1
    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/b/f;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v3, Lcom/google/android/gms/drive/g/h;->a:Ljavax/crypto/KeyGenerator;

    if-nez v3, :cond_3

    new-instance v0, Lcom/google/android/gms/drive/g/s;

    const-string v1, "KeyGenerator not initialized."

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/g/s;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/google/android/gms/drive/g/s; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 280
    :catch_0
    move-exception v0

    .line 281
    :try_start_2
    const-string v1, "ContentMaintenance"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to move content to shared storage: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    goto :goto_2

    .line 278
    :cond_3
    :try_start_3
    sget-object v3, Lcom/google/android/gms/drive/g/h;->a:Ljavax/crypto/KeyGenerator;

    invoke-virtual {v3}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v6

    iget-object v7, v2, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v2, v4, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2, v8}, Lcom/google/android/gms/drive/database/r;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/gms/drive/g/s; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v2, 0x0

    :try_start_4
    invoke-virtual {v4, v7, v2}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v4, v8, v3}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-static {v6, v2}, Lcom/google/android/gms/drive/g/g;->a(Ljava/security/Key;Ljava/io/OutputStream;)Ljavax/crypto/CipherOutputStream;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J

    iget-object v2, v4, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2, v5}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v9

    if-nez v6, :cond_5

    move v3, v0

    :goto_3
    if-nez v8, :cond_6

    move v2, v0

    :goto_4
    if-ne v3, v2, :cond_7

    move v2, v0

    :goto_5
    const-string v3, "encryptionKey must be set if and only if sharedFilename is set."

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    if-nez v8, :cond_4

    iget-object v2, v9, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    :goto_6
    const-string v1, "internal and shared filenames cannot both be null"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    :cond_4
    iput-object v8, v9, Lcom/google/android/gms/drive/database/model/ax;->c:Ljava/lang/String;

    iput-object v6, v9, Lcom/google/android/gms/drive/database/model/ax;->d:Ljavax/crypto/SecretKey;

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lcom/google/android/gms/drive/database/model/ax;->a(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/google/android/gms/drive/database/model/ax;->i()V

    const-string v0, "ContentManager"

    const-string v1, "Moved from internal storage (filename %s) to shared storage (filename %s): %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v7, v2, v3

    const/4 v3, 0x1

    aput-object v8, v2, v3

    const/4 v3, 0x2

    aput-object v5, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v0, v4, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, v8}, Lcom/google/android/gms/drive/database/r;->e(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/google/android/gms/drive/g/s; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_1

    .line 282
    :catch_1
    move-exception v0

    .line 283
    :try_start_6
    const-string v1, "ContentMaintenance"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to move content to shared storage: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    goto/16 :goto_2

    :cond_5
    move v3, v1

    .line 278
    goto :goto_3

    :cond_6
    move v2, v1

    goto :goto_4

    :cond_7
    move v2, v1

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_6

    :catchall_0
    move-exception v0

    :try_start_7
    iget-object v1, v4, Lcom/google/android/gms/drive/b/f;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, v8}, Lcom/google/android/gms/drive/database/r;->e(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Lcom/google/android/gms/drive/g/s; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 285
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 5

    .prologue
    .line 109
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/d;->g:Ljava/util/Collection;

    .line 110
    const-string v0, "ContentMaintenance"

    const-string v1, "Open hashes %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 111
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/drive/b/d;->a()Lcom/google/android/gms/drive/g/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/al;->a()V

    .line 87
    return-void
.end method

.method final c()V
    .locals 9

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/w;->f()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    sget-object v0, Lcom/google/android/gms/drive/ai;->at:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/drive/ai;->au:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/g/w;->b()J

    move-result-wide v4

    long-to-float v1, v4

    mul-float/2addr v0, v1

    float-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->r()J

    move-result-wide v0

    .line 191
    const-string v4, "ContentMaintenance"

    const-string v5, "Shared cache bytes used: %d; limit: %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 193
    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 196
    const-string v0, "ContentMaintenance"

    const-string v1, "Evicting LRU items from shared cache..."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 199
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->q()Lcom/google/android/gms/drive/database/b/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 201
    :try_start_1
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ax;

    .line 202
    iget-object v5, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v5}, Lcom/google/android/gms/drive/database/r;->r()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-lez v5, :cond_3

    .line 203
    iget-object v5, p0, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/g/w;->f()Ljava/io/File;

    move-result-object v5

    if-nez v5, :cond_2

    .line 206
    const-string v5, "ContentMaintenance"

    const-string v6, "External storage removed while pruning shared cache; aborting."

    invoke-static {v5, v6}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :cond_2
    const-string v5, "ContentMaintenance"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Evicting from shared cache: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v0, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ax;->j()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 213
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0

    .line 211
    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 213
    :try_start_4
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 216
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    goto/16 :goto_0
.end method

.method public final d()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 294
    const-string v0, "ContentMaintenance"

    const-string v2, "Beginning garbage collection."

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "collectGarbage() must not be run while in a database transaction"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 304
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/w;->e()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 305
    iget-object v5, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/gms/drive/database/r;->f(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 306
    const-string v5, "ContentMaintenance"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Deleting (internal): "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 304
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 301
    goto :goto_0

    .line 309
    :cond_1
    const-string v5, "ContentMaintenance"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Keeping (internal): "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 312
    :catch_0
    move-exception v0

    .line 313
    const-string v2, "ContentMaintenance"

    const-string v3, "Unable to open internal content directory; skipping internal content garbage collection."

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 316
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/w;->f()Ljava/io/File;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_4

    .line 318
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v2, v0

    :goto_3
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    .line 319
    iget-object v4, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/gms/drive/database/r;->f(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 320
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 321
    const-string v4, "ContentMaintenance"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Deleting (shared): "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 323
    :cond_3
    const-string v4, "ContentMaintenance"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Keeping (shared): "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 327
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/drive/b/d;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/l;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;)V

    .line 328
    const-string v0, "ContentMaintenance"

    const-string v1, "Finished garbage collection."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    return-void
.end method
