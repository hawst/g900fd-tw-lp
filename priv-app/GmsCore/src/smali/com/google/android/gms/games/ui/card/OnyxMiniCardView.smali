.class public final Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;
.super Lcom/google/android/gms/games/ui/card/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/card/g;


# instance fields
.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:I

.field private k:Landroid/support/v7/widget/bp;

.field private l:Landroid/widget/TextView;

.field private m:Z

.field private n:Landroid/view/View;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/view/View;

.field private q:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private r:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/bp;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->k:Landroid/support/v7/widget/bp;

    .line 168
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    const/4 v1, 0x4

    const/4 v4, 0x0

    const/16 v2, 0x8

    .line 79
    invoke-super {p0}, Lcom/google/android/gms/games/ui/card/a;->e()V

    .line 81
    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->g(I)V

    .line 84
    iget-object v3, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->n:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->m:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->g:Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->m:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->o:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->f:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->f:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->r:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->r:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->r:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/f;->r:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->a(Landroid/widget/TextView;I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->p:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 98
    return-void

    :cond_0
    move v0, v2

    .line 84
    goto :goto_0

    :cond_1
    move v1, v2

    .line 85
    goto :goto_1
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/a;->e(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->n:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 148
    return-void
.end method

.method public final g(I)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 172
    iput p1, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->j:I

    .line 173
    if-lez p1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->i:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 180
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->b:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 248
    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const-string v2, "icon"

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 249
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->c:Lcom/google/android/gms/games/ui/card/s;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/util/Pair;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/card/s;->u()V

    .line 258
    :goto_0
    return-void

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->h:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 251
    new-instance v0, Landroid/support/v7/widget/bn;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/bn;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 252
    iget v1, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->j:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bn;->a(I)V

    .line 253
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->k:Landroid/support/v7/widget/bp;

    iput-object v1, v0, Landroid/support/v7/widget/bn;->c:Landroid/support/v7/widget/bp;

    .line 254
    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->h:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/view/View;Landroid/support/v7/widget/bn;)V

    goto :goto_0

    .line 256
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/a;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected final onFinishInflate()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Lcom/google/android/gms/games/ui/card/a;->onFinishInflate()V

    .line 56
    sget v0, Lcom/google/android/gms/j;->nd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->h:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    sget v0, Lcom/google/android/gms/j;->ne:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->i:Landroid/view/View;

    .line 61
    sget v0, Lcom/google/android/gms/j;->sk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->l:Landroid/widget/TextView;

    .line 63
    sget v0, Lcom/google/android/gms/j;->pG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->n:Landroid/view/View;

    .line 64
    sget v0, Lcom/google/android/gms/j;->pH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->o:Landroid/widget/ImageView;

    .line 66
    const/4 v0, 0x2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->a(IF)V

    .line 68
    sget v0, Lcom/google/android/gms/j;->rQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->p:Landroid/view/View;

    .line 69
    sget v0, Lcom/google/android/gms/j;->rT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->r:Landroid/widget/TextView;

    .line 70
    sget v0, Lcom/google/android/gms/j;->rR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->q:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->q:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->an:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/OnyxMiniCardView;->q:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->ao:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 75
    return-void
.end method
