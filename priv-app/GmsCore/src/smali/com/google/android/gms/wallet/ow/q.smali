.class public final Lcom/google/android/gms/wallet/ow/q;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/android/gms/wallet/common/ui/bb;

.field c:I

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/TextView;

.field g:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field h:Landroid/widget/ProgressBar;

.field protected final i:Lcom/google/android/gms/wallet/service/l;

.field private j:Lcom/google/aa/b/a/a/a/a/g;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private n:Landroid/accounts/Account;

.field private o:Lcom/google/android/gms/wallet/common/ui/dh;

.field private p:Z

.field private q:Lcom/google/android/gms/wallet/payform/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "createWalletObjects"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ow/q;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/q;->c:I

    .line 261
    new-instance v0, Lcom/google/android/gms/wallet/ow/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ow/r;-><init>(Lcom/google/android/gms/wallet/ow/q;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->i:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/g;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/ow/q;
    .locals 3

    .prologue
    .line 94
    new-instance v0, Lcom/google/android/gms/wallet/ow/q;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/ow/q;-><init>()V

    .line 95
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 96
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 97
    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 98
    const-string v2, "request"

    invoke-static {v1, v2, p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 99
    const-string v2, "issuerName"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v2, "objectName"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ow/q;->setArguments(Landroid/os/Bundle;)V

    .line 102
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 240
    iget v0, p0, Lcom/google/android/gms/wallet/ow/q;->c:I

    if-gez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->b()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 242
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->b()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/q;->i:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/q;->c:I

    .line 246
    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 305
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->c()V

    .line 306
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 307
    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 308
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/q;->a(ILandroid/content/Intent;)V

    .line 309
    return-void
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 299
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->c()V

    .line 300
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/q;->setResult(ILandroid/content/Intent;)V

    .line 301
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    .line 302
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/q;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->b:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/q;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/q;->b:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->b:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->b:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->b:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/q;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "CreateWalletObjectsFragment.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/q;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/q;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/q;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, -0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/ow/q;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private b()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/ow/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/q;->p:Z

    .line 318
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->d()V

    .line 319
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 322
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/q;->g:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/q;->p:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 323
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/q;->h:Landroid/widget/ProgressBar;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/q;->p:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->q:Lcom/google/android/gms/wallet/payform/f;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->q:Lcom/google/android/gms/wallet/payform/f;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ow/q;->p:Z

    if-nez v3, :cond_3

    :goto_2
    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/payform/f;->a(Z)V

    .line 327
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 322
    goto :goto_0

    .line 323
    :cond_2
    const/4 v0, 0x4

    goto :goto_1

    :cond_3
    move v1, v2

    .line 325
    goto :goto_2
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/q;->a(I)V

    .line 215
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 139
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/q;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 142
    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->m:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 143
    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->n:Landroid/accounts/Account;

    .line 144
    const-string v0, "request"

    const-class v2, Lcom/google/aa/b/a/a/a/a/g;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/g;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->j:Lcom/google/aa/b/a/a/a/a/g;

    .line 146
    const-string v0, "issuerName"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->k:Ljava/lang/String;

    .line 147
    const-string v0, "objectName"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->l:Ljava/lang/String;

    .line 149
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->b()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-nez v0, :cond_0

    .line 150
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/q;->m:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/q;->n:Landroid/accounts/Account;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/q;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/ow/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->o:Lcom/google/android/gms/wallet/common/ui/dh;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/dh;->onAttach(Landroid/app/Activity;)V

    .line 165
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/wallet/payform/f;

    if-eqz v0, :cond_1

    .line 166
    check-cast p1, Lcom/google/android/gms/wallet/payform/f;

    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/q;->q:Lcom/google/android/gms/wallet/payform/f;

    .line 170
    :goto_0
    return-void

    .line 168
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->q:Lcom/google/android/gms/wallet/payform/f;

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->pB:I

    if-ne v0, v1, :cond_0

    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/q;->p:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->d()V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->b()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/q;->j:Lcom/google/aa/b/a/a/a/a/g;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/aa/b/a/a/a/a/g;)V

    .line 210
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 108
    sget v0, Lcom/google/android/gms/l;->gx:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 111
    sget v0, Lcom/google/android/gms/j;->dm:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->f:Landroid/widget/TextView;

    .line 112
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/q;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->j:Lcom/google/aa/b/a/a/a/a/g;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    array-length v0, v0

    if-lez v0, :cond_1

    sget v0, Lcom/google/android/gms/p;->As:I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/q;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    sget v0, Lcom/google/android/gms/j;->kC:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->d:Landroid/widget/TextView;

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/q;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    sget v0, Lcom/google/android/gms/j;->mI:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->e:Landroid/widget/TextView;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/q;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->g:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->g:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 123
    sget v0, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->h:Landroid/widget/ProgressBar;

    .line 125
    if-eqz p3, :cond_0

    .line 126
    const-string v0, "serviceConnectionSavePoint"

    const/4 v2, -0x1

    invoke-virtual {p3, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/q;->c:I

    .line 128
    const-string v0, "remoteOperationInProgress"

    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/q;->p:Z

    .line 132
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->d()V

    .line 134
    return-object v1

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->j:Lcom/google/aa/b/a/a/a/a/g;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    array-length v0, v0

    if-lez v0, :cond_2

    sget v0, Lcom/google/android/gms/p;->At:I

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CreateWalletObjectRequest should contain either loyalty object or offer object"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 201
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->q:Lcom/google/android/gms/wallet/payform/f;

    .line 203
    return-void
.end method

.method public final onPause()V
    .locals 0

    .prologue
    .line 187
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 188
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->a()V

    .line 189
    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 174
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/q;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "CreateWalletObjectsFragment.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->b:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->b:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/q;->b:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 182
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->b()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->b()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/q;->i:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ow/q;->c:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/q;->c:I

    .line 183
    :cond_1
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 193
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 194
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/q;->a()V

    .line 195
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ow/q;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 196
    const-string v0, "remoteOperationInProgress"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/q;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 197
    return-void
.end method
