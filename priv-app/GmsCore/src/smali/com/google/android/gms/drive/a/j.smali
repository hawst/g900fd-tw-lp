.class public final Lcom/google/android/gms/drive/a/j;
.super Lcom/google/android/gms/drive/a/b;
.source "SourceFile"


# instance fields
.field d:J

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:J

.field private final i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private j:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/a/a/l;)V
    .locals 8

    .prologue
    .line 98
    sget-object v2, Lcom/google/android/gms/drive/a/e;->d:Lcom/google/android/gms/drive/a/e;

    sget-object v6, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v7, p10

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;Lcom/google/android/gms/drive/a/a/l;)V

    .line 100
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    .line 101
    if-nez p5, :cond_0

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object p5

    :cond_0
    iput-object p5, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 103
    iput-wide p6, p0, Lcom/google/android/gms/drive/a/j;->h:J

    .line 104
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    .line 105
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/drive/a/j;->g:Ljava/lang/String;

    .line 106
    invoke-virtual/range {p10 .. p10}, Lcom/google/android/gms/drive/a/a/l;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    const-string v1, "baseContentHash cannot be null when conflict detection is required"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :cond_1
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 114
    sget-object v0, Lcom/google/android/gms/drive/a/e;->d:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 115
    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "pendingUploadSqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    .line 119
    :cond_0
    const-string v0, "contentHash"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    .line 120
    const-string v0, "baseContentHash"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    const-string v0, "baseContentHash"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    .line 122
    const-string v0, "baseContentSize"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/j;->d:J

    .line 128
    :goto_0
    iput-object v2, p0, Lcom/google/android/gms/drive/a/j;->g:Ljava/lang/String;

    .line 129
    const-string v0, "writeOpenTime"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    const-string v0, "writeOpenTime"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/j;->h:J

    .line 134
    :goto_1
    const-string v0, "metadataDelta"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135
    const-string v0, "metadataDelta"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 140
    :goto_2
    return-void

    .line 124
    :cond_1
    iput-object v2, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    .line 125
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/j;->d:J

    goto :goto_0

    .line 132
    :cond_2
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/j;->h:J

    goto :goto_1

    .line 138
    :cond_3
    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    goto :goto_2
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/j;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/b/b/n;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/l;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 330
    if-eqz p3, :cond_0

    .line 335
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/google/android/gms/drive/b/b/n;->a(Ljava/lang/String;)V

    .line 339
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v0

    .line 340
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/drive/a/j;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v3

    .line 341
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v4

    .line 343
    :try_start_0
    new-instance v5, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    invoke-direct {v5, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    .line 344
    invoke-virtual {v5, v3, v4}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)V

    .line 345
    const-string v3, "ContentAndMetadataAction"

    const-string v5, "Completed syncing entry (%s) after conflict detection"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {v3, v5, v6}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 358
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v3

    .line 359
    invoke-virtual {p0, v3}, Lcom/google/android/gms/drive/a/j;->f(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v3

    .line 360
    iget-object v4, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 362
    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 364
    :goto_0
    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    .line 368
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Upload failed. Filtered false positive conflict"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :catch_0
    move-exception v0

    .line 347
    const-string v3, "ContentAndMetadataAction"

    const-string v5, "Failed to sync entry (%s) after conflict detection"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-static {v3, v5, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 348
    throw v0

    .line 349
    :catch_1
    move-exception v0

    .line 355
    const-string v3, "ContentAndMetadataAction"

    const-string v5, "Failed to sync entry (%s) after conflict detection"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-static {v3, v5, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 356
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Etag mismatch and can\'t sync "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    move v0, v2

    .line 362
    goto :goto_0

    .line 372
    :cond_2
    new-instance v0, Lcom/google/android/gms/drive/a/i;

    invoke-direct {v0}, Lcom/google/android/gms/drive/a/i;-><init>()V

    .line 373
    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 16

    .prologue
    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/a/a/l;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 164
    const-string v2, "ContentAndMetadataAction"

    const-string v3, "Conflict detected in applyLocally. entryLocalContentHash: %s,action baseContentHash: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->p()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 166
    new-instance v2, Lcom/google/android/gms/drive/a/i;

    invoke-direct {v2}, Lcom/google/android/gms/drive/a/i;-><init>()V

    throw v2

    .line 169
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    if-nez v2, :cond_1

    .line 174
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/drive/a/j;->g:Ljava/lang/String;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v6

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/drive/a/j;->h:J

    move-object/from16 v2, p1

    invoke-interface/range {v2 .. v9}, Lcom/google/android/gms/drive/database/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)Lcom/google/android/gms/drive/database/model/bi;

    move-result-object v2

    .line 177
    iget-wide v2, v2, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    .line 180
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->q()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/gms/drive/a/j;->d:J

    .line 181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/database/model/ah;->i(Ljava/lang/String;)V

    .line 182
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->r()V

    .line 184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v3

    .line 185
    if-eqz v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    const-string v4, "Content does not exist: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v2, v4, v5}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 186
    iget-wide v2, v3, Lcom/google/android/gms/drive/database/model/ax;->h:J

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/ah;->a(J)V

    .line 190
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/gms/drive/a/j;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v9

    .line 193
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 194
    const-string v2, "ContentAndMetadataAction"

    const-string v3, "Apply locally completed, EntryRevision: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 197
    new-instance v3, Lcom/google/android/gms/drive/a/ak;

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/drive/a/j;->h:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/drive/a/j;->d:J

    invoke-direct/range {v3 .. v15}, Lcom/google/android/gms/drive/a/ak;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JJJ)V

    return-object v3

    .line 185
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x2

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 207
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v10

    .line 208
    invoke-virtual {p0, v10}, Lcom/google/android/gms/drive/a/j;->f(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v9

    .line 212
    const-string v1, "ContentAndMetadataAction"

    const-string v2, "Checking conflicts. entry headRevisionContentHash: %s, action baseContentHash: %s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v9}, Lcom/google/android/gms/drive/database/model/ah;->l()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    aput-object v4, v3, v11

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 215
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/a/a/l;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/google/android/gms/drive/database/model/ah;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 219
    const-string v1, "ContentAndMetadataAction"

    const-string v2, "Conflict detected in applyOnServer. entry headRevisionContentHash: %s, action baseContentHash: %s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v9}, Lcom/google/android/gms/drive/database/model/ah;->l()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    aput-object v4, v3, v11

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 222
    invoke-direct {p0, p3, v9, v0}, Lcom/google/android/gms/drive/a/j;->a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/b/b/n;)V

    .line 227
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 228
    invoke-virtual {p0, v10}, Lcom/google/android/gms/drive/a/j;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    iget-wide v2, v2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 229
    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/a/a/l;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 237
    :try_start_0
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v2

    invoke-interface {v2, p1, p2, v1}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/gms/drive/d/d;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 246
    invoke-interface {v1}, Lcom/google/android/gms/drive/d/d;->L()Ljava/lang/String;

    move-result-object v2

    .line 247
    invoke-interface {v1}, Lcom/google/android/gms/drive/d/d;->o()Ljava/lang/String;

    move-result-object v4

    .line 248
    invoke-virtual {v9}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v1

    .line 249
    const-string v3, "ContentAndMetadataAction"

    const-string v5, "Checking conflicts. entryRevision: %s, v2BetaEntryRevision: %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v1, v7, v6

    aput-object v2, v7, v11

    invoke-static {v3, v5, v7}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 252
    if-eqz v1, :cond_1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 253
    :cond_1
    const-string v3, "ContentAndMetadataAction"

    const-string v5, "Conflict detected in applyOnServer. entryRevision: %s, v2BetaEntryRevision: %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v1, v7, v6

    aput-object v2, v7, v11

    invoke-static {v3, v5, v7}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 256
    invoke-direct {p0, p3, v9, v0}, Lcom/google/android/gms/drive/a/j;->a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/b/b/n;)V

    .line 262
    :cond_2
    :goto_0
    const-string v0, "ContentAndMetadataAction"

    const-string v1, "Creating UploadRequest with httpEtag: %s"

    new-array v2, v11, [Ljava/lang/Object;

    aput-object v4, v2, v6

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->toString()Ljava/lang/String;

    move-result-object v5

    .line 265
    invoke-virtual {p0, v10}, Lcom/google/android/gms/drive/a/j;->f(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v10}, Lcom/google/android/gms/drive/a/j;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v7

    const/16 v1, 0x19c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object v1, p3

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/drive/b/b/n;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/g/aw;JLjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/drive/auth/g;Ljava/lang/Integer;)Lcom/google/android/gms/drive/b/b/n;

    move-result-object v2

    .line 268
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->B()Lcom/google/android/gms/drive/b/b/p;

    move-result-object v0

    invoke-virtual {v0, v2, p3, v11}, Lcom/google/android/gms/drive/b/b/p;->a(Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/g/aw;Z)Lcom/google/android/gms/drive/b/b/o;

    move-result-object v0

    .line 273
    :try_start_1
    new-instance v1, Lcom/google/android/gms/drive/b/b/f;

    invoke-direct {v1}, Lcom/google/android/gms/drive/b/b/f;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/b/b/o;->a(Lcom/google/android/gms/drive/b/b/l;)Lcom/google/android/gms/drive/d/e;

    move-result-object v0

    .line 274
    iget-object v3, v0, Lcom/google/android/gms/drive/d/e;->f:Ljava/lang/String;

    .line 275
    iget-object v4, v0, Lcom/google/android/gms/drive/d/e;->b:Ljava/lang/String;

    .line 276
    iget-object v5, v0, Lcom/google/android/gms/drive/d/e;->c:Ljava/lang/String;

    .line 277
    iget-object v7, v0, Lcom/google/android/gms/drive/d/e;->e:Ljava/lang/String;

    .line 278
    iget-object v8, v0, Lcom/google/android/gms/drive/d/e;->d:Ljava/lang/String;

    .line 281
    invoke-virtual {p0, v10}, Lcom/google/android/gms/drive/a/j;->f(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/database/model/ah;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/drive/b/b/j; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    .line 283
    :try_start_2
    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/database/model/ah;->d(Ljava/lang/String;)V

    .line 284
    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/database/model/ah;->f(Ljava/lang/String;)V

    .line 286
    if-eqz v4, :cond_3

    if-nez v3, :cond_7

    .line 290
    :cond_3
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v9

    const-string v10, "ContentAndMetadataAction"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v11, "Null revision values returned by the server after uploading:"

    invoke-direct {v1, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    const-string v1, " HEAD"

    :goto_1
    invoke-static {v9, v10, v1}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :goto_2
    const-string v1, "ContentAndMetadataAction"

    const-string v9, "Head revision:%s, Uploaded revision:%s."

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    const/4 v4, 0x1

    aput-object v3, v10, v4

    invoke-static {v1, v9, v10}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 299
    const-string v1, "ContentAndMetadataAction"

    const-string v3, "Upload content is head revision: %b"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v4, v9

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 301
    if-eqz v6, :cond_8

    .line 304
    iget-object v1, p0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->e(Ljava/lang/String;)V

    .line 311
    :goto_3
    invoke-virtual {v0, v5}, Lcom/google/android/gms/drive/database/model/ah;->g(Ljava/lang/String;)V

    .line 312
    invoke-virtual {v0, v7}, Lcom/google/android/gms/drive/database/model/ah;->m(Ljava/lang/String;)V

    .line 313
    invoke-virtual {v0, v8}, Lcom/google/android/gms/drive/database/model/ah;->h(Ljava/lang/String;)V

    .line 314
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/drive/b/b/j; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_2 .. :try_end_2} :catch_3

    .line 323
    :goto_4
    return-void

    .line 241
    :catch_0
    move-exception v0

    .line 242
    const-string v1, "ContentAndMetadataAction"

    const-string v2, "Request to get file metadata failed. Cannot proceed with conflict-aware upload of %s"

    new-array v3, v11, [Ljava/lang/Object;

    aput-object p2, v3, v6

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 244
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_4
    move-object v4, v0

    .line 260
    goto/16 :goto_0

    .line 290
    :cond_5
    if-nez v3, :cond_6

    :try_start_3
    const-string v1, " LOCAL"

    goto :goto_1

    :cond_6
    const-string v1, ""

    goto :goto_1

    .line 295
    :cond_7
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    goto :goto_2

    .line 307
    :cond_8
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->e(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/drive/b/b/j; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_3

    .line 315
    :catch_1
    move-exception v0

    .line 316
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 318
    :catch_2
    move-exception v0

    move-object v0, v9

    :goto_5
    const-string v1, "ContentAndMetadataAction"

    const-string v3, "Conflict detected in applyOnServer during upload"

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-direct {p0, p3, v0, v2}, Lcom/google/android/gms/drive/a/j;->a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/b/b/n;)V

    goto :goto_4

    .line 321
    :catch_3
    move-exception v0

    .line 322
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Upload failed"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 318
    :catch_4
    move-exception v1

    goto :goto_5
.end method

.method public final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 459
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/drive/a/j;->b(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/drive/g/aw;)V
    .locals 4

    .prologue
    .line 378
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/drive/database/r;->b(J)Lcom/google/android/gms/drive/database/model/bi;

    move-result-object v0

    .line 380
    if-nez v0, :cond_0

    .line 381
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v0

    const-string v1, "ContentAndMetadataAction"

    const-string v2, "PendingUpload should not be null"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/b;->b(Lcom/google/android/gms/drive/g/aw;)V

    .line 388
    return-void

    .line 384
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bi;->j()V

    .line 385
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->v()Lcom/google/android/gms/drive/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/d;->b()V

    goto :goto_0
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    return-object v0
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected final e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 406
    if-ne p0, p1, :cond_1

    .line 419
    :cond_0
    :goto_0
    return v0

    .line 407
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 409
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/a/j;

    .line 411
    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 413
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 414
    goto :goto_0

    .line 411
    :cond_5
    iget-object v2, p1, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    if-nez v2, :cond_4

    .line 415
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 417
    goto :goto_0

    .line 415
    :cond_7
    iget-object v2, p1, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final f()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 392
    invoke-super {p0}, Lcom/google/android/gms/drive/a/b;->f()Lorg/json/JSONObject;

    move-result-object v0

    .line 393
    const-string v1, "pendingUploadSqlId"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 394
    const-string v1, "contentHash"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 395
    const-string v1, "writeOpenTime"

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/j;->h:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 396
    const-string v1, "metadataDelta"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/a/ax;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 397
    iget-object v1, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 398
    const-string v1, "baseContentHash"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 399
    const-string v1, "baseContentSize"

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/j;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 401
    :cond_0
    return-object v0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 426
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 427
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 428
    return v0

    :cond_1
    move v0, v1

    .line 424
    goto :goto_0

    :cond_2
    move v0, v1

    .line 426
    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 433
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ContentAndMetadataAction[%s, pendingUploadSqlId=%d,  contentHash=%s, metadataChangeSet=%s]"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/j;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/a/j;->j:Ljava/lang/Long;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/drive/a/j;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/gms/drive/a/j;->i:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
