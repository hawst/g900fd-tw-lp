.class public final Lcom/google/android/gms/games/ui/common/requests/k;
.super Lcom/google/android/gms/games/ui/s;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/bs;
.implements Lcom/google/android/gms/games/ui/cj;


# instance fields
.field private l:Lcom/google/android/gms/games/ui/common/requests/n;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;-><init>()V

    return-void
.end method

.method private C()V
    .locals 5

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/k;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    const-string v0, "RequestInboxFrag"

    const-string v1, "reloadData: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->z()V

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 143
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/k;->l:Lcom/google/android/gms/games/ui/common/requests/n;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/requests/n;->a(Lcom/google/android/gms/common/api/v;)V

    goto :goto_0

    .line 145
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/k;->l:Lcom/google/android/gms/games/ui/common/requests/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v4, v1}, Lcom/google/android/gms/games/ui/common/requests/n;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final B()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/k;->C()V

    .line 129
    return-void
.end method

.method public final M_()V
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/k;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    :goto_0
    return-void

    .line 119
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/k;->C()V

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/k;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->A()V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->h:Lcom/google/android/gms/games/ui/d/p;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->I()V

    .line 82
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/k;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->A()V

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->h:Lcom/google/android/gms/games/ui/d/p;

    invoke-virtual {v0, p1, v1, v1}, Lcom/google/android/gms/games/ui/d/p;->a(IIZ)V

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->I()V

    .line 92
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 4

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->l:Lcom/google/android/gms/games/ui/common/requests/n;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/n;->a(Lcom/google/android/gms/common/api/v;)V

    .line 73
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/k;->l:Lcom/google/android/gms/games/ui/common/requests/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v2, v3, v0}, Lcom/google/android/gms/games/ui/common/requests/n;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;ZLjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->l:Lcom/google/android/gms/games/ui/common/requests/n;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/requests/n;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;ZLjava/util/ArrayList;)V

    .line 109
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onActivityCreated(Landroid/os/Bundle;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/requests/m;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/bo;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/requests/m;->O_()Lcom/google/android/gms/games/ui/common/requests/l;

    move-result-object v1

    .line 41
    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 43
    sget v0, Lcom/google/android/gms/h;->av:I

    sget v2, Lcom/google/android/gms/p;->kR:I

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/ui/common/requests/k;->a(III)V

    .line 46
    new-instance v2, Lcom/google/android/gms/games/ui/common/requests/n;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/bo;

    invoke-direct {v2, v3, v1, v0, p0}, Lcom/google/android/gms/games/ui/common/requests/n;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/requests/l;Lcom/google/android/gms/games/ui/bo;Lcom/google/android/gms/games/ui/bs;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/k;->l:Lcom/google/android/gms/games/ui/common/requests/n;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/k;->l:Lcom/google/android/gms/games/ui/common/requests/n;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/k;->a(Landroid/support/v7/widget/bv;)V

    .line 50
    return-void
.end method

.method public final onDetach()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Lcom/google/android/gms/games/ui/s;->onDetach()V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    const-string v0, "RequestInboxFrag"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_0
    return-void
.end method
