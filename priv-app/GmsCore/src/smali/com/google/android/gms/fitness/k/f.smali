.class public final Lcom/google/android/gms/fitness/k/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/ThreadLocal;


# instance fields
.field private final b:Lcom/google/android/gms/fitness/service/d;

.field private final c:Ljava/util/Map;

.field private final d:Lcom/google/android/gms/fitness/data/Device;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/fitness/k/g;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/k/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/k/f;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/fitness/service/d;Ljava/util/Map;Lcom/google/android/gms/fitness/data/Device;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/google/android/gms/fitness/k/f;->b:Lcom/google/android/gms/fitness/service/d;

    .line 67
    iput-object p3, p0, Lcom/google/android/gms/fitness/k/f;->d:Lcom/google/android/gms/fitness/data/Device;

    .line 68
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/k/f;->c:Ljava/util/Map;

    .line 69
    return-void
.end method

.method private a(Lcom/google/android/gms/fitness/b/ad;JJILcom/google/android/gms/fitness/l/b;Ljava/util/Map;)Lcom/google/android/gms/fitness/k/h;
    .locals 28

    .prologue
    .line 115
    new-instance v22, Ljava/util/HashMap;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashMap;-><init>()V

    .line 116
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 118
    sget-object v4, Lcom/google/android/gms/fitness/k/f;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 120
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/fitness/b/ad;->b()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_0
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/google/android/gms/fitness/b/u;

    move-object/from16 v19, v0

    .line 121
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p8

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/b/u;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_1
    :goto_0
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/fitness/data/DataSource;

    .line 125
    invoke-virtual {v5}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v26

    .line 126
    move-object/from16 v0, v19

    iget-boolean v4, v0, Lcom/google/android/gms/fitness/b/u;->c:Z

    if-eqz v4, :cond_4

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    move v8, v4

    .line 128
    :goto_1
    if-eqz v8, :cond_5

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/k/h;

    iget-wide v6, v4, Lcom/google/android/gms/fitness/k/h;->b:J

    move-wide/from16 v20, v6

    .line 133
    :goto_2
    move-object/from16 v0, p8

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/google/android/gms/fitness/c/d;

    move-object v12, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v13

    if-nez v13, :cond_c

    if-nez v12, :cond_6

    const-wide/16 v6, 0x0

    :goto_3
    move-wide v14, v6

    :goto_4
    cmp-long v4, v20, v14

    if-gtz v4, :cond_2

    if-eqz v8, :cond_7

    cmp-long v4, v20, v14

    if-nez v4, :cond_7

    move-object/from16 v0, v19

    iget v4, v0, Lcom/google/android/gms/fitness/b/u;->f:I

    if-nez v4, :cond_7

    move-object/from16 v0, v19

    iget v4, v0, Lcom/google/android/gms/fitness/b/u;->e:I

    if-nez v4, :cond_7

    :cond_2
    new-instance v4, Lcom/google/android/gms/fitness/k/h;

    invoke-direct {v4}, Lcom/google/android/gms/fitness/k/h;-><init>()V

    move-object/from16 v18, v4

    .line 138
    :goto_5
    move-object/from16 v0, v19

    iget-boolean v4, v0, Lcom/google/android/gms/fitness/b/u;->c:Z

    if-eqz v4, :cond_3

    .line 139
    move-object/from16 v0, v18

    iget-wide v8, v0, Lcom/google/android/gms/fitness/k/h;->a:J

    move-wide/from16 v6, v20

    move-object/from16 v10, p7

    move-object/from16 v11, v23

    move-wide/from16 v12, p2

    move-wide/from16 v14, p4

    move/from16 v16, p6

    move-object/from16 v17, p8

    invoke-static/range {v6 .. v17}, Lcom/google/android/gms/fitness/k/f;->a(JJLcom/google/android/gms/fitness/l/b;Ljava/util/List;JJILjava/util/Map;)V

    .line 143
    :cond_3
    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 147
    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/k/h;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/google/android/gms/fitness/k/h;->a(Lcom/google/android/gms/fitness/k/h;)Lcom/google/android/gms/fitness/k/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 154
    :catchall_0
    move-exception v4

    move-object v5, v4

    sget-object v4, Lcom/google/android/gms/fitness/k/f;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    throw v5

    .line 126
    :cond_4
    const/4 v4, 0x0

    move v8, v4

    goto/16 :goto_1

    :cond_5
    move-wide/from16 v20, p2

    .line 128
    goto/16 :goto_2

    .line 133
    :cond_6
    :try_start_1
    invoke-virtual {v12}, Lcom/google/android/gms/fitness/c/d;->b()J

    move-result-wide v6

    move-wide/from16 v0, p4

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    goto :goto_3

    :cond_7
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v19

    iget v6, v0, Lcom/google/android/gms/fitness/b/u;->f:I

    int-to-long v6, v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v19

    iget v8, v0, Lcom/google/android/gms/fitness/b/u;->e:I

    int-to-long v8, v8

    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    sub-long v6, v20, v6

    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    add-long/2addr v8, v14

    move-object/from16 v4, p7

    move/from16 v10, p6

    move-object/from16 v11, p8

    invoke-virtual/range {v4 .. v11}, Lcom/google/android/gms/fitness/l/b;->a(Lcom/google/android/gms/fitness/data/DataSource;JJILjava/util/Map;)V

    invoke-static {v5, v6, v7, v8, v9}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/data/DataSource;JJ)V

    if-eqz v13, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/fitness/k/f;->c:Ljava/util/Map;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/gms/fitness/b/ad;

    move-object/from16 v10, p0

    move-wide v12, v6

    move-wide v14, v8

    move/from16 v16, p6

    move-object/from16 v17, p7

    move-object/from16 v18, p8

    invoke-direct/range {v10 .. v18}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/b/ad;JJILcom/google/android/gms/fitness/l/b;Ljava/util/Map;)Lcom/google/android/gms/fitness/k/h;

    move-result-object v4

    move-object/from16 v18, v4

    goto/16 :goto_5

    :cond_8
    new-instance v4, Lcom/google/android/gms/fitness/k/h;

    invoke-virtual {v12}, Lcom/google/android/gms/fitness/c/d;->a()J

    move-result-wide v6

    invoke-direct {v4, v6, v7, v14, v15}, Lcom/google/android/gms/fitness/k/h;-><init>(JJ)V

    move-object/from16 v18, v4

    goto/16 :goto_5

    .line 148
    :cond_9
    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/google/android/gms/fitness/k/h;->b:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 149
    move-object/from16 v0, v22

    move-object/from16 v1, v26

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 154
    :cond_a
    sget-object v4, Lcom/google/android/gms/fitness/k/f;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 161
    new-instance v5, Lcom/google/android/gms/fitness/k/h;

    invoke-direct {v5}, Lcom/google/android/gms/fitness/k/h;-><init>()V

    .line 162
    invoke-interface/range {v22 .. v22}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/k/h;

    .line 163
    invoke-virtual {v5, v4}, Lcom/google/android/gms/fitness/k/h;->a(Lcom/google/android/gms/fitness/k/h;)Lcom/google/android/gms/fitness/k/h;

    goto :goto_6

    .line 165
    :cond_b
    return-object v5

    :cond_c
    move-wide/from16 v14, p4

    goto/16 :goto_4
.end method

.method private a(Lcom/google/android/gms/fitness/b/ad;JJILcom/google/android/gms/fitness/l/b;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 14

    .prologue
    .line 457
    move-object/from16 v0, p7

    move-object/from16 v1, p8

    move-object/from16 v2, p9

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/l/b;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v5

    .line 458
    const/4 v4, 0x0

    .line 459
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v11, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/b/a/i;

    .line 460
    iget-object v4, v4, Lcom/google/android/gms/fitness/b/a/i;->a:Lcom/google/android/gms/fitness/data/DataSet;

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSet;->f()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 461
    const/4 v4, 0x1

    :goto_1
    move v11, v4

    .line 463
    goto :goto_0

    .line 466
    :cond_0
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 467
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 468
    new-instance v10, Lcom/google/android/gms/fitness/b/a/k;

    invoke-direct {v10}, Lcom/google/android/gms/fitness/b/a/k;-><init>()V

    move-object v4, p1

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    invoke-interface/range {v4 .. v10}, Lcom/google/android/gms/fitness/b/ad;->a(Ljava/util/List;JJLcom/google/android/gms/fitness/b/t;)Ljava/util/List;

    move-result-object v4

    .line 470
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/b/r;

    move-object v5, v4

    .line 471
    check-cast v5, Lcom/google/android/gms/fitness/b/a/i;

    iget-object v5, v5, Lcom/google/android/gms/fitness/b/a/i;->a:Lcom/google/android/gms/fitness/data/DataSet;

    .line 472
    invoke-virtual {v5, v11}, Lcom/google/android/gms/fitness/data/DataSet;->a(Z)V

    .line 474
    if-lez p6, :cond_1

    invoke-interface {v4}, Lcom/google/android/gms/fitness/b/r;->b()I

    move-result v4

    move/from16 v0, p6

    if-le v4, v0, :cond_1

    .line 475
    invoke-virtual {v5}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v4

    .line 476
    invoke-virtual {v5}, Lcom/google/android/gms/fitness/data/DataSet;->b()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v5

    .line 477
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    sub-int v7, v7, p6

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    invoke-interface {v4, v7, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/android/gms/fitness/data/DataSet;->a(Ljava/lang/Iterable;)V

    .line 479
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 481
    :cond_1
    invoke-interface {v12, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 485
    :cond_2
    return-object v12

    :cond_3
    move v4, v11

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/fitness/b/ad;Lcom/google/android/gms/fitness/l/b;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 15

    .prologue
    .line 498
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 499
    sget-object v2, Lcom/google/android/gms/fitness/k/f;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 501
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/fitness/b/ad;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/b/u;

    .line 502
    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/b/u;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataSource;

    .line 503
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/gms/fitness/l/b;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 506
    invoke-direct {p0, v2}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 510
    iget-object v3, p0, Lcom/google/android/gms/fitness/k/f;->c:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/b/ad;

    .line 512
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/l/b;->a(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/l/b;->b(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/l/b;->c(Lcom/google/android/gms/fitness/data/DataSource;)I

    move-result v8

    move-object v2, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    invoke-direct/range {v2 .. v11}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/b/ad;JJILcom/google/android/gms/fitness/l/b;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataSet;

    .line 520
    new-instance v4, Lcom/google/android/gms/fitness/b/a/i;

    invoke-direct {v4, v2}, Lcom/google/android/gms/fitness/b/a/i;-><init>(Lcom/google/android/gms/fitness/data/DataSet;)V

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 532
    :catchall_0
    move-exception v2

    move-object v3, v2

    sget-object v2, Lcom/google/android/gms/fitness/k/f;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    throw v3

    .line 523
    :cond_2
    :try_start_1
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataSet;

    .line 524
    new-instance v3, Lcom/google/android/gms/fitness/b/a/i;

    invoke-direct {v3, v2}, Lcom/google/android/gms/fitness/b/a/i;-><init>(Lcom/google/android/gms/fitness/data/DataSet;)V

    .line 525
    invoke-virtual {v3}, Lcom/google/android/gms/fitness/b/a/i;->b()I

    move-result v2

    if-lez v2, :cond_1

    .line 526
    invoke-interface {v12, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 532
    :cond_3
    sget-object v2, Lcom/google/android/gms/fitness/k/f;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 534
    return-object v12
.end method

.method private a(Lcom/google/android/gms/fitness/b/u;Ljava/util/Map;)Ljava/util/Set;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 340
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 341
    iget-object v0, p1, Lcom/google/android/gms/fitness/b/u;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/fitness/data/DataType;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v5

    .line 344
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 345
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/c/d;

    .line 346
    invoke-static {v0}, Lcom/google/android/gms/fitness/a/e;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-boolean v1, v1, Lcom/google/android/gms/fitness/c/d;->b:Z

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/fitness/a/e;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "session_activity_segment"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move v1, v3

    :goto_1
    invoke-static {p1, v0, v1}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/b/u;Lcom/google/android/gms/fitness/data/DataSource;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 349
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 346
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v1

    const-string v7, "com.google.android.gms"

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    if-nez v1, :cond_3

    move v1, v2

    goto :goto_1

    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/data/Device;)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "Found GMS data source from different device: %s, local: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v1, v8, v3

    iget-object v1, p0, Lcom/google/android/gms/fitness/k/f;->d:Lcom/google/android/gms/fitness/data/Device;

    aput-object v1, v8, v2

    invoke-static {v7, v8}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    if-eqz v1, :cond_6

    invoke-direct {p0, v1}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/data/Device;)Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    goto :goto_1

    :cond_6
    move v1, v3

    goto :goto_1

    .line 354
    :cond_7
    sget-object v0, Lcom/google/android/gms/fitness/k/f;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 355
    iget-object v1, p0, Lcom/google/android/gms/fitness/k/f;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_8
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 356
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataSource;

    .line 357
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/b/ad;

    .line 358
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {p1, v2, v3}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/b/u;Lcom/google/android/gms/fitness/data/DataSource;Z)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 362
    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 366
    :cond_9
    return-object v4
.end method

.method private static a(JJLcom/google/android/gms/fitness/l/b;Ljava/util/List;JJILjava/util/Map;)V
    .locals 12

    .prologue
    .line 256
    cmp-long v2, p2, p0

    if-lez v2, :cond_0

    .line 257
    move-wide/from16 v0, p8

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 259
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/data/DataSource;

    move-object/from16 v2, p4

    move-wide/from16 v4, p6

    move/from16 v8, p10

    move-object/from16 v9, p11

    .line 260
    invoke-virtual/range {v2 .. v9}, Lcom/google/android/gms/fitness/l/b;->a(Lcom/google/android/gms/fitness/data/DataSource;JJILjava/util/Map;)V

    .line 266
    move-wide/from16 v0, p6

    invoke-static {v3, v0, v1, v6, v7}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/data/DataSource;JJ)V

    goto :goto_0

    .line 269
    :cond_0
    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/data/DataSource;JJ)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 313
    const-string v0, "Fitness"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    :goto_0
    return-void

    .line 318
    :cond_0
    sget-object v0, Lcom/google/android/gms/fitness/k/f;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    .line 319
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 320
    :goto_1
    if-ge v0, v2, :cond_1

    .line 321
    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 325
    :cond_1
    const-string v0, "%1$s%2$s %3$tF %3$tT - %4$tF %4$tT"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataSource;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v2, v6

    const/4 v1, 0x3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p3, p4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/u;Lcom/google/android/gms/fitness/data/DataSource;Z)Z
    .locals 2

    .prologue
    .line 432
    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/fitness/b/u;->d:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/u;->b:Lcom/google/android/gms/fitness/b/o;

    new-instance v1, Lcom/google/android/gms/fitness/b/a/e;

    invoke-direct {v1, p1}, Lcom/google/android/gms/fitness/b/a/e;-><init>(Lcom/google/android/gms/fitness/data/DataSource;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/o;->a(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/fitness/data/DataSource;)Z
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/gms/fitness/k/f;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/google/android/gms/fitness/data/Device;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 414
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Device;->e()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 420
    :cond_0
    :goto_0
    return v0

    .line 417
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/k/f;->d:Lcom/google/android/gms/fitness/data/Device;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Device;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Device;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 414
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/b/ad;JJILjava/util/Map;Z)Ljava/util/List;
    .locals 12

    .prologue
    .line 81
    new-instance v9, Lcom/google/android/gms/fitness/l/b;

    invoke-direct {v9}, Lcom/google/android/gms/fitness/l/b;-><init>()V

    .line 82
    const-string v2, "Fitness"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/fitness/k/f;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/b/ad;

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataSource;

    move-wide/from16 v0, p4

    invoke-static {v2, p2, p3, v0, v1}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/data/DataSource;JJ)V

    :cond_1
    :goto_0
    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-object/from16 v10, p7

    .line 83
    invoke-direct/range {v2 .. v10}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/b/ad;JJILcom/google/android/gms/fitness/l/b;Ljava/util/Map;)Lcom/google/android/gms/fitness/k/h;

    .line 85
    invoke-virtual {v9}, Lcom/google/android/gms/fitness/l/b;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez p8, :cond_3

    .line 86
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 95
    :goto_1
    return-object v2

    .line 82
    :cond_2
    const-string v2, "Transformation %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 90
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/fitness/k/f;->b:Lcom/google/android/gms/fitness/service/d;

    move/from16 v0, p8

    invoke-virtual {v2, v9, v0}, Lcom/google/android/gms/fitness/service/d;->a(Lcom/google/android/gms/fitness/l/b;Z)Ljava/util/Map;

    move-result-object v10

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-object/from16 v11, p7

    .line 95
    invoke-direct/range {v2 .. v11}, Lcom/google/android/gms/fitness/k/f;->a(Lcom/google/android/gms/fitness/b/ad;JJILcom/google/android/gms/fitness/l/b;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v2

    goto :goto_1
.end method
