.class final Lcom/google/android/gms/smart_profile/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/o;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/smart_profile/o;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/r;->a:Lcom/google/android/gms/smart_profile/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/smart_profile/o;B)V
    .locals 0

    .prologue
    .line 316
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/r;-><init>(Lcom/google/android/gms/smart_profile/o;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 4

    .prologue
    .line 316
    check-cast p1, Lcom/google/android/gms/people/p;

    invoke-interface {p1}, Lcom/google/android/gms/people/p;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/people/p;->c()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/people/af;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/r;->a:Lcom/google/android/gms/smart_profile/o;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/o;->e(Lcom/google/android/gms/smart_profile/o;)Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    move-result-object v0

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/r;->a:Lcom/google/android/gms/smart_profile/o;

    invoke-static {v3}, Lcom/google/android/gms/smart_profile/o;->d(Lcom/google/android/gms/smart_profile/o;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/r;->a:Lcom/google/android/gms/smart_profile/o;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/o;->g(Lcom/google/android/gms/smart_profile/o;)Lcom/google/android/gms/common/api/am;

    return-void

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v1
.end method
