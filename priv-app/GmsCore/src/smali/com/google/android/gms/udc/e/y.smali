.class public final Lcom/google/android/gms/udc/e/y;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[B

.field public b:[I

.field public c:I

.field public d:Lcom/google/android/gms/udc/e/t;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 39
    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/udc/e/y;->a:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/udc/e/y;->c:I

    iput-object v1, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/y;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/y;->cachedSize:I

    .line 40
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 119
    iget-object v2, p0, Lcom/google/android/gms/udc/e/y;->a:[B

    sget-object v3, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    .line 120
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/udc/e/y;->a:[B

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 123
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    .line 125
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 126
    iget-object v3, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    aget v3, v3, v1

    .line 127
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130
    :cond_1
    add-int/2addr v0, v2

    .line 131
    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 133
    :cond_2
    iget v1, p0, Lcom/google/android/gms/udc/e/y;->c:I

    if-eqz v1, :cond_3

    .line 134
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/udc/e/y;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    if-eqz v1, :cond_4

    .line 138
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 55
    const/4 v0, 0x1

    .line 80
    :cond_0
    :goto_0
    return v0

    .line 57
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/y;

    if-eqz v1, :cond_0

    .line 60
    check-cast p1, Lcom/google/android/gms/udc/e/y;

    .line 61
    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->a:[B

    iget-object v2, p1, Lcom/google/android/gms/udc/e/y;->a:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    iget-object v2, p1, Lcom/google/android/gms/udc/e/y;->b:[I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    iget v1, p0, Lcom/google/android/gms/udc/e/y;->c:I

    iget v2, p1, Lcom/google/android/gms/udc/e/y;->c:I

    if-ne v1, v2, :cond_0

    .line 71
    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    if-nez v1, :cond_3

    .line 72
    iget-object v1, p1, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    if-nez v1, :cond_0

    .line 80
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/y;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 76
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 87
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/udc/e/y;->c:I

    add-int/2addr v0, v1

    .line 90
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 92
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/y;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    return v0

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/t;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 9
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/y;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/y;->a:[B

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/udc/e/y;->c:I

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/gms/udc/e/t;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x18 -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->a:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->a:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 103
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 104
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/y;->b:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    :cond_1
    iget v0, p0, Lcom/google/android/gms/udc/e/y;->c:I

    if-eqz v0, :cond_2

    .line 108
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/udc/e/y;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    if-eqz v0, :cond_3

    .line 111
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 113
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 114
    return-void
.end method
