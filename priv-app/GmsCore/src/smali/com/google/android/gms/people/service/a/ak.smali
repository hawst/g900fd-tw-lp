.class public final Lcom/google/android/gms/people/service/a/ak;
.super Lcom/google/android/gms/people/service/a/e;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Landroid/net/Uri;

.field private final j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 9

    .prologue
    .line 35
    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/ak;->g:Ljava/lang/String;

    .line 38
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/ak;->h:Ljava/lang/String;

    .line 39
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/ak;->i:Landroid/net/Uri;

    .line 40
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/gms/people/service/a/ak;->j:Z

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 6

    .prologue
    .line 47
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ak;->i:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/ak;->h:Ljava/lang/String;

    invoke-virtual {p2, p1, p3, v0, v2}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_3

    .line 52
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    .line 53
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/ak;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/ak;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/android/gms/people/c/f;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 54
    const-string v2, "avatarurl"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/ak;->i:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/gms/common/util/ac;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/ak;->i:Landroid/net/Uri;

    invoke-static {p1, v2}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;Landroid/net/Uri;)[B

    move-result-object v2

    .line 59
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/ak;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/f/d;->a(Landroid/content/Context;)Lcom/google/android/gms/people/f/d;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/ak;->g:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/ak;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v5, v0, v2}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)Z

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ak;->i:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ac;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/people/service/a/ak;->j:Z

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ak;->i:Landroid/net/Uri;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/ac;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ak;->h:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ak;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/ak;->g:Ljava/lang/String;

    const-string v3, "SetAvatar"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/people/sync/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_2
    new-instance v0, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 79
    :goto_0
    return-object v0

    :cond_3
    new-instance v0, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/gms/people/service/b;->f:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
