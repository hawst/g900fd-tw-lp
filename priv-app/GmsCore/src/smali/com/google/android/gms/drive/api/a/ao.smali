.class public final Lcom/google/android/gms/drive/api/a/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/drive/api/a/ao;->a:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 16
    check-cast p1, Lcom/google/android/gms/drive/api/DriveAsyncService;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ao;->a:Ljava/lang/String;

    const-string v0, "Drive.UninstallOperation"

    const-string v2, "Uninstall %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-static {p1, v1}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/drive/u;->b(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/ao;->a:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/gms/drive/database/r;->g(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->v()Lcom/google/android/gms/drive/b/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/d;->b()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Drive.UninstallOperation"

    const-string v3, "Interrupted while uninstalling %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v0, v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_0
    const-string v0, "Drive.UninstallOperation"

    const-string v2, "Package still installed %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
