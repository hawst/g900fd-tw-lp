.class public final Lcom/google/android/gms/drive/b/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/b/a/d;

.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/Integer;

.field public final e:Lcom/google/android/gms/drive/b/a/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/b/a/d;IILjava/lang/Integer;Lcom/google/android/gms/drive/b/a/c;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/google/android/gms/drive/b/a/d;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/b/a/d;-><init>(Lcom/google/android/gms/drive/b/a/d;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    .line 34
    iput p2, p0, Lcom/google/android/gms/drive/b/a/i;->b:I

    .line 35
    iput p3, p0, Lcom/google/android/gms/drive/b/a/i;->c:I

    .line 36
    iput-object p4, p0, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    .line 37
    iput-object p5, p0, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    .line 38
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/google/android/gms/drive/b/a/d;

    const-string v2, "chunkMap"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/drive/b/a/d;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    .line 46
    const-string v0, "chunkSize"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/i;->b:I

    .line 47
    const-string v0, "remainderLength"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/i;->c:I

    .line 48
    const-string v0, "remainderWeakHash"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "remainderWeakHash"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    .line 50
    const-string v0, "remainderInfo"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/drive/b/a/c;

    const-string v0, "remainderInfo"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/b/a/c;-><init>(Lorg/json/JSONObject;)V

    :cond_0
    iput-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    .line 52
    return-void

    :cond_1
    move-object v0, v1

    .line 48
    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 123
    instance-of v1, p1, Lcom/google/android/gms/drive/b/a/i;

    if-nez v1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 126
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/b/a/i;

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    iget-object v2, p1, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/b/a/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/drive/b/a/i;->b:I

    iget v2, p1, Lcom/google/android/gms/drive/b/a/i;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/drive/b/a/i;->c:I

    iget v2, p1, Lcom/google/android/gms/drive/b/a/i;->c:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    if-nez v1, :cond_0

    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    iget-object v2, p1, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/b/a/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/a/d;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 142
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/drive/b/a/i;->b:I

    add-int/2addr v0, v2

    .line 143
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/drive/b/a/i;->c:I

    add-int/2addr v0, v2

    .line 144
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 145
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/a/c;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 146
    return v0

    :cond_1
    move v0, v1

    .line 144
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "chunkInfoMap: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->a:Lcom/google/android/gms/drive/b/a/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", chunkSize: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/drive/b/a/i;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", remainderLength: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/drive/b/a/i;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", remainderWeakHash: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->d:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", remainderInfo: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/i;->e:Lcom/google/android/gms/drive/b/a/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
