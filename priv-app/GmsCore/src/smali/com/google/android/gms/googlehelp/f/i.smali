.class public final Lcom/google/android/gms/googlehelp/f/i;
.super Lcom/google/android/gms/googlehelp/f/a;
.source "SourceFile"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private final d:Lcom/google/android/gms/googlehelp/d/b;

.field private final e:Lcom/google/android/gms/googlehelp/common/t;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/t;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/f/a;-><init>(Lcom/google/android/gms/googlehelp/f/n;)V

    .line 52
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/i;->b:Landroid/content/Context;

    .line 53
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/i;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 54
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d()Lcom/google/android/gms/googlehelp/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/i;->d:Lcom/google/android/gms/googlehelp/d/b;

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/f/i;->e:Lcom/google/android/gms/googlehelp/common/t;

    .line 56
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/i;->e:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/t;->c()Z

    move-result v0

    return v0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/i;->e:Lcom/google/android/gms/googlehelp/common/t;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/f/i;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "GOOGLEHELP_PrefetchLeafAnswersTask"

    const-string v1, "Prefetching is cancelled."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    return-object v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/i;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/common/util/ak;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->r()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "GOOGLEHELP_PrefetchLeafAnswersTask"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Skip prefetching leaf answer for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/i;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/f/i;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/googlehelp/a/m;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->j()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/f/i;->d:Lcom/google/android/gms/googlehelp/d/b;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/googlehelp/d/b;->a(Lcom/google/android/gms/googlehelp/common/k;)V

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/common/k;->d(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/i;->d:Lcom/google/android/gms/googlehelp/d/b;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/d/b;->c(Lcom/google/android/gms/googlehelp/common/k;)V

    :cond_4
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/common/k;->a(Z)V

    goto :goto_0
.end method
