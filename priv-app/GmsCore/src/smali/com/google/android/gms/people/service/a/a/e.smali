.class public abstract Lcom/google/android/gms/people/service/a/a/e;
.super Lcom/google/android/gms/people/service/a;
.source "SourceFile"


# static fields
.field public static final i:[B

.field public static final j:[B


# instance fields
.field protected final k:Lcom/google/android/gms/people/service/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    new-array v0, v1, [B

    sput-object v0, Lcom/google/android/gms/people/service/a/a/e;->i:[B

    .line 39
    new-array v0, v1, [B

    sput-object v0, Lcom/google/android/gms/people/service/a/a/e;->j:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/people/service/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    .line 86
    iput-object p4, p0, Lcom/google/android/gms/people/service/a/a/e;->k:Lcom/google/android/gms/people/service/d;

    .line 87
    return-void
.end method

.method private a(Lcom/google/android/gms/people/service/a/a/f;)V
    .locals 7

    .prologue
    .line 112
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/people/service/a/a/f;->e:Lcom/google/android/gms/people/f/m;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/people/service/a/a/f;->e:Lcom/google/android/gms/people/f/m;

    invoke-virtual {v0}, Lcom/google/android/gms/people/f/m;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 114
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 116
    const-string v2, "width"

    iget v3, p1, Lcom/google/android/gms/people/service/a/a/f;->f:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    const-string v2, "height"

    iget v3, p1, Lcom/google/android/gms/people/service/a/a/f;->g:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    const-string v2, "rewindable"

    iget-boolean v3, p1, Lcom/google/android/gms/people/service/a/a/f;->h:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    const-string v2, "PeopleService"

    const-string v3, "%s status=%d [%d x %d] rewindable=%s fd=%s"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/a/e;->f()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p1, Lcom/google/android/gms/people/service/a/a/f;->d:Lcom/google/android/gms/people/service/b;

    iget v6, v6, Lcom/google/android/gms/people/service/b;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v6, p1, Lcom/google/android/gms/people/service/a/a/f;->f:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget v6, p1, Lcom/google/android/gms/people/service/a/a/f;->g:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    iget-boolean v6, p1, Lcom/google/android/gms/people/service/a/a/f;->h:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-static {v0}, Lcom/google/android/gms/people/f/l;->a(Landroid/os/ParcelFileDescriptor;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/a/e;->k:Lcom/google/android/gms/people/service/d;

    iget-object v3, p1, Lcom/google/android/gms/people/service/a/a/f;->d:Lcom/google/android/gms/people/service/b;

    iget v3, v3, Lcom/google/android/gms/people/service/b;->a:I

    iget-object v4, p1, Lcom/google/android/gms/people/service/a/a/f;->d:Lcom/google/android/gms/people/service/b;

    iget-object v4, v4, Lcom/google/android/gms/people/service/b;->b:Landroid/os/Bundle;

    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/google/android/gms/people/service/d;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_1
    return-void

    .line 112
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    .line 136
    const-string v1, "PeopleService"

    const-string v2, "Unknown error"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 137
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final e()V
    .locals 3

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/a;->e:Z

    if-eqz v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 96
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/a/e;->h()Lcom/google/android/gms/people/service/a/a/f;

    move-result-object v0

    .line 99
    iget-object v1, v0, Lcom/google/android/gms/people/service/a/a/f;->e:Lcom/google/android/gms/people/f/m;

    if-eqz v1, :cond_1

    .line 100
    iget-object v1, v0, Lcom/google/android/gms/people/service/a/a/f;->e:Lcom/google/android/gms/people/f/m;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/people/service/a;->f:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/people/service/a/a/e;->a(Lcom/google/android/gms/people/service/a/a/f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    const-string v1, "PeopleService"

    const-string v2, "Error during operation"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 106
    sget-object v0, Lcom/google/android/gms/people/service/a/a/f;->c:Lcom/google/android/gms/people/service/a/a/f;

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/service/a/a/e;->a(Lcom/google/android/gms/people/service/a/a/f;)V

    goto :goto_0
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method protected abstract h()Lcom/google/android/gms/people/service/a/a/f;
.end method
