.class public Lcom/google/android/gms/notifications/GunsNotificationActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 23
    const-string v0, "com.google.android.gms.notifications.intents.START_ACTIVITY"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    invoke-static {v1}, Lcom/google/android/gms/notifications/k;->a(Landroid/content/Intent;)Lcom/google/c/e/a/a/a/a/c;

    move-result-object v2

    .line 25
    if-eqz v2, :cond_1

    iget-object v0, v2, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 26
    const-string v0, "GnotsActivity"

    const-string v1, "Failed to start activity due to invalid payload."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationActivity;->finish()V

    .line 53
    :cond_0
    :goto_1
    return-void

    .line 25
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 31
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 32
    iget-object v3, v2, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v3, v3, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    iget-boolean v3, v3, Lcom/google/c/e/a/a/a/a/h;->b:Z

    if-eqz v3, :cond_3

    .line 34
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gms/notifications/GunsBrowserActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    const-string v0, "com.google.android.gms.notifications.intents.LOAD_URL"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 37
    invoke-virtual {p0, v2}, Lcom/google/android/gms/notifications/GunsNotificationActivity;->startActivity(Landroid/content/Intent;)V

    .line 51
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationActivity;->finish()V

    goto :goto_1

    .line 41
    :cond_3
    iget-object v1, v2, Lcom/google/c/e/a/a/a/a/c;->e:Lcom/google/c/e/a/a/a/a/g;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/g;->a:Lcom/google/c/e/a/a/a/a/h;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/h;->a:Ljava/lang/String;

    .line 42
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 43
    invoke-virtual {p0, v1}, Lcom/google/android/gms/notifications/GunsNotificationActivity;->startActivity(Landroid/content/Intent;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/notifications/g;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    .line 48
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 58
    invoke-virtual {p0, p1}, Lcom/google/android/gms/notifications/GunsNotificationActivity;->setIntent(Landroid/content/Intent;)V

    .line 59
    return-void
.end method
