.class public Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/cache/e;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/bp;
.implements Lcom/google/android/gms/wallet/common/ui/cq;
.implements Lcom/google/android/gms/wallet/common/ui/de;


# instance fields
.field protected a:Lcom/google/android/gms/wallet/common/ui/bb;

.field protected b:Lcom/google/android/gms/wallet/address/GoogleTopBarView;

.field protected c:Lcom/google/android/gms/wallet/common/ui/v;

.field protected d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field protected e:Landroid/widget/TextView;

.field f:Lcom/google/android/gms/wallet/common/ui/w;

.field private g:Landroid/accounts/Account;

.field private h:Ljava/util/HashSet;

.field private i:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private j:Lcom/google/checkout/inapp/proto/a/b;

.field private k:Lcom/google/android/gms/wallet/cache/j;

.field private l:Lcom/google/android/gms/identity/intents/UserAddressRequest;

.field private m:Landroid/widget/ProgressBar;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->p:I

    .line 435
    new-instance v0, Lcom/google/android/gms/wallet/address/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/address/d;-><init>(Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/w;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/identity/intents/UserAddressRequest;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 106
    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107
    const-string v1, "request"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 108
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    return-object p1
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 332
    if-nez p2, :cond_0

    .line 333
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    .line 335
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->setResult(ILandroid/content/Intent;)V

    .line 336
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->finish()V

    .line 337
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i()V

    return-void
.end method

.method private a(ZZ)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 344
    move-object v5, p0

    :goto_0
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 345
    if-eqz p2, :cond_0

    .line 346
    invoke-direct {v5, v4}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(Z)V

    .line 348
    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/gms/wallet/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move p2, v4

    move p1, v1

    .line 349
    goto :goto_0

    .line 350
    :cond_1
    if-eqz p1, :cond_5

    .line 354
    iget-object v0, v5, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->k:Lcom/google/android/gms/wallet/cache/j;

    iget-object v2, v5, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;I)Lcom/google/aa/a/a/a/b;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_4

    .line 357
    iget-object v2, v5, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->l:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/aa/a/a/a/b;Lcom/google/android/gms/identity/intents/UserAddressRequest;)[Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    .line 359
    invoke-direct {v5, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    .line 360
    array-length v0, v0

    if-nez v0, :cond_3

    :goto_1
    invoke-direct {v5, v1, v4}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ZZ)V

    .line 371
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v4, v1

    .line 360
    goto :goto_1

    .line 362
    :cond_4
    invoke-direct {v5, v1}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(Z)V

    move p2, v4

    move p1, v1

    .line 363
    goto :goto_0

    .line 365
    :cond_5
    new-instance v0, Lcom/google/android/gms/wallet/cache/c;

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v2

    iget-object v3, v5, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/wallet/cache/c;-><init>(Landroid/content/Context;Lcom/android/volley/s;Landroid/accounts/Account;ILcom/google/android/gms/wallet/cache/e;Landroid/os/Looper;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/cache/c;->run()V

    goto :goto_2
.end method

.method private a([Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 374
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    array-length v0, p1

    if-lez v0, :cond_1

    .line 376
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(Z)V

    .line 377
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->c:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/common/ui/v;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    .line 378
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_0

    .line 379
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->c:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 382
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->c:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/v;->a(Z)V

    .line 384
    :cond_1
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 326
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 327
    const-string v1, "com.google.android.gms.identity.intents.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 328
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ILandroid/content/Intent;)V

    .line 329
    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 387
    iget v3, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->p:I

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->p:I

    .line 388
    iget v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->p:I

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->h()Z

    move-result v3

    if-eq v0, v3, :cond_0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->c:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    :cond_0
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i()V

    .line 389
    return-void

    .line 387
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 388
    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->m:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->c:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    goto :goto_2
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 203
    invoke-static {p0}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "RequestUserAddressActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 209
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->h:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->h:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    new-array v1, v4, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 207
    :cond_2
    invoke-direct {p0, v4, v3}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ZZ)V

    goto :goto_0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 411
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 413
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 414
    return-void

    :cond_1
    move v0, v1

    .line 411
    goto :goto_0

    :cond_2
    move v2, v1

    .line 413
    goto :goto_1
.end method

.method private j()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 426
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 428
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 431
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "App Label not found"

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 287
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    .line 291
    :goto_0
    return-void

    .line 289
    :cond_0
    const/16 v0, 0x19b

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 249
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    .line 250
    packed-switch p1, :pswitch_data_0

    .line 259
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(Z)V

    .line 260
    const-string v0, "RequestUserAddressActivity"

    const-string v1, "Unexpected result from error dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :goto_0
    return-void

    .line 252
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g()V

    goto :goto_0

    .line 255
    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 264
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    goto :goto_0

    .line 250
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    :goto_0
    return-void

    .line 242
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    .line 243
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    .line 244
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g()V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;IILcom/google/checkout/inapp/proto/ai;)V
    .locals 3

    .prologue
    const/16 v2, 0x22b

    .line 296
    if-eqz p4, :cond_0

    iget-object v0, p4, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    if-nez v0, :cond_1

    iget-object v0, p4, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 298
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    .line 311
    :goto_0
    return-void

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->k:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {v0, p1, p2, p4}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/ai;)V

    .line 302
    new-instance v0, Lcom/google/aa/a/a/a/b;

    invoke-direct {v0}, Lcom/google/aa/a/a/a/b;-><init>()V

    .line 303
    iget-object v1, p4, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    iput-object v1, v0, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    .line 304
    iget-object v1, p4, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    iput-object v1, v0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    .line 305
    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->l:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/aa/a/a/a/b;Lcom/google/android/gms/identity/intents/UserAddressRequest;)[Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    .line 306
    if-eqz v0, :cond_2

    array-length v1, v0

    if-nez v1, :cond_3

    .line 307
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b(I)V

    goto :goto_0

    .line 310
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 321
    iget-object v3, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->n:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 323
    return-void

    :cond_0
    move v0, v2

    .line 321
    goto :goto_0

    :cond_1
    move v1, v2

    .line 322
    goto :goto_1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 272
    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 277
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 281
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ILandroid/content/Intent;)V

    .line 282
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.identity.intents.EXTRA_ADDRESS"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(ILandroid/content/Intent;)V

    .line 316
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "request"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/UserAddressRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->l:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->l:Lcom/google/android/gms/identity/intents/UserAddressRequest;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 120
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->c:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 123
    sget v0, Lcom/google/android/gms/l;->gl:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->setContentView(I)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/Window;)V

    .line 125
    sget v0, Lcom/google/android/gms/j;->cc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->n:Landroid/view/View;

    .line 126
    sget v0, Lcom/google/android/gms/j;->te:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->o:Landroid/view/View;

    .line 133
    :goto_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->h:Ljava/util/HashSet;

    .line 134
    sget v0, Lcom/google/android/gms/j;->jp:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/address/GoogleTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b:Lcom/google/android/gms/wallet/address/GoogleTopBarView;

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b:Lcom/google/android/gms/wallet/address/GoogleTopBarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Lcom/google/android/gms/wallet/common/ui/cq;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    .line 139
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    if-eqz p1, :cond_4

    .line 142
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    .line 143
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->h:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 149
    :cond_0
    const-string v0, "selectedAddress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    const-string v0, "selectedAddress"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    .line 158
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->b:Lcom/google/android/gms/wallet/address/GoogleTopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/address/GoogleTopBarView;->a(Landroid/accounts/Account;)V

    .line 159
    sget v0, Lcom/google/android/gms/j;->jn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/v;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->c:Lcom/google/android/gms/wallet/common/ui/v;

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->c:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->f:Lcom/google/android/gms/wallet/common/ui/w;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/android/gms/wallet/common/ui/w;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->c:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/v;->b()V

    .line 163
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    sget v0, Lcom/google/android/gms/j;->fQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    .line 165
    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->c:Lcom/google/android/gms/wallet/common/ui/v;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 166
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bp;)V

    .line 169
    :cond_2
    new-instance v0, Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/cache/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->k:Lcom/google/android/gms/wallet/cache/j;

    .line 170
    sget v0, Lcom/google/android/gms/j;->aI:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->e:Landroid/widget/TextView;

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->e:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    sget v0, Lcom/google/android/gms/j;->pO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->m:Landroid/widget/ProgressBar;

    .line 173
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 176
    invoke-direct {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g()V

    .line 177
    return-void

    .line 128
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->i:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 130
    sget v0, Lcom/google/android/gms/l;->gk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 154
    :cond_4
    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    goto :goto_1
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 181
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "RequestUserAddressActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 187
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 191
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 193
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->g:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 194
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->h:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    .line 197
    const-string v0, "selectedAddress"

    iget-object v1, p0, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->j:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 199
    :cond_0
    return-void
.end method
