.class public abstract Lcom/google/android/gms/wallet/common/ui/validator/d;
.super Lcom/google/android/gms/wallet/common/ui/validator/r;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/validator/r;-><init>(Ljava/lang/CharSequence;)V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/d;->a:Ljava/util/ArrayList;

    .line 21
    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/CharSequence;[Lcom/google/android/gms/wallet/common/ui/validator/r;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/r;-><init>(Ljava/lang/CharSequence;)V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/d;->a:Ljava/util/ArrayList;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 49
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 50
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/validator/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/validator/r;

    .line 52
    instance-of v4, v0, Landroid/os/Parcelable;

    if-eqz v4, :cond_0

    .line 53
    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {p1, p2, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 57
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V
    .locals 1

    .prologue
    .line 29
    if-eqz p1, :cond_0

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 60
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 61
    if-eqz v2, :cond_1

    .line 62
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 63
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 64
    instance-of v4, v0, Lcom/google/android/gms/wallet/common/ui/validator/r;

    if-eqz v4, :cond_0

    .line 65
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/validator/d;->a:Ljava/util/ArrayList;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/validator/r;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 69
    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/gms/wallet/common/ui/validator/r;)V
    .locals 1

    .prologue
    .line 35
    if-eqz p1, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 38
    :cond_0
    return-void
.end method
