.class public Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/util/bd;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Z

.field private d:Landroid/widget/ProgressBar;

.field private e:Ljava/lang/String;

.field private f:Landroid/webkit/WebView;

.field private g:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;ZLjava/lang/CharSequence;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 50
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 51
    if-eqz p1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    const-class v3, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "AuthWebviewUseAuthentication"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "AuthWebviewHomeAsUpEnabled"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "AuthWebviewShowProgressBar"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "AuthWebviewTitle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "AuthWebviewAccountName"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    .line 50
    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->d:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 115
    sget v0, Lcom/google/android/gms/l;->ag:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->setContentView(I)V

    .line 116
    sget v0, Lcom/google/android/gms/j;->de:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->g:Landroid/widget/FrameLayout;

    .line 117
    sget v0, Lcom/google/android/gms/j;->df:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->d:Landroid/widget/ProgressBar;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    .line 119
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    new-instance v1, Lcom/google/android/gms/common/ui/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/ui/b;-><init>(Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    new-instance v1, Lcom/google/android/gms/common/ui/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/ui/c;-><init>(Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-boolean v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 121
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->g:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 122
    return-void

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->e:Ljava/lang/String;

    invoke-static {p0, v0, v1, p0}, Lcom/google/android/gms/common/util/az;->a(Landroid/support/v4/app/q;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/util/bd;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->finish()V

    .line 236
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 217
    :goto_0
    return-void

    .line 215
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/d;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 193
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 195
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f()V

    .line 196
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const-string v0, "com.google.android.gms"

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->setResult(I)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->finish()V

    .line 112
    :goto_0
    return-void

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->e:Ljava/lang/String;

    .line 87
    const-string v3, "AuthWebviewAccountName"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->a:Ljava/lang/String;

    .line 88
    const-string v3, "AuthWebviewUseAuthentication"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->b:Z

    .line 89
    const-string v3, "AuthWebviewShowProgressBar"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->c:Z

    .line 90
    const-string v3, "AuthWebviewTitle"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 91
    const-string v4, "AuthWebviewHomeAsUpEnabled"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 92
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v4, :cond_2

    :cond_1
    move v0, v2

    .line 94
    :goto_1
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v5

    .line 95
    if-eqz v0, :cond_4

    .line 96
    invoke-virtual {v5, v4}, Landroid/support/v7/app/a;->a(Z)V

    .line 98
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    invoke-virtual {v5, v1}, Landroid/support/v7/app/a;->b(Z)V

    .line 104
    :goto_2
    invoke-virtual {v5}, Landroid/support/v7/app/a;->d()V

    .line 109
    :goto_3
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f()V

    .line 111
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->setResult(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 92
    goto :goto_1

    .line 101
    :cond_3
    invoke-virtual {v5, v2}, Landroid/support/v7/app/a;->b(Z)V

    .line 102
    invoke-virtual {v5, v3}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 106
    :cond_4
    invoke-virtual {v5}, Landroid/support/v7/app/a;->e()V

    goto :goto_3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 221
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 222
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->finish()V

    .line 223
    const/4 v0, 0x1

    .line 225
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 206
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 208
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 200
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 202
    return-void
.end method
