.class public final Lcom/google/android/gms/wallet/cache/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:Lcom/google/android/gms/common/util/ap;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lcom/google/android/gms/wallet/cache/k;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/cache/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/cache/j;->d:Lcom/google/android/gms/common/util/ap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/gms/wallet/cache/j;->a:Landroid/content/Context;

    .line 78
    const-string v0, "com.google.android.gms.wallet.cache.WalletPersistentCache"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/j;->b:Landroid/content/SharedPreferences;

    .line 79
    const-string v0, "com.google.android.gms.wallet.cache.CacheUpdateLog"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/j;->c:Landroid/content/SharedPreferences;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/j;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "VERSION"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/j;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "VERSION"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 82
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/google/aa/a/a/a/b;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 390
    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/j;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 391
    if-nez v1, :cond_0

    .line 394
    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/google/aa/a/a/a/b;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/a/a/a/b;

    goto :goto_0
.end method

.method private a(Landroid/accounts/Account;ILcom/google/aa/a/a/a/b;I)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 110
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    and-int/lit8 v0, p4, 0x2

    if-nez v0, :cond_3

    .line 115
    iget-object v0, p3, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v3, v0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    .line 116
    iget-object v4, p3, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, p3, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v0, v0, v2

    iget-boolean v5, v0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    if-nez v5, :cond_2

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v5, v5

    if-ne v5, v7, :cond_2

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    aget v5, v5, v1

    if-ne v5, v8, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    iput-boolean v7, v0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    new-array v6, v7, [I

    aput v8, v6, v1

    invoke-static {v5, v6}, Lcom/google/android/gms/common/util/h;->a([I[I)[I

    move-result-object v5

    iput-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    :cond_2
    aput-object v0, v4, v2

    .line 115
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 121
    :cond_3
    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/cache/j;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v3

    .line 122
    if-eqz p4, :cond_4

    .line 123
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/cache/j;->a(Ljava/lang/String;)Lcom/google/aa/a/a/a/b;

    move-result-object v4

    if-eqz p3, :cond_4

    if-nez v4, :cond_5

    .line 125
    :cond_4
    :goto_2
    invoke-direct {p0, v3, p3}, Lcom/google/android/gms/wallet/cache/j;->a(Ljava/lang/String;Lcom/google/aa/a/a/a/b;)V

    .line 126
    if-nez p4, :cond_0

    .line 127
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/wallet/cache/j;->c(Landroid/accounts/Account;I)V

    goto :goto_0

    .line 123
    :cond_5
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_8

    iget-object v0, v4, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    if-lez v0, :cond_8

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iget-object v2, v4, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v6, v2

    move v0, v1

    :goto_3
    if-ge v0, v6, :cond_6

    aget-object v7, v2, v0

    iget-object v8, v7, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v0, p3, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v6, v0

    move v2, v1

    :goto_4
    if-ge v2, v6, :cond_8

    iget-object v0, p3, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v1, v0, v2

    iget-object v0, v1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_7

    iget-object v7, v1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v7, :cond_7

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v7, :cond_7

    iget-object v7, v1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v8, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v7, v8}, Lcom/google/android/gms/wallet/cache/j;->a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v7

    if-eqz v7, :cond_7

    iget-object v7, p3, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/j;

    aput-object v1, v7, v2

    iget-object v1, p3, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v1, v1, v2

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_8
    invoke-static {p3, v4, p4}, Lcom/google/android/gms/wallet/cache/j;->a(Lcom/google/aa/a/a/a/b;Lcom/google/aa/a/a/a/b;I)V

    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_4

    iget-object v0, v4, Lcom/google/aa/a/a/a/b;->c:Lcom/google/checkout/inapp/proto/a/d;

    iput-object v0, p3, Lcom/google/aa/a/a/a/b;->c:Lcom/google/checkout/inapp/proto/a/d;

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 318
    const-string v0, "com.google.android.gms.wallet.cache.WalletPersistentCache"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 320
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 321
    const-string v0, "com.google.android.gms.wallet.cache.CacheUpdateLog"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 323
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 324
    return-void
.end method

.method private static a(Landroid/content/SharedPreferences;Ljava/util/HashSet;)V
    .locals 4

    .prologue
    .line 327
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 328
    invoke-interface {p0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 329
    invoke-virtual {p1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 330
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 333
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 334
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 335
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 336
    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 338
    :cond_2
    invoke-static {v2}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 340
    :cond_3
    return-void
.end method

.method private static a(Lcom/google/aa/a/a/a/b;Lcom/google/aa/a/a/a/b;I)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 441
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_1

    .line 442
    iget-object v0, p1, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, p1, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, p0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    .line 464
    :cond_0
    return-void

    .line 446
    :cond_1
    iget-object v1, p1, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v1, v1

    if-eqz v1, :cond_0

    .line 449
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_0

    .line 450
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 451
    iget-object v3, p1, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 452
    iget-object v6, v5, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v2, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 454
    :cond_2
    iget-object v1, p0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v3, v1

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    .line 455
    iget-object v0, p0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v4, v0, v1

    .line 456
    iget-object v0, v4, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    .line 457
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 458
    if-eqz v0, :cond_3

    invoke-static {v4, v0}, Lcom/google/android/gms/wallet/cache/j;->a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 459
    iget-boolean v4, v4, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    iput-boolean v4, v0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    .line 460
    iget-object v4, p0, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    aput-object v0, v4, v1

    .line 454
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/google/aa/a/a/a/b;)V
    .locals 2

    .prologue
    .line 398
    invoke-static {p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->d(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    .line 399
    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/j;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 400
    return-void
.end method

.method private a(Ljava/util/ArrayList;I[Landroid/accounts/Account;J)V
    .locals 6

    .prologue
    .line 382
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p3, v0

    .line 383
    invoke-virtual {p0, v2, p2}, Lcom/google/android/gms/wallet/cache/j;->b(Landroid/accounts/Account;I)J

    move-result-wide v4

    cmp-long v3, v4, p4

    if-gez v3, :cond_0

    .line 384
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 387
    :cond_1
    return-void
.end method

.method private static a()Z
    .locals 1

    .prologue
    .line 507
    sget-object v0, Lcom/google/android/gms/wallet/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private static a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 474
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    iget-boolean v2, p1, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    if-eq v1, v2, :cond_1

    .line 490
    :cond_0
    :goto_0
    return v0

    .line 477
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 480
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v1, :cond_2

    .line 481
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/a/e;->a(Lcom/google/t/a/b;Lcom/google/t/a/b;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 487
    :cond_2
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    iget-boolean v2, p1, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    if-ne v1, v2, :cond_0

    .line 490
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 274
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/cache/j;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v2

    .line 278
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/cache/j;->a(Ljava/lang/String;)Lcom/google/aa/a/a/a/b;

    move-result-object v3

    .line 279
    if-eqz v3, :cond_0

    .line 283
    iget-object v1, v3, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v4, v1

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    .line 284
    iget-object v5, v3, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v5, v5, v1

    .line 285
    iget-object v6, v5, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 286
    iget-boolean v0, v5, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    iput-boolean v0, p3, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    .line 287
    iget-object v0, v3, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    aput-object p3, v0, v1

    .line 288
    const/4 v0, 0x1

    .line 292
    :cond_2
    if-nez v0, :cond_3

    .line 293
    iget-object v0, v3, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, v3, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    .line 295
    :cond_3
    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/wallet/cache/j;->a(Ljava/lang/String;Lcom/google/aa/a/a/a/b;)V

    goto :goto_0

    .line 283
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private b(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/j;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 220
    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/cache/j;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v2

    .line 221
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/cache/j;->a(Ljava/lang/String;)Lcom/google/aa/a/a/a/b;

    move-result-object v3

    .line 222
    if-nez v3, :cond_0

    .line 240
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v1, v3, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v4, v1

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    .line 227
    iget-object v5, v3, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v5, v5, v1

    .line 228
    iget-object v6, v5, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 229
    iget-boolean v0, v5, Lcom/google/checkout/inapp/proto/j;->f:Z

    iput-boolean v0, p3, Lcom/google/checkout/inapp/proto/j;->f:Z

    .line 230
    iget-object v0, v3, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    aput-object p3, v0, v1

    .line 231
    const/4 v0, 0x1

    .line 235
    :cond_1
    if-nez v0, :cond_2

    .line 236
    iget-object v0, v3, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/j;

    iput-object v0, v3, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    .line 239
    :cond_2
    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/wallet/cache/j;->a(Ljava/lang/String;Lcom/google/aa/a/a/a/b;)V

    goto :goto_0

    .line 226
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static d(Landroid/accounts/Account;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 403
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;I)Lcom/google/aa/a/a/a/b;
    .locals 1

    .prologue
    .line 89
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    const/4 v0, 0x0

    .line 92
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/cache/j;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/cache/j;->a(Ljava/lang/String;)Lcom/google/aa/a/a/a/b;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILcom/google/aa/a/a/a/b;)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/aa/a/a/a/b;I)V

    .line 100
    return-void
.end method

.method public final a(Landroid/accounts/Account;ILcom/google/aa/a/a/a/f;I)V
    .locals 3

    .prologue
    .line 148
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    new-instance v1, Lcom/google/aa/a/a/a/b;

    invoke-direct {v1}, Lcom/google/aa/a/a/a/b;-><init>()V

    .line 152
    iget-object v0, p3, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    iget-object v2, p3, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v2, v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/j;

    iput-object v0, v1, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    .line 154
    iget-object v0, p3, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    iget-object v2, p3, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v2, v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, v1, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    .line 156
    iget-object v0, p3, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v0, :cond_2

    iget-object v0, p3, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p3, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/d;

    iput-object v0, v1, Lcom/google/aa/a/a/a/b;->c:Lcom/google/checkout/inapp/proto/a/d;

    .line 159
    :cond_2
    invoke-direct {p0, p1, p2, v1, p4}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/aa/a/a/a/b;I)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/a/b;)V
    .locals 1

    .prologue
    .line 247
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v0, p3, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/wallet/cache/j;->b(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 260
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/wallet/cache/j;->b(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/ai;)V
    .locals 4

    .prologue
    .line 167
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    new-instance v1, Lcom/google/aa/a/a/a/b;

    invoke-direct {v1}, Lcom/google/aa/a/a/a/b;-><init>()V

    .line 171
    iget-object v0, p3, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    iget-object v2, p3, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    sget-object v3, Lcom/google/android/gms/wallet/cache/j;->d:Lcom/google/android/gms/common/util/ap;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Lcom/google/android/gms/common/util/ap;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/j;

    iput-object v0, v1, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    .line 173
    iget-object v0, p3, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    iget-object v2, p3, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v2, v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, v1, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    .line 177
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/aa/a/a/a/b;I)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/j;)V
    .locals 1

    .prologue
    .line 193
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-object v0, p3, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/wallet/cache/j;->b(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/j;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/j;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 206
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/wallet/cache/j;->b(Landroid/accounts/Account;ILcom/google/checkout/inapp/proto/j;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a([Landroid/accounts/Account;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 302
    if-nez p1, :cond_0

    .line 312
    :goto_0
    return-void

    .line 305
    :cond_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 306
    array-length v3, p1

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, p1, v0

    .line 307
    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/gms/wallet/cache/j;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 308
    invoke-static {v4, v1}, Lcom/google/android/gms/wallet/cache/j;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 306
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/j;->b:Landroid/content/SharedPreferences;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/content/SharedPreferences;Ljava/util/HashSet;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/j;->c:Landroid/content/SharedPreferences;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/content/SharedPreferences;Ljava/util/HashSet;)V

    goto :goto_0
.end method

.method public final b(Landroid/accounts/Account;I)J
    .locals 4

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/j;->c:Landroid/content/SharedPreferences;

    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/cache/j;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b([Landroid/accounts/Account;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 366
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 367
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 377
    :cond_0
    :goto_0
    return-object v1

    .line 370
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x240c8400

    sub-long v4, v2, v4

    .line 372
    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/cache/j;->a(Ljava/util/ArrayList;I[Landroid/accounts/Account;J)V

    .line 374
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/j;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/cache/j;->a(Ljava/util/ArrayList;I[Landroid/accounts/Account;J)V

    goto :goto_0
.end method

.method public final c(Landroid/accounts/Account;I)V
    .locals 4

    .prologue
    .line 354
    invoke-static {}, Lcom/google/android/gms/wallet/cache/j;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/j;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/cache/j;->d(Landroid/accounts/Account;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method
