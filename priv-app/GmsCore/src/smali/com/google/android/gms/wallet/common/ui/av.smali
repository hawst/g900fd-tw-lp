.class public final Lcom/google/android/gms/wallet/common/ui/av;
.super Lcom/google/android/gms/wallet/common/ui/at;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>([Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/at;-><init>([Landroid/widget/ImageView;)V

    .line 20
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/av;->c:Landroid/widget/ImageView;

    .line 21
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 7

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 25
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/av;->b:I

    if-eq p1, v0, :cond_2

    .line 26
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/av;->c(I)I

    move-result v4

    .line 27
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/av;->a:[Landroid/widget/ImageView;

    array-length v5, v3

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_1

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/av;->a:[Landroid/widget/ImageView;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    if-ne v3, v4, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {v6, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 27
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 28
    goto :goto_1

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/av;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v3, -0x1

    if-ne v4, v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 31
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/av;->b:I

    .line 33
    :cond_2
    return-void

    :cond_3
    move v1, v2

    .line 30
    goto :goto_2
.end method

.method public final b(I)V
    .locals 7

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 37
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/av;->c(I)I

    move-result v4

    .line 38
    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/av;->a:[Landroid/widget/ImageView;

    array-length v5, v3

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_1

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/av;->a:[Landroid/widget/ImageView;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/av;->a:[Landroid/widget/ImageView;

    aget-object v6, v0, v3

    if-ne v3, v4, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 38
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 42
    goto :goto_1

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/av;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/av;->c:Landroid/widget/ImageView;

    const/4 v3, -0x1

    if-ne v4, v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 46
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/av;->b:I

    .line 47
    return-void

    :cond_2
    move v1, v2

    .line 45
    goto :goto_2
.end method
