.class public Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 29
    sget v0, Lcom/google/android/gms/j;->td:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->a:Landroid/view/View;

    .line 30
    sget v0, Lcom/google/android/gms/j;->bc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->b:Landroid/widget/TextView;

    .line 31
    return-void
.end method
