.class final Lcom/google/android/gms/feedback/ao;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/feedback/SuggestionsActivity;

.field final synthetic b:Lcom/google/android/gms/feedback/SuggestionsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/feedback/SuggestionsActivity;Lcom/google/android/gms/feedback/SuggestionsActivity;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    iput-object p2, p0, Lcom/google/android/gms/feedback/ao;->a:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 4

    .prologue
    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ao;->a:Lcom/google/android/gms/feedback/SuggestionsActivity;

    iget-object v2, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v2}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v3}, Lcom/google/android/gms/feedback/SuggestionsActivity;->b(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/feedback/an;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Lcom/google/android/gms/feedback/SuggestionsActivity;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    const-string v1, "GFEEDBACK_SuggestionsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error retrieving suggestions: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/google/android/gms/feedback/ao;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->b(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->b(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->b(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    const-string v1, "helpSessionId"

    iget-object v2, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v2}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->c(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    sget-object v0, Lcom/google/android/gms/feedback/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Lcom/google/android/gms/feedback/SuggestionsActivity;ZZ)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v0, v3, v3}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Lcom/google/android/gms/feedback/SuggestionsActivity;ZZ)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v0}, Lcom/google/android/gms/feedback/SuggestionsActivity;->d(Lcom/google/android/gms/feedback/SuggestionsActivity;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ao;->b:Lcom/google/android/gms/feedback/SuggestionsActivity;

    invoke-static {v1}, Lcom/google/android/gms/feedback/SuggestionsActivity;->e(Lcom/google/android/gms/feedback/SuggestionsActivity;)Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    move-result-object v1

    const-string v2, "SHOWN_FEEDBACK_SUGGESTION"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/feedback/SuggestionsActivity;->a(Lcom/google/android/gms/feedback/SuggestionsActivity;Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;Ljava/lang/String;)V

    goto :goto_0
.end method
