.class public final Lcom/google/android/gms/googlehelp/fragments/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field public final b:Lcom/google/android/gms/googlehelp/search/c;

.field public final c:Landroid/widget/ListView;

.field public final d:Lcom/google/android/gms/googlehelp/fragments/i;

.field private final e:Lcom/google/android/gms/googlehelp/common/HelpConfig;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/l;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->e:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 50
    sget v1, Lcom/google/android/gms/j;->jd:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->c:Landroid/widget/ListView;

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->e:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->co:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/l;->c:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 55
    new-instance v1, Lcom/google/android/gms/googlehelp/search/c;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/l;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    sget v3, Lcom/google/android/gms/j;->je:I

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/gms/googlehelp/search/c;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Landroid/view/View;I)V

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/l;->b:Lcom/google/android/gms/googlehelp/search/c;

    .line 59
    :goto_0
    new-instance v0, Lcom/google/android/gms/googlehelp/fragments/i;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/l;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-direct {v0, v1}, Lcom/google/android/gms/googlehelp/fragments/i;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->d:Lcom/google/android/gms/googlehelp/fragments/i;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/l;->d:Lcom/google/android/gms/googlehelp/fragments/i;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 61
    return-void

    .line 57
    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/l;->b:Lcom/google/android/gms/googlehelp/search/c;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/l;->d:Lcom/google/android/gms/googlehelp/fragments/i;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/fragments/i;->b()Lcom/google/android/gms/googlehelp/common/t;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/fragments/o;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/t;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/gms/googlehelp/fragments/o;->a(Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/t;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/googlehelp/fragments/o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Z)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->b:Lcom/google/android/gms/googlehelp/search/c;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/l;->b:Lcom/google/android/gms/googlehelp/search/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/googlehelp/search/c;->a(Ljava/lang/CharSequence;Z)V

    .line 101
    :cond_0
    return-void
.end method
