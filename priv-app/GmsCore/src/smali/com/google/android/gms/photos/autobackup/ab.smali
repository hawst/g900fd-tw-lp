.class public final Lcom/google/android/gms/photos/autobackup/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ab;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ab;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->c(I)V

    .line 81
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 3

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "plusone:autobackup_allow_migration"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ab;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->c(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "SetAutoBackupSettingsOp"

    const-string v1, "Failed to deliver failure result"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    const-class v0, Lcom/google/android/gms/photos/autobackup/a;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/a;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/photos/autobackup/ac;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/photos/autobackup/ac;-><init>(Lcom/google/android/gms/photos/autobackup/ab;Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a;->a(Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/c;)V

    goto :goto_0
.end method
