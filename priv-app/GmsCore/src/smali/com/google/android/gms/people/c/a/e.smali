.class final Lcom/google/android/gms/people/c/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/people/c/a/f;

.field private final c:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/people/c/a/f;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/a/e;->c:Landroid/content/ContentValues;

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/people/c/a/e;->a:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/people/c/a/e;->b:Lcom/google/android/gms/people/c/a/f;

    .line 38
    return-void
.end method

.method private a()Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/e;->c:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/people/c/a/e;->c:Landroid/content/ContentValues;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/people/c/e;JJLjava/lang/String;I)V
    .locals 4

    .prologue
    .line 211
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/c/a/e;->a()Landroid/content/ContentValues;

    move-result-object v0

    .line 218
    const-string v1, "owner_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 219
    const-string v1, "item_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 221
    invoke-static {p1, v0, p6, p7}, Lcom/google/android/gms/people/c/a/e;->a(Lcom/google/android/gms/people/c/e;Landroid/content/ContentValues;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/people/c/e;Landroid/content/ContentValues;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 158
    const-string v0, "is_normalized"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 159
    const-string v0, "kind"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 161
    invoke-static {p2}, Lcom/google/android/gms/people/c/a/f;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 162
    const-string v5, "value"

    invoke-static {v0}, Lcom/google/android/gms/people/c/a/f;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v5, "ac_index"

    invoke-virtual {p0, v5, p1}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 166
    invoke-static {v0}, Lcom/google/android/gms/people/c/a/f;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 167
    invoke-static {v0}, Lcom/google/android/gms/people/c/a/f;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move v0, v1

    .line 170
    :goto_1
    array-length v6, v5

    if-ge v0, v6, :cond_0

    .line 171
    const-string v6, "value"

    aget-object v7, v5, v0

    invoke-static {v7}, Lcom/google/android/gms/people/c/a/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v6, "ac_index"

    invoke-virtual {p0, v6, p1}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 161
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 176
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/people/c/e;J)V
    .locals 12

    .prologue
    .line 82
    invoke-virtual {p1}, Lcom/google/android/gms/people/c/e;->d()V

    .line 86
    const-string v0, "SELECT p.owner_id,i._id,i.item_type,i.value,i.value2,c.nickname,c.given_name,c.family_name,c.middle_name,c.honorific_prefix,c.honorific_suffix,c.yomi_given_name,c.yomi_family_name,c.yomi_honorific_prefix,c.yomi_honorific_suffix FROM ac_people AS p JOIN ac_container AS c ON p._id=c.people_id JOIN ac_item AS i ON c._id=i.container_id WHERE (p._id = ?1)"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 111
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    .line 119
    :cond_0
    :goto_0
    :pswitch_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v2, v0

    .line 121
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 122
    const/4 v0, 0x2

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 124
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 134
    :pswitch_1
    int-to-long v4, v11

    const/4 v0, 0x5

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x28

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/c/a/e;->a(Lcom/google/android/gms/people/c/e;JJLjava/lang/String;I)V

    .line 137
    const/4 v0, 0x6

    move v8, v0

    :goto_1
    if-gt v8, v10, :cond_0

    .line 138
    int-to-long v4, v11

    invoke-interface {v9, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x14

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/c/a/e;->a(Lcom/google/android/gms/people/c/e;JJLjava/lang/String;I)V

    .line 137
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 126
    :pswitch_2
    int-to-long v0, v11

    const/4 v4, 0x3

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/people/c/a/e;->a()Landroid/content/ContentValues;

    move-result-object v5

    const-string v6, "owner_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "item_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "is_normalized"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "kind"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "value"

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ac_index"

    invoke-virtual {p1, v0, v5}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    const/4 v0, 0x1

    invoke-static {p1, v5, v4, v0}, Lcom/google/android/gms/people/c/a/e;->a(Lcom/google/android/gms/people/c/e;Landroid/content/ContentValues;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 146
    return-void

    .line 124
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
