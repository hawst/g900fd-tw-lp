.class public final Lcom/google/android/gms/drive/b/a/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/b/a/g;


# static fields
.field private static final a:Ljava/nio/charset/Charset;


# instance fields
.field private final b:I

.field private final c:Ljava/io/OutputStream;

.field private final d:[B

.field private e:I

.field private f:Lcom/google/android/gms/drive/b/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/b/a/r;->a:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/drive/b/a/r;->c:Ljava/io/OutputStream;

    .line 35
    iput p2, p0, Lcom/google/android/gms/drive/b/a/r;->b:I

    .line 36
    new-array v0, p2, [B

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->d:[B

    .line 37
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->c:Ljava/io/OutputStream;

    iget v1, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/b/a/r;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->c:Ljava/io/OutputStream;

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->c:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/r;->d:[B

    iget v2, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->c:Ljava/io/OutputStream;

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write(I)V

    .line 84
    iput v3, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    .line 85
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->c:Ljava/io/OutputStream;

    const-string v1, "%d-%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    iget-wide v4, v4, Lcom/google/android/gms/drive/b/a/b;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    iget-wide v4, v4, Lcom/google/android/gms/drive/b/a/b;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/b/a/r;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->c:Ljava/io/OutputStream;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    .line 94
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 70
    iget v0, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    if-eqz v0, :cond_1

    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/a/r;->b()V

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    if-eqz v0, :cond_2

    .line 74
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/a/r;->c()V

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->c:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 77
    return-void

    .line 69
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(B)V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    if-eqz v0, :cond_0

    .line 42
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/a/r;->c()V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->d:[B

    iget v1, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    aput-byte p1, v0, v1

    .line 45
    iget v0, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    iget v1, p0, Lcom/google/android/gms/drive/b/a/r;->b:I

    if-ne v0, v1, :cond_1

    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/a/r;->b()V

    .line 48
    :cond_1
    return-void
.end method

.method public final a(JI)V
    .locals 11

    .prologue
    const-wide/16 v8, 0x1

    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    cmp-long v0, p1, v6

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 53
    if-lez p3, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 54
    iget v0, p0, Lcom/google/android/gms/drive/b/a/r;->e:I

    if-eqz v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/a/r;->b()V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    iget-wide v4, v0, Lcom/google/android/gms/drive/b/a/b;->b:J

    add-long/2addr v4, v8

    cmp-long v0, v4, p1

    if-nez v0, :cond_4

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    int-to-long v4, p3

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    new-instance v1, Lcom/google/android/gms/drive/b/a/b;

    iget-wide v2, v0, Lcom/google/android/gms/drive/b/a/b;->a:J

    iget-wide v6, v0, Lcom/google/android/gms/drive/b/a/b;->b:J

    add-long/2addr v4, v6

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/b/a/b;-><init>(JJ)V

    iput-object v1, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    .line 65
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 52
    goto :goto_0

    :cond_2
    move v0, v2

    .line 53
    goto :goto_1

    :cond_3
    move v1, v2

    .line 60
    goto :goto_2

    .line 62
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/a/r;->c()V

    .line 63
    new-instance v0, Lcom/google/android/gms/drive/b/a/b;

    int-to-long v2, p3

    add-long/2addr v2, p1

    sub-long/2addr v2, v8

    invoke-direct {v0, p1, p2, v2, v3}, Lcom/google/android/gms/drive/b/a/b;-><init>(JJ)V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/r;->f:Lcom/google/android/gms/drive/b/a/b;

    goto :goto_3
.end method
