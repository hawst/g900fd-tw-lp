.class final Lcom/google/android/gms/social/location/x;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lcom/google/android/gms/social/location/x;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;B)V
    .locals 0

    .prologue
    .line 472
    invoke-direct {p0, p1}, Lcom/google/android/gms/social/location/x;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 496
    iget-boolean v0, p0, Lcom/google/android/gms/social/location/x;->b:Z

    return v0
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 472
    new-instance v0, Lcom/google/android/gms/social/location/i;

    iget-object v1, p0, Lcom/google/android/gms/social/location/x;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/social/location/x;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/social/location/i;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/social/location/i;->a()Lcom/google/android/gms/social/location/DeviceLocationSettings;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 472
    check-cast p1, Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iput-boolean v0, p0, Lcom/google/android/gms/social/location/x;->b:Z

    iget-object v1, p0, Lcom/google/android/gms/social/location/x;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v1, p1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Lcom/google/android/gms/social/location/DeviceLocationSettings;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    iget-object v1, p0, Lcom/google/android/gms/social/location/x;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    iget-object v2, p0, Lcom/google/android/gms/social/location/x;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v2}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->c(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/DeviceLocationSettings;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/social/location/x;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->d(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    iget-object v0, p0, Lcom/google/android/gms/social/location/x;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onPreExecute()V
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/gms/social/location/x;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    .line 478
    return-void
.end method
