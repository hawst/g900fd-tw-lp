.class public final Lcom/google/android/gms/social/location/r;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/common/internal/a/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/support/v4/app/v;)Lcom/google/android/gms/social/location/r;
    .locals 3

    .prologue
    .line 23
    const-string v0, "LocationSharingActivityRetainFragment"

    invoke-virtual {p0, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/r;

    .line 25
    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/google/android/gms/social/location/r;

    invoke-direct {v0}, Lcom/google/android/gms/social/location/r;-><init>()V

    .line 27
    invoke-virtual {p0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "LocationSharingActivityRetainFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    .line 29
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/r;->setRetainInstance(Z)V

    .line 36
    return-void
.end method
