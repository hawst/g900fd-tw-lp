.class public final Lcom/google/android/gms/ads/internal/p;
.super Lcom/google/android/gms/ads/internal/client/l;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/ads/internal/client/f;

.field private b:Lcom/google/android/gms/ads/internal/f/a/g;

.field private c:Lcom/google/android/gms/ads/internal/f/a/j;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/ads/internal/k/a/a;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/client/l;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p;->d:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/p;->f:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/p;->e:Lcom/google/android/gms/ads/internal/k/a/a;

    .line 38
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/p;->g:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/ads/internal/client/i;
    .locals 8

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/gms/ads/internal/n;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/p;->e:Lcom/google/android/gms/ads/internal/k/a/a;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/p;->g:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/p;->a:Lcom/google/android/gms/ads/internal/client/f;

    iget-object v6, p0, Lcom/google/android/gms/ads/internal/p;->b:Lcom/google/android/gms/ads/internal/f/a/g;

    iget-object v7, p0, Lcom/google/android/gms/ads/internal/p;->c:Lcom/google/android/gms/ads/internal/f/a/j;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/ads/internal/n;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/ads/internal/client/f;Lcom/google/android/gms/ads/internal/f/a/g;Lcom/google/android/gms/ads/internal/f/a/j;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/f;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p;->a:Lcom/google/android/gms/ads/internal/client/f;

    .line 56
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/f/a/g;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p;->b:Lcom/google/android/gms/ads/internal/f/a/g;

    .line 66
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/f/a/j;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p;->c:Lcom/google/android/gms/ads/internal/f/a/j;

    .line 61
    return-void
.end method
