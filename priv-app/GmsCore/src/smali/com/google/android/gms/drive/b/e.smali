.class final Lcom/google/android/gms/drive/b/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/b/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/b/d;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/gms/drive/b/e;->a:Lcom/google/android/gms/drive/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    const/4 v9, 0x0

    const/4 v12, 0x1

    .line 72
    iget-object v1, p0, Lcom/google/android/gms/drive/b/e;->a:Lcom/google/android/gms/drive/b/d;

    const-string v0, "ContentMaintenance"

    const-string v2, "Starting content maintenance."

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v1, Lcom/google/android/gms/drive/b/d;->f:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v3, v1, Lcom/google/android/gms/drive/b/d;->g:Ljava/util/Collection;

    invoke-interface {v0, v3}, Lcom/google/android/gms/drive/database/r;->a(Ljava/util/Collection;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const-string v0, "ContentMaintenance"

    const-string v2, "Pruning caches..."

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/w;->c()J

    move-result-wide v2

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->p()J

    move-result-wide v4

    const-string v0, "ContentMaintenance"

    const-string v6, "Internal cache bytes used: %d; limit: %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v12

    invoke-static {v0, v6, v7}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    cmp-long v0, v4, v2

    if-lez v0, :cond_2

    const-string v0, "ContentMaintenance"

    const-string v4, "Evicting LRU items from internal cache..."

    invoke-static {v0, v4}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->b()V

    :try_start_1
    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->o()Lcom/google/android/gms/drive/database/b/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    :try_start_2
    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ax;

    iget-object v6, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v6}, Lcom/google/android/gms/drive/database/r;->p()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-lez v6, :cond_1

    iget-object v6, v1, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/g/w;->f()Ljava/io/File;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/b/d;->a(Lcom/google/android/gms/drive/database/model/ax;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_4
    const-string v6, "ContentMaintenance"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Evicting from internal cache: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v0, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "ContentMaintenance"

    const-string v7, "%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-wide v10, v0, Lcom/google/android/gms/drive/database/model/ax;->g:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ax;->j()V

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/d;->c()V

    const-string v0, "ContentMaintenance"

    const-string v2, "Cache pruning complete."

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/w;->f()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v2, v1, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    sget-object v0, Lcom/google/android/gms/drive/ai;->av:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/drive/ai;->aw:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/gms/drive/g/w;->b()J

    move-result-wide v6

    long-to-float v3, v6

    mul-float/2addr v0, v3

    float-to-long v6, v0

    const-wide/16 v8, 0x0

    invoke-virtual {v2}, Lcom/google/android/gms/drive/g/w;->a()J

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/drive/ai;->ax:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long/2addr v2, v10

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->n()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-lez v0, :cond_3

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->m()Lcom/google/android/gms/drive/database/b/d;

    move-result-object v4

    :try_start_6
    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ax;

    iget-object v6, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v6}, Lcom/google/android/gms/drive/database/r;->n()J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result-wide v6

    cmp-long v6, v6, v2

    if-gtz v6, :cond_5

    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->close()V

    :cond_3
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/d;->d()V

    iget-wide v2, v1, Lcom/google/android/gms/drive/b/d;->h:J

    sget-object v0, Lcom/google/android/gms/drive/ai;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->d:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gtz v0, :cond_4

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->d:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/gms/drive/b/d;->h:J

    iget-object v0, v1, Lcom/google/android/gms/drive/b/d;->c:Lcom/google/android/gms/drive/c/b;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/b;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->b()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    const/16 v2, 0x18

    invoke-interface {v0, v12, v2}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/drive/b/d;->b:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/g/w;->c()J

    move-result-wide v2

    iget-object v1, v1, Lcom/google/android/gms/drive/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->p()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/android/gms/drive/c/a;->a(JJ)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->a()V

    .line 73
    :cond_4
    return-void

    .line 72
    :cond_5
    :try_start_7
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/b/d;->a(Lcom/google/android/gms/drive/database/model/ax;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v0

    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0

    :cond_6
    invoke-interface {v4}, Lcom/google/android/gms/drive/database/b/d;->close()V

    goto :goto_2
.end method
