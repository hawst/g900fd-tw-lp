.class public final Lcom/google/android/gms/ads/settings/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 15
    const-string v0, "vending_ad_prefs_more_url"

    const-string v1, "https://www.google.com/ads/preferences/html/mobile-about.html"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/a/b;->a:Lcom/google/android/gms/common/a/d;

    .line 22
    const-string v0, "enable_gms_limit_ad_tracking_setting"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/a/b;->b:Lcom/google/android/gms/common/a/d;

    .line 28
    const-string v0, "enable_gms_adid_setting"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/a/b;->c:Lcom/google/android/gms/common/a/d;

    .line 34
    const-string v0, "adid_prefs_more_url"

    const-string v1, "https://support.google.com/googleplay/answer/3405269"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/a/b;->d:Lcom/google/android/gms/common/a/d;

    return-void
.end method
