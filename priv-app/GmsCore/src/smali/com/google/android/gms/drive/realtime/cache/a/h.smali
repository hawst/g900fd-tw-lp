.class public final enum Lcom/google/android/gms/drive/realtime/cache/a/h;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/realtime/cache/a/h;

.field public static final enum b:Lcom/google/android/gms/drive/realtime/cache/a/h;

.field public static final enum c:Lcom/google/android/gms/drive/realtime/cache/a/h;

.field private static final synthetic e:[Lcom/google/android/gms/drive/realtime/cache/a/h;


# instance fields
.field private final d:Lcom/google/android/gms/drive/database/model/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 50
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/a/h;

    const-string v1, "DOCUMENT_STORE_TABLE"

    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/gms/drive/realtime/cache/a/h;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/h;->a:Lcom/google/android/gms/drive/realtime/cache/a/h;

    .line 51
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/a/h;

    const-string v1, "MUTATION_HISTORY_TABLE"

    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a()Lcom/google/android/gms/drive/realtime/cache/a/c;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/realtime/cache/a/h;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/h;->b:Lcom/google/android/gms/drive/realtime/cache/a/h;

    .line 52
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/a/h;

    const-string v1, "PENDING_MUTATIONS_TABLE"

    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/e;->a()Lcom/google/android/gms/drive/realtime/cache/a/e;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/gms/drive/realtime/cache/a/h;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/h;->c:Lcom/google/android/gms/drive/realtime/cache/a/h;

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/drive/realtime/cache/a/h;

    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/h;->a:Lcom/google/android/gms/drive/realtime/cache/a/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/h;->b:Lcom/google/android/gms/drive/realtime/cache/a/h;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/h;->c:Lcom/google/android/gms/drive/realtime/cache/a/h;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/h;->e:[Lcom/google/android/gms/drive/realtime/cache/a/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/cache/a/h;->d:Lcom/google/android/gms/drive/database/model/ae;

    .line 59
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/realtime/cache/a/h;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/gms/drive/realtime/cache/a/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/realtime/cache/a/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/realtime/cache/a/h;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/a/h;->e:[Lcom/google/android/gms/drive/realtime/cache/a/h;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/realtime/cache/a/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/realtime/cache/a/h;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/a/h;->d:Lcom/google/android/gms/drive/database/model/ae;

    return-object v0
.end method
