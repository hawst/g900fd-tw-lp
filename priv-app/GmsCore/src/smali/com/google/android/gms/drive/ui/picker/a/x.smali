.class public final Lcom/google/android/gms/drive/ui/picker/a/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/picker/a/l;


# instance fields
.field private final a:Lcom/google/android/gms/drive/ak;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/ak;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->a:Lcom/google/android/gms/drive/ak;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    .line 24
    return-void
.end method

.method private q()Lcom/google/android/gms/drive/aj;
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->a:Lcom/google/android/gms/drive/ak;

    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ak;->b(I)Lcom/google/android/gms/drive/aj;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->a:Lcom/google/android/gms/drive/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ak;->c()I

    move-result v0

    if-ge p1, v0, :cond_1

    const/4 v0, 0x1

    .line 91
    :goto_0
    if-eqz v0, :cond_0

    .line 92
    iput p1, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    .line 94
    :cond_0
    return v0

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->q()Z

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->a:Lcom/google/android/gms/drive/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ak;->w_()V

    .line 71
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->j()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->p()Z

    move-result v0

    return v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->i()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->f()Ljava/util/Date;

    move-result-object v0

    .line 65
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->a:Lcom/google/android/gms/drive/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ak;->c()I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    return v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 100
    :goto_0
    if-eqz v0, :cond_0

    .line 101
    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    .line 103
    :cond_0
    return v0

    .line 99
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 108
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->a:Lcom/google/android/gms/drive/ak;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/ak;->c()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    .line 109
    :goto_0
    if-eqz v0, :cond_0

    .line 110
    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    .line 112
    :cond_0
    return v0

    .line 108
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->a:Lcom/google/android/gms/drive/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ak;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/ui/picker/a/x;->b:I

    .line 118
    return-void
.end method

.method public final n()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->h()Ljava/util/Date;

    move-result-object v0

    .line 128
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/x;->q()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->k()Ljava/util/Date;

    move-result-object v0

    .line 134
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method
