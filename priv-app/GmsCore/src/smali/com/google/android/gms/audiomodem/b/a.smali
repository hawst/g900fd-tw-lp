.class public final Lcom/google/android/gms/audiomodem/b/a;
.super Lcom/google/android/gms/audiomodem/a/i;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Landroid/content/pm/PackageManager;

.field private final d:Lcom/google/android/gms/audiomodem/n;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/a/i;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/b/a;->a:Landroid/content/Context;

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/b/a;->b:Landroid/os/Handler;

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/b/a;->c:Landroid/content/pm/PackageManager;

    .line 47
    new-instance v0, Lcom/google/android/gms/audiomodem/n;

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/b/a;->b:Landroid/os/Handler;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/audiomodem/n;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/b/a;->d:Lcom/google/android/gms/audiomodem/n;

    .line 48
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/audiomodem/b/a;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/audiomodem/b/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/audiomodem/b/a;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/audiomodem/b/a;)Lcom/google/android/gms/audiomodem/n;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/b/a;->d:Lcom/google/android/gms/audiomodem/n;

    return-object v0
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 128
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/b/a;->c:Landroid/content/pm/PackageManager;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/b/a;->c:Landroid/content/pm/PackageManager;

    aget-object v0, v1, v0

    invoke-static {v2, v0}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;Lcom/google/android/gms/audiomodem/a/n;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 7

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 82
    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/audiomodem/b/a;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/audiomodem/b/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/audiomodem/b/d;-><init>(Lcom/google/android/gms/audiomodem/b/a;Landroid/os/IBinder;Lcom/google/android/gms/audiomodem/a/n;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;Lcom/google/android/gms/audiomodem/a/e;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 87
    return-void
.end method

.method public final a(Landroid/os/IBinder;Lcom/google/android/gms/audiomodem/a/q;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 7

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 56
    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/audiomodem/b/a;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/audiomodem/b/b;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/audiomodem/b/b;-><init>(Lcom/google/android/gms/audiomodem/b/a;Landroid/os/IBinder;Lcom/google/android/gms/audiomodem/a/q;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;Lcom/google/android/gms/audiomodem/a/e;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 61
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/Snoop$Params;Lcom/google/android/gms/audiomodem/a/k;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/b/a;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/audiomodem/b/f;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/gms/audiomodem/b/f;-><init>(Lcom/google/android/gms/audiomodem/b/a;Lcom/google/android/gms/audiomodem/a/k;Lcom/google/android/gms/audiomodem/Snoop$Params;Lcom/google/android/gms/audiomodem/a/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 114
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/a/n;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/b/a;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/audiomodem/b/e;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/audiomodem/b/e;-><init>(Lcom/google/android/gms/audiomodem/b/a;Lcom/google/android/gms/audiomodem/a/n;Lcom/google/android/gms/audiomodem/a/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 100
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/a/q;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/b/a;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/audiomodem/b/c;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/audiomodem/b/c;-><init>(Lcom/google/android/gms/audiomodem/b/a;Lcom/google/android/gms/audiomodem/a/q;Lcom/google/android/gms/audiomodem/a/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 74
    return-void
.end method

.method public final dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 121
    const-string v0, "\nAudioModem State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 123
    const-string v0, "\nEnd AudioModem State\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 125
    return-void
.end method
