.class public final Lcom/google/android/gms/plus/internal/model/apps/e;
.super Lcom/google/android/gms/common/data/i;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/model/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/i;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 21
    return-void
.end method


# virtual methods
.method public final synthetic c()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 17
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Please use ApplicationEntity.from(application)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string v0, "display_name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "icon_url"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, "application_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/content/pm/ApplicationInfo;
    .locals 1

    .prologue
    .line 45
    const-string v0, "application_info"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/e;->h(Ljava/lang/String;)[B

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/model/apps/c;->a([B)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 51
    const-string v0, "is_aspen"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/e;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 56
    const-string v0, "is_fitness"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/e;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "scopes"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string v0, "revoke_handle"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 71
    const-string v0, "has_conn_read"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/model/apps/e;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
