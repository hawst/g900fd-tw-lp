.class public final Lcom/google/android/gms/plus/service/a/ai;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/ai;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/ai;->b:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/ai;->c:Lcom/google/android/gms/plus/internal/f;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 39
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ai;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/ai;->b:Ljava/lang/String;

    iget-object v2, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/plus/b/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ai;->c:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/ai;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 54
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 43
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v4, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 44
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ai;->c:Lcom/google/android/gms/plus/internal/f;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/ai;->b:Ljava/lang/String;

    invoke-interface {v0, v5, v1, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ai;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/ai;->c:Lcom/google/android/gms/plus/internal/f;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/ai;->b:Ljava/lang/String;

    invoke-interface {v1, v5, v0, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ai;->c:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/ai;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v6, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ai;->c:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/ai;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Ljava/lang/String;)V

    .line 59
    return-void
.end method
