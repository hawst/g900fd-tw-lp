.class public final Lcom/google/android/gms/fitness/sensors/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/sensors/c/b;


# static fields
.field private static final a:Lcom/google/android/gms/fitness/data/DataType;

.field private static final b:Lcom/google/android/gms/fitness/data/DataType;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->b:Lcom/google/android/gms/fitness/data/DataType;

    sput-object v0, Lcom/google/android/gms/fitness/sensors/c/a;->a:Lcom/google/android/gms/fitness/data/DataType;

    .line 38
    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->a:Lcom/google/android/gms/fitness/data/DataType;

    sput-object v0, Lcom/google/android/gms/fitness/sensors/c/a;->b:Lcom/google/android/gms/fitness/data/DataType;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/c/a;->c:Landroid/content/Context;

    .line 46
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/a;->d:Ljava/util/concurrent/atomic/AtomicReference;

    .line 47
    return-void
.end method

.method private a(Lcom/google/android/gms/fitness/data/DataPoint;Z)Lcom/google/android/gms/fitness/data/DataPoint;
    .locals 6

    .prologue
    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/a;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 126
    if-eqz p2, :cond_1

    if-eqz v0, :cond_1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 128
    const-string v1, "Out of order data point: %s is before %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 129
    const/4 v0, 0x0

    .line 135
    :goto_0
    return-object v0

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/c/a;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method private static b(Lcom/google/android/gms/fitness/data/DataPoint;)I
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->a()[Lcom/google/android/gms/fitness/data/Value;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Value;->c()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/fitness/data/DataSource;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/gms/fitness/data/f;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/data/f;-><init>()V

    sget-object v1, Lcom/google/android/gms/fitness/sensors/c/a;->b:Lcom/google/android/gms/fitness/data/DataType;

    iput-object v1, v0, Lcom/google/android/gms/fitness/data/f;->a:Lcom/google/android/gms/fitness/data/DataType;

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/c/a;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/fitness/data/Device;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/Device;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/fitness/data/f;->d:Lcom/google/android/gms/fitness/data/Device;

    sget-object v1, Lcom/google/android/gms/fitness/data/Application;->a:Lcom/google/android/gms/fitness/data/Application;

    iput-object v1, v0, Lcom/google/android/gms/fitness/data/f;->e:Lcom/google/android/gms/fitness/data/Application;

    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/gms/fitness/data/f;->b:I

    const-string v1, "live_step_deltas"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/f;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataPoint;)Ljava/lang/Iterable;
    .locals 10

    .prologue
    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 72
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataPoint;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/sensors/c/a;->a:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Require cumulative step counts instead of: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v7

    :goto_0
    if-nez v0, :cond_1

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 110
    :goto_1
    return-object v0

    :cond_0
    move v0, v8

    .line 72
    goto :goto_0

    .line 76
    :cond_1
    invoke-direct {p0, p1, v8}, Lcom/google/android/gms/fitness/sensors/c/a;->a(Lcom/google/android/gms/fitness/data/DataPoint;Z)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v0

    .line 77
    if-nez v0, :cond_2

    .line 78
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 80
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sensors/c/a;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v1

    .line 83
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    .line 84
    const-string v2, "%s does not intersect with %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v7

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 86
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 90
    new-array v0, v8, [I

    invoke-static {p1}, Lcom/google/android/gms/fitness/sensors/c/a;->b(Lcom/google/android/gms/fitness/data/DataPoint;)I

    move-result v2

    aput v2, v0, v7

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a([I)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 110
    :goto_2
    new-array v0, v8, [Lcom/google/android/gms/fitness/data/DataPoint;

    aput-object v1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 92
    :cond_3
    invoke-static {p1}, Lcom/google/android/gms/fitness/sensors/c/a;->b(Lcom/google/android/gms/fitness/data/DataPoint;)I

    move-result v2

    invoke-static {v0}, Lcom/google/android/gms/fitness/sensors/c/a;->b(Lcom/google/android/gms/fitness/data/DataPoint;)I

    move-result v3

    sub-int v9, v2, v3

    .line 93
    if-gtz v9, :cond_4

    .line 97
    invoke-direct {p0, v0, v7}, Lcom/google/android/gms/fitness/sensors/c/a;->a(Lcom/google/android/gms/fitness/data/DataPoint;Z)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 98
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 100
    :cond_4
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    .line 101
    const-string v1, "prev is greater than current: %s\n%s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v7

    aput-object p1, v2, v8

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 102
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_1

    .line 104
    :cond_5
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 108
    new-array v0, v8, [I

    aput v9, v0, v7

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a([I)Lcom/google/android/gms/fitness/data/DataPoint;

    goto :goto_2
.end method

.method public final b()Lcom/google/android/gms/fitness/data/DataType;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/gms/fitness/sensors/c/a;->a:Lcom/google/android/gms/fitness/data/DataType;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/fitness/data/DataType;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/gms/fitness/sensors/c/a;->b:Lcom/google/android/gms/fitness/data/DataType;

    return-object v0
.end method
