.class public Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    .line 33
    return-void
.end method


# virtual methods
.method public requestLayout()V
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    if-nez v0, :cond_0

    .line 61
    invoke-super {p0}, Landroid/widget/ImageView;->requestLayout()V

    .line 63
    :cond_0
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 38
    iput-boolean v1, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    .line 40
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    iput-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    .line 43
    return-void

    :cond_0
    move v0, v2

    .line 37
    goto :goto_0

    .line 42
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    throw v0
.end method

.method public setImageResource(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 49
    iput-boolean v1, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    .line 51
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    iput-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    .line 54
    return-void

    :cond_0
    move v0, v2

    .line 48
    goto :goto_0

    .line 53
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeImageView;->a:Z

    throw v0
.end method
