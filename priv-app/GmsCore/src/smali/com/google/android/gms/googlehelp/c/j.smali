.class public final Lcom/google/android/gms/googlehelp/c/j;
.super Landroid/widget/TextView;
.source "SourceFile"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Lcom/google/android/gms/googlehelp/c/e;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/support/v4/app/q;Lcom/google/ad/a/a/h;)V
    .locals 5

    .prologue
    .line 60
    const/4 v0, 0x0

    const v1, 0x1010081

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/j;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 62
    sget v0, Lcom/google/android/gms/g;->aR:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/j;->setMinHeight(I)V

    .line 63
    sget v0, Lcom/google/android/gms/g;->aT:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget v2, Lcom/google/android/gms/g;->aV:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sget v3, Lcom/google/android/gms/g;->aU:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sget v4, Lcom/google/android/gms/g;->aS:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/google/android/gms/googlehelp/c/j;->setPadding(IIII)V

    .line 68
    iget-object v0, p2, Lcom/google/ad/a/a/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/j;->setTag(Ljava/lang/Object;)V

    .line 71
    iget-object v0, p2, Lcom/google/ad/a/a/h;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 81
    :goto_0
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/j;->setText(Ljava/lang/CharSequence;)V

    .line 82
    sget v0, Lcom/google/android/gms/k;->u:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/j;->setTextSize(F)V

    .line 83
    new-instance v0, Lcom/google/android/gms/googlehelp/c/k;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/googlehelp/c/k;-><init>(Lcom/google/android/gms/googlehelp/c/j;Landroid/support/v4/app/q;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/j;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    return-void

    .line 75
    :cond_0
    :try_start_0
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v0

    iget-object v2, p2, Lcom/google/ad/a/a/h;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final c()Ljava/util/List;
    .locals 4

    .prologue
    .line 95
    const/4 v0, 0x1

    new-array v1, v0, [Lcom/google/android/gms/googlehelp/e/n;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/j;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/j;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final onDateSet(Landroid/widget/DatePicker;III)V
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p2, p3, p4}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 109
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/j;->setText(Ljava/lang/CharSequence;)V

    .line 110
    return-void
.end method
