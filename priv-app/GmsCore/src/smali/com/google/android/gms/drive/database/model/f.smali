.class public final Lcom/google/android/gms/drive/database/model/f;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Lcom/google/android/gms/drive/auth/AppIdentity;

.field public d:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;JJLcom/google/android/gms/drive/auth/AppIdentity;J)V
    .locals 2

    .prologue
    .line 29
    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 30
    iput-wide p2, p0, Lcom/google/android/gms/drive/database/model/f;->a:J

    .line 31
    iput-wide p4, p0, Lcom/google/android/gms/drive/database/model/f;->b:J

    .line 32
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 33
    iput-wide p7, p0, Lcom/google/android/gms/drive/database/model/f;->d:J

    .line 34
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/f;
    .locals 9

    .prologue
    .line 93
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->a:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 94
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->b:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 95
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->d:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 96
    sget-object v1, Lcom/google/android/gms/drive/database/model/h;->e:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v6

    .line 98
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->c:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->c(Landroid/database/Cursor;)J

    move-result-wide v7

    .line 99
    new-instance v0, Lcom/google/android/gms/drive/database/model/f;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/database/model/f;-><init>(Lcom/google/android/gms/drive/database/i;JJLcom/google/android/gms/drive/auth/AppIdentity;J)V

    .line 101
    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/f;->d(J)V

    .line 103
    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->a:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/f;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 74
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->b:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/f;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 75
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->d:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->e:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->c:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/f;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 78
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 82
    const-string v0, "AuthenticatedApp [appId=%s, appIdentity=%s, scope=%s, expiryTimestamp=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/f;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/f;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
