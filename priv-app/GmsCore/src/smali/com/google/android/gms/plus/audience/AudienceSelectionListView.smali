.class public Lcom/google/android/gms/plus/audience/AudienceSelectionListView;
.super Landroid/widget/ListView;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/plus/audience/at;

.field private b:Lcom/google/android/gms/plus/audience/as;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/audience/as;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->b:Lcom/google/android/gms/plus/audience/as;

    .line 46
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/audience/at;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->a:Lcom/google/android/gms/plus/audience/at;

    .line 42
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->b:Lcom/google/android/gms/plus/audience/as;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->b:Lcom/google/android/gms/plus/audience/as;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/audience/as;->a(Lcom/google/android/gms/plus/audience/AudienceSelectionListView;)V

    .line 61
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onOverScrolled(IIZZ)V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onOverScrolled(IIZZ)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->a:Lcom/google/android/gms/plus/audience/at;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->a:Lcom/google/android/gms/plus/audience/at;

    invoke-interface {v0, p0, p4}, Lcom/google/android/gms/plus/audience/at;->a(Lcom/google/android/gms/plus/audience/AudienceSelectionListView;Z)V

    .line 54
    :cond_0
    return-void
.end method
