.class public final Lcom/google/android/gms/drive/metadata/sync/c/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;


# static fields
.field public static final a:Lcom/google/android/gms/drive/metadata/sync/c/a;

.field private static volatile b:Lcom/google/android/gms/drive/metadata/sync/c/g;


# instance fields
.field private final c:[Lcom/google/android/gms/drive/metadata/sync/c/a;

.field private final d:Ljava/util/Map;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/Set;

.field private final g:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

.field private final h:Lcom/google/android/gms/drive/g/ae;

.field private final i:I

.field private final j:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/c/h;

    invoke-direct {v0}, Lcom/google/android/gms/drive/metadata/sync/c/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/c/g;->a:Lcom/google/android/gms/drive/metadata/sync/c/a;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/util/Set;Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Lcom/google/android/gms/drive/g/ae;[Lcom/google/android/gms/drive/metadata/sync/c/a;)V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->d:Ljava/util/Map;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->j:Ljava/util/Map;

    .line 113
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->e:Landroid/content/Context;

    .line 114
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->f:Ljava/util/Set;

    .line 115
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->g:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    .line 116
    iput-object p4, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->h:Lcom/google/android/gms/drive/g/ae;

    .line 117
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->i:I

    .line 118
    iput-object p5, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->c:[Lcom/google/android/gms/drive/metadata/sync/c/a;

    .line 119
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/c/g;->c()V

    .line 120
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/metadata/sync/c/g;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/d;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/c/g;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/d;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcom/google/android/gms/drive/metadata/sync/c/g;
    .locals 2

    .prologue
    .line 208
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/c/g;->b:Lcom/google/android/gms/drive/metadata/sync/c/g;

    const-string v1, "Must be inited before get"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/c/g;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 11

    .prologue
    .line 137
    const-class v7, Lcom/google/android/gms/drive/metadata/sync/c/g;

    monitor-enter v7

    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/c/g;->b:Lcom/google/android/gms/drive/metadata/sync/c/g;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/gms/drive/api/a/k;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/android/gms/drive/api/a/m;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/android/gms/drive/api/a/n;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/android/gms/drive/api/a/o;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/android/gms/drive/api/a/p;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/android/gms/drive/api/a/s;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/android/gms/drive/api/a/v;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/android/gms/drive/api/a/x;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/android/gms/drive/api/a/y;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/android/gms/drive/api/a/ab;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/android/gms/drive/api/a/ad;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/android/gms/drive/api/a/af;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/android/gms/drive/api/a/aj;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/android/gms/drive/api/a/ak;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-class v3, Lcom/google/android/gms/drive/api/a/am;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-class v3, Lcom/google/android/gms/drive/api/a/an;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/android/gms/drive/api/a/aq;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v8

    const/4 v0, 0x2

    new-array v9, v0, [Lcom/google/android/gms/drive/metadata/sync/c/a;

    const/4 v10, 0x0

    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/c/i;

    const-string v1, "HIGH"

    sget-object v2, Lcom/google/android/gms/drive/ai;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Lcom/google/android/gms/drive/ai;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sget-object v5, Lcom/google/android/gms/drive/ai;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/metadata/sync/c/i;-><init>(Ljava/lang/String;JIJ)V

    aput-object v0, v9, v10

    const/4 v10, 0x1

    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/c/l;

    const-string v1, "LOW"

    sget-object v2, Lcom/google/android/gms/drive/ai;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Lcom/google/android/gms/drive/ai;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sget-object v5, Lcom/google/android/gms/drive/ai;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/metadata/sync/c/l;-><init>(Ljava/lang/String;JIJ)V

    aput-object v0, v9, v10

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/g/aw;->p()Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/c/g;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/g/aw;->m()Lcom/google/android/gms/drive/g/ae;

    move-result-object v4

    move-object v1, p0

    move-object v2, v8

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/metadata/sync/c/g;-><init>(Landroid/content/Context;Ljava/util/Set;Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Lcom/google/android/gms/drive/g/ae;[Lcom/google/android/gms/drive/metadata/sync/c/a;)V

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/u;)V

    const-string v1, "PushNotificationManager"

    const-string v2, "setupInstance finished initializing"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/c/g;->b:Lcom/google/android/gms/drive/metadata/sync/c/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :cond_0
    monitor-exit v7

    return-void

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method private declared-synchronized c(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/d;
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 215
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/c/d;

    .line 216
    if-nez v0, :cond_3

    .line 217
    const-string v0, "PushNotificationManager"

    const-string v1, "getNotificationController initializing for [%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->h:Lcom/google/android/gms/drive/g/ae;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/g/ae;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/c;

    move-result-object v8

    .line 219
    new-instance v6, Lcom/google/android/gms/drive/metadata/sync/c/k;

    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->i:I

    invoke-direct {v6, v0}, Lcom/google/android/gms/drive/metadata/sync/c/k;-><init>(I)V

    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/c/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->e:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->g:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->h:Lcom/google/android/gms/drive/g/ae;

    iget-object v5, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->c:[Lcom/google/android/gms/drive/metadata/sync/c/a;

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/metadata/sync/c/d;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Lcom/google/android/gms/drive/g/ae;[Lcom/google/android/gms/drive/metadata/sync/c/a;Lcom/google/android/gms/drive/metadata/sync/c/k;)V

    sget-object v1, Lcom/google/android/gms/drive/metadata/sync/c/g;->a:Lcom/google/android/gms/drive/metadata/sync/c/a;

    if-eqz v8, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->c:[Lcom/google/android/gms/drive/metadata/sync/c/a;

    array-length v5, v4

    move v3, v7

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v2, v4, v3

    invoke-interface {v2}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v8, Lcom/google/android/gms/drive/metadata/sync/c/c;->a:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    move-object v1, v2

    :cond_0
    iget-object v3, v8, Lcom/google/android/gms/drive/metadata/sync/c/c;->b:[J

    array-length v4, v3

    move v2, v7

    :goto_1
    if-ge v2, v4, :cond_2

    aget-wide v8, v3, v2

    invoke-virtual {v6, v8, v9}, Lcom/google/android/gms/drive/metadata/sync/c/k;->a(J)J

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/c/d;->a(Lcom/google/android/gms/drive/metadata/sync/c/a;)V

    .line 220
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->j:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    :cond_3
    monitor-exit p0

    return-object v0

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->d:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/drive/metadata/sync/c/g;->a:Lcom/google/android/gms/drive/metadata/sync/c/a;

    invoke-interface {v1}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/metadata/sync/c/g;->a:Lcom/google/android/gms/drive/metadata/sync/c/a;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->c:[Lcom/google/android/gms/drive/metadata/sync/c/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 126
    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->d:Ljava/util/Map;

    invoke-interface {v3}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Duplicate level name "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 260
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/c/g;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/d;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v2, v1, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    if-eqz v2, :cond_1

    :goto_0
    const-string v2, "Not initialized yet"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    iget-boolean v0, v1, Lcom/google/android/gms/drive/metadata/sync/c/d;->f:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/gms/drive/metadata/sync/c/d;->g:Z

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/c/d;->e()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/c/d;->d()V

    .line 261
    :cond_0
    return-void

    .line 260
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, v1, Lcom/google/android/gms/drive/metadata/sync/c/d;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 2

    .prologue
    .line 300
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/c/g;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/d;

    move-result-object v1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/SyncResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/c/d;->a(Z)V

    .line 302
    return-void

    .line 300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/service/b;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 250
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->f:Ljava/util/Set;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 251
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/c/g;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/d;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    if-eqz v3, :cond_1

    :goto_0
    const-string v1, "Not initialized yet"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/c/d;->g()J

    move-result-wide v0

    const-string v3, "PushNotificationController"

    const-string v4, "handleActivity[%s] at %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v2, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v3, v2, Lcom/google/android/gms/drive/metadata/sync/c/d;->c:Lcom/google/android/gms/drive/metadata/sync/c/k;

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/drive/metadata/sync/c/k;->a(J)J

    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/sync/c/d;->e()Z

    move-result v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/sync/c/d;->d()V

    .line 253
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 251
    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/events/a;)V
    .locals 1

    .prologue
    .line 271
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/c/j;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/metadata/sync/c/j;-><init>(Lcom/google/android/gms/drive/metadata/sync/c/g;Ljava/lang/String;)V

    iput-object v0, p2, Lcom/google/android/gms/drive/events/a;->b:Lcom/google/android/gms/drive/events/b;

    .line 281
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/g;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/c/d;

    .line 289
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/c/d;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 291
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 295
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/c/g;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/c/d;->c()V

    .line 296
    return-void
.end method
