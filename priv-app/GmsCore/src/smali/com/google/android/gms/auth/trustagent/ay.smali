.class public final Lcom/google/android/gms/auth/trustagent/ay;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/auth/trustagent/at;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/trustagent/at;)V
    .locals 1

    .prologue
    .line 238
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 239
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/at;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/ay;->a:Lcom/google/android/gms/auth/trustagent/at;

    .line 240
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 244
    const-string v0, "TrustStateTracker"

    const-string v1, "Received %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    const-string v0, "com.google.android.gms.auth.trustagent.TRUST_AGENT_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ay;->a:Lcom/google/android/gms/auth/trustagent/at;

    const-string v1, "is_enabled"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/trustagent/at;->a(Lcom/google/android/gms/auth/trustagent/at;Z)V

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 249
    :cond_1
    const-string v0, "com.google.android.gms.auth.trustagent.TRUST_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ay;->a:Lcom/google/android/gms/auth/trustagent/at;

    const-string v1, "is_trusted"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/trustagent/at;->b(Lcom/google/android/gms/auth/trustagent/at;Z)V

    goto :goto_0
.end method
