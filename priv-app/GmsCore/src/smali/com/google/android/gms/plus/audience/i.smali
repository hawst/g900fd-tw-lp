.class final Lcom/google/android/gms/plus/audience/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/audience/g;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/audience/g;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/i;->a:Lcom/google/android/gms/plus/audience/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/audience/g;B)V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/audience/i;-><init>(Lcom/google/android/gms/plus/audience/g;)V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 6

    .prologue
    .line 338
    new-instance v0, Landroid/support/v4/a/d;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/i;->a:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/a/d;-><init>(Landroid/content/Context;)V

    .line 339
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    iput-object v1, v0, Landroid/support/v4/a/d;->g:Landroid/net/Uri;

    .line 340
    sget-object v1, Lcom/google/android/gms/plus/audience/v;->b:[Ljava/lang/String;

    iput-object v1, v0, Landroid/support/v4/a/d;->h:[Ljava/lang/String;

    .line 341
    invoke-static {}, Lcom/google/android/gms/plus/audience/g;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/a/d;->i:Ljava/lang/String;

    .line 342
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/i;->a:Lcom/google/android/gms/plus/audience/g;

    invoke-static {v2}, Lcom/google/android/gms/plus/audience/g;->a(Lcom/google/android/gms/plus/audience/g;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 345
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "% "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/i;->a:Lcom/google/android/gms/plus/audience/g;

    invoke-static {v3}, Lcom/google/android/gms/plus/audience/g;->a(Lcom/google/android/gms/plus/audience/g;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 346
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/i;->a:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/audience/g;->k()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    const/4 v4, 0x2

    aput-object v1, v3, v4

    const/4 v1, 0x3

    aput-object v2, v3, v1

    iput-object v3, v0, Landroid/support/v4/a/d;->j:[Ljava/lang/String;

    .line 349
    return-object v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 362
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 335
    check-cast p2, Landroid/database/Cursor;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/i;->a:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/g;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/e;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/audience/e;->a(Landroid/database/Cursor;)V

    :cond_0
    return-void
.end method
