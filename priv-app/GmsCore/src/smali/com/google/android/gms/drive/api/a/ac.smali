.class final Lcom/google/android/gms/drive/api/a/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/auth/g;

.field final synthetic b:Lcom/google/android/gms/drive/api/a/ab;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/api/a/ab;Lcom/google/android/gms/drive/auth/g;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/gms/drive/api/a/ac;->b:Lcom/google/android/gms/drive/api/a/ab;

    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/ac;->a:Lcom/google/android/gms/drive/auth/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    packed-switch p1, :pswitch_data_0

    .line 116
    :goto_0
    return-void

    .line 102
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ac;->b:Lcom/google/android/gms/drive/api/a/ab;

    iget-object v3, v0, Lcom/google/android/gms/drive/api/a/ab;->c:Lcom/google/android/gms/drive/c/a;

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/c/a;->a(Z)Lcom/google/android/gms/drive/c/a;

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ac;->b:Lcom/google/android/gms/drive/api/a/ab;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/ac;->a:Lcom/google/android/gms/drive/auth/g;

    const/4 v4, 0x6

    if-ne p1, v4, :cond_1

    :goto_2
    invoke-static {v0, v3, v1}, Lcom/google/android/gms/drive/api/a/ab;->a(Lcom/google/android/gms/drive/api/a/ab;Lcom/google/android/gms/drive/auth/g;Z)V

    goto :goto_0

    :cond_0
    move v0, v2

    .line 102
    goto :goto_1

    :cond_1
    move v1, v2

    .line 103
    goto :goto_2

    .line 108
    :pswitch_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ac;->b:Lcom/google/android/gms/drive/api/a/ab;

    iget-object v0, v0, Lcom/google/android/gms/drive/api/a/ab;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    const-string v3, "Error downloading file"

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    const-string v1, "OpenContentsOperation"

    const-string v2, "Error downloading file."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(JJ)V
    .locals 3

    .prologue
    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ac;->b:Lcom/google/android/gms/drive/api/a/ab;

    iget-object v0, v0, Lcom/google/android/gms/drive/api/a/ab;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v1, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;-><init>(JJ)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    const-string v1, "OpenContentsOperation"

    const-string v2, "Error syncing file."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
