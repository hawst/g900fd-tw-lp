.class public final Lcom/google/android/gms/drive/ui/picker/a/a/f;
.super Lcom/google/android/gms/drive/ui/picker/a/a/g;
.source "SourceFile"


# static fields
.field private static b:[Ljava/lang/CharSequence;


# instance fields
.field private c:[Lcom/google/android/gms/drive/ui/picker/a/aa;

.field private final d:Ljava/util/Calendar;

.field private final e:Lcom/google/android/gms/drive/ui/picker/a/a/z;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/gms/drive/ui/picker/a/a/a;

.field private final h:Lcom/google/android/gms/drive/query/SortOrder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->b:[Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/ui/picker/a/a/a;Lcom/google/android/gms/drive/ui/picker/a/a/h;)V
    .locals 5

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/a/g;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->c:[Lcom/google/android/gms/drive/ui/picker/a/aa;

    .line 183
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 184
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->b()Lcom/google/android/gms/drive/g/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 185
    new-instance v1, Lcom/google/android/gms/drive/ui/picker/a/a/z;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/z;-><init>(Ljava/util/Calendar;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->e:Lcom/google/android/gms/drive/ui/picker/a/a/z;

    .line 186
    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->d:Ljava/util/Calendar;

    .line 187
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->f:Landroid/content/Context;

    .line 188
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->g:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    .line 190
    new-instance v0, Lcom/google/android/gms/drive/query/k;

    invoke-direct {v0}, Lcom/google/android/gms/drive/query/k;-><init>()V

    .line 191
    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/h;->b:Lcom/google/android/gms/drive/ui/picker/a/a/h;

    invoke-virtual {v1, p3}, Lcom/google/android/gms/drive/ui/picker/a/a/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    invoke-virtual {p2}, Lcom/google/android/gms/drive/ui/picker/a/a/a;->a()Lcom/google/android/gms/drive/metadata/k;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/drive/query/k;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/gms/drive/query/internal/FieldWithSortOrder;

    invoke-interface {v1}, Lcom/google/android/gms/drive/metadata/k;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lcom/google/android/gms/drive/query/internal/FieldWithSortOrder;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/query/k;->a()Lcom/google/android/gms/drive/query/SortOrder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->h:Lcom/google/android/gms/drive/query/SortOrder;

    .line 197
    return-void

    .line 194
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/drive/ui/picker/a/a/a;->a()Lcom/google/android/gms/drive/metadata/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/query/k;->a(Lcom/google/android/gms/drive/metadata/k;)Lcom/google/android/gms/drive/query/k;

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v2, 0x0

    .line 66
    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    move v1, v2

    .line 67
    :goto_0
    if-ge v1, v7, :cond_0

    .line 68
    add-int/lit8 v3, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    .line 69
    sget v4, Lcom/google/android/gms/n;->a:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {p1, v4, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 72
    invoke-static {p0, v3, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 73
    const/4 v3, -0x1

    invoke-virtual {v0, v7, v3}, Ljava/util/Calendar;->add(II)V

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 76
    :cond_0
    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x2

    .line 98
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 101
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 102
    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 104
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 105
    invoke-virtual {v1, v6, v7}, Ljava/util/Calendar;->add(II)V

    .line 108
    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    .line 109
    invoke-virtual {v1, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 111
    sget-object v4, Lcom/google/android/gms/drive/ui/picker/a/a/f;->b:[Ljava/lang/CharSequence;

    aget-object v3, v4, v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 112
    invoke-static {p0, v3, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 113
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 114
    invoke-virtual {v1, v6, v7}, Ljava/util/Calendar;->add(II)V

    .line 108
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 117
    :cond_1
    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/Calendar;Ljava/lang/String;)Ljava/util/Calendar;
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 123
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 126
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 127
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 129
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-nez v2, :cond_0

    .line 130
    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->add(II)V

    :cond_0
    move v2, v3

    .line 133
    :goto_0
    const/4 v4, 0x3

    if-ge v2, v4, :cond_1

    .line 134
    const-string v4, "yyyy"

    invoke-static {v4, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 135
    new-array v5, v8, [Ljava/lang/Object;

    aput-object v4, v5, v3

    invoke-static {p2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 136
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 137
    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->add(II)V

    .line 133
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 140
    :cond_1
    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V
    .locals 3

    .prologue
    .line 60
    new-instance v2, Lcom/google/android/gms/drive/ui/picker/a/aa;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {v2, p1, v0}, Lcom/google/android/gms/drive/ui/picker/a/aa;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    return-void

    .line 60
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method private static b(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 81
    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    move v1, v2

    .line 82
    :goto_0
    if-gtz v1, :cond_0

    .line 83
    sget v3, Lcom/google/android/gms/n;->b:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p1, v3, v6, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 87
    invoke-static {p0, v3, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 88
    const/4 v3, 0x5

    const/4 v4, -0x7

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 91
    :cond_0
    return-object v0
.end method

.method private declared-synchronized f()[Lcom/google/android/gms/drive/ui/picker/a/aa;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 37
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/f;->b:[Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    .line 38
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/c;->h:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 41
    sput-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/f;->b:[Ljava/lang/CharSequence;

    array-length v1, v1

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->d:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v2

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Insufficient number of months in the resources"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->c:[Lcom/google/android/gms/drive/ui/picker/a/aa;

    if-nez v0, :cond_2

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->d:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->gv:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    const/16 v3, 0xb

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xc

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xd

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    sget v3, Lcom/google/android/gms/p;->gw:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    const/4 v3, 0x5

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->b(Ljava/util/ArrayList;Landroid/content/res/Resources;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->gu:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Ljava/util/Calendar;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->gt:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/util/Calendar;)V

    .line 52
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/drive/ui/picker/a/aa;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->c:[Lcom/google/android/gms/drive/ui/picker/a/aa;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->c:[Lcom/google/android/gms/drive/ui/picker/a/aa;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 55
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->c:[Lcom/google/android/gms/drive/ui/picker/a/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/SortOrder;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->h:Lcom/google/android/gms/drive/query/SortOrder;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/ui/picker/a/a/r;
    .locals 12

    .prologue
    const/4 v7, 0x0

    const-wide/16 v2, -0x1

    const/4 v6, 0x1

    .line 206
    .line 207
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->g:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/a;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 209
    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v4}, Lcom/google/android/gms/drive/ui/picker/a/l;->l()Z

    .line 215
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v4}, Lcom/google/android/gms/drive/ui/picker/a/l;->l()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 216
    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->g:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    iget-object v5, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/ui/picker/a/a/a;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 217
    iget-object v8, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v8}, Lcom/google/android/gms/drive/ui/picker/a/l;->k()Z

    .line 222
    :goto_1
    iget-object v8, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->e:Lcom/google/android/gms/drive/ui/picker/a/a/z;

    iget-object v9, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->g:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    iget-object v10, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-virtual {v9, v10}, Lcom/google/android/gms/drive/ui/picker/a/a/a;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/google/android/gms/drive/ui/picker/a/a/z;->a(J)Lcom/google/android/gms/drive/ui/picker/a/a/y;

    move-result-object v9

    cmp-long v10, v0, v2

    if-nez v10, :cond_2

    move v1, v6

    :goto_2
    cmp-long v0, v4, v2

    if-nez v0, :cond_4

    move v0, v6

    :goto_3
    invoke-virtual {v9, v1, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/y;->a(ZZ)Lcom/google/android/gms/drive/ui/picker/a/a/r;

    move-result-object v0

    return-object v0

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0, v7}, Lcom/google/android/gms/drive/ui/picker/a/l;->a(I)Z

    move-wide v0, v2

    goto :goto_0

    .line 219
    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v4}, Lcom/google/android/gms/drive/ui/picker/a/l;->m()V

    move-wide v4, v2

    goto :goto_1

    .line 222
    :cond_2
    invoke-virtual {v8, v0, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/z;->a(J)Lcom/google/android/gms/drive/ui/picker/a/a/y;

    move-result-object v0

    if-eq v9, v0, :cond_3

    move v1, v6

    goto :goto_2

    :cond_3
    move v1, v7

    goto :goto_2

    :cond_4
    invoke-virtual {v8, v4, v5}, Lcom/google/android/gms/drive/ui/picker/a/a/z;->a(J)Lcom/google/android/gms/drive/ui/picker/a/a/y;

    move-result-object v0

    if-eq v9, v0, :cond_5

    move v0, v6

    goto :goto_3

    :cond_5
    move v0, v7

    goto :goto_3
.end method

.method public final c()Landroid/widget/SectionIndexer;
    .locals 4

    .prologue
    .line 228
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/y;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->g:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/a/f;->f()[Lcom/google/android/gms/drive/ui/picker/a/aa;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/ui/picker/a/y;-><init>(Lcom/google/android/gms/drive/ui/picker/a/l;Lcom/google/android/gms/drive/ui/picker/a/a/a;[Lcom/google/android/gms/drive/ui/picker/a/aa;)V

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/drive/ui/picker/a/a/a;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/f;->g:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    return-object v0
.end method
