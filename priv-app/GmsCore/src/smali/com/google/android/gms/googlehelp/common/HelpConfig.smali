.class public Lcom/google/android/gms/googlehelp/common/HelpConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final a:Ljava/util/Set;


# instance fields
.field A:Lcom/google/android/gms/feedback/ThemeSettings;

.field B:Ljava/util/Map;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Landroid/accounts/Account;

.field f:Ljava/lang/String;

.field g:Landroid/os/Bundle;

.field h:Z

.field i:Z

.field j:Ljava/util/List;

.field k:Lcom/google/ad/a/a/u;

.field l:I

.field m:I

.field n:Landroid/os/Bundle;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field o:Landroid/graphics/Bitmap;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field p:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field q:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field r:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field s:Ljava/lang/String;

.field t:Landroid/net/Uri;

.field u:Ljava/util/List;

.field v:Ljava/util/List;

.field w:Z

.field x:Z

.field y:Ljava/lang/String;

.field z:Lcom/google/android/gms/feedback/ErrorReport;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/googlehelp/common/j;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 70
    new-instance v0, Ljava/util/LinkedHashSet;

    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lcom/google/k/h/a;->a([I)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a:Ljava/util/Set;

    return-void

    nop

    :array_0
    .array-data 4
        0x4
        0x1
        0x2
        0x3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 27

    .prologue
    .line 350
    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    new-instance v25, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct/range {v25 .. v25}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    new-instance v26, Lcom/google/android/gms/feedback/ThemeSettings;

    invoke-direct/range {v26 .. v26}, Lcom/google/android/gms/feedback/ThemeSettings;-><init>()V

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v26}, Lcom/google/android/gms/googlehelp/common/HelpConfig;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZZLjava/util/List;Landroid/os/Bundle;IILandroid/os/Bundle;Landroid/graphics/Bitmap;[BIILjava/lang/String;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;ZZLjava/lang/String;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/ThemeSettings;)V

    .line 377
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZZLjava/util/List;Landroid/os/Bundle;IILandroid/os/Bundle;Landroid/graphics/Bitmap;[BIILjava/lang/String;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;ZZLjava/lang/String;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/ThemeSettings;)V
    .locals 2

    .prologue
    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    iput p1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->b:I

    .line 306
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c:Ljava/lang/String;

    .line 307
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->d:Ljava/lang/String;

    .line 308
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->e:Landroid/accounts/Account;

    .line 309
    iput-object p5, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->f:Ljava/lang/String;

    .line 310
    iput-object p6, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g:Landroid/os/Bundle;

    .line 313
    iput-boolean p7, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->h:Z

    .line 314
    iput-boolean p8, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->i:Z

    .line 317
    iput-object p9, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->j:Ljava/util/List;

    .line 318
    invoke-static {p10}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Landroid/os/Bundle;)Lcom/google/ad/a/a/u;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    .line 321
    iput-object p13, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->n:Landroid/os/Bundle;

    .line 323
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->o:Landroid/graphics/Bitmap;

    .line 324
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->p:[B

    .line 325
    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->q:I

    .line 326
    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->r:I

    .line 329
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->s:Ljava/lang/String;

    .line 332
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->t:Landroid/net/Uri;

    .line 333
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->u:Ljava/util/List;

    .line 334
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->A:Lcom/google/android/gms/feedback/ThemeSettings;

    .line 335
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->v:Ljava/util/List;

    .line 336
    move/from16 v0, p22

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->w:Z

    .line 337
    move/from16 v0, p23

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->x:Z

    .line 340
    iput p11, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->l:I

    .line 341
    iput p12, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->m:I

    .line 342
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->y:Ljava/lang/String;

    .line 344
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->B:Ljava/util/Map;

    .line 345
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->z:Lcom/google/android/gms/feedback/ErrorReport;

    .line 346
    return-void
.end method

.method private T()Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 2

    .prologue
    .line 394
    new-instance v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;-><init>()V

    .line 395
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c:Ljava/lang/String;

    .line 396
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->d:Ljava/lang/String;

    .line 397
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->e:Landroid/accounts/Account;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->e:Landroid/accounts/Account;

    .line 398
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->f:Ljava/lang/String;

    .line 399
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g:Landroid/os/Bundle;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g:Landroid/os/Bundle;

    .line 400
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->A:Lcom/google/android/gms/feedback/ThemeSettings;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->A:Lcom/google/android/gms/feedback/ThemeSettings;

    .line 401
    return-object v0
.end method

.method private U()Z
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->j:Lcom/google/ad/a/a/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->j:Lcom/google/ad/a/a/v;

    iget-object v0, v0, Lcom/google/ad/a/a/v;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->j:Lcom/google/ad/a/a/v;

    iget-object v0, v0, Lcom/google/ad/a/a/v;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/os/Bundle;)Lcom/google/ad/a/a/u;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 588
    if-eqz p0, :cond_0

    const-string v0, "EXTRA_ESCALATION_OPTIONS"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 596
    :goto_0
    return-object v0

    .line 593
    :cond_1
    :try_start_0
    const-string v0, "EXTRA_ESCALATION_OPTIONS"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v2, Lcom/google/ad/a/a/u;

    invoke-direct {v2}, Lcom/google/ad/a/a/u;-><init>()V

    invoke-static {v0, v2}, Lcom/google/android/gms/googlehelp/common/s;->a([BLcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ad/a/a/u;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 596
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/googlehelp/GoogleHelp;)Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 27

    .prologue
    .line 405
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 407
    new-instance v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->c()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->d()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->e()Z

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->f()Z

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->g()Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->h()Landroid/os/Bundle;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->j()Landroid/graphics/Bitmap;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->i()[B

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->k()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->l()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->m()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->n()Landroid/net/Uri;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->o()Ljava/util/List;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->q()Ljava/util/List;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->r()Z

    move-result v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->s()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->p()Lcom/google/android/gms/feedback/ThemeSettings;

    move-result-object v26

    invoke-direct/range {v0 .. v26}, Lcom/google/android/gms/googlehelp/common/HelpConfig;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZZLjava/util/List;Landroid/os/Bundle;IILandroid/os/Bundle;Landroid/graphics/Bitmap;[BIILjava/lang/String;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;ZZLjava/lang/String;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/ThemeSettings;)V

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/util/Set;
    .locals 2

    .prologue
    .line 730
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->U()Z

    move-result v0

    if-nez v0, :cond_0

    .line 731
    sget-object v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a:Ljava/util/Set;

    .line 737
    :goto_0
    return-object v0

    .line 734
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    sget-object v1, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 735
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v1, v1, Lcom/google/ad/a/a/u;->j:Lcom/google/ad/a/a/v;

    iget-object v1, v1, Lcom/google/ad/a/a/v;->a:[I

    invoke-static {v1}, Lcom/google/k/h/a;->a([I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 736
    sget-object v1, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 742
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->j:Lcom/google/ad/a/a/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->j:Lcom/google/ad/a/a/v;

    iget-object v0, v0, Lcom/google/ad/a/a/v;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->n:Landroid/os/Bundle;

    return-object v0
.end method

.method public final D()Lcom/google/android/gms/feedback/Screenshot;
    .locals 3

    .prologue
    .line 758
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->p:[B

    if-eqz v0, :cond_0

    .line 759
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->p:[B

    iget v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->q:I

    iget v2, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->r:I

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/feedback/Screenshot;->a([BII)Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v0

    .line 766
    :goto_0
    return-object v0

    .line 762
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->o:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 763
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->o:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/google/android/gms/feedback/Screenshot;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v0

    goto :goto_0

    .line 766
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final E()Z
    .locals 1

    .prologue
    .line 769
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final F()Ljava/lang/String;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 2

    .prologue
    .line 775
    sget-object v0, Lcom/google/android/gms/googlehelp/common/d;->b:Lcom/google/android/gms/googlehelp/common/d;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Lcom/google/android/gms/googlehelp/common/d;)Ljava/lang/String;

    move-result-object v0

    .line 776
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 781
    :goto_0
    return-object v0

    .line 778
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->t:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 779
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->t:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 781
    :cond_1
    sget-object v0, Lcom/google/android/gms/googlehelp/common/d;->b:Lcom/google/android/gms/googlehelp/common/d;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/d;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final H()Ljava/util/List;
    .locals 1

    .prologue
    .line 786
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->u:Ljava/util/List;

    return-object v0
.end method

.method public final I()Lcom/google/android/gms/feedback/ThemeSettings;
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->A:Lcom/google/android/gms/feedback/ThemeSettings;

    return-object v0
.end method

.method public final J()I
    .locals 1

    .prologue
    .line 794
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->A:Lcom/google/android/gms/feedback/ThemeSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/ThemeSettings;->a()I

    move-result v0

    return v0
.end method

.method public final K()I
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->A:Lcom/google/android/gms/feedback/ThemeSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/ThemeSettings;->b()I

    move-result v0

    return v0
.end method

.method public final L()Z
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->A:Lcom/google/android/gms/feedback/ThemeSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/ThemeSettings;->b()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final M()Z
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->v:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final N()Ljava/util/List;
    .locals 3

    .prologue
    .line 810
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->v:Ljava/util/List;

    if-nez v0, :cond_0

    .line 811
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 819
    :goto_0
    return-object v0

    .line 814
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 815
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;

    .line 816
    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/k;->a(Lcom/google/android/gms/googlehelp/OfflineSuggestion;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 819
    goto :goto_0
.end method

.method public final O()Z
    .locals 2

    .prologue
    .line 823
    sget-object v0, Lcom/google/android/gms/googlehelp/common/d;->c:Lcom/google/android/gms/googlehelp/common/d;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Lcom/google/android/gms/googlehelp/common/d;)Ljava/lang/String;

    move-result-object v0

    .line 824
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 825
    const-string v1, "top"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 827
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->w:Z

    goto :goto_0
.end method

.method public final P()Z
    .locals 1

    .prologue
    .line 831
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->x:Z

    return v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final R()Ljava/util/Map;
    .locals 1

    .prologue
    .line 840
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->B:Ljava/util/Map;

    return-object v0
.end method

.method public final S()Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->z:Lcom/google/android/gms/feedback/ErrorReport;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 3

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->T()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    .line 381
    new-instance v1, Lcom/google/ad/a/a/u;

    invoke-direct {v1}, Lcom/google/ad/a/a/u;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    .line 382
    iget-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v2, v2, Lcom/google/ad/a/a/u;->d:Lcom/google/ad/a/a/r;

    iput-object v2, v1, Lcom/google/ad/a/a/u;->d:Lcom/google/ad/a/a/r;

    .line 383
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/ad/a/a/u;)Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 448
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    .line 449
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->c:Lcom/google/ad/a/a/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->c:Lcom/google/ad/a/a/q;

    iget-object v0, v0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->l:I

    .line 450
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->e:Lcom/google/ad/a/a/w;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->e:Lcom/google/ad/a/a/w;

    iget-object v0, v0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    if-nez v0, :cond_5

    :goto_3
    iput v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->m:I

    .line 451
    return-object p0

    :cond_0
    move v0, v1

    .line 449
    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/google/android/gms/common/util/l;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/common/util/l;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v3

    goto :goto_1

    :cond_3
    move v0, v4

    goto :goto_1

    :cond_4
    move v0, v1

    .line 450
    goto :goto_2

    :cond_5
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p1}, Lcom/google/android/gms/common/util/l;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    move v1, v2

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {p1}, Lcom/google/android/gms/common/util/l;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    move v1, v3

    goto :goto_3

    :cond_7
    move v1, v4

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c:Ljava/lang/String;

    .line 438
    return-object p0
.end method

.method public final a(Ljava/util/Map;)Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->B:Ljava/util/Map;

    .line 515
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 0

    .prologue
    .line 461
    iput-boolean p1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->x:Z

    .line 462
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->B:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 846
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/d;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 661
    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/common/util/ak;->d(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->t()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final b()Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 3

    .prologue
    .line 387
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->T()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    .line 388
    new-instance v1, Lcom/google/ad/a/a/u;

    invoke-direct {v1}, Lcom/google/ad/a/a/u;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    .line 389
    iget-object v1, v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v2, v2, Lcom/google/ad/a/a/u;->a:Lcom/google/ad/a/a/s;

    iput-object v2, v1, Lcom/google/ad/a/a/u;->a:Lcom/google/ad/a/a/s;

    .line 390
    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->f:Ljava/lang/String;

    .line 457
    return-object p0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->c:Lcom/google/ad/a/a/q;

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->c:Lcom/google/ad/a/a/q;

    iput-object p1, v0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    .line 469
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->e:Lcom/google/ad/a/a/w;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->e:Lcom/google/ad/a/a/w;

    iput-object p1, v0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    .line 475
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 854
    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 0

    .prologue
    .line 519
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->y:Ljava/lang/String;

    .line 520
    return-object p0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->e:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->e:Landroid/accounts/Account;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/util/List;
    .locals 4

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 552
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 561
    :goto_0
    return-object v0

    .line 555
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 556
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 557
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g:Landroid/os/Bundle;

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 561
    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 565
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k()I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 569
    sget-object v0, Lcom/google/android/gms/googlehelp/common/d;->a:Lcom/google/android/gms/googlehelp/common/d;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Lcom/google/android/gms/googlehelp/common/d;)Ljava/lang/String;

    move-result-object v0

    .line 571
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 573
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 578
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->h:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 582
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->i:Z

    return v0
.end method

.method public final n()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 602
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz v1, :cond_0

    const-string v2, "EXTRA_ESCALATION_OPTIONS"

    invoke-static {v1}, Lcom/google/ad/a/a/u;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 621
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->d:Lcom/google/ad/a/a/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->d:Lcom/google/ad/a/a/r;

    iget-boolean v0, v0, Lcom/google/ad/a/a/r;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 627
    iget v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->l:I

    return v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->c:Lcom/google/ad/a/a/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->c:Lcom/google/ad/a/a/q;

    iget-object v0, v0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 644
    iget v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->m:I

    return v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->e:Lcom/google/ad/a/a/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->e:Lcom/google/ad/a/a/w;

    iget-object v0, v0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Ljava/util/List;
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->b:Lcom/google/ad/a/a/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->b:Lcom/google/ad/a/a/x;

    iget-object v0, v0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->b:Lcom/google/ad/a/a/x;

    iget-object v0, v0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 672
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->j:Ljava/util/List;

    goto :goto_0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->a:Lcom/google/ad/a/a/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->a:Lcom/google/ad/a/a/s;

    iget-object v0, v0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Lcom/google/ad/a/a/g;
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->a:Lcom/google/ad/a/a/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->a:Lcom/google/ad/a/a/s;

    iget-object v0, v0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->k:Lcom/google/ad/a/a/n;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 858
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/googlehelp/common/j;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Landroid/os/Parcel;I)V

    .line 859
    return-void
.end method

.method public final x()Lcom/google/ad/a/a/n;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->k:Lcom/google/ad/a/a/n;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Ljava/util/Map;
    .locals 6

    .prologue
    .line 701
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 703
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v0, v0, Lcom/google/ad/a/a/u;->l:[Lcom/google/ad/a/a/p;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v2, v0, Lcom/google/ad/a/a/u;->l:[Lcom/google/ad/a/a/p;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 705
    iget v5, v4, Lcom/google/ad/a/a/p;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 704
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 709
    :cond_0
    return-object v1
.end method

.method public final z()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 720
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->U()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v1, v1, Lcom/google/ad/a/a/u;->j:Lcom/google/ad/a/a/v;

    iget-object v1, v1, Lcom/google/ad/a/a/v;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 726
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/HelpConfig;->k:Lcom/google/ad/a/a/u;

    iget-object v1, v1, Lcom/google/ad/a/a/u;->j:Lcom/google/ad/a/a/v;

    iget-object v1, v1, Lcom/google/ad/a/a/v;->a:[I

    aget v0, v1, v0

    goto :goto_0
.end method
