.class public final Lcom/google/android/gms/drive/database/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/drive/database/c/c;

.field private final d:Ljava/lang/String;

.field private e:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/database/c/c;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/c/b;->e:Z

    .line 29
    iput-object v1, p0, Lcom/google/android/gms/drive/database/c/b;->a:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/google/android/gms/drive/database/c/b;->b:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/drive/database/c/b;->c:Lcom/google/android/gms/drive/database/c/c;

    .line 34
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/c/b;->d:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/b;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/drive/database/c/b;

    sget-object v1, Lcom/google/android/gms/drive/database/c/c;->b:Lcom/google/android/gms/drive/database/c/c;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/drive/database/c/b;-><init>(Lcom/google/android/gms/drive/database/c/c;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/b;
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/drive/database/c/b;

    sget-object v1, Lcom/google/android/gms/drive/database/c/c;->c:Lcom/google/android/gms/drive/database/c/c;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/drive/database/c/b;-><init>(Lcom/google/android/gms/drive/database/c/c;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/c/b;->e:Z

    if-eqz v0, :cond_2

    const-string v0, "NATURAL "

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/c/b;->c:Lcom/google/android/gms/drive/database/c/c;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/c/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/c/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/drive/database/c/b;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 100
    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/database/c/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/database/c/b;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 103
    const-string v1, " ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/database/c/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 94
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method
