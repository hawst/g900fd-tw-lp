.class public final Lcom/google/android/gms/ads/internal/f/a;
.super Lcom/google/android/gms/ads/internal/f/a/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/f/d;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:Ljava/lang/String;

.field private final d:Landroid/graphics/drawable/Drawable;

.field private final e:Ljava/lang/String;

.field private final f:D

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/Object;

.field private j:Lcom/google/android/gms/ads/internal/f/c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;DLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/f/a/b;-><init>()V

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->i:Ljava/lang/Object;

    .line 84
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/f/a;->a:Ljava/lang/String;

    .line 85
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/f/a;->b:Landroid/graphics/drawable/Drawable;

    .line 86
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/f/a;->c:Ljava/lang/String;

    .line 87
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/f/a;->d:Landroid/graphics/drawable/Drawable;

    .line 88
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/f/a;->e:Ljava/lang/String;

    .line 89
    iput-wide p6, p0, Lcom/google/android/gms/ads/internal/f/a;->f:D

    .line 90
    iput-object p8, p0, Lcom/google/android/gms/ads/internal/f/a;->g:Ljava/lang/String;

    .line 91
    iput-object p9, p0, Lcom/google/android/gms/ads/internal/f/a;->h:Ljava/lang/String;

    .line 92
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 136
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/f/a;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 137
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->j:Lcom/google/android/gms/ads/internal/f/c;

    if-nez v0, :cond_0

    .line 138
    const-string v0, "Attempt to perform click before app install ad initialized."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    .line 139
    monitor-exit v1

    .line 142
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->j:Lcom/google/android/gms/ads/internal/f/c;

    const-string v2, "2"

    invoke-virtual {v0, v2, p1}, Lcom/google/android/gms/ads/internal/f/c;->a(Ljava/lang/String;I)V

    .line 142
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/f/c;)V
    .locals 2

    .prologue
    .line 158
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/f/a;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 159
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/f/a;->j:Lcom/google/android/gms/ads/internal/f/c;

    .line 160
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Lcom/google/android/gms/b/l;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/b/l;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->d:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()D
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lcom/google/android/gms/ads/internal/f/a;->f:D

    return-wide v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 147
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/f/a;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 148
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->j:Lcom/google/android/gms/ads/internal/f/c;

    if-nez v0, :cond_0

    .line 149
    const-string v0, "Attempt to record impression before app install ad initialized."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    .line 150
    monitor-exit v1

    .line 153
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/a;->j:Lcom/google/android/gms/ads/internal/f/c;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/f/c;->a:Lcom/google/android/gms/ads/internal/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b;->n()V

    .line 153
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
