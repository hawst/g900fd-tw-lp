.class public final Lcom/google/android/gms/feedback/ab;
.super Landroid/os/Binder;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/feedback/LegacyBugReportService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/feedback/LegacyBugReportService;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/gms/feedback/ab;->a:Lcom/google/android/gms/feedback/LegacyBugReportService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method protected final onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 48
    iget-object v2, p0, Lcom/google/android/gms/feedback/ab;->a:Lcom/google/android/gms/feedback/LegacyBugReportService;

    invoke-virtual {v2}, Lcom/google/android/gms/feedback/LegacyBugReportService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 50
    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 51
    array-length v3, v1

    if-nez v3, :cond_0

    .line 61
    :goto_0
    return v0

    .line 54
    :cond_0
    new-instance v3, Landroid/app/ApplicationErrorReport;

    invoke-direct {v3}, Landroid/app/ApplicationErrorReport;-><init>()V

    .line 55
    aget-object v0, v1, v0

    iput-object v0, v3, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    .line 56
    const/16 v0, 0xb

    iput v0, v3, Landroid/app/ApplicationErrorReport;->type:I

    .line 57
    iget-object v0, v3, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Landroid/app/ApplicationErrorReport;->installerPackageName:Ljava/lang/String;

    .line 58
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/feedback/ab;->a:Lcom/google/android/gms/feedback/LegacyBugReportService;

    const-class v2, Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.extra.BUG_REPORT"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/os/Parcel;->dataSize()I

    move-result v0

    if-lez v0, :cond_1

    sget-object v0, Landroid/graphics/Bitmap;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, Lcom/google/android/gms/feedback/Screenshot;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v0

    const-string v2, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/ab;->a:Lcom/google/android/gms/feedback/LegacyBugReportService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/LegacyBugReportService;->startActivity(Landroid/content/Intent;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/feedback/ab;->a:Lcom/google/android/gms/feedback/LegacyBugReportService;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/LegacyBugReportService;->stopSelf()V

    .line 61
    const/4 v0, 0x1

    goto :goto_0
.end method
