.class public final Lcom/google/android/gms/auth/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "com.google.android.gms.auth.change.REFRESH"

    sput-object v0, Lcom/google/android/gms/auth/k;->a:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/google/android/gms/auth/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 23
    invoke-static {p0}, Lcom/google/android/gms/auth/k;->b(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v1

    .line 34
    sget-object v0, Lcom/google/android/gms/auth/b/a;->ab:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 35
    const-wide/16 v4, 0x1

    sub-long v4, v2, v4

    .line 36
    const-string v0, "auth_droidguard_recurring_run"

    new-instance v6, Lcom/google/android/gms/gcm/aq;

    invoke-direct {v6}, Lcom/google/android/gms/gcm/aq;-><init>()V

    const-class v7, Lcom/google/android/gms/auth/be/cron/AuthCronService;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/gcm/aq;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/aq;

    move-result-object v6

    iput-wide v2, v6, Lcom/google/android/gms/gcm/aq;->a:J

    invoke-virtual {v6, v0}, Lcom/google/android/gms/gcm/aq;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/aq;

    move-result-object v0

    iput-wide v4, v0, Lcom/google/android/gms/gcm/aq;->b:J

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/aq;->b()Lcom/google/android/gms/gcm/PeriodicTask;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    .line 38
    return-void
.end method
