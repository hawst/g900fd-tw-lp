.class final Lcom/google/android/gms/fitness/b/c/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/b/o;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/b/c/o;

.field private final b:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/b/c/o;Z)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/q;->a:Lcom/google/android/gms/fitness/b/c/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-boolean p2, p0, Lcom/google/android/gms/fitness/b/c/q;->b:Z

    .line 167
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/b/c/o;ZB)V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/b/c/q;-><init>(Lcom/google/android/gms/fitness/b/c/o;Z)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/b/e;)Z
    .locals 2

    .prologue
    .line 171
    sget-object v0, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->e()Lcom/google/android/gms/fitness/b/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/b/h;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/q;->a:Lcom/google/android/gms/fitness/b/c/o;

    iget-object v0, v0, Lcom/google/android/gms/fitness/b/c/o;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->a()Lcom/google/android/gms/fitness/b/b;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms"

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->a()Lcom/google/android/gms/fitness/b/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/fitness/b/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/q;->a:Lcom/google/android/gms/fitness/b/c/o;

    iget-object v0, v0, Lcom/google/android/gms/fitness/b/c/o;->a:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/fitness/b/c/q;->b:Z

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/b/c/v;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
