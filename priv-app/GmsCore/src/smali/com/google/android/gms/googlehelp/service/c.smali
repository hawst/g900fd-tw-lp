.class final Lcom/google/android/gms/googlehelp/service/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;


# instance fields
.field final synthetic a:Lcom/google/android/gms/googlehelp/common/o;

.field final synthetic b:Ljava/util/concurrent/CountDownLatch;

.field final synthetic c:Lcom/google/android/gms/googlehelp/service/MetricsReportService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/googlehelp/service/MetricsReportService;Lcom/google/android/gms/googlehelp/common/o;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/service/c;->c:Lcom/google/android/gms/googlehelp/service/MetricsReportService;

    iput-object p2, p0, Lcom/google/android/gms/googlehelp/service/c;->a:Lcom/google/android/gms/googlehelp/common/o;

    iput-object p3, p0, Lcom/google/android/gms/googlehelp/service/c;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/ac;)V
    .locals 5

    .prologue
    .line 65
    const-string v0, "GOOGLEHELP_MetricsReportService"

    const-string v1, "Metrics reporting failed: %s %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/service/c;->a:Lcom/google/android/gms/googlehelp/common/o;

    invoke-virtual {v4}, Lcom/google/android/gms/googlehelp/common/o;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/service/c;->a:Lcom/google/android/gms/googlehelp/common/o;

    invoke-virtual {v4}, Lcom/google/android/gms/googlehelp/common/o;->i()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/c;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 69
    return-void
.end method
