.class public final Lcom/google/android/gms/drive/events/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/HashMap;

.field public volatile b:Lcom/google/android/gms/drive/events/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/drive/events/a;->b:Lcom/google/android/gms/drive/events/b;

    .line 187
    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/events/b;->a(I)V

    .line 190
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/events/DriveEvent;Lcom/google/android/gms/drive/DriveId;)Z
    .locals 8

    .prologue
    .line 147
    const/4 v2, 0x0

    .line 148
    iget-object v3, p0, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    monitor-enter v3

    .line 149
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 150
    if-eqz v0, :cond_2

    .line 151
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 152
    new-instance v5, Lcom/google/android/gms/drive/internal/OnEventResponse;

    invoke-direct {v5, p1}, Lcom/google/android/gms/drive/internal/OnEventResponse;-><init>(Lcom/google/android/gms/drive/events/DriveEvent;)V

    .line 153
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/events/c;

    .line 155
    invoke-interface {p1}, Lcom/google/android/gms/drive/events/DriveEvent;->a()I

    move-result v6

    iget v7, v1, Lcom/google/android/gms/drive/events/c;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v6, v7, :cond_0

    .line 157
    :try_start_1
    iget-object v1, v1, Lcom/google/android/gms/drive/events/c;->b:Lcom/google/android/gms/drive/internal/cd;

    invoke-interface {v1, v5}, Lcom/google/android/gms/drive/internal/cd;->a(Lcom/google/android/gms/drive/internal/OnEventResponse;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 158
    :catch_0
    move-exception v1

    .line 160
    :try_start_2
    const-string v6, "CallbackStore"

    const-string v7, "Event callback error"

    invoke-static {v6, v1, v7}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 161
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 165
    :cond_1
    :try_start_3
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    const/4 v0, 0x1

    .line 170
    :goto_1
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 171
    return v0

    :cond_2
    move v0, v2

    goto :goto_1
.end method
