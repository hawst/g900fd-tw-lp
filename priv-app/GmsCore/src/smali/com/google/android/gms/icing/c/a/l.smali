.class public final Lcom/google/android/gms/icing/c/a/l;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:[Lcom/google/android/gms/icing/c/a/m;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2355
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2356
    iput-boolean v1, p0, Lcom/google/android/gms/icing/c/a/l;->a:Z

    invoke-static {}, Lcom/google/android/gms/icing/c/a/m;->a()[Lcom/google/android/gms/icing/c/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    iput v1, p0, Lcom/google/android/gms/icing/c/a/l;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/l;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/l;->cachedSize:I

    .line 2357
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 2423
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2424
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/l;->a:Z

    if-eqz v1, :cond_0

    .line 2425
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/icing/c/a/l;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2428
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 2429
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2430
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    aget-object v2, v2, v0

    .line 2431
    if-eqz v2, :cond_1

    .line 2432
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2429
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2437
    :cond_3
    iget v1, p0, Lcom/google/android/gms/icing/c/a/l;->c:I

    if-eqz v1, :cond_4

    .line 2438
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/c/a/l;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2441
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2370
    if-ne p1, p0, :cond_1

    .line 2371
    const/4 v0, 0x1

    .line 2387
    :cond_0
    :goto_0
    return v0

    .line 2373
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/l;

    if-eqz v1, :cond_0

    .line 2376
    check-cast p1, Lcom/google/android/gms/icing/c/a/l;

    .line 2377
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/l;->a:Z

    iget-boolean v2, p1, Lcom/google/android/gms/icing/c/a/l;->a:Z

    if-ne v1, v2, :cond_0

    .line 2380
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2384
    iget v1, p0, Lcom/google/android/gms/icing/c/a/l;->c:I

    iget v2, p1, Lcom/google/android/gms/icing/c/a/l;->c:I

    if-ne v1, v2, :cond_0

    .line 2387
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/l;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2392
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/l;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2394
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2396
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/c/a/l;->c:I

    add-int/2addr v0, v1

    .line 2397
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/l;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2398
    return v0

    .line 2392
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2173
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/l;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/l;->a:Z

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/m;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/c/a/m;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/c/a/m;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/c/a/l;->c:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 2404
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/l;->a:Z

    if-eqz v0, :cond_0

    .line 2405
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/l;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2407
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 2408
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 2409
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/l;->b:[Lcom/google/android/gms/icing/c/a/m;

    aget-object v1, v1, v0

    .line 2410
    if-eqz v1, :cond_1

    .line 2411
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2408
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2415
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/c/a/l;->c:I

    if-eqz v0, :cond_3

    .line 2416
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/c/a/l;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2418
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2419
    return-void
.end method
