.class public Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/location/copresence/internal/o;

.field public final c:Landroid/app/PendingIntent;

.field private final d:I

.field private e:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/y;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/y;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILandroid/os/IBinder;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->d:I

    .line 67
    iput p2, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    .line 68
    if-nez p3, :cond_0

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    .line 73
    :goto_0
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->c:Landroid/app/PendingIntent;

    .line 74
    return-void

    .line 71
    :cond_0
    invoke-static {p3}, Lcom/google/android/gms/location/copresence/internal/p;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    goto :goto_0
.end method

.method private constructor <init>(ILcom/google/android/gms/location/copresence/internal/o;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->d:I

    .line 79
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    .line 80
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    .line 81
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->c:Landroid/app/PendingIntent;

    .line 82
    return-void
.end method

.method public static a()Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 90
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    const/4 v1, 0x3

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;-><init>(ILcom/google/android/gms/location/copresence/internal/o;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;)Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 122
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->a:I

    packed-switch v0, :pswitch_data_0

    .line 128
    const-string v0, "UnsubscribeOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown subscribe op type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 129
    :goto_0
    return-object v0

    .line 124
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->d:Lcom/google/android/gms/location/copresence/internal/o;

    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a(Lcom/google/android/gms/location/copresence/internal/o;)Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    move-result-object v0

    goto :goto_0

    .line 126
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->e:Landroid/app/PendingIntent;

    new-instance v0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    const/4 v3, 0x2

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;-><init>(ILcom/google/android/gms/location/copresence/internal/o;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/google/android/gms/location/copresence/internal/o;)Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;
    .locals 3

    .prologue
    .line 106
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;-><init>(ILcom/google/android/gms/location/copresence/internal/o;Landroid/app/PendingIntent;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    invoke-static {}, Lcom/google/android/gms/location/copresence/internal/r;->a()Lcom/google/android/gms/location/copresence/internal/r;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/location/copresence/internal/r;->a(Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/internal/o;

    move-result-object v0

    .line 101
    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a(Lcom/google/android/gms/location/copresence/internal/o;)Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->e:Ljava/util/List;

    .line 156
    return-void
.end method

.method final b()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->d:I

    return v0
.end method

.method final c()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    if-nez v0, :cond_0

    .line 139
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    invoke-interface {v0}, Lcom/google/android/gms/location/copresence/internal/o;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->e:Ljava/util/List;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 175
    if-ne p0, p1, :cond_1

    .line 185
    :cond_0
    :goto_0
    return v0

    .line 178
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 179
    goto :goto_0

    .line 181
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 182
    goto :goto_0

    .line 184
    :cond_3
    check-cast p1, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    .line 185
    iget v2, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    iget v3, p1, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 170
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->b:Lcom/google/android/gms/location/copresence/internal/o;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 165
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/internal/y;->a(Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;Landroid/os/Parcel;I)V

    .line 166
    return-void
.end method
