.class final Lcom/google/android/gms/wallet/ow/b;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ow/a;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 511
    .line 512
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 513
    packed-switch v4, :pswitch_data_0

    .line 518
    const/4 v0, 0x1

    .line 512
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 515
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    iget-object v5, v5, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v4, v5}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    goto :goto_1

    .line 523
    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 524
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    const/16 v1, 0x64

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/ow/c;->a(I)V

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/a;->dismiss()V

    .line 528
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->e(Lcom/google/android/gms/wallet/ow/a;)V

    .line 529
    return-void

    .line 513
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 470
    .line 471
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->c:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 473
    packed-switch v4, :pswitch_data_0

    .line 478
    const/4 v0, 0x1

    .line 471
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 475
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    iget-object v5, v5, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v4, v5}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    goto :goto_1

    .line 483
    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    const/16 v1, 0x64

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/ow/c;->a(I)V

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/a;->dismiss()V

    .line 488
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->e(Lcom/google/android/gms/wallet/ow/a;)V

    .line 489
    return-void

    .line 473
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 494
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/a;->f(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/wallet/ow/c;->b(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V

    .line 497
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/a;->dismiss()V

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    .line 500
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    .line 506
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->e(Lcom/google/android/gms/wallet/ow/a;)V

    .line 507
    return-void

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    .line 503
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->d(Lcom/google/android/gms/wallet/ow/a;)Z

    goto :goto_0
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 454
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 455
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/a;->c(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/wallet/ow/c;->b(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/a;->dismiss()V

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    .line 460
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    .line 465
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->e(Lcom/google/android/gms/wallet/ow/a;)V

    .line 466
    return-void

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    .line 463
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->d(Lcom/google/android/gms/wallet/ow/a;)Z

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;

    move-result-object v0

    const/16 v1, 0x65

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/ow/c;->a(I)V

    .line 535
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/a;->dismiss()V

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/b;->a:Lcom/google/android/gms/wallet/ow/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/a;->e(Lcom/google/android/gms/wallet/ow/a;)V

    .line 539
    return-void
.end method
