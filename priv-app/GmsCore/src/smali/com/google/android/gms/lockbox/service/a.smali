.class final Lcom/google/android/gms/lockbox/service/a;
.super Lcom/google/android/gms/lockbox/c/b;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/lockbox/e/a;

.field private c:Lcom/google/android/gms/lockbox/c/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/google/android/gms/lockbox/c/b;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/google/android/gms/lockbox/service/a;->a:Landroid/content/Context;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/lockbox/e/a;->a(Landroid/content/Context;)Lcom/google/android/gms/lockbox/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    .line 81
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/lockbox/c/o;
    .locals 4

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/gms/lockbox/c/o;

    iget-object v1, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Lcom/google/android/gms/lockbox/e/a;->a(Ljava/lang/String;I)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    const/4 v3, 0x2

    invoke-virtual {v2, p1, v3}, Lcom/google/android/gms/lockbox/e/a;->a(Ljava/lang/String;I)Z

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/lockbox/c/o;-><init>(Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->c:Lcom/google/android/gms/lockbox/c/d;

    if-eqz v0, :cond_0

    .line 162
    const-string v0, "LockboxBrokerService"

    const-string v1, "Removed status changed listener."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->c:Lcom/google/android/gms/lockbox/c/d;

    invoke-static {v0}, Lcom/google/android/gms/lockbox/e/a;->b(Lcom/google/android/gms/lockbox/c/d;)V

    .line 165
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/lockbox/c/d;)V
    .locals 2

    .prologue
    .line 154
    const-string v0, "LockboxBrokerService"

    const-string v1, "Set status changed listener."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iput-object p1, p0, Lcom/google/android/gms/lockbox/service/a;->c:Lcom/google/android/gms/lockbox/c/d;

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->c:Lcom/google/android/gms/lockbox/c/d;

    invoke-static {v0}, Lcom/google/android/gms/lockbox/e/a;->a(Lcom/google/android/gms/lockbox/c/d;)V

    .line 157
    return-void
.end method

.method public final a(Lcom/google/android/gms/lockbox/c/o;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 95
    invoke-virtual {p1}, Lcom/google/android/gms/lockbox/c/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 96
    new-array v1, v5, [I

    fill-array-data v1, :array_0

    .line 100
    iget-object v2, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/lockbox/c/o;->b()Z

    move-result v4

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/lockbox/e/a;->a(Ljava/lang/String;IZ)V

    .line 103
    iget-object v2, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {p1}, Lcom/google/android/gms/lockbox/c/o;->c()Z

    move-result v3

    invoke-virtual {v2, v0, v5, v3}, Lcom/google/android/gms/lockbox/e/a;->a(Ljava/lang/String;IZ)V

    .line 106
    iget-object v2, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/lockbox/e/a;->b(Ljava/lang/String;)V

    .line 107
    iget-object v2, p0, Lcom/google/android/gms/lockbox/service/a;->a:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/common/internal/ay;->a(Landroid/content/Context;Ljava/lang/String;[I)V

    .line 110
    invoke-virtual {p1}, Lcom/google/android/gms/lockbox/c/o;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/lockbox/c/o;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/lockbox/service/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/lockbox/LockboxAlarmReceiver;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 114
    :cond_1
    return-void

    .line 96
    nop

    :array_0
    .array-data 4
        0x8
        0x7
    .end array-data
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/lockbox/g;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 119
    .line 120
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 121
    invoke-virtual {p2}, Lcom/google/android/gms/lockbox/g;->a()I

    move-result v0

    if-eqz v0, :cond_5

    .line 122
    invoke-virtual {p2}, Lcom/google/android/gms/lockbox/g;->a()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 124
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {v4, p1, v1, v0}, Lcom/google/android/gms/lockbox/e/a;->a(Ljava/lang/String;IZ)V

    .line 126
    or-int/lit8 v0, v0, 0x0

    .line 127
    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    :goto_1
    invoke-virtual {p2}, Lcom/google/android/gms/lockbox/g;->b()I

    move-result v4

    if-eqz v4, :cond_0

    .line 131
    invoke-virtual {p2}, Lcom/google/android/gms/lockbox/g;->b()I

    move-result v4

    if-ne v4, v1, :cond_2

    .line 133
    :goto_2
    iget-object v4, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    const/4 v5, 0x2

    invoke-virtual {v4, p1, v5, v1}, Lcom/google/android/gms/lockbox/e/a;->a(Ljava/lang/String;IZ)V

    .line 135
    or-int/2addr v0, v1

    .line 136
    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    move v1, v0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/lockbox/e/a;->b(Ljava/lang/String;)V

    .line 140
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v4, v0, [I

    .line 141
    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 142
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v4, v2

    .line 141
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_1
    move v0, v2

    .line 122
    goto :goto_0

    :cond_2
    move v1, v2

    .line 131
    goto :goto_2

    .line 144
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->a:Landroid/content/Context;

    invoke-static {v0, p1, v4}, Lcom/google/android/gms/common/internal/ay;->a(Landroid/content/Context;Ljava/lang/String;[I)V

    .line 146
    if-eqz v1, :cond_4

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/lockbox/service/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/lockbox/LockboxAlarmReceiver;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 149
    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/lockbox/e/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public final b()Lcom/google/android/gms/lockbox/c/q;
    .locals 6

    .prologue
    .line 175
    new-instance v0, Lcom/google/android/gms/lockbox/c/q;

    iget-object v1, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {v1}, Lcom/google/android/gms/lockbox/e/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {v2}, Lcom/google/android/gms/lockbox/e/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/lockbox/service/a;->b:Lcom/google/android/gms/lockbox/e/a;

    invoke-virtual {v3}, Lcom/google/android/gms/lockbox/e/a;->c()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/gms/lockbox/c/q;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    return-object v0
.end method
