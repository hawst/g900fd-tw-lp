.class public Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;
.super Lcom/google/android/gms/games/ui/s;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/ui/common/a/b;
.implements Lcom/google/android/gms/games/ui/common/leaderboards/e;
.implements Lcom/google/android/gms/games/ui/common/leaderboards/l;
.implements Lcom/google/android/gms/games/ui/common/leaderboards/p;
.implements Lcom/google/android/gms/games/ui/g;


# instance fields
.field private l:Lcom/google/android/gms/games/ui/common/leaderboards/o;

.field private m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

.field private n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

.field private o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

.field private p:Lcom/google/android/gms/games/ui/common/a/a;

.field private q:I

.field private r:I

.field private s:Ljava/lang/String;

.field private t:Lcom/google/android/gms/games/ui/common/leaderboards/j;

.field private u:Landroid/view/View;

.field private v:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;-><init>()V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->l:Lcom/google/android/gms/games/ui/common/leaderboards/o;

    .line 85
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    .line 654
    return-void
.end method

.method private C()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 635
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->c(Z)V

    .line 636
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Z)V

    .line 637
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->p:Lcom/google/android/gms/games/ui/common/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/a/a;->c(Z)V

    .line 638
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->i()V

    .line 639
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;)Lcom/google/android/gms/games/ui/q;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method

.method private b(Z)V
    .locals 14

    .prologue
    .line 438
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 439
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 457
    :goto_0
    return-void

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->t:Lcom/google/android/gms/games/ui/common/leaderboards/j;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/j;->V()Ljava/lang/String;

    move-result-object v2

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->t:Lcom/google/android/gms/games/ui/common/leaderboards/j;

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/ab;->a(Landroid/content/Context;)I

    move-result v5

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    .line 448
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 449
    sget-object v0, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/e/n;

    iget v3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    iget v4, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    move v6, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/e/n;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 453
    :cond_1
    sget-object v6, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/e/n;

    const/4 v8, 0x0

    iget v10, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    iget v11, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    move-object v7, v1

    move-object v9, v2

    move v12, v5

    move v13, p1

    invoke-interface/range {v6 .. v13}, Lcom/google/android/gms/games/e/n;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method


# virtual methods
.method public final B()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 628
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->b(Z)V

    .line 630
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->h:Lcom/google/android/gms/games/ui/d/p;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    .line 631
    return-void
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 643
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/n;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/n;-><init>()V

    .line 644
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/n;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 645
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    const-string v2, "nonPublicPaclButterBarDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 646
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 482
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 484
    const-string v0, "LeaderboardScoreFrag"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    :goto_0
    return-void

    .line 488
    :cond_0
    sget-object v2, Lcom/google/android/gms/games/d;->j:Lcom/google/android/gms/games/e/n;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->f()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/e/f;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/d/ab;->a(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v2, v1, v0, v3, p1}, Lcom/google/android/gms/games/e/n;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/e/f;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 5

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/q;

    .line 179
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 199
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 184
    const-string v3, "com.google.android.gms.games.EXTRA_GAME_THEME_COLOR"

    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/cn;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    invoke-static {v4}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/games/ui/cn;)I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->v:I

    .line 187
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 190
    new-instance v3, Landroid/view/View;

    invoke-direct {v3, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 191
    iget v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->v:I

    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 192
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 193
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 194
    const/4 v4, -0x1

    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 196
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 197
    iget v0, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    sget v4, Lcom/google/android/gms/g;->E:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 198
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 184
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 12

    .prologue
    .line 58
    check-cast p1, Lcom/google/android/gms/games/e/p;

    invoke-interface {p1}, Lcom/google/android/gms/games/e/p;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/e/p;->d()Lcom/google/android/gms/games/e/f;

    move-result-object v4

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v4}, Lcom/google/android/gms/games/e/f;->w_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/q;->b(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/games/e/f;->w_()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-static {v3}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->k()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->d()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/s;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/view/animation/Animation;->cancel()V

    :cond_3
    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->b(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->a_(Z)V

    :cond_4
    invoke-interface {p1}, Lcom/google/android/gms/games/e/p;->c()Lcom/google/android/gms/games/e/a;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/e/a;->i()Lcom/google/android/gms/games/Game;

    move-result-object v6

    const-string v1, ""

    if-eqz v6, :cond_5

    invoke-interface {v5}, Lcom/google/android/gms/games/e/a;->i()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s_()Ljava/lang/String;

    move-result-object v1

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->u:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->fu:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v8, Lcom/google/android/gms/g;->aL:I

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v8, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->u:Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->u:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->u:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getPaddingRight()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->u:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getPaddingBottom()I

    move-result v11

    invoke-virtual {v8, v9, v2, v10, v11}, Landroid/view/View;->setPadding(IIII)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    sget v8, Lcom/google/android/gms/q;->g:I

    invoke-virtual {v0, v2, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    const/4 v2, 0x0

    sget v8, Lcom/google/android/gms/h;->ax:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v0, v2, v8, v9, v10}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_1
    sget v2, Lcom/google/android/gms/g;->V:I

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v8

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v9

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v10

    invoke-virtual {v0, v8, v9, v10, v2}, Landroid/view/View;->setPadding(IIII)V

    iget v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    if-nez v2, :cond_a

    iget v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid TimeSpan "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/android/gms/games/e/f;->w_()V

    throw v0

    :cond_6
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    sget v8, Lcom/google/android/gms/q;->f:I

    invoke-virtual {v0, v2, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    const/4 v2, 0x0

    sget v8, Lcom/google/android/gms/h;->ay:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v0, v2, v8, v9, v10}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1

    :pswitch_0
    sget v1, Lcom/google/android/gms/p;->jx:I

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->s:Ljava/lang/String;

    if-nez v0, :cond_7

    invoke-interface {v5}, Lcom/google/android/gms/games/e/a;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->s:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->l:Lcom/google/android/gms/games/ui/common/leaderboards/o;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->s:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    invoke-interface {v5}, Lcom/google/android/gms/games/e/a;->e()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v0, v1, v2, v7}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->a(Ljava/lang/String;ILandroid/net/Uri;)V

    :cond_7
    const/4 v1, 0x0

    invoke-interface {v5}, Lcom/google/android/gms/games/e/a;->h()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v0, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v2, v0

    :goto_4
    if-ge v2, v7, :cond_17

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/e/k;

    invoke-interface {v0}, Lcom/google/android/gms/games/e/k;->d()I

    move-result v8

    iget v9, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    if-ne v8, v9, :cond_c

    iget v8, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    invoke-interface {v0}, Lcom/google/android/gms/games/e/k;->a()I

    move-result v9

    if-ne v8, v9, :cond_c

    move-object v2, v0

    :goto_5
    const-string v0, "Leaderboard variant must not be null."

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v2}, Lcom/google/android/gms/games/e/k;->f()J

    move-result-wide v0

    const-wide/16 v8, -0x1

    cmp-long v0, v0, v8

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    move v1, v0

    :goto_6
    invoke-interface {v2}, Lcom/google/android/gms/games/e/k;->h()J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v0, v8, v10

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_7
    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->f()V

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->i()V

    if-eqz v1, :cond_8

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    if-nez v0, :cond_8

    invoke-interface {v6}, Lcom/google/android/gms/games/Game;->t()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_f

    const/4 v0, 0x1

    :goto_8
    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->h()V

    :cond_8
    :goto_9
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->l:Lcom/google/android/gms/games/ui/common/leaderboards/o;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->a(Lcom/google/android/gms/games/e/k;)V

    const-wide/16 v0, 0x1

    invoke-interface {v2}, Lcom/google/android/gms/games/e/k;->k()J

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(J)V

    invoke-virtual {v4}, Lcom/google/android/gms/games/e/f;->c()I

    move-result v5

    const/4 v0, 0x3

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    if-lez v5, :cond_15

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/games/e/f;->b(I)Lcom/google/android/gms/games/e/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/e/e;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/e/e;

    const/4 v1, 0x1

    if-le v5, v1, :cond_11

    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Lcom/google/android/gms/games/e/f;->b(I)Lcom/google/android/gms/games/e/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/e/e;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/e/e;

    move-object v2, v1

    :goto_a
    const/4 v1, 0x2

    if-le v5, v1, :cond_12

    const/4 v1, 0x2

    invoke-virtual {v4, v1}, Lcom/google/android/gms/games/e/f;->b(I)Lcom/google/android/gms/games/e/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/e/e;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/e/e;

    :goto_b
    iget-object v7, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

    invoke-virtual {v7, v0, v2, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->a(Lcom/google/android/gms/games/e/e;Lcom/google/android/gms/games/e/e;Lcom/google/android/gms/games/e/e;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

    const/4 v0, 0x3

    if-gt v5, v0, :cond_13

    const/4 v0, 0x1

    :goto_c
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Z)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->p:Lcom/google/android/gms/games/ui/common/a/a;

    const/4 v0, 0x3

    if-gt v5, v0, :cond_14

    iget v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_14

    const/4 v0, 0x1

    :goto_d
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/a/a;->c(Z)V

    :goto_e
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->f(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->l:Lcom/google/android/gms/games/ui/common/leaderboards/o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->c(Z)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    const/4 v0, 0x3

    if-gt v5, v0, :cond_9

    if-nez v5, :cond_16

    :cond_9
    const/4 v0, 0x1

    :goto_f
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->h:Lcom/google/android/gms/games/ui/d/p;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v5, v1}, Lcom/google/android/gms/games/ui/d/p;->a(IIZ)V

    goto/16 :goto_0

    :pswitch_1
    sget v1, Lcom/google/android/gms/p;->jz:I

    goto/16 :goto_2

    :pswitch_2
    sget v1, Lcom/google/android/gms/p;->jy:I

    goto/16 :goto_2

    :cond_a
    iget v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    packed-switch v2, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid TimeSpan "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_3
    sget v2, Lcom/google/android/gms/p;->jD:I

    :goto_10
    if-nez v1, :cond_b

    const-string v1, ""

    :cond_b
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v8, v9

    invoke-virtual {v7, v2, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :pswitch_4
    sget v2, Lcom/google/android/gms/p;->jF:I

    goto :goto_10

    :pswitch_5
    sget v2, Lcom/google/android/gms/p;->jE:I

    goto :goto_10

    :cond_c
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_4

    :cond_d
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_6

    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_8

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->e()V

    goto/16 :goto_9

    :cond_11
    const/4 v1, 0x0

    move-object v2, v1

    goto/16 :goto_a

    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_b

    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_c

    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_d

    :cond_15
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->p:Lcom/google/android/gms/games/ui/common/a/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/a/a;->c(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_e

    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_f

    :cond_17
    move-object v2, v1

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 3

    .prologue
    .line 251
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/y;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/Player;

    move-result-object v1

    .line 252
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    .line 253
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 254
    const-string v0, "LeaderboardScoreFrag"

    const-string v1, "couldn\'t get current player ID; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->finish()V

    .line 269
    :goto_1
    return-void

    .line 252
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->l:Lcom/google/android/gms/games/ui/common/leaderboards/o;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->a(Lcom/google/android/gms/games/Player;)V

    .line 259
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->a(Ljava/lang/String;)V

    .line 260
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->p:Lcom/google/android/gms/games/ui/common/a/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->t:Lcom/google/android/gms/games/ui/common/leaderboards/j;

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/common/leaderboards/j;->T()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->t:Lcom/google/android/gms/games/ui/common/leaderboards/j;

    invoke-interface {v2}, Lcom/google/android/gms/games/ui/common/leaderboards/j;->U()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->b(Z)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 4

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    .line 462
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v1

    .line 463
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 466
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 468
    if-eqz v1, :cond_0

    .line 469
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_MY_PROFILE"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.gms.games.PLAYER"

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/q;->startActivity(Landroid/content/Intent;)V

    .line 477
    :goto_0
    return-void

    .line 471
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.destination.VIEW_PROFILE_COMPARISON"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.gms.games.OTHER_PLAYER"

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/q;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 475
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {p1, v1}, Lcom/google/android/gms/games/ui/common/players/g;->a(Lcom/google/android/gms/games/Player;Z)Lcom/google/android/gms/games/ui/common/players/g;

    move-result-object v1

    const-string v2, "com.google.android.gms.games.ui.dialog.profileSummaryDialog"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 502
    iget v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    if-ne p1, v0, :cond_0

    .line 512
    :goto_0
    return-void

    .line 507
    :cond_0
    iput p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    .line 510
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->b(Z)V

    .line 511
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->C()V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 521
    iget v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    if-ne p1, v0, :cond_0

    .line 531
    :goto_0
    return-void

    .line 526
    :cond_0
    iput p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    .line 529
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->b(Z)V

    .line 530
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->C()V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x1

    return v0
.end method

.method public final h()F
    .locals 1

    .prologue
    .line 216
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public final n()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/q;

    .line 205
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    invoke-super {p0}, Lcom/google/android/gms/games/ui/s;->n()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 209
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->v:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method

.method public final o()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 611
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->a_(Z)V

    .line 614
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->b(Z)V

    .line 615
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    const v6, 0x3f59999a    # 0.85f

    .line 97
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onActivityCreated(Landroid/os/Bundle;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/j;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->t:Lcom/google/android/gms/games/ui/common/leaderboards/j;

    .line 101
    if-eqz p1, :cond_3

    const-string v0, "collection"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    const-string v0, "time_span"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    .line 104
    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/leaderboards/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/leaderboards/e;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->c(Z)V

    .line 107
    if-eqz p1, :cond_1

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->a(Landroid/os/Bundle;)V

    .line 112
    :cond_1
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/o;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    iget v3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;-><init>(Landroid/content/Context;IILcom/google/android/gms/games/ui/common/leaderboards/p;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->l:Lcom/google/android/gms/games/ui/common/leaderboards/o;

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->l:Lcom/google/android/gms/games/ui/common/leaderboards/o;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->c(Z)V

    .line 117
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/r;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/leaderboards/l;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Z)V

    .line 122
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/leaderboards/l;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(Lcom/google/android/gms/games/ui/g;)V

    .line 127
    new-instance v0, Lcom/google/android/gms/games/ui/common/a/a;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->lB:I

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/gms/games/ui/common/a/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/a/b;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->p:Lcom/google/android/gms/games/ui/common/a/a;

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->p:Lcom/google/android/gms/games/ui/common/a/a;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/common/a/a;->c(Z)V

    .line 133
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->aK:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->u:Landroid/view/View;

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(Landroid/view/View;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 141
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/f;->W:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->a(II)V

    .line 145
    new-instance v0, Lcom/google/android/gms/games/ui/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/by;-><init>()V

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->l:Lcom/google/android/gms/games/ui/common/leaderboards/o;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 148
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->m:Lcom/google/android/gms/games/ui/common/leaderboards/r;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 149
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->n:Lcom/google/android/gms/games/ui/common/leaderboards/k;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 150
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->p:Lcom/google/android/gms/games/ui/common/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 151
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/by;->a()Lcom/google/android/gms/games/ui/bw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->a(Landroid/support/v7/widget/bv;)V

    .line 154
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r()V

    .line 158
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 160
    iget v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->v:I

    shr-int/lit8 v2, v1, 0x18

    and-int/lit16 v2, v2, 0xff

    shr-int/lit8 v3, v1, 0x10

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v4, v1, 0x8

    and-int/lit16 v4, v4, 0xff

    and-int/lit16 v1, v1, 0xff

    const/high16 v5, 0x42190000    # 38.25f

    int-to-float v2, v2

    mul-float/2addr v2, v6

    add-float/2addr v2, v5

    float-to-int v2, v2

    int-to-float v3, v3

    mul-float/2addr v3, v6

    add-float/2addr v3, v7

    float-to-int v3, v3

    int-to-float v4, v4

    mul-float/2addr v4, v6

    add-float/2addr v4, v7

    float-to-int v4, v4

    int-to-float v1, v1

    mul-float/2addr v1, v6

    add-float/2addr v1, v7

    float-to-int v1, v1

    shl-int/lit8 v2, v2, 0x18

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    shl-int/lit8 v3, v4, 0x8

    or-int/2addr v2, v3

    or-int/2addr v1, v2

    .line 161
    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 163
    :cond_2
    return-void

    .line 101
    :cond_3
    iput v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->t:Lcom/google/android/gms/games/ui/common/leaderboards/j;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/j;->W()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    iget v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    const-string v1, "games.leaderboard_prefs"

    invoke-virtual {v0, v1, v4}, Lcom/google/android/gms/games/ui/q;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "time_span"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 428
    invoke-super {p0}, Lcom/google/android/gms/games/ui/s;->onPause()V

    .line 431
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->d:Lcom/google/android/gms/games/ui/q;

    const-string v1, "games.leaderboard_prefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/q;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 433
    const-string v1, "time_span"

    iget v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 434
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 435
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 221
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 222
    const-string v0, "collection"

    iget v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    const-string v0, "time_span"

    iget v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->r:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->o:Lcom/google/android/gms/games/ui/common/leaderboards/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/leaderboards/d;->b(Landroid/os/Bundle;)V

    .line 226
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 619
    const/4 v0, 0x1

    return v0
.end method
