.class public final Lcom/google/android/gms/fitness/d/r;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/k/a/az;

.field private static final f:Ljava/util/Map;

.field private static final g:Ljava/util/Map;


# instance fields
.field private b:Ljava/util/Map;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/Map;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/16 v0, 0x7c

    invoke-static {v0}, Lcom/google/k/a/at;->a(C)Lcom/google/k/a/at;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Lcom/google/k/a/at;->b(Ljava/lang/String;)Lcom/google/k/a/az;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/d/r;->a:Lcom/google/k/a/az;

    .line 46
    invoke-static {}, Lcom/google/k/c/bo;->h()Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->j:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->i:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->h:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->d:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->x:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->e:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->f:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->t:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->s:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->q:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->r:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->o:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->n:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->k:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->C:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->u:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->l:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->m:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->D:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->g:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->E:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->p:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->F:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->c:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->b:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->a:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->v:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->G:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.read"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/bp;->a()Lcom/google/k/c/bo;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/d/r;->f:Ljava/util/Map;

    .line 78
    invoke-static {}, Lcom/google/k/c/bo;->h()Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->j:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->i:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->h:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->d:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->x:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->e:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->f:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->t:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->s:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->q:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->r:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->o:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->n:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->k:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->C:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->u:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->m:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->l:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->D:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->g:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->E:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->p:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->F:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->c:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->b:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->a:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->v:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->G:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com/auth/fitness.body.write"

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/bp;->a()Lcom/google/k/c/bo;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/d/r;->g:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/d/r;->b:Ljava/util/Map;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/fitness/d/r;->c:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/d/r;->d:Ljava/util/Map;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/fitness/d/r;->e:Ljava/lang/String;

    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/fitness/d/r;->a()V

    .line 112
    return-void
.end method

.method public static a(Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Ljava/util/List;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/d/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    sget-object v0, Lcom/google/android/gms/fitness/d/r;->f:Ljava/util/Map;

    invoke-static {p0, v0}, Lcom/google/android/gms/fitness/d/r;->a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    .line 122
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/fitness/d/r;->g:Ljava/util/Map;

    invoke-static {p0, v0}, Lcom/google/android/gms/fitness/d/r;->a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 6

    .prologue
    .line 132
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 134
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    .line 135
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 136
    if-nez v1, :cond_1

    .line 137
    const-string v1, "No scope found. Assuming private custom data: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v1, v4}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 140
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    :cond_2
    return-object v2
.end method

.method private declared-synchronized a()V
    .locals 5

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/fitness/d/r;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 160
    iput-object v0, p0, Lcom/google/android/gms/fitness/d/r;->c:Ljava/lang/String;

    .line 161
    iget-object v1, p0, Lcom/google/android/gms/fitness/d/r;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 162
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 163
    sget-object v1, Lcom/google/android/gms/fitness/d/r;->a:Lcom/google/k/a/az;

    invoke-virtual {v1, v0}, Lcom/google/k/a/az;->a(Ljava/lang/CharSequence;)Ljava/util/Map;

    move-result-object v0

    .line 165
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 166
    iget-object v2, p0, Lcom/google/android/gms/fitness/d/r;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Cannot parse GServices flags %s and %s. Clearing all configs."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/fitness/d/r;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/fitness/d/r;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 188
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/fitness/d/r;->c:Ljava/lang/String;

    .line 189
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/fitness/d/r;->e:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    :cond_0
    monitor-exit p0

    return-void

    .line 171
    :cond_1
    :try_start_2
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 172
    iget-object v1, p0, Lcom/google/android/gms/fitness/d/r;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 173
    iput-object v0, p0, Lcom/google/android/gms/fitness/d/r;->e:Ljava/lang/String;

    .line 174
    iget-object v1, p0, Lcom/google/android/gms/fitness/d/r;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 175
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 176
    sget-object v1, Lcom/google/android/gms/fitness/d/r;->a:Lcom/google/k/a/az;

    invoke-virtual {v1, v0}, Lcom/google/k/a/az;->a(Ljava/lang/CharSequence;)Ljava/util/Map;

    move-result-object v0

    .line 178
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 179
    iget-object v2, p0, Lcom/google/android/gms/fitness/d/r;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final b(Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Ljava/util/List;
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/gms/fitness/d/r;->a()V

    .line 150
    sget-object v0, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    if-ne p2, v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/fitness/d/r;->b:Ljava/util/Map;

    invoke-static {p1, v0}, Lcom/google/android/gms/fitness/d/r;->a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/d/r;->d:Ljava/util/Map;

    invoke-static {p1, v0}, Lcom/google/android/gms/fitness/d/r;->a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
