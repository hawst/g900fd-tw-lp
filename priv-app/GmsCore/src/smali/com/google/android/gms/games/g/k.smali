.class public final Lcom/google/android/gms/games/g/k;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Z

.field public d:Lcom/google/android/gms/games/g/aa;

.field public e:Lcom/google/android/gms/games/g/l;

.field public f:Ljava/lang/String;

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 398
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 399
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/k;->b:I

    iput-boolean v1, p0, Lcom/google/android/gms/games/g/k;->c:Z

    iput-object v2, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    iput-object v2, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/k;->g:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/k;->cachedSize:I

    .line 400
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 513
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 514
    iget-object v1, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 515
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 518
    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/g/k;->b:I

    if-eqz v1, :cond_1

    .line 519
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/games/g/k;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 522
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/k;->c:Z

    if-eqz v1, :cond_2

    .line 523
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/k;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 526
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    if-eqz v1, :cond_3

    .line 527
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 530
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    if-eqz v1, :cond_4

    .line 531
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 534
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 535
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 538
    :cond_5
    iget v1, p0, Lcom/google/android/gms/games/g/k;->g:I

    if-eqz v1, :cond_6

    .line 539
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/games/g/k;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 542
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 416
    if-ne p1, p0, :cond_1

    .line 464
    :cond_0
    :goto_0
    return v0

    .line 419
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/k;

    if-nez v2, :cond_2

    move v0, v1

    .line 420
    goto :goto_0

    .line 422
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/k;

    .line 423
    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 424
    iget-object v2, p1, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 425
    goto :goto_0

    .line 427
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 428
    goto :goto_0

    .line 430
    :cond_4
    iget v2, p0, Lcom/google/android/gms/games/g/k;->b:I

    iget v3, p1, Lcom/google/android/gms/games/g/k;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 431
    goto :goto_0

    .line 433
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/k;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/k;->c:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 434
    goto :goto_0

    .line 436
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    if-nez v2, :cond_7

    .line 437
    iget-object v2, p1, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    if-eqz v2, :cond_8

    move v0, v1

    .line 438
    goto :goto_0

    .line 441
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    iget-object v3, p1, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 442
    goto :goto_0

    .line 445
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    if-nez v2, :cond_9

    .line 446
    iget-object v2, p1, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    if-eqz v2, :cond_a

    move v0, v1

    .line 447
    goto :goto_0

    .line 450
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    iget-object v3, p1, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/l;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 451
    goto :goto_0

    .line 454
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 455
    iget-object v2, p1, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 456
    goto :goto_0

    .line 458
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 459
    goto :goto_0

    .line 461
    :cond_c
    iget v2, p0, Lcom/google/android/gms/games/g/k;->g:I

    iget v3, p1, Lcom/google/android/gms/games/g/k;->g:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 462
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 469
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 472
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/g/k;->b:I

    add-int/2addr v0, v2

    .line 473
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/k;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    .line 474
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 476
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 478
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 480
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/k;->g:I

    add-int/2addr v0, v1

    .line 481
    return v0

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 473
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    .line 474
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/aa;->hashCode()I

    move-result v0

    goto :goto_2

    .line 476
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/l;->hashCode()I

    move-result v0

    goto :goto_3

    .line 478
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 352
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/k;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/k;->c:Z

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/aa;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/g/l;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/k;->g:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/k;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 490
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/g/k;->b:I

    if-eqz v0, :cond_1

    .line 491
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/g/k;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 493
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/k;->c:Z

    if-eqz v0, :cond_2

    .line 494
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/k;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 496
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    if-eqz v0, :cond_3

    .line 497
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/g/k;->d:Lcom/google/android/gms/games/g/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 499
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    if-eqz v0, :cond_4

    .line 500
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/g/k;->e:Lcom/google/android/gms/games/g/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 502
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 503
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/games/g/k;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 505
    :cond_5
    iget v0, p0, Lcom/google/android/gms/games/g/k;->g:I

    if-eqz v0, :cond_6

    .line 506
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/games/g/k;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 508
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 509
    return-void
.end method
