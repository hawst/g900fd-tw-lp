.class final Lcom/google/android/gms/smart_profile/header/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/header/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/smart_profile/header/b;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/header/e;->a:Lcom/google/android/gms/smart_profile/header/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 4

    .prologue
    .line 232
    new-instance v0, Lcom/google/android/gms/smart_profile/b/g;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/e;->a:Lcom/google/android/gms/smart_profile/header/b;

    iget-object v1, v1, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/e;->a:Lcom/google/android/gms/smart_profile/header/b;

    iget-object v2, v2, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v2, v2, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/header/e;->a:Lcom/google/android/gms/smart_profile/header/b;

    iget-object v3, v3, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/bg;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/smart_profile/b/g;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/api/v;)V

    return-object v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 228
    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/e;->a:Lcom/google/android/gms/smart_profile/header/b;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/e;->a:Lcom/google/android/gms/smart_profile/header/b;

    iget-object v2, v2, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/e;->a:Lcom/google/android/gms/smart_profile/header/b;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/header/b;->d:Lcom/google/android/gms/smart_profile/header/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/e;->a:Lcom/google/android/gms/smart_profile/header/b;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/header/b;->d:Lcom/google/android/gms/smart_profile/header/d;

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/header/d;->a()V

    :cond_0
    return-void
.end method
