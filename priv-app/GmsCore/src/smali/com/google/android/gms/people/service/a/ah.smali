.class public final Lcom/google/android/gms/people/service/a/ah;
.super Lcom/google/android/gms/people/service/a;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:J

.field private final k:Z

.field private final l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/google/android/gms/people/service/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    .line 28
    iput-object p5, p0, Lcom/google/android/gms/people/service/a/ah;->g:Ljava/lang/String;

    .line 29
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/ah;->h:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Lcom/google/android/gms/people/service/a/ah;->i:Ljava/lang/String;

    .line 31
    iput-wide p7, p0, Lcom/google/android/gms/people/service/a/ah;->j:J

    .line 32
    iput-boolean p9, p0, Lcom/google/android/gms/people/service/a/ah;->k:Z

    .line 33
    iput-boolean p10, p0, Lcom/google/android/gms/people/service/a/ah;->l:Z

    .line 34
    return-void
.end method


# virtual methods
.method public final e()V
    .locals 18

    .prologue
    .line 38
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/service/a/ah;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/service/a/ah;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 51
    :goto_0
    return-void

    .line 43
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/service/a/ah;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v2

    .line 44
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/people/service/a/ah;->k:Z

    if-eqz v3, :cond_1

    .line 45
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/service/a/ah;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/a/ah;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/gms/people/service/a/ah;->j:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/service/a/ah;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/service/a/ah;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/gms/people/service/a/ah;->l:Z

    const-string v8, "by client (ua)"

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Z

    goto :goto_0

    .line 48
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/service/a/ah;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/a/ah;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/gms/people/service/a/ah;->j:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/people/service/a/ah;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/people/service/a/ah;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/gms/people/service/a/ah;->l:Z

    const-string v12, "by client (non-ua)"

    sget-object v5, Lcom/google/android/gms/people/a/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v0, v5

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v12}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;)Z

    goto :goto_0
.end method
