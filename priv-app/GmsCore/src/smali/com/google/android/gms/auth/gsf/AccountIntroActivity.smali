.class public Lcom/google/android/gms/auth/gsf/AccountIntroActivity;
.super Lcom/google/android/gms/auth/gsf/a;
.source "SourceFile"


# static fields
.field private static final t:Lcom/google/android/gms/auth/d/a;

.field private static volatile u:Z


# instance fields
.field private v:Landroid/os/Bundle;

.field private w:Landroid/content/Intent;

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 59
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "GLSActivity"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "AccountIntroActivity"

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    .line 99
    sput-boolean v4, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->u:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/a;-><init>()V

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 732
    if-nez p1, :cond_0

    .line 733
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 739
    :cond_0
    const-string v0, "specialNotificationMsgHtml"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/gsf/h;->I:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 740
    const-string v0, "nameCompleted"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 741
    const-string v0, "photoCompleted"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 743
    const-string v1, "firstName"

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->b()Ljava/util/HashMap;

    move-result-object v0

    const-string v2, "firstName"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 745
    const-string v1, "lastName"

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->b()Ljava/util/HashMap;

    move-result-object v0

    const-string v2, "lastName"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 751
    const-string v0, "mUserData"

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 753
    const-string v0, "is_new_account"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 754
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(ILandroid/content/Intent;)V

    .line 755
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/gsf/AccountIntroActivity;)Z
    .locals 1

    .prologue
    .line 58
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/gsf/AccountIntroActivity;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->j()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/auth/gsf/AccountIntroActivity;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->d()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->aa:Z

    if-eqz v0, :cond_1

    .line 239
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Edu provisioning without name and email."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 251
    :goto_0
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Setup a Google account using GmsCore managed workflow."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 252
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->i()Z

    move-result v0

    .line 253
    if-nez v0, :cond_0

    .line 254
    new-instance v0, Lcom/google/android/gms/auth/frp/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/frp/a;-><init>(Landroid/content/Context;)V

    .line 257
    new-instance v1, Lcom/google/android/gms/auth/gsf/e;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/auth/gsf/e;-><init>(Lcom/google/android/gms/auth/gsf/AccountIntroActivity;Lcom/google/android/gms/auth/frp/a;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/gsf/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 290
    :cond_0
    return-void

    .line 241
    :cond_1
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Provisioning with name/email. Edu: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v2, v2, Lcom/google/android/gms/auth/gsf/h;->aa:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 347
    sget-object v0, Lcom/google/android/gms/auth/gsf/b;->r:Lcom/google/android/gms/auth/gsf/d;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/d;->a:Z

    if-nez v0, :cond_0

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/b;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 348
    const-string v0, "com.google.android.gsf.login.SetupWirelessActivity"

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/i;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 349
    const/16 v1, 0x3fd

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    .line 353
    :goto_1
    return-void

    .line 347
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 352
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->f()V

    goto :goto_1
.end method

.method private f()V
    .locals 2

    .prologue
    .line 358
    sget-boolean v0, Lcom/google/android/gms/auth/gsf/b;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/b;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "digest"

    invoke-static {v0, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 359
    const-string v0, "com.google.android.gsf.login.SetupWirelessActivity"

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/i;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 360
    const/16 v1, 0x3f0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    .line 365
    :goto_1
    return-void

    .line 358
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 364
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->g()V

    goto :goto_1
.end method

.method private g()V
    .locals 2

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->aa:Z

    if-eqz v0, :cond_0

    .line 369
    const-string v0, "com.google.android.gsf.login.EduLoginActivity"

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/i;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 370
    iget v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    .line 374
    :goto_0
    return-void

    .line 373
    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->b:I

    const/16 v1, 0x3f6

    if-ne v0, v1, :cond_1

    const-string v0, "com.google.android.gsf.login.CreateAccountActivity"

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/i;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    :goto_1
    iget v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/auth/gsf/h;->c:Lcom/google/android/gms/auth/firstparty/shared/k;

    const-string v0, "com.google.android.gsf.login.LoginActivity"

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/i;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method private h()V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    if-nez v0, :cond_0

    .line 397
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->d()V

    .line 416
    :goto_0
    return-void

    .line 400
    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/frp/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/frp/a;-><init>(Landroid/content/Context;)V

    .line 402
    new-instance v1, Lcom/google/android/gms/auth/gsf/f;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/auth/gsf/f;-><init>(Lcom/google/android/gms/auth/gsf/AccountIntroActivity;Lcom/google/android/gms/auth/frp/a;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/gsf/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 419
    iget-boolean v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->x:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->ab:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/b/a;->P:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->o:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/gsf/h;->ai:Z

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 425
    if-eqz v0, :cond_0

    .line 426
    const v1, 0xa411

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    .line 427
    const/4 v0, 0x1

    .line 430
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 5

    .prologue
    const/16 v3, 0x408

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 443
    sget-object v0, Lcom/google/android/gms/auth/b/a;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/gms/auth/b/a;->K:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 445
    new-instance v0, Lcom/google/android/gms/auth/login/al;

    invoke-direct {v0}, Lcom/google/android/gms/auth/login/al;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/login/al;->a(I)Lcom/google/android/gms/auth/login/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/al;->k()Lcom/google/android/gms/auth/login/al;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v3, v3, Lcom/google/android/gms/auth/gsf/h;->ab:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/login/al;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/auth/login/al;

    move-result-object v0

    const-string v3, "SID"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/login/al;->d(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v3, v3, Lcom/google/android/gms/auth/gsf/h;->ac:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/login/al;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lcom/google/android/gms/auth/login/al;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/auth/firstparty/shared/k;->b:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/login/al;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/login/al;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v3, v3, Lcom/google/android/gms/auth/gsf/h;->C:Landroid/os/Bundle;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/login/al;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/login/al;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/auth/b/a;->K:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/auth/login/al;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v3, v3, Lcom/google/android/gms/auth/gsf/h;->l:Z

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/login/al;->b(Z)Lcom/google/android/gms/auth/login/al;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->bF:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/login/al;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->W:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    iget-object v4, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v4, v4, Lcom/google/android/gms/auth/gsf/h;->D:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/auth/login/al;->c(Z)Lcom/google/android/gms/auth/login/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/gsf/h;->P:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v3, "purchaser_email"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, v1, Lcom/google/android/gms/auth/gsf/h;->Q:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v3, "purchaser_name"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/auth/login/al;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    const v1, 0xa412

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    .line 449
    :goto_2
    return-void

    .line 445
    :cond_0
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    const-string v4, "com.android.settings"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "com.android.vending"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 447
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Starting legacy setup wizard account setup flow."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    const-string v0, "com.google.android.gsf.login.AccountPreIntroUIActivity"

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/i;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Starting legacy standard account setup flow."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    const-string v0, "com.google.android.gsf.login.AccountIntroUIActivity"

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/i;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    goto :goto_2
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;
    .locals 1

    .prologue
    .line 759
    invoke-static {p1}, Lcom/google/android/gms/auth/a/h;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    .prologue
    const/16 v9, 0x3f7

    const/4 v2, 0x0

    const/4 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 503
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v1, "GLSActivity"

    const-string v3, "onActivityResult(%d, %d, %s) called."

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    aput-object p3, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    sparse-switch p1, :sswitch_data_0

    .line 678
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/gsf/a;->onActivityResult(IILandroid/content/Intent;)V

    .line 681
    :cond_0
    :goto_0
    return-void

    .line 510
    :sswitch_0
    if-ne p2, v8, :cond_1

    .line 511
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->h()V

    goto :goto_0

    .line 515
    :cond_1
    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    .line 516
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto :goto_0

    .line 521
    :sswitch_1
    if-ne p2, v8, :cond_2

    .line 522
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->j()V

    goto :goto_0

    .line 524
    :cond_2
    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    .line 525
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto :goto_0

    .line 531
    :sswitch_2
    sparse-switch p2, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->c()V

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto :goto_0

    :sswitch_5
    iput v9, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->b:I

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-boolean v6, v0, Lcom/google/android/gms/auth/gsf/h;->m:Z

    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->e()V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x3f6

    iput v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->b:I

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-boolean v7, v0, Lcom/google/android/gms/auth/gsf/h;->m:Z

    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->e()V

    goto :goto_0

    .line 536
    :sswitch_7
    if-nez p2, :cond_3

    .line 539
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-object v2, v0, Lcom/google/android/gms/auth/gsf/h;->P:Ljava/lang/String;

    .line 540
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-object v2, v0, Lcom/google/android/gms/auth/gsf/h;->Q:Ljava/lang/String;

    .line 541
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->d()V

    goto :goto_0

    .line 543
    :cond_3
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AccountIntro: activity result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 544
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 545
    invoke-direct {p0, p3}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;)V

    .line 549
    :goto_1
    iput-boolean v6, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->f:Z

    .line 552
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->F:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 554
    if-ne p2, v8, :cond_5

    .line 555
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Ljava/lang/String;)V

    .line 560
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto/16 :goto_0

    .line 547
    :cond_4
    invoke-virtual {p0, p2}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    goto :goto_1

    .line 558
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->c()V

    goto :goto_2

    .line 569
    :sswitch_8
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->g()V

    goto/16 :goto_0

    .line 573
    :sswitch_9
    if-nez p2, :cond_6

    .line 574
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->d()V

    goto/16 :goto_0

    .line 576
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->f()V

    goto/16 :goto_0

    .line 580
    :sswitch_a
    if-eq p2, v8, :cond_7

    .line 583
    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    .line 584
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto/16 :goto_0

    .line 588
    :cond_7
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->d()V

    goto/16 :goto_0

    .line 592
    :sswitch_b
    packed-switch p2, :pswitch_data_0

    .line 614
    :sswitch_c
    sparse-switch p2, :sswitch_data_2

    .line 658
    :sswitch_d
    packed-switch p2, :pswitch_data_1

    .line 669
    :goto_3
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->w:Landroid/content/Intent;

    if-eqz v0, :cond_d

    .line 670
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->w:Landroid/content/Intent;

    invoke-virtual {p0, v8, v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(ILandroid/content/Intent;)V

    .line 671
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto/16 :goto_0

    .line 594
    :pswitch_1
    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    .line 595
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto/16 :goto_0

    .line 598
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->w:Landroid/content/Intent;

    .line 600
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.ACTION_CARRIER_SETUP"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "device_setup"

    invoke-virtual {v1, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/telephony/TelephonyManager;->getCarrierPackageNamesForIntent(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v7, :cond_8

    const-string v2, "GLSActivity"

    const-string v3, "Multiple matching carrier apps found, launching the first."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "disable_back"

    invoke-virtual {v1, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-object v0, v1

    .line 601
    :goto_4
    if-eqz v0, :cond_a

    .line 602
    const/16 v1, 0x414

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_9
    move-object v0, v2

    .line 600
    goto :goto_4

    .line 605
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->w:Landroid/content/Intent;

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(ILandroid/content/Intent;)V

    .line 606
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto/16 :goto_0

    .line 610
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->j()V

    goto/16 :goto_0

    .line 616
    :sswitch_e
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Starting AfW"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.androidforwork"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v1, "com.google.android.androidforwork.PROVISIONING_TYPE"

    const-string v2, "device_owner"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    const-string v1, "AfW start requested, no intent found."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    goto/16 :goto_0

    .line 619
    :sswitch_f
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->i()Z

    move-result v0

    .line 620
    if-nez v0, :cond_0

    .line 621
    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    .line 622
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto/16 :goto_0

    .line 626
    :sswitch_10
    if-nez p3, :cond_c

    .line 628
    invoke-virtual {p0, p2}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    .line 629
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto/16 :goto_0

    .line 638
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    const-string v1, "is_new_account"

    invoke-virtual {p3, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/gsf/h;->k:Z

    .line 640
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-boolean v6, v0, Lcom/google/android/gms/auth/gsf/h;->p:Z

    .line 641
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    const-string v1, "authAccount"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/auth/gsf/h;->a:Ljava/lang/String;

    .line 642
    const-string v0, "com.google.android.gsf.login.LoginActivity"

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/i;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 644
    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/k;->a:Lcom/google/android/gms/auth/firstparty/shared/k;

    invoke-static {v1}, Lcom/google/android/gms/auth/a/h;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;

    move-result-object v1

    .line 645
    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/a/h;->b(Landroid/content/Intent;)V

    .line 648
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/auth/login/a/b;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    .line 650
    invoke-virtual {p0, v0, v9}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 653
    :sswitch_11
    invoke-virtual {p0, v7}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    .line 654
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    goto/16 :goto_0

    .line 662
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-boolean v7, v0, Lcom/google/android/gms/auth/gsf/h;->af:Z

    goto/16 :goto_3

    .line 665
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-boolean v6, v0, Lcom/google/android/gms/auth/gsf/h;->af:Z

    goto/16 :goto_3

    .line 673
    :cond_d
    const-string v0, "GLSActivity"

    const-string v1, "mD2dData missing!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->j()V

    goto/16 :goto_0

    .line 508
    nop

    :sswitch_data_0
    .sparse-switch
        0x3f0 -> :sswitch_8
        0x3f6 -> :sswitch_7
        0x3f7 -> :sswitch_7
        0x3fd -> :sswitch_9
        0x408 -> :sswitch_2
        0x414 -> :sswitch_d
        0xa410 -> :sswitch_a
        0xa411 -> :sswitch_b
        0xa412 -> :sswitch_c
        0xa413 -> :sswitch_1
        0xa414 -> :sswitch_0
    .end sparse-switch

    .line 531
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_3
        0x7 -> :sswitch_4
        0x8 -> :sswitch_5
        0x9 -> :sswitch_6
    .end sparse-switch

    .line 592
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 614
    :sswitch_data_2
    .sparse-switch
        -0x1 -> :sswitch_10
        0x0 -> :sswitch_f
        0x1 -> :sswitch_11
        0xb -> :sswitch_e
    .end sparse-switch

    .line 658
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 685
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->setResult(I)V

    .line 686
    invoke-super {p0}, Lcom/google/android/gms/auth/gsf/a;->onBackPressed()V

    .line 687
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 107
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/gsf/a;->onCreate(Landroid/os/Bundle;)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 109
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->u:Z

    .line 110
    if-nez p1, :cond_0

    .line 111
    const-string v0, "session"

    invoke-static {v0}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    move-result-object v0

    .line 112
    const-string v3, "AccountIntroActivity"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "suppress_device_to_device_setup"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->x:Z

    .line 133
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->r:Lcom/google/android/gms/auth/gsf/d;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/d;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    if-eqz v0, :cond_1

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v3, "com.google"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_1

    invoke-direct {p0, v4}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Landroid/content/Intent;)V

    const v0, 0x320cf

    invoke-static {v0, v4}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    const-string v0, "SetupWizardAccountInfoSharedPrefs"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/a;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->finish()V

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 213
    :goto_1
    return-void

    .line 115
    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Creating from icicle."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 116
    invoke-static {p1}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    .line 117
    const-string v0, "d2dData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->w:Landroid/content/Intent;

    .line 118
    const-string v0, "suppress_device_to_device_setup"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->x:Z

    goto :goto_1

    :cond_1
    move v0, v1

    .line 133
    goto :goto_0

    .line 146
    :cond_2
    new-instance v0, Lcom/google/android/gms/auth/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/a/c;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/a/c;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/auth/gsf/h;->ac:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    .line 148
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    if-eqz v0, :cond_5

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v3, "allowed_domains"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 152
    if-eqz v3, :cond_3

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, Lcom/google/android/gms/auth/gsf/h;->ab:Ljava/util/ArrayList;

    .line 154
    array-length v4, v3

    move v0, v1

    :goto_2
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    .line 155
    iget-object v6, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v6, v6, Lcom/google/android/gms/auth/gsf/h;->ab:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 159
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v3, "suppressLoginTos"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 160
    if-eqz v0, :cond_4

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-boolean v2, v0, Lcom/google/android/gms/auth/gsf/h;->x:Z

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-boolean v2, v0, Lcom/google/android/gms/auth/gsf/h;->K:Z

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-boolean v2, v0, Lcom/google/android/gms/auth/gsf/h;->R:Z

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-boolean v2, v0, Lcom/google/android/gms/auth/gsf/h;->Z:Z

    .line 167
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v2, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v3, "isEduSignin"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/gms/auth/gsf/h;->aa:Z

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v2, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v3, "suppressCreditCardRequestActivity"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/gsf/h;->W:Z

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v2, "suppressGoogleServicesActivity"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/gsf/h;->X:Z

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v2, "suppressNameCheck"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/gsf/h;->Y:Z

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v2, "suppressLoginTos"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/gsf/h;->Z:Z

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v2, "carrierSetupLaunched"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/gsf/h;->af:Z

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v2, "wifiScreenShown"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/gsf/h;->ag:Z

    .line 184
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    if-eqz v0, :cond_6

    .line 185
    invoke-virtual {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "google_setup:provisioned_info"

    invoke-static {v0, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 189
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 191
    const-string v0, "purchaser_gaia_email"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 192
    const-string v2, "purchaser_name"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 194
    iget-object v2, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-object v0, v2, Lcom/google/android/gms/auth/gsf/h;->P:Ljava/lang/String;

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-object v1, v0, Lcom/google/android/gms/auth/gsf/h;->Q:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :cond_6
    :goto_3
    sget-object v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->q:Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;

    const-string v1, "GLSActivity"

    const-string v2, "starting account intro."

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/firstparty/shared/LatencyTracker;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->ai:Z

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->o:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->a(Landroid/content/Context;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const v1, 0xa414

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 197
    :catch_0
    move-exception v0

    .line 198
    sget-object v1, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->t:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Unable to read provisionedInfo."

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 200
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    if-eqz v0, :cond_6

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v1, "purchaser_gaia_email"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->v:Landroid/os/Bundle;

    const-string v2, "purchaser_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 204
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 205
    iget-object v2, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-object v0, v2, Lcom/google/android/gms/auth/gsf/h;->P:Ljava/lang/String;

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iput-object v1, v0, Lcom/google/android/gms/auth/gsf/h;->Q:Ljava/lang/String;

    goto :goto_3

    .line 212
    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->h()V

    goto/16 :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 227
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/gsf/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 228
    const-string v0, "d2dData"

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->w:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 229
    const-string v0, "suppress_device_to_device_setup"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 230
    return-void
.end method
