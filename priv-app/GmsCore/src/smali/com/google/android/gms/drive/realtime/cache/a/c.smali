.class public final Lcom/google/android/gms/drive/realtime/cache/a/c;
.super Lcom/google/android/gms/drive/database/model/ae;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/drive/realtime/cache/a/c;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/drive/realtime/cache/a/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/c;->a:Lcom/google/android/gms/drive/realtime/cache/a/c;

    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/drive/realtime/cache/a/d;->a:Lcom/google/android/gms/drive/realtime/cache/a/d;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/c;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ae;-><init>(I)V

    .line 34
    return-void
.end method

.method public static a()Lcom/google/android/gms/drive/realtime/cache/a/c;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/a/c;->a:Lcom/google/android/gms/drive/realtime/cache/a/c;

    return-object v0
.end method

.method static synthetic d()Lcom/google/android/gms/drive/realtime/cache/a/c;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/a/c;->a:Lcom/google/android/gms/drive/realtime/cache/a/c;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/google/android/gms/drive/realtime/cache/a/c;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/realtime/cache/a/c;->b:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/realtime/cache/a/c;->f()Ljava/lang/String;

    move-result-object v7

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 71
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 72
    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/d;->a:Lcom/google/android/gms/drive/realtime/cache/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/a/d;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/drive/realtime/cache/a/c;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 74
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/gms/drive/realtime/cache/a/c;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "MutationHistory"

    return-object v0
.end method

.method public final c(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/drive/realtime/cache/a/c;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final synthetic c()[Lcom/google/android/gms/drive/g/ak;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/d;->values()[Lcom/google/android/gms/drive/realtime/cache/a/d;

    move-result-object v0

    return-object v0
.end method
