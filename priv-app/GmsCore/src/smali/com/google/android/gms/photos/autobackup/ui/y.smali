.class public final Lcom/google/android/gms/photos/autobackup/ui/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/ui/aj;


# instance fields
.field a:Lcom/google/android/gms/photos/autobackup/ui/ak;

.field private b:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>(Landroid/preference/PreferenceActivity;)V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget v0, Lcom/google/android/gms/p;->sp:I

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/y;->b:Landroid/preference/CheckBoxPreference;

    .line 19
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/y;->b:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/z;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/z;-><init>(Lcom/google/android/gms/photos/autobackup/ui/y;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/photos/autobackup/ui/ak;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/y;->a:Lcom/google/android/gms/photos/autobackup/ui/ak;

    .line 31
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/y;->b:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 36
    return-void
.end method
