.class public final Lcom/google/android/gms/plus/service/v1whitelisted/i;
.super Lcom/google/android/gms/common/server/j;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/common/server/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/n;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/common/server/j;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    .line 35
    return-void
.end method

.method public static a(Lcom/google/android/gms/plus/service/v1whitelisted/j;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 222
    const-string v0, "people/%1$s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 223
    if-eqz p0, :cond_0

    .line 224
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/j;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 226
    :cond_0
    if-eqz p2, :cond_1

    .line 227
    const-string v1, "onBehalfOf"

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 229
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;
    .locals 6

    .prologue
    .line 1663
    const-string v0, "people"

    const-string v1, "query"

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz p3, :cond_0

    const-string v0, "language"

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p4, :cond_1

    const-string v0, "maxResults"

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz p5, :cond_2

    const-string v0, "onBehalfOf"

    invoke-static {p5}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz p6, :cond_3

    const-string v0, "pageToken"

    invoke-static {p6}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1664
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 574
    const-string v0, "people/%1$s/people/%2$s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v3, 0x1

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz p4, :cond_0

    const-string v0, "includeObjectType"

    const-string v1, "&includeObjectType="

    invoke-static {v1, p4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p5, :cond_1

    const-string v0, "maxResults"

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz p6, :cond_2

    const-string v0, "orderBy"

    invoke-static {p6}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz p7, :cond_3

    const-string v0, "pageToken"

    invoke-static {p7}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 575
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 279
    invoke-static {v4, p2, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/plus/service/v1whitelisted/j;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 4

    .prologue
    .line 1314
    const-string v0, "people/%1$s/recordSuggestionAction"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "action"

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p4, :cond_0

    const-string v1, "suggestionId"

    invoke-static {p4}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1315
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v1, p1, v0, p5, p6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 1317
    return-void
.end method
