.class public Lcom/google/android/gms/wallet/service/ow/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Landroid/content/Intent;

.field private static final b:Ljava/text/DecimalFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 99
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/ao;->b:Ljava/text/DecimalFormat;

    .line 103
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.ENABLE_WALLET_OPTIMIZATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/ao;->a:Landroid/content/Intent;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/checkout/b/a/c;)I
    .locals 7

    .prologue
    const/16 v0, 0x194

    const/16 v1, 0x8

    .line 463
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    if-eqz v2, :cond_1

    .line 464
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    .line 465
    iget-object v3, v2, Lcom/google/checkout/b/a/b;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 466
    const-string v3, "OwServiceUtils"

    iget-object v4, v2, Lcom/google/checkout/b/a/b;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    :cond_0
    iget-object v3, v2, Lcom/google/checkout/b/a/b;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 469
    const-string v3, "OwServiceUtils"

    iget-object v2, v2, Lcom/google/checkout/b/a/b;->b:Ljava/lang/String;

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    :cond_1
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    if-eqz v2, :cond_4

    .line 474
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    .line 475
    const-string v3, "OwServiceUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Wallet error code: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v2, Lcom/google/aa/b/a/j;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v3, v2, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 478
    const-string v3, "OwServiceUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Wallet error detail: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    :cond_2
    iget-object v3, v2, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    if-eqz v3, :cond_3

    .line 482
    iget-object v3, v2, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    .line 483
    iget-object v4, v3, Lcom/google/aa/b/a/k;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 484
    const-string v4, "OwServiceUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Wallet error user message title: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/aa/b/a/k;->a:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    :cond_3
    iget v2, v2, Lcom/google/aa/b/a/j;->a:I

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    .line 517
    :goto_0
    :sswitch_0
    return v0

    .line 490
    :sswitch_1
    const/16 v0, 0x199

    goto :goto_0

    .line 493
    :sswitch_2
    const/16 v0, 0x198

    goto :goto_0

    .line 497
    :sswitch_3
    const/16 v0, 0x19c

    goto :goto_0

    .line 499
    :sswitch_4
    const/16 v0, 0x195

    goto :goto_0

    .line 501
    :sswitch_5
    const/16 v0, 0x192

    goto :goto_0

    .line 503
    :sswitch_6
    const/16 v0, 0x196

    goto :goto_0

    :cond_4
    move v0, v1

    .line 517
    goto :goto_0

    .line 488
    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x1f -> :sswitch_0
        0x20 -> :sswitch_6
        0x29 -> :sswitch_0
        0x2a -> :sswitch_0
        0x2b -> :sswitch_3
        0x33 -> :sswitch_4
        0x3d -> :sswitch_5
    .end sparse-switch
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;Lcom/google/android/gms/wallet/cache/i;ILjava/lang/String;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 762
    invoke-static/range {p1 .. p7}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;Lcom/google/android/gms/wallet/cache/i;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;ILjava/lang/String;)Landroid/app/PendingIntent;
    .locals 8

    .prologue
    .line 701
    iget-object v0, p4, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    invoke-static {v0, p3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/u;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p4

    move-object v7, p6

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;Lcom/google/android/gms/wallet/cache/i;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/checkout/inapp/proto/j;Lcom/google/aa/b/a/a/a/a/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 881
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_AUTHENTICATE_INSTRUMENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 882
    const-string v1, "com.google.android.gms"

    const-class v2, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 884
    const-string v1, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 886
    const-string v1, "com.google.android.gms.wallet.FULL_WALLET_REQUEST"

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 887
    const-string v1, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 888
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;Lcom/google/android/gms/wallet/cache/i;ILjava/lang/String;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 814
    .line 816
    sget-object v0, Lcom/google/android/gms/wallet/b/h;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 820
    if-eqz p3, :cond_6

    iget-object v2, p3, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 822
    iget-object v2, p3, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    .line 823
    iget-object v1, p3, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    .line 824
    iget-object v3, p3, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    if-eqz v3, :cond_0

    .line 825
    iget-object v0, p3, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-boolean v0, v0, Lcom/google/aa/a/a/a/d;->d:Z

    .line 833
    :cond_0
    :goto_0
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.wallet.onlinewallet.ACTION_GET_MASKED_WALLET"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.gms"

    const-class v5, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p1, :cond_1

    const-string v4, "com.google.android.gms.wallet.EXTRA_REQUEST"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT_ID"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "com.google.android.gms.wallet.EXTRA_SELECTED_ADDRESS_ID"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_4
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "com.google.android.gms.wallet.ANALYTICS_SESSION_ID"

    invoke-virtual {v3, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_5
    const-string v1, "com.google.android.gms.wallet.MASKED_WALLET_FLOW_TYPE"

    invoke-virtual {v3, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v3, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.EXTRA_USE_WALLET_BALANCE_CHECKED"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v3

    .line 827
    :cond_6
    if-eqz p4, :cond_7

    iget-boolean v2, p4, Lcom/google/android/gms/wallet/cache/i;->g:Z

    if-eqz v2, :cond_7

    iget-object v2, p4, Lcom/google/android/gms/wallet/cache/i;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 829
    iget-object v2, p4, Lcom/google/android/gms/wallet/cache/i;->d:Ljava/lang/String;

    .line 830
    iget-object v1, p4, Lcom/google/android/gms/wallet/cache/i;->f:Ljava/lang/String;

    .line 831
    iget-boolean v0, p4, Lcom/google/android/gms/wallet/cache/i;->e:Z

    goto :goto_0

    :cond_7
    move-object v2, v1

    goto :goto_0
.end method

.method public static a(Landroid/app/PendingIntent;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 712
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 713
    const-string v1, "com.google.android.gms.wallet.EXTRA_PENDING_INTENT"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 714
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 12

    .prologue
    .line 917
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 918
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 920
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->f()Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    move-result-object v0

    .line 924
    :goto_0
    invoke-static {p2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v2

    new-instance v3, Lcom/google/aa/b/a/a/a/a/g;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/g;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v4

    new-instance v5, Lcom/google/j/a/a/a/j;

    invoke-direct {v5}, Lcom/google/j/a/a/a/j;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->f()Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/wallet/service/ow/ap;->a(Lcom/google/android/gms/wallet/wobs/CommonWalletObject;)Lcom/google/j/a/a/a/ac;

    move-result-object v6

    iput-object v6, v5, Lcom/google/j/a/a/a/j;->a:Lcom/google/j/a/a/a/ac;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->d()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/j/a/a/a/j;->b:Ljava/lang/String;

    :cond_0
    invoke-virtual {v4}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->c()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/j/a/a/a/j;->c:Ljava/lang/String;

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->e()Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->e()Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;

    move-result-object v4

    new-instance v6, Lcom/google/j/a/a/a/k;

    invoke-direct {v6}, Lcom/google/j/a/a/a/k;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->b()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/j/a/a/a/k;->a:Ljava/lang/String;

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->c()Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->c()Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->g()I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_3

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->c()Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;

    move-result-object v7

    new-instance v8, Lcom/google/j/a/a/a/l;

    invoke-direct {v8}, Lcom/google/j/a/a/a/l;-><init>()V

    invoke-virtual {v7}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->g()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :goto_1
    iput-object v8, v6, Lcom/google/j/a/a/a/k;->b:Lcom/google/j/a/a/a/l;

    :cond_3
    invoke-virtual {v4}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->d()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/j/a/a/a/k;->c:Ljava/lang/String;

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->e()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/wobs/LoyaltyPoints;->e()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/wallet/service/ow/ap;->a(Lcom/google/android/gms/wallet/wobs/TimeInterval;)Lcom/google/j/a/a/a/y;

    move-result-object v4

    iput-object v4, v6, Lcom/google/j/a/a/a/k;->d:Lcom/google/j/a/a/a/y;

    :cond_5
    iput-object v6, v5, Lcom/google/j/a/a/a/j;->d:Lcom/google/j/a/a/a/k;

    :cond_6
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/j/a/a/a/j;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    iput-object v4, v3, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    :cond_7
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.google.android.gms.wallet.onlinewallet.ACTION_CREATE_WALLET_OBJECTS"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "com.google.android.gms"

    const-class v7, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "com.google.android.gms.wallet.CREATE_WALLET_OBJECTS_REQUEST"

    invoke-static {v5, v6, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    const-string v3, "com.google.android.gms.wallet.WOBS_ISSUER_NAME"

    invoke-virtual {v5, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.gms.wallet.WOBS_OBJECT_NAME"

    invoke-virtual {v5, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p0, v5, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 929
    const-string v2, "com.google.android.gms.wallet.EXTRA_PENDING_INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 930
    return-object v1

    .line 922
    :cond_8
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->c()Lcom/google/android/gms/wallet/OfferWalletObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/OfferWalletObject;->c()Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    move-result-object v0

    goto/16 :goto_0

    .line 924
    :pswitch_0
    invoke-virtual {v7}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->b()I

    move-result v7

    iput v7, v8, Lcom/google/j/a/a/a/l;->b:I

    goto :goto_1

    :pswitch_1
    invoke-virtual {v7}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->c()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, Lcom/google/j/a/a/a/l;->a:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    invoke-virtual {v7}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->d()D

    move-result-wide v10

    iput-wide v10, v8, Lcom/google/j/a/a/a/l;->c:D

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual {v7}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7}, Lcom/google/android/gms/wallet/wobs/LoyaltyPointsBalance;->f()J

    move-result-wide v10

    new-instance v7, Lcom/google/j/a/a/a/n;

    invoke-direct {v7}, Lcom/google/j/a/a/a/n;-><init>()V

    iput-wide v10, v7, Lcom/google/j/a/a/a/n;->a:J

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9

    iput-object v9, v7, Lcom/google/j/a/a/a/n;->b:Ljava/lang/String;

    :cond_9
    iput-object v7, v8, Lcom/google/j/a/a/a/l;->d:Lcom/google/j/a/a/a/n;

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->c()Lcom/google/android/gms/wallet/OfferWalletObject;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->c()Lcom/google/android/gms/wallet/OfferWalletObject;

    move-result-object v4

    new-instance v5, Lcom/google/j/a/a/a/p;

    invoke-direct {v5}, Lcom/google/j/a/a/a/p;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/OfferWalletObject;->c()Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/wallet/service/ow/ap;->a(Lcom/google/android/gms/wallet/wobs/CommonWalletObject;)Lcom/google/j/a/a/a/ac;

    move-result-object v4

    iput-object v4, v5, Lcom/google/j/a/a/a/p;->a:Lcom/google/j/a/a/a/ac;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/j/a/a/a/p;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    iput-object v4, v3, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Lcom/google/android/gms/wallet/cache/i;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 674
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 675
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, p3

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;Lcom/google/android/gms/wallet/cache/i;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 678
    const-string v1, "com.google.android.gms.wallet.EXTRA_PENDING_INTENT"

    invoke-virtual {v8, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 679
    return-object v8
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 8

    .prologue
    .line 686
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 687
    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 693
    const-string v1, "com.google.android.gms.wallet.EXTRA_PENDING_INTENT"

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 694
    return-object v7
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILjava/lang/String;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 840
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 841
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 842
    const-string v2, "com.google.android.gms.wallet.fragment.ACTION_FRAGMENT_GET_MASKED_WALLET"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 843
    const-string v2, "com.google.android.gms"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 844
    const-string v2, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 845
    const-string v2, "com.google.android.gms.wallet.EXTRA_PARAMETERS"

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 847
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 848
    const-string v2, "com.google.android.gms.wallet.ANALYTICS_SESSION_ID"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 850
    :cond_0
    const-string v2, "com.google.android.gms.wallet.MASKED_WALLET_FLOW_TYPE"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 851
    const-string v2, "com.google.android.gms.wallet.INTENT"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 852
    return-object v0
.end method

.method public static a(Lcom/google/aa/b/a/a/a/a/p;)Lcom/google/aa/a/a/a/f;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 998
    new-instance v2, Lcom/google/aa/a/a/a/f;

    invoke-direct {v2}, Lcom/google/aa/a/a/a/f;-><init>()V

    .line 999
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    .line 1000
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    array-length v4, v1

    .line 1001
    new-array v1, v4, [Lcom/google/checkout/inapp/proto/j;

    iput-object v1, v2, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    move v1, v0

    .line 1002
    :goto_0
    if-ge v1, v4, :cond_1

    .line 1003
    iget-object v5, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    aget-object v5, v5, v1

    .line 1004
    invoke-static {v5}, Lcom/google/android/gms/wallet/common/z;->a(Lcom/google/aa/b/a/a/a/a/w;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v5

    .line 1007
    iget-object v6, v5, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1008
    iput-boolean v7, v5, Lcom/google/checkout/inapp/proto/j;->f:Z

    .line 1010
    :cond_0
    iget-object v6, v2, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    aput-object v5, v6, v1

    .line 1002
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1012
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    .line 1013
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v3, v3

    .line 1014
    new-array v4, v3, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v4, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    .line 1015
    :goto_1
    if-ge v0, v3, :cond_3

    .line 1016
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v4, v4, v0

    .line 1017
    iget-object v5, v4, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1018
    iput-boolean v7, v4, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    .line 1020
    :cond_2
    iget-object v5, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    aput-object v4, v5, v0

    .line 1015
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1022
    :cond_3
    iput-object p0, v2, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    .line 1023
    return-object v2
.end method

.method public static a(Lcom/google/aa/b/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/aa/b/a/e;Ljava/lang/String;Z)Lcom/google/aa/b/a/a/a/a/i;
    .locals 3

    .prologue
    .line 338
    new-instance v0, Lcom/google/aa/b/a/a/a/a/i;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/i;-><init>()V

    .line 339
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    iput-boolean v1, v0, Lcom/google/aa/b/a/a/a/a/i;->i:Z

    .line 340
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    iput-boolean v1, v0, Lcom/google/aa/b/a/a/a/a/i;->h:Z

    .line 341
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    iput-boolean v1, v0, Lcom/google/aa/b/a/a/a/a/i;->t:Z

    .line 342
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    iput-boolean v1, v0, Lcom/google/aa/b/a/a/a/a/i;->u:Z

    .line 343
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    iput-boolean v1, v0, Lcom/google/aa/b/a/a/a/a/i;->v:Z

    .line 344
    iput-boolean p3, v0, Lcom/google/aa/b/a/a/a/a/i;->w:Z

    .line 345
    iput-boolean p4, v0, Lcom/google/aa/b/a/a/a/a/i;->x:Z

    .line 346
    iput-boolean p7, v0, Lcom/google/aa/b/a/a/a/a/i;->y:Z

    .line 347
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 348
    iput-object p1, v0, Lcom/google/aa/b/a/a/a/a/i;->b:Ljava/lang/String;

    .line 350
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 351
    iput-object p2, v0, Lcom/google/aa/b/a/a/a/a/i;->c:Ljava/lang/String;

    .line 353
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 354
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    .line 356
    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 357
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/i;->g:Ljava/lang/String;

    .line 359
    :cond_3
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 360
    iput-object p6, v0, Lcom/google/aa/b/a/a/a/a/i;->q:Ljava/lang/String;

    .line 362
    :cond_4
    if-eqz p5, :cond_5

    .line 363
    iput-object p5, v0, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    .line 365
    :cond_5
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    if-eqz v1, :cond_6

    .line 366
    new-instance v1, Lcom/google/aa/b/a/d;

    invoke-direct {v1}, Lcom/google/aa/b/a/d;-><init>()V

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    .line 367
    iget-object v1, v0, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/aa/b/a/d;->a:Ljava/lang/String;

    .line 369
    :cond_6
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    array-length v1, v1

    if-lez v1, :cond_7

    .line 370
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/i;->r:[Lcom/google/aa/b/a/a/a/a/x;

    .line 373
    :cond_7
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 374
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/i;->s:Ljava/lang/String;

    .line 376
    :cond_8
    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/FullWalletRequest;Lcom/google/android/gms/wallet/service/ow/as;)Lcom/google/aa/b/a/a/a/a/i;
    .locals 4

    .prologue
    .line 387
    iget-object v0, p1, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    .line 388
    new-instance v1, Lcom/google/aa/b/a/a/a/a/i;

    invoke-direct {v1}, Lcom/google/aa/b/a/a/a/a/i;-><init>()V

    .line 389
    iget-object v2, p1, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/i;->b:Ljava/lang/String;

    .line 390
    iget-object v2, p1, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-boolean v2, v2, Lcom/google/aa/a/a/a/d;->d:Z

    iput-boolean v2, v1, Lcom/google/aa/b/a/a/a/a/i;->y:Z

    .line 392
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    .line 393
    iget-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    iput-boolean v2, v1, Lcom/google/aa/b/a/a/a/a/i;->h:Z

    .line 394
    iget-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    iput-boolean v2, v1, Lcom/google/aa/b/a/a/a/a/i;->i:Z

    .line 395
    iget-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    iput-boolean v2, v1, Lcom/google/aa/b/a/a/a/a/i;->t:Z

    .line 396
    iget-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    iput-boolean v2, v1, Lcom/google/aa/b/a/a/a/a/i;->u:Z

    .line 397
    iget-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    iput-boolean v2, v1, Lcom/google/aa/b/a/a/a/a/i;->v:Z

    .line 398
    iget-boolean v2, p1, Lcom/google/android/gms/wallet/service/ow/as;->g:Z

    iput-boolean v2, v1, Lcom/google/aa/b/a/a/a/a/i;->m:Z

    .line 400
    iget-boolean v2, p1, Lcom/google/android/gms/wallet/service/ow/as;->i:Z

    iput-boolean v2, v1, Lcom/google/aa/b/a/a/a/a/i;->w:Z

    .line 401
    iget-boolean v2, p1, Lcom/google/android/gms/wallet/service/ow/as;->j:Z

    iput-boolean v2, v1, Lcom/google/aa/b/a/a/a/a/i;->x:Z

    .line 404
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->d()Lcom/google/android/gms/wallet/Cart;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 405
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->d()Lcom/google/android/gms/wallet/Cart;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/Cart;)Lcom/google/aa/b/a/e;

    move-result-object v2

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    .line 407
    :cond_0
    iget-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    if-eqz v2, :cond_1

    .line 408
    new-instance v2, Lcom/google/aa/b/a/d;

    invoke-direct {v2}, Lcom/google/aa/b/a/d;-><init>()V

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    .line 409
    iget-object v2, v1, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    iget-object v3, v0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/aa/b/a/d;->a:Ljava/lang/String;

    .line 411
    :cond_1
    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 412
    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/i;->r:[Lcom/google/aa/b/a/a/a/a/x;

    .line 415
    :cond_2
    iget-object v2, p1, Lcom/google/android/gms/wallet/service/ow/as;->h:[Ljava/lang/String;

    .line 416
    if-eqz v2, :cond_3

    array-length v3, v2

    if-lez v3, :cond_3

    .line 417
    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/i;->l:[Ljava/lang/String;

    .line 419
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 420
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/i;->g:Ljava/lang/String;

    .line 423
    :cond_4
    iget-object v2, p1, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 424
    iget-object v2, p1, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/i;->c:Ljava/lang/String;

    .line 426
    :cond_5
    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 427
    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/aa/b/a/a/a/a/i;->s:Ljava/lang/String;

    .line 429
    :cond_6
    return-object v1
.end method

.method public static a()Lcom/google/aa/b/a/a/a/a/t;
    .locals 3

    .prologue
    .line 958
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 959
    new-instance v1, Lcom/google/aa/b/a/a/a/a/t;

    invoke-direct {v1}, Lcom/google/aa/b/a/a/a/a/t;-><init>()V

    .line 960
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/t;->a:Ljava/lang/String;

    .line 961
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/aa/b/a/a/a/a/t;->b:Ljava/lang/String;

    .line 962
    return-object v1
.end method

.method public static a(Lcom/google/aa/b/a/a/a/a/i;)Lcom/google/aa/b/a/a/a/a/u;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1028
    new-instance v2, Lcom/google/aa/b/a/a/a/a/u;

    invoke-direct {v2}, Lcom/google/aa/b/a/a/a/a/u;-><init>()V

    .line 1029
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_0
    iput-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    .line 1030
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/i;->i:Z

    iput-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    .line 1031
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/i;->h:Z

    iput-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    .line 1032
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/i;->u:Z

    iput-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    .line 1033
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/i;->v:Z

    iput-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    .line 1034
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-eqz v0, :cond_0

    .line 1035
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->a:Lcom/google/aa/b/a/a/a/a/b;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    .line 1037
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1038
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    .line 1040
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1041
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->g:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    .line 1043
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1044
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->s:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    .line 1046
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1047
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->z:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    .line 1049
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    if-eqz v0, :cond_8

    .line 1053
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    iget-object v0, v0, Lcom/google/aa/b/a/e;->a:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    .line 1054
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    iget-object v0, v0, Lcom/google/aa/b/a/e;->b:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    .line 1060
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->r:[Lcom/google/aa/b/a/a/a/a/x;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 1061
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->r:[Lcom/google/aa/b/a/a/a/a/x;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    .line 1064
    :cond_6
    return-object v2

    .line 1029
    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    .line 1055
    :cond_8
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    if-eqz v0, :cond_5

    .line 1056
    iput-boolean v1, v2, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    .line 1057
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    iget-object v0, v0, Lcom/google/aa/b/a/d;->a:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/aa/b/a/a/a/a/u;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 621
    new-instance v0, Lcom/google/aa/b/a/a/a/a/u;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/u;-><init>()V

    .line 622
    iput-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    .line 623
    if-eqz p0, :cond_1

    .line 624
    const-string v1, "USD"

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    .line 625
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    .line 626
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    .line 627
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    .line 628
    iput-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    .line 629
    iput-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    .line 630
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 631
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    .line 633
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 634
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Ljava/util/ArrayList;)[Lcom/google/aa/b/a/a/a/a/x;

    move-result-object v1

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    .line 638
    :cond_1
    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/b;)Lcom/google/aa/b/a/a/a/a/u;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 584
    new-instance v3, Lcom/google/aa/b/a/a/a/a/u;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/u;-><init>()V

    .line 585
    if-eqz p0, :cond_6

    .line 586
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 587
    iput-object p1, v3, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    .line 589
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 590
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    .line 592
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 593
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    .line 595
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 596
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    .line 598
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 599
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    .line 601
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->p()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 602
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->p()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Ljava/util/ArrayList;)[Lcom/google/aa/b/a/a/a/a/x;

    move-result-object v0

    iput-object v0, v3, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    .line 605
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->d()Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    .line 606
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->e()Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    .line 607
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->f()Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    .line 608
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->k()Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    .line 609
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->l()Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    .line 610
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    .line 611
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->o()Z

    move-result v0

    if-nez v0, :cond_9

    :goto_1
    iput-boolean v1, v3, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    .line 613
    :cond_6
    if-eqz p2, :cond_7

    .line 614
    iput-object p2, v3, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    .line 616
    :cond_7
    return-object v3

    :cond_8
    move v0, v2

    .line 610
    goto :goto_0

    :cond_9
    move v1, v2

    .line 611
    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/wallet/Cart;)Lcom/google/aa/b/a/e;
    .locals 8

    .prologue
    .line 521
    new-instance v2, Lcom/google/aa/b/a/e;

    invoke-direct {v2}, Lcom/google/aa/b/a/e;-><init>()V

    .line 523
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 524
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/aa/b/a/e;->a:Ljava/lang/String;

    .line 526
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 527
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/aa/b/a/e;->b:Ljava/lang/String;

    .line 529
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->d()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 530
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->d()Ljava/util/ArrayList;

    move-result-object v3

    .line 531
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 532
    new-array v0, v4, [Lcom/google/aa/b/a/f;

    iput-object v0, v2, Lcom/google/aa/b/a/e;->c:[Lcom/google/aa/b/a/f;

    .line 534
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_7

    .line 535
    iget-object v5, v2, Lcom/google/aa/b/a/e;->c:[Lcom/google/aa/b/a/f;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/LineItem;

    new-instance v6, Lcom/google/aa/b/a/f;

    invoke-direct {v6}, Lcom/google/aa/b/a/f;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->b()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->c()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->d()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->e()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->e()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->g()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->g()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/LineItem;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    aput-object v6, v5, v1

    .line 534
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 535
    :pswitch_0
    const/4 v0, 0x1

    iput v0, v6, Lcom/google/aa/b/a/f;->e:I

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x2

    iput v0, v6, Lcom/google/aa/b/a/f;->e:I

    goto :goto_1

    .line 538
    :cond_7
    return-object v2

    .line 535
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    .line 279
    if-nez p0, :cond_0

    .line 280
    const/4 v0, 0x0

    .line 324
    :goto_0
    return-object v0

    .line 282
    :cond_0
    invoke-static {}, Lcom/google/android/gms/identity/intents/model/UserAddress;->a()Lcom/google/android/gms/identity/intents/model/b;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object v1, v2, Lcom/google/android/gms/identity/intents/model/UserAddress;->l:Ljava/lang/String;

    .line 284
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    iget-object v2, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-boolean v1, v2, Lcom/google/android/gms/identity/intents/model/UserAddress;->m:Z

    .line 285
    iget-object v1, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object p1, v1, Lcom/google/android/gms/identity/intents/model/UserAddress;->o:Ljava/lang/String;

    .line 286
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    .line 287
    if-eqz v1, :cond_1

    .line 288
    iget-object v2, v1, Lcom/google/t/a/b;->s:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object v2, v3, Lcom/google/android/gms/identity/intents/model/UserAddress;->a:Ljava/lang/String;

    .line 289
    iget-object v2, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    array-length v2, v2

    .line 290
    if-lez v2, :cond_2

    .line 291
    iget-object v3, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lcom/google/android/gms/identity/intents/model/b;->a(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    .line 295
    :goto_1
    if-lt v2, v5, :cond_3

    .line 296
    iget-object v3, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lcom/google/android/gms/identity/intents/model/b;->b(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    .line 300
    :goto_2
    if-lt v2, v6, :cond_4

    .line 301
    iget-object v3, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-virtual {v0, v3}, Lcom/google/android/gms/identity/intents/model/b;->c(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    .line 305
    :goto_3
    if-lt v2, v7, :cond_5

    .line 306
    iget-object v3, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-virtual {v0, v3}, Lcom/google/android/gms/identity/intents/model/b;->d(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    .line 310
    :goto_4
    const/4 v3, 0x5

    if-lt v2, v3, :cond_6

    .line 311
    iget-object v2, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    aget-object v2, v2, v7

    invoke-virtual {v0, v2}, Lcom/google/android/gms/identity/intents/model/b;->e(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    .line 315
    :goto_5
    iget-object v2, v1, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object v2, v3, Lcom/google/android/gms/identity/intents/model/UserAddress;->i:Ljava/lang/String;

    .line 316
    iget-object v2, v1, Lcom/google/t/a/b;->f:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object v2, v3, Lcom/google/android/gms/identity/intents/model/UserAddress;->h:Ljava/lang/String;

    .line 317
    iget-object v2, v1, Lcom/google/t/a/b;->d:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object v2, v3, Lcom/google/android/gms/identity/intents/model/UserAddress;->g:Ljava/lang/String;

    .line 319
    iget-object v2, v1, Lcom/google/t/a/b;->k:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object v2, v3, Lcom/google/android/gms/identity/intents/model/UserAddress;->j:Ljava/lang/String;

    .line 320
    iget-object v2, v1, Lcom/google/t/a/b;->m:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object v2, v3, Lcom/google/android/gms/identity/intents/model/UserAddress;->k:Ljava/lang/String;

    .line 321
    iget-object v1, v1, Lcom/google/t/a/b;->r:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    iput-object v1, v2, Lcom/google/android/gms/identity/intents/model/UserAddress;->n:Ljava/lang/String;

    .line 324
    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/identity/intents/model/b;->a:Lcom/google/android/gms/identity/intents/model/UserAddress;

    goto/16 :goto_0

    .line 293
    :cond_2
    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/google/android/gms/identity/intents/model/b;->a(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    goto :goto_1

    .line 298
    :cond_3
    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/google/android/gms/identity/intents/model/b;->b(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    goto :goto_2

    .line 303
    :cond_4
    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/google/android/gms/identity/intents/model/b;->c(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    goto :goto_3

    .line 308
    :cond_5
    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/google/android/gms/identity/intents/model/b;->d(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    goto :goto_4

    .line 313
    :cond_6
    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/google/android/gms/identity/intents/model/b;->e(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/b;

    goto :goto_5
.end method

.method public static a(Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/Address;
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 242
    if-nez p0, :cond_0

    .line 243
    const/4 v0, 0x0

    .line 274
    :goto_0
    return-object v0

    .line 245
    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/Address;->a()Lcom/google/android/gms/wallet/a;

    move-result-object v0

    .line 246
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/Address;

    iput-object v1, v2, Lcom/google/android/gms/wallet/Address;->i:Ljava/lang/String;

    .line 247
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    iget-object v2, v0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/Address;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/Address;->j:Z

    .line 248
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    .line 249
    if-eqz v1, :cond_1

    .line 250
    iget-object v2, v1, Lcom/google/t/a/b;->s:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/Address;

    iput-object v2, v3, Lcom/google/android/gms/wallet/Address;->a:Ljava/lang/String;

    .line 251
    iget-object v2, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    array-length v2, v2

    .line 252
    if-lez v2, :cond_2

    .line 253
    iget-object v3, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/a;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;

    .line 257
    :goto_1
    if-lt v2, v5, :cond_3

    .line 258
    iget-object v3, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/a;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;

    .line 262
    :goto_2
    const/4 v3, 0x3

    if-lt v2, v3, :cond_4

    .line 263
    iget-object v2, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/a;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;

    .line 267
    :goto_3
    iget-object v2, v1, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/Address;

    iput-object v2, v3, Lcom/google/android/gms/wallet/Address;->e:Ljava/lang/String;

    .line 268
    iget-object v2, v1, Lcom/google/t/a/b;->f:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/Address;

    iput-object v2, v3, Lcom/google/android/gms/wallet/Address;->f:Ljava/lang/String;

    .line 269
    iget-object v2, v1, Lcom/google/t/a/b;->d:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/Address;

    iput-object v2, v3, Lcom/google/android/gms/wallet/Address;->g:Ljava/lang/String;

    .line 270
    iget-object v2, v1, Lcom/google/t/a/b;->k:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/Address;

    iput-object v2, v3, Lcom/google/android/gms/wallet/Address;->h:Ljava/lang/String;

    .line 271
    iget-object v1, v1, Lcom/google/t/a/b;->r:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/Address;

    iput-object v1, v2, Lcom/google/android/gms/wallet/Address;->k:Ljava/lang/String;

    .line 274
    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/wallet/a;->a:Lcom/google/android/gms/wallet/Address;

    goto :goto_0

    .line 255
    :cond_2
    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/a;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;

    goto :goto_1

    .line 260
    :cond_3
    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/a;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;

    goto :goto_2

    .line 265
    :cond_4
    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/a;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;

    goto :goto_3
.end method

.method public static a(Lcom/google/aa/b/a/a/a/a/j;Lcom/google/aa/b/a/a/a/a/i;Ljava/lang/String;J)Lcom/google/android/gms/wallet/FullWallet;
    .locals 11

    .prologue
    .line 186
    const/4 v1, 0x0

    .line 187
    const/4 v0, 0x0

    .line 188
    if-eqz p1, :cond_0

    .line 189
    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    .line 190
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/i;->g:Ljava/lang/String;

    .line 192
    :cond_0
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    iget v4, p0, Lcom/google/aa/b/a/a/a/a/j;->c:I

    iget v5, p0, Lcom/google/aa/b/a/a/a/a/j;->d:I

    invoke-static {v3}, Lcom/google/android/gms/common/util/am;->a(Ljava/lang/String;)J

    move-result-wide v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v8, "%013d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    xor-long/2addr v6, p3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v9, v10

    invoke-static {v3, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v6, 0x10

    invoke-virtual {v2, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/16 v6, 0x10

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    new-instance v6, Lcom/google/android/gms/wallet/ProxyCard;

    invoke-direct {v6, v3, v2, v4, v5}, Lcom/google/android/gms/wallet/ProxyCard;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lcom/google/android/gms/wallet/f;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/android/gms/wallet/f;->a(Lcom/google/android/gms/wallet/ProxyCard;)Lcom/google/android/gms/wallet/f;

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/f;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/f;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->a(Lcom/google/android/gms/wallet/Address;)Lcom/google/android/gms/wallet/f;

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, p2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->a(Lcom/google/android/gms/identity/intents/model/UserAddress;)Lcom/google/android/gms/wallet/f;

    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->b(Lcom/google/android/gms/wallet/Address;)Lcom/google/android/gms/wallet/f;

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, p2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->b(Lcom/google/android/gms/identity/intents/model/UserAddress;)Lcom/google/android/gms/wallet/f;

    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->a([Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/wallet/f;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    array-length v1, v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a([Lcom/google/aa/b/a/g;)[Lcom/google/android/gms/wallet/InstrumentInfo;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object v1, v2, Lcom/google/android/gms/wallet/FullWallet;->j:[Lcom/google/android/gms/wallet/InstrumentInfo;

    :cond_3
    iget-object v0, v0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    return-object v0
.end method

.method public static a(Lcom/google/aa/b/a/a/a/a/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 108
    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lcom/google/android/gms/wallet/o;

    move-result-object v1

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/o;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/o;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    move-result-object v2

    .line 113
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    iget-object v1, v1, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    iget-object v1, v1, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->e:Lcom/google/android/gms/wallet/Address;

    .line 115
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    iget-object v1, v1, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, p2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->i:Lcom/google/android/gms/identity/intents/model/UserAddress;

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_1

    .line 119
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->f:Lcom/google/android/gms/wallet/Address;

    .line 120
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, p2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->j:Lcom/google/android/gms/identity/intents/model/UserAddress;

    .line 123
    :cond_1
    iget-object v1, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object p2, v1, Lcom/google/android/gms/wallet/MaskedWallet;->d:Ljava/lang/String;

    .line 124
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    iget-object v3, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->c:[Ljava/lang/String;

    .line 125
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 126
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a([Lcom/google/aa/b/a/g;)[Lcom/google/android/gms/wallet/InstrumentInfo;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v1, v3, Lcom/google/android/gms/wallet/MaskedWallet;->k:[Lcom/google/android/gms/wallet/InstrumentInfo;

    .line 128
    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 129
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    array-length v1, v3

    new-array v4, v1, [Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    array-length v5, v3

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_3

    aget-object v6, v3, v1

    invoke-static {}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->a()Lcom/google/android/gms/wallet/m;

    move-result-object v7

    iget-object v8, v6, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    iget-object v9, v7, Lcom/google/android/gms/wallet/m;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->a:Ljava/lang/String;

    iget-object v8, v6, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    iget-object v9, v7, Lcom/google/android/gms/wallet/m;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->b:Ljava/lang/String;

    iget-object v8, v6, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    iget-object v9, v7, Lcom/google/android/gms/wallet/m;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->c:Ljava/lang/String;

    iget-object v8, v6, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    iget-object v9, v7, Lcom/google/android/gms/wallet/m;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->d:Ljava/lang/String;

    iget-object v8, v6, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    iget-object v9, v7, Lcom/google/android/gms/wallet/m;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->e:Ljava/lang/String;

    iget-object v8, v6, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    iget-object v9, v7, Lcom/google/android/gms/wallet/m;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->f:Ljava/lang/String;

    iget-object v8, v6, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    iget-object v9, v7, Lcom/google/android/gms/wallet/m;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v8, v9, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->g:Ljava/lang/String;

    iget-object v6, v6, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    iget-object v8, v7, Lcom/google/android/gms/wallet/m;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    iput-object v6, v8, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->h:Ljava/lang/String;

    iget-object v6, v7, Lcom/google/android/gms/wallet/m;->a:Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    aput-object v6, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v4, v1, Lcom/google/android/gms/wallet/MaskedWallet;->g:[Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    .line 132
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 133
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    array-length v3, v1

    new-array v3, v3, [Lcom/google/android/gms/wallet/OfferWalletObject;

    array-length v4, v1

    :goto_1
    if-ge v0, v4, :cond_5

    aget-object v5, v1, v0

    invoke-static {}, Lcom/google/android/gms/wallet/OfferWalletObject;->a()Lcom/google/android/gms/wallet/t;

    move-result-object v6

    iget-object v7, v5, Lcom/google/aa/b/a/i;->a:Ljava/lang/String;

    iget-object v8, v6, Lcom/google/android/gms/wallet/t;->a:Lcom/google/android/gms/wallet/wobs/a;

    invoke-virtual {v8, v7}, Lcom/google/android/gms/wallet/wobs/a;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/wobs/a;

    iget-object v8, v6, Lcom/google/android/gms/wallet/t;->b:Lcom/google/android/gms/wallet/OfferWalletObject;

    iput-object v7, v8, Lcom/google/android/gms/wallet/OfferWalletObject;->a:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/aa/b/a/i;->b:Ljava/lang/String;

    iget-object v7, v6, Lcom/google/android/gms/wallet/t;->b:Lcom/google/android/gms/wallet/OfferWalletObject;

    iput-object v5, v7, Lcom/google/android/gms/wallet/OfferWalletObject;->b:Ljava/lang/String;

    iget-object v5, v6, Lcom/google/android/gms/wallet/t;->b:Lcom/google/android/gms/wallet/OfferWalletObject;

    iget-object v7, v6, Lcom/google/android/gms/wallet/t;->a:Lcom/google/android/gms/wallet/wobs/a;

    iget-object v7, v7, Lcom/google/android/gms/wallet/wobs/a;->a:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    iput-object v7, v5, Lcom/google/android/gms/wallet/OfferWalletObject;->c:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    iget-object v5, v6, Lcom/google/android/gms/wallet/t;->b:Lcom/google/android/gms/wallet/OfferWalletObject;

    aput-object v5, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v0, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v3, v0, Lcom/google/android/gms/wallet/MaskedWallet;->h:[Lcom/google/android/gms/wallet/OfferWalletObject;

    .line 135
    :cond_6
    iget-object v0, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    return-object v0
.end method

.method public static a(Lcom/google/aa/b/a/a/a/a/u;)Lcom/google/android/gms/wallet/MaskedWalletRequest;
    .locals 1

    .prologue
    .line 719
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/u;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/aa/b/a/a/a/a/u;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWalletRequest;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 724
    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->a()Lcom/google/android/gms/wallet/q;

    move-result-object v3

    .line 725
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 726
    invoke-virtual {v3, p1}, Lcom/google/android/gms/wallet/q;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/q;

    .line 730
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 731
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/q;

    .line 733
    :cond_1
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->e(Z)Lcom/google/android/gms/wallet/q;

    .line 734
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 735
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/q;

    .line 737
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 738
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->d(Ljava/lang/String;)Lcom/google/android/gms/wallet/q;

    .line 740
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    array-length v4, v0

    .line 741
    if-lez v4, :cond_5

    move v0, v2

    .line 742
    :goto_1
    if-ge v0, v4, :cond_5

    .line 743
    iget-object v5, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/google/aa/b/a/a/a/a/x;->a:Ljava/lang/String;

    .line 745
    new-instance v6, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-direct {v6, v5}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Lcom/google/android/gms/wallet/q;->a(Lcom/google/android/gms/identity/intents/model/CountrySpecification;)Lcom/google/android/gms/wallet/q;

    .line 742
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 727
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 728
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/q;

    goto :goto_0

    .line 749
    :cond_5
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->a(Z)Lcom/google/android/gms/wallet/q;

    .line 750
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->b(Z)Lcom/google/android/gms/wallet/q;

    .line 751
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->c(Z)Lcom/google/android/gms/wallet/q;

    .line 752
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->d(Z)Lcom/google/android/gms/wallet/q;

    .line 753
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/q;->f(Z)Lcom/google/android/gms/wallet/q;

    .line 754
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    if-nez v0, :cond_7

    :goto_3
    invoke-virtual {v3, v1}, Lcom/google/android/gms/wallet/q;->g(Z)Lcom/google/android/gms/wallet/q;

    .line 755
    iget-object v0, v3, Lcom/google/android/gms/wallet/q;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    return-object v0

    :cond_6
    move v0, v2

    .line 753
    goto :goto_2

    :cond_7
    move v1, v2

    .line 754
    goto :goto_3
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 3

    .prologue
    .line 859
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 860
    const-class v1, Lcom/google/android/gms/wallet/service/ow/ao;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 861
    const-string v1, "androidPackageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 862
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/c;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    .line 863
    invoke-static {}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a()Lcom/google/android/gms/wallet/shared/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/shared/d;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/shared/d;->d(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v1

    const-string v2, "onlinewallet"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/shared/d;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/shared/d;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/d;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;ILandroid/accounts/Account;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 649
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/16 v0, 0x15

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 652
    :goto_0
    if-eqz v0, :cond_2

    const-string v0, "https://www.googleapis.com/auth/paymentssandbox.make_payments"

    .line 654
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "oauth2:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "callerUid"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "androidPackageName"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v2, v0, v1}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 649
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 652
    :cond_2
    const-string v0, "https://www.googleapis.com/auth/payments.make_payments"

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    .line 934
    new-instance v1, Ljava/math/BigDecimal;

    sget-object v0, Lcom/google/android/gms/wallet/b/c;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-double v2, v0

    div-double/2addr v2, v6

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    .line 936
    new-instance v2, Ljava/math/BigDecimal;

    sget-object v0, Lcom/google/android/gms/wallet/b/c;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-double v4, v0

    div-double/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    .line 938
    new-instance v3, Ljava/math/BigDecimal;

    sget-object v0, Lcom/google/android/gms/wallet/b/c;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/math/BigDecimal;-><init>(I)V

    .line 940
    const/4 v0, 0x0

    .line 941
    if-eqz p0, :cond_0

    .line 943
    :try_start_0
    new-instance v4, Ljava/math/BigDecimal;

    invoke-direct {v4, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x2

    sget-object v6, Ljava/math/RoundingMode;->UP:Ljava/math/RoundingMode;

    invoke-virtual {v4, v5, v6}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v2

    .line 949
    sget-object v1, Lcom/google/android/gms/wallet/service/ow/ao;->b:Ljava/text/DecimalFormat;

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 954
    :cond_0
    :goto_0
    return-object v0

    .line 950
    :catch_0
    move-exception v1

    .line 951
    const-string v2, "OwServiceUtils"

    const-string v3, "Exception parsing masked wallet price"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 973
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 974
    if-eqz p0, :cond_0

    .line 975
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 976
    if-eqz p1, :cond_0

    .line 977
    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 978
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 979
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 983
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/cache/f;Ljava/lang/String;ILcom/google/aa/a/a/a/f;)V
    .locals 1

    .prologue
    .line 1084
    invoke-virtual {p0, p2, p1}, Lcom/google/android/gms/wallet/cache/f;->a(ILjava/lang/String;)Lcom/google/android/gms/wallet/cache/g;

    move-result-object v0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/f;Ljava/lang/String;ILcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/g;)V

    .line 1086
    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/cache/f;Ljava/lang/String;ILcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/g;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1091
    iget-object v2, p3, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    .line 1092
    const/4 v0, 0x0

    .line 1093
    iget-boolean v3, v2, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    iget-boolean v4, p4, Lcom/google/android/gms/wallet/cache/g;->b:Z

    if-eq v3, v4, :cond_0

    .line 1094
    iget-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    iput-boolean v0, p4, Lcom/google/android/gms/wallet/cache/g;->b:Z

    move v0, v1

    .line 1097
    :cond_0
    iget-object v3, v2, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v2, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    iget-object v4, p4, Lcom/google/android/gms/wallet/cache/g;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1099
    iget-object v0, v2, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    iput-object v0, p4, Lcom/google/android/gms/wallet/cache/g;->c:Ljava/lang/String;

    move v0, v1

    .line 1102
    :cond_1
    iget-boolean v3, v2, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    iget-boolean v4, p4, Lcom/google/android/gms/wallet/cache/g;->d:Z

    if-eq v3, v4, :cond_2

    .line 1103
    iget-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    iput-boolean v0, p4, Lcom/google/android/gms/wallet/cache/g;->d:Z

    move v0, v1

    .line 1106
    :cond_2
    iget-boolean v3, v2, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    iget-boolean v4, p4, Lcom/google/android/gms/wallet/cache/g;->e:Z

    if-eq v3, v4, :cond_4

    .line 1108
    iget-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    iput-boolean v0, p4, Lcom/google/android/gms/wallet/cache/g;->e:Z

    .line 1112
    :goto_0
    if-eqz v1, :cond_3

    .line 1113
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/f;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {p2, p1}, Lcom/google/android/gms/wallet/cache/f;->b(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Lcom/google/android/gms/wallet/cache/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1115
    :cond_3
    return-void

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/wallet/cache/h;Landroid/accounts/Account;Ljava/lang/String;ILcom/google/aa/a/a/a/f;)V
    .locals 6

    .prologue
    .line 1120
    invoke-virtual {p0, p3, p1, p2}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/i;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/h;Landroid/accounts/Account;Ljava/lang/String;ILcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/i;)V

    .line 1123
    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/cache/h;Landroid/accounts/Account;Ljava/lang/String;ILcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/i;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1129
    if-eqz p5, :cond_3

    iget-object v0, p5, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p5, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    const-string v1, "dont_send_loyalty_wob_id"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p4, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v1, 0x0

    iget-object v5, p4, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v0, v5, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v6, v0

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_b

    iget-object v0, v5, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    aget-object v0, v0, v4

    iget-object v7, p5, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    iget-object v8, v0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :goto_1
    if-eqz v0, :cond_3

    if-eqz v4, :cond_3

    new-array v7, v6, [Lcom/google/aa/b/a/h;

    aput-object v0, v7, v2

    move v1, v3

    :goto_2
    if-ge v1, v6, :cond_2

    iget-object v8, v5, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    if-gt v1, v4, :cond_1

    add-int/lit8 v0, v1, -0x1

    :goto_3
    aget-object v0, v8, v0

    aput-object v0, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_3

    :cond_2
    iput-object v7, v5, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    .line 1130
    :cond_3
    iget-object v1, p4, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    .line 1131
    iget-object v0, v1, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v4, v0

    .line 1132
    new-array v5, v4, [Lcom/google/aa/b/a/h;

    move v0, v2

    .line 1133
    :goto_4
    if-ge v0, v4, :cond_4

    .line 1134
    iget-object v6, v1, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    aget-object v6, v6, v0

    aput-object v6, v5, v0

    .line 1133
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1136
    :cond_4
    iget-object v1, p5, Lcom/google/android/gms/wallet/cache/i;->c:[Lcom/google/aa/b/a/h;

    if-eq v1, v5, :cond_a

    if-nez v1, :cond_6

    move v0, v2

    :goto_5
    if-nez v0, :cond_5

    .line 1137
    iput-object v5, p5, Lcom/google/android/gms/wallet/cache/i;->c:[Lcom/google/aa/b/a/h;

    .line 1138
    invoke-virtual {p0, p3, p1, p2, p5}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;Lcom/google/android/gms/wallet/cache/i;)V

    .line 1141
    :cond_5
    return-void

    .line 1136
    :cond_6
    array-length v0, v1

    array-length v4, v5

    if-eq v0, v4, :cond_7

    move v0, v2

    goto :goto_5

    :cond_7
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v0, v4, :cond_8

    move v0, v2

    goto :goto_5

    :cond_8
    array-length v4, v1

    move v0, v2

    :goto_6
    if-ge v0, v4, :cond_a

    aget-object v6, v1, v0

    aget-object v7, v5, v0

    invoke-static {v6, v7}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Z

    move-result v6

    if-nez v6, :cond_9

    move v0, v2

    goto :goto_5

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    move v0, v3

    goto :goto_5

    :cond_b
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/wallet/cache/j;Landroid/accounts/Account;ILcom/google/aa/a/a/a/f;ZZZ)V
    .locals 1

    .prologue
    .line 1070
    iget-object v0, p3, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1071
    const/4 v0, 0x0

    .line 1072
    if-nez p4, :cond_0

    .line 1073
    const/4 v0, 0x2

    .line 1075
    :cond_0
    if-nez p5, :cond_1

    if-nez p6, :cond_2

    .line 1076
    :cond_1
    or-int/lit8 v0, v0, 0x1

    .line 1078
    :cond_2
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/aa/a/a/a/f;I)V

    .line 1080
    :cond_3
    return-void
.end method

.method public static a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 642
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 967
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 968
    sget-object v2, Lcom/google/android/gms/wallet/service/ow/ao;->a:Landroid/content/Intent;

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static a(Lcom/google/checkout/b/a/c;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 437
    .line 438
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    .line 439
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    if-eqz v3, :cond_0

    .line 440
    iget-object v3, v2, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    .line 441
    iget v2, v2, Lcom/google/aa/b/a/j;->a:I

    sparse-switch v2, :sswitch_data_0

    .line 454
    :cond_0
    :goto_0
    return v1

    .line 443
    :sswitch_0
    if-nez p1, :cond_1

    iget-object v2, v3, Lcom/google/aa/b/a/k;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v3, Lcom/google/aa/b/a/k;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    move v1, v0

    .line 445
    goto :goto_0

    :cond_1
    move v0, v1

    .line 443
    goto :goto_1

    .line 448
    :sswitch_1
    iget-object v2, v3, Lcom/google/aa/b/a/k;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v3, Lcom/google/aa/b/a/k;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    .line 441
    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0x2c -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Ljava/util/ArrayList;)[Lcom/google/aa/b/a/a/a/a/x;
    .locals 5

    .prologue
    .line 573
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 574
    new-array v3, v2, [Lcom/google/aa/b/a/a/a/a/x;

    .line 575
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 576
    new-instance v0, Lcom/google/aa/b/a/a/a/a/x;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/x;-><init>()V

    aput-object v0, v3, v1

    .line 577
    aget-object v4, v3, v1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    invoke-virtual {v0}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/aa/b/a/a/a/a/x;->a:Ljava/lang/String;

    .line 575
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 579
    :cond_0
    return-object v3
.end method

.method private static a([Lcom/google/aa/b/a/g;)[Lcom/google/android/gms/wallet/InstrumentInfo;
    .locals 6

    .prologue
    .line 140
    array-length v0, p0

    new-array v1, v0, [Lcom/google/android/gms/wallet/InstrumentInfo;

    .line 141
    const/4 v0, 0x0

    array-length v2, p0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 142
    new-instance v3, Lcom/google/android/gms/wallet/InstrumentInfo;

    aget-object v4, p0, v0

    iget-object v4, v4, Lcom/google/aa/b/a/g;->a:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aget-object v5, p0, v0

    iget-object v5, v5, Lcom/google/aa/b/a/g;->b:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/wallet/InstrumentInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v0

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    :cond_0
    return-object v1
.end method

.method public static b(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 988
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 989
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 990
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/model/CountrySpecification;

    .line 991
    invoke-virtual {v0}, Lcom/google/android/gms/identity/intents/model/CountrySpecification;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 993
    :cond_0
    return-object v1
.end method
