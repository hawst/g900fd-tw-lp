.class final Lcom/google/android/gms/smart_profile/card/view/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/ViewTreeObserver;

.field final synthetic b:[I

.field final synthetic c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;Landroid/view/ViewTreeObserver;[I)V
    .locals 0

    .prologue
    .line 776
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/view/d;->c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iput-object p2, p0, Lcom/google/android/gms/smart_profile/card/view/d;->a:Landroid/view/ViewTreeObserver;

    iput-object p3, p0, Lcom/google/android/gms/smart_profile/card/view/d;->b:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 779
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/d;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/d;->c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->h(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 782
    :cond_0
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 783
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/d;->c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->h(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 784
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/d;->c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 785
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/view/d;->c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/d;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/d;->b:[I

    aget v2, v2, v0

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/smart_profile/card/view/d;->c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v4}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->h(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-static {v3, v2, v4, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;III)Landroid/animation/ValueAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 783
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v2, v1

    .line 785
    goto :goto_1

    .line 792
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/d;->c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/d;->c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->h(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    const/4 v3, -0x1

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;II)V

    .line 793
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/d;->c:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->i(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)V

    .line 796
    :cond_4
    return v1
.end method
