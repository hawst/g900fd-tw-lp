.class public final Lcom/google/android/gms/fitness/wearables/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/p;


# instance fields
.field private final a:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/fitness/wearables/b;->a:Landroid/app/Application;

    .line 34
    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/l/z;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 118
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {p0, p1, v0, v1}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;Z)Ljava/util/Set;

    .line 121
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wearable/r;)V
    .locals 14

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WearableSyncHostService.onMessageReceived: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 39
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 41
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/WearablesSync/DataPoint/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 43
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->b()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/af/a/b/a/a/p;->a([B)Lcom/google/af/a/b/a/a/p;

    move-result-object v1

    .line 49
    iget-object v4, p0, Lcom/google/android/gms/fitness/wearables/b;->a:Landroid/app/Application;

    invoke-static {v4, v0}, Lcom/google/android/gms/fitness/h/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/a;->a()Lcom/google/android/gms/fitness/l/z;

    move-result-object v4

    .line 52
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 53
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, v1, Lcom/google/af/a/b/a/a/p;->a:[Lcom/google/af/a/b/a/a/o;

    array-length v0, v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 55
    iget-object v7, v1, Lcom/google/af/a/b/a/a/p;->a:[Lcom/google/af/a/b/a/a/o;

    array-length v8, v7

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v8, :cond_5

    aget-object v9, v7, v1

    .line 56
    iget-object v0, v9, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 57
    if-nez v0, :cond_0

    .line 58
    iget-object v10, v9, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    invoke-interface {v4, v10}, Lcom/google/android/gms/fitness/l/z;->c(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v10

    .line 59
    invoke-interface {v10}, Ljava/util/Set;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 61
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 62
    iget-object v10, v9, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    invoke-interface {v5, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 70
    iget-object v10, v9, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    const/4 v11, 0x0

    invoke-static {v10, v0, v11}, Lcom/google/android/gms/fitness/apiary/c;->a(Lcom/google/af/a/b/a/a/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v10

    .line 72
    iget-object v11, v9, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    if-eqz v11, :cond_4

    iget-object v9, v9, Lcom/google/af/a/b/a/a/o;->a:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 73
    invoke-static {v4, v6}, Lcom/google/android/gms/fitness/wearables/b;->a(Lcom/google/android/gms/fitness/l/z;Ljava/util/List;)V

    .line 74
    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 76
    const/4 v9, 0x1

    new-array v9, v9, [Lcom/google/android/gms/fitness/data/DataPoint;

    const/4 v11, 0x0

    aput-object v10, v9, v11

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x1

    invoke-interface {v4, v0, v9, v10}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;Z)I

    .line 55
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 64
    :cond_2
    const-string v10, "WearableSyncHostService unable to find dataSource: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v9, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 86
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "WearableSyncHostService unable to sync: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 106
    :cond_3
    :goto_3
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sub-long/2addr v0, v2

    .line 107
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "WearableSyncHostService elapsedNanos: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 109
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 110
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 111
    const-string v1, "com.google.android.gms.fitness.wearables.SYNC_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v1, "TimestampMillis"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/fitness/wearables/b;->a:Landroid/app/Application;

    invoke-virtual {v1, v0}, Landroid/app/Application;->sendBroadcast(Landroid/content/Intent;)V

    .line 114
    return-void

    .line 79
    :cond_4
    :try_start_1
    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 84
    :cond_5
    invoke-static {v4, v6}, Lcom/google/android/gms/fitness/wearables/b;->a(Lcom/google/android/gms/fitness/l/z;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 89
    :cond_6
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/WearablesSync/DataSource/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    :try_start_2
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/google/android/gms/fitness/wearables/b;->a:Landroid/app/Application;

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/h/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/a;->a()Lcom/google/android/gms/fitness/l/z;

    move-result-object v1

    .line 97
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->b()[B

    move-result-object v0

    sget-object v4, Lcom/google/android/gms/fitness/data/DataSource;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/safeparcel/d;->a([BLandroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 99
    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSource;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 100
    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "WearableSyncHostService unable to sync: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_3
.end method
