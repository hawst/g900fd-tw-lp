.class public abstract Lcom/google/android/gms/drive/metadata/a/aw;
.super Lcom/google/android/gms/drive/metadata/a/ay;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/metadata/a/ay;-><init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected final synthetic a(Lorg/json/JSONObject;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 16
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 16
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Lorg/json/JSONObject;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 16
    check-cast p2, Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method
