.class public final Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/photos/autobackup/service/a/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 102
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;)Lcom/google/android/gms/photos/autobackup/service/a/b;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    return-object v0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 54
    const-string v1, "com.google.android.gms.photos.autobackup.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    invoke-static {p0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    new-instance v1, Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/photos/autobackup/service/a/f;)V

    iput-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    .line 58
    :cond_0
    new-instance v0, Lcom/google/android/gms/photos/autobackup/service/c;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/photos/autobackup/service/c;-><init>(Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/c;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 60
    :cond_1
    return-object v0
.end method

.method public final onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->a:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->e()V

    .line 69
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
