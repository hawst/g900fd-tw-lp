.class public final Lcom/google/android/gms/people/service/a/b/h;
.super Lcom/google/android/gms/people/service/a/b/c;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9

    .prologue
    .line 34
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/h;->i:Ljava/lang/String;

    .line 38
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/gms/people/service/a/b/h;->j:Z

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 6

    .prologue
    .line 45
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 47
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/a/b/h;->j:Z

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/h;->i:Ljava/lang/String;

    invoke-virtual {p2, p3, v0}, Lcom/google/android/gms/people/service/e;->c(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V

    .line 53
    :goto_0
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 55
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/h;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/h;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/h;->i:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/people/service/a/b/h;->j:Z

    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    new-instance v4, Lcom/google/android/gms/common/server/y;

    invoke-direct {v4, p1}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v4

    if-eqz v3, :cond_1

    sget-object v1, Lcom/google/android/gms/plus/a/j;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/plus/a/k;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    .line 58
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/h;->i:Ljava/lang/String;

    invoke-virtual {p2, p3, v0}, Lcom/google/android/gms/people/service/e;->d(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :cond_1
    sget-object v1, Lcom/google/android/gms/plus/a/j;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1
.end method
