.class public final Lcom/google/android/gms/drive/database/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/StringBuilder;

.field private final b:Ljava/util/List;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/w;->a:Ljava/lang/StringBuilder;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/w;->b:Ljava/util/List;

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/util/Collection;B)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/database/w;-><init>(Ljava/lang/String;Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/drive/database/w;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/w;->b:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/w;
    .locals 4

    .prologue
    .line 44
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/w;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/gms/drive/database/w;->a:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    const-string v3, "("

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/w;->a:Ljava/lang/StringBuilder;

    const-string v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/w;->a:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/x;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/w;->a:Ljava/lang/StringBuilder;

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/w;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/drive/database/w;->a:Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-object p0
.end method
