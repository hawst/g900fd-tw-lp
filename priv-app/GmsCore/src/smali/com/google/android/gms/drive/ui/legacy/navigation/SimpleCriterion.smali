.class public final Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/drive/query/Filter;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->a:Ljava/util/Map;

    .line 41
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->B:Lcom/google/android/gms/drive/metadata/internal/a/j;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/metadata/i;Ljava/lang/Object;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v0

    .line 42
    sget-object v1, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->a:Ljava/util/Map;

    const-string v2, "notInTrash"

    new-instance v3, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;

    const-string v4, "notInTrash"

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/query/Filter;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    new-instance v0, Lcom/google/android/gms/drive/ui/legacy/navigation/i;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/query/Filter;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->b:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->c:Lcom/google/android/gms/drive/query/Filter;

    .line 62
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;

    .line 48
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/Filter;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->c:Lcom/google/android/gms/drive/query/Filter;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 71
    if-ne p1, p0, :cond_0

    .line 72
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    .line 73
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;

    if-eqz v0, :cond_1

    .line 74
    check-cast p1, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 77
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 84
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 116
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "SimpleCriterion {kind = \"%s\"}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    return-void
.end method
