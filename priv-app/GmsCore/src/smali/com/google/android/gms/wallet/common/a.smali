.class public final Lcom/google/android/gms/wallet/common/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/analytics/bv;


# direct methods
.method private static a(Lcom/google/android/gms/wallet/common/b;)Lcom/google/android/gms/analytics/bv;
    .locals 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a;->b(Landroid/content/Context;)Lcom/google/android/gms/analytics/bv;

    move-result-object v0

    .line 161
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/b;->b:Ljava/lang/String;

    const-string v2, "&dr"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 4

    .prologue
    .line 123
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const-string v0, "AnalyticsUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tracking event [\ncategory: \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\",\naction: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\",\nlabel, \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\",\nvalue: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/a;->b(Landroid/content/Context;)Lcom/google/android/gms/analytics/bv;

    move-result-object v1

    .line 129
    new-instance v0, Lcom/google/android/gms/analytics/bf;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/bf;-><init>()V

    .line 130
    if-eqz p4, :cond_1

    .line 131
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/analytics/bf;->a(J)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    .line 133
    :cond_1
    invoke-virtual {v0, p1}, Lcom/google/android/gms/analytics/bf;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/analytics/bf;->b(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/analytics/bf;->c(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/bf;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 136
    :cond_2
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 145
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const-string v0, ":"

    invoke-static {v0, p3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-static {p0, p1, p2, v0, p4}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 149
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const-string v0, "AnalyticsUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tracking view [\nflowName: \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\",\nscreenName: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;)Lcom/google/android/gms/analytics/bv;

    move-result-object v1

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "&cd"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/google/android/gms/analytics/bi;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/bi;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "utm_source="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/b;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/bi;->d(Ljava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/bi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/bi;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 64
    :cond_1
    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "AnalyticsUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tracking event [\ncategory: \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\",\naction: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\",\nlabel, \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\",\nvalue: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;)Lcom/google/android/gms/analytics/bv;

    move-result-object v1

    .line 91
    new-instance v0, Lcom/google/android/gms/analytics/bf;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/bf;-><init>()V

    .line 92
    if-eqz p4, :cond_1

    .line 93
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/analytics/bf;->a(J)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    .line 95
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "utm_source="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/b;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/bf;->d(Ljava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/analytics/bf;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/analytics/bf;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/analytics/bf;->b(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/analytics/bf;->c(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/bf;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 101
    :cond_2
    return-void
.end method

.method public static varargs a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-static {v1, p3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 166
    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/gms/wallet/b/e;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)Lcom/google/android/gms/analytics/bv;
    .locals 2

    .prologue
    .line 152
    sget-object v0, Lcom/google/android/gms/wallet/common/a;->a:Lcom/google/android/gms/analytics/bv;

    if-eqz v0, :cond_0

    .line 153
    sget-object v0, Lcom/google/android/gms/wallet/common/a;->a:Lcom/google/android/gms/analytics/bv;

    .line 156
    :goto_0
    return-object v0

    .line 155
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    const-string v1, "UA-35792483-1"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/ax;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bv;

    move-result-object v0

    .line 156
    sput-object v0, Lcom/google/android/gms/wallet/common/a;->a:Lcom/google/android/gms/analytics/bv;

    goto :goto_0
.end method
