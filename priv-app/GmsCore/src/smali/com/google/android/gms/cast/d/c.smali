.class final Lcom/google/android/gms/cast/d/c;
.super Lcom/google/android/gms/cast/e/d;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/d/a;

.field private final d:Ljava/lang/String;

.field private e:Lcom/google/android/gms/cast/b/o;

.field private f:Lcom/google/android/gms/cast/CastDevice;

.field private g:Lcom/google/android/gms/cast/d/d;

.field private final h:Z

.field private i:I

.field private final j:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/d/a;Ljava/lang/String;ILcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;ZLjava/lang/String;)V
    .locals 5

    .prologue
    .line 280
    iput-object p1, p0, Lcom/google/android/gms/cast/d/c;->a:Lcom/google/android/gms/cast/d/a;

    .line 281
    invoke-static {p1}, Lcom/google/android/gms/cast/d/a;->b(Lcom/google/android/gms/cast/d/a;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "%s_%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p8, v2, v3

    const/4 v3, 0x1

    const-string v4, "session"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/cast/d/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/gms/cast/d/a;->g(Lcom/google/android/gms/cast/d/a;)Landroid/os/Handler;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/cast/e/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 283
    iput-object p2, p0, Lcom/google/android/gms/cast/d/c;->d:Ljava/lang/String;

    .line 284
    iput p3, p0, Lcom/google/android/gms/cast/d/c;->j:I

    .line 285
    iput-object p4, p0, Lcom/google/android/gms/cast/d/c;->f:Lcom/google/android/gms/cast/CastDevice;

    .line 286
    iput-object p5, p0, Lcom/google/android/gms/cast/d/c;->e:Lcom/google/android/gms/cast/b/o;

    .line 287
    iput-object p6, p0, Lcom/google/android/gms/cast/d/c;->g:Lcom/google/android/gms/cast/d/d;

    .line 288
    iput-boolean p7, p0, Lcom/google/android/gms/cast/d/c;->h:Z

    .line 289
    return-void
.end method

.method private a(Lcom/google/i/a/a/a/a;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 420
    :try_start_0
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 421
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 422
    iget-object v0, p1, Lcom/google/i/a/a/a/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/a/a/d;

    .line 423
    const-string v4, "%s:%d:%d|"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/google/i/a/a/a/d;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, v0, Lcom/google/i/a/a/a/d;->b:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget v0, v0, Lcom/google/i/a/a/a/d;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 428
    :catch_0
    move-exception v0

    .line 429
    iget-object v1, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Unable to get instance of sha1."

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 433
    :goto_1
    const/4 v0, 0x0

    :goto_2
    return-object v0

    .line 426
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    .line 427
    array-length v1, v0

    invoke-static {}, Lcom/google/p/a/e/c;->a()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/p/a/e/c;->a([BI[BZ)Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_2

    .line 430
    :catch_1
    move-exception v0

    .line 431
    iget-object v1, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "can\'t base64 encode environment scan digest."

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/google/i/a/a/a/a;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 385
    new-instance v1, Lcom/google/android/gms/common/c/a;

    iget-object v2, p0, Lcom/google/android/gms/cast/d/c;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v2}, Lcom/google/android/gms/cast/d/a;->b(Lcom/google/android/gms/cast/d/a;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/c/a;-><init>(Landroid/content/Context;)V

    .line 386
    invoke-static {}, Lcom/google/android/gms/cast/d/a;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 387
    iget-object v1, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Droidguard disabled"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 411
    :goto_0
    return-object v0

    .line 392
    :cond_0
    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/cast/d/c;->a(Lcom/google/i/a/a/a/a;)Ljava/lang/String;

    move-result-object v1

    .line 393
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 394
    iget-object v1, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Unable to create hash of the environmentScan"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 408
    :catch_0
    move-exception v1

    .line 409
    iget-object v2, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Droidguard runtime exception: "

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 397
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v3, "wifi scan digest = %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 399
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 400
    const-string v3, "PIN"

    invoke-virtual {v2, v3, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    const-string v3, "EnvironmentScan"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    iget-object v1, p0, Lcom/google/android/gms/cast/d/c;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v1}, Lcom/google/android/gms/cast/d/a;->b(Lcom/google/android/gms/cast/d/a;)Landroid/content/Context;

    move-result-object v1

    const-string v3, "opencast_createsession"

    invoke-static {v1, v3, v2}, Lcom/google/android/gms/droidguard/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 407
    iget-object v1, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "dr blob length = %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 379
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.cast.session.ACTION_CAST_NEARBY_ABORTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 381
    iget-object v1, p0, Lcom/google/android/gms/cast/d/c;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v1}, Lcom/google/android/gms/cast/d/a;->b(Lcom/google/android/gms/cast/d/a;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 382
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/d/a;->h(Lcom/google/android/gms/cast/d/a;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/e/i;->a(Ljava/util/List;)Lcom/google/i/a/a/a/a;

    move-result-object v1

    .line 295
    invoke-virtual {v1}, Lcom/google/i/a/a/a/a;->c()I

    move-result v0

    if-nez v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "No CastNearby device found."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 297
    const/4 v0, 0x0

    .line 312
    :cond_0
    :goto_0
    return-object v0

    .line 299
    :cond_1
    new-instance v0, Lcom/google/i/a/a/b/a;

    invoke-direct {v0}, Lcom/google/i/a/a/b/a;-><init>()V

    .line 300
    iget-object v2, p0, Lcom/google/android/gms/cast/d/c;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/i/a/a/b/a;->a(Ljava/lang/String;)Lcom/google/i/a/a/b/a;

    .line 301
    iget v2, p0, Lcom/google/android/gms/cast/d/c;->j:I

    invoke-virtual {v0, v2}, Lcom/google/i/a/a/b/a;->a(I)Lcom/google/i/a/a/b/a;

    .line 302
    invoke-virtual {v0, v1}, Lcom/google/i/a/a/b/a;->a(Lcom/google/i/a/a/a/a;)Lcom/google/i/a/a/b/a;

    .line 304
    iget-object v2, p0, Lcom/google/android/gms/cast/d/c;->d:Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/cast/d/c;->a(Ljava/lang/String;Lcom/google/i/a/a/a/a;)Ljava/lang/String;

    move-result-object v1

    .line 305
    if-eqz v1, :cond_2

    .line 306
    invoke-virtual {v0, v1}, Lcom/google/i/a/a/b/a;->b(Ljava/lang/String;)Lcom/google/i/a/a/b/a;

    .line 308
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/cast/d/c;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v1}, Lcom/google/android/gms/cast/d/a;->b(Lcom/google/android/gms/cast/d/a;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/cast/b/d;->a(Landroid/content/Context;)Lcom/google/android/gms/cast/b/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/d;->a()[B

    move-result-object v1

    .line 309
    if-eqz v1, :cond_0

    .line 310
    invoke-static {v1}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/i/a/a/b/a;->a(Lcom/google/protobuf/a/a;)Lcom/google/i/a/a/b/a;

    goto :goto_0
.end method

.method protected final a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Failed to create session %d"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Testing for retry, mAllowPairingDialogOnFailure=%b, mConnectionAttemptCount=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/google/android/gms/cast/d/c;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/google/android/gms/cast/d/c;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 365
    iget-boolean v0, p0, Lcom/google/android/gms/cast/d/c;->h:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/d/c;->i:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/gms/cast/d/c;->i:I

    invoke-static {}, Lcom/google/android/gms/cast/d/a;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "retrying"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->a:Lcom/google/android/gms/cast/d/a;

    iget-object v1, p0, Lcom/google/android/gms/cast/d/c;->f:Lcom/google/android/gms/cast/CastDevice;

    iget-object v2, p0, Lcom/google/android/gms/cast/d/c;->e:Lcom/google/android/gms/cast/b/o;

    iget-object v3, p0, Lcom/google/android/gms/cast/d/c;->g:Lcom/google/android/gms/cast/d/d;

    iget-object v4, p0, Lcom/google/android/gms/cast/d/c;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/cast/d/a;->a(Lcom/google/android/gms/cast/d/a;Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;Ljava/lang/String;)V

    .line 369
    iput-object v5, p0, Lcom/google/android/gms/cast/d/c;->f:Lcom/google/android/gms/cast/CastDevice;

    .line 370
    iput-object v5, p0, Lcom/google/android/gms/cast/d/c;->e:Lcom/google/android/gms/cast/b/o;

    .line 371
    iput-object v5, p0, Lcom/google/android/gms/cast/d/c;->g:Lcom/google/android/gms/cast/d/d;

    .line 376
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->g:Lcom/google/android/gms/cast/d/d;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast/d/d;->a(I)V

    .line 374
    invoke-direct {p0}, Lcom/google/android/gms/cast/d/c;->b()V

    goto :goto_0
.end method

.method protected final a([B)V
    .locals 8

    .prologue
    const/16 v7, 0x7d1

    const/4 v6, 0x0

    .line 318
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onSuccessResponse"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    new-instance v0, Lcom/google/i/a/a/b/b;

    invoke-direct {v0}, Lcom/google/i/a/a/b/b;-><init>()V

    array-length v1, p1

    invoke-virtual {v0, p1, v1}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/a/b/b;

    .line 320
    iget-boolean v1, v0, Lcom/google/i/a/a/b/b;->a:Z

    if-eqz v1, :cond_1

    .line 321
    iget-object v1, v0, Lcom/google/i/a/a/b/b;->b:Lcom/google/i/a/a/a/c;

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "response.Session server=%s,port=%d,token=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/google/i/a/a/a/c;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, v1, Lcom/google/i/a/a/a/c;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, v1, Lcom/google/i/a/a/a/c;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->e:Lcom/google/android/gms/cast/b/o;

    iget-object v2, v1, Lcom/google/i/a/a/a/c;->a:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/i/a/a/a/c;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {v3}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/b/o;->a(Ljava/lang/String;[B)V

    .line 327
    iget-object v0, v1, Lcom/google/i/a/a/a/c;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    check-cast v0, Ljava/net/Inet4Address;

    .line 330
    if-eqz v0, :cond_0

    .line 331
    iget-object v2, p0, Lcom/google/android/gms/cast/d/c;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v2}, Lcom/google/android/gms/cast/d/a;->e(Lcom/google/android/gms/cast/d/a;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 332
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/cast/d/c;->a:Lcom/google/android/gms/cast/d/a;

    invoke-static {v3}, Lcom/google/android/gms/cast/d/a;->f(Lcom/google/android/gms/cast/d/a;)I

    .line 333
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/cast/d/c;->g:Lcom/google/android/gms/cast/d/d;

    iget-object v3, p0, Lcom/google/android/gms/cast/d/c;->d:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/gms/cast/d/d;->a(Ljava/lang/String;)V

    .line 336
    iget-object v2, p0, Lcom/google/android/gms/cast/d/c;->e:Lcom/google/android/gms/cast/b/o;

    iget v1, v1, Lcom/google/i/a/a/a/c;->c:I

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/cast/b/o;->a(Ljava/net/Inet4Address;I)V

    .line 357
    :goto_0
    return-void

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_2
    .catch Lcom/google/protobuf/a/e; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 349
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Unable to parse CreateSession response data"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->g:Lcom/google/android/gms/cast/d/d;

    invoke-interface {v0, v7}, Lcom/google/android/gms/cast/d/d;->a(I)V

    .line 351
    invoke-direct {p0}, Lcom/google/android/gms/cast/d/c;->b()V

    goto :goto_0

    .line 338
    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Unable to get the address for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v1, v1, Lcom/google/i/a/a/a/c;->b:Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->g:Lcom/google/android/gms/cast/d/d;

    const/16 v1, 0x7d1

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast/d/d;->a(I)V

    .line 341
    invoke-direct {p0}, Lcom/google/android/gms/cast/d/c;->b()V
    :try_end_3
    .catch Lcom/google/protobuf/a/e; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 353
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Unable to parse CreateSession response data"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->g:Lcom/google/android/gms/cast/d/d;

    invoke-interface {v0, v7}, Lcom/google/android/gms/cast/d/d;->a(I)V

    .line 355
    invoke-direct {p0}, Lcom/google/android/gms/cast/d/c;->b()V

    goto :goto_0

    .line 344
    :cond_1
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "response does not contain a session token."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/cast/d/c;->g:Lcom/google/android/gms/cast/d/d;

    const/16 v1, 0x7d1

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast/d/d;->a(I)V

    .line 346
    invoke-direct {p0}, Lcom/google/android/gms/cast/d/c;->b()V
    :try_end_4
    .catch Lcom/google/protobuf/a/e; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0
.end method
