.class public Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ad;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:[Lcom/google/android/gms/appdatasearch/CorpusId;

.field final d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/appdatasearch/ad;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ad;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:Lcom/google/android/gms/appdatasearch/ad;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/CorpusId;[I)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->a:I

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->b:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->c:[Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 47
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->d:[I

    .line 48
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, v1, v1}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;-><init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/CorpusId;[I)V

    .line 52
    return-void
.end method

.method public constructor <init>([Lcom/google/android/gms/appdatasearch/CorpusId;[I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 55
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;-><init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/CorpusId;[I)V

    .line 56
    array-length v1, p1

    if-eqz v1, :cond_0

    array-length v1, p2

    array-length v2, p1

    rem-int/2addr v1, v2

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 58
    return-void

    .line 56
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->b:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:Lcom/google/android/gms/appdatasearch/ad;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:Lcom/google/android/gms/appdatasearch/ad;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ad;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;Landroid/os/Parcel;I)V

    .line 130
    return-void
.end method
