.class public Lcom/google/android/gms/droidguard/DroidGuardService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/droidguard/d/e;

.field private b:Lcom/google/android/gms/droidguard/e/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 210
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/droidguard/DroidGuardService;)Lcom/google/android/gms/droidguard/e/a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->b:Lcom/google/android/gms/droidguard/e/a;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/droidguard/DroidGuardService;)Lcom/google/android/gms/droidguard/d/e;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->a:Lcom/google/android/gms/droidguard/d/e;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 60
    const-string v0, "com.google.android.gms.droidguard.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Lcom/google/android/gms/droidguard/o;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/gms/droidguard/o;-><init>(Lcom/google/android/gms/droidguard/DroidGuardService;Landroid/content/Context;B)V

    invoke-virtual {v0}, Lcom/google/android/gms/droidguard/o;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 51
    invoke-static {p0}, Lcom/google/android/gms/droidguard/d/j;->a(Landroid/content/Context;)Lcom/google/android/gms/droidguard/d/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->a:Lcom/google/android/gms/droidguard/d/e;

    .line 53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 54
    new-instance v0, Lcom/google/android/gms/droidguard/e/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/droidguard/e/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/DroidGuardService;->b:Lcom/google/android/gms/droidguard/e/a;

    .line 56
    :cond_0
    return-void
.end method
