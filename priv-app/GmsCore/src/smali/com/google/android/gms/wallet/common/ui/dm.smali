.class public final Lcom/google/android/gms/wallet/common/ui/dm;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cb;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field d:Lcom/google/android/gms/wallet/common/ui/ax;

.field e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

.field f:Landroid/widget/TextView;

.field g:Landroid/widget/ImageView;

.field h:Landroid/widget/EditText;

.field private i:Lcom/google/checkout/inapp/proto/j;

.field private j:Z

.field private k:Lcom/google/android/gms/wallet/shared/common/b/a;

.field private l:Ljava/lang/String;

.field private m:Lcom/google/android/gms/wallet/common/ui/bh;

.field private n:Lcom/google/android/gms/wallet/common/ui/bi;

.field private o:Lcom/google/android/gms/wallet/common/ui/aw;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/checkout/inapp/proto/j;ZZLjava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/common/ui/dm;
    .locals 3

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/dm;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/dm;-><init>()V

    .line 84
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 85
    const-string v2, "com.google.android.gms.wallet.instrument"

    invoke-static {v1, v2, p0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 86
    const-string v2, "com.google.android.gms.wallet.requireAddressUpgrade"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 87
    const-string v2, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 88
    const-string v2, "com.google.android.gms.wallet.addressHints"

    invoke-static {v1, v2, p3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    .line 90
    const-string v2, "com.google.android.gms.wallet.baseAddress"

    invoke-static {v1, v2, p4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 91
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dm;->setArguments(Landroid/os/Bundle;)V

    .line 93
    return-object v0
.end method

.method private b(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 282
    const/4 v0, 0x4

    new-array v4, v0, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/dm;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    aput-object v3, v4, v0

    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/dm;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    aput-object v3, v4, v0

    .line 285
    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 287
    if-eqz v6, :cond_0

    .line 288
    if-eqz p1, :cond_2

    .line 291
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    .line 285
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 291
    goto :goto_1

    .line 292
    :cond_2
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 296
    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/google/checkout/a/a/a/d;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 350
    new-instance v2, Lcom/google/checkout/a/a/a/b;

    invoke-direct {v2}, Lcom/google/checkout/a/a/a/b;-><init>()V

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h()Lcom/google/t/a/b;

    move-result-object v0

    iput-object v0, v2, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->e(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 361
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 367
    :goto_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit16 v4, v4, 0x7d0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 372
    :goto_1
    if-eqz v0, :cond_1

    .line 373
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Lcom/google/checkout/a/a/a/b;->b:I

    .line 375
    :cond_1
    if-eqz v1, :cond_2

    .line 376
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Lcom/google/checkout/a/a/a/b;->c:I

    .line 378
    :cond_2
    new-instance v0, Lcom/google/checkout/a/a/a/c;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/c;-><init>()V

    iput-object v0, v2, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    .line 379
    iget-object v0, v2, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iput-object v3, v0, Lcom/google/checkout/a/a/a/c;->b:Ljava/lang/String;

    .line 382
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    .line 386
    :cond_4
    new-instance v0, Lcom/google/checkout/a/a/a/d;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/d;-><init>()V

    .line 387
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/checkout/a/a/a/d;->a:I

    .line 388
    iput-object v2, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    .line 389
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Z)V

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->h:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->h:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 335
    :cond_2
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b()V

    .line 268
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dm;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dm;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 301
    const/4 v2, 0x3

    new-array v3, v2, [Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    .line 302
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 304
    if-eqz v5, :cond_0

    .line 305
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 308
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 309
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    .line 317
    :goto_1
    return v0

    .line 302
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 313
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e()Z

    move-result v2

    if-nez v2, :cond_2

    .line 314
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g()Z

    goto :goto_1

    :cond_2
    move v0, v1

    .line 317
    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 102
    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Fragment requires instrument extra!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 104
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v2, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    .line 107
    const-string v0, "com.google.android.gms.wallet.requireAddressUpgrade"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->j:Z

    .line 110
    new-instance v0, Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->k:Lcom/google/android/gms/wallet/shared/common/b/a;

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->k:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/image/a;->a(Landroid/content/Context;)Lcom/google/android/gms/wallet/dynamite/image/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Lcom/google/android/gms/wallet/dynamite/image/a;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/j;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->l:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    .line 119
    sget v0, Lcom/google/android/gms/l;->gJ:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 121
    sget v0, Lcom/google/android/gms/j;->cx:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->f:Landroid/widget/TextView;

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    sget v0, Lcom/google/android/gms/j;->cA:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->h:Landroid/widget/EditText;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->l:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 132
    :cond_0
    sget v0, Lcom/google/android/gms/j;->cz:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->g:Landroid/widget/ImageView;

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->k:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/widget/ImageView;Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/common/b/a;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->g:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/j;)Lcom/google/aa/b/a/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/aa/b/a/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 142
    :goto_0
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    sget v0, Lcom/google/android/gms/j;->bW:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->zZ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->e(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 148
    sget v0, Lcom/google/android/gms/j;->fK:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 199
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->j:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-boolean v0, v0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    if-nez v0, :cond_9

    :cond_2
    const/4 v0, 0x1

    move v1, v0

    .line 202
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v2, Lcom/google/android/gms/j;->as:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez v0, :cond_5

    .line 205
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    .line 207
    iget-object v0, v6, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v7, v0, Lcom/google/t/a/b;->a:Ljava/lang/String;

    .line 208
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v7, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    .line 210
    const-string v0, "com.google.android.gms.wallet.addressHints"

    const-class v2, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v9

    .line 214
    invoke-static {v9}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v10

    .line 217
    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    const/4 v2, 0x0

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v6, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    :cond_3
    const/4 v3, 0x1

    .line 220
    :goto_3
    if-eqz v3, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->e(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-nez v0, :cond_b

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_b

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    move v2, v0

    .line 225
    :goto_4
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v11

    if-nez v1, :cond_c

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {v11, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/List;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [C

    fill-array-data v1, :array_0

    iget-object v7, v0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-object v1, v7, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    sget v1, Lcom/google/android/gms/p;->Ak:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/s;->a(I)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    iget-object v0, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/Collection;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/s;->b(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/s;->c(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    .line 237
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 238
    const-string v0, "com.google.android.gms.wallet.baseAddress"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 242
    if-eqz v0, :cond_e

    .line 243
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v2, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/t/a/b;)V

    .line 244
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x1

    .line 249
    :goto_6
    if-eqz v1, :cond_4

    .line 251
    iget-object v1, v6, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v1, v6, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Ljava/lang/String;)V

    .line 259
    :cond_4
    :goto_7
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->as:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 263
    :cond_5
    return-object v4

    .line 139
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->g:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 150
    :cond_7
    sget v0, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 151
    sget v0, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 152
    sget v0, Lcom/google/android/gms/j;->dT:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 154
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->dZ:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ax;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->d:Lcom/google/android/gms/wallet/common/ui/ax;

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->d:Lcom/google/android/gms/wallet/common/ui/ax;

    if-nez v0, :cond_8

    .line 157
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/ax;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->d:Lcom/google/android/gms/wallet/common/ui/ax;

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->dZ:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->d:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 163
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    iget v0, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    .line 165
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    .line 167
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/text/InputFilter;

    const/4 v5, 0x0

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 168
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->d:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/ax;->a(I)V

    .line 170
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->i:Lcom/google/checkout/inapp/proto/j;

    iget v0, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_8
    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/wallet/common/ui/aw;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;I)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    .line 173
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bg;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dm;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/bg;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    .line 175
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/bh;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/wallet/common/ui/bh;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/dn;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    .line 176
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/bi;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/bi;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/dn;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dm;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->dT:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->cA:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusUpId(I)V

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dm;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusUpId(I)V

    goto/16 :goto_1

    .line 170
    :sswitch_0
    const/4 v0, 0x3

    goto/16 :goto_8

    :sswitch_1
    const/4 v0, 0x4

    goto/16 :goto_8

    :sswitch_2
    const/4 v0, 0x2

    goto/16 :goto_8

    :sswitch_3
    const/4 v0, 0x1

    goto/16 :goto_8

    :sswitch_4
    const/4 v0, 0x5

    goto/16 :goto_8

    .line 199
    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_2

    .line 217
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 220
    :cond_b
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_4

    .line 225
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 244
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_6

    .line 247
    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v2, v6, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/t/a/b;)V

    move v1, v3

    goto/16 :goto_6

    .line 253
    :cond_f
    if-eqz v0, :cond_4

    .line 255
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dm;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 225
    nop

    :array_0
    .array-data 2
        0x52s
        0x4es
    .end array-data

    .line 170
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x2 -> :sswitch_2
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x1b -> :sswitch_4
    .end sparse-switch
.end method
