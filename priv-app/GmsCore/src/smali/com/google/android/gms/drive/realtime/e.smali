.class public final Lcom/google/android/gms/drive/realtime/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/drive/DriveId;

.field public b:Lcom/google/android/gms/drive/auth/g;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/auth/g;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/e;->b:Lcom/google/android/gms/drive/auth/g;

    .line 26
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41
    if-ne p0, p1, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 44
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 45
    goto :goto_0

    .line 47
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/realtime/e;

    .line 48
    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p1, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-static {v2, v3}, Lcom/google/c/a/a/b/h/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/e;->b:Lcom/google/android/gms/drive/auth/g;

    iget-object v3, p1, Lcom/google/android/gms/drive/realtime/e;->b:Lcom/google/android/gms/drive/auth/g;

    invoke-static {v2, v3}, Lcom/google/c/a/a/b/h/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/e;->b:Lcom/google/android/gms/drive/auth/g;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
