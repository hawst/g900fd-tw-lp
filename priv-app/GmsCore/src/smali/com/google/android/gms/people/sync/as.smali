.class final Lcom/google/android/gms/people/sync/as;
.super Lcom/google/android/gms/people/sync/bb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/sync/ap;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;Z)V
    .locals 0

    .prologue
    .line 2554
    iput-object p1, p0, Lcom/google/android/gms/people/sync/as;->a:Lcom/google/android/gms/people/sync/ap;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/gms/people/sync/bb;-><init>(Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;Z)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 2554
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    check-cast p3, Landroid/content/ContentValues;

    const-string v0, "compressed_avatar_url"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "has_avatar"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "has_avatar"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p3, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-nez p2, :cond_0

    const-string v1, "compressed_avatar_url"

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
