.class public final Lcom/google/android/gms/wallet/common/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 1

    .prologue
    .line 56
    if-nez p0, :cond_0

    .line 59
    const/4 v0, 0x0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/common/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/h;-><init>(Landroid/os/Bundle;)V

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/g;->a(Lcom/google/android/gms/wallet/common/i;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/wallet/common/i;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 80
    const-string v0, "sellerName"

    const-string v2, ""

    invoke-interface {p0, v0, v2}, Lcom/google/android/gms/wallet/common/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 81
    new-instance v4, Lcom/google/checkout/inapp/proto/d;

    invoke-direct {v4}, Lcom/google/checkout/inapp/proto/d;-><init>()V

    .line 82
    const-string v0, "items"

    invoke-interface {p0, v0}, Lcom/google/android/gms/wallet/common/i;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 83
    if-eqz v5, :cond_1

    .line 84
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 85
    new-array v0, v6, [Lcom/google/checkout/inapp/proto/f;

    iput-object v0, v4, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    .line 86
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_1

    .line 87
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/i;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/g;->b(Lcom/google/android/gms/wallet/common/i;)Lcom/google/checkout/inapp/proto/f;

    move-result-object v0

    .line 88
    if-nez v0, :cond_0

    .line 89
    const-string v0, "DisplayHintsParser"

    const-string v2, "Invalid cart item"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 101
    :goto_1
    return-object v0

    .line 92
    :cond_0
    iget-object v7, v4, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    aput-object v0, v7, v2

    .line 86
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 95
    :cond_1
    iget-object v0, v4, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v0, v0

    if-nez v0, :cond_2

    .line 96
    const-string v0, "DisplayHintsParser"

    const-string v2, "No valid cart items"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 97
    goto :goto_1

    .line 99
    :cond_2
    invoke-static {v4}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/d;)V

    .line 100
    const-string v0, "reviewPurchaseTitle"

    const-string v1, ""

    invoke-interface {p0, v0, v1}, Lcom/google/android/gms/wallet/common/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 101
    new-instance v0, Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-direct {v0, v3, v1, v4}, Lcom/google/android/gms/wallet/common/DisplayHints;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/checkout/inapp/proto/d;)V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 65
    if-nez p0, :cond_0

    .line 75
    :goto_0
    return-object v0

    .line 71
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 72
    new-instance v2, Lcom/google/android/gms/wallet/common/j;

    invoke-direct {v2, v1}, Lcom/google/android/gms/wallet/common/j;-><init>(Lorg/json/JSONObject;)V

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/g;->a(Lcom/google/android/gms/wallet/common/i;)Lcom/google/android/gms/wallet/common/DisplayHints;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 74
    :catch_0
    move-exception v1

    const-string v1, "DisplayHintsParser"

    const-string v2, "String is not valid JSON"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static b(Lcom/google/android/gms/wallet/common/i;)Lcom/google/checkout/inapp/proto/f;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    const-string v0, "name"

    const-string v3, ""

    invoke-interface {p0, v0, v3}, Lcom/google/android/gms/wallet/common/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 107
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const-string v0, "DisplayHintsParser"

    const-string v1, "Empty or missing \'name\' field in cart item"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 158
    :goto_0
    return-object v0

    .line 112
    :cond_0
    const-string v0, "description"

    const-string v3, ""

    invoke-interface {p0, v0, v3}, Lcom/google/android/gms/wallet/common/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 113
    const-string v0, "quantity"

    invoke-interface {p0, v0, v2}, Lcom/google/android/gms/wallet/common/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    :try_start_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 124
    :goto_1
    const-string v3, "currencyCode"

    const-string v6, ""

    invoke-interface {p0, v3, v6}, Lcom/google/android/gms/wallet/common/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 128
    new-instance v3, Lcom/google/checkout/inapp/proto/f;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/f;-><init>()V

    .line 129
    iput-object v5, v3, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    .line 130
    iput-object v4, v3, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    .line 131
    iput v0, v3, Lcom/google/checkout/inapp/proto/f;->e:I

    .line 133
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 134
    const-string v4, "price"

    const-string v5, ""

    invoke-interface {p0, v4, v5}, Lcom/google/android/gms/wallet/common/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 136
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 137
    invoke-static {v6, v4}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/d;

    move-result-object v2

    .line 138
    if-eqz v2, :cond_1

    .line 139
    iput-object v2, v3, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    .line 143
    :cond_1
    if-le v0, v1, :cond_2

    .line 144
    const-string v1, "net_cost"

    const-string v4, ""

    invoke-interface {p0, v1, v4}, Lcom/google/android/gms/wallet/common/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 145
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 146
    invoke-static {v6, v1}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/d;

    move-result-object v0

    .line 147
    if-eqz v0, :cond_2

    .line 148
    iput-object v0, v3, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    :cond_2
    :goto_2
    move-object v0, v3

    .line 158
    goto :goto_0

    .line 116
    :cond_3
    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_1

    .line 119
    :catch_0
    move-exception v0

    .line 121
    const-string v1, "DisplayHintsParser"

    const-string v3, "Invalid numeric field in cart item"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v2

    .line 122
    goto :goto_0

    .line 150
    :cond_4
    if-eqz v2, :cond_2

    .line 151
    new-instance v1, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v1, v3, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    .line 152
    iget-object v1, v3, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    iput-object v6, v1, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    .line 153
    iget-object v1, v3, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    int-to-long v4, v0

    iget-wide v6, v2, Lcom/google/checkout/inapp/proto/a/d;->a:J

    mul-long/2addr v4, v6

    iput-wide v4, v1, Lcom/google/checkout/inapp/proto/a/d;->a:J

    goto :goto_2
.end method
