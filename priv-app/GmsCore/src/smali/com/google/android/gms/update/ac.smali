.class final Lcom/google/android/gms/update/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/d;

.field final synthetic b:Lcom/google/android/gms/update/UpdateFromSdCardActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;Lcom/google/android/gms/update/d;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lcom/google/android/gms/update/ac;->b:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    iput-object p2, p0, Lcom/google/android/gms/update/ac;->a:Lcom/google/android/gms/update/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 412
    sget v0, Lcom/google/android/gms/j;->kJ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 415
    iget-object v1, p0, Lcom/google/android/gms/update/ac;->b:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->l(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 416
    iget-object v1, p0, Lcom/google/android/gms/update/ac;->b:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->l(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 418
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/update/ac;->b:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v1, p2}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;Landroid/view/View;)Landroid/view/View;

    .line 419
    iget-object v1, p0, Lcom/google/android/gms/update/ac;->b:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 421
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 422
    iget-object v1, p0, Lcom/google/android/gms/update/ac;->b:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->g(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/update/ac;->b:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    .line 440
    :goto_0
    return-void

    .line 428
    :cond_1
    sget v0, Lcom/google/android/gms/f;->ay:I

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 436
    iget-object v0, p0, Lcom/google/android/gms/update/ac;->a:Lcom/google/android/gms/update/d;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/update/d;->a(I)V

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/update/ac;->b:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->ge:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
