.class abstract Lcom/google/android/gms/people/sync/a/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/sync/a/a;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/sync/a/a;)V
    .locals 0

    .prologue
    .line 1435
    iput-object p1, p0, Lcom/google/android/gms/people/sync/a/j;->b:Lcom/google/android/gms/people/sync/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/sync/a/a;B)V
    .locals 0

    .prologue
    .line 1435
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/a/j;-><init>(Lcom/google/android/gms/people/sync/a/a;)V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/String;
.end method

.method protected abstract a(Lcom/google/android/gms/people/sync/a/l;ILjava/lang/String;)V
.end method

.method public final a(Lcom/google/android/gms/people/sync/a/n;)V
    .locals 5

    .prologue
    .line 1437
    iget-object v0, p1, Lcom/google/android/gms/people/sync/a/n;->b:Ljava/lang/String;

    .line 1438
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 1440
    iget-object v1, p0, Lcom/google/android/gms/people/sync/a/j;->b:Lcom/google/android/gms/people/sync/a/a;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/a/a;->b(Lcom/google/android/gms/people/sync/a/a;)Lcom/google/android/gms/people/c/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/a/j;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/people/sync/a/j;->b:Lcom/google/android/gms/people/sync/a/a;

    invoke-static {v3}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/a/a;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1443
    const/4 v0, 0x2

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1444
    const-string v0, "PeopleContactsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/a/j;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1448
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1449
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1450
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1451
    const/4 v3, 0x2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1453
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/a/j;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4, v0}, Lcom/google/android/gms/people/sync/a/a;->a(Lcom/google/android/gms/people/sync/a/n;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/sync/a/l;

    move-result-object v0

    .line 1454
    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/people/sync/a/j;->a(Lcom/google/android/gms/people/sync/a/l;ILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1457
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1458
    return-void
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected abstract c()Ljava/lang/String;
.end method
