.class public final Lcom/google/android/gms/games/ui/common/requests/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/data/w;

.field final b:Lcom/google/android/gms/common/data/w;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/d;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/google/android/gms/common/data/w;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/w;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/a;->a:Lcom/google/android/gms/common/data/w;

    .line 22
    new-instance v0, Lcom/google/android/gms/common/data/w;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/w;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/a;->b:Lcom/google/android/gms/common/data/w;

    .line 27
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 31
    const/4 v0, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v5

    move v4, v0

    move-object v2, v1

    :goto_0
    if-ge v4, v5, :cond_3

    .line 32
    invoke-interface {p1, v4}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequestEntity;

    .line 34
    invoke-virtual {v0}, Lcom/google/android/gms/games/request/GameRequestEntity;->g()Lcom/google/android/gms/games/Player;

    move-result-object v3

    .line 35
    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->k()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 36
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/a;->a:Lcom/google/android/gms/common/data/w;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/data/w;->a(Ljava/lang/Object;)V

    move-object v0, v1

    move-object v1, v2

    .line 31
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 42
    if-eqz v1, :cond_1

    .line 43
    new-instance v2, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-direct {v2, v1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;-><init>(Ljava/util/ArrayList;)V

    .line 45
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/a;->b:Lcom/google/android/gms/common/data/w;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/data/w;->a(Ljava/lang/Object;)V

    .line 48
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 49
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v1, v3

    goto :goto_1

    .line 51
    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    move-object v1, v2

    goto :goto_1

    .line 57
    :cond_3
    if-eqz v1, :cond_4

    .line 58
    new-instance v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;-><init>(Ljava/util/ArrayList;)V

    .line 59
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/a;->b:Lcom/google/android/gms/common/data/w;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/w;->a(Ljava/lang/Object;)V

    .line 61
    :cond_4
    return-void
.end method
