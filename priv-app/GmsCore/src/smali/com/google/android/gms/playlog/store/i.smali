.class final Lcom/google/android/gms/playlog/store/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/database/sqlite/SQLiteOpenHelper;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteOpenHelper;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteOpenHelper;

    iput-object v0, p0, Lcom/google/android/gms/playlog/store/i;->a:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 63
    return-void
.end method

.method public static a([BI)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 51
    array-length v1, p0

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/common/util/aj;->a([BII)I

    move-result v1

    if-ne v1, p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private b([BI)J
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 176
    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v1, v2

    const-string v0, "play_logger_context"

    aput-object v0, v1, v4

    .line 180
    invoke-virtual {p0}, Lcom/google/android/gms/playlog/store/i;->a()Lcom/google/android/gms/playlog/store/j;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/playlog/store/j;->b:Ljava/lang/Integer;

    if-eq v3, v2, :cond_0

    iget-object v3, v0, Lcom/google/android/gms/playlog/store/j;->b:Ljava/lang/Integer;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/google/android/gms/playlog/store/j;->b:Ljava/lang/Integer;

    invoke-virtual {v3, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/gms/playlog/store/j;->a([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 184
    :cond_1
    :goto_1
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 185
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 186
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 187
    invoke-static {v3, p2}, Lcom/google/android/gms/playlog/store/i;->a([BI)Z

    move-result v4

    if-nez v4, :cond_3

    .line 188
    const-string v3, "PlayLoggerContextTable"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SQLite database row is corrupted: id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 200
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 180
    :cond_2
    iput-object v2, v0, Lcom/google/android/gms/playlog/store/j;->b:Ljava/lang/Integer;

    iput-boolean v4, v0, Lcom/google/android/gms/playlog/store/j;->c:Z

    goto :goto_0

    .line 192
    :cond_3
    :try_start_1
    invoke-static {v3, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 193
    invoke-static {}, Lcom/google/android/gms/playlog/store/i;->c()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 194
    const-string v3, "PlayLoggerContextTable"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SELECT: id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 202
    :goto_2
    return-wide v0

    .line 200
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 202
    const-wide/16 v0, -0x1

    goto :goto_2
.end method

.method private static c()Z
    .locals 1

    .prologue
    .line 255
    sget-object v0, Lcom/google/android/gms/playlog/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/playlog/store/h;)J
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 212
    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    .line 213
    array-length v0, v2

    const/4 v1, 0x0

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/common/util/aj;->a([BII)I

    move-result v3

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/playlog/store/i;->a:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 215
    invoke-static {v4}, Lcom/google/android/gms/playlog/store/a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 218
    :try_start_0
    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/playlog/store/i;->b([BI)J

    move-result-wide v0

    .line 219
    cmp-long v5, v0, v8

    if-ltz v5, :cond_0

    .line 220
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 237
    :goto_0
    return-wide v0

    .line 224
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 225
    const-string v1, "hash"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 226
    const-string v1, "play_logger_context"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 227
    const-string v1, "play_logger_context"

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 228
    invoke-static {}, Lcom/google/android/gms/playlog/store/i;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    const-string v2, "PlayLoggerContextTable"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "INSERT: id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_1
    cmp-long v2, v0, v8

    if-gez v2, :cond_2

    .line 232
    new-instance v2, Landroid/database/SQLException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "INSERT: id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " loggerContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 236
    :cond_2
    :try_start_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 237
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0
.end method

.method public final a()Lcom/google/android/gms/playlog/store/j;
    .locals 3

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/gms/playlog/store/j;

    iget-object v1, p0, Lcom/google/android/gms/playlog/store/i;->a:Landroid/database/sqlite/SQLiteOpenHelper;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/playlog/store/j;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;B)V

    return-object v0
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/playlog/store/i;->a:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 251
    const-string v1, "play_logger_context"

    const-string v2, "NOT EXISTS (SELECT 1 FROM log_event WHERE log_event.play_logger_context_id = play_logger_context._id)"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method
