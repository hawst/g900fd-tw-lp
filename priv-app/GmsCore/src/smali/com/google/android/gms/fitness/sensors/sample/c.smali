.class public final Lcom/google/android/gms/fitness/sensors/sample/c;
.super Lcom/google/android/gms/fitness/sensors/b/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final c:Ljava/util/Map;


# instance fields
.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Map;

.field private final f:Landroid/content/Context;

.field private final g:J

.field private final h:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/sensors/sample/c;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 36
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/sensors/sample/c;->c:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(JLandroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p4}, Lcom/google/android/gms/fitness/sensors/b/c;-><init>(Lcom/google/android/gms/fitness/sensors/a;)V

    .line 40
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->d:Ljava/util/Map;

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->e:Ljava/util/Map;

    .line 63
    iput-wide p1, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->g:J

    .line 64
    iput-object p3, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->f:Landroid/content/Context;

    .line 65
    iput-object p5, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->h:Ljava/util/Map;

    .line 66
    return-void
.end method

.method static a(J)Lcom/google/android/gms/fitness/sensors/sample/c;
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/gms/fitness/sensors/sample/c;->c:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/sample/c;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;Ljava/util/Map;)Lcom/google/android/gms/fitness/sensors/sample/c;
    .locals 7

    .prologue
    .line 49
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 50
    new-instance v1, Lcom/google/android/gms/fitness/sensors/sample/c;

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/fitness/sensors/sample/c;-><init>(JLandroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;Ljava/util/Map;)V

    .line 52
    sget-object v0, Lcom/google/android/gms/fitness/sensors/sample/c;->c:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-object v1
.end method

.method private b(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/android/gms/fitness/sensors/sample/d;
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    .line 70
    if-nez v0, :cond_0

    .line 71
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/sample/d;

    return-object v0
.end method


# virtual methods
.method final a(I)Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 78
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/sensors/sample/c;->b(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/android/gms/fitness/sensors/sample/d;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_1

    .line 80
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->f()J

    move-result-wide v2

    iget-wide v4, v0, Lcom/google/android/gms/fitness/sensors/sample/d;->c:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 81
    sget-object v0, Lcom/google/android/gms/fitness/sensors/sample/c;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->d:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->e:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/sensors/sample/c;->b(I)V

    .line 88
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    .line 90
    :cond_0
    const-string v1, "sample rate is smaller than %sus"

    new-array v2, v6, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, v0, Lcom/google/android/gms/fitness/sensors/sample/d;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->a:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/l;)Z
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 99
    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->a:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/data/l;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/fitness/sensors/a;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->a:Lcom/google/android/gms/fitness/sensors/a;

    return-object v0
.end method

.method final b(I)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    .line 117
    if-nez v0, :cond_0

    .line 118
    const-string v0, "request no longer valid %s"

    new-array v1, v10, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 142
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/sensors/sample/c;->b(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/android/gms/fitness/sensors/sample/d;

    move-result-object v2

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->f:Landroid/content/Context;

    const-string v3, "alarm"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 125
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->f:Landroid/content/Context;

    const-class v5, Lcom/google/android/gms/fitness/sensors/sample/CollectSensorReceiver;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    const-string v4, "adapter_id"

    iget-wide v6, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->g:J

    invoke-virtual {v3, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 127
    const-string v4, "request_id"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 128
    const-string v4, "max_sample_points"

    iget v5, v2, Lcom/google/android/gms/fitness/sensors/sample/d;->b:I

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 129
    const-string v4, "max_sample_secs"

    iget v2, v2, Lcom/google/android/gms/fitness/sensors/sample/d;->a:I

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 131
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/sample/c;->f:Landroid/content/Context;

    const/high16 v4, 0x10000000

    invoke-static {v2, p1, v3, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 134
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->f()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 135
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long/2addr v6, v4

    .line 137
    const-string v0, "Next manual Sample in %sms (%tT)"

    new-array v3, v11, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v3, v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    add-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 141
    invoke-virtual {v1, v11, v6, v7, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method
