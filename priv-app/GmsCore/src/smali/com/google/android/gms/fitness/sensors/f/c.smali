.class final Lcom/google/android/gms/fitness/sensors/f/c;
.super Lcom/google/android/gms/fitness/sensors/b/a;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/content/Intent;

.field final c:Ljava/util/concurrent/ConcurrentMap;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/Intent;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/b/a;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/f/c;->a:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/f/c;->b:Landroid/content/Intent;

    .line 67
    iput-object p3, p0, Lcom/google/android/gms/fitness/sensors/f/c;->d:Ljava/util/List;

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/f/c;->e:Ljava/util/concurrent/atomic/AtomicReference;

    .line 69
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/f/c;->c:Ljava/util/concurrent/ConcurrentMap;

    .line 70
    return-void
.end method

.method private b()Lcom/google/k/k/a/af;
    .locals 6

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/f/c;->e:Ljava/util/concurrent/atomic/AtomicReference;

    monitor-enter v1

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/f/c;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/f/c;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/k/a/af;

    monitor-exit v1

    .line 106
    :goto_0
    return-object v0

    .line 93
    :cond_0
    const-string v0, "Connecting to service %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/f/c;->b:Landroid/content/Intent;

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 94
    new-instance v0, Lcom/google/android/gms/fitness/f/h;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/f/c;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0, v2}, Lcom/google/android/gms/fitness/f/h;-><init>(Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 95
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/f/c;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/f/c;->b:Landroid/content/Intent;

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v0, v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 97
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/f/c;->e:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v3, Lcom/google/android/gms/fitness/sensors/f/d;

    invoke-direct {v3, p0}, Lcom/google/android/gms/fitness/sensors/f/d;-><init>(Lcom/google/android/gms/fitness/sensors/f/c;)V

    invoke-static {v0, v3}, Lcom/google/k/k/a/n;->b(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/af;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/f/c;->e:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/k/k/a/af;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/sensors/f/c;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    .line 205
    :goto_0
    return-object v0

    .line 174
    :cond_0
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v0

    .line 175
    new-instance v1, Lcom/google/android/gms/fitness/sensors/f/g;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/f/g;-><init>(Lcom/google/android/gms/fitness/sensors/f/c;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Lcom/google/k/k/a/aq;)V

    .line 184
    new-instance v2, Lcom/google/android/gms/fitness/sensors/f/h;

    invoke-direct {v2, p0, p1, v1, v0}, Lcom/google/android/gms/fitness/sensors/f/h;-><init>(Lcom/google/android/gms/fitness/sensors/f/c;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Lcom/google/android/gms/fitness/internal/ab;Lcom/google/k/k/a/aq;)V

    .line 204
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/f/c;->b()Lcom/google/k/k/a/af;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;)Z
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->e()Lcom/google/android/gms/fitness/data/Application;

    move-result-object v0

    .line 80
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/fitness/sensors/f/c;->a(Lcom/google/android/gms/fitness/data/DataType;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Application;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/f/c;->b:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataType;)Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/f/c;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/l;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/f/c;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 223
    if-nez v0, :cond_0

    .line 224
    const-string v0, "Couldn\'t find a data source for listener %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 253
    :goto_0
    return v0

    .line 229
    :cond_0
    new-instance v1, Lcom/google/android/gms/fitness/sensors/f/i;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/fitness/sensors/f/i;-><init>(Lcom/google/android/gms/fitness/sensors/f/c;Lcom/google/android/gms/fitness/data/l;)V

    .line 239
    new-instance v3, Lcom/google/android/gms/fitness/sensors/f/j;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/gms/fitness/sensors/f/j;-><init>(Lcom/google/android/gms/fitness/sensors/f/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/internal/ac;)V

    .line 252
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/f/c;->b()Lcom/google/k/k/a/af;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    move v0, v2

    .line 253
    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 112
    invoke-virtual {p0, p1}, Lcom/google/android/gms/fitness/sensors/f/c;->a(Lcom/google/android/gms/fitness/data/DataType;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    .line 117
    :cond_0
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v0

    .line 118
    new-instance v1, Lcom/google/android/gms/fitness/sensors/f/e;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/fitness/sensors/f/e;-><init>(Lcom/google/android/gms/fitness/sensors/f/c;Lcom/google/k/k/a/aq;)V

    .line 135
    new-instance v2, Lcom/google/android/gms/fitness/sensors/f/f;

    invoke-direct {v2, p0, p1, v1, v0}, Lcom/google/android/gms/fitness/sensors/f/f;-><init>(Lcom/google/android/gms/fitness/sensors/f/c;Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/internal/j;Lcom/google/k/k/a/aq;)V

    .line 153
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/f/c;->b()Lcom/google/k/k/a/af;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    .line 155
    const-wide/16 v2, 0x12c

    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/k/k/a/aq;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 156
    :catch_0
    move-exception v0

    const-string v1, "Interrupted while waiting on FitnessSensorService"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 163
    :goto_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 158
    :catch_1
    move-exception v0

    const-string v1, "Execution exception waiting on FitnessSensorService"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 161
    :catch_2
    move-exception v0

    const-string v0, "Application %s didn\'t respond in time"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/f/c;->b:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
