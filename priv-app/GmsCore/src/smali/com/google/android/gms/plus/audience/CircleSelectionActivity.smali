.class public Lcom/google/android/gms/plus/audience/CircleSelectionActivity;
.super Lcom/google/android/gms/plus/audience/n;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/n;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 45
    sget v0, Lcom/google/android/gms/p;->sY:I

    return v0
.end method

.method protected synthetic a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/plus/audience/al;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->n()Lcom/google/android/gms/plus/audience/ac;

    move-result-object v0

    return-object v0
.end method

.method protected d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/gms/plus/a/n;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method protected final l()Lcom/google/android/gms/plus/audience/an;
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleSelectionActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "audienceSelectionList"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/an;

    return-object v0
.end method

.method protected n()Lcom/google/android/gms/plus/audience/ac;
    .locals 4

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/plus/audience/ac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/ac;

    move-result-object v0

    return-object v0
.end method
