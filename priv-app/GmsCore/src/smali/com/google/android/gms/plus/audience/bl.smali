.class public final Lcom/google/android/gms/plus/audience/bl;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Lcom/google/android/gms/plus/audience/AudienceAvatarImageView;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/ImageView;

.field private g:Lcom/google/android/gms/common/api/v;

.field private h:Lcom/google/android/gms/common/people/data/AudienceMember;

.field private i:Ljava/lang/String;

.field private j:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 575
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/bl;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/bl;->j:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private a(ILandroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 606
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/bn;

    .line 607
    if-eqz v0, :cond_0

    .line 608
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/plus/audience/bn;->a(ILandroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V

    .line 610
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/bl;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bl;->d()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/bl;Landroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 63
    const/4 v0, -0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/plus/audience/bl;->a(ILandroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/bl;Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 63
    new-instance v0, Lcom/google/android/gms/common/audience/a/h;

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/google/android/gms/common/audience/a/h;-><init>(ILcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/audience/a/h;->a()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x7

    invoke-direct {p0, v1, v0, v3}, Lcom/google/android/gms/plus/audience/bl;->a(ILandroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/bl;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/support/v4/app/q;->overridePendingTransition(II)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->a:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/audience/br;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/audience/br;-><init>(Lcom/google/android/gms/plus/audience/bl;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bl;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private static b(I)I
    .locals 2

    .prologue
    const/high16 v1, -0x1000000

    .line 306
    and-int v0, p0, v1

    if-nez v0, :cond_0

    .line 307
    or-int/2addr p0, v1

    .line 309
    :cond_0
    return p0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/bl;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/bl;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/bl;->i:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 166
    new-instance v0, Lcom/google/android/gms/common/audience/a/h;

    invoke-direct {v0, v1, v2, v1, v2}, Lcom/google/android/gms/common/audience/a/h;-><init>(ILcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/audience/a/h;->a()Landroid/content/Intent;

    move-result-object v0

    .line 172
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/gms/plus/audience/bl;->a(ILandroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V

    .line 173
    return-void
.end method

.method private c(I)I
    .locals 3

    .prologue
    .line 313
    const/4 v0, 0x1

    int-to-float v1, p1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/audience/bl;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 176
    new-instance v0, Lcom/google/android/gms/common/audience/a/h;

    const/4 v1, 0x2

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/google/android/gms/common/audience/a/h;-><init>(ILcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/audience/a/h;->a()Landroid/content/Intent;

    move-result-object v0

    .line 182
    const/4 v1, 0x7

    invoke-direct {p0, v1, v0, v3}, Lcom/google/android/gms/plus/audience/bl;->a(ILandroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V

    .line 183
    return-void
.end method

.method private d()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0xb

    const/4 v2, 0x1

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 321
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->cB:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 322
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 337
    :goto_0
    return-void

    .line 328
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->cA:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 330
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_2

    .line 331
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 333
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Lcom/google/android/gms/plus/audience/bp;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/plus/audience/bp;-><init>(Lcom/google/android/gms/plus/audience/bl;Z)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/gms/plus/audience/bp;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/plus/audience/bp;-><init>(Lcom/google/android/gms/plus/audience/bl;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/audience/bl;)V
    .locals 4

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bl;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInputFromWindow(Landroid/os/IBinder;II)V

    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->j:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/g;->l:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    const/16 v2, 0x50

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/audience/bl;->c(I)I

    move-result v2

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->d:Lcom/google/android/gms/plus/audience/AudienceAvatarImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceAvatarImageView;->setVisibility(I)V

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->d:Lcom/google/android/gms/plus/audience/AudienceAvatarImageView;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bl;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceAvatarImageView;->a(Landroid/graphics/Bitmap;)V

    .line 358
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 352
    goto :goto_0

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->d:Lcom/google/android/gms/plus/audience/AudienceAvatarImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceAvatarImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic e(Lcom/google/android/gms/plus/audience/bl;)V
    .locals 4

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bl;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInputFromWindow(Landroid/os/IBinder;II)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/plus/audience/bl;)Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/audience/bl;)Lcom/google/android/gms/plus/audience/bn;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/bn;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/plus/audience/bl;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bl;->e()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/plus/audience/bl;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bl;->c()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/plus/audience/bl;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/plus/audience/bl;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->g:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bl;->b()V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 381
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bl;->c()V

    goto :goto_0

    .line 385
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/bn;

    .line 386
    if-eqz v0, :cond_0

    .line 389
    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/bn;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 395
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 396
    const-string v2, "com.google.android.gms.common.audience.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 397
    const-string v3, "com.google.android.gms.common.audience.EXTRA_PAGE_ID"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 398
    sget-object v3, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/bl;->g:Lcom/google/android/gms/common/api/v;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/bl;->i:Ljava/lang/String;

    invoke-interface {v3, v4, v2, v1, v5}, Lcom/google/android/gms/people/j;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/plus/audience/bq;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/audience/bq;-><init>(Lcom/google/android/gms/plus/audience/bl;B)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 400
    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/bn;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bl;->b()V

    .line 163
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 149
    :goto_0
    if-eqz v0, :cond_0

    .line 150
    sget-object v1, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/bl;->g:Lcom/google/android/gms/common/api/v;

    const/4 v3, 0x3

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/audience/bm;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/plus/audience/bm;-><init>(Lcom/google/android/gms/plus/audience/bl;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 153
    :cond_0
    return-void

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 158
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 116
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/plus/audience/bn;

    if-nez v0, :cond_0

    .line 117
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement CircleCreationFragmentHost."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 124
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/bl;->setRetainInstance(Z)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/audience/a/g;->b(Landroid/content/Intent;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 128
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/audience/a/g;->a(Landroid/content/Intent;)I

    move-result v0

    .line 130
    new-instance v1, Lcom/google/android/gms/people/ad;

    invoke-direct {v1}, Lcom/google/android/gms/people/ad;-><init>()V

    iput v0, v1, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v1}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    .line 133
    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    sget-object v2, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->g:Lcom/google/android/gms/common/api/v;

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 137
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 198
    sget v0, Lcom/google/android/gms/l;->dH:I

    invoke-virtual {p1, v0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 200
    sget v0, Lcom/google/android/gms/j;->cO:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->e:Landroid/widget/EditText;

    .line 201
    sget v0, Lcom/google/android/gms/j;->dJ:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->f:Landroid/widget/ImageView;

    .line 202
    sget v0, Lcom/google/android/gms/j;->jx:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceAvatarImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->d:Lcom/google/android/gms/plus/audience/AudienceAvatarImageView;

    .line 203
    sget v0, Lcom/google/android/gms/j;->em:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->a:Landroid/view/View;

    .line 204
    sget v0, Lcom/google/android/gms/j;->en:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->b:Landroid/view/View;

    .line 206
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v3, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xd

    if-ge v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bl;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/16 v2, 0x258

    if-lt v0, v2, :cond_4

    const/16 v0, 0x1ab

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/bl;->c(I)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 208
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/f;->ac:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const-string v2, "com.google.android.gms.common.audience.EXTRA_HEADER_TEXT_COLOR"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 211
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/bl;->b(I)I

    move-result v2

    .line 212
    sget v0, Lcom/google/android/gms/j;->jG:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/google/android/gms/f;->aa:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const-string v6, "com.google.android.gms.common.audience.EXTRA_HEADER_BACKGROUND_COLOR"

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 217
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/bl;->b(I)I

    move-result v1

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 220
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/gms/plus/audience/bo;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/plus/audience/bo;-><init>(Lcom/google/android/gms/plus/audience/bl;B)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->e:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/gms/plus/audience/bt;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/plus/audience/bt;-><init>(Lcom/google/android/gms/plus/audience/bl;B)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 225
    sget v0, Lcom/google/android/gms/j;->tl:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->c:Landroid/view/View;

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->c:Landroid/view/View;

    new-instance v1, Lcom/google/android/gms/plus/audience/bp;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/plus/audience/bp;-><init>(Lcom/google/android/gms/plus/audience/bl;Z)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->a:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    sget v0, Lcom/google/android/gms/j;->db:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 230
    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    new-instance v6, Lcom/google/android/gms/plus/audience/bp;

    invoke-direct {v6, p0, v4}, Lcom/google/android/gms/plus/audience/bp;-><init>(Lcom/google/android/gms/plus/audience/bl;Z)V

    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    new-instance v1, Lcom/google/android/gms/plus/audience/bp;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/plus/audience/bp;-><init>(Lcom/google/android/gms/plus/audience/bl;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/google/android/gms/h;->cv:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 235
    sget-object v6, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 236
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 238
    if-eqz v5, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_a

    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    move v2, v3

    :goto_2
    sget v0, Lcom/google/android/gms/j;->dK:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v2, :cond_5

    const/16 v6, 0x9

    invoke-virtual {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v0, Lcom/google/android/gms/j;->cO:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v2, :cond_6

    sget v6, Lcom/google/android/gms/j;->dK:I

    invoke-virtual {v1, v3, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v0, Lcom/google/android/gms/j;->cP:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v2, :cond_7

    sget v2, Lcom/google/android/gms/j;->dK:I

    invoke-virtual {v0, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    :cond_1
    sget v0, Lcom/google/android/gms/j;->jG:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->th:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    :goto_6
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bl;->e()V

    .line 243
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/bl;->d()V

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v5

    .line 257
    :goto_7
    return-object v0

    .line 206
    :cond_3
    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    goto/16 :goto_0

    :cond_4
    add-int/lit8 v0, v0, -0x20

    goto/16 :goto_1

    .line 238
    :cond_5
    const/16 v6, 0xb

    invoke-virtual {v1, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_3

    :cond_6
    sget v6, Lcom/google/android/gms/j;->dK:I

    invoke-virtual {v1, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_4

    :cond_7
    sget v2, Lcom/google/android/gms/j;->dK:I

    invoke-virtual {v0, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_5

    .line 240
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->ti:I

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/bl;->h:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v6}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 250
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/bl;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->b:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 254
    new-instance v1, Lcom/google/android/gms/plus/audience/bs;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/plus/audience/bs;-><init>(Lcom/google/android/gms/plus/audience/bl;B)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 255
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bl;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    move-object v0, v5

    .line 257
    goto :goto_7

    :cond_a
    move v2, v4

    goto/16 :goto_2
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bl;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 143
    return-void
.end method
