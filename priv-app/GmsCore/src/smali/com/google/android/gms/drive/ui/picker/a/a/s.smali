.class final Lcom/google/android/gms/drive/ui/picker/a/a/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/picker/a/a/r;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:Z


# direct methods
.method public constructor <init>(IZZ)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 26
    iput p1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->a:I

    .line 27
    iput-boolean p2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->b:Z

    .line 28
    iput-boolean p3, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->c:Z

    .line 29
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->b:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 43
    instance-of v1, p1, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    if-nez v1, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v0

    .line 46
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    .line 47
    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->a:I

    iget v2, p1, Lcom/google/android/gms/drive/ui/picker/a/a/s;->a:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->b:Z

    iget-boolean v2, p1, Lcom/google/android/gms/drive/ui/picker/a/a/s;->b:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->c:Z

    iget-boolean v2, p1, Lcom/google/android/gms/drive/ui/picker/a/a/s;->c:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/s;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
