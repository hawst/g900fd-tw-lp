.class public final Lcom/google/android/gms/plus/sharebox/h;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field a:Landroid/widget/CheckBox;

.field b:Landroid/widget/TextView;

.field c:Lcom/google/android/gms/plus/sharebox/l;

.field d:Landroid/widget/Spinner;

.field private e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Lcom/google/android/gms/plus/sharebox/as;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/h;)Lcom/google/android/gms/plus/sharebox/as;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->j:Lcom/google/android/gms/plus/sharebox/as;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/h;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/google/android/gms/plus/sharebox/h;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/plus/sharebox/h;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/google/android/gms/plus/sharebox/h;->h:Z

    return p1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 209
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    iget v0, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 210
    return-void

    .line 209
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    .line 130
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->b:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->m()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/h;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/n;->v:I

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/h;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/p;->vy:I

    new-array v6, v0, [Ljava/lang/Object;

    aput-object v3, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v6, Lcom/google/android/gms/plus/sharebox/i;

    invoke-direct {v6, p0}, Lcom/google/android/gms/plus/sharebox/i;-><init>(Lcom/google/android/gms/plus/sharebox/h;)V

    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v5, v6, v4, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 134
    if-eqz p2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/gms/plus/sharebox/h;->i:Z

    if-eqz v2, :cond_0

    .line 135
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    iget-boolean v3, p0, Lcom/google/android/gms/plus/sharebox/h;->g:Z

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 136
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    iget-boolean v3, p0, Lcom/google/android/gms/plus/sharebox/h;->h:Z

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 141
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->m()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 145
    new-instance v0, Lcom/google/android/gms/plus/sharebox/l;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/h;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/plus/sharebox/l;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->c:Lcom/google/android/gms/plus/sharebox/l;

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->c:Lcom/google/android/gms/plus/sharebox/l;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 148
    if-eqz p2, :cond_2

    iget v0, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    if-ltz v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    iget v1, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 155
    :goto_2
    return-void

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->k()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 141
    goto :goto_1

    .line 150
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_2

    .line 153
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/plus/sharebox/Circle;)V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->c:Lcom/google/android/gms/plus/sharebox/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/sharebox/l;->a(Lcom/google/android/gms/plus/sharebox/Circle;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->c:Lcom/google/android/gms/plus/sharebox/l;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/l;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    iget v1, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 233
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 217
    return-void
.end method

.method public final b()Lcom/google/android/gms/plus/sharebox/AddToCircleData;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->b()Ljava/lang/String;

    move-result-object v0

    .line 248
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/h;->j:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->m()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    .line 249
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->m()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 66
    instance-of v0, p1, Lcom/google/android/gms/plus/sharebox/as;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/sharebox/as;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/sharebox/as;

    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/h;->j:Lcom/google/android/gms/plus/sharebox/as;

    .line 71
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 77
    if-eqz p1, :cond_0

    .line 78
    const-string v0, "add_to_circle_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    .line 79
    const-string v0, "last_position"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    .line 80
    const-string v0, "last_checked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/h;->g:Z

    .line 81
    const-string v0, "last_enabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/h;->h:Z

    .line 84
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/h;->i:Z

    .line 85
    return-void

    .line 84
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 90
    sget v0, Lcom/google/android/gms/l;->eO:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 93
    sget v0, Lcom/google/android/gms/j;->cS:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    .line 94
    sget v0, Lcom/google/android/gms/j;->cT:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->b:Landroid/widget/TextView;

    .line 95
    sget v0, Lcom/google/android/gms/j;->cW:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->d:Landroid/widget/Spinner;

    .line 97
    return-object v1
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 182
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 183
    :goto_0
    if-nez v0, :cond_1

    .line 199
    :goto_1
    return-void

    .line 182
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    goto :goto_0

    .line 186
    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->a(Lcom/google/android/gms/plus/sharebox/Circle;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->j:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->l()V

    goto :goto_1

    .line 191
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    if-eq v1, p3, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 197
    :cond_3
    iput p3, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    goto :goto_1
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    const-string v0, "add_to_circle_data"

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 113
    const-string v0, "last_position"

    iget v2, p0, Lcom/google/android/gms/plus/sharebox/h;->f:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    const-string v2, "last_checked"

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116
    const-string v0, "last_enabled"

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    if-nez v2, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 118
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_0

    .line 116
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/h;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v1

    goto :goto_1
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/h;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/h;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V

    .line 107
    :cond_0
    return-void
.end method
