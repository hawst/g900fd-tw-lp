.class public abstract enum Lcom/google/android/gms/drive/ui/picker/a/a/t;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/ui/picker/a/a/t;

.field public static final enum b:Lcom/google/android/gms/drive/ui/picker/a/a/t;

.field public static final enum c:Lcom/google/android/gms/drive/ui/picker/a/a/t;

.field public static final enum d:Lcom/google/android/gms/drive/ui/picker/a/a/t;

.field public static final e:Ljava/util/Map;

.field private static final synthetic g:[Lcom/google/android/gms/drive/ui/picker/a/a/t;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/u;

    const-string v1, "FOLDERS_THEN_TITLE"

    sget v2, Lcom/google/android/gms/p;->gE:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/u;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->a:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 25
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/v;

    const-string v1, "LAST_MODIFIED"

    sget v2, Lcom/google/android/gms/p;->gB:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->b:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 32
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/w;

    const-string v1, "SHARED_WITH_ME_DATE"

    sget v2, Lcom/google/android/gms/p;->gD:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->c:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 39
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/x;

    const-string v1, "LAST_OPENED_BY_ME"

    sget v2, Lcom/google/android/gms/p;->gC:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->d:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/drive/ui/picker/a/a/t;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/t;->a:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/t;->b:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/t;->c:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/t;->d:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->g:[Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->e:Ljava/util/Map;

    .line 50
    const-class v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 51
    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/t;->e:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 53
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput p3, p0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->f:I

    .line 59
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIB)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/ui/picker/a/a/t;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/a/a/t;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/ui/picker/a/a/t;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->g:[Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/ui/picker/a/a/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/ui/picker/a/a/t;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Lcom/google/android/gms/drive/ui/picker/a/a/g;
.end method
