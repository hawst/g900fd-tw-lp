.class public Lcom/google/android/gms/wallet/common/ui/ButtonBar;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 54
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_2

    .line 55
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->setOrientation(I)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    sget v2, Lcom/google/android/gms/f;->aF:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->setBackgroundColor(I)V

    .line 58
    sget v2, Lcom/google/android/gms/g;->bZ:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 59
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->setPadding(IIII)V

    .line 64
    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 65
    new-array v2, v4, [I

    sget v3, Lcom/google/android/gms/d;->f:I

    aput v3, v2, v1

    invoke-virtual {p1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 67
    sget v3, Lcom/google/android/gms/l;->ho:I

    invoke-virtual {v2, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 68
    invoke-virtual {v0, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 70
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    sget v0, Lcom/google/android/gms/j;->cr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->b:Landroid/widget/Button;

    .line 73
    sget v0, Lcom/google/android/gms/j;->pB:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a:Landroid/widget/Button;

    .line 75
    sget-object v0, Lcom/google/android/gms/r;->aU:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 76
    sget v0, Lcom/google/android/gms/r;->aX:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    sget v0, Lcom/google/android/gms/r;->aX:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    .line 84
    :goto_1
    sget v0, Lcom/google/android/gms/r;->aW:I

    invoke-virtual {v2, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 86
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->b:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 89
    sget v0, Lcom/google/android/gms/r;->aV:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 91
    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->b:Landroid/widget/Button;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->b:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    :cond_1
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 96
    return-void

    .line 61
    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->setOrientation(I)V

    goto :goto_0

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 91
    :cond_4
    const/16 v0, 0x8

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 139
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 143
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 120
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 103
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 104
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 106
    :cond_0
    return-void
.end method
