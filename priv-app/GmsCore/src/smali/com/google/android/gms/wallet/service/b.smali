.class public abstract Lcom/google/android/gms/wallet/service/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/service/k;


# instance fields
.field protected final a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field protected final b:Landroid/accounts/Account;

.field protected final c:Ljava/util/Set;

.field protected final d:Ljava/util/concurrent/PriorityBlockingQueue;

.field protected final e:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field protected final f:Landroid/content/Context;

.field protected final g:Ljava/util/LinkedList;

.field protected final h:Ljava/lang/String;

.field protected i:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/b;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 55
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 56
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/b;->b:Landroid/accounts/Account;

    .line 57
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/b;->f:Landroid/content/Context;

    .line 58
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/b;->c:Ljava/util/Set;

    .line 60
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/b;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 61
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 62
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/b;->g:Ljava/util/LinkedList;

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/b;->b:Landroid/accounts/Account;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/b;->h:Ljava/lang/String;

    .line 64
    return-void
.end method

.method private a(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/b;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/l;

    .line 229
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/service/l;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 231
    :cond_0
    return-void
.end method

.method private declared-synchronized c()V
    .locals 1

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/b;->g:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    monitor-exit p0

    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d()I
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/b;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 205
    if-nez v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/service/l;)V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/service/l;->a(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/b;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 155
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/service/l;I)V
    .locals 6

    .prologue
    .line 175
    if-ltz p2, :cond_2

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 177
    iget v2, v0, Landroid/os/Message;->arg1:I

    if-le v2, p2, :cond_0

    .line 180
    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/service/l;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/b;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 184
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/b;->d()I

    move-result v0

    int-to-long v2, v0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    iget v0, v0, Landroid/os/Message;->arg1:I

    int-to-long v4, v0

    cmp-long v0, v4, v2

    if-lez v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 186
    :cond_2
    return-void
.end method

.method protected final a(ILjava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 73
    const-string v0, "BasePaymentServiceConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleRequest paymentServiceId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/wallet/service/b;->c(ILjava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    .line 76
    if-eqz v2, :cond_0

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 77
    :cond_0
    const-string v0, "BasePaymentServiceConnection"

    const-string v3, "Null response"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 82
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v3

    const/16 v4, 0x16

    if-ne v3, v4, :cond_2

    .line 85
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/b;->b:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/accounts/Account;Ljava/lang/String;)V

    move v0, v1

    .line 96
    :goto_1
    return v0

    .line 80
    :cond_1
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0

    .line 89
    :cond_2
    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/b;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v4

    invoke-static {v3, v0, v4, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 94
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/service/b;->a(Landroid/os/Message;)V

    iget v1, v0, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    iget v1, v0, Landroid/os/Message;->arg1:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/b;->d()I

    move-result v2

    if-le v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/b;->e:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_3
    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/b;->c()V

    .line 96
    const/4 v0, 0x1

    goto :goto_1

    .line 94
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xf -> :sswitch_0
        0x13 -> :sswitch_0
        0x1b -> :sswitch_0
        0x1f -> :sswitch_0
    .end sparse-switch
.end method

.method protected final a(Lcom/google/protobuf/nano/j;)Z
    .locals 1

    .prologue
    .line 114
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/service/b;->a(Lcom/google/protobuf/nano/j;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected final declared-synchronized a(Lcom/google/protobuf/nano/j;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/b;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/common/y;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/b;->g:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    const/4 v0, 0x1

    .line 125
    :goto_0
    monitor-exit p0

    return v0

    .line 124
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/b;->g:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 125
    const/4 v0, 0x0

    goto :goto_0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final b(ILjava/lang/Object;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lcom/google/android/gms/wallet/service/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/wallet/service/c;-><init>(Lcom/google/android/gms/wallet/service/b;ILjava/lang/Object;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/service/c;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 108
    return-void
.end method

.method public final b(Lcom/google/android/gms/wallet/service/l;)V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/service/l;->a(Z)V

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/b;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 163
    return-void
.end method

.method public final b(Lcom/google/android/gms/wallet/service/l;I)V
    .locals 0

    .prologue
    .line 191
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/b;->a(Lcom/google/android/gms/wallet/service/l;)V

    .line 192
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/wallet/service/b;->a(Lcom/google/android/gms/wallet/service/l;I)V

    .line 193
    return-void
.end method

.method public final c(Lcom/google/android/gms/wallet/service/l;)I
    .locals 3

    .prologue
    .line 167
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/b;->b(Lcom/google/android/gms/wallet/service/l;)V

    .line 168
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/l;->h()I

    move-result v0

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/b;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 170
    return v0
.end method

.method protected abstract c(ILjava/lang/Object;)Landroid/util/Pair;
.end method
