.class final Lcom/google/android/gms/fitness/apiary/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/apiary/b;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/gms/fitness/data/Session;

    new-instance v0, Lcom/google/af/a/b/a/a/ah;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/ah;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "session require identifier: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    :cond_1
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/af/a/b/a/a/b;

    invoke-direct {v1}, Lcom/google/af/a/b/a/a/b;-><init>()V

    iput-object v1, v0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    iget-object v1, v0, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->h()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/af/a/b/a/a/b;->a:Ljava/lang/String;

    :cond_2
    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/j;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/16 v9, 0x3e8

    const/16 v8, 0x64

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 36
    check-cast p1, Lcom/google/af/a/b/a/a/ah;

    iget-object v0, p1, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    if-nez v0, :cond_6

    const-string v0, "unknown"

    :goto_0
    new-instance v4, Lcom/google/android/gms/fitness/data/r;

    invoke-direct {v4}, Lcom/google/android/gms/fitness/data/r;-><init>()V

    iget-object v1, p1, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v5, p1, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    if-eqz v5, :cond_7

    invoke-static {v5}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v1

    if-lez v1, :cond_7

    move v1, v2

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    iput-object v5, v4, Lcom/google/android/gms/fitness/data/r;->d:Ljava/lang/String;

    :cond_0
    iget-object v1, p1, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v5, p1, Lcom/google/af/a/b/a/a/ah;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v8, :cond_8

    move v1, v2

    :goto_2
    const-string v6, "Session name cannot exceed %d characters"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v1, v6, v7}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-object v5, v4, Lcom/google/android/gms/fitness/data/r;->c:Ljava/lang/String;

    :cond_1
    iget-object v1, p1, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v5, p1, Lcom/google/af/a/b/a/a/ah;->c:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v9, :cond_9

    move v1, v2

    :goto_3
    const-string v6, "Session description cannot exceed %d characters"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v1, v6, v7}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-object v5, v4, Lcom/google/android/gms/fitness/data/r;->e:Ljava/lang/String;

    :cond_2
    iget-object v1, p1, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/google/af/a/b/a/a/ah;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_a

    :goto_4
    const-string v3, "Start time should be positive."

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    iput-wide v2, v4, Lcom/google/android/gms/fitness/data/r;->a:J

    :cond_3
    iget-object v1, p1, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/google/af/a/b/a/a/ah;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3, v1}, Lcom/google/android/gms/fitness/data/r;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/r;

    :cond_4
    iget-object v1, p1, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/google/af/a/b/a/a/ah;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v4, Lcom/google/android/gms/fitness/data/r;->f:I

    :cond_5
    new-instance v1, Lcom/google/android/gms/fitness/data/Application;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/fitness/data/Application;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v4, Lcom/google/android/gms/fitness/data/r;->g:Lcom/google/android/gms/fitness/data/Application;

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/r;->a()Lcom/google/android/gms/fitness/data/Session;

    move-result-object v0

    return-object v0

    :cond_6
    iget-object v0, p1, Lcom/google/af/a/b/a/a/ah;->g:Lcom/google/af/a/b/a/a/b;

    iget-object v0, v0, Lcom/google/af/a/b/a/a/b;->a:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    move v1, v3

    goto/16 :goto_1

    :cond_8
    move v1, v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    :cond_a
    move v2, v3

    goto :goto_4
.end method
