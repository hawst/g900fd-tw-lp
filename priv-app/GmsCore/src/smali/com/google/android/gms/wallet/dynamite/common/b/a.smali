.class public final Lcom/google/android/gms/wallet/dynamite/common/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 458
    const-string v0, "^[\\-,\\s]+|[\\-,\\s]+$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static a(Lcom/google/t/a/b;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 432
    const-string v0, "\n"

    invoke-static {p0, v0, v1, v1}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->b(Lcom/google/t/a/b;Ljava/lang/String;[C[C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/t/a/b;C)Ljava/lang/String;
    .locals 1

    .prologue
    .line 640
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/t/a/b;CLjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 657
    if-nez p0, :cond_1

    .line 717
    :cond_0
    :goto_0
    return-object v0

    .line 660
    :cond_1
    if-nez p2, :cond_2

    .line 661
    const-string p2, "\n"

    .line 663
    :cond_2
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 674
    :sswitch_0
    iget-object v1, p0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    .line 665
    :sswitch_1
    iget-object v1, p0, Lcom/google/t/a/b;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->d:Ljava/lang/String;

    goto :goto_0

    .line 668
    :sswitch_2
    iget-object v1, p0, Lcom/google/t/a/b;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->f:Ljava/lang/String;

    goto :goto_0

    .line 670
    :sswitch_3
    iget-object v1, p0, Lcom/google/t/a/b;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->s:Ljava/lang/String;

    goto :goto_0

    .line 672
    :sswitch_4
    iget-object v1, p0, Lcom/google/t/a/b;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->r:Ljava/lang/String;

    goto :goto_0

    .line 676
    :sswitch_5
    iget-object v1, p0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    array-length v1, v1

    if-lt v1, v3, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_0

    .line 678
    :sswitch_6
    iget-object v1, p0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    array-length v1, v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    aget-object v0, v0, v3

    goto :goto_0

    .line 680
    :sswitch_7
    iget-object v1, p0, Lcom/google/t/a/b;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->g:Ljava/lang/String;

    goto :goto_0

    .line 683
    :sswitch_8
    iget-object v1, p0, Lcom/google/t/a/b;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->k:Ljava/lang/String;

    goto :goto_0

    .line 686
    :sswitch_9
    iget-object v1, p0, Lcom/google/t/a/b;->m:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->m:Ljava/lang/String;

    goto :goto_0

    .line 688
    :sswitch_a
    iget-object v1, p0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 692
    :sswitch_b
    iget-object v1, p0, Lcom/google/t/a/b;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 695
    :sswitch_c
    iget-object v1, p0, Lcom/google/t/a/b;->p:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 697
    :sswitch_d
    iget-object v1, p0, Lcom/google/t/a/b;->o:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 699
    :sswitch_e
    iget-object v1, p0, Lcom/google/t/a/b;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 702
    :sswitch_f
    iget-object v1, p0, Lcom/google/t/a/b;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/t/a/b;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 705
    :sswitch_10
    iget-object v1, p0, Lcom/google/t/a/b;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 706
    iget-object v0, p0, Lcom/google/t/a/b;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 708
    :cond_3
    iget-object v1, p0, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v1

    .line 709
    if-eqz v1, :cond_0

    const/16 v2, 0x35a

    if-eq v2, v1, :cond_0

    .line 710
    iget-object v0, p0, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 663
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_5
        0x33 -> :sswitch_6
        0x41 -> :sswitch_a
        0x42 -> :sswitch_f
        0x43 -> :sswitch_2
        0x44 -> :sswitch_7
        0x46 -> :sswitch_c
        0x4e -> :sswitch_3
        0x4f -> :sswitch_4
        0x50 -> :sswitch_d
        0x52 -> :sswitch_10
        0x53 -> :sswitch_1
        0x54 -> :sswitch_e
        0x55 -> :sswitch_b
        0x58 -> :sswitch_9
        0x5a -> :sswitch_8
    .end sparse-switch
.end method

.method public static a(Lcom/google/t/a/b;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 421
    invoke-static {p0, p1, v0, v0}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->b(Lcom/google/t/a/b;Ljava/lang/String;[C[C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/t/a/b;Ljava/lang/String;[C[C)Ljava/lang/String;
    .locals 1

    .prologue
    .line 451
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->b(Lcom/google/t/a/b;Ljava/lang/String;[C[C)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/google/t/a/b;Ljava/lang/String;[C[C)Ljava/lang/String;
    .locals 22

    .prologue
    .line 483
    if-nez p1, :cond_0

    .line 484
    const-string p1, "\n"

    .line 487
    :cond_0
    if-nez p2, :cond_1

    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    array-length v3, v0

    if-lez v3, :cond_1

    .line 489
    invoke-static {}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->a()[C

    move-result-object p2

    .line 491
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v3

    .line 493
    if-nez v3, :cond_2

    .line 494
    const/16 v3, 0x35a

    .line 496
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/t/a/b;->c:Ljava/lang/String;

    .line 497
    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(ILjava/lang/String;)Z

    move-result v5

    .line 498
    sparse-switch v3, :sswitch_data_0

    const-string v3, "%N%n%O%n%A%n%C"

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x4

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    if-nez v5, :cond_b

    const-string v5, "ja"

    invoke-static {v5, v4}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "ko"

    invoke-static {v5, v4}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "zh"

    invoke-static {v5, v4}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    :cond_3
    const-string v4, "%R%n"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 501
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 502
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 503
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 504
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v15

    .line 507
    if-eqz p2, :cond_e

    .line 509
    new-instance v3, Landroid/util/SparseBooleanArray;

    invoke-static {}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v3, v4}, Landroid/util/SparseBooleanArray;-><init>(I)V

    .line 510
    move-object/from16 v0, p2

    array-length v5, v0

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v5, :cond_c

    aget-char v6, p2, v4

    .line 511
    invoke-static {v6}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->a(C)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 512
    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 510
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 498
    :sswitch_0
    if-eqz v5, :cond_5

    const-string v3, "%N%n%O%n%A, %D%n%C%n%S, %Z"

    goto :goto_0

    :cond_5
    const-string v3, "%Z%n%S%C%D%n%A%n%O%n%N"

    goto :goto_0

    :sswitch_1
    if-eqz v5, :cond_6

    const-string v3, "%N%n%O%n%A%n%D%n%C%n%S%n%Z"

    goto :goto_0

    :cond_6
    const-string v3, "%S %C%D%n%A%n%O%n%N%n%Z"

    goto :goto_0

    :sswitch_2
    if-eqz v5, :cond_7

    const-string v3, "%N%n%O%n%A%n%C, %S %Z"

    goto :goto_0

    :cond_7
    const-string v3, "%Z%n%S%C%n%A%n%O%n%N"

    goto :goto_0

    :sswitch_3
    if-eqz v5, :cond_8

    const-string v3, "%N%n%O%n%A"

    goto/16 :goto_0

    :cond_8
    const-string v3, "%A%n%O%n%N"

    goto/16 :goto_0

    :sswitch_4
    if-eqz v5, :cond_9

    const-string v3, "%N%n%O%n%A%n%C, %S%n%Z"

    goto/16 :goto_0

    :cond_9
    const-string v3, "\u3012%Z%n%S%C%n%A%n%O%n%N"

    goto/16 :goto_0

    :sswitch_5
    if-eqz v5, :cond_a

    const-string v3, "%N%n%O%n%A%n%S"

    goto/16 :goto_0

    :cond_a
    const-string v3, "%S%n%A%n%O%n%N"

    goto/16 :goto_0

    :sswitch_6
    const-string v3, "%N%n%O%n%A%n%Z %C%n%S"

    goto/16 :goto_0

    :sswitch_7
    const-string v3, "%N%n%O%n%A%n%C - %Z"

    goto/16 :goto_0

    :sswitch_8
    const-string v3, "%N%n%O%n%A%n%C %Z, %S"

    goto/16 :goto_0

    :sswitch_9
    const-string v3, "%N%n%O%n%A%n%C%n%S%n%Z"

    goto/16 :goto_0

    :sswitch_a
    const-string v3, "%N%n%O%n%A%n%S%n%C"

    goto/16 :goto_0

    :sswitch_b
    const-string v3, "%N%n%O%n%A%n%Z %C, %S"

    goto/16 :goto_0

    :sswitch_c
    const-string v3, "%N%n%O%n%A%n%C %X%n%S"

    goto/16 :goto_0

    :sswitch_d
    const-string v3, "%N%n%O%n%A%n%X%n%C%nGUERNSEY%n%Z"

    goto/16 :goto_0

    :sswitch_e
    const-string v3, "%Z%n%S%n%C%n%A%n%O%n%N"

    goto/16 :goto_0

    :sswitch_f
    const-string v3, "%N%n%O%n%A%n%C PR %Z"

    goto/16 :goto_0

    :sswitch_10
    const-string v3, "%N%n%O%n%A%n%Z%n%C, %S"

    goto/16 :goto_0

    :sswitch_11
    const-string v3, "%O%n%N%n%A%nSE-%Z %C"

    goto/16 :goto_0

    :sswitch_12
    const-string v3, "%N%n%O%n%A%nSINGAPORE %Z"

    goto/16 :goto_0

    :sswitch_13
    const-string v3, "%N%n%O%n%A%n%X%n%C%nJERSEY%n%Z"

    goto/16 :goto_0

    :sswitch_14
    const-string v3, "%O%n%N%n%A%nFL-%Z %C"

    goto/16 :goto_0

    :sswitch_15
    const-string v3, "%O%n%N%n%A%n%C-%S%n%Z"

    goto/16 :goto_0

    :sswitch_16
    const-string v3, "%N%n%O%n%X %A %C %X"

    goto/16 :goto_0

    :sswitch_17
    const-string v3, "%N%n%O%n%A%nHT%Z %C %X"

    goto/16 :goto_0

    :sswitch_18
    const-string v3, "%N%n%O%n%C%n%A%n%Z"

    goto/16 :goto_0

    :sswitch_19
    const-string v3, "%N%n%O%n%A%n%C%n%S"

    goto/16 :goto_0

    :sswitch_1a
    const-string v3, "%N%n%O%n%A%n%Z %C %S"

    goto/16 :goto_0

    :sswitch_1b
    const-string v3, "%N%n%O%n%A%n%C, %Z"

    goto/16 :goto_0

    :sswitch_1c
    const-string v3, "%N%n%O%n%A%n%C%n%Z"

    goto/16 :goto_0

    :sswitch_1d
    const-string v3, "%N%n%O%n%A%n%C%n%S %Z"

    goto/16 :goto_0

    :sswitch_1e
    const-string v3, "%N%n%O%n%A%n%C, %S %Z"

    goto/16 :goto_0

    :sswitch_1f
    const-string v3, "%N%n%O%n%A%n%C%n%S %X"

    goto/16 :goto_0

    :sswitch_20
    const-string v3, "%N%n%O%n%A%n%Z- %C"

    goto/16 :goto_0

    :sswitch_21
    const-string v3, "%N%n%O%n%A%n%C, %S"

    goto/16 :goto_0

    :sswitch_22
    const-string v3, "%N%n%O%n%A%nMD-%Z %C"

    goto/16 :goto_0

    :sswitch_23
    const-string v3, "%N%n%O%n%A%n%C-%Z"

    goto/16 :goto_0

    :sswitch_24
    const-string v3, "%N%n%O%n%A%n%Z %C %X"

    goto/16 :goto_0

    :sswitch_25
    const-string v3, "%O%n%N%n%A%nAX-%Z %C%n\u00c5LAND"

    goto/16 :goto_0

    :sswitch_26
    const-string v3, "%N%n%O%n%Z %A %C"

    goto/16 :goto_0

    :sswitch_27
    const-string v3, "%N%n%O%n%A%n%Z%n%C%n%S"

    goto/16 :goto_0

    :sswitch_28
    const-string v3, "%O%n%N%n%A%nCH-%Z %C"

    goto/16 :goto_0

    :sswitch_29
    const-string v3, "%N%n%O%n%A%n%C, %S%n%Z"

    goto/16 :goto_0

    :sswitch_2a
    const-string v3, "%O%n%N%n%A%nL-%Z %C"

    goto/16 :goto_0

    :sswitch_2b
    const-string v3, "%O%n%N%n%A%n%Z %C %X"

    goto/16 :goto_0

    :sswitch_2c
    const-string v3, "%N%n%O%n%A%n%X%n%C%n%Z"

    goto/16 :goto_0

    :sswitch_2d
    const-string v3, "%N%n%O%n%A%n%C %S %Z"

    goto/16 :goto_0

    :sswitch_2e
    const-string v3, "%O%n%N%n%A%n%Z %C"

    goto/16 :goto_0

    :sswitch_2f
    const-string v3, "%Z %C%n%A%n%O%n%N"

    goto/16 :goto_0

    :sswitch_30
    const-string v3, "%N%n%O%n%A%nSI- %Z %C"

    goto/16 :goto_0

    :sswitch_31
    const-string v3, "%Z %C %X%n%A%n%O%n%N"

    goto/16 :goto_0

    :sswitch_32
    const-string v3, "%S%n%Z %C %X%n%A%n%O%n%N"

    goto/16 :goto_0

    :sswitch_33
    const-string v3, "%N%n%O%n%A%nMC-%Z %C %X"

    goto/16 :goto_0

    :sswitch_34
    const-string v3, "%N%n%O%n%A%nAZ %Z %C"

    goto/16 :goto_0

    :sswitch_35
    const-string v3, "%N%n%O%n%A%n%C %X"

    goto/16 :goto_0

    :sswitch_36
    const-string v3, "%N%n%O%n%A%nFO%Z %C"

    goto/16 :goto_0

    :sswitch_37
    const-string v3, "%N%n%O%n%A%n%C %Z"

    goto/16 :goto_0

    :sswitch_38
    const-string v3, "%N%n%O%n%A%n%C %Z%n%S"

    goto/16 :goto_0

    :sswitch_39
    const-string v3, "%N%n%O%n%A%n%Z%n%C"

    goto/16 :goto_0

    :sswitch_3a
    const-string v3, "%N%n%O%n%A"

    goto/16 :goto_0

    :sswitch_3b
    const-string v3, "%N%n%O%n%A%nHR-%Z %C"

    goto/16 :goto_0

    :sswitch_3c
    const-string v3, "%N%n%O%n%A%n%C"

    goto/16 :goto_0

    :sswitch_3d
    const-string v3, "%O%n%N%n%A%nLT-%Z %C"

    goto/16 :goto_0

    :sswitch_3e
    const-string v3, "%O%n%N%n%A%n%C, %S%n%Z"

    goto/16 :goto_0

    :sswitch_3f
    const-string v3, "%O%n%N%n%A%nFI-%Z %C"

    goto/16 :goto_0

    :sswitch_40
    const-string v3, "%N%n%O%n%A%n%S %C-%X%n%Z"

    goto/16 :goto_0

    :sswitch_41
    const-string v3, "%Z %C  %n%A%n%O%n%N"

    goto/16 :goto_0

    :sswitch_42
    const-string v3, "%O%n%N%n%A%n%C %S %Z"

    goto/16 :goto_0

    :sswitch_43
    const-string v3, "%N%n%O%n%A%n%Z %C"

    goto/16 :goto_0

    :sswitch_44
    const-string v3, "%N%n%O%n%A%n%S"

    goto/16 :goto_0

    :sswitch_45
    const-string v3, "%N%n%O%n%A%n%X%n%C%n%S"

    goto/16 :goto_0

    :sswitch_46
    const-string v3, "%N%n%O%n%A%n%C %Z %S"

    goto/16 :goto_0

    :sswitch_47
    const-string v3, "%N%n%O%n%A%n%Z-%C%n%S"

    goto/16 :goto_0

    :sswitch_48
    const-string v3, "%N%n%O%n%A%n%Z %S"

    goto/16 :goto_0

    :cond_b
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "%n%R"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 516
    :cond_c
    if-eqz p3, :cond_f

    .line 517
    move-object/from16 v0, p3

    array-length v5, v0

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v5, :cond_f

    aget-char v6, p3, v4

    .line 518
    invoke-static {v6}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->a(C)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 519
    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 517
    :cond_d
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 525
    :cond_e
    const/4 v3, 0x0

    .line 528
    :cond_f
    const/4 v10, 0x0

    .line 529
    const/4 v5, 0x0

    .line 530
    const/4 v9, 0x0

    .line 531
    const/4 v8, 0x0

    .line 532
    const/4 v7, 0x0

    .line 533
    const/4 v6, 0x0

    .line 535
    const-string v4, "%A"

    const-string v16, "%1%n%2%n%3"

    move-object/from16 v0, v16

    invoke-virtual {v11, v4, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 537
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v16

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    const/4 v4, 0x0

    move v11, v4

    move v4, v5

    move v5, v10

    :goto_4
    move/from16 v0, v17

    if-ge v11, v0, :cond_1b

    aget-char v18, v16, v11

    .line 538
    if-eqz v5, :cond_19

    .line 539
    const/4 v10, 0x0

    .line 540
    const/16 v5, 0x6e

    move/from16 v0, v18

    if-ne v5, v0, :cond_12

    .line 542
    const/4 v5, 0x0

    .line 543
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v18

    if-lez v18, :cond_11

    .line 544
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 545
    sget-object v5, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    const/4 v5, 0x1

    .line 547
    const/4 v6, 0x0

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    move v6, v7

    .line 560
    :goto_5
    if-eqz v5, :cond_10

    if-lez v15, :cond_10

    .line 561
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    const/4 v4, 0x1

    .line 564
    :cond_10
    const/4 v5, 0x0

    move v7, v9

    move/from16 v20, v6

    move v6, v8

    move v8, v4

    move v4, v5

    move/from16 v5, v20

    .line 592
    :goto_6
    const/4 v9, 0x0

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    move v9, v7

    move v7, v5

    move v5, v10

    move/from16 v20, v4

    move v4, v8

    move v8, v6

    move/from16 v6, v20

    .line 537
    :goto_7
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_4

    .line 548
    :cond_11
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v18

    if-lez v18, :cond_24

    if-nez v6, :cond_24

    .line 552
    sget-object v6, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v14}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 554
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_24

    .line 555
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    const/4 v5, 0x1

    .line 557
    const/4 v7, 0x1

    move v6, v7

    goto :goto_5

    .line 566
    :cond_12
    invoke-static/range {v18 .. v18}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->a(C)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 567
    if-eqz v3, :cond_13

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 568
    :cond_13
    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 570
    if-eqz v5, :cond_14

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 571
    :goto_8
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_15

    .line 572
    const/4 v5, 0x1

    move v6, v8

    move v8, v4

    move v4, v5

    move v5, v7

    move v7, v9

    goto :goto_6

    .line 570
    :cond_14
    const/4 v5, 0x0

    goto :goto_8

    .line 575
    :cond_15
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 576
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    const/16 v5, 0x4e

    move/from16 v0, v18

    if-ne v5, v0, :cond_16

    .line 578
    const/4 v5, 0x1

    move/from16 v20, v6

    move v6, v8

    move v8, v4

    move/from16 v4, v20

    move/from16 v21, v5

    move v5, v7

    move/from16 v7, v21

    goto/16 :goto_6

    .line 580
    :cond_16
    const/4 v5, 0x1

    move v8, v4

    move v4, v6

    move v6, v5

    move v5, v7

    move v7, v9

    .line 583
    goto/16 :goto_6

    .line 584
    :cond_17
    const/4 v5, 0x1

    move v6, v8

    move v8, v4

    move v4, v5

    move v5, v7

    move v7, v9

    goto/16 :goto_6

    .line 587
    :cond_18
    const-string v5, "AddressFormatter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v19, "Could not format AddressField."

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    const/4 v5, 0x1

    move v6, v8

    move v8, v4

    move v4, v5

    move v5, v7

    move v7, v9

    goto/16 :goto_6

    .line 593
    :cond_19
    const/16 v10, 0x25

    move/from16 v0, v18

    if-ne v0, v10, :cond_1a

    .line 594
    const/4 v5, 0x1

    goto/16 :goto_7

    .line 597
    :cond_1a
    move/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 602
    :cond_1b
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-gtz v5, :cond_1c

    if-nez v6, :cond_23

    .line 603
    :cond_1c
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-nez v4, :cond_1d

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_1d

    .line 604
    const/4 v7, 0x1

    .line 606
    :cond_1d
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 607
    sget-object v4, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 614
    :cond_1e
    :goto_9
    if-eqz v3, :cond_1f

    const/16 v4, 0x52

    invoke-virtual {v3, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 615
    :cond_1f
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_20

    if-eqz v9, :cond_22

    if-nez v8, :cond_22

    if-nez v7, :cond_22

    .line 617
    :cond_20
    const/16 v3, 0x52

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 619
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_22

    .line 620
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_21

    if-lez v15, :cond_21

    .line 621
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    :cond_21
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 628
    :cond_22
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 608
    :cond_23
    if-eqz v4, :cond_1e

    .line 609
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    sub-int/2addr v4, v15

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_9

    :cond_24
    move v6, v7

    goto/16 :goto_5

    .line 498
    nop

    :sswitch_data_0
    .sparse-switch
        0x24 -> :sswitch_48
        0x25 -> :sswitch_3c
        0x2d -> :sswitch_27
        0x32 -> :sswitch_6
        0x33 -> :sswitch_2d
        0x34 -> :sswitch_2e
        0x35 -> :sswitch_42
        0x38 -> :sswitch_25
        0x3a -> :sswitch_34
        0x41 -> :sswitch_43
        0x44 -> :sswitch_7
        0x45 -> :sswitch_2e
        0x46 -> :sswitch_35
        0x47 -> :sswitch_43
        0x48 -> :sswitch_37
        0x4c -> :sswitch_2b
        0x4d -> :sswitch_37
        0x4e -> :sswitch_37
        0x52 -> :sswitch_15
        0x53 -> :sswitch_21
        0x59 -> :sswitch_32
        0x61 -> :sswitch_2d
        0x63 -> :sswitch_42
        0x64 -> :sswitch_35
        0x68 -> :sswitch_28
        0x69 -> :sswitch_16
        0x6b -> :sswitch_37
        0x6c -> :sswitch_6
        0x6e -> :sswitch_0
        0x6f -> :sswitch_21
        0x72 -> :sswitch_43
        0x73 -> :sswitch_43
        0x76 -> :sswitch_6
        0x78 -> :sswitch_42
        0x79 -> :sswitch_43
        0x7a -> :sswitch_43
        0x85 -> :sswitch_43
        0x8b -> :sswitch_2e
        0x8f -> :sswitch_43
        0x9a -> :sswitch_43
        0xa3 -> :sswitch_39
        0xa5 -> :sswitch_43
        0xa7 -> :sswitch_9
        0xb3 -> :sswitch_1a
        0xb4 -> :sswitch_43
        0xc9 -> :sswitch_3f
        0xcb -> :sswitch_2c
        0xcd -> :sswitch_2d
        0xcf -> :sswitch_36
        0xd2 -> :sswitch_2b
        0xe2 -> :sswitch_9
        0xe5 -> :sswitch_43
        0xe6 -> :sswitch_2b
        0xe7 -> :sswitch_d
        0xe9 -> :sswitch_3a
        0xec -> :sswitch_43
        0xee -> :sswitch_26
        0xf0 -> :sswitch_2b
        0xf2 -> :sswitch_43
        0xf3 -> :sswitch_2c
        0xf4 -> :sswitch_20
        0xf5 -> :sswitch_2d
        0xf7 -> :sswitch_43
        0x10b -> :sswitch_5
        0x10d -> :sswitch_42
        0x10e -> :sswitch_29
        0x112 -> :sswitch_3b
        0x114 -> :sswitch_17
        0x115 -> :sswitch_18
        0x124 -> :sswitch_1d
        0x125 -> :sswitch_19
        0x12c -> :sswitch_37
        0x12d -> :sswitch_2c
        0x12e -> :sswitch_38
        0x12f -> :sswitch_2c
        0x131 -> :sswitch_3e
        0x133 -> :sswitch_43
        0x134 -> :sswitch_1a
        0x145 -> :sswitch_13
        0x14d -> :sswitch_1f
        0x14f -> :sswitch_37
        0x150 -> :sswitch_4
        0x165 -> :sswitch_1c
        0x167 -> :sswitch_31
        0x168 -> :sswitch_37
        0x169 -> :sswitch_a
        0x16e -> :sswitch_21
        0x172 -> :sswitch_1
        0x177 -> :sswitch_43
        0x179 -> :sswitch_44
        0x17a -> :sswitch_e
        0x181 -> :sswitch_43
        0x182 -> :sswitch_37
        0x189 -> :sswitch_14
        0x18b -> :sswitch_1c
        0x192 -> :sswitch_24
        0x193 -> :sswitch_37
        0x194 -> :sswitch_3d
        0x195 -> :sswitch_2a
        0x196 -> :sswitch_1b
        0x1a1 -> :sswitch_43
        0x1a3 -> :sswitch_33
        0x1a4 -> :sswitch_22
        0x1a5 -> :sswitch_43
        0x1a6 -> :sswitch_2b
        0x1a7 -> :sswitch_43
        0x1a8 -> :sswitch_2d
        0x1ab -> :sswitch_43
        0x1ae -> :sswitch_40
        0x1af -> :sswitch_3
        0x1b0 -> :sswitch_2d
        0x1b1 -> :sswitch_2b
        0x1b4 -> :sswitch_37
        0x1b5 -> :sswitch_39
        0x1b6 -> :sswitch_37
        0x1b7 -> :sswitch_35
        0x1b8 -> :sswitch_b
        0x1b9 -> :sswitch_b
        0x1ba -> :sswitch_3c
        0x1c3 -> :sswitch_2b
        0x1c5 -> :sswitch_43
        0x1c6 -> :sswitch_42
        0x1c7 -> :sswitch_38
        0x1c9 -> :sswitch_10
        0x1cc -> :sswitch_2e
        0x1cf -> :sswitch_43
        0x1d0 -> :sswitch_37
        0x1d2 -> :sswitch_44
        0x1da -> :sswitch_37
        0x1ed -> :sswitch_39
        0x201 -> :sswitch_19
        0x206 -> :sswitch_1a
        0x207 -> :sswitch_46
        0x208 -> :sswitch_6
        0x20b -> :sswitch_23
        0x20c -> :sswitch_43
        0x20d -> :sswitch_2b
        0x20e -> :sswitch_2c
        0x212 -> :sswitch_f
        0x214 -> :sswitch_43
        0x217 -> :sswitch_2d
        0x219 -> :sswitch_43
        0x245 -> :sswitch_2b
        0x24f -> :sswitch_43
        0x253 -> :sswitch_43
        0x255 -> :sswitch_41
        0x261 -> :sswitch_37
        0x263 -> :sswitch_19
        0x265 -> :sswitch_11
        0x267 -> :sswitch_12
        0x268 -> :sswitch_2c
        0x269 -> :sswitch_30
        0x26a -> :sswitch_43
        0x26b -> :sswitch_43
        0x26d -> :sswitch_43
        0x26e -> :sswitch_43
        0x26f -> :sswitch_1e
        0x272 -> :sswitch_c
        0x274 -> :sswitch_35
        0x276 -> :sswitch_47
        0x27a -> :sswitch_1c
        0x283 -> :sswitch_2c
        0x288 -> :sswitch_1d
        0x28a -> :sswitch_43
        0x28d -> :sswitch_43
        0x28e -> :sswitch_43
        0x292 -> :sswitch_43
        0x296 -> :sswitch_45
        0x297 -> :sswitch_2
        0x2a1 -> :sswitch_2f
        0x2ad -> :sswitch_2d
        0x2b3 -> :sswitch_2d
        0x2b9 -> :sswitch_1a
        0x2ba -> :sswitch_6
        0x2c1 -> :sswitch_43
        0x2c5 -> :sswitch_8
        0x2c9 -> :sswitch_2d
        0x2ce -> :sswitch_19
        0x2e6 -> :sswitch_2b
        0x334 -> :sswitch_2b
        0x335 -> :sswitch_43
        0x341 -> :sswitch_1c
        0x34d -> :sswitch_43
    .end sparse-switch
.end method
