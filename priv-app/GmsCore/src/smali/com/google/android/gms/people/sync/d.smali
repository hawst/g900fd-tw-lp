.class public final Lcom/google/android/gms/people/sync/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/gms/people/f/f;

.field private final c:F


# direct methods
.method public constructor <init>(Landroid/content/Context;F)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->j()Lcom/google/android/gms/people/f/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    .line 39
    iput p2, p0, Lcom/google/android/gms/people/sync/d;->c:F

    .line 40
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v0

    .line 206
    iget-object v1, v0, Lcom/google/android/gms/people/a/c;->a:Landroid/content/SharedPreferences;

    const-string v2, "saved_cover_photo_width_pixels"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 207
    iget-object v2, p0, Lcom/google/android/gms/people/sync/d;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/a/a;->a(Landroid/content/Context;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 209
    if-ne v1, v2, :cond_1

    .line 223
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 213
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/people/sync/d;->b:Lcom/google/android/gms/people/f/f;

    invoke-virtual {v1}, Lcom/google/android/gms/people/f/f;->b()V

    .line 214
    iget-object v0, v0, Lcom/google/android/gms/people/a/c;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "saved_cover_photo_width_pixels"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    .line 217
    :try_start_2
    const-string v1, "PeopleCPSM"

    const-string v2, "Failed to wipe cached files."

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 219
    invoke-static {}, Lcom/google/android/gms/common/ew;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 220
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
