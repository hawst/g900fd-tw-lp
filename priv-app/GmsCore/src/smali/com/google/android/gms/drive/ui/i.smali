.class public Lcom/google/android/gms/drive/ui/i;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/q;->F:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/ui/i;->a(II)V

    .line 47
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/v4/app/m;->f:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/i;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Landroid/support/v4/app/m;->f:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 55
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/m;->onDestroyView()V

    .line 56
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 61
    const-string v0, "BaseIsRestart"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 62
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method
