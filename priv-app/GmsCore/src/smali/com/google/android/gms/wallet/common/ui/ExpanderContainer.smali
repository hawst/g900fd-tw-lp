.class public Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;
.super Landroid/widget/ScrollView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/bk;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Z

.field private c:Lcom/google/android/gms/wallet/common/ui/bp;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a:Ljava/util/ArrayList;

    .line 41
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->b:Z

    .line 48
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->d:Z

    .line 52
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->setScrollbarFadingEnabled(Z)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a:Ljava/util/ArrayList;

    .line 41
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->b:Z

    .line 48
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->d:Z

    .line 57
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->setScrollbarFadingEnabled(Z)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a:Ljava/util/ArrayList;

    .line 41
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->b:Z

    .line 48
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->d:Z

    .line 62
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->setScrollbarFadingEnabled(Z)V

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 148
    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->getMeasuredHeight()I

    move-result v3

    if-le v0, v3, :cond_1

    move v0, v1

    .line 149
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->b:Z

    .line 150
    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    .line 151
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Z)V

    .line 155
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 148
    goto :goto_0

    .line 152
    :cond_2
    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    .line 153
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Z)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bn;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/wallet/common/ui/bn;-><init>(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Ljava/util/List;)V

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/bo;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/wallet/common/ui/bo;-><init>(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/k;->F:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x10c000d

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/content/Context;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->b:Z

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->c:Lcom/google/android/gms/wallet/common/ui/bp;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->c:Lcom/google/android/gms/wallet/common/ui/bp;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->b:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/bp;->a(Z)V

    .line 162
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Z)Z
    .locals 0

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;)Lcom/google/android/gms/wallet/common/ui/bk;
    .locals 0

    .prologue
    .line 26
    return-object p0
.end method


# virtual methods
.method public final a(F)V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a()V

    .line 175
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/bj;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bm;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/wallet/common/ui/bm;-><init>(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Lcom/google/android/gms/wallet/common/ui/bj;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/wallet/common/ui/bj;->a(Lcom/google/android/gms/wallet/common/ui/bl;)V

    .line 125
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/bp;)V
    .locals 2

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->c:Lcom/google/android/gms/wallet/common/ui/bp;

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->c:Lcom/google/android/gms/wallet/common/ui/bp;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->b:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/bp;->a(Z)V

    .line 130
    return-void
.end method

.method public onAnimationEnd()V
    .locals 0

    .prologue
    .line 181
    return-void
.end method

.method public onAnimationStart()V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 68
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a()V

    .line 69
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onSizeChanged(IIII)V

    .line 74
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->d:Z

    if-nez v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a()V

    .line 77
    :cond_0
    return-void
.end method
