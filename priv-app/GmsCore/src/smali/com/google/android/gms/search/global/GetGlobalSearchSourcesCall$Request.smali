.class public Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/f;


# instance fields
.field public a:Z

.field final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/gms/search/global/f;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;->CREATOR:Lcom/google/android/gms/search/global/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;->b:I

    .line 58
    return-void
.end method

.method constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput p1, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;->b:I

    .line 74
    iput-boolean p2, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;->a:Z

    .line 75
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;->CREATOR:Lcom/google/android/gms/search/global/f;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;->CREATOR:Lcom/google/android/gms/search/global/f;

    invoke-static {p0, p1}, Lcom/google/android/gms/search/global/f;->a(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;Landroid/os/Parcel;)V

    .line 88
    return-void
.end method
