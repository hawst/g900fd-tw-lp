.class public final Lcom/google/android/gms/drive/g/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/at;


# instance fields
.field private final a:Lcom/google/android/gms/drive/g/i;

.field private b:I

.field private c:J

.field private d:J

.field private e:I

.field private final f:Lcom/google/android/gms/common/a/d;

.field private final g:Lcom/google/android/gms/common/a/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;)V
    .locals 8

    .prologue
    .line 43
    const/4 v5, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/drive/g/e;-><init>(Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;IJ)V

    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;IJ)V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/g/e;->a:Lcom/google/android/gms/drive/g/i;

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/drive/g/e;->f:Lcom/google/android/gms/common/a/d;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/drive/g/e;->g:Lcom/google/android/gms/common/a/d;

    .line 51
    invoke-virtual {p2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/g/e;->b:I

    .line 52
    invoke-virtual {p3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/g/e;->c:J

    .line 53
    iput p4, p0, Lcom/google/android/gms/drive/g/e;->e:I

    .line 54
    iput-wide p5, p0, Lcom/google/android/gms/drive/g/e;->d:J

    .line 55
    return-void
.end method

.method private declared-synchronized a(J)V
    .locals 7

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/drive/g/e;->d:J

    sub-long v0, p1, v0

    .line 70
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/e;->c:J

    neg-long v2, v2

    iget v4, p0, Lcom/google/android/gms/drive/g/e;->b:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 71
    iget-wide v0, p0, Lcom/google/android/gms/drive/g/e;->c:J

    iget v2, p0, Lcom/google/android/gms/drive/g/e;->b:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/gms/drive/g/e;->d:J

    .line 72
    iget-wide v0, p0, Lcom/google/android/gms/drive/g/e;->c:J

    neg-long v0, v0

    .line 75
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/e;->c:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 76
    iget v2, p0, Lcom/google/android/gms/drive/g/e;->e:I

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/google/android/gms/drive/g/e;->c:J

    div-long v4, v0, v4

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, p0, Lcom/google/android/gms/drive/g/e;->e:I

    .line 77
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/e;->c:J

    rem-long/2addr v0, v2

    sub-long v0, p1, v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/g/e;->d:J

    .line 80
    iget v0, p0, Lcom/google/android/gms/drive/g/e;->e:I

    iget v1, p0, Lcom/google/android/gms/drive/g/e;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/g/e;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :cond_1
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()J
    .locals 6

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/g/e;->a:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    .line 121
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/g/e;->a(J)V

    .line 122
    iget v2, p0, Lcom/google/android/gms/drive/g/e;->e:I

    if-lez v2, :cond_0

    .line 123
    iget v0, p0, Lcom/google/android/gms/drive/g/e;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/g/e;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    const-wide/16 v0, 0x0

    .line 127
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 126
    :cond_0
    :try_start_1
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/e;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/g/e;->c:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/gms/drive/g/e;->d:J

    .line 127
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/e;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long v0, v2, v0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final declared-synchronized a()J
    .locals 2

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/drive/g/e;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b()I
    .locals 1

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/drive/g/e;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 2

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/g/e;->a:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    .line 101
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/g/e;->a(J)V

    .line 102
    iget v0, p0, Lcom/google/android/gms/drive/g/e;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 2

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/g/e;->c()Z

    move-result v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    iget v1, p0, Lcom/google/android/gms/drive/g/e;->e:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/gms/drive/g/e;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_0
    monitor-exit p0

    return v0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 6

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/e;->g()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 135
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 136
    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    :cond_0
    monitor-exit p0

    return-void

    .line 138
    :catch_0
    move-exception v0

    .line 140
    :try_start_2
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/e;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/g/e;->c:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/gms/drive/g/e;->d:J

    .line 141
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 148
    return-void
.end method
