.class public Lcom/google/android/gms/analytics/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:Lcom/google/android/gms/analytics/bm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/analytics/v;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/v;-><init>()V

    const-class v1, Lcom/google/android/gms/analytics/bl;

    monitor-enter v1

    :try_start_0
    sput-object v0, Lcom/google/android/gms/analytics/bl;->a:Lcom/google/android/gms/analytics/bm;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/google/android/gms/analytics/bl;->b()Lcom/google/android/gms/analytics/bm;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    invoke-interface {v0, p0}, Lcom/google/android/gms/analytics/bm;->d(Ljava/lang/String;)V

    .line 36
    :cond_0
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-static {}, Lcom/google/android/gms/analytics/bl;->b()Lcom/google/android/gms/analytics/bm;

    move-result-object v1

    .line 88
    if-eqz v1, :cond_0

    .line 89
    invoke-static {}, Lcom/google/android/gms/analytics/bl;->b()Lcom/google/android/gms/analytics/bm;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/analytics/bm;->a()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 91
    :cond_0
    return v0
.end method

.method static b()Lcom/google/android/gms/analytics/bm;
    .locals 2

    .prologue
    .line 103
    const-class v1, Lcom/google/android/gms/analytics/bl;

    monitor-enter v1

    .line 104
    :try_start_0
    sget-object v0, Lcom/google/android/gms/analytics/bl;->a:Lcom/google/android/gms/analytics/bm;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/google/android/gms/analytics/bl;->b()Lcom/google/android/gms/analytics/bm;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_0

    .line 58
    invoke-interface {v0, p0}, Lcom/google/android/gms/analytics/bm;->b(Ljava/lang/String;)V

    .line 60
    :cond_0
    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/google/android/gms/analytics/bl;->b()Lcom/google/android/gms/analytics/bm;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_0

    .line 70
    invoke-interface {v0, p0}, Lcom/google/android/gms/analytics/bm;->a(Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Lcom/google/android/gms/analytics/bl;->b()Lcom/google/android/gms/analytics/bm;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    .line 82
    invoke-interface {v0, p0}, Lcom/google/android/gms/analytics/bm;->c(Ljava/lang/String;)V

    .line 84
    :cond_0
    return-void
.end method
