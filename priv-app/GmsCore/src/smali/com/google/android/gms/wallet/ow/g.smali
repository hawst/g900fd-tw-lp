.class public final Lcom/google/android/gms/wallet/ow/g;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/bp;
.implements Lcom/google/android/gms/wallet/common/ui/db;
.implements Lcom/google/android/gms/wallet/ow/c;
.implements Lcom/google/android/gms/wallet/ow/u;


# static fields
.field private static final H:[I

.field static final a:Ljava/util/ArrayList;


# instance fields
.field A:Z

.field B:Z

.field C:Lcom/google/aa/a/a/a/f;

.field D:I

.field E:Lcom/google/android/gms/wallet/common/ui/cd;

.field F:Lcom/google/android/gms/wallet/common/ui/w;

.field protected final G:Lcom/google/android/gms/wallet/service/l;

.field private I:Landroid/view/View;

.field private J:Landroid/view/View;

.field private K:Landroid/view/View;

.field private L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

.field private N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

.field private O:Landroid/accounts/Account;

.field private P:[Lcom/google/aa/b/a/a/a/a/s;

.field private Q:Ljava/util/ArrayList;

.field private R:Lcom/google/aa/b/a/a/a/a/i;

.field private S:I

.field private T:Z

.field private U:Z

.field private V:Ljava/util/ArrayList;

.field private W:Z

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aa:[I

.field private ab:[I

.field private ac:Z

.field private ad:Lcom/google/android/apps/common/a/a/i;

.field private ae:Lcom/google/android/apps/common/a/a/h;

.field private af:Lcom/google/android/apps/common/a/a/i;

.field private ag:Lcom/google/android/apps/common/a/a/h;

.field private ah:Lcom/google/android/gms/wallet/ow/p;

.field private ai:Z

.field private aj:Z

.field private ak:Lcom/google/aa/b/a/a/a/a/u;

.field private al:I

.field private am:I

.field private an:Lcom/google/android/gms/wallet/payform/f;

.field private ao:I

.field private ap:Landroid/content/Intent;

.field private aq:Ljava/lang/String;

.field private ar:Ljava/lang/String;

.field private final as:Lcom/google/android/gms/common/util/w;

.field b:Landroid/view/View;

.field c:Landroid/widget/ProgressBar;

.field d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field e:Lcom/google/android/gms/wallet/common/ui/cc;

.field f:Landroid/widget/CheckBox;

.field g:Landroid/view/View;

.field h:Landroid/view/View;

.field i:Lcom/google/android/gms/wallet/common/ui/v;

.field j:Landroid/view/View;

.field k:Landroid/widget/CheckBox;

.field l:Landroid/view/View;

.field m:Landroid/view/View;

.field n:Landroid/view/View;

.field o:Lcom/google/android/gms/wallet/ow/t;

.field p:Landroid/widget/TextView;

.field q:Landroid/widget/TextView;

.field r:Lcom/google/android/gms/wallet/common/ui/bb;

.field s:Lcom/google/android/gms/wallet/common/ui/bb;

.field t:Lcom/google/android/gms/wallet/common/ui/ca;

.field u:Lcom/google/android/gms/wallet/common/ui/bb;

.field v:Lcom/google/android/gms/wallet/common/ui/cz;

.field w:Landroid/view/ViewGroup;

.field x:Landroid/view/View;

.field y:Lcom/google/android/gms/wallet/service/k;

.field z:Lcom/google/android/gms/wallet/common/PaymentModel;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 196
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "US"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ow/g;->a:Ljava/util/ArrayList;

    .line 204
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/wallet/ow/g;->H:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 111
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 286
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    .line 287
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    .line 289
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    .line 290
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->X:Z

    .line 291
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->Y:Z

    .line 292
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    .line 295
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    .line 302
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->B:Z

    .line 306
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    .line 307
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ai:Z

    .line 308
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    .line 310
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/g;->D:I

    .line 315
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/wallet/ow/g;->ao:I

    .line 316
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ap:Landroid/content/Intent;

    .line 1362
    new-instance v0, Lcom/google/android/gms/wallet/ow/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ow/j;-><init>(Lcom/google/android/gms/wallet/ow/g;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->E:Lcom/google/android/gms/wallet/common/ui/cd;

    .line 1463
    new-instance v0, Lcom/google/android/gms/wallet/ow/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ow/k;-><init>(Lcom/google/android/gms/wallet/ow/g;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->F:Lcom/google/android/gms/wallet/common/ui/w;

    .line 1764
    new-instance v0, Lcom/google/android/gms/wallet/ow/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ow/l;-><init>(Lcom/google/android/gms/wallet/ow/g;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->as:Lcom/google/android/gms/common/util/w;

    .line 2539
    new-instance v0, Lcom/google/android/gms/wallet/ow/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ow/n;-><init>(Lcom/google/android/gms/wallet/ow/g;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->G:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method static synthetic A(Lcom/google/android/gms/wallet/ow/g;)Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    return v0
.end method

.method static synthetic B(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "ChooseMethodsFragment.MaskedWalletNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic C(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->s:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->s:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->s:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->s:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->s:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "ChooseMethodsFragment.WalletItemsNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic D(Lcom/google/android/gms/wallet/ow/g;)Z
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/checkout/inapp/proto/a/b;)I
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->e(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/checkout/inapp/proto/j;)I
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    return v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/accounts/Account;II)Lcom/google/android/gms/wallet/ow/g;
    .locals 3

    .prologue
    .line 382
    new-instance v0, Lcom/google/android/gms/wallet/ow/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/ow/g;-><init>()V

    .line 383
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 384
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 385
    const-string v2, "immediateFullWalletRequest"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 386
    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 387
    const-string v2, "startingContentHeight"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 388
    const-string v2, "startingContentWidth"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 389
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->setArguments(Landroid/os/Bundle;)V

    .line 390
    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;II)Lcom/google/android/gms/wallet/ow/g;
    .locals 3

    .prologue
    .line 349
    new-instance v0, Lcom/google/android/gms/wallet/ow/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/ow/g;-><init>()V

    .line 350
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 351
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 352
    const-string v2, "maskedWalletRequest"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 353
    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 354
    const-string v2, "googleTransactionId"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v2, "selectedInstrumentId"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v2, "useWalletBalanceChecked"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 357
    const-string v2, "selectedAddressId"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v2, "startingContentHeight"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 359
    const-string v2, "startingContentWidth"

    invoke-virtual {v1, v2, p8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 360
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->setArguments(Landroid/os/Bundle;)V

    .line 361
    return-object v0
.end method

.method private a([Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 5

    .prologue
    .line 1989
    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 1990
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->e(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v3

    const/16 v4, 0x7f

    if-ne v3, v4, :cond_0

    .line 1994
    :goto_1
    return-object v0

    .line 1989
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1994
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a([Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;
    .locals 5

    .prologue
    .line 1784
    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 1785
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v3

    const/16 v4, 0x7f

    if-ne v3, v4, :cond_0

    .line 1789
    :goto_1
    return-object v0

    .line 1784
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1789
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;
    .locals 1

    .prologue
    .line 1775
    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    .line 1776
    if-nez v0, :cond_0

    .line 1777
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->as:Lcom/google/android/gms/common/util/w;

    invoke-static {p1, p2, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/common/util/w;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    .line 1780
    :cond_0
    return-object v0
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1307
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 1308
    if-nez v0, :cond_1

    .line 1309
    iput p1, p0, Lcom/google/android/gms/wallet/ow/g;->ao:I

    .line 1310
    iput-object p2, p0, Lcom/google/android/gms/wallet/ow/g;->ap:Landroid/content/Intent;

    .line 1323
    :cond_0
    :goto_0
    return-void

    .line 1313
    :cond_1
    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1314
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1316
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ae:Lcom/google/android/apps/common/a/a/h;

    if-eqz v0, :cond_0

    .line 1317
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ae:Lcom/google/android/apps/common/a/a/h;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "click_to_activity_result"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 1319
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 1320
    iput-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    .line 1321
    iput-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->ae:Lcom/google/android/apps/common/a/a/h;

    goto :goto_0
.end method

.method private a(Lcom/google/aa/a/a/a/f;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1534
    iget-object v2, p1, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    .line 1535
    iget-object v1, v2, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    const/16 v3, 0x8

    invoke-static {v1, v3}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1536
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    .line 1538
    :cond_0
    iget-boolean v1, v2, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    if-eqz v1, :cond_1

    .line 1539
    sget-object v1, Lcom/google/android/gms/wallet/ow/g;->H:[I

    array-length v1, v1

    .line 1540
    sget-object v3, Lcom/google/android/gms/wallet/ow/g;->H:[I

    add-int/lit8 v4, v1, 0x1

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    .line 1542
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    const/4 v4, 0x3

    aput v4, v3, v1

    .line 1546
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    .line 1547
    iget-object v1, p1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 1548
    iget-object v3, p1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 1549
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1548
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1544
    :cond_1
    sget-object v1, Lcom/google/android/gms/wallet/ow/g;->H:[I

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    goto :goto_0

    .line 1552
    :cond_2
    iget-object v1, p1, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 1553
    iget-object v1, p1, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v3, v1

    :goto_2
    if-ge v0, v3, :cond_4

    aget-object v4, v1, v0

    .line 1554
    iget-object v5, v4, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v5, :cond_3

    .line 1555
    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    iget-object v4, v4, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1553
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1561
    :cond_4
    iget-object v0, v2, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1562
    iget-object v0, v2, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->P:[Lcom/google/aa/b/a/a/a/a/s;

    .line 1567
    :goto_3
    return-void

    .line 1565
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->P:[Lcom/google/aa/b/a/a/a/a/s;

    goto :goto_3
.end method

.method private a(Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V
    .locals 11

    .prologue
    const/16 v6, 0x75

    const/4 v4, 0x0

    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1835
    iget-object v7, p1, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    .line 1838
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v0, v1, :cond_9

    .line 1839
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_f

    .line 1841
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v7, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    .line 1843
    if-eqz v0, :cond_2

    move-object v3, v0

    move v0, v1

    .line 1849
    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v5

    .line 1850
    if-eqz p2, :cond_4

    .line 1851
    iget-object v8, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v8, v8, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v5, v6, :cond_3

    const-string v0, "notFound"

    const-string v6, ""

    invoke-virtual {p2, v0, v6}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    move v0, v5

    .line 1855
    :goto_1
    if-nez v3, :cond_7

    .line 1856
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    packed-switch v1, :pswitch_data_0

    :goto_2
    move v1, v0

    move-object v0, v3

    .line 1897
    :goto_3
    const/16 v3, 0x7f

    if-eq v1, v3, :cond_0

    move-object v0, v4

    .line 1900
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 1901
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    if-eqz v1, :cond_d

    .line 1921
    :cond_1
    :goto_4
    return-void

    .line 1846
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->as:Lcom/google/android/gms/common/util/w;

    invoke-static {v7, v0, v3}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/common/util/w;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    move-object v3, v0

    move v0, v2

    goto :goto_0

    .line 1851
    :cond_3
    if-eqz v0, :cond_5

    const-string v0, "found"

    :goto_5
    packed-switch v5, :pswitch_data_1

    :pswitch_0
    const-string v0, "ChooseMethodsFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "unknown instrument score "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v0, v5

    goto :goto_1

    :cond_5
    const-string v0, "matchFound"

    goto :goto_5

    :pswitch_1
    const-string v6, "valid"

    invoke-virtual {p2, v0, v6}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    move v0, v5

    goto :goto_1

    :pswitch_2
    const-string v6, "expired"

    invoke-virtual {p2, v0, v6}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    if-eq v8, v9, :cond_4

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_3
    const-string v6, "minAddress"

    invoke-virtual {p2, v0, v6}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    if-eq v8, v9, :cond_4

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_4
    const-string v6, "missingPhone"

    invoke-virtual {p2, v0, v6}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    if-eq v8, v9, :cond_4

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_5
    const-string v6, "amexDisallowed"

    invoke-virtual {p2, v0, v6}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_6
    const-string v6, "otherDisallowed"

    invoke-virtual {p2, v0, v6}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    move v0, v5

    goto :goto_1

    :pswitch_7
    const-string v6, "declined"

    invoke-virtual {p2, v0, v6}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    move v0, v5

    goto/16 :goto_1

    :pswitch_8
    const-string v6, "otherInvalid"

    invoke-virtual {p2, v0, v6}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    move v0, v5

    goto/16 :goto_1

    .line 1859
    :pswitch_9
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v1, :cond_6

    new-instance v1, Lcom/google/android/gms/wallet/ow/p;

    const/16 v5, 0xc9

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/ui/cz;->d(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v6

    invoke-direct {v1, p0, v5, v6, v2}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    .line 1860
    :cond_6
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->ai:Z

    goto/16 :goto_2

    .line 1863
    :cond_7
    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v5, v5, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v5, v9, :cond_8

    .line 1864
    packed-switch v0, :pswitch_data_2

    move-object v1, v3

    :goto_6
    move v10, v0

    move-object v0, v1

    move v1, v10

    .line 1873
    goto/16 :goto_3

    .line 1868
    :pswitch_a
    invoke-direct {p0, v3, v4}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V

    .line 1869
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    .line 1870
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-direct {p0, v7, v0}, Lcom/google/android/gms/wallet/ow/g;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v1

    .line 1872
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    goto :goto_6

    .line 1875
    :cond_8
    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v5, v5, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v5, v1, :cond_e

    .line 1876
    invoke-direct {p0, v3, v0, v7}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/checkout/inapp/proto/j;I[Lcom/google/checkout/inapp/proto/j;)V

    move v1, v0

    move-object v0, v3

    goto/16 :goto_3

    .line 1880
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-direct {p0, v7, v0}, Lcom/google/android/gms/wallet/ow/g;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v3

    .line 1882
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    .line 1883
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-nez v1, :cond_e

    .line 1884
    array-length v1, v7

    if-nez v1, :cond_b

    .line 1885
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v1, :cond_a

    new-instance v1, Lcom/google/android/gms/wallet/ow/p;

    const/16 v5, 0xc9

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/ui/cz;->h(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v6

    invoke-direct {v1, p0, v5, v6, v2}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    :cond_a
    move v1, v0

    move-object v0, v3

    goto/16 :goto_3

    .line 1886
    :cond_b
    if-nez v3, :cond_e

    .line 1887
    invoke-static {v7}, Lcom/google/android/gms/wallet/ow/g;->b([Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    .line 1888
    if-nez v0, :cond_c

    .line 1889
    invoke-direct {p0, v7}, Lcom/google/android/gms/wallet/ow/g;->a([Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    .line 1891
    :cond_c
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v1

    .line 1892
    invoke-direct {p0, v0, v1, v7}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/checkout/inapp/proto/j;I[Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_3

    .line 1905
    :cond_d
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v3

    invoke-interface {v1, v3}, Lcom/google/android/gms/wallet/common/ui/cc;->c(Z)V

    .line 1906
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    invoke-interface {v1, v3}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Z)V

    .line 1908
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v2}, Lcom/google/android/gms/wallet/common/ui/cc;->b_(Z)V

    .line 1909
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    invoke-interface {v1, v2}, Lcom/google/android/gms/wallet/common/ui/cc;->a([I)V

    .line 1910
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->ab:[I

    invoke-interface {v1, v2}, Lcom/google/android/gms/wallet/common/ui/cc;->b([I)V

    .line 1911
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v7}, Lcom/google/android/gms/wallet/common/ui/cc;->a([Lcom/google/checkout/inapp/proto/j;)V

    .line 1912
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 1917
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 1918
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-nez v1, :cond_1

    .line 1919
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    goto/16 :goto_4

    :cond_e
    move v1, v0

    move-object v0, v3

    goto/16 :goto_3

    :cond_f
    move v0, v6

    move-object v3, v4

    goto/16 :goto_1

    .line 1856
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_9
    .end packed-switch

    .line 1851
    :pswitch_data_1
    .packed-switch 0x76
        :pswitch_7
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch

    .line 1864
    :pswitch_data_2
    .packed-switch 0x7c
        :pswitch_a
        :pswitch_a
        :pswitch_a
    .end packed-switch
.end method

.method private a(Lcom/google/aa/b/a/a/a/a/p;)V
    .locals 2

    .prologue
    .line 1621
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1622
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->w:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1629
    :goto_0
    return-void

    .line 1623
    :cond_0
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1624
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->p:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1625
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->w:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 1627
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->w:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->t:Lcom/google/android/gms/wallet/common/ui/ca;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->t:Lcom/google/android/gms/wallet/common/ui/ca;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/ca;->b()Lcom/google/android/gms/wallet/common/ui/ca;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->t:Lcom/google/android/gms/wallet/common/ui/ca;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->t:Lcom/google/android/gms/wallet/common/ui/ca;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "ChooseMethodsFragment.DefaultWalletInfoDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/ca;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;I)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/g;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/aa/a/a/a/f;)V

    iget-object v0, p1, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, p1, v3}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V

    invoke-direct {p0, p1, v3}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V

    invoke-direct {p0, p1, v3}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V

    iget-object v0, p1, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/aa/b/a/a/a/a/p;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/aa/b/a/a/a/a/p;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/aa/b/a/a/a/a/p;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->q()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->af:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ag:Lcom/google/android/apps/common/a/a/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->af:Lcom/google/android/apps/common/a/a/i;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->ag:Lcom/google/android/apps/common/a/a/h;

    new-array v1, v1, [Ljava/lang/String;

    const-string v4, "create_to_ui_loaded_from_cache"

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;Lcom/google/android/gms/wallet/cache/a;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 111
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    array-length v0, v0

    if-lez v0, :cond_4

    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v0, v3, :cond_3

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/android/gms/wallet/cache/a;->b:[Ljava/lang/String;

    const/4 v1, 0x5

    const-string v2, "updatedLegalDocs"

    aput-object v2, v0, v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v0, v3, :cond_3

    if-eqz p2, :cond_1

    iput-boolean v4, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/wallet/ow/p;

    const/16 v1, 0xcb

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/cz;->g(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, v4}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    :cond_2
    iput-boolean v4, p0, Lcom/google/android/gms/wallet/ow/g;->ai:Z

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->q:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->CN:I

    const-string v2, "wallet_tos_activity"

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/wallet/ow/m;

    invoke-direct {v3, p0}, Lcom/google/android/gms/wallet/ow/m;-><init>(Lcom/google/android/gms/wallet/ow/g;)V

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/widget/TextView;ILjava/util/Set;Lcom/google/android/gms/wallet/common/ui/al;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->q:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/android/gms/wallet/common/ui/cz;)V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/android/gms/wallet/common/ui/db;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "ChooseMethodsFragment.RequiredActionDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->u:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->u:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->u:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->u:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->u:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "ChooseMethodsFragment.OwErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;Z)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->b(Z)V

    return-void
.end method

.method private a(Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/a/b;Z)V
    .locals 7

    .prologue
    const/16 v6, 0xca

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2434
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-nez v0, :cond_1

    .line 2464
    :cond_0
    :goto_0
    return-void

    .line 2437
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-eqz v0, :cond_3

    .line 2439
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    iget v0, v0, Lcom/google/android/gms/wallet/ow/p;->b:I

    const/16 v1, 0xc9

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-eq v0, v2, :cond_0

    .line 2442
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    .line 2443
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    goto :goto_0

    .line 2447
    :cond_3
    array-length v0, p2

    if-nez v0, :cond_4

    .line 2448
    new-instance v0, Lcom/google/android/gms/wallet/ow/p;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->V:Ljava/util/ArrayList;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p1, v1, v2, v3, v4}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/checkout/inapp/proto/a/b;Ljava/util/List;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-direct {v0, p0, v6, v1, v5}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto :goto_0

    .line 2453
    :cond_4
    if-eqz p3, :cond_5

    .line 2454
    new-instance v0, Lcom/google/android/gms/wallet/ow/p;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-direct {v0, p0, v6, v1, v5}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto :goto_0

    .line 2458
    :cond_5
    new-instance v0, Lcom/google/android/gms/wallet/ow/p;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/cz;->b(Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-direct {v0, p0, v6, v1, v5}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto :goto_0
.end method

.method private a(Lcom/google/checkout/inapp/proto/j;I[Lcom/google/checkout/inapp/proto/j;)V
    .locals 9

    .prologue
    const/16 v8, 0xc9

    const/4 v7, 0x0

    .line 1805
    const/16 v0, 0x7f

    if-ne p2, v0, :cond_1

    .line 1831
    :cond_0
    :goto_0
    return-void

    .line 1808
    :cond_1
    iput-boolean v7, p0, Lcom/google/android/gms/wallet/ow/g;->ai:Z

    .line 1809
    packed-switch p2, :pswitch_data_0

    .line 1828
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v0, :cond_0

    new-instance v6, Lcom/google/android/gms/wallet/ow/p;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/g;->ab:[I

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/cz;->b([Lcom/google/checkout/inapp/proto/j;ZZ[I[ILcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    invoke-direct {v6, p0, v8, v0, v7}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v6, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto :goto_0

    .line 1812
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v0, :cond_0

    new-instance v6, Lcom/google/android/gms/wallet/ow/p;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/g;->ab:[I

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/cz;->a([Lcom/google/checkout/inapp/proto/j;ZZ[I[ILcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    invoke-direct {v6, p0, v8, v0, v7}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v6, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto :goto_0

    .line 1815
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/ow/p;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p1, p3, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/checkout/inapp/proto/j;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-direct {v0, p0, v8, v1, v7}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto :goto_0

    .line 1819
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/ow/p;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/cz;->c(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-direct {v0, p0, v8, v1, v7}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto/16 :goto_0

    .line 1822
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/ow/p;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-direct {v0, p0, v8, v1, v7}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto/16 :goto_0

    .line 1825
    :pswitch_4
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/ow/p;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/cz;->b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-direct {v0, p0, v8, v1, v7}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto/16 :goto_0

    .line 1809
    :pswitch_data_0
    .packed-switch 0x79
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1176
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1177
    if-eqz p2, :cond_0

    .line 1178
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/ow/g;->b(Z)V

    .line 1180
    :cond_0
    if-eqz p1, :cond_2

    sget-object v0, Lcom/google/android/gms/wallet/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1191
    :cond_1
    :goto_0
    return-void

    .line 1183
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1184
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ak:Lcom/google/aa/b/a/a/a/a/u;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    .line 1186
    :cond_3
    new-instance v0, Lcom/google/aa/b/a/a/a/a/o;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/o;-><init>()V

    .line 1187
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ak:Lcom/google/aa/b/a/a/a/a/u;

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/o;->a:Lcom/google/aa/b/a/a/a/a/u;

    .line 1188
    iput v2, v0, Lcom/google/aa/b/a/a/a/a/o;->f:I

    .line 1189
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->y:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v1, v0, p1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/aa/b/a/a/a/a/o;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;)Z
    .locals 14

    .prologue
    const/4 v13, 0x1

    .line 111
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    invoke-static {v0, v13}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    const-string v1, "US"

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->P:[Lcom/google/aa/b/a/a/a/a/s;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;-><init>(Ljava/lang/String;[Lcom/google/aa/b/a/a/a/a/s;)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/analytics/events/SignupActivityStartedEvent;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ar:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ak:Lcom/google/aa/b/a/a/a/a/u;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ak:Lcom/google/aa/b/a/a/a/a/u;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    const-string v4, "US"

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    iget-boolean v7, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    iget-object v8, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    iget-object v9, p0, Lcom/google/android/gms/wallet/ow/g;->ab:[I

    iget-object v10, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/aa/b/a/a/a/a/p;)Z

    move-result v11

    iget-object v12, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static/range {v0 .. v12}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/Cart;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z[I[ILjava/util/Collection;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/g;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v13

    :goto_1
    return v0

    :cond_0
    sget-object v5, Lcom/google/android/gms/wallet/ow/g;->a:Ljava/util/ArrayList;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/g;[I)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 111
    if-eqz p1, :cond_0

    array-length v1, p1

    if-lez v1, :cond_0

    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p1, v1

    const-string v4, "ChooseMethodsFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Required action: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch v3, :sswitch_data_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :sswitch_0
    const/16 v0, 0x199

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    const/4 v0, 0x1

    :cond_0
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x9 -> :sswitch_0
    .end sparse-switch
.end method

.method private b(Lcom/google/checkout/inapp/proto/j;)I
    .locals 5

    .prologue
    const/16 v0, 0x77

    .line 1652
    if-nez p1, :cond_1

    .line 1653
    const/16 v0, 0x75

    .line 1700
    :cond_0
    :goto_0
    return v0

    .line 1655
    :cond_1
    iget v1, p1, Lcom/google/checkout/inapp/proto/j;->l:I

    .line 1656
    iget v2, p1, Lcom/google/checkout/inapp/proto/j;->c:I

    .line 1657
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->ab:[I

    invoke-static {v3, v1}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1658
    packed-switch v1, :pswitch_data_0

    .line 1664
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected instrument category: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1660
    :pswitch_0
    const/16 v0, 0x7b

    goto :goto_0

    .line 1662
    :pswitch_1
    const/16 v0, 0x7a

    goto :goto_0

    .line 1667
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    invoke-static {v1, v2}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1668
    const/4 v0, 0x3

    if-ne v2, v0, :cond_3

    .line 1669
    const/16 v0, 0x79

    goto :goto_0

    .line 1671
    :cond_3
    const/16 v0, 0x78

    goto :goto_0

    .line 1673
    :cond_4
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v1, v1

    if-lez v1, :cond_6

    .line 1675
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v4, v1

    .line 1676
    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_5

    .line 1677
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    aget v1, v1, v2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 1678
    const/16 v3, 0x7d

    .line 1676
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    :cond_5
    move v0, v1

    .line 1683
    goto :goto_0

    .line 1685
    :cond_6
    iget v1, p1, Lcom/google/checkout/inapp/proto/j;->h:I

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 1687
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1689
    const/16 v0, 0x7c

    goto :goto_0

    .line 1690
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1692
    const/16 v0, 0x7e

    goto :goto_0

    .line 1694
    :cond_8
    const/16 v0, 0x7f

    goto :goto_0

    .line 1697
    :pswitch_3
    const/16 v0, 0x76

    goto/16 :goto_0

    .line 1658
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1685
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static b([Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 4

    .prologue
    .line 1998
    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p0, v1

    .line 1999
    iget-boolean v3, v0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    if-eqz v3, :cond_0

    .line 2003
    :goto_1
    return-object v0

    .line 1998
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2003
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static b([Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;
    .locals 4

    .prologue
    .line 1793
    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p0, v1

    .line 1794
    iget-boolean v3, v0, Lcom/google/checkout/inapp/proto/j;->f:Z

    if-eqz v3, :cond_0

    .line 1798
    :goto_1
    return-object v0

    .line 1793
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1798
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 1303
    const/4 v0, 0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lcom/google/android/gms/wallet/o;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/o;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/o;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    :cond_0
    const-string v3, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    iget-object v2, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(ILandroid/content/Intent;)V

    .line 1304
    return-void
.end method

.method private b(Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/16 v9, 0x7f

    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2008
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    if-nez v0, :cond_1

    .line 2009
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2010
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2011
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v4}, Lcom/google/android/gms/wallet/common/ui/v;->setVisibility(I)V

    .line 2085
    :cond_0
    :goto_0
    return-void

    .line 2014
    :cond_1
    iget-object v6, p1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    .line 2016
    const/16 v0, 0x7b

    .line 2017
    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v4, v4, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v4, v1, :cond_b

    .line 2018
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v6, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v4

    .line 2019
    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/ow/g;->e(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    .line 2020
    if-eqz p2, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v5, v5, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v5, :cond_2

    .line 2021
    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v5, v5, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    const/16 v7, 0x7b

    if-ne v0, v7, :cond_5

    const-string v5, "notFound"

    const-string v7, ""

    invoke-virtual {p2, v5, v7}, Lcom/google/android/gms/wallet/cache/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    .line 2023
    :cond_2
    :goto_1
    const/16 v5, 0x7d

    if-eq v0, v5, :cond_3

    const/16 v5, 0x7e

    if-ne v0, v5, :cond_6

    :cond_3
    move v5, v1

    .line 2025
    :goto_2
    if-nez v4, :cond_8

    .line 2026
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    packed-switch v1, :pswitch_data_0

    :goto_3
    :pswitch_0
    move v1, v0

    move-object v0, v4

    .line 2066
    :goto_4
    if-eq v1, v9, :cond_4

    move-object v0, v3

    .line 2069
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 2070
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    if-nez v1, :cond_0

    .line 2074
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v6}, Lcom/google/android/gms/wallet/common/ui/v;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    .line 2075
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 2076
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v2}, Lcom/google/android/gms/wallet/common/ui/v;->a(Z)V

    .line 2081
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 2082
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-nez v1, :cond_0

    .line 2083
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lcom/google/checkout/inapp/proto/a/b;

    goto :goto_0

    .line 2021
    :cond_5
    const-string v7, "found"

    packed-switch v0, :pswitch_data_1

    const-string v5, "ChooseMethodsFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unknown address score "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_1
    const-string v5, "valid"

    invoke-virtual {p2, v7, v5}, Lcom/google/android/gms/wallet/cache/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    goto :goto_1

    :pswitch_2
    const-string v8, "minShippingAddress"

    invoke-virtual {p2, v7, v8}, Lcom/google/android/gms/wallet/cache/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    const/4 v7, 0x3

    if-eq v5, v7, :cond_2

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    goto :goto_1

    :pswitch_3
    const-string v5, "missingPhone"

    invoke-virtual {p2, v7, v5}, Lcom/google/android/gms/wallet/cache/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    goto :goto_1

    :pswitch_4
    const-string v5, "otherInvalid"

    invoke-virtual {p2, v7, v5}, Lcom/google/android/gms/wallet/cache/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    iput-boolean v2, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    goto :goto_1

    :cond_6
    move v5, v2

    .line 2023
    goto :goto_2

    .line 2029
    :pswitch_5
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v1, :cond_7

    new-instance v1, Lcom/google/android/gms/wallet/ow/p;

    const/16 v5, 0xca

    iget-object v7, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v7}, Lcom/google/android/gms/wallet/common/ui/cz;->f(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v7

    invoke-direct {v1, p0, v5, v7, v2}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    .line 2030
    :cond_7
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->ai:Z

    goto/16 :goto_3

    .line 2033
    :cond_8
    iget-object v7, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v7, v7, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_9

    if-eqz v5, :cond_9

    .line 2034
    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 2035
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    .line 2036
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v6, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v1

    .line 2037
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/g;->e(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    move v10, v0

    move-object v0, v1

    move v1, v10

    goto/16 :goto_4

    .line 2038
    :cond_9
    iget-object v7, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v7, v7, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v7, v1, :cond_a

    if-eq v0, v9, :cond_a

    .line 2040
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->ai:Z

    .line 2041
    invoke-direct {p0, v4, v6, v5}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/a/b;Z)V

    :cond_a
    move v1, v0

    move-object v0, v4

    .line 2043
    goto/16 :goto_4

    .line 2044
    :cond_b
    array-length v4, v6

    if-nez v4, :cond_c

    .line 2045
    invoke-direct {p0, v3, v6, v2}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/a/b;Z)V

    move v1, v0

    move-object v0, v3

    goto/16 :goto_4

    .line 2048
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v6, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v4

    .line 2049
    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/ow/g;->e(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    .line 2050
    if-nez v4, :cond_11

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v5, v5, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-nez v5, :cond_11

    .line 2052
    invoke-static {v6}, Lcom/google/android/gms/wallet/ow/g;->b([Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    .line 2053
    if-nez v0, :cond_d

    .line 2054
    invoke-direct {p0, v6}, Lcom/google/android/gms/wallet/ow/g;->a([Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    .line 2056
    :cond_d
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->e(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v4

    .line 2057
    if-eq v4, v9, :cond_f

    .line 2058
    const/16 v5, 0x7d

    if-eq v4, v5, :cond_e

    const/16 v5, 0x7e

    if-ne v4, v5, :cond_10

    .line 2060
    :cond_e
    :goto_5
    invoke-direct {p0, v0, v6, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/a/b;Z)V

    :cond_f
    move v1, v4

    goto/16 :goto_4

    :cond_10
    move v1, v2

    .line 2058
    goto :goto_5

    :cond_11
    move v1, v0

    move-object v0, v4

    goto/16 :goto_4

    .line 2026
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 2021
    :pswitch_data_1
    .packed-switch 0x7c
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private b(Lcom/google/aa/b/a/a/a/a/p;)V
    .locals 2

    .prologue
    .line 2088
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/aa/b/a/a/a/a/p;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2089
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2093
    :goto_0
    return-void

    .line 2091
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->m()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/aa/a/a/a/f;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/aa/b/a/a/a/a/p;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/checkout/inapp/proto/a/b;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/checkout/inapp/proto/j;)V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1262
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/wallet/ow/g;->S:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/g;->S:I

    .line 1263
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->l()V

    .line 1264
    return-void

    .line 1262
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private c(I)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 1444
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    const-string v2, "US"

    sget-object v3, Lcom/google/android/gms/wallet/ow/g;->a:Ljava/util/ArrayList;

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/g;->aa:[I

    iget-object v7, p0, Lcom/google/android/gms/wallet/ow/g;->ab:[I

    const/4 v8, 0x0

    iget-object v11, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    move-object v10, v9

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/g;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1446
    return-void
.end method

.method private c(Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2169
    iget-object v2, p1, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    .line 2170
    iget-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    const-string v3, "USD"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_3

    .line 2172
    iget v0, p1, Lcom/google/aa/a/a/a/f;->d:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    .line 2173
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2174
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    sget v3, Lcom/google/android/gms/p;->CZ:I

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/wallet/ow/g;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 2191
    :cond_0
    :goto_1
    return-void

    .line 2170
    :cond_1
    iget-object v0, v2, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2178
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 2181
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2183
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/wallet/ow/p;

    const/16 v2, 0xcc

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/cz;->e(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3, v1}, Lcom/google/android/gms/wallet/ow/p;-><init>(Lcom/google/android/gms/wallet/ow/g;ILcom/google/android/gms/wallet/common/ui/cz;B)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    .line 2184
    :cond_4
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->ai:Z

    .line 2185
    if-eqz p2, :cond_0

    .line 2186
    iget-object v0, p2, Lcom/google/android/gms/wallet/cache/a;->b:[Ljava/lang/String;

    const/4 v2, 0x6

    const-string v3, "walletBalanceNotPermitted"

    aput-object v3, v0, v2

    .line 2187
    iput-boolean v1, p2, Lcom/google/android/gms/wallet/cache/a;->a:Z

    goto :goto_1
.end method

.method private c(Lcom/google/aa/b/a/a/a/a/p;)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 2096
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ak:Lcom/google/aa/b/a/a/a/a/u;

    iget-boolean v0, v0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 2098
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Lcom/google/aa/b/a/h;

    .line 2099
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    invoke-interface {v1, v2}, Lcom/google/android/gms/wallet/ow/t;->a([Lcom/google/aa/b/a/h;)V

    .line 2100
    if-eqz v0, :cond_1

    .line 2101
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/ow/t;->a(Lcom/google/aa/b/a/h;)V

    .line 2105
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/e;->g:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2108
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->n:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2109
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    invoke-interface {v0, v3}, Lcom/google/android/gms/wallet/ow/t;->setVisibility(I)V

    .line 2115
    :goto_1
    return-void

    .line 2103
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    aget-object v1, v1, v3

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/ow/t;->a(Lcom/google/aa/b/a/h;)V

    goto :goto_0

    .line 2111
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2112
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2113
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/ow/t;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->g()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/aa/b/a/a/a/a/p;)V

    return-void
.end method

.method private c(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1507
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->V:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    iget-boolean v8, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    move-object v4, v2

    move-object v5, v2

    move-object v7, p1

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1517
    const/16 v1, 0x1f7

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/g;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1518
    return-void
.end method

.method private c(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 1450
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    iget-object v7, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    move-object v2, p1

    move-object v6, v5

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;ZZLjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;)Landroid/content/Intent;

    move-result-object v0

    .line 1460
    const/16 v1, 0x1f8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/g;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1461
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ow/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->aq:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/aa/b/a/a/a/a/p;)V

    return-void
.end method

.method private d(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1521
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->V:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    iget-boolean v7, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    move-object v2, p1

    move-object v5, v4

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/a/b;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1530
    const/16 v1, 0x1f9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/g;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1531
    return-void
.end method

.method private d(Lcom/google/aa/b/a/a/a/a/p;)Z
    .locals 3

    .prologue
    .line 2125
    iget-boolean v0, p1, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    .line 2127
    iget-boolean v1, p1, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    .line 2129
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Lcom/google/checkout/inapp/proto/a/b;)I
    .locals 1

    .prologue
    .line 1940
    if-nez p1, :cond_0

    .line 1941
    const/16 v0, 0x7b

    .line 1949
    :goto_0
    return v0

    .line 1942
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1943
    const/16 v0, 0x7c

    goto :goto_0

    .line 1944
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1945
    const/16 v0, 0x7d

    goto :goto_0

    .line 1946
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1947
    const/16 v0, 0x7e

    goto :goto_0

    .line 1949
    :cond_3
    const/16 v0, 0x7f

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 565
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    .line 566
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ow/g;->B:Z

    .line 567
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->n()V

    .line 568
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "ChooseMethodsFragment.WalletItemsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->s:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 570
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "ChooseMethodsFragment.MaskedWalletNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 572
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "ChooseMethodsFragment.OwErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->u:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 574
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "ChooseMethodsFragment.DefaultWalletInfoDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ca;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->t:Lcom/google/android/gms/wallet/common/ui/ca;

    .line 576
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "ChooseMethodsFragment.RequiredActionDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cz;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    .line 579
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->y:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->G:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;)V

    .line 580
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->s:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_3

    .line 582
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->s:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 593
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->r:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 597
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->u:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 598
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->u:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 601
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    if-eqz v0, :cond_2

    .line 602
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/android/gms/wallet/common/ui/db;)V

    .line 605
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->y:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->G:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ow/g;->D:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/g;->D:I

    .line 606
    return-void

    .line 583
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    if-eqz v0, :cond_4

    .line 584
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->G:Lcom/google/android/gms/wallet/service/l;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/aa/a/a/a/f;)V

    goto :goto_0

    .line 585
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_5

    .line 587
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->X:Z

    .line 588
    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/wallet/ow/g;->a(ZZ)V

    goto :goto_0

    .line 590
    :cond_5
    invoke-direct {p0, v2, v2}, Lcom/google/android/gms/wallet/ow/g;->a(ZZ)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->p()V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 745
    iget v0, p0, Lcom/google/android/gms/wallet/ow/g;->D:I

    if-gez v0, :cond_0

    .line 746
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->y:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->G:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/g;->D:I

    .line 750
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->c(I)V

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1051
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->m()V

    .line 1054
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1055
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->h()V

    .line 1059
    :goto_0
    return-void

    .line 1057
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ae:Lcom/google/android/apps/common/a/a/h;

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v3, "encrypt_otp_and_get_full_wallet"

    invoke-direct {v0, v3}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ae:Lcom/google/android/apps/common/a/a/h;

    :cond_2
    new-instance v0, Lcom/google/aa/b/a/a/a/a/i;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    new-instance v3, Lcom/google/aa/b/a/e;

    invoke-direct {v3}, Lcom/google/aa/b/a/e;-><init>()V

    iput-object v3, v0, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    const-string v3, "USD"

    iput-object v3, v0, Lcom/google/aa/b/a/e;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v3, v0, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    sget-object v0, Lcom/google/android/gms/wallet/b/c;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/aa/b/a/e;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iput-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/i;->m:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    iput-boolean v3, v0, Lcom/google/aa/b/a/a/a/a/i;->h:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, v3, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/aa/b/a/a/a/a/i;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lcom/google/aa/b/a/a/a/a/i;->i:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/aa/b/a/a/a/a/i;->z:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ow/g;->Y:Z

    iput-boolean v3, v0, Lcom/google/aa/b/a/a/a/a/i;->w:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->s()Z

    move-result v3

    iput-boolean v3, v0, Lcom/google/aa/b/a/a/a/a/i;->y:Z

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, v3, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/aa/b/a/a/a/a/i;->c:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->P:[Lcom/google/aa/b/a/a/a/a/s;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->P:[Lcom/google/aa/b/a/a/a/a/s;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/aa/b/a/a/a/a/s;)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/aa/b/a/a/a/a/i;->l:[Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Ljava/util/ArrayList;)[Lcom/google/aa/b/a/a/a/a/x;

    move-result-object v3

    iput-object v3, v0, Lcom/google/aa/b/a/a/a/a/i;->r:[Lcom/google/aa/b/a/a/a/a/x;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->y:Lcom/google/android/gms/wallet/service/k;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    invoke-interface {v0, v3, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/aa/b/a/a/a/a/i;Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method private h()V
    .locals 7

    .prologue
    const/16 v5, 0x7e

    const/4 v1, 0x0

    .line 1103
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ae:Lcom/google/android/apps/common/a/a/h;

    if-nez v0, :cond_1

    .line 1104
    :cond_0
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v2, "get_masked_wallet"

    invoke-direct {v0, v2}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    .line 1105
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ad:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ae:Lcom/google/android/apps/common/a/a/h;

    .line 1108
    :cond_1
    new-instance v2, Lcom/google/aa/b/a/a/a/a/k;

    invoke-direct {v2}, Lcom/google/aa/b/a/a/a/a/k;-><init>()V

    .line 1110
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ak:Lcom/google/aa/b/a/a/a/a/u;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    .line 1111
    iget-object v0, v2, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    .line 1114
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v3

    .line 1115
    const/16 v0, 0x7b

    .line 1116
    iget-boolean v4, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    if-eqz v4, :cond_2

    .line 1117
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->e(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    .line 1120
    :cond_2
    if-ne v3, v5, :cond_4

    .line 1121
    if-ne v0, v5, :cond_3

    .line 1122
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    invoke-static {v0, v3, v4, v5, v6}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/ow/a;

    move-result-object v0

    .line 1136
    :goto_0
    if-eqz v0, :cond_5

    .line 1137
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/ow/c;)V

    .line 1138
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "ChooseMethodsFragment.AddPhoneNumberDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 1173
    :goto_1
    return-void

    .line 1127
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/ow/a;

    move-result-object v0

    goto :goto_0

    .line 1132
    :cond_4
    if-ne v0, v5, :cond_b

    .line 1133
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/g;->Q:Ljava/util/ArrayList;

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/ow/a;

    move-result-object v0

    goto :goto_0

    .line 1142
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    .line 1143
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    if-eqz v0, :cond_6

    .line 1144
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    .line 1147
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v3

    .line 1149
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    .line 1150
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    .line 1151
    iget-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    if-eqz v0, :cond_8

    const-string v0, "requested"

    .line 1153
    :goto_2
    const-string v4, "choose_methods"

    const-string v5, "preauth"

    invoke-static {v3, v4, v5, v0, v1}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1160
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->P:[Lcom/google/aa/b/a/a/a/a/s;

    if-eqz v0, :cond_7

    .line 1161
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->P:[Lcom/google/aa/b/a/a/a/a/s;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/aa/b/a/a/a/a/s;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    .line 1165
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Lcom/google/aa/b/a/h;

    if-nez v0, :cond_a

    move-object v0, v1

    .line 1168
    :goto_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->s()Z

    move-result v1

    iput-boolean v1, v2, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    .line 1170
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->y:Lcom/google/android/gms/wallet/service/k;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/aa/b/a/a/a/a/k;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Z)V

    .line 1172
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    goto/16 :goto_1

    .line 1151
    :cond_8
    const-string v0, "not_requested"

    goto :goto_2

    .line 1156
    :cond_9
    const-string v0, "choose_methods"

    const-string v4, "preauth"

    const-string v5, "not_shown"

    invoke-static {v3, v0, v4, v5, v1}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_3

    .line 1165
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Lcom/google/aa/b/a/h;

    iget-object v0, v0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    goto :goto_4

    :cond_b
    move-object v0, v1

    goto/16 :goto_0
.end method

.method static synthetic h(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/checkout/inapp/proto/a/b;)V

    return-void
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/android/gms/wallet/ow/g;)[Lcom/google/aa/b/a/a/a/a/s;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->P:[Lcom/google/aa/b/a/a/a/a/s;

    return-object v0
.end method

.method private j()Lcom/google/android/gms/wallet/Cart;
    .locals 1

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_0

    .line 1199
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v0

    .line 1201
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic j(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 111
    invoke-direct {p0, v0, v0}, Lcom/google/android/gms/wallet/ow/g;->a(ZZ)V

    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1205
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_0

    .line 1206
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    .line 1208
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic k(Lcom/google/android/gms/wallet/ow/g;)Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    return v0
.end method

.method private l()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1212
    iget v0, p0, Lcom/google/android/gms/wallet/ow/g;->S:I

    if-lez v0, :cond_1

    move v0, v1

    .line 1213
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->o()Z

    move-result v3

    if-eq v0, v3, :cond_0

    .line 1214
    if-eqz v0, :cond_2

    .line 1215
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1216
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 1217
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    .line 1218
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/ow/t;->setEnabled(Z)V

    .line 1219
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1220
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 1221
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1222
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1234
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->p()V

    .line 1235
    return-void

    :cond_1
    move v0, v2

    .line 1212
    goto :goto_0

    .line 1224
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->c:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1225
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 1226
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    .line 1227
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/ow/t;->setEnabled(Z)V

    .line 1228
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1229
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1230
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1231
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_1
.end method

.method static synthetic l(Lcom/google/android/gms/wallet/ow/g;)Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ai:Z

    return v0
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1238
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->x:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->x:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->dQ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v1, v1, Lcom/google/t/a/b;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->n()V

    .line 1244
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->an:Lcom/google/android/gms/wallet/payform/f;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/payform/f;->a(Z)V

    .line 1245
    return-void

    .line 1241
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Z)V

    goto :goto_0
.end method

.method static synthetic m(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    return-void
.end method

.method private n()V
    .locals 1

    .prologue
    .line 1267
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/g;->S:I

    .line 1268
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->l()V

    .line 1269
    return-void
.end method

.method static synthetic n(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 111
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    iget v1, v0, Lcom/google/android/gms/wallet/ow/p;->a:I

    packed-switch v1, :pswitch_data_0

    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ah:Lcom/google/android/gms/wallet/ow/p;

    goto :goto_0

    :pswitch_0
    iget-object v1, v0, Lcom/google/android/gms/wallet/ow/p;->f:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/p;->c:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/android/gms/wallet/common/ui/cz;)V

    goto :goto_1

    :pswitch_1
    iget-object v1, v0, Lcom/google/android/gms/wallet/ow/p;->f:Lcom/google/android/gms/wallet/ow/g;

    iget-object v2, v0, Lcom/google/android/gms/wallet/ow/p;->d:Landroid/content/Intent;

    iget v3, v0, Lcom/google/android/gms/wallet/ow/p;->e:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/wallet/ow/g;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/p;->f:Lcom/google/android/gms/wallet/ow/g;

    iput-boolean v4, v0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    goto :goto_1

    :cond_2
    iput-boolean v4, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic o(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->q()V

    return-void
.end method

.method private o()Z
    .locals 1

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1282
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->o()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->x:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    .line 1286
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez v0, :cond_3

    :goto_2
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 1287
    return-void

    :cond_1
    move v0, v1

    .line 1282
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v2, v1

    .line 1286
    goto :goto_2
.end method

.method static synthetic p(Lcom/google/android/gms/wallet/ow/g;)Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->X:Z

    return v0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 2207
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2208
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2209
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2210
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2211
    return-void
.end method

.method static synthetic q(Lcom/google/android/gms/wallet/ow/g;)Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ai:Z

    return v0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 2251
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    if-nez v0, :cond_0

    .line 2263
    :goto_0
    return-void

    .line 2255
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cz;->dismiss()V

    .line 2256
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 2260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->v:Lcom/google/android/gms/wallet/common/ui/cz;

    .line 2261
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    .line 2262
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Z)V

    goto :goto_0
.end method

.method static synthetic r(Lcom/google/android/gms/wallet/ow/g;)Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->X:Z

    return v0
.end method

.method static synthetic s(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->af:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private s()Z
    .locals 1

    .prologue
    .line 2479
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic t(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ag:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method static synthetic u(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->af:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ag:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/gms/wallet/ow/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lcom/google/android/gms/wallet/ow/g;)Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic y(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/aa/b/a/a/a/a/i;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    return-object v0
.end method

.method static synthetic z(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/payform/f;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->an:Lcom/google/android/gms/wallet/payform/f;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2280
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->r()V

    .line 2284
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->c(I)V

    .line 2285
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2327
    sparse-switch p1, :sswitch_data_0

    .line 2336
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    .line 2339
    :goto_0
    return-void

    .line 2329
    :sswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Z)V

    goto :goto_0

    .line 2332
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->G:Lcom/google/android/gms/wallet/service/l;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/l;->c()V

    goto :goto_0

    .line 2327
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x65 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1015
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_1

    .line 1016
    packed-switch p1, :pswitch_data_0

    .line 1030
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/g;->b(Z)V

    .line 1039
    :goto_0
    return-void

    .line 1018
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_0

    .line 1019
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->g()V

    goto :goto_0

    .line 1021
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/g;->a(ZZ)V

    goto :goto_0

    .line 1026
    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/g;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1033
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1034
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->an:Lcom/google/android/gms/wallet/payform/f;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/payform/f;->a(Landroid/accounts/Account;)V

    goto :goto_0

    .line 1037
    :cond_2
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    goto :goto_0

    .line 1016
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/aa/b/a/h;)V
    .locals 1

    .prologue
    .line 2119
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Lcom/google/aa/b/a/h;

    .line 2121
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 0

    .prologue
    .line 2289
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->r()V

    .line 2293
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 2294
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 1

    .prologue
    .line 2274
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->r()V

    .line 2275
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 2276
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 0

    .prologue
    .line 2268
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->r()V

    .line 2269
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V

    .line 2270
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1044
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->J:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1045
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->K:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1046
    return-void

    :cond_0
    move v0, v2

    .line 1044
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1045
    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2304
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->r()V

    .line 2305
    return-void
.end method

.method public final b(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 1

    .prologue
    .line 2298
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->r()V

    .line 2299
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 2300
    return-void
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2310
    if-eqz p1, :cond_0

    .line 2311
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 2312
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    .line 2313
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 2316
    :cond_0
    if-eqz p2, :cond_1

    .line 2317
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 2318
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lcom/google/checkout/inapp/proto/a/b;

    .line 2319
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 2322
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->h()V

    .line 2323
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->I:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->I:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v1, 0x8

    .line 798
    iput-boolean v5, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    .line 799
    packed-switch p1, :pswitch_data_0

    .line 996
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1000
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "choose_methods"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 1004
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->B:Z

    if-eqz v0, :cond_1

    .line 1005
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->e()V

    .line 1007
    :cond_1
    invoke-direct {p0, v5}, Lcom/google/android/gms/wallet/ow/g;->b(Z)V

    .line 1010
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1011
    :goto_1
    return-void

    .line 801
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    .line 872
    :pswitch_1
    if-eqz p3, :cond_7

    const-string v0, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 873
    const-string v0, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    .line 879
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->ar:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/analytics/events/SignupActivityClosedEvent;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 803
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ar:Ljava/lang/String;

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/wallet/analytics/events/SignupActivityClosedEvent;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    .line 808
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 809
    const/4 v0, -0x1

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/wallet/ow/g;->a(ILandroid/content/Intent;)V

    goto :goto_1

    .line 813
    :cond_3
    iput-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 815
    const-string v0, "EXTRA_SIGN_UP_RESULT"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lcom/google/aa/b/a/a/a/a/l;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/l;

    .line 819
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->i()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 820
    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    .line 822
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 823
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/z;->a(Lcom/google/aa/b/a/a/a/a/w;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 826
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    .line 827
    iput-boolean v4, p0, Lcom/google/android/gms/wallet/ow/g;->Y:Z

    .line 831
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    if-eqz v0, :cond_5

    .line 832
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 835
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 837
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 840
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 846
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->g()V

    .line 852
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-nez v0, :cond_0

    .line 853
    iput-boolean v4, p0, Lcom/google/android/gms/wallet/ow/g;->X:Z

    goto/16 :goto_0

    .line 856
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->G:Lcom/google/android/gms/wallet/service/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/aa/b/a/a/a/a/l;)V

    goto/16 :goto_0

    .line 861
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->an:Lcom/google/android/gms/wallet/payform/f;

    const-string v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/payform/f;->a(Landroid/accounts/Account;)V

    .line 863
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ar:Ljava/lang/String;

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/wallet/analytics/events/SignupActivityClosedEvent;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 867
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->ar:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/analytics/events/SignupActivityClosedEvent;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    .line 869
    invoke-direct {p0, v5, v3}, Lcom/google/android/gms/wallet/ow/g;->a(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 877
    :cond_7
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    goto/16 :goto_2

    .line 885
    :pswitch_5
    packed-switch p2, :pswitch_data_2

    .line 908
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    goto/16 :goto_0

    .line 888
    :pswitch_6
    iput-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 889
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 892
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 893
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    .line 894
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 897
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_0

    .line 901
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 902
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_0

    .line 913
    :pswitch_8
    packed-switch p2, :pswitch_data_3

    .line 936
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    goto/16 :goto_0

    .line 916
    :pswitch_9
    iput-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 917
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 920
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 921
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    .line 922
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 925
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_0

    .line 929
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 930
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_0

    .line 941
    :pswitch_b
    packed-switch p2, :pswitch_data_4

    .line 963
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    goto/16 :goto_0

    .line 944
    :pswitch_c
    iput-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 945
    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 948
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 949
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lcom/google/checkout/inapp/proto/a/b;

    .line 950
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 953
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 957
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 958
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 968
    :pswitch_e
    packed-switch p2, :pswitch_data_5

    .line 990
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/g;->b(I)V

    goto/16 :goto_0

    .line 971
    :pswitch_f
    iput-object v3, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 972
    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 975
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 976
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lcom/google/checkout/inapp/proto/a/b;

    .line 977
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 980
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 984
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 985
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 799
    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_5
        :pswitch_b
        :pswitch_8
        :pswitch_e
    .end packed-switch

    .line 801
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 885
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 913
    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 941
    :pswitch_data_4
    .packed-switch -0x1
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 968
    :pswitch_data_5
    .packed-switch -0x1
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 409
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 410
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 411
    if-eqz v0, :cond_0

    .line 412
    const-string v3, "com.google.android.gms.wallet.ANALYTICS_SESSION_ID"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->aq:Ljava/lang/String;

    .line 416
    :cond_0
    iget v0, p0, Lcom/google/android/gms/wallet/ow/g;->ao:I

    const/4 v3, -0x2

    if-eq v0, v3, :cond_1

    .line 417
    iget v0, p0, Lcom/google/android/gms/wallet/ow/g;->ao:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ap:Landroid/content/Intent;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(ILandroid/content/Intent;)V

    .line 477
    :goto_0
    return-void

    .line 421
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 422
    const-string v0, "buyFlowConfig"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 423
    const-string v0, "maskedWalletRequest"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 424
    const-string v0, "maskedWalletRequest"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    .line 425
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->f()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    .line 428
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    if-eqz v0, :cond_2

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->p()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->V:Ljava/util/ArrayList;

    .line 434
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 437
    :goto_2
    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->o()Z

    move-result v4

    if-nez v4, :cond_3

    .line 438
    add-int/lit8 v0, v0, 0x1

    .line 440
    :cond_3
    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ab:[I

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->n()Z

    move-result v0

    if-nez v0, :cond_a

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ab:[I

    aput v1, v0, v2

    .line 445
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->o()Z

    move-result v0

    if-nez v0, :cond_4

    .line 446
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ab:[I

    aput v5, v0, v1

    .line 464
    :cond_4
    :goto_4
    const-string v0, "account"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->y:Lcom/google/android/gms/wallet/service/k;

    if-nez v0, :cond_5

    .line 467
    new-instance v0, Lcom/google/android/gms/wallet/service/e;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->O:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/gms/wallet/service/e;-><init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->y:Lcom/google/android/gms/wallet/service/k;

    .line 470
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->y:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/service/k;->a()V

    .line 474
    :cond_5
    :try_start_0
    check-cast p1, Lcom/google/android/gms/wallet/payform/f;

    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/g;->an:Lcom/google/android/gms/wallet/payform/f;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 476
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Activity must implement PaymentsDialogEventListener"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v0, v2

    .line 425
    goto/16 :goto_1

    .line 448
    :cond_7
    const-string v0, "immediateFullWalletRequest"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 449
    const-string v0, "immediateFullWalletRequest"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    .line 450
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g()Z

    move-result v0

    if-nez v0, :cond_8

    :goto_5
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->Z:Z

    .line 452
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    .line 453
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->U:Z

    .line 454
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->T:Z

    if-eqz v0, :cond_4

    .line 455
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->V:Ljava/util/ArrayList;

    goto :goto_4

    :cond_8
    move v1, v2

    .line 450
    goto :goto_5

    .line 461
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must specify masked wallet request or immediate full wallet request"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    move v1, v2

    goto/16 :goto_3

    :cond_b
    move v0, v2

    goto/16 :goto_2
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 482
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 483
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->setRetainInstance(Z)V

    .line 485
    if-eqz p1, :cond_6

    .line 486
    const-string v0, "model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    .line 487
    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    .line 489
    const-string v0, "dismissedRequiredAction"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    .line 491
    const-string v0, "walletItemsReceived"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    .line 492
    const-string v0, "performContinueClickAfterWalletItemsReceived"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->X:Z

    .line 494
    const-string v0, "isChromeSignUpFlow"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->Y:Z

    .line 495
    const-string v0, "getWalletItemsResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    const-string v0, "getWalletItemsResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lcom/google/aa/a/a/a/f;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/a/a/a/f;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 500
    :cond_0
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 501
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/g;->D:I

    .line 504
    :cond_1
    const-string v0, "pendingResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 505
    const-string v0, "pendingResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/g;->ao:I

    .line 507
    :cond_2
    const-string v0, "pendingResultData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 508
    const-string v0, "pendingResultData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ap:Landroid/content/Intent;

    .line 510
    :cond_3
    const-string v0, "fullWalletRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 511
    const-string v0, "fullWalletRequest"

    const-class v1, Lcom/google/aa/b/a/a/a/a/i;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/i;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    .line 514
    :cond_4
    const-string v0, "analyticsSessionId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 515
    const-string v0, "analyticsSessionId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ar:Ljava/lang/String;

    .line 544
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_9

    .line 545
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->M:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/b;)Lcom/google/aa/b/a/a/a/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ak:Lcom/google/aa/b/a/a/a/a/u;

    .line 551
    :goto_1
    return-void

    .line 518
    :cond_6
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "get_wallet_items"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->af:Lcom/google/android/apps/common/a/a/i;

    .line 519
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->af:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ag:Lcom/google/android/apps/common/a/a/h;

    .line 521
    new-instance v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    .line 522
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 523
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    const-string v2, "googleTransactionId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    .line 524
    const-string v1, "selectedInstrumentId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 525
    if-eqz v1, :cond_7

    .line 526
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    new-instance v3, Lcom/google/checkout/inapp/proto/j;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/j;-><init>()V

    iput-object v3, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 527
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iput-object v1, v2, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    .line 529
    :cond_7
    const-string v1, "selectedAddressId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 530
    if-eqz v1, :cond_8

    .line 531
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    new-instance v3, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v3, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 532
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v1, v2, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    .line 534
    :cond_8
    const-string v1, "startingContentHeight"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/ow/g;->al:I

    .line 535
    const-string v1, "startingContentHeight"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 536
    const-string v1, "startingContentWidth"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/ow/g;->am:I

    .line 537
    const-string v1, "startingContentWidth"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 540
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->L:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "choose_methods"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 548
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->N:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/aa/b/a/a/a/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ak:Lcom/google/aa/b/a/a/a/a/u;

    goto/16 :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v3, 0x0

    .line 612
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 613
    sget v0, Lcom/google/android/gms/l;->gw:I

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    .line 618
    :goto_0
    sget v0, Lcom/google/android/gms/j;->dG:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->I:Landroid/view/View;

    .line 619
    sget v0, Lcom/google/android/gms/j;->cL:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->b:Landroid/view/View;

    .line 620
    sget v0, Lcom/google/android/gms/j;->pO:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->c:Landroid/widget/ProgressBar;

    .line 621
    iget v0, p0, Lcom/google/android/gms/wallet/ow/g;->al:I

    if-ne v0, v5, :cond_3

    move v0, v1

    :goto_1
    iget v4, p0, Lcom/google/android/gms/wallet/ow/g;->am:I

    if-ne v4, v5, :cond_4

    move v4, v1

    :goto_2
    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 623
    iget v0, p0, Lcom/google/android/gms/wallet/ow/g;->al:I

    if-eq v0, v5, :cond_0

    .line 628
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->I:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 629
    iget v4, p0, Lcom/google/android/gms/wallet/ow/g;->al:I

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 630
    iget v4, p0, Lcom/google/android/gms/wallet/ow/g;->am:I

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 631
    iput v5, p0, Lcom/google/android/gms/wallet/ow/g;->al:I

    .line 632
    iput v5, p0, Lcom/google/android/gms/wallet/ow/g;->am:I

    .line 634
    :cond_0
    sget v0, Lcom/google/android/gms/j;->nu:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->w:Landroid/view/ViewGroup;

    .line 635
    sget v0, Lcom/google/android/gms/j;->kz:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    .line 637
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/g;->E:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v0, v4}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/android/gms/wallet/common/ui/cd;)V

    .line 638
    sget v0, Lcom/google/android/gms/j;->tI:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    .line 640
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "useWalletBalanceChecked"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 642
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 643
    const-string v5, "loading"

    new-instance v6, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v7

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    sget v0, Lcom/google/android/gms/q;->r:I

    :goto_4
    invoke-direct {v6, v7, v0}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 646
    sget v0, Lcom/google/android/gms/p;->CZ:I

    new-array v1, v1, [Ljava/lang/Object;

    sget v5, Lcom/google/android/gms/p;->Dd:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/wallet/ow/g;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/g;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 648
    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/common/ae;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 650
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->f:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 651
    sget v0, Lcom/google/android/gms/j;->au:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->g:Landroid/view/View;

    .line 652
    sget v0, Lcom/google/android/gms/j;->aw:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->h:Landroid/view/View;

    .line 653
    sget v0, Lcom/google/android/gms/j;->at:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/v;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    .line 654
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->F:Lcom/google/android/gms/wallet/common/ui/w;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/android/gms/wallet/common/ui/w;)V

    .line 655
    sget v0, Lcom/google/android/gms/j;->lz:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->m:Landroid/view/View;

    .line 656
    sget v0, Lcom/google/android/gms/j;->lA:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->n:Landroid/view/View;

    .line 657
    sget v0, Lcom/google/android/gms/j;->ly:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ow/t;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    .line 658
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    invoke-interface {v0, p0}, Lcom/google/android/gms/wallet/ow/t;->a(Lcom/google/android/gms/wallet/ow/u;)V

    .line 660
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 661
    sget v0, Lcom/google/android/gms/j;->cc:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->J:Landroid/view/View;

    .line 663
    sget v0, Lcom/google/android/gms/j;->te:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->K:Landroid/view/View;

    .line 664
    sget v0, Lcom/google/android/gms/j;->fQ:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    .line 666
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 667
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->i:Lcom/google/android/gms/wallet/common/ui/v;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 668
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->o:Lcom/google/android/gms/wallet/ow/t;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 669
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bp;)V

    .line 672
    :cond_1
    sget v0, Lcom/google/android/gms/j;->eb:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->j:Landroid/view/View;

    .line 674
    sget v0, Lcom/google/android/gms/j;->ea:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->k:Landroid/widget/CheckBox;

    .line 676
    sget v0, Lcom/google/android/gms/j;->ec:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->l:Landroid/view/View;

    .line 678
    sget v0, Lcom/google/android/gms/j;->dP:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->x:Landroid/view/View;

    .line 680
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->l:Landroid/view/View;

    new-instance v1, Lcom/google/android/gms/wallet/ow/h;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/ow/h;-><init>(Lcom/google/android/gms/wallet/ow/g;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 687
    sget v0, Lcom/google/android/gms/j;->ma:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->p:Landroid/widget/TextView;

    .line 688
    sget v0, Lcom/google/android/gms/j;->tg:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->q:Landroid/widget/TextView;

    .line 690
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 691
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    new-instance v1, Lcom/google/android/gms/wallet/ow/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/ow/i;-><init>(Lcom/google/android/gms/wallet/ow/g;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 708
    return-object v2

    .line 616
    :cond_2
    sget v0, Lcom/google/android/gms/l;->gv:I

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 621
    goto/16 :goto_1

    :cond_4
    move v4, v3

    goto/16 :goto_2

    :cond_5
    move v0, v3

    goto/16 :goto_3

    .line 643
    :cond_6
    sget v0, Lcom/google/android/gms/q;->v:I

    goto/16 :goto_4
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 792
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 793
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->an:Lcom/google/android/gms/wallet/payform/f;

    .line 794
    return-void
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 754
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    .line 755
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 756
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->f()V

    .line 757
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 555
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 557
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    if-eqz v0, :cond_0

    .line 558
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->B:Z

    .line 562
    :goto_0
    return-void

    .line 560
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->e()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 761
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 763
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/g;->f()V

    .line 765
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ow/g;->D:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 766
    const-string v0, "model"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 767
    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 768
    const-string v0, "dismissedRequiredAction"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->ac:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 769
    const-string v0, "walletItemsReceived"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 770
    const-string v0, "performContinueClickAfterWalletItemsReceived"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->X:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 772
    const-string v0, "isChromeSignUpFlow"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/g;->Y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 773
    const-string v0, "analyticsSessionId"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ar:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    if-eqz v0, :cond_0

    .line 775
    const-string v0, "getWalletItemsResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 778
    :cond_0
    iget v0, p0, Lcom/google/android/gms/wallet/ow/g;->ao:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_1

    .line 779
    const-string v0, "pendingResultCode"

    iget v1, p0, Lcom/google/android/gms/wallet/ow/g;->ao:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 781
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->ap:Landroid/content/Intent;

    if-eqz v0, :cond_2

    .line 782
    const-string v0, "pendingResultData"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->ap:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 784
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    if-eqz v0, :cond_3

    .line 785
    const-string v0, "fullWalletRequest"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/g;->R:Lcom/google/aa/b/a/a/a/a/i;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 788
    :cond_3
    return-void
.end method

.method public final startActivityForResult(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1327
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/g;->W:Z

    .line 1328
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/g;->aj:Z

    .line 1329
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Z)V

    .line 1330
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1331
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->E:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/q;->overridePendingTransition(II)V

    .line 1332
    return-void
.end method
