.class public abstract Lcom/google/android/gms/wallet/common/ui/at;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:[Landroid/widget/ImageView;

.field protected b:I


# direct methods
.method public constructor <init>([Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/at;->b:I

    .line 19
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/at;->a:[Landroid/widget/ImageView;

    .line 20
    return-void
.end method


# virtual methods
.method public abstract a(I)V
.end method

.method public abstract b(I)V
.end method

.method protected final c(I)I
    .locals 4

    .prologue
    .line 36
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/at;->a:[Landroid/widget/ImageView;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 37
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/at;->a:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 41
    :goto_1
    return v0

    .line 36
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
