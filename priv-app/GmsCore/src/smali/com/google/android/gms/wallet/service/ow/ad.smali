.class final Lcom/google/android/gms/wallet/service/ow/ad;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic d:Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ow/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/ad;->e:Lcom/google/android/gms/wallet/service/ow/y;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/ad;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ow/ad;->d:Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    const/16 v5, 0x18

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ad;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->c(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ad;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/ad;->d:Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/e;

    move-result-object v4

    const-string v2, "/online/v2/wallet/sdk/v1/getBinDerivedData"

    const-string v6, "get_bin_derived_data"

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ak;->a(ILjava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/protobuf/nano/j;ILjava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 269
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/f;

    .line 273
    iget v1, v0, Lcom/google/aa/b/a/a/a/a/f;->a:I

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/z;->a(I)I

    move-result v1

    iput v1, v0, Lcom/google/aa/b/a/a/a/a/f;->a:I

    .line 276
    new-instance v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v1, v5, v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    move-object v0, v1

    .line 279
    :cond_0
    return-object v0
.end method
