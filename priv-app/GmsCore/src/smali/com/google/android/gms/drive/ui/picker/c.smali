.class public final Lcom/google/android/gms/drive/ui/picker/c;
.super Lcom/google/android/gms/drive/ui/i;
.source "SourceFile"


# instance fields
.field private j:Landroid/widget/EditText;

.field private k:Lcom/google/android/gms/drive/ui/n;

.field private l:Z

.field private m:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/i;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->l:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/c;)Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->m:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)Lcom/google/android/gms/drive/ui/picker/c;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/c;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/picker/c;-><init>()V

    .line 40
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 41
    const-string v2, "progressTextId"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 42
    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/c;->setArguments(Landroid/os/Bundle;)V

    .line 43
    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/drive/ui/picker/c;)Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->l:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/drive/ui/picker/c;)Lcom/google/android/gms/drive/ui/n;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->k:Lcom/google/android/gms/drive/ui/n;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/drive/ui/picker/c;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    .line 62
    new-instance v1, Lcom/google/android/gms/drive/ui/picker/d;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/drive/ui/picker/d;-><init>(Lcom/google/android/gms/drive/ui/picker/c;Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/c;->k:Lcom/google/android/gms/drive/ui/n;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/ui/k;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 75
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    .line 76
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/c;->m:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    iget v2, v2, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->b:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 77
    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    invoke-virtual {v2, v6}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 79
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->setSingleLine()V

    .line 80
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    const/16 v3, 0x4001

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 83
    invoke-static {v0}, Lcom/google/android/gms/drive/ui/k;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->m:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    iget v0, v0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->d:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    const/16 v3, 0x15

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/c;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/g;->w:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    new-instance v4, Landroid/widget/FrameLayout;

    invoke-direct {v4, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v4, v3, v5, v3, v5}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 86
    :goto_0
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 87
    const v0, 0x104000a

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/e;-><init>(Lcom/google/android/gms/drive/ui/picker/c;)V

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 96
    const/high16 v0, 0x1040000

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/f;-><init>(Lcom/google/android/gms/drive/ui/picker/c;)V

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 104
    sget-object v0, Lcom/google/android/gms/drive/ui/o;->a:Lcom/google/android/gms/drive/ui/o;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 105
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/gms/drive/ui/picker/g;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/drive/ui/picker/g;-><init>(Lcom/google/android/gms/drive/ui/picker/c;Landroid/app/Dialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/gms/drive/ui/l;

    const v3, 0x1020019

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/drive/ui/l;-><init>(Landroid/app/Dialog;I)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 119
    return-object v0

    .line 85
    :cond_0
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/i;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/c;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 55
    const-string v1, "progressTextId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->m:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    .line 56
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->k:Lcom/google/android/gms/drive/ui/n;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->k:Lcom/google/android/gms/drive/ui/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/n;->a()V

    .line 144
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/i;->onDismiss(Landroid/content/DialogInterface;)V

    .line 145
    return-void
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/i;->onResume()V

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/c;->l:Z

    .line 151
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/c;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/c;->m:Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    iget v1, v1, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;->b:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/c;->j:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method
