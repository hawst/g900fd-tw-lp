.class public final Lcom/google/android/gms/games/g/m;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Z

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 659
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 660
    iput v0, p0, Lcom/google/android/gms/games/g/m;->a:I

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/m;->b:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/m;->cachedSize:I

    .line 661
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 723
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 724
    iget v1, p0, Lcom/google/android/gms/games/g/m;->a:I

    if-eqz v1, :cond_0

    .line 725
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/m;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 728
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/m;->b:Z

    if-eqz v1, :cond_1

    .line 729
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/m;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 732
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 733
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 736
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 673
    if-ne p1, p0, :cond_1

    .line 693
    :cond_0
    :goto_0
    return v0

    .line 676
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/m;

    if-nez v2, :cond_2

    move v0, v1

    .line 677
    goto :goto_0

    .line 679
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/m;

    .line 680
    iget v2, p0, Lcom/google/android/gms/games/g/m;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/m;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 681
    goto :goto_0

    .line 683
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/m;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/m;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 684
    goto :goto_0

    .line 686
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 687
    iget-object v2, p1, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 688
    goto :goto_0

    .line 690
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 691
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 698
    iget v0, p0, Lcom/google/android/gms/games/g/m;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 700
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/m;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 701
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 703
    return v0

    .line 700
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 701
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 620
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/m;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/m;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 709
    iget v0, p0, Lcom/google/android/gms/games/g/m;->a:I

    if-eqz v0, :cond_0

    .line 710
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/m;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 712
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/m;->b:Z

    if-eqz v0, :cond_1

    .line 713
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/m;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 715
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 716
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/g/m;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 718
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 719
    return-void
.end method
