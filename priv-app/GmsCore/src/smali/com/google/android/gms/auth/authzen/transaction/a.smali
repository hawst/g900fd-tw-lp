.class public abstract Lcom/google/android/gms/auth/authzen/transaction/a;
.super Lcom/google/android/gms/auth/authzen/transaction/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field protected a:Landroid/os/Bundle;

.field protected b:Lcom/google/android/gms/common/api/v;

.field private m:Ljava/lang/String;

.field private n:Lcom/google/android/gms/auth/authzen/transaction/h;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;-><init>()V

    return-void
.end method

.method protected static a(Lcom/google/ab/b/a/a/r;Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 68
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 70
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 71
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v1, "tx_request"

    invoke-virtual {p0}, Lcom/google/ab/b/a/a/r;->g()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 73
    const-string v1, "encryption_key_handle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 74
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x200000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x800000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x40000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 80
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/authzen/transaction/a;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/authzen/transaction/a;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 54
    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/gms/j;->pM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/authzen/transaction/a;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    invoke-static {p1}, Lcom/google/android/gms/common/util/au;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/gms/j;->mi:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 344
    invoke-static {p1}, Lcom/google/android/gms/common/util/au;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 345
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 347
    :cond_0
    return-void
.end method

.method private dismiss()V
    .locals 2

    .prologue
    const/16 v1, 0xc7

    .line 174
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {p0, v1, v1}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(II)V

    .line 177
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->setResult(I)V

    .line 178
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->finish()V

    .line 179
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->e:Lcom/google/ab/b/a/a/s;

    iget-boolean v0, v0, Lcom/google/ab/b/a/a/s;->a:Z

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->e:Lcom/google/ab/b/a/a/s;

    new-instance v1, Lcom/google/ab/b/a/c/b;

    invoke-direct {v1}, Lcom/google/ab/b/a/c/b;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/ab/b/a/a/s;->a(Lcom/google/ab/b/a/c/b;)Lcom/google/ab/b/a/a/s;

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->e:Lcom/google/ab/b/a/a/s;

    iget-object v0, v0, Lcom/google/ab/b/a/a/s;->b:Lcom/google/ab/b/a/c/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/ab/b/a/c/b;->b(Z)Lcom/google/ab/b/a/c/b;

    .line 252
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->b(Ljava/lang/String;)V

    .line 192
    return-void
.end method

.method protected final a(Lcom/google/android/gms/auth/authzen/transaction/a/f;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 261
    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(II)V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/a;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    .line 263
    return-void
.end method

.method protected final a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V
    .locals 3

    .prologue
    .line 280
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 282
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/auth/authzen/transaction/a/f;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->m:Ljava/lang/String;

    .line 284
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/auth/authzen/transaction/a/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    sget v0, Lcom/google/android/gms/j;->gs:I

    invoke-virtual {p2}, Lcom/google/android/gms/auth/authzen/transaction/a/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, p2, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->d()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    .line 285
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 3

    .prologue
    .line 410
    const-string v0, "AuthZen"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load Image due to connection failure : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 350
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/a;->b(Ljava/lang/String;)V

    .line 351
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->setResult(I)V

    .line 352
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->finish()V

    .line 353
    return-void
.end method

.method protected final a(Ljava/lang/String;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V
    .locals 3

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->m:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Current fragment doesn\'t match the one to be swapped"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/auth/authzen/transaction/a/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    sget v0, Lcom/google/android/gms/j;->gs:I

    invoke-virtual {p2}, Lcom/google/android/gms/auth/authzen/transaction/a/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, p2, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->d()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->b()I

    .line 292
    :cond_2
    return-void
.end method

.method public a(Lcom/google/android/gms/auth/authzen/transaction/a/f;I)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->n:Lcom/google/android/gms/auth/authzen/transaction/h;

    if-eqz v0, :cond_0

    .line 226
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->n:Lcom/google/android/gms/auth/authzen/transaction/h;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/authzen/transaction/a/f;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/e;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p2, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/auth/authzen/transaction/h;->b()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v3}, Lcom/google/android/gms/auth/authzen/transaction/h;->a()V

    :goto_1
    packed-switch v0, :pswitch_data_0

    :cond_0
    move v1, v2

    .line 244
    :goto_2
    return v1

    .line 226
    :cond_1
    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    .line 228
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    sget-object v2, Lcom/google/android/gms/auth/authzen/transaction/b/h;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->b(Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/d;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/d;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto :goto_2

    .line 233
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/d;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/d;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    .line 234
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->e()V

    goto :goto_2

    .line 237
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_0

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->b(Ljava/lang/String;)V

    .line 212
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/16 v5, 0x9f

    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 86
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/authzen/transaction/a;->setRequestedOrientation(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->d:Lcom/google/ab/b/a/a/r;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/b/i;->a(Lcom/google/ab/b/a/a/r;)Lcom/google/android/gms/auth/authzen/transaction/b/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/auth/authzen/transaction/b/h;->b()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    .line 91
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/authzen/transaction/a;->requestWindowFeature(I)Z

    sget v0, Lcom/google/android/gms/l;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->setContentView(I)V

    .line 92
    new-instance v0, Lcom/google/android/gms/people/ad;

    invoke-direct {v0}, Lcom/google/android/gms/people/ad;-><init>()V

    iput v5, v0, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v0}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    .line 94
    new-instance v3, Lcom/google/android/gms/common/api/w;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v4, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0, v5, p0}, Lcom/google/android/gms/common/api/w;->a(Landroid/support/v4/app/q;ILcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->b:Lcom/google/android/gms/common/api/v;

    .line 102
    sget v0, Lcom/google/android/gms/j;->fm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 103
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->d:Lcom/google/ab/b/a/a/r;

    iget-object v0, v0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-boolean v0, v0, Lcom/google/ab/b/a/a/q;->d:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_7

    move-object v0, v7

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->n:Lcom/google/android/gms/auth/authzen/transaction/h;

    .line 106
    if-nez p1, :cond_9

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->n:Lcom/google/android/gms/auth/authzen/transaction/h;

    if-eqz v0, :cond_8

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->n:Lcom/google/android/gms/auth/authzen/transaction/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/authzen/transaction/h;->b:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/e;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/e;

    move-result-object v0

    invoke-virtual {p0, v7, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    .line 116
    :goto_2
    return-void

    .line 105
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/auth/authzen/a/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/auth/authzen/a/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v0, "device_policy"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getActiveAdmins()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    if-nez v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_3

    :cond_5
    const-string v0, "AuthZen"

    const-string v3, "Not device admin, cannot lock screen"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_0

    :cond_7
    new-instance v0, Lcom/google/android/gms/auth/authzen/transaction/h;

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->d:Lcom/google/ab/b/a/a/r;

    iget-object v2, v1, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    iget-wide v4, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->h:J

    new-instance v6, Lcom/google/android/gms/auth/authzen/transaction/b;

    invoke-direct {v6, p0}, Lcom/google/android/gms/auth/authzen/transaction/b;-><init>(Lcom/google/android/gms/auth/authzen/transaction/a;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/auth/authzen/transaction/h;-><init>(Landroid/app/Activity;Lcom/google/ab/b/a/a/q;Landroid/os/Bundle;JLcom/google/android/gms/auth/authzen/transaction/k;)V

    goto :goto_1

    .line 111
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/d;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/d;

    move-result-object v0

    invoke-virtual {p0, v7, v0}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto :goto_2

    .line 114
    :cond_9
    const-string v0, "current_fragment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->m:Ljava/lang/String;

    goto :goto_2
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 372
    sget-object v0, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->b:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/authzen/transaction/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/authzen/transaction/c;-><init>(Lcom/google/android/gms/auth/authzen/transaction/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 384
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->b:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/people/o;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/authzen/transaction/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/authzen/transaction/d;-><init>(Lcom/google/android/gms/auth/authzen/transaction/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 401
    return-void
.end method

.method protected final c()Lcom/google/android/gms/auth/authzen/transaction/a/f;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->a:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/d;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 406
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 169
    invoke-super {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->onBackPressed()V

    .line 170
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->dismiss()V

    .line 171
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->onDestroy()V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->n:Lcom/google/android/gms/auth/authzen/transaction/h;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->n:Lcom/google/android/gms/auth/authzen/transaction/h;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/authzen/transaction/h;->a()V

    .line 187
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->onResume()V

    .line 153
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/e;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 158
    const-string v0, "current_fragment"

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/a;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public onUserLeaveHint()V
    .locals 0

    .prologue
    .line 163
    invoke-super {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->onUserLeaveHint()V

    .line 164
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->dismiss()V

    .line 165
    return-void
.end method
