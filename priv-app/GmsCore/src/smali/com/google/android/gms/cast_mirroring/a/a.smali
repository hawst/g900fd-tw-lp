.class public final Lcom/google/android/gms/cast_mirroring/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 9
    const-string v0, "gms:cast_mirroring:allow_non_chromecast_app_mirroring"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast_mirroring/a/a;->a:Lcom/google/android/gms/common/a/d;

    .line 12
    const-string v0, "gms:cast_mirroring:google_cast_package_name"

    const-string v1, "com.google.android.apps.chromecast.app"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast_mirroring/a/a;->b:Lcom/google/android/gms/common/a/d;

    .line 16
    const-string v0, "gms:cast_mirroring:chromecast_mirror_test_app_package_name"

    const-string v1, "com.google.android.gms.chromecast_mirroring"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast_mirroring/a/a;->c:Lcom/google/android/gms/common/a/d;

    .line 20
    const-string v0, "gms:cast:media:stats_flags"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast_mirroring/a/a;->d:Lcom/google/android/gms/common/a/d;

    return-void
.end method
