.class public final Lcom/google/android/gms/googlehelp/c/a/b;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:[Lcom/google/ad/a/a/m;

.field private final c:Z

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Spinner;Lcom/google/ad/a/a/n;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/a/b;->a:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/a/b;->e:Landroid/widget/Spinner;

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->d:Landroid/view/LayoutInflater;

    .line 30
    iget-boolean v0, p3, Lcom/google/ad/a/a/n;->c:Z

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->c:Z

    .line 31
    iget-object v2, p3, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    .line 32
    array-length v0, v2

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Lcom/google/ad/a/a/m;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->b:[Lcom/google/ad/a/a/m;

    .line 34
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/c/a/b;->b:[Lcom/google/ad/a/a/m;

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/b;->b()I

    move-result v4

    new-instance v5, Lcom/google/ad/a/a/m;

    invoke-direct {v5}, Lcom/google/ad/a/a/m;-><init>()V

    const-string v0, ""

    iput-object v0, v5, Lcom/google/ad/a/a/m;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->c:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/p;->oB:I

    :goto_0
    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/google/ad/a/a/m;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 35
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/c/a/b;->b:[Lcom/google/ad/a/a/m;

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    array-length v4, v2

    invoke-static {v2, v1, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    return-void

    .line 34
    :cond_0
    sget v0, Lcom/google/android/gms/p;->oA:I

    goto :goto_0

    .line 35
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 127
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->d:Landroid/view/LayoutInflater;

    const v1, 0x1090008

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 130
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/b;->a(I)Lcom/google/ad/a/a/m;

    move-result-object v0

    .line 131
    iget-object v2, v0, Lcom/google/ad/a/a/m;->d:Ljava/lang/String;

    move-object v0, v1

    .line 132
    check-cast v0, Landroid/widget/TextView;

    .line 133
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 135
    return-object v1

    :cond_0
    move-object v1, p2

    .line 127
    goto :goto_0
.end method

.method private b()I
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->b:[Lcom/google/ad/a/a/m;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/google/ad/a/a/m;
    .locals 1

    .prologue
    .line 67
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->b:[Lcom/google/ad/a/a/m;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->b:[Lcom/google/ad/a/a/m;

    aget-object v0, v0, p1

    .line 71
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->e:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->e:Landroid/widget/Spinner;

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/b;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 43
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->b:[Lcom/google/ad/a/a/m;

    array-length v1, v0

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sub-int v0, v1, v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->c:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/b;->b()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 99
    const/4 v1, 0x0

    .line 112
    :goto_0
    return-object v1

    .line 102
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/googlehelp/c/a/b;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/b;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/g;->aZ:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 108
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/a/b;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/g;->aY:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 110
    invoke-virtual {v1, v0, v0, v0, v2}, Landroid/view/View;->setPadding(IIII)V

    move-object v0, v1

    .line 111
    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/gms/googlehelp/c/a/c;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/googlehelp/c/a/c;-><init>(Lcom/google/android/gms/googlehelp/c/a/b;Landroid/widget/TextView;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/b;->a(I)Lcom/google/ad/a/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/a/b;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    int-to-long v0, p1

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 81
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/googlehelp/c/a/b;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/b;->b()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/b;->b:[Lcom/google/ad/a/a/m;

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/google/ad/a/a/m;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 85
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/b;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->bb:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 91
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 93
    return-object v0
.end method
