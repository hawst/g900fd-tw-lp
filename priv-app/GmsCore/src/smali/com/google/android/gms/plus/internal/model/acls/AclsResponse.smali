.class public Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/internal/model/acls/c;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/plus/internal/model/acls/c;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/model/acls/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->CREATOR:Lcom/google/android/gms/plus/internal/model/acls/c;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput p1, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->a:I

    .line 71
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->b:Ljava/lang/String;

    .line 72
    iput-object p3, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->c:Ljava/lang/String;

    .line 73
    iput-object p4, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->d:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 74
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->d:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 128
    instance-of v1, p1, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    if-nez v1, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v0

    .line 132
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    .line 133
    iget v1, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->a:I

    iget v2, p1, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->d:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->d:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->d:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 105
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/internal/model/acls/c;->a(Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;Landroid/os/Parcel;I)V

    .line 106
    return-void
.end method
