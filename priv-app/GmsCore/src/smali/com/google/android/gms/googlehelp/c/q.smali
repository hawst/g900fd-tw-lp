.class public final Lcom/google/android/gms/googlehelp/c/q;
.super Landroid/widget/RadioGroup;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/c/af;
.implements Lcom/google/android/gms/googlehelp/c/c;
.implements Lcom/google/android/gms/googlehelp/c/e;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Z

.field private c:Lcom/google/android/gms/googlehelp/c/d;

.field private d:Lcom/google/android/gms/googlehelp/c/ac;

.field private e:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 58
    invoke-direct {p0, p1}, Landroid/widget/RadioGroup;-><init>(Landroid/content/Context;)V

    .line 60
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/q;->c:Lcom/google/android/gms/googlehelp/c/d;

    .line 61
    iput-object v1, p0, Lcom/google/android/gms/googlehelp/c/q;->d:Lcom/google/android/gms/googlehelp/c/ac;

    .line 62
    iput-object v1, p0, Lcom/google/android/gms/googlehelp/c/q;->e:Ljava/util/List;

    .line 63
    iget-boolean v1, p3, Lcom/google/ad/a/a/h;->c:Z

    iput-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/q;->b:Z

    .line 64
    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/q;->b:Z

    if-eqz v1, :cond_0

    .line 65
    new-instance v1, Lcom/google/android/gms/googlehelp/c/r;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/c/r;-><init>(Lcom/google/android/gms/googlehelp/c/q;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/c/q;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 75
    :cond_0
    iget-object v1, p3, Lcom/google/ad/a/a/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/c/q;->setTag(Ljava/lang/Object;)V

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/c/q;->a:Ljava/util/List;

    .line 80
    iget-object v4, p3, Lcom/google/ad/a/a/h;->e:[Lcom/google/ad/a/a/k;

    array-length v5, v4

    move v2, v0

    move v1, v0

    move v0, v3

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    .line 83
    new-instance v7, Lcom/google/android/gms/googlehelp/c/p;

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v7, p1, v1, v6}, Lcom/google/android/gms/googlehelp/c/p;-><init>(Landroid/content/Context;ILcom/google/ad/a/a/k;)V

    .line 84
    iget-object v8, p0, Lcom/google/android/gms/googlehelp/c/q;->a:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    invoke-virtual {p0, v7}, Lcom/google/android/gms/googlehelp/c/q;->addView(Landroid/view/View;)V

    .line 87
    invoke-virtual {v7}, Lcom/google/android/gms/googlehelp/c/p;->a()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 88
    invoke-virtual {v7}, Lcom/google/android/gms/googlehelp/c/p;->b()Landroid/widget/EditText;

    move-result-object v7

    new-instance v8, Landroid/widget/LinearLayout;

    invoke-direct {v8, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v9, Landroid/widget/RadioGroup$LayoutParams;

    const/4 v10, -0x2

    invoke-direct {v9, v3, v10}, Landroid/widget/RadioGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v8, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0, v8}, Lcom/google/android/gms/googlehelp/c/q;->addView(Landroid/view/View;)V

    .line 91
    :cond_1
    iget-boolean v6, v6, Lcom/google/ad/a/a/k;->c:Z

    if-eqz v6, :cond_2

    move v0, v1

    .line 80
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 95
    :cond_3
    if-lez v0, :cond_4

    .line 96
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/q;->check(I)V

    .line 98
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/q;->c:Lcom/google/android/gms/googlehelp/c/d;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/c/d;->a(Lcom/google/android/gms/googlehelp/c/af;)V

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/c/q;)Lcom/google/android/gms/googlehelp/c/ac;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/q;->d:Lcom/google/android/gms/googlehelp/c/ac;

    return-object v0
.end method

.method private g()Lcom/google/android/gms/googlehelp/c/p;
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/q;->getCheckedRadioButtonId()I

    move-result v0

    .line 141
    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/p;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/c/ac;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/q;->d:Lcom/google/android/gms/googlehelp/c/ac;

    .line 158
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/q;->e:Ljava/util/List;

    .line 113
    new-instance v0, Lcom/google/android/gms/googlehelp/c/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/c/s;-><init>(Lcom/google/android/gms/googlehelp/c/q;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/q;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 119
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/q;->getCheckedRadioButtonId()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/q;->c:Lcom/google/android/gms/googlehelp/c/d;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/d;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 5

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/q;->g()Lcom/google/android/gms/googlehelp/c/p;

    move-result-object v3

    .line 163
    if-nez v3, :cond_0

    .line 164
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    .line 167
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/q;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 169
    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/c/p;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 170
    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/c/p;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 173
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "--"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/c/p;->b()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object v0, v2

    .line 177
    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 182
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/q;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/googlehelp/c/c;

    aput-object p0, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v1, [Lcom/google/android/gms/googlehelp/c/c;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/q;->g()Lcom/google/android/gms/googlehelp/c/p;

    move-result-object v0

    .line 124
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/p;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/q;->e:Ljava/util/List;

    if-nez v0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/q;->e:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/c/ad;->a(Ljava/util/List;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/q;->d:Lcom/google/android/gms/googlehelp/c/ac;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/q;->d:Lcom/google/android/gms/googlehelp/c/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/ac;->a()V

    goto :goto_0
.end method
