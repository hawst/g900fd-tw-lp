.class final Lcom/google/android/gms/plus/audience/cc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/cc;->a:Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 6

    .prologue
    .line 454
    check-cast p1, Lcom/google/android/gms/people/m;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cc;->a:Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Status;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/cc;->a:Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cc;->a:Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cc;->a:Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->b(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)Z

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cc;->a:Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->c(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cc;->a:Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->d(Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->b()Z

    move-result v2

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->e()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
