.class public Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;
.super Lcom/google/android/gms/drive/ui/h;
.source "SourceFile"


# instance fields
.field private b:Lcom/google/android/gms/drive/g/t;

.field private c:Lcom/google/android/gms/drive/DriveId;

.field private final d:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/h;-><init>()V

    .line 150
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->d:Landroid/os/Handler;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/DriveId;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 136
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    const-string v1, "com.google.android.gms.drive.ui.picker.CreateFolderActivity.CREATE_DOC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const-string v1, "driveId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 143
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->a(Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)V
    .locals 3

    .prologue
    .line 225
    invoke-static {p1}, Lcom/google/android/gms/drive/ui/picker/c;->a(Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)Lcom/google/android/gms/drive/ui/picker/c;

    move-result-object v0

    .line 234
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/c;->setRetainInstance(Z)V

    .line 235
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "editTitleDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/c;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->c:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)Lcom/google/android/gms/drive/g/t;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->b:Lcom/google/android/gms/drive/g/t;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/a;-><init>(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 217
    return-void
.end method

.method final a(Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)V
    .locals 2

    .prologue
    .line 250
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/b;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/drive/ui/picker/b;-><init>(Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;Ljava/lang/String;Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 251
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 182
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/drive/ui/h;->onActivityResult(IILandroid/content/Intent;)V

    .line 184
    const-string v0, "CreateFolderActivity"

    const-string v1, "Unexpected activity request code: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 189
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 154
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/h;->onCreate(Landroid/os/Bundle;)V

    .line 156
    if-eqz p1, :cond_0

    .line 178
    :goto_0
    return-void

    .line 160
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/g/ay;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/g/ay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->b:Lcom/google/android/gms/drive/g/t;

    .line 162
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 163
    const-string v0, "driveId"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->c:Lcom/google/android/gms/drive/DriveId;

    .line 165
    const-string v0, "accountName"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 166
    if-nez v0, :cond_1

    .line 167
    const-string v0, "CreateFolderActivity"

    const-string v2, "Account name is not specified in the intent."

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-virtual {p0, v3}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->setResult(I)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->finish()V

    .line 172
    :cond_1
    const-string v0, "com.google.android.gms.drive.ui.picker.CreateFolderActivity.CREATE_DOC"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;

    const-string v1, "application/vnd.google-apps.folder"

    sget v2, Lcom/google/android/gms/p;->fO:I

    sget v3, Lcom/google/android/gms/p;->fR:I

    sget v4, Lcom/google/android/gms/p;->gH:I

    sget v5, Lcom/google/android/gms/p;->fQ:I

    sget v6, Lcom/google/android/gms/p;->fP:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;-><init>(Ljava/lang/String;IIIII)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->a(Lcom/google/android/gms/drive/ui/picker/SimpleEntryCreator;)V

    goto :goto_0

    .line 175
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->setResult(I)V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->finish()V

    goto :goto_0
.end method
