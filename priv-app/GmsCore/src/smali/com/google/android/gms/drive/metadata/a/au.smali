.class public abstract Lcom/google/android/gms/drive/metadata/a/au;
.super Lcom/google/android/gms/drive/metadata/a/ay;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/drive/metadata/a/ay;-><init>(Lcom/google/android/gms/drive/metadata/f;ZLjava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final synthetic a(Lorg/json/JSONObject;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 19
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v1}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 19
    check-cast p1, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 19
    check-cast p1, Ljava/util/Date;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/drive/database/h;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lorg/json/JSONObject;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 19
    check-cast p2, Ljava/util/Date;

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    return-void
.end method
