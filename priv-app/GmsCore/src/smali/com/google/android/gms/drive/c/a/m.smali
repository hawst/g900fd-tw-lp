.class Lcom/google/android/gms/drive/c/a/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/c/a/g;


# instance fields
.field protected final a:Lcom/google/android/libraries/rocket/impressions/h;

.field protected final b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/rocket/impressions/h;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/rocket/impressions/h;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/m;->a:Lcom/google/android/libraries/rocket/impressions/h;

    .line 19
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/m;->b:Landroid/content/Context;

    .line 20
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/m;->a:Lcom/google/android/libraries/rocket/impressions/h;

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/h;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/google/android/libraries/rocket/impressions/l;->a:Lcom/google/android/libraries/rocket/impressions/l;

    const-string v3, "startSession"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/rocket/impressions/h;->a(Lcom/google/android/libraries/rocket/impressions/l;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/libraries/rocket/impressions/h;->a()V

    iget-object v0, v0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    sget-object v2, Lcom/google/android/libraries/rocket/impressions/l;->c:Lcom/google/android/libraries/rocket/impressions/l;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Lcom/google/android/libraries/rocket/impressions/l;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public a(Lcom/google/android/gms/drive/c/a/d;)V
    .locals 6

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/m;->a:Lcom/google/android/libraries/rocket/impressions/h;

    iget-object v1, p1, Lcom/google/android/gms/drive/c/a/d;->a:Lcom/google/android/libraries/rocket/impressions/g;

    new-instance v2, Lcom/google/android/libraries/rocket/impressions/g;

    invoke-direct {v2, v1}, Lcom/google/android/libraries/rocket/impressions/g;-><init>(Lcom/google/android/libraries/rocket/impressions/g;)V

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/h;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v3, Lcom/google/android/libraries/rocket/impressions/l;->c:Lcom/google/android/libraries/rocket/impressions/l;

    const-string v4, "log"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/rocket/impressions/h;->a(Lcom/google/android/libraries/rocket/impressions/l;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/libraries/rocket/impressions/h;->e()Z

    iget-object v3, v0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v3}, Lcom/google/android/libraries/rocket/impressions/Session;->e()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/rocket/impressions/g;->b(J)Lcom/google/android/libraries/rocket/impressions/g;

    iget-object v3, v0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    invoke-virtual {v3}, Lcom/google/android/libraries/rocket/impressions/Session;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/libraries/rocket/impressions/g;->a(J)Lcom/google/android/libraries/rocket/impressions/g;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/rocket/impressions/h;->a(Lcom/google/android/libraries/rocket/impressions/g;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/m;->a:Lcom/google/android/libraries/rocket/impressions/h;

    iget-object v1, v0, Lcom/google/android/libraries/rocket/impressions/h;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/google/android/libraries/rocket/impressions/l;->c:Lcom/google/android/libraries/rocket/impressions/l;

    const-string v3, "endSession"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/rocket/impressions/h;->a(Lcom/google/android/libraries/rocket/impressions/l;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/libraries/rocket/impressions/h;->d()V

    iget-object v2, v0, Lcom/google/android/libraries/rocket/impressions/h;->d:Lcom/google/android/libraries/rocket/impressions/Session;

    sget-object v3, Lcom/google/android/libraries/rocket/impressions/l;->d:Lcom/google/android/libraries/rocket/impressions/l;

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/rocket/impressions/Session;->a(Lcom/google/android/libraries/rocket/impressions/l;)V

    iget-object v0, v0, Lcom/google/android/libraries/rocket/impressions/h;->c:Lcom/google/android/libraries/rocket/impressions/o;

    invoke-interface {v0}, Lcom/google/android/libraries/rocket/impressions/o;->b()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public synthetic c()Lcom/google/android/gms/drive/c/a;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/google/android/gms/drive/c/a/m;->d()Lcom/google/android/gms/drive/c/a/d;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/google/android/gms/drive/c/a/d;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/drive/c/a/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/m;->b:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/drive/c/a/d;-><init>(Lcom/google/android/gms/drive/c/a/g;Landroid/content/Context;)V

    return-object v0
.end method
