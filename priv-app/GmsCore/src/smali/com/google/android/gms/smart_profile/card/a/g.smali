.class public final Lcom/google/android/gms/smart_profile/card/a/g;
.super Lcom/google/android/gms/smart_profile/card/a/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/smart_profile/b/b;


# instance fields
.field e:Lcom/google/ac/c/a/a/a/al;

.field private f:Landroid/widget/LinearLayout;

.field private g:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/b;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/gms/smart_profile/bg;ILandroid/widget/LinearLayout;)V
    .locals 6

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    aget-object v1, v0, p2

    .line 104
    new-instance v3, Lcom/google/android/gms/smart_profile/card/view/a;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/gms/smart_profile/card/view/a;-><init>(Landroid/content/Context;)V

    .line 105
    iget-object v0, v1, Lcom/google/ac/c/a/a/a/an;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 106
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 107
    new-instance v0, Lcom/google/android/gms/smart_profile/card/a/h;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/smart_profile/card/a/h;-><init>(Lcom/google/android/gms/smart_profile/card/a/g;Lcom/google/android/gms/smart_profile/bg;Lcom/google/ac/c/a/a/a/an;)V

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, v1, Lcom/google/ac/c/a/a/a/an;->c:Ljava/lang/String;

    .line 123
    if-eqz v0, :cond_0

    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 126
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .line 130
    :goto_0
    new-instance v0, Lcom/google/android/gms/smart_profile/b/a;

    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/bg;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    const/4 v5, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/smart_profile/b/a;-><init>(Lcom/google/android/gms/smart_profile/b/b;Lcom/google/android/gms/common/api/v;Landroid/view/View;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/b/a;->a()V

    .line 133
    invoke-virtual {p3, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 134
    return-void

    :cond_1
    move-object v4, v0

    goto :goto_0
.end method

.method private d()I
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/al;->c:Ljava/lang/Integer;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/al;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/au;)V
    .locals 4

    .prologue
    const/4 v1, 0x5

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->f:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 99
    :cond_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 90
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    .line 91
    const/4 v0, 0x0

    .line 92
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v3, v3, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    if-ge v0, v1, :cond_2

    .line 93
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/a/g;->f:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2, v0, v3}, Lcom/google/android/gms/smart_profile/card/a/g;->a(Lcom/google/android/gms/smart_profile/bg;ILandroid/widget/LinearLayout;)V

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 96
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v1, v1, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/g;->g:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/gms/smart_profile/card/a/g;->a(Lcom/google/android/gms/smart_profile/bg;ILandroid/widget/LinearLayout;)V

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final synthetic a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 39
    check-cast p1, Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final synthetic a(Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 39
    check-cast p1, Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v1, v0, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/a/g;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 82
    :cond_0
    :goto_1
    return-void

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    goto :goto_0

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 60
    invoke-virtual {p3}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 61
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v1, v1, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v1, v1, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 63
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->xg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/g;->d()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_4

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->tR:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v1

    invoke-static {v1, v0}, Lcom/google/android/gms/smart_profile/bf;->a(ILandroid/widget/TextView;)V

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/p;->xk:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/g;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    new-instance v1, Lcom/google/android/gms/smart_profile/card/a/i;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/smart_profile/card/a/i;-><init>(Lcom/google/android/gms/smart_profile/card/a/g;Lcom/google/android/gms/smart_profile/bg;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->bM:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->f:Landroid/widget/LinearLayout;

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->bN:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->g:Landroid/widget/LinearLayout;

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    array-length v0, v0

    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->g:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 66
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v1, Landroid/support/v7/widget/CardView;

    invoke-virtual {v1}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->xh:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 73
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v1, Lcom/google/android/gms/j;->rl:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 193
    invoke-super {p0}, Lcom/google/android/gms/smart_profile/card/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    if-nez v0, :cond_2

    :cond_0
    move v1, v2

    :goto_0
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    array-length v0, v0

    move v1, v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/google/android/gms/smart_profile/c;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method
