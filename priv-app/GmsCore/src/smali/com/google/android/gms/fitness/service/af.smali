.class final Lcom/google/android/gms/fitness/service/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/internal/ae;

.field final synthetic b:Lcom/google/android/gms/fitness/service/i;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/internal/ae;)V
    .locals 0

    .prologue
    .line 2104
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/af;->b:Lcom/google/android/gms/fitness/service/i;

    iput-object p2, p0, Lcom/google/android/gms/fitness/service/af;->a:Lcom/google/android/gms/fitness/internal/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2109
    :try_start_0
    new-instance v0, Lcom/google/android/gms/fitness/result/SyncInfoResult;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/af;->b:Lcom/google/android/gms/fitness/service/i;

    invoke-static {v2}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/l/z;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/fitness/l/z;->c()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/fitness/result/SyncInfoResult;-><init>(Lcom/google/android/gms/common/api/Status;J)V
    :try_end_0
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_0

    .line 2116
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/af;->a:Lcom/google/android/gms/fitness/internal/ae;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/internal/ae;->a(Lcom/google/android/gms/fitness/result/SyncInfoResult;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2120
    :goto_1
    return-void

    .line 2111
    :catch_0
    move-exception v0

    const-string v1, "Cannot get the oldest change timestamp"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2113
    invoke-static {}, Lcom/google/android/gms/fitness/service/i;->b()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/result/SyncInfoResult;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/SyncInfoResult;

    move-result-object v0

    goto :goto_0

    .line 2117
    :catch_1
    move-exception v0

    const-string v1, "Couldn\'t return result to caller"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
