.class public final Lcom/google/android/gms/appstate/service/a/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/appstate/service/c;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/appstate/c/h;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/appstate/service/a/i;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/appstate/service/a/i;->b:Lcom/google/android/gms/appstate/c/h;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/appstate/a/e;)V
    .locals 3

    .prologue
    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/i;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/appstate/a/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/appstate/d/b/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    .line 51
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a/i;->b:Lcom/google/android/gms/appstate/c/h;

    invoke-interface {v1, v0}, Lcom/google/android/gms/appstate/c/h;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3

    .line 55
    :goto_1
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    const-string v1, "WipeStateOp"

    const-string v2, "Auth error while performing operation, requesting reconnect"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 38
    const/4 v0, 0x2

    .line 47
    goto :goto_0

    .line 39
    :catch_1
    move-exception v0

    .line 40
    const-string v1, "WipeStateOp"

    invoke-virtual {v0}, Lcom/google/android/gms/appstate/d/b/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 41
    invoke-virtual {v0}, Lcom/google/android/gms/appstate/d/b/a;->a()I

    move-result v0

    goto :goto_0

    .line 42
    :catch_2
    move-exception v0

    .line 44
    const-string v1, "WipeStateOp"

    const-string v2, "Runtime exception while performing operation"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 45
    const-string v1, "WipeStateOp"

    const-string v2, "Killing (on development devices) due to RuntimeException"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 46
    const/4 v0, 0x1

    goto :goto_0

    .line 55
    :catch_3
    move-exception v0

    goto :goto_1
.end method
