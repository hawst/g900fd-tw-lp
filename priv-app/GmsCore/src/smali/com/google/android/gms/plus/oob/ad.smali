.class public final Lcom/google/android/gms/plus/oob/ad;
.super Landroid/text/style/ClickableSpan;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/ad;->a:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/google/android/gms/plus/oob/ad;->b:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/google/android/gms/plus/oob/ad;->c:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lcom/google/android/gms/plus/oob/ad;->d:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/e;->f:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 49
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/common/activity/DpadNavigableWebViewActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    iget-object v2, p0, Lcom/google/android/gms/plus/oob/ad;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 51
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 57
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ad;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/ad;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/ad;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/ui/p;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/ui/p;

    move-result-object v1

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/ad;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/ui/p;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ad;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    sget v1, Lcom/google/android/gms/e;->e:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 63
    sget v1, Lcom/google/android/gms/f;->ah:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 64
    return-void
.end method
