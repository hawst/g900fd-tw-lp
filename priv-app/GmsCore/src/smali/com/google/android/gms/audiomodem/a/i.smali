.class public abstract Lcom/google/android/gms/audiomodem/a/i;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/audiomodem/a/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.google.android.gms.audiomodem.internal.IAudioModemService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/audiomodem/a/i;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/h;
    .locals 2

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    const-string v0, "com.google.android.gms.audiomodem.internal.IAudioModemService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/audiomodem/a/h;

    if-eqz v1, :cond_1

    .line 33
    check-cast v0, Lcom/google/android/gms/audiomodem/a/h;

    goto :goto_0

    .line 35
    :cond_1
    new-instance v0, Lcom/google/android/gms/audiomodem/a/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/audiomodem/a/j;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 131
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 47
    :sswitch_0
    const-string v0, "com.google.android.gms.audiomodem.internal.IAudioModemService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    .line 48
    goto :goto_0

    .line 52
    :sswitch_1
    const-string v0, "com.google.android.gms.audiomodem.internal.IAudioModemService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/a/r;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/q;

    move-result-object v4

    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    .line 65
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/audiomodem/a/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/e;

    move-result-object v1

    .line 66
    invoke-virtual {p0, v2, v4, v0, v1}, Lcom/google/android/gms/audiomodem/a/i;->a(Landroid/os/IBinder;Lcom/google/android/gms/audiomodem/a/q;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;Lcom/google/android/gms/audiomodem/a/e;)V

    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    .line 68
    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 62
    goto :goto_1

    .line 72
    :sswitch_2
    const-string v0, "com.google.android.gms.audiomodem.internal.IAudioModemService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/a/r;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/q;

    move-result-object v0

    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/audiomodem/a/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/e;

    move-result-object v1

    .line 77
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/audiomodem/a/i;->a(Lcom/google/android/gms/audiomodem/a/q;Lcom/google/android/gms/audiomodem/a/e;)V

    .line 78
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    .line 79
    goto :goto_0

    .line 83
    :sswitch_3
    const-string v0, "com.google.android.gms.audiomodem.internal.IAudioModemService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/a/o;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/n;

    move-result-object v4

    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    sget-object v0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    .line 96
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/audiomodem/a/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/e;

    move-result-object v1

    .line 97
    invoke-virtual {p0, v2, v4, v0, v1}, Lcom/google/android/gms/audiomodem/a/i;->a(Landroid/os/IBinder;Lcom/google/android/gms/audiomodem/a/n;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;Lcom/google/android/gms/audiomodem/a/e;)V

    .line 98
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    .line 99
    goto/16 :goto_0

    :cond_1
    move-object v0, v1

    .line 93
    goto :goto_2

    .line 103
    :sswitch_4
    const-string v0, "com.google.android.gms.audiomodem.internal.IAudioModemService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/a/o;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/n;

    move-result-object v0

    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/audiomodem/a/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/e;

    move-result-object v1

    .line 108
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/audiomodem/a/i;->a(Lcom/google/android/gms/audiomodem/a/n;Lcom/google/android/gms/audiomodem/a/e;)V

    .line 109
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    .line 110
    goto/16 :goto_0

    .line 114
    :sswitch_5
    const-string v0, "com.google.android.gms.audiomodem.internal.IAudioModemService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    sget-object v0, Lcom/google/android/gms/audiomodem/Snoop$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/Snoop$Params;

    move-object v2, v0

    .line 123
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    if-nez v4, :cond_3

    .line 125
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/a/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/e;

    move-result-object v0

    .line 126
    invoke-virtual {p0, v2, v1, v0}, Lcom/google/android/gms/audiomodem/a/i;->a(Lcom/google/android/gms/audiomodem/Snoop$Params;Lcom/google/android/gms/audiomodem/a/k;Lcom/google/android/gms/audiomodem/a/e;)V

    .line 127
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    .line 128
    goto/16 :goto_0

    :cond_2
    move-object v2, v1

    .line 120
    goto :goto_3

    .line 123
    :cond_3
    const-string v0, "com.google.android.gms.audiomodem.internal.ISnoopCallback"

    invoke-interface {v4, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v1, v0, Lcom/google/android/gms/audiomodem/a/k;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/google/android/gms/audiomodem/a/k;

    move-object v1, v0

    goto :goto_4

    :cond_4
    new-instance v1, Lcom/google/android/gms/audiomodem/a/m;

    invoke-direct {v1, v4}, Lcom/google/android/gms/audiomodem/a/m;-><init>(Landroid/os/IBinder;)V

    goto :goto_4

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
