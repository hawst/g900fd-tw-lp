.class Lcom/google/android/gms/drive/metadata/sync/c/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/c/a;


# instance fields
.field private final a:J

.field private final b:I

.field private final c:J

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JIJ)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    cmp-long v0, p2, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Duration must not be negative"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 30
    if-ltz p4, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Threshold must not be negative"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 31
    cmp-long v0, p5, v4

    if-ltz v0, :cond_2

    :goto_2
    const-string v0, "Window size must not be negative"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 32
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/l;->d:Ljava/lang/String;

    .line 33
    iput-wide p2, p0, Lcom/google/android/gms/drive/metadata/sync/c/l;->a:J

    .line 34
    iput p4, p0, Lcom/google/android/gms/drive/metadata/sync/c/l;->b:I

    .line 35
    iput-wide p5, p0, Lcom/google/android/gms/drive/metadata/sync/c/l;->c:J

    .line 36
    return-void

    :cond_0
    move v0, v2

    .line 28
    goto :goto_0

    :cond_1
    move v0, v2

    .line 30
    goto :goto_1

    :cond_2
    move v1, v2

    .line 31
    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/l;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(J)Z
    .locals 3

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/l;->a:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/metadata/sync/c/b;J)Z
    .locals 4

    .prologue
    .line 40
    invoke-interface {p1}, Lcom/google/android/gms/drive/metadata/sync/c/b;->a()Lcom/google/android/gms/drive/metadata/sync/c/k;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/l;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/c/k;->a(I)J

    move-result-wide v0

    sub-long v0, p2, v0

    .line 42
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/l;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
