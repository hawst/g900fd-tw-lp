.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/service/v2whitelisted/models/a;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/d;

.field private static final i:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Z

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/d;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/d;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 71
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->i:Ljava/util/HashMap;

    const-string v1, "description"

    const-string v2, "description"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->i:Ljava/util/HashMap;

    const-string v1, "enabledForSharing"

    const-string v2, "enabledForSharing"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->i:Ljava/util/HashMap;

    const-string v1, "etag"

    const-string v2, "etag"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->i:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "id"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->i:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->i:Ljava/util/HashMap;

    const-string v1, "people"

    const-string v2, "people"

    const/16 v3, 0x8

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 137
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->b:I

    .line 138
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a:Ljava/util/Set;

    .line 139
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;)V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 152
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a:Ljava/util/Set;

    .line 153
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->b:I

    .line 154
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->c:Ljava/lang/String;

    .line 155
    iput-boolean p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->d:Z

    .line 156
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->e:Ljava/lang/String;

    .line 157
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->f:Ljava/lang/String;

    .line 158
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->g:Ljava/lang/String;

    .line 159
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->h:Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;

    .line 160
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;)V
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 172
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a:Ljava/util/Set;

    .line 173
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->b:I

    .line 174
    iput-object p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->c:Ljava/lang/String;

    .line 175
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->d:Z

    .line 176
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->e:Ljava/lang/String;

    .line 177
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->f:Ljava/lang/String;

    .line 178
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->g:Ljava/lang/String;

    .line 179
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->h:Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;

    .line 180
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 591
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 592
    packed-switch v0, :pswitch_data_0

    .line 597
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 594
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->h:Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;

    .line 601
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 602
    return-void

    .line 592
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 567
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 568
    packed-switch v0, :pswitch_data_0

    .line 582
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 570
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->c:Ljava/lang/String;

    .line 585
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 586
    return-void

    .line 573
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 576
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->f:Ljava/lang/String;

    goto :goto_0

    .line 579
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->g:Ljava/lang/String;

    goto :goto_0

    .line 568
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 552
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 553
    packed-switch v0, :pswitch_data_0

    .line 558
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 555
    :pswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->d:Z

    .line 561
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 562
    return-void

    .line 553
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 518
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 532
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->c:Ljava/lang/String;

    .line 530
    :goto_0
    return-object v0

    .line 522
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 524
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 526
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->f:Ljava/lang/String;

    goto :goto_0

    .line 528
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->g:Ljava/lang/String;

    goto :goto_0

    .line 530
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->h:Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;

    goto :goto_0

    .line 518
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 20
    return-object p0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->d:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 503
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/d;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 630
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 661
    :goto_0
    return v0

    .line 635
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 636
    goto :goto_0

    .line 639
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    .line 640
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 641
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 642
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 644
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 646
    goto :goto_0

    :cond_3
    move v0, v1

    .line 651
    goto :goto_0

    .line 654
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 656
    goto :goto_0

    :cond_5
    move v0, v2

    .line 661
    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 617
    const/4 v0, 0x0

    .line 618
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->i:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 619
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 620
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 621
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 623
    goto :goto_0

    .line 624
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 508
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/d;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/d;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;Landroid/os/Parcel;I)V

    .line 509
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 612
    const/4 v0, 0x1

    return v0
.end method
