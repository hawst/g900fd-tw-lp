.class final Lcom/google/android/gms/droidguard/d/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/io/File;

.field final b:Ljava/io/File;

.field final c:Ljava/io/File;

.field final d:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/droidguard/d/ab;->b:Ljava/io/File;

    .line 33
    iput-object p3, p0, Lcom/google/android/gms/droidguard/d/ab;->c:Ljava/io/File;

    .line 34
    iput-object p4, p0, Lcom/google/android/gms/droidguard/d/ab;->d:Ljava/io/File;

    .line 35
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v1, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v0

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->b:Ljava/io/File;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->c:Ljava/io/File;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    .line 106
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v0

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->b:Ljava/io/File;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->c:Ljava/io/File;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->d:Ljava/io/File;

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->d:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->d:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 127
    :catch_0
    move-exception v1

    goto :goto_0
.end method
