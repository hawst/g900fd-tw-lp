.class public Lcom/google/android/gms/cast/media/CastMirroringProvider;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/media/aa;


# static fields
.field private static a:Lcom/google/android/gms/cast/media/CastMirroringProvider;


# instance fields
.field private final b:Lcom/google/android/gms/cast/e/g;

.field private final c:Lcom/google/android/gms/cast/media/a;

.field private final d:Landroid/content/Context;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/android/gms/cast/media/o;

.field private g:Lcom/google/android/gms/cast_mirroring/b/a;

.field private h:Lcom/google/android/gms/cast_mirroring/b/a;

.field private i:Lcom/google/android/gms/cast_mirroring/b/f;

.field private j:Lcom/google/android/gms/cast/media/ab;

.field private k:Lcom/google/android/gms/cast/CastDevice;

.field private l:Lcom/google/android/gms/cast/CastDevice;

.field private m:Z

.field private n:Z

.field private o:Landroid/app/PendingIntent;

.field private p:Landroid/app/PendingIntent;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d:Landroid/content/Context;

    .line 70
    invoke-static {p1}, Lcom/google/android/gms/cast/media/a;->a(Landroid/content/Context;)Lcom/google/android/gms/cast/media/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->c:Lcom/google/android/gms/cast/media/a;

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->c:Lcom/google/android/gms/cast/media/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/aa;)V

    .line 72
    new-instance v0, Lcom/google/android/gms/cast/e/g;

    const-string v1, "CastMirroringProvider"

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/cast/e/g;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->b:Lcom/google/android/gms/cast/e/g;

    .line 73
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/cast/media/CastMirroringProvider;
    .locals 2

    .prologue
    .line 60
    const-class v1, Lcom/google/android/gms/cast/media/CastMirroringProvider;

    monitor-enter v1

    .line 61
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/google/android/gms/cast/media/CastMirroringProvider;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    .line 64
    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->c:Lcom/google/android/gms/cast/media/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/media/a;->a(Ljava/lang/String;)Landroid/support/v7/c/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 86
    if-eqz v0, :cond_0

    .line 87
    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f:Lcom/google/android/gms/cast/media/o;

    .line 88
    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/o;->d()V

    .line 90
    iget-object v0, v0, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->l:Lcom/google/android/gms/cast/CastDevice;

    .line 91
    iput-boolean v1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->m:Z

    .line 92
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->h()V

    move v0, v1

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e()Lcom/google/android/gms/cast/media/CastMirroringProvider;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    return-object v0
.end method

.method private f()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->p:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    .line 220
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.MAIN"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/cast_mirroring/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 222
    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->p:Landroid/app/PendingIntent;

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->p:Landroid/app/PendingIntent;

    return-object v0
.end method

.method private g()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->o:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    .line 230
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.cast.media.ACTION_DISCONNECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/cast/media/CastMirroringProvider$Receiver;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 232
    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->o:Landroid/app/PendingIntent;

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->o:Landroid/app/PendingIntent;

    return-object v0
.end method

.method private h()V
    .locals 9

    .prologue
    const v8, 0x1080038

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->j:Lcom/google/android/gms/cast/media/ab;

    if-nez v0, :cond_1

    .line 257
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->k:Lcom/google/android/gms/cast/CastDevice;

    .line 258
    const-string v0, "CastMirroringProvider"

    const-string v1, "No Notifier"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->l:Lcom/google/android/gms/cast/CastDevice;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->m:Z

    if-eqz v0, :cond_4

    move v0, v1

    .line 263
    :goto_1
    const-string v3, "CastMirroringProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Connecting: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v3, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->k:Lcom/google/android/gms/cast/CastDevice;

    iget-object v4, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->l:Lcom/google/android/gms/cast/CastDevice;

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->n:Z

    if-eq v3, v0, :cond_0

    .line 265
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->k:Lcom/google/android/gms/cast/CastDevice;

    if-eqz v3, :cond_3

    .line 266
    const-string v3, "CastMirroringProvider"

    const-string v4, "Clearing Notification for new one."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v3, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->j:Lcom/google/android/gms/cast/media/ab;

    invoke-interface {v3}, Lcom/google/android/gms/cast/media/ab;->a()V

    .line 270
    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->l:Lcom/google/android/gms/cast/CastDevice;

    iput-object v3, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->k:Lcom/google/android/gms/cast/CastDevice;

    .line 271
    iput-boolean v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->n:Z

    .line 272
    const-string v3, "CastMirroringProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Notified Display: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->k:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-object v3, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->k:Lcom/google/android/gms/cast/CastDevice;

    if-eqz v3, :cond_0

    .line 276
    iget-object v3, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 277
    if-eqz v0, :cond_5

    .line 278
    new-instance v0, Landroid/support/v4/app/bk;

    iget-object v4, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/gms/p;->dL:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->dK:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->l:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v6}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f()Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v0, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    sget v2, Lcom/google/android/gms/h;->bW:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0, v7, v1}, Landroid/support/v4/app/bk;->a(IZ)V

    sget v1, Lcom/google/android/gms/p;->dN:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v8, v1, v2}, Landroid/support/v4/app/bk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->j:Lcom/google/android/gms/cast/media/ab;

    sget v2, Lcom/google/android/gms/p;->dL:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/cast/media/ab;->a(ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 262
    goto/16 :goto_1

    .line 295
    :cond_5
    new-instance v0, Landroid/support/v4/app/bk;

    iget-object v4, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/gms/p;->dJ:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->dI:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->l:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v6}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f()Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v0, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    sget v2, Lcom/google/android/gms/h;->bX:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0, v7, v1}, Landroid/support/v4/app/bk;->a(IZ)V

    sget v1, Lcom/google/android/gms/p;->dN:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v8, v1, v2}, Landroid/support/v4/app/bk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    .line 308
    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->j:Lcom/google/android/gms/cast/media/ab;

    sget v2, Lcom/google/android/gms/p;->dJ:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/cast/media/ab;->a(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->c:Lcom/google/android/gms/cast/media/a;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/a;->a()Lcom/google/android/gms/cast/media/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f:Lcom/google/android/gms/cast/media/o;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f:Lcom/google/android/gms/cast/media/o;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f:Lcom/google/android/gms/cast/media/o;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->l:Lcom/google/android/gms/cast/CastDevice;

    .line 80
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->b:Lcom/google/android/gms/cast/e/g;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/e/g;->a()V

    .line 169
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g:Lcom/google/android/gms/cast_mirroring/b/a;

    if-eqz v0, :cond_0

    .line 172
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v0}, Lcom/google/android/gms/cast_mirroring/b/a;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :goto_0
    iput-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g:Lcom/google/android/gms/cast_mirroring/b/a;

    .line 177
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->m:Z

    .line 178
    iput-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->e:Ljava/lang/String;

    .line 180
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->h()V

    .line 181
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f:Lcom/google/android/gms/cast/media/o;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a(Ljava/lang/String;)Z

    .line 154
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;Z)V
    .locals 5

    .prologue
    const/16 v0, 0x7d5

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 188
    iget-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->b:Lcom/google/android/gms/cast/e/g;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/e/g;->b()V

    .line 189
    iget-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->h:Lcom/google/android/gms/cast_mirroring/b/a;

    if-eqz v2, :cond_3

    .line 191
    if-eqz p2, :cond_2

    .line 192
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->h:Lcom/google/android/gms/cast_mirroring/b/a;

    const/4 v3, 0x7

    invoke-interface {v2, v3}, Lcom/google/android/gms/cast_mirroring/b/a;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_0
    iput-object v4, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->h:Lcom/google/android/gms/cast_mirroring/b/a;

    .line 205
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->i:Lcom/google/android/gms/cast_mirroring/b/f;

    if-eqz v2, :cond_1

    .line 207
    if-eqz p2, :cond_4

    .line 209
    :goto_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->i:Lcom/google/android/gms/cast_mirroring/b/f;

    invoke-interface {v2, v0}, Lcom/google/android/gms/cast_mirroring/b/f;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 213
    :cond_1
    :goto_3
    iput-object v4, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->l:Lcom/google/android/gms/cast/CastDevice;

    .line 214
    iput-boolean v1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->m:Z

    .line 215
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->h()V

    .line 216
    return-void

    .line 194
    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->h:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v2}, Lcom/google/android/gms/cast_mirroring/b/a;->b()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    .line 199
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g:Lcom/google/android/gms/cast_mirroring/b/a;

    if-eqz v2, :cond_0

    .line 201
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g:Lcom/google/android/gms/cast_mirroring/b/a;

    const/16 v3, 0x7d5

    invoke-interface {v2, v3}, Lcom/google/android/gms/cast_mirroring/b/a;->a(I)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    .line 203
    :goto_4
    iput-object v4, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g:Lcom/google/android/gms/cast_mirroring/b/a;

    goto :goto_1

    :cond_4
    move v0, v1

    .line 207
    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v2

    goto :goto_4
.end method

.method public final a(Lcom/google/android/gms/cast/media/ab;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->j:Lcom/google/android/gms/cast/media/ab;

    .line 142
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->h()V

    .line 143
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast_mirroring/b/a;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f:Lcom/google/android/gms/cast/media/o;

    if-eqz v0, :cond_0

    .line 119
    iput-object p1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->h:Lcom/google/android/gms/cast_mirroring/b/a;

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f:Lcom/google/android/gms/cast/media/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/o;->e()V

    .line 131
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 124
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/cast_mirroring/b/a;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 128
    :cond_1
    :goto_1
    const/16 v0, 0x7d1

    :try_start_1
    invoke-interface {p1, v0}, Lcom/google/android/gms/cast_mirroring/b/a;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/cast_mirroring/b/a;Lcom/google/android/gms/cast_mirroring/b/f;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f:Lcom/google/android/gms/cast/media/o;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->f:Lcom/google/android/gms/cast/media/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/o;->e()V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g:Lcom/google/android/gms/cast_mirroring/b/a;

    if-eqz v0, :cond_1

    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g:Lcom/google/android/gms/cast_mirroring/b/a;

    const/16 v1, 0x7d2

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast_mirroring/b/a;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->g:Lcom/google/android/gms/cast_mirroring/b/a;

    .line 111
    iput-object p3, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->e:Ljava/lang/String;

    .line 112
    iput-object p4, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->p:Landroid/app/PendingIntent;

    .line 113
    iput-object p2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->i:Lcom/google/android/gms/cast_mirroring/b/f;

    .line 114
    invoke-direct {p0, p3}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a(Ljava/lang/String;)Z

    .line 115
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/cast/CastDevice;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->l:Lcom/google/android/gms/cast/CastDevice;

    return-object v0
.end method

.method final c()V
    .locals 3

    .prologue
    .line 239
    const-string v0, "CastMirroringProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Disconnecting: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->k:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->k:Lcom/google/android/gms/cast/CastDevice;

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a(Lcom/google/android/gms/cast_mirroring/b/a;)V

    .line 243
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->d()V

    .line 244
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->k:Lcom/google/android/gms/cast/CastDevice;

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->j:Lcom/google/android/gms/cast/media/ab;

    if-nez v0, :cond_0

    .line 249
    const-string v0, "CastMirroringProvider"

    const-string v1, "No Notifier"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastMirroringProvider;->j:Lcom/google/android/gms/cast/media/ab;

    invoke-interface {v0}, Lcom/google/android/gms/cast/media/ab;->a()V

    goto :goto_0
.end method
