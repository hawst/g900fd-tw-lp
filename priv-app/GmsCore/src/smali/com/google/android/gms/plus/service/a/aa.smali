.class public final Lcom/google/android/gms/plus/service/a/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/common/images/a;

.field private final b:Landroid/net/Uri;

.field private final c:I

.field private final d:Lcom/google/android/gms/plus/internal/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/images/a;Landroid/net/Uri;ILcom/google/android/gms/plus/internal/c;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/aa;->a:Lcom/google/android/gms/common/images/a;

    .line 43
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/aa;->b:Landroid/net/Uri;

    .line 44
    iput p3, p0, Lcom/google/android/gms/plus/service/a/aa;->c:I

    .line 45
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/aa;->d:Lcom/google/android/gms/plus/internal/c;

    .line 46
    return-void
.end method

.method private a(ILandroid/os/ParcelFileDescriptor;)V
    .locals 4

    .prologue
    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/aa;->d:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, p2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    if-eqz p2, :cond_0

    .line 70
    :try_start_1
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    const-string v1, "LoadImageOperation"

    const-string v2, "Failed close"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    if-eqz p2, :cond_1

    .line 70
    :try_start_2
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 73
    :cond_1
    :goto_1
    throw v0

    .line 71
    :catch_1
    move-exception v1

    .line 72
    const-string v2, "LoadImageOperation"

    const-string v3, "Failed close"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/aa;->d:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/aa;->d:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    .line 84
    :cond_0
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 3

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/gms/plus/service/ImageIntentService;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/aa;->b:Landroid/net/Uri;

    iget v1, p0, Lcom/google/android/gms/plus/service/a/aa;->c:I

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/aa;->b:Landroid/net/Uri;

    iget v1, p0, Lcom/google/android/gms/plus/service/a/aa;->c:I

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "bounding_box"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/aa;->a:Lcom/google/android/gms/common/images/a;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/service/a/aa;->a(ILandroid/os/ParcelFileDescriptor;)V

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/plus/service/a/aa;->a(ILandroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method
