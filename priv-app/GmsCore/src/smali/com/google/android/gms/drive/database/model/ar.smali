.class public final Lcom/google/android/gms/drive/database/model/ar;
.super Lcom/google/android/gms/drive/database/model/ae;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/drive/database/model/ar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/gms/drive/database/model/ar;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/model/ar;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ar;->a:Lcom/google/android/gms/drive/database/model/ar;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 58
    const/16 v0, 0x2b

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ae;-><init>(I)V

    .line 60
    return-void
.end method

.method public static a()Lcom/google/android/gms/drive/database/model/ar;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/gms/drive/database/model/ar;->a:Lcom/google/android/gms/drive/database/model/ar;

    return-object v0
.end method

.method public static a(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 425
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->j:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d()Lcom/google/android/gms/drive/database/model/ar;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/gms/drive/database/model/ar;->a:Lcom/google/android/gms/drive/database/model/ar;

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string v0, "Entry"

    return-object v0
.end method

.method public final synthetic c()[Lcom/google/android/gms/drive/g/ak;
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/google/android/gms/drive/database/model/as;->values()[Lcom/google/android/gms/drive/database/model/as;

    move-result-object v0

    return-object v0
.end method
