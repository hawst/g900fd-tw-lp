.class public final enum Lcom/google/android/gms/drive/database/model/ca;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum f:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum g:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum h:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum i:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum j:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum k:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum l:Lcom/google/android/gms/drive/database/model/ca;

.field public static final enum m:Lcom/google/android/gms/drive/database/model/ca;

.field private static final synthetic o:[Lcom/google/android/gms/drive/database/model/ca;


# instance fields
.field private final n:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/16 v8, 0x19

    const/4 v7, 0x1

    .line 46
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "ENTRY_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "entryId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->a:Lcom/google/android/gms/drive/database/model/ca;

    .line 51
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "REQUEST_TIME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "requestTime"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->b:Lcom/google/android/gms/drive/database/model/ca;

    .line 55
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "IS_COMPLETED"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "isCompleted"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->c:Lcom/google/android/gms/drive/database/model/ca;

    .line 59
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "IS_PAUSED_MANUALLY"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "isPausedManually"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->d:Lcom/google/android/gms/drive/database/model/ca;

    .line 65
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "IS_UPLOAD_REQUESTED_EVER"

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "isUploadRequestedEver"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->e:Lcom/google/android/gms/drive/database/model/ca;

    .line 70
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "IS_IMPLICIT"

    const/4 v2, 0x5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "isImplicit"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->f:Lcom/google/android/gms/drive/database/model/ca;

    .line 85
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "BATCH_NUMBER"

    const/4 v2, 0x6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "batchNumber"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->g:Lcom/google/android/gms/drive/database/model/ca;

    .line 97
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "SYNC_DIRECTION_IN_BATCH"

    const/4 v2, 0x7

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "syncDirectionInBatch"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->h:Lcom/google/android/gms/drive/database/model/ca;

    .line 102
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "BYTES_TRANSFERRED"

    const/16 v2, 0x8

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "bytesTransferred"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->i:Lcom/google/android/gms/drive/database/model/ca;

    .line 108
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "ATTEMPT_COUNT"

    const/16 v2, 0x9

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "attemptCount"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->j:Lcom/google/android/gms/drive/database/model/ca;

    .line 118
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "UPLOAD_URI"

    const/16 v2, 0xa

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "uploadUri"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->k:Lcom/google/android/gms/drive/database/model/ca;

    .line 127
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "UPLOAD_SNAPSHOT_LAST_MODIFIED_TIME"

    const/16 v2, 0xb

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "uploadSnapshotLastModifiedTime"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->l:Lcom/google/android/gms/drive/database/model/ca;

    .line 136
    new-instance v0, Lcom/google/android/gms/drive/database/model/ca;

    const-string v1, "DOCUMENT_CONTENT_ID"

    const/16 v2, 0xc

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->d()Lcom/google/android/gms/drive/database/model/bz;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "documentContentId"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->a()Lcom/google/android/gms/drive/database/model/af;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ca;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->m:Lcom/google/android/gms/drive/database/model/ca;

    .line 45
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/ca;

    sget-object v1, Lcom/google/android/gms/drive/database/model/ca;->a:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/ca;->b:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/ca;->c:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/ca;->d:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/gms/drive/database/model/ca;->e:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/database/model/ca;->f:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/drive/database/model/ca;->g:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/gms/drive/database/model/ca;->h:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/gms/drive/database/model/ca;->i:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/gms/drive/database/model/ca;->j:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/gms/drive/database/model/ca;->k:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/gms/drive/database/model/ca;->l:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/gms/drive/database/model/ca;->m:Lcom/google/android/gms/drive/database/model/ca;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/ca;->o:[Lcom/google/android/gms/drive/database/model/ca;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 145
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ca;->n:Lcom/google/android/gms/drive/database/model/ab;

    .line 146
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ca;
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/google/android/gms/drive/database/model/ca;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ca;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/ca;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/gms/drive/database/model/ca;->o:[Lcom/google/android/gms/drive/database/model/ca;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/ca;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/ca;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ca;->n:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
