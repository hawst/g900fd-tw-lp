.class final Lcom/google/android/gms/wallet/common/ui/by;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/FormEditText;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V
    .locals 0

    .prologue
    .line 546
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/by;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/by;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    .line 551
    invoke-interface {v0, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    goto :goto_0

    .line 553
    :cond_0
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 557
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/by;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    .line 558
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    goto :goto_0

    .line 560
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/by;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/TextWatcher;

    .line 565
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    goto :goto_0

    .line 568
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/by;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 569
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/by;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->g()V

    .line 571
    :cond_1
    return-void
.end method
