.class final Lcom/google/android/gms/usagereporting/a/r;
.super Lcom/google/android/gms/usagereporting/a/p;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/a/p;-><init>(B)V

    .line 184
    iput-object p1, p0, Lcom/google/android/gms/usagereporting/a/r;->a:Lcom/google/android/gms/common/api/m;

    .line 185
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)V
    .locals 3

    .prologue
    .line 190
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/r;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/usagereporting/a/j;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/usagereporting/a/j;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 196
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/r;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/usagereporting/a/j;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, v2, p2}, Lcom/google/android/gms/usagereporting/a/j;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
