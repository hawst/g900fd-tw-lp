.class public final Lcom/google/android/gms/deviceconnection/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static d:Lcom/google/android/gms/deviceconnection/a/a;


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/HashSet;

.field private final g:Landroid/database/sqlite/SQLiteDatabase;

.field private final h:Lcom/google/android/gms/deviceconnection/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "feature_name=?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/deviceconnection/a/a;->a:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "last_connection_timestamp>=?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/deviceconnection/a/a;->b:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "last_connection_timestamp<?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/deviceconnection/a/a;->c:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/gms/deviceconnection/a/a;->e:Landroid/content/Context;

    .line 70
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/deviceconnection/a/a;->f:Ljava/util/HashSet;

    .line 71
    new-instance v0, Lcom/google/android/gms/deviceconnection/d/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/deviceconnection/d/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/deviceconnection/d/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/deviceconnection/a/a;->g:Landroid/database/sqlite/SQLiteDatabase;

    .line 73
    new-instance v0, Lcom/google/android/gms/deviceconnection/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/deviceconnection/a/b;-><init>(Lcom/google/android/gms/deviceconnection/a/a;)V

    iput-object v0, p0, Lcom/google/android/gms/deviceconnection/a/a;->h:Lcom/google/android/gms/deviceconnection/a/b;

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/a;->h:Lcom/google/android/gms/deviceconnection/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/deviceconnection/a/b;->run()V

    .line 78
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    new-instance v0, Lcom/google/android/gms/deviceconnection/a/c;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/deviceconnection/a/c;-><init>(Lcom/google/android/gms/deviceconnection/a/a;Landroid/content/Context;)V

    .line 81
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/deviceconnection/a/a;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/a;->g:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/deviceconnection/a/a;
    .locals 2

    .prologue
    .line 49
    const-class v1, Lcom/google/android/gms/deviceconnection/a/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/deviceconnection/a/a;->d:Lcom/google/android/gms/deviceconnection/a/a;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/google/android/gms/deviceconnection/a/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/deviceconnection/a/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/deviceconnection/a/a;->d:Lcom/google/android/gms/deviceconnection/a/a;

    .line 52
    :cond_0
    sget-object v0, Lcom/google/android/gms/deviceconnection/a/a;->d:Lcom/google/android/gms/deviceconnection/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/deviceconnection/a/a;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a([Ljava/lang/String;)I
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/deviceconnection/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x3

    .line 124
    :goto_0
    monitor-exit p0

    return v0

    .line 97
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move v3, v2

    move v0, v2

    .line 98
    :goto_1
    array-length v6, p1

    if-ge v3, v6, :cond_4

    .line 99
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 100
    const-string v7, "feature_name"

    aget-object v8, p1, v3

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v7, "last_connection_timestamp"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 102
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v9, p1, v3

    aput-object v9, v7, v8

    .line 105
    iget-object v8, p0, Lcom/google/android/gms/deviceconnection/a/a;->g:Landroid/database/sqlite/SQLiteDatabase;

    const-string v9, "device_features"

    sget-object v10, Lcom/google/android/gms/deviceconnection/a/a;->a:Ljava/lang/String;

    invoke-virtual {v8, v9, v6, v10, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 107
    if-nez v7, :cond_3

    .line 109
    iget-object v7, p0, Lcom/google/android/gms/deviceconnection/a/a;->g:Landroid/database/sqlite/SQLiteDatabase;

    const-string v8, "device_features"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-nez v7, :cond_2

    .line 111
    const-string v7, "DeviceConnectionAgent"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Insert failed for the following values: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 113
    goto :goto_2

    .line 115
    :cond_3
    if-le v7, v1, :cond_1

    .line 116
    const-string v7, "DeviceConnectionAgent"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Update affected multiple rows for the following values: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 121
    :cond_4
    if-eqz v0, :cond_5

    .line 122
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/a;->e:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.deviceconnection.device_feature_added"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_5
    move v0, v2

    .line 124
    goto/16 :goto_0
.end method

.method public final declared-synchronized a()Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/deviceconnection/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 134
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/a;->g:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "device_features"

    sget-object v2, Lcom/google/android/gms/deviceconnection/c/b;->a:[Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/deviceconnection/a/a;->b:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "last_connection_timestamp DESC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 138
    new-instance v1, Lcom/google/android/gms/common/data/DataHolder;

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
