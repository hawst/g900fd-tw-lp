.class public final Lcom/google/android/gms/fitness/wearables/a;
.super Lcom/google/android/gms/fitness/sync/e;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/v;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sync/e;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/gms/fitness/wearables/a;->a:Lcom/google/android/gms/common/api/v;

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/fitness/wearables/a;->b:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/google/android/gms/fitness/wearables/a;->c:Ljava/lang/String;

    .line 47
    return-void
.end method

.method private a(Ljava/lang/String;[B)Lcom/google/android/gms/common/api/am;
    .locals 5

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/fitness/wearables/a;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const-string v0, "Wearable API client is not connected. Cannot send sync message"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 53
    :cond_0
    sget-object v0, Lcom/google/android/gms/wearable/z;->b:Lcom/google/android/gms/wearable/o;

    iget-object v1, p0, Lcom/google/android/gms/fitness/wearables/a;->a:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/fitness/wearables/a;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/fitness/wearables/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/wearable/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/util/Collection;
    .locals 4

    .prologue
    .line 60
    const-string v0, "syncing %s points"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 62
    new-instance v1, Lcom/google/af/a/b/a/a/p;

    invoke-direct {v1}, Lcom/google/af/a/b/a/a/p;-><init>()V

    .line 64
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/af/a/b/a/a/o;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/af/a/b/a/a/o;

    iput-object v0, v1, Lcom/google/af/a/b/a/a/p;->a:[Lcom/google/af/a/b/a/a/o;

    .line 67
    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    .line 69
    const-string v1, "/WearablesSync/DataPoint/"

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/fitness/wearables/a;->a(Ljava/lang/String;[B)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    .line 71
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->O:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v2, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/q;

    invoke-interface {v0}, Lcom/google/android/gms/wearable/q;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p1

    .line 79
    :cond_0
    return-object p1
.end method

.method public final b(Ljava/util/List;)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "syncing: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 87
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 89
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 90
    invoke-static {v0}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)[B

    move-result-object v3

    .line 91
    const-string v4, "/WearablesSync/DataSource/"

    invoke-direct {p0, v4, v3}, Lcom/google/android/gms/fitness/wearables/a;->a(Ljava/lang/String;[B)Lcom/google/android/gms/common/api/am;

    move-result-object v3

    .line 92
    invoke-virtual {v1, v0, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 95
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 96
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 97
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/api/am;

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->O:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v6, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v6, v7, v2}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/q;

    invoke-interface {v1}, Lcom/google/android/gms/wearable/q;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 99
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v2

    if-nez v2, :cond_1

    .line 100
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "unable to sync: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data source: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 101
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 105
    :cond_2
    return-object v3
.end method
