.class public final Lcom/google/android/gms/googlehelp/c/i;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/c/e;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V
    .locals 5

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 53
    iget-object v0, p3, Lcom/google/ad/a/a/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/i;->setTag(Ljava/lang/Object;)V

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/i;->setOrientation(I)V

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p3, Lcom/google/ad/a/a/h;->e:[Lcom/google/ad/a/a/k;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/i;->a:Ljava/util/List;

    .line 57
    iget-object v1, p3, Lcom/google/ad/a/a/h;->e:[Lcom/google/ad/a/a/k;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 58
    new-instance v4, Lcom/google/android/gms/googlehelp/c/f;

    invoke-direct {v4, p1, p2, v3}, Lcom/google/android/gms/googlehelp/c/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/k;)V

    .line 59
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/c/i;->a:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    invoke-virtual {p2, v4}, Lcom/google/android/gms/googlehelp/c/d;->a(Lcom/google/android/gms/googlehelp/c/af;)V

    .line 61
    invoke-virtual {p0, v4}, Lcom/google/android/gms/googlehelp/c/i;->addView(Landroid/view/View;)V

    .line 63
    invoke-virtual {v4}, Lcom/google/android/gms/googlehelp/c/f;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    invoke-virtual {v4}, Lcom/google/android/gms/googlehelp/c/f;->d()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/googlehelp/c/i;->addView(Landroid/view/View;)V

    .line 57
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_1
    return-void
.end method

.method private a()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 103
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/i;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/f;

    .line 105
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/f;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    if-le v0, v3, :cond_2

    move v2, v3

    .line 110
    :cond_0
    return v2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    .line 108
    goto :goto_0
.end method


# virtual methods
.method public final c()Ljava/util/List;
    .locals 9

    .prologue
    .line 71
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/i;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 73
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/i;->a()Z

    move-result v6

    .line 74
    const/4 v1, 0x0

    .line 75
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/c/i;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v1

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/googlehelp/c/f;

    .line 76
    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/c/f;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 77
    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/c/f;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 78
    if-eqz v6, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "_"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-static {v3, v2}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/c/f;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 82
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "--"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/c/f;->d()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move v1, v4

    :goto_2
    move v3, v1

    .line 86
    goto :goto_0

    :cond_1
    move v4, v3

    move-object v3, v0

    .line 78
    goto :goto_1

    .line 88
    :cond_2
    return-object v5

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method public final d()Ljava/util/List;
    .locals 4

    .prologue
    .line 93
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/i;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/f;

    .line 95
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/f;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 96
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 99
    :cond_1
    return-object v1
.end method
