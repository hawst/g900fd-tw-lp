.class public final Lcom/google/android/gms/smart_profile/header/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/smart_profile/b/b;


# instance fields
.field public a:Z

.field public b:Lcom/google/ac/c/a/a/a/w;

.field public final c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

.field public final d:Lcom/google/android/gms/smart_profile/header/d;

.field public e:Lcom/google/android/gms/smart_profile/bg;

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>(Lcom/google/ac/c/a/a/a/w;Lcom/google/android/gms/smart_profile/header/view/HeaderView;Lcom/google/android/gms/smart_profile/header/d;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    .line 66
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    .line 67
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/header/b;->d:Lcom/google/android/gms/smart_profile/header/d;

    .line 68
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "r"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 166
    :goto_0
    return-object v0

    .line 163
    :catch_0
    move-exception v0

    .line 164
    const-string v1, "SmartProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Problem decoding bitmap from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SmartProfile"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 166
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 225
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/smart_profile/bg;->a(Landroid/graphics/Bitmap;)V

    .line 226
    return-void

    .line 219
    :cond_1
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->d:Lcom/google/android/gms/smart_profile/header/d;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->d:Lcom/google/android/gms/smart_profile/header/d;

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/header/d;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/au;)V
    .locals 6

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/people/identity/internal/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/header/b;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_0

    .line 142
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/smart_profile/header/b;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Landroid/graphics/Bitmap;)V

    .line 150
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    const/4 v0, 0x0

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/smart_profile/header/e;

    invoke-direct {v2, p0}, Lcom/google/android/gms/smart_profile/header/e;-><init>(Lcom/google/android/gms/smart_profile/header/b;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 155
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/header/b;->a:Z

    .line 156
    return-void

    .line 145
    :cond_2
    new-instance v0, Lcom/google/android/gms/smart_profile/b/a;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v4, v1, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/b;->a()Z

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/smart_profile/b/a;-><init>(Lcom/google/android/gms/smart_profile/b/b;Lcom/google/android/gms/common/api/v;Landroid/view/View;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/b/a;->a()V

    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/smart_profile/header/b;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->c:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/b;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/bg;->b()Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    .line 190
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/b;->b:Lcom/google/ac/c/a/a/a/w;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
