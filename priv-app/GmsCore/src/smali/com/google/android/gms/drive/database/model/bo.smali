.class public final enum Lcom/google/android/gms/drive/database/model/bo;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/bo;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/bo;

.field private static final synthetic d:[Lcom/google/android/gms/drive/database/model/bo;


# instance fields
.field private final c:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x1f

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 40
    new-instance v0, Lcom/google/android/gms/drive/database/model/bo;

    const-string v1, "PERSISTED_EVENT_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bn;->d()Lcom/google/android/gms/drive/database/model/bn;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "persistedEventId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bp;->a()Lcom/google/android/gms/drive/database/model/bp;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/bo;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bo;->a:Lcom/google/android/gms/drive/database/model/bo;

    .line 50
    new-instance v0, Lcom/google/android/gms/drive/database/model/bo;

    const-string v1, "CONTENT_HASH"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bn;->d()Lcom/google/android/gms/drive/database/model/bn;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "contentHash"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/database/model/bo;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bo;->b:Lcom/google/android/gms/drive/database/model/bo;

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/bo;

    sget-object v1, Lcom/google/android/gms/drive/database/model/bo;->a:Lcom/google/android/gms/drive/database/model/bo;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/bo;->b:Lcom/google/android/gms/drive/database/model/bo;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/drive/database/model/bo;->d:[Lcom/google/android/gms/drive/database/model/bo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bo;->c:Lcom/google/android/gms/drive/database/model/ab;

    .line 62
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/bo;
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/gms/drive/database/model/bo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bo;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/bo;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/gms/drive/database/model/bo;->d:[Lcom/google/android/gms/drive/database/model/bo;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/bo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/bo;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bo;->c:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bo;->c:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
