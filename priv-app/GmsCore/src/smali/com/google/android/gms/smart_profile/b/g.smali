.class public final Lcom/google/android/gms/smart_profile/b/g;
.super Landroid/support/v4/a/a;
.source "SourceFile"


# instance fields
.field private final f:Ljava/lang/String;

.field private g:Landroid/graphics/Bitmap;

.field private h:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/support/v4/a/a;-><init>(Landroid/content/Context;)V

    .line 33
    const-string v0, "URL must not be empty."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 34
    const-string v0, "GoogleApiClient must not be null."

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-interface {p3, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    const-string v1, "People API must be present."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/b/g;->f:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/b/g;->h:Lcom/google/android/gms/common/api/v;

    .line 39
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/b/g;->g:Landroid/graphics/Bitmap;

    .line 72
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 73
    invoke-super {p0, p1}, Landroid/support/v4/a/a;->b(Ljava/lang/Object;)V

    .line 75
    :cond_0
    return-void
.end method

.method private k()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/g;->f:Ljava/lang/String;

    const-string v2, "android.resource"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    :try_start_0
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/b/g;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .line 87
    :catch_0
    move-exception v0

    const-string v0, "SmartProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load image "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/b/g;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v0, v1

    .line 102
    goto :goto_0

    .line 90
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/b/g;->h:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/b/g;->f:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/p;

    .line 92
    invoke-interface {v0}, Lcom/google/android/gms/people/p;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/people/p;->c()Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 95
    :try_start_1
    invoke-interface {v0}, Lcom/google/android/gms/people/p;->c()Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 96
    invoke-static {v1}, Lcom/google/android/gms/people/af;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 98
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/b/g;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/b/g;->k()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Landroid/support/v4/a/a;->e()V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/g;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/g;->g:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/b/g;->a(Landroid/graphics/Bitmap;)V

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/g;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/g;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 50
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 52
    :cond_2
    return-void
.end method

.method protected final f()V
    .locals 0

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/g;->b()Z

    .line 58
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/support/v4/a/a;->g()V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/g;->b()Z

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/g;->g:Landroid/graphics/Bitmap;

    .line 67
    return-void
.end method
