.class public Lcom/google/android/gms/location/copresence/CopresenceSettings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/location/copresence/m;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:Z

.field private d:[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

.field private e:[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/location/copresence/m;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->CREATOR:Lcom/google/android/gms/location/copresence/m;

    return-void
.end method

.method constructor <init>(IZZ[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput p1, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a:I

    .line 85
    iput-boolean p2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b:Z

    .line 86
    iput-boolean p3, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c:Z

    .line 87
    iput-object p4, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d:[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    .line 88
    iput-object p5, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->e:[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    .line 89
    return-void
.end method

.method private constructor <init>(Z[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 93
    move-object v0, p0

    move v2, p1

    move v3, v1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/copresence/CopresenceSettings;-><init>(IZZ[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)V

    .line 94
    return-void
.end method

.method public static a(Z[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)Lcom/google/android/gms/location/copresence/CopresenceSettings;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 137
    new-instance v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/location/copresence/CopresenceSettings;-><init>(Z[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c:Z

    return v0
.end method

.method public final c()[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d:[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d:[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    invoke-virtual {v0}, [Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    goto :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 250
    new-instance v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    iget v1, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a:I

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b:Z

    iget-boolean v3, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c:Z

    iget-object v4, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d:[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    if-nez v4, :cond_0

    move-object v4, v5

    :goto_0
    iget-object v6, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->e:[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    if-nez v6, :cond_1

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/copresence/CopresenceSettings;-><init>(IZZ[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;)V

    return-object v0

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d:[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    invoke-virtual {v4}, [Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->e:[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    invoke-virtual {v5}, [Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    goto :goto_1
.end method

.method public final d()[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->e:[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->e:[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    invoke-virtual {v0}, [Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 239
    sget-object v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->CREATOR:Lcom/google/android/gms/location/copresence/m;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 263
    if-ne p0, p1, :cond_1

    .line 271
    :cond_0
    :goto_0
    return v0

    .line 266
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 267
    goto :goto_0

    .line 270
    :cond_3
    check-cast p1, Lcom/google/android/gms/location/copresence/CopresenceSettings;

    .line 271
    iget v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a:I

    iget v3, p1, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a:I

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c:Z

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d:[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d:[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->e:[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/CopresenceSettings;->e:[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 257
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->d:[Lcom/google/android/gms/location/copresence/internal/CopresenceNamedAcl;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->e:[Lcom/google/android/gms/location/copresence/internal/CopresenceFeatureOptIn;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->CREATOR:Lcom/google/android/gms/location/copresence/m;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/m;->a(Lcom/google/android/gms/location/copresence/CopresenceSettings;Landroid/os/Parcel;I)V

    .line 245
    return-void
.end method
