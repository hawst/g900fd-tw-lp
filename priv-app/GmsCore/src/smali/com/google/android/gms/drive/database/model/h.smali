.class public final enum Lcom/google/android/gms/drive/database/model/h;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/h;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/h;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/h;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/h;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/h;

.field private static final synthetic g:[Lcom/google/android/gms/drive/database/model/h;


# instance fields
.field private final f:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 47
    new-instance v0, Lcom/google/android/gms/drive/database/model/h;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->d()Lcom/google/android/gms/drive/database/model/g;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "accountId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/h;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/h;->a:Lcom/google/android/gms/drive/database/model/h;

    .line 58
    new-instance v0, Lcom/google/android/gms/drive/database/model/h;

    const-string v1, "PACKAGING_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->d()Lcom/google/android/gms/drive/database/model/g;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "packagingId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/database/model/h;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/h;->b:Lcom/google/android/gms/drive/database/model/h;

    .line 67
    new-instance v0, Lcom/google/android/gms/drive/database/model/h;

    const-string v1, "EXPIRY_TIMESTAMP"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->d()Lcom/google/android/gms/drive/database/model/g;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "expiryTimestamp"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/model/h;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/h;->c:Lcom/google/android/gms/drive/database/model/h;

    .line 76
    new-instance v0, Lcom/google/android/gms/drive/database/model/h;

    const-string v1, "PACKAGE_NAME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->d()Lcom/google/android/gms/drive/database/model/g;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "packageName"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/h;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/h;->d:Lcom/google/android/gms/drive/database/model/h;

    .line 85
    new-instance v0, Lcom/google/android/gms/drive/database/model/h;

    const-string v1, "CERTIFICATE_HASH"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->d()Lcom/google/android/gms/drive/database/model/g;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "certificateHash"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/drive/database/model/h;->a:Lcom/google/android/gms/drive/database/model/h;

    iget-object v5, v5, Lcom/google/android/gms/drive/database/model/h;->f:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    sget-object v5, Lcom/google/android/gms/drive/database/model/h;->d:Lcom/google/android/gms/drive/database/model/h;

    iget-object v5, v5, Lcom/google/android/gms/drive/database/model/h;->f:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/h;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/h;->e:Lcom/google/android/gms/drive/database/model/h;

    .line 46
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/h;

    sget-object v1, Lcom/google/android/gms/drive/database/model/h;->a:Lcom/google/android/gms/drive/database/model/h;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/h;->b:Lcom/google/android/gms/drive/database/model/h;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/drive/database/model/h;->c:Lcom/google/android/gms/drive/database/model/h;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/h;->d:Lcom/google/android/gms/drive/database/model/h;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/h;->e:Lcom/google/android/gms/drive/database/model/h;

    aput-object v1, v0, v10

    sput-object v0, Lcom/google/android/gms/drive/database/model/h;->g:[Lcom/google/android/gms/drive/database/model/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/h;->f:Lcom/google/android/gms/drive/database/model/ab;

    .line 96
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/h;
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/gms/drive/database/model/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/h;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/gms/drive/database/model/h;->g:[Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/h;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/h;->f:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/h;->f:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
