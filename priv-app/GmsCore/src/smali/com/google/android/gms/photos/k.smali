.class final Lcom/google/android/gms/photos/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/PhotosListPreference;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/PhotosListPreference;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x1

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v0}, Lcom/google/android/gms/photos/PhotosListPreference;->a(Lcom/google/android/gms/photos/PhotosListPreference;)[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v0}, Lcom/google/android/gms/photos/PhotosListPreference;->b(Lcom/google/android/gms/photos/PhotosListPreference;)[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 251
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 265
    if-nez p2, :cond_0

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v0}, Lcom/google/android/gms/photos/PhotosListPreference;->c(Lcom/google/android/gms/photos/PhotosListPreference;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/google/android/gms/l;->fp:I

    invoke-virtual {v0, v3, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 271
    :cond_0
    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 272
    iget-object v3, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v3}, Lcom/google/android/gms/photos/PhotosListPreference;->a(Lcom/google/android/gms/photos/PhotosListPreference;)[Ljava/lang/CharSequence;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    const v0, 0x1020015

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 274
    iget-object v3, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v3}, Lcom/google/android/gms/photos/PhotosListPreference;->d(Lcom/google/android/gms/photos/PhotosListPreference;)Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 275
    iget-object v3, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v3}, Lcom/google/android/gms/photos/PhotosListPreference;->e(Lcom/google/android/gms/photos/PhotosListPreference;)[Ljava/lang/CharSequence;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v5}, Lcom/google/android/gms/photos/PhotosListPreference;->d(Lcom/google/android/gms/photos/PhotosListPreference;)Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    :goto_0
    sget v0, Lcom/google/android/gms/j;->ql:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 282
    iget-object v3, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v3}, Lcom/google/android/gms/photos/PhotosListPreference;->f(Lcom/google/android/gms/photos/PhotosListPreference;)I

    move-result v3

    if-ne p1, v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 284
    return-object p2

    .line 278
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v3}, Lcom/google/android/gms/photos/PhotosListPreference;->e(Lcom/google/android/gms/photos/PhotosListPreference;)[Ljava/lang/CharSequence;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 282
    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x1

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/photos/k;->a:Lcom/google/android/gms/photos/PhotosListPreference;

    invoke-static {v0}, Lcom/google/android/gms/photos/PhotosListPreference;->a(Lcom/google/android/gms/photos/PhotosListPreference;)[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x1

    return v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 237
    return-void
.end method
