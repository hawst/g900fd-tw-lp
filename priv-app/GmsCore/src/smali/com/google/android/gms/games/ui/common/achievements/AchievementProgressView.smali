.class public Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;
.super Landroid/widget/TextView;
.source "SourceFile"


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Rect;

.field private final d:Landroid/graphics/RectF;

.field private e:F

.field private f:F

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->c:Landroid/graphics/Rect;

    .line 30
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->d:Landroid/graphics/RectF;

    .line 41
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a()V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->c:Landroid/graphics/Rect;

    .line 30
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->d:Landroid/graphics/RectF;

    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a()V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->c:Landroid/graphics/Rect;

    .line 30
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->d:Landroid/graphics/RectF;

    .line 51
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a()V

    .line 52
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->B:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->e:F

    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->A:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->f:F

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->g:I

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->V:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->h:I

    .line 70
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->g:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->e:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->b:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->b:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->f:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->b:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 71
    return-void
.end method


# virtual methods
.method public final a(II)Ljava/lang/String;
    .locals 5

    .prologue
    .line 93
    iput p1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->i:I

    .line 94
    iput p2, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->j:I

    .line 98
    iget v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->i:I

    mul-int/lit8 v0, v0, 0x64

    iget v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->j:I

    div-int/2addr v0, v1

    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->ke:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    const/high16 v6, 0x40000000    # 2.0f

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->c:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->d:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 122
    iget v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->e:F

    div-float/2addr v0, v6

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->d:Landroid/graphics/RectF;

    invoke-virtual {v1, v0, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->d:Landroid/graphics/RectF;

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 130
    iget v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->e:F

    div-float/2addr v0, v6

    iget v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->f:F

    div-float/2addr v1, v6

    add-float/2addr v0, v1

    .line 131
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->d:Landroid/graphics/RectF;

    invoke-virtual {v1, v0, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 134
    iget v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->i:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    iget v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->j:I

    int-to-float v1, v1

    div-float v3, v0, v1

    .line 135
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->d:Landroid/graphics/RectF;

    const/high16 v2, 0x43870000    # 270.0f

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementProgressView;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 138
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 139
    return-void
.end method
