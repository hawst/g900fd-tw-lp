.class public final Lcom/google/android/gms/wallet/dynamite/image/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Lcom/google/android/gms/wallet/dynamite/image/a;


# instance fields
.field final a:Lcom/google/android/gms/common/internal/a/b;


# direct methods
.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/image/b;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/wallet/dynamite/image/b;-><init>(Lcom/google/android/gms/wallet/dynamite/image/a;I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/a;->a:Lcom/google/android/gms/common/internal/a/b;

    .line 56
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/wallet/dynamite/image/a;
    .locals 2

    .prologue
    .line 32
    const-string v0, "context is null"

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/image/a;->b:Lcom/google/android/gms/wallet/dynamite/image/a;

    if-nez v0, :cond_1

    .line 34
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_0

    const/high16 v0, 0x200000

    :cond_0
    new-instance v1, Lcom/google/android/gms/wallet/dynamite/image/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/dynamite/image/a;-><init>(I)V

    sput-object v1, Lcom/google/android/gms/wallet/dynamite/image/a;->b:Lcom/google/android/gms/wallet/dynamite/image/a;

    .line 36
    :cond_1
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/image/a;->b:Lcom/google/android/gms/wallet/dynamite/image/a;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/a;->a:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method
