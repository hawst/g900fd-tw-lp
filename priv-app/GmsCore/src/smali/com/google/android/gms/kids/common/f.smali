.class public final Lcom/google/android/gms/kids/common/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Z

.field private static final b:Lcom/google/android/gms/common/internal/az;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    const-string v0, "6.7.77 (1747363-000)"

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/kids/common/f;->a:Z

    .line 31
    new-instance v0, Lcom/google/android/gms/common/internal/az;

    const-string v1, "Kids"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/az;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 47
    invoke-static {}, Lcom/google/android/gms/kids/common/f;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const-string v0, "Kids"

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/kids/common/f;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :cond_0
    return-void
.end method

.method public static varargs a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 54
    invoke-static {}, Lcom/google/android/gms/kids/common/f;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "Kids"

    invoke-static {p0, p2, p3}, Lcom/google/android/gms/kids/common/f;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 58
    :cond_0
    return-void
.end method

.method private static a()Z
    .locals 2

    .prologue
    .line 43
    sget-boolean v0, Lcom/google/android/gms/kids/common/f;->a:Z

    if-nez v0, :cond_0

    const-string v0, "Kids"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const-string v1, "Kids"

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/kids/common/f;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/az;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    return-void
.end method

.method public static varargs b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const-string v1, "Kids"

    invoke-static {p0, p2, p3}, Lcom/google/android/gms/kids/common/f;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/gms/common/internal/az;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 84
    :cond_0
    return-void
.end method

.method public static varargs c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const-string v1, "Kids"

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/kids/common/f;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/az;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    return-void
.end method

.method public static varargs c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 93
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const-string v1, "Kids"

    invoke-static {p0, p2, p3}, Lcom/google/android/gms/kids/common/f;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/gms/common/internal/az;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96
    :cond_0
    return-void
.end method

.method public static varargs d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 99
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const-string v1, "Kids"

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/kids/common/f;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/az;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_0
    return-void
.end method

.method public static varargs d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 105
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    const-string v1, "Kids"

    invoke-static {p0, p2, p3}, Lcom/google/android/gms/kids/common/f;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/gms/common/internal/az;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 108
    :cond_0
    return-void
.end method

.method public static varargs e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 111
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    sget-object v0, Lcom/google/android/gms/kids/common/f;->b:Lcom/google/android/gms/common/internal/az;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/kids/common/f;->f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    .line 114
    :cond_0
    return-void
.end method

.method private static varargs f(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 34
    array-length v0, p2

    if-nez v0, :cond_1

    move-object v0, p1

    .line 35
    :goto_0
    if-eqz p0, :cond_0

    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 38
    :cond_0
    return-object v0

    .line 34
    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
