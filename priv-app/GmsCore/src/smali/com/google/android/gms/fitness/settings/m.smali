.class public final Lcom/google/android/gms/fitness/settings/m;
.super Lcom/google/android/gms/fitness/settings/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/fitness/settings/a;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/settings/n;)Lcom/google/android/gms/fitness/settings/o;
    .locals 5

    .prologue
    .line 34
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 35
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 37
    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "www.googleapis.com"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "/oauth2/v3/RevokeToken"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "revocation_handle"

    iget-object v4, p0, Lcom/google/android/gms/fitness/settings/n;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 41
    const-string v1, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OAuth "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/fitness/settings/n;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    const/4 v1, 0x0

    .line 45
    :try_start_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/settings/m;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 46
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 47
    new-instance v2, Lcom/google/android/gms/fitness/settings/o;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/settings/o;-><init>()V

    .line 48
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v3, 0xc8

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v2, Lcom/google/android/gms/fitness/settings/o;->c:Z
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    return-object v2

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    :try_start_1
    new-instance v2, Lcom/google/android/gms/fitness/settings/d;

    invoke-direct {v2, v0}, Lcom/google/android/gms/fitness/settings/d;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 56
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v0

    .line 52
    :catch_1
    move-exception v0

    .line 53
    :try_start_2
    new-instance v2, Lcom/google/android/gms/fitness/settings/d;

    invoke-direct {v2, v0}, Lcom/google/android/gms/fitness/settings/d;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method


# virtual methods
.method protected final synthetic b(Lcom/google/android/gms/fitness/settings/b;)Lcom/google/android/gms/fitness/settings/c;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/gms/fitness/settings/n;

    invoke-static {p1}, Lcom/google/android/gms/fitness/settings/m;->a(Lcom/google/android/gms/fitness/settings/n;)Lcom/google/android/gms/fitness/settings/o;

    move-result-object v0

    return-object v0
.end method
