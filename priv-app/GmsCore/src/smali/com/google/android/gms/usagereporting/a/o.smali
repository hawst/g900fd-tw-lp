.class public final Lcom/google/android/gms/usagereporting/a/o;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V
    .locals 6

    .prologue
    .line 104
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 98
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/usagereporting/a/o;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 105
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 32
    invoke-static {p1}, Lcom/google/android/gms/usagereporting/a/h;->a(Landroid/os/IBinder;)Lcom/google/android/gms/usagereporting/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/aj;Lcom/google/android/gms/common/api/m;)V
    .locals 4

    .prologue
    .line 121
    if-nez p1, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    .line 124
    :goto_0
    new-instance v2, Lcom/google/android/gms/usagereporting/a/t;

    invoke-virtual {p0}, Lcom/google/android/gms/usagereporting/a/o;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/usagereporting/a/g;

    iget-object v3, p0, Lcom/google/android/gms/usagereporting/a/o;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2, v0, p2, v3, v1}, Lcom/google/android/gms/usagereporting/a/t;-><init>(Lcom/google/android/gms/usagereporting/a/g;Lcom/google/android/gms/common/api/m;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/gms/usagereporting/a/u;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/o;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/usagereporting/a/u;

    .line 128
    if-eqz v0, :cond_1

    .line 130
    invoke-virtual {p0}, Lcom/google/android/gms/usagereporting/a/o;->k()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/usagereporting/a/g;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/usagereporting/a/g;->b(Lcom/google/android/gms/usagereporting/a/d;Lcom/google/android/gms/usagereporting/a/a;)V

    .line 139
    :goto_1
    return-void

    .line 121
    :cond_0
    new-instance v0, Lcom/google/android/gms/usagereporting/a/u;

    invoke-direct {v0, p1}, Lcom/google/android/gms/usagereporting/a/u;-><init>(Lcom/google/android/gms/common/api/aj;)V

    move-object v1, v0

    goto :goto_0

    .line 133
    :cond_1
    if-nez v1, :cond_2

    .line 134
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {p2, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 137
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/o;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/usagereporting/a/o;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/usagereporting/a/g;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/usagereporting/a/g;->a(Lcom/google/android/gms/usagereporting/a/d;Lcom/google/android/gms/usagereporting/a/a;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/m;)V
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/usagereporting/a/o;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/usagereporting/a/g;

    new-instance v1, Lcom/google/android/gms/usagereporting/a/r;

    invoke-direct {v1, p1}, Lcom/google/android/gms/usagereporting/a/r;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/usagereporting/a/g;->a(Lcom/google/android/gms/usagereporting/a/a;)V

    .line 110
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 173
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 174
    const v1, 0x6768a8

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/bj;->t(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 178
    return-void
.end method

.method public final a(Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;Lcom/google/android/gms/common/api/m;)V
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/usagereporting/a/o;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/usagereporting/a/g;

    new-instance v1, Lcom/google/android/gms/usagereporting/a/s;

    invoke-direct {v1, p2}, Lcom/google/android/gms/usagereporting/a/s;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/usagereporting/a/g;->a(Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;Lcom/google/android/gms/usagereporting/a/a;)V

    .line 116
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    const-string v0, "com.google.android.gms.usagereporting.service.START"

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/a/o;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/usagereporting/a/u;

    .line 145
    if-eqz v0, :cond_0

    .line 146
    new-instance v2, Lcom/google/android/gms/usagereporting/a/q;

    const/4 v1, 0x0

    invoke-direct {v2, v1}, Lcom/google/android/gms/usagereporting/a/q;-><init>(B)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/usagereporting/a/o;->k()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/usagereporting/a/g;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/usagereporting/a/g;->b(Lcom/google/android/gms/usagereporting/a/d;Lcom/google/android/gms/usagereporting/a/a;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->b()V

    .line 153
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 150
    const-string v1, "UsageReportingClientImpl"

    const-string v2, "disconnect(): Could not unregister listener from remote:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    const-string v0, "com.google.android.gms.usagereporting.internal.IUsageReportingService"

    return-object v0
.end method
