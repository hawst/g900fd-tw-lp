.class final Lcom/google/android/gms/plus/service/a;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/plus/service/a;


# instance fields
.field private final b:Lcom/google/android/gms/plus/b/b;

.field private final c:Landroid/content/pm/PackageManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;Landroid/content/pm/PackageManager;)V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 107
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a;->b:Lcom/google/android/gms/plus/b/b;

    .line 108
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a;->c:Landroid/content/pm/PackageManager;

    .line 109
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/plus/service/a;
    .locals 3

    .prologue
    .line 112
    sget-object v0, Lcom/google/android/gms/plus/service/a;->a:Lcom/google/android/gms/plus/service/a;

    if-nez v0, :cond_0

    .line 113
    new-instance v0, Lcom/google/android/gms/plus/service/a;

    invoke-static {p0}, Lcom/google/android/gms/plus/b/b;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/b/b;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/plus/service/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;Landroid/content/pm/PackageManager;)V

    sput-object v0, Lcom/google/android/gms/plus/service/a;->a:Lcom/google/android/gms/plus/service/a;

    .line 118
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/service/a;->a:Lcom/google/android/gms/plus/service/a;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 388
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;-><init>()V

    .line 389
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 391
    :try_start_0
    new-instance v2, Lcom/google/android/gms/common/server/response/d;

    invoke-direct {v2}, Lcom/google/android/gms/common/server/response/d;-><init>()V

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/common/server/response/d;->a(Ljava/io/InputStream;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_0
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 406
    :cond_0
    :goto_0
    return-object v0

    .line 400
    :catch_0
    move-exception v1

    .line 401
    const-string v2, "OASyncAdapter"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 402
    const-string v2, "OASyncAdapter"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 392
    :catch_1
    move-exception v0

    .line 393
    :try_start_2
    const-string v2, "OASyncAdapter"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 394
    const-string v2, "OASyncAdapter"

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/m;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 396
    :cond_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 404
    :cond_2
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 400
    :catch_2
    move-exception v0

    .line 401
    const-string v1, "OASyncAdapter"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 402
    const-string v1, "OASyncAdapter"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 398
    :catchall_0
    move-exception v0

    .line 399
    :try_start_4
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 404
    :cond_3
    :goto_2
    throw v0

    .line 400
    :catch_3
    move-exception v1

    .line 401
    const-string v2, "OASyncAdapter"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 402
    const-string v2, "OASyncAdapter"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)Ljava/util/HashMap;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 355
    sget-object v1, Lcom/google/android/gms/plus/internal/v;->a:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/plus/service/b;->a:[Ljava/lang/String;

    const-string v3, "accountName=? AND type=?"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object p1, v4, v6

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const-string v5, "onBehalfOf, timestamp"

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 363
    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 365
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 366
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/service/a;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;

    move-result-object v3

    .line 368
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 369
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/e;

    .line 370
    if-eqz v3, :cond_2

    .line 371
    if-nez v0, :cond_0

    .line 372
    new-instance v0, Lcom/google/android/gms/plus/service/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/service/e;-><init>(Lcom/google/android/gms/plus/service/a;)V

    .line 373
    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;->d()J

    move-result-wide v4

    iget-wide v6, v0, Lcom/google/android/gms/plus/service/e;->a:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;->d()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/gms/plus/service/e;->a:J

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/plus/service/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 382
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 377
    :cond_2
    :try_start_1
    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v0, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v0, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 382
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v2
.end method

.method private a(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 12

    .prologue
    .line 228
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb800

    sub-long/2addr v0, v2

    sget-object v2, Lcom/google/android/gms/plus/internal/v;->a:Landroid/net/Uri;

    const-string v3, "timestamp<?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {p2, v2, v3, v4}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 229
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/gms/plus/service/a;->a(Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)Ljava/util/HashMap;

    move-result-object v8

    .line 232
    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 233
    const-string v0, "OASyncAdapter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    const-string v0, "OASyncAdapter"

    const-string v1, "No logs to transmit at this time"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :goto_0
    return-void

    .line 239
    :cond_0
    const-wide/16 v2, 0x0

    .line 240
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/a;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v0, v4, v5, v6}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    const-string v0, "https://www.googleapis.com/auth/plus.me"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 243
    const-string v0, "https://www.googleapis.com/auth/plus.pages.manage"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 244
    const-string v0, "application_name"

    const-string v4, "80"

    invoke-virtual {v1, v0, v4}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 246
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 247
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 248
    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/e;

    .line 249
    iget-wide v6, v0, Lcom/google/android/gms/plus/service/e;->a:J

    cmp-long v4, v6, v2

    if-lez v4, :cond_5

    .line 250
    iget-wide v6, v0, Lcom/google/android/gms/plus/service/e;->a:J

    .line 252
    :goto_2
    iget-object v2, v0, Lcom/google/android/gms/plus/service/e;->b:Ljava/util/ArrayList;

    .line 254
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a;->b:Lcom/google/android/gms/plus/b/b;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/a;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/a;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v10, Lcom/google/android/gms/e;->d:I

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    iget-object v0, v0, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/b/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;IZLjava/lang/String;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_2

    move-wide v2, v6

    .line 282
    goto :goto_1

    .line 257
    :catch_0
    move-exception v0

    .line 258
    iget-object v2, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v2, :cond_1

    .line 259
    iget-object v2, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v4, v10

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    .line 260
    const-string v2, "OASyncAdapter"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 261
    const-string v2, "OASyncAdapter"

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    move-wide v2, v6

    .line 282
    goto :goto_1

    .line 269
    :catch_1
    move-exception v0

    const-string v0, "OASyncAdapter"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 270
    const-string v0, "OASyncAdapter"

    const-string v2, "Failed to upload logs due to transient authentication problem."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-wide v2, v6

    .line 282
    goto :goto_1

    .line 276
    :catch_2
    move-exception v0

    const-string v0, "OASyncAdapter"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 277
    const-string v0, "OASyncAdapter"

    const-string v2, "Failed to upload logs due to fatal authentication problem."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-wide v2, v6

    .line 283
    goto/16 :goto_1

    .line 287
    :cond_4
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/plus/internal/v;->a:Landroid/net/Uri;

    const-string v4, "accountName=? AND type=? AND timestamp<=?"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-virtual {p2, v1, v4, v5}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    move-wide v6, v2

    goto/16 :goto_2
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 14

    .prologue
    .line 124
    const-string v2, "com.google.android.gms.plus.action"

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 137
    :goto_0
    return-void

    .line 130
    :cond_0
    const/4 v8, 0x0

    :try_start_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget-object v3, Lcom/google/android/gms/plus/internal/w;->a:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/gms/plus/service/d;->a:[Ljava/lang/String;

    const-string v5, "accountName=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v7, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v7, v6, v2

    const-string v7, "_id"

    move-object/from16 v2, p4

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    :goto_1
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lcom/google/android/gms/plus/service/c;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/google/android/gms/plus/service/c;-><init>(B)V

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/gms/plus/service/c;->a:J

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/gms/plus/service/c;->b:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/gms/plus/service/c;->c:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/gms/plus/service/c;->d:Ljava/lang/String;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    :goto_2
    if-eqz v3, :cond_1

    :try_start_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 132
    :catch_0
    move-exception v2

    .line 134
    const-string v3, "OASyncAdapter"

    const-string v4, "Sync Failed"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 135
    const/4 v2, 0x1

    move-object/from16 v0, p5

    iput-boolean v2, v0, Landroid/content/SyncResult;->databaseError:Z

    goto :goto_0

    .line 130
    :cond_2
    if-eqz v3, :cond_3

    :try_start_4
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v2

    :goto_3
    if-ge v4, v6, :cond_5

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/c;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    :try_start_5
    iget-object v3, p0, Lcom/google/android/gms/plus/service/a;->c:Landroid/content/pm/PackageManager;

    iget-object v7, v2, Lcom/google/android/gms/plus/service/c;->d:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v3

    :try_start_6
    new-instance v7, Lcom/google/android/gms/common/server/ClientContext;

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v8, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v10, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v11, v2, Lcom/google/android/gms/plus/service/c;->d:Ljava/lang/String;

    invoke-direct {v7, v3, v8, v10, v11}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "https://www.googleapis.com/auth/plus.login"

    invoke-virtual {v7, v3}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    const-string v3, "application_name"

    sget-object v8, Lcom/google/android/gms/common/analytics/a;->c:Ljava/lang/String;

    invoke-virtual {v7, v3, v8}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0

    :try_start_7
    iget-object v3, p0, Lcom/google/android/gms/plus/service/a;->b:Lcom/google/android/gms/plus/b/b;

    iget-object v8, v2, Lcom/google/android/gms/plus/service/c;->b:Ljava/lang/String;

    iget-object v10, v2, Lcom/google/android/gms/plus/service/c;->c:Ljava/lang/String;
    :try_end_7
    .catch Lcom/android/volley/ac; {:try_start_7 .. :try_end_7} :catch_3
    .catch Lcom/google/android/gms/auth/q; {:try_start_7 .. :try_end_7} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0

    :try_start_8
    iget-object v3, v3, Lcom/google/android/gms/plus/b/b;->d:Lcom/google/android/gms/plus/b/c;

    invoke-virtual {v3, v7, v8, v10}, Lcom/google/android/gms/plus/b/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/android/volley/ac; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_8 .. :try_end_8} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_0

    :cond_4
    :goto_4
    :try_start_9
    sget-object v3, Lcom/google/android/gms/plus/internal/w;->a:Landroid/net/Uri;

    iget-wide v10, v2, Lcom/google/android/gms/plus/service/c;->a:J

    invoke-static {v3, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_5
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    :catch_1
    move-exception v3

    sget-object v3, Lcom/google/android/gms/plus/internal/w;->a:Landroid/net/Uri;

    iget-wide v10, v2, Lcom/google/android/gms/plus/service/c;->a:J

    invoke-static {v3, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_0

    goto :goto_5

    :catch_2
    move-exception v3

    :try_start_a
    const-string v7, "writeMoment"

    invoke-static {v3, v7}, Lcom/google/android/gms/plus/b/b;->a(Lcom/android/volley/ac;Ljava/lang/String;)V

    throw v3
    :try_end_a
    .catch Lcom/android/volley/ac; {:try_start_a .. :try_end_a} :catch_3
    .catch Lcom/google/android/gms/auth/q; {:try_start_a .. :try_end_a} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_0

    :catch_3
    move-exception v3

    :try_start_b
    iget-object v7, v3, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v7, :cond_5

    move-object/from16 v0, p5

    iget-object v7, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v7, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v7, Landroid/content/SyncStats;->numIoExceptions:J

    const-string v7, "OASyncAdapter"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "OASyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "Failed to upload moment: "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v2, Lcom/google/android/gms/plus/service/c;->c:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :catch_4
    move-exception v3

    const-string v3, "OASyncAdapter"

    const/4 v7, 0x3

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "OASyncAdapter"

    const-string v7, "Failed to upload moment due to fatal authentication problem."

    invoke-static {v3, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_0

    move-result v2

    if-lez v2, :cond_6

    :try_start_c
    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/content/ContentProviderClient;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_c
    .catch Landroid/content/OperationApplicationException; {:try_start_c .. :try_end_c} :catch_5
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_0

    .line 131
    :cond_6
    :goto_6
    :try_start_d
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/plus/service/a;->a(Landroid/accounts/Account;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V

    goto/16 :goto_0

    .line 130
    :catch_5
    move-exception v2

    const-string v3, "OASyncAdapter"

    const-string v4, "Failed to delete"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v2, 0x1

    move-object/from16 v0, p5

    iput-boolean v2, v0, Landroid/content/SyncResult;->databaseError:Z
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_0

    goto :goto_6

    :catchall_1
    move-exception v2

    move-object v3, v8

    goto/16 :goto_2
.end method
