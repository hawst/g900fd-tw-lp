.class final Lcom/google/android/gms/wallet/cache/d;
.super Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/cache/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/cache/c;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    .line 83
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;-><init>(Landroid/content/Context;)V

    .line 84
    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x0

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v2}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Fetching auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/d;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v2}, Lcom/google/android/gms/wallet/cache/c;->b(Lcom/google/android/gms/wallet/cache/c;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v3}, Lcom/google/android/gms/wallet/cache/c;->c(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_2

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v0}, Lcom/google/android/gms/wallet/cache/c;->d(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 108
    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v2}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "GoogleAuthUtil returned a null token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;I)V

    .line 120
    :goto_0
    return-object v7

    .line 95
    :catch_0
    move-exception v0

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v2}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "UserRecoverableAuthException when getting auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;I)V

    goto :goto_0

    .line 99
    :catch_1
    move-exception v0

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v2}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "IOException when getting auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;I)V

    goto :goto_0

    .line 103
    :catch_2
    move-exception v0

    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v2}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "GoogleAuthException when getting auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;I)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113
    const-string v0, "GetWalletProfileTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v2}, Lcom/google/android/gms/wallet/cache/c;->a(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Successfully received auth token"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v0}, Lcom/google/android/gms/wallet/cache/c;->f(Lcom/google/android/gms/wallet/cache/c;)Lcom/android/volley/s;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/wallet/cache/l;

    iget-object v2, p0, Lcom/google/android/gms/wallet/cache/d;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v3}, Lcom/google/android/gms/wallet/cache/c;->e(Lcom/google/android/gms/wallet/cache/c;)I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/wallet/cache/c;->a(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/wallet/a/b;

    iget-object v5, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v5}, Lcom/google/android/gms/wallet/cache/c;->c(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-static {v6}, Lcom/google/android/gms/wallet/cache/c;->d(Lcom/google/android/gms/wallet/cache/c;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/wallet/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/wallet/cache/d;->a:Lcom/google/android/gms/wallet/cache/c;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/wallet/cache/l;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/gms/wallet/cache/d;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
