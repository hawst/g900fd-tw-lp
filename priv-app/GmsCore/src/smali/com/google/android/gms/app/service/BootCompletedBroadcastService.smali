.class public Lcom/google/android/gms/app/service/BootCompletedBroadcastService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "BootCompletedBroadcastService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 45
    const-string v1, "broadcastIntent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 46
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 47
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 55
    const-string v0, "broadcastIntent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 58
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    invoke-static {p0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/ComponentName;)V

    .line 64
    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 65
    invoke-static {p0, v1}, Lcom/google/android/gms/gcm/ServiceAutoStarter;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 68
    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 69
    const-class v2, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 70
    invoke-virtual {p0, v1}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 73
    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 74
    const-class v2, Lcom/google/android/gms/auth/authzen/GcmReceiverService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 75
    invoke-virtual {p0, v1}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 78
    invoke-static {}, Lcom/google/android/gms/ads/settings/b/d;->a()V

    invoke-static {}, Lcom/google/android/gms/ads/settings/b/a;->a()V

    .line 81
    invoke-static {p0}, Lcom/google/android/gms/fitness/g;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 85
    const-string v2, "com.google.android.gms.fitness.service.BrokeredFitnessService"

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0, v1}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 91
    :cond_1
    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 92
    invoke-static {p0, v1}, Lcom/google/android/gms/icing/service/SystemEventReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 95
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    const-string v2, "fromDeviceBoot"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    invoke-virtual {p0, v1}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 100
    invoke-static {p0}, Lcom/google/android/gms/deviceconnection/a;->a(Landroid/content/Context;)V

    .line 103
    invoke-static {p0, v0}, Lcom/google/android/location/reporting/service/ac;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 106
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->e:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/mdm/e/a;->f:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-static {p0, v0, v1}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_3
    invoke-static {p0}, Lcom/google/android/gms/mdm/receivers/RetryAfterAlarmReceiver;->a(Landroid/content/Context;)V

    .line 110
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 111
    const-string v1, "com.google.android.gms.lockbox.LockboxAlarmReceiver"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->sendBroadcast(Landroid/content/Intent;)V

    .line 116
    invoke-static {p0}, Lcom/google/android/gms/common/download/DownloadAlarmReceiver;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->sendBroadcast(Landroid/content/Intent;)V

    .line 119
    sget-object v0, Lcom/google/android/gms/security/snet/SnetReceiver;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 120
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 121
    const-string v1, "com.google.android.gms.security.snet.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const-class v1, Lcom/google/android/gms/security/snet/SnetReceiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 123
    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->sendBroadcast(Landroid/content/Intent;)V

    .line 127
    :cond_4
    invoke-static {p0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->f(Landroid/content/Context;)V

    .line 130
    invoke-static {}, Lcom/google/android/gms/playlog/store/VacuumService;->b()V

    .line 131
    return-void
.end method
