.class public final Lcom/google/android/gms/plus/e/b;
.super Lcom/google/android/gms/plus/e/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/plus/e/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 11

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/e/b;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v6

    .line 89
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/e/b;->d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v7

    .line 90
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v4

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/plus/e/b;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v6}, Lcom/google/android/gms/plus/e/b;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v9

    .line 92
    iget-object v10, p0, Lcom/google/android/gms/plus/e/b;->d:Lcom/android/volley/s;

    new-instance v0, Lcom/google/android/gms/plus/e/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/e/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v8, p0, Lcom/google/android/gms/plus/e/b;->b:Z

    move-object v2, p3

    move-object v3, p4

    move-object v5, v4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/e/a;-><init>(Ljava/lang/String;Lorg/json/JSONArray;Ljava/lang/Class;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    invoke-virtual {v10, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 95
    :try_start_0
    invoke-virtual {v4}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 97
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 98
    new-instance v0, Lcom/google/android/gms/common/server/ac;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/ac;-><init>()V

    throw v0

    .line 99
    :catch_1
    move-exception v0

    .line 100
    new-instance v1, Lcom/android/volley/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
