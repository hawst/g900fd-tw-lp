.class public final Lcom/google/android/gms/plus/sharebox/al;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/gms/plus/audience/bh;


# instance fields
.field private a:Lcom/google/android/gms/plus/sharebox/bf;

.field private b:Lcom/google/android/gms/plus/audience/ae;

.field private c:Lcom/google/android/gms/plus/audience/a;

.field private d:Lcom/google/android/gms/plus/audience/g;

.field private e:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 368
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->q:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/gms/f;->al:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 373
    :cond_0
    return-void

    .line 369
    :cond_1
    sget v0, Lcom/google/android/gms/f;->am:I

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 6

    .prologue
    .line 212
    if-nez p1, :cond_0

    .line 213
    new-instance v0, Lcom/google/android/gms/plus/audience/a/d;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/bf;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/au;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v4}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v5}, Lcom/google/android/gms/plus/sharebox/bf;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/a/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 219
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown loader ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 241
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 46
    check-cast p2, Lcom/google/android/gms/people/model/h;

    iget v0, p1, Landroid/support/v4/a/j;->m:I

    if-nez v0, :cond_0

    check-cast p1, Lcom/google/android/gms/plus/audience/a/d;

    iget-object v0, p1, Lcom/google/android/gms/common/ui/k;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/people/model/h;->c()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/android/gms/people/model/h;->b(I)Lcom/google/android/gms/people/model/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/a;->a(Lcom/google/android/gms/people/model/g;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    new-instance v2, Lcom/google/android/gms/common/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->i()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/c;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/model/posts/Settings;)V
    .locals 18

    .prologue
    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/sharebox/al;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v16

    .line 169
    invoke-virtual/range {v16 .. v16}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v17

    .line 170
    const-string v1, "selection"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/audience/a;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    .line 172
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    if-nez v1, :cond_1

    .line 173
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bf;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/au;->f()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->g()I

    move-result v1

    if-lez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bf;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/au;->f()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->h()I

    move-result v12

    const/4 v13, 0x0

    :goto_0
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->q:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->q:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v15

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bf;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/au;->b()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/android/gms/plus/sharebox/bg;->j:Z

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v8}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v8

    iget-object v8, v8, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v9}, Lcom/google/android/gms/plus/sharebox/bf;->getCallingPackage()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v10}, Lcom/google/android/gms/plus/sharebox/bf;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v11}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v11

    iget-boolean v11, v11, Lcom/google/android/gms/plus/sharebox/bg;->k:Z

    const/4 v14, 0x0

    invoke-static/range {v1 .. v15}, Lcom/google/android/gms/plus/audience/a;->a(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIIILjava/util/List;)Lcom/google/android/gms/plus/audience/a;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    .line 174
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/audience/a;->b(Z)V

    .line 175
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/plus/model/posts/Settings;->e()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/audience/a;->a(Z)V

    .line 176
    sget v1, Lcom/google/android/gms/j;->q:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    const-string v3, "selection"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 179
    :cond_1
    const-string v1, "search"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/audience/g;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    .line 181
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    if-nez v1, :cond_2

    .line 182
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bf;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/au;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/sharebox/al;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v4}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v4}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v5}, Lcom/google/android/gms/plus/sharebox/bf;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v1, v4, v5}, Lcom/google/android/gms/plus/sharebox/an;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/g;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    .line 183
    sget v1, Lcom/google/android/gms/j;->q:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    const-string v3, "search"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 187
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 188
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 189
    invoke-virtual/range {v17 .. v17}, Landroid/support/v4/app/aj;->b()I

    .line 191
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bf;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v1

    if-eqz v1, :cond_3

    .line 192
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/sharebox/al;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v1, v2, v3, v0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 194
    :cond_3
    return-void

    .line 173
    :cond_4
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bf;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/au;->f()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->h()I

    move-result v13

    goto/16 :goto_0

    .line 182
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/g;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/a;->isHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->b()V

    .line 303
    :cond_0
    return-void
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/g;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->b()V

    .line 338
    const/4 v0, 0x1

    .line 340
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 317
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->h()Lcom/google/android/gms/plus/sharebox/bh;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/bh;->a(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/al;->a(Z)V

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/g;->a(Ljava/lang/String;)V

    .line 323
    :goto_0
    return-void

    .line 321
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->b()V

    goto :goto_0
.end method

.method final b()V
    .locals 2

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 348
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 349
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->h()Lcom/google/android/gms/plus/sharebox/bh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/bh;->a(I)V

    .line 354
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/al;->a(Z)V

    .line 355
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 328
    return-void
.end method

.method final c()I
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/ac;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/o;->g()I

    move-result v0

    return v0
.end method

.method final d()I
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/ac;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/o;->h()I

    move-result v0

    return v0
.end method

.method final e()I
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/ac;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/o;->i()I

    move-result v0

    return v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 79
    instance-of v0, p1, Lcom/google/android/gms/plus/sharebox/bf;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/sharebox/bf;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 84
    check-cast v0, Lcom/google/android/gms/plus/sharebox/bf;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    .line 85
    instance-of v0, p1, Lcom/google/android/gms/plus/audience/ae;

    if-eqz v0, :cond_1

    .line 86
    check-cast p1, Lcom/google/android/gms/plus/audience/ae;

    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    .line 89
    :cond_1
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 72
    sget v0, Lcom/google/android/gms/l;->eM:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 2

    .prologue
    .line 198
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 201
    return-void
.end method

.method public final onDetach()V
    .locals 2

    .prologue
    .line 205
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/au;->a(I)V

    .line 208
    return-void
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 307
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 309
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 310
    const/4 v0, 0x1

    .line 312
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onHiddenChanged(Z)V
    .locals 2

    .prologue
    .line 151
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onHiddenChanged(Z)V

    .line 152
    if-nez p1, :cond_1

    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->b()V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->a:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->h()Lcom/google/android/gms/plus/sharebox/bh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/bh;->a(Z)V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/plus/audience/bh;)V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->b(Lcom/google/android/gms/plus/audience/bh;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "selection_hidden"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/a;->isHidden()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    if-eqz v0, :cond_1

    .line 98
    const-string v0, "search_hidden"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/g;->isHidden()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 100
    :cond_1
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 333
    return-void
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->pp:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    sget v1, Lcom/google/android/gms/p;->vM:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->e:Landroid/widget/EditText;

    const/16 v1, 0x61

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/al;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 115
    const-string v0, "selection"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/a;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    .line 117
    const-string v0, "search"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/g;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    .line 119
    if-eqz p1, :cond_3

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/al;->b:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/plus/audience/bh;)V

    .line 126
    :cond_0
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    if-eqz v1, :cond_1

    .line 128
    const-string v1, "selection_hidden"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 129
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 135
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    if-eqz v1, :cond_2

    .line 136
    const-string v1, "search_hidden"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 137
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 143
    :cond_2
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->d()Z

    move-result v1

    if-nez v1, :cond_3

    .line 144
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 147
    :cond_3
    return-void

    .line 131
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->c:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 132
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/sharebox/al;->a(Z)V

    goto :goto_0

    .line 139
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/al;->d:Lcom/google/android/gms/plus/audience/g;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 140
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/sharebox/al;->a(Z)V

    goto :goto_1
.end method
