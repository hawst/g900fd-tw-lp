.class final Lcom/google/android/gms/drive/events/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/google/android/gms/drive/events/l;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/events/l;J)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/gms/drive/events/n;->b:Lcom/google/android/gms/drive/events/l;

    iput-wide p2, p0, Lcom/google/android/gms/drive/events/n;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/drive/events/n;->b:Lcom/google/android/gms/drive/events/l;

    iget-object v0, v0, Lcom/google/android/gms/drive/events/l;->f:Lcom/google/android/gms/drive/database/r;

    iget-wide v2, p0, Lcom/google/android/gms/drive/events/n;->a:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/drive/database/r;->g(J)Lcom/google/android/gms/drive/database/model/bl;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    .line 129
    iget-object v1, p0, Lcom/google/android/gms/drive/events/n;->b:Lcom/google/android/gms/drive/events/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/events/l;->a(Lcom/google/android/gms/drive/database/model/bl;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/drive/events/n;->b:Lcom/google/android/gms/drive/events/l;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/events/l;->a()V

    .line 134
    :goto_0
    return-void

    .line 132
    :cond_0
    const-string v0, "EventDistributorImpl"

    const-string v1, "New persisted event is missing in the database."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
