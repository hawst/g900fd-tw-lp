.class public final Lcom/google/android/gms/photos/autobackup/model/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Lcom/google/android/gms/photos/autobackup/model/UserQuota;

.field private final h:Ljava/lang/String;

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/a;->b:Z

    .line 179
    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/a;->d:Z

    .line 180
    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/a;->e:Z

    .line 187
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/model/a;->h:Ljava/lang/String;

    .line 188
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;
    .locals 12

    .prologue
    .line 236
    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/model/a;->h:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/photos/autobackup/model/a;->a:Z

    iget-boolean v4, p0, Lcom/google/android/gms/photos/autobackup/model/a;->b:Z

    iget-boolean v5, p0, Lcom/google/android/gms/photos/autobackup/model/a;->c:Z

    iget-boolean v6, p0, Lcom/google/android/gms/photos/autobackup/model/a;->d:Z

    iget-boolean v7, p0, Lcom/google/android/gms/photos/autobackup/model/a;->e:Z

    iget-boolean v8, p0, Lcom/google/android/gms/photos/autobackup/model/a;->f:Z

    iget-boolean v9, p0, Lcom/google/android/gms/photos/autobackup/model/a;->i:Z

    iget-boolean v10, p0, Lcom/google/android/gms/photos/autobackup/model/a;->j:Z

    iget-object v11, p0, Lcom/google/android/gms/photos/autobackup/model/a;->g:Lcom/google/android/gms/photos/autobackup/model/UserQuota;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;-><init>(ILjava/lang/String;ZZZZZZZZLcom/google/android/gms/photos/autobackup/model/UserQuota;)V

    return-object v0
.end method
