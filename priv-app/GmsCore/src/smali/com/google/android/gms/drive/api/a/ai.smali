.class public final Lcom/google/android/gms/drive/api/a/ai;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 26
    const/16 v0, 0x13

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/ai;->e:Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ai;->e:Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;

    const-string v1, "Invalid request: unable to update preferences."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ai;->e:Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;->a()Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;

    move-result-object v0

    const-string v1, "Invalid request: unable to update preferences."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ai;->c:Lcom/google/android/gms/drive/c/a;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ai;->e:Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;->a()Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/ah;)Lcom/google/android/gms/drive/c/a;

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ai;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ai;->e:Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;->a()Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ai;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 42
    return-void
.end method
