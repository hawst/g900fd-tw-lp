.class public Lcom/google/android/gms/fitness/wearables/WearableSyncService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static a:J

.field private static b:J

.field private static c:J

.field private static d:J

.field private static final e:Ljava/util/PriorityQueue;

.field private static f:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 61
    sput-wide v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a:J

    .line 62
    sput-wide v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->b:J

    .line 63
    sput-wide v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->c:J

    .line 64
    sput-wide v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->d:J

    .line 71
    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Ljava/util/PriorityQueue;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->e:Ljava/util/PriorityQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 82
    const-class v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/api/v;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 180
    const-string v0, "syncing to node: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 181
    array-length v3, p2

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p2, v1

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "WearableSyncService syncing account:"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-static {p0}, Lcom/google/android/gms/fitness/h/d;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/h/f;

    move-result-object v0

    invoke-static {p0, v4}, Lcom/google/android/gms/fitness/h/c;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->a()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->b()Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/gms/fitness/h/a;->a(Landroid/os/Handler;)Lcom/google/android/gms/fitness/sensors/a/a;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/fitness/h/a;->a(Ljava/util/List;Lcom/google/android/gms/fitness/sensors/a/a;)Lcom/google/android/gms/fitness/sensors/a;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/fitness/h/a;->a(Lcom/google/android/gms/fitness/sensors/a;)Lcom/google/android/gms/fitness/k/a;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/gms/fitness/h/a;->a()Lcom/google/android/gms/fitness/l/z;

    move-result-object v5

    new-instance v7, Lcom/google/android/gms/fitness/wearables/a;

    invoke-direct {v7, p1, v4, p3}, Lcom/google/android/gms/fitness/wearables/a;-><init>(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->f()Lcom/google/android/gms/fitness/l/a/b;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/fitness/l/a/b;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/l/a/a;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/gms/fitness/l/a/a;->a()V

    new-instance v0, Lcom/google/android/gms/fitness/sync/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/fitness/sync/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v7, v5, v6}, Lcom/google/android/gms/fitness/sync/b;->a(Lcom/google/android/gms/fitness/sync/d;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/k/a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 182
    :catch_0
    move-exception v0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WearableSyncService. unable to sync: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 184
    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Intent;J)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 119
    const-string v0, "WearableSyncService.done"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 120
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 121
    const-string v2, "Sync duration millis: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 122
    sget-object v2, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->e:Ljava/util/PriorityQueue;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    :goto_0
    sget-object v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->e:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->e:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    goto :goto_0

    .line 123
    :cond_0
    sget-wide v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    sput-wide v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->d:J

    .line 124
    invoke-static {p0}, Lcom/google/android/gms/fitness/wearables/WearableSyncServiceReceiver;->a(Landroid/content/Intent;)Z

    .line 125
    return-void
.end method

.method public static a(Ljava/io/PrintWriter;)V
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 232
    const-string v0, "TotalSyncRequestCount: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    sget-wide v2, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->append(C)Ljava/io/PrintWriter;

    .line 234
    const-string v0, "ReconnectSyncRequestCount: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    sget-wide v2, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->b:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->append(C)Ljava/io/PrintWriter;

    .line 236
    const-string v0, "SkippedRequestCount: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    sget-wide v2, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->c:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->append(C)Ljava/io/PrintWriter;

    .line 238
    const-string v0, "SyncsCompleted: "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    sget-wide v2, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->d:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->append(C)Ljava/io/PrintWriter;

    .line 239
    const-string v0, "Top"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "10"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "SlowestRequests"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->e:Ljava/util/PriorityQueue;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->append(C)Ljava/io/PrintWriter;

    .line 241
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 223
    invoke-super {p0, p1, p2, p3}, Landroid/app/IntentService;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 224
    invoke-static {p2}, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a(Ljava/io/PrintWriter;)V

    .line 225
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const-wide/16 v10, 0x1

    const/4 v2, 0x0

    .line 87
    const-string v0, "WearableSyncService.start"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 90
    sget-wide v6, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a:J

    add-long/2addr v6, v10

    sput-wide v6, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a:J

    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v8, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->f:J

    sub-long/2addr v6, v8

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    const-string v0, "Seconds since last completed sync intent: %d"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->N:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v8, v0

    cmp-long v0, v6, v8

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 92
    const-string v0, "Skipping intent"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 93
    sget-wide v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->c:J

    add-long/2addr v0, v10

    sput-wide v0, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->c:J

    .line 94
    invoke-static {p1, v4, v5}, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a(Landroid/content/Intent;J)V

    .line 115
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 91
    goto :goto_0

    .line 97
    :cond_1
    const/4 v1, 0x0

    .line 99
    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/wearable/z;->g:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 100
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->O:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v2, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/common/api/v;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/c;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 103
    const-string v0, "accounts"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-string v0, "node_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "Dispatching sync all with node"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-wide v6, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->b:J

    add-long/2addr v6, v10

    sput-wide v6, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->b:J

    const-string v0, "node_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/v;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :cond_2
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->f:J

    .line 111
    if-eqz v1, :cond_3

    .line 112
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    .line 114
    :cond_3
    invoke-static {p1, v4, v5}, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a(Landroid/content/Intent;J)V

    goto :goto_1

    .line 103
    :cond_4
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "WearableSyncService.syncAll: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/wearable/z;->c:Lcom/google/android/gms/wearable/t;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wearable/t;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->O:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v6, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v6, v7, v0}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/u;

    invoke-interface {v0}, Lcom/google/android/gms/wearable/u;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Lcom/google/android/gms/wearable/u;->b()Ljava/util/List;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "WearableSyncService syncing to "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " nodes."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v6}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/s;

    invoke-interface {v0}, Lcom/google/android/gms/wearable/s;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v2, v0}, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/v;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 107
    :catch_0
    move-exception v0

    :try_start_2
    const-string v2, "WearableSyncService error"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->f:J

    .line 111
    if-eqz v1, :cond_5

    .line 112
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    .line 114
    :cond_5
    invoke-static {p1, v4, v5}, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a(Landroid/content/Intent;J)V

    goto/16 :goto_1

    .line 103
    :cond_6
    :try_start_3
    const-string v0, "WearableSyncService.failed to enableConnection"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 110
    :catchall_0
    move-exception v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->f:J

    .line 111
    if-eqz v1, :cond_7

    .line 112
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    .line 114
    :cond_7
    invoke-static {p1, v4, v5}, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a(Landroid/content/Intent;J)V

    throw v0

    .line 105
    :cond_8
    :try_start_4
    const-string v0, "WearableSyncService.failed to connect to Wearable.API"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2
.end method
