.class final Lcom/google/android/gms/car/dx;
.super Lcom/google/android/gms/car/li;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;

.field private volatile b:Z

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/dt;)V
    .locals 1

    .prologue
    .line 720
    invoke-direct {p0}, Lcom/google/android/gms/car/li;-><init>()V

    .line 716
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/dx;->b:Z

    .line 717
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/dx;->c:Ljava/lang/Object;

    .line 721
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/car/dx;->d:Ljava/lang/ref/WeakReference;

    .line 722
    return-void
.end method

.method private a(Lcom/google/android/gms/car/t;I)V
    .locals 3

    .prologue
    .line 807
    iget-object v0, p0, Lcom/google/android/gms/car/dx;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/dt;

    .line 808
    if-nez v0, :cond_0

    .line 822
    :goto_0
    return-void

    .line 811
    :cond_0
    const-string v1, "CAR.CLIENT"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 812
    const-string v1, "CAR.CLIENT"

    const-string v2, "ICarConnectionListenerImpl.notifyCarConnectionToClient"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/car/dt;->i()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/car/dy;

    invoke-direct {v2, p0, v0, p1, p2}, Lcom/google/android/gms/car/dy;-><init>(Lcom/google/android/gms/car/dx;Lcom/google/android/gms/car/dt;Lcom/google/android/gms/car/t;I)V

    invoke-static {v1, v2}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v1, 0x0

    .line 783
    iget-object v0, p0, Lcom/google/android/gms/car/dx;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/dt;

    .line 784
    if-nez v0, :cond_1

    .line 803
    :cond_0
    :goto_0
    return-void

    .line 787
    :cond_1
    const-string v2, "CAR.CLIENT"

    invoke-static {v2, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 788
    const-string v2, "CAR.CLIENT"

    const-string v3, "ICarConnectionListenerImpl.onDisconnected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/car/dx;->c:Ljava/lang/Object;

    monitor-enter v3

    .line 793
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/gms/car/dx;->b:Z

    if-eqz v2, :cond_5

    .line 794
    const/4 v1, 0x1

    move v2, v1

    .line 796
    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/dx;->b:Z

    .line 797
    iget-object v1, p0, Lcom/google/android/gms/car/dx;->a:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_4

    const/4 v1, 0x0

    .line 798
    :goto_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 799
    invoke-static {v0}, Lcom/google/android/gms/car/dt;->d(Lcom/google/android/gms/car/dt;)V

    .line 800
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 801
    iget-object v0, p0, Lcom/google/android/gms/car/dx;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/dt;

    if-eqz v0, :cond_0

    const-string v2, "CAR.CLIENT"

    invoke-static {v2, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "CAR.CLIENT"

    const-string v3, "ICarConnectionListenerImpl.notifyCarDisconnectionToClient"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/car/dt;->i()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/car/dz;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/gms/car/dz;-><init>(Lcom/google/android/gms/car/dx;Lcom/google/android/gms/car/dt;Lcom/google/android/gms/car/t;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 797
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/car/dx;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 798
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5
    move v2, v1

    goto :goto_1
.end method

.method public final a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 760
    const-string v0, "CAR.CLIENT"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 761
    const-string v0, "CAR.CLIENT"

    const-string v2, "ICarConnectionListenerImpl.onConnected"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    :cond_0
    const/4 v2, 0x0

    .line 766
    iget-object v4, p0, Lcom/google/android/gms/car/dx;->c:Ljava/lang/Object;

    monitor-enter v4

    .line 767
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/dx;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v3, v0

    .line 768
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/dx;->b:Z

    if-nez v0, :cond_3

    if-eqz v3, :cond_3

    .line 770
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/dx;->b:Z

    move v0, v1

    .line 772
    :goto_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 773
    if-eqz v0, :cond_2

    .line 774
    invoke-direct {p0, v3, p1}, Lcom/google/android/gms/car/dx;->a(Lcom/google/android/gms/car/t;I)V

    .line 779
    :goto_2
    return-void

    .line 767
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/dx;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v3, v0

    goto :goto_0

    .line 772
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 776
    :cond_2
    const-string v0, "CAR.CLIENT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not notifying car connection [listener="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", shouldNotify=false, mConnectionNotified="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/car/dx;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/car/t;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 725
    iget-object v0, p0, Lcom/google/android/gms/car/dx;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/dt;

    .line 726
    if-nez v0, :cond_1

    .line 756
    :cond_0
    :goto_0
    return-void

    .line 729
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/car/dx;->c:Ljava/lang/Object;

    monitor-enter v3

    .line 730
    const/4 v4, 0x0

    :try_start_0
    iput-boolean v4, p0, Lcom/google/android/gms/car/dx;->b:Z

    .line 731
    if-nez p1, :cond_2

    .line 732
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/dx;->a:Ljava/lang/ref/WeakReference;

    .line 733
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 736
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 735
    :cond_2
    :try_start_1
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/google/android/gms/car/dx;->a:Ljava/lang/ref/WeakReference;

    .line 736
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 743
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/car/dx;->c:Ljava/lang/Object;

    monitor-enter v3
    :try_end_2
    .catch Lcom/google/android/gms/car/fw; {:try_start_2 .. :try_end_2} :catch_0

    .line 744
    :try_start_3
    invoke-virtual {v0}, Lcom/google/android/gms/car/dt;->e()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/google/android/gms/car/dx;->b:Z

    if-nez v4, :cond_3

    .line 746
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/car/dx;->b:Z

    .line 748
    :goto_1
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 749
    if-eqz v1, :cond_0

    .line 750
    :try_start_4
    invoke-virtual {v0}, Lcom/google/android/gms/car/dt;->f()I

    move-result v0

    .line 751
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/dx;->a(Lcom/google/android/gms/car/t;I)V

    goto :goto_0

    .line 756
    :catch_0
    move-exception v0

    goto :goto_0

    .line 748
    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0
    :try_end_4
    .catch Lcom/google/android/gms/car/fw; {:try_start_4 .. :try_end_4} :catch_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method
