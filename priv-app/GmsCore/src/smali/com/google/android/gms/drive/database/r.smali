.class public interface abstract Lcom/google/android/gms/drive/database/r;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Lcom/google/android/gms/drive/auth/g;J)I
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/ah;JLcom/google/android/gms/drive/auth/a;)Lcom/google/android/gms/drive/auth/a;
.end method

.method public abstract a(JJ)Lcom/google/android/gms/drive/auth/g;
.end method

.method public abstract a(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/auth/g;
.end method

.method public abstract a()Lcom/google/android/gms/drive/database/b/d;
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/database/b/d;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/EntrySpec;I)Lcom/google/android/gms/drive/database/b/d;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/b/d;
.end method

.method public abstract a(J)Lcom/google/android/gms/drive/database/model/a;
.end method

.method public abstract a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/a;
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/ah;
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Lcom/google/android/gms/drive/database/model/ah;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/t;)Lcom/google/android/gms/drive/database/model/ah;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/am;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/c;J)Lcom/google/android/gms/drive/database/model/bb;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)Lcom/google/android/gms/drive/database/model/bi;
.end method

.method public abstract a(JLcom/google/android/gms/drive/database/model/EntrySpec;I)Lcom/google/android/gms/drive/database/model/bv;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/ce;)Ljava/lang/Long;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/e;)Ljava/util/List;
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/g;Ljava/util/Set;)Ljava/util/Set;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;)Ljava/util/Set;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/ah;)Ljava/util/Set;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/database/model/EntrySpec;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/a;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/ah;Ljava/util/Set;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/g/at;)V
.end method

.method public abstract a(Ljava/util/Collection;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;)Z
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/model/ae;)Z
.end method

.method public abstract b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;
.end method

.method public abstract b(J)Lcom/google/android/gms/drive/database/model/bi;
.end method

.method public abstract b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;
.end method

.method public abstract b(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/i;
.end method

.method public abstract b(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Ljava/util/List;
.end method

.method public abstract b()V
.end method

.method public abstract b(JLcom/google/android/gms/drive/auth/AppIdentity;)V
.end method

.method public abstract b(JLcom/google/android/gms/drive/database/model/EntrySpec;I)V
.end method

.method public abstract b(Lcom/google/android/gms/drive/database/model/a;)V
.end method

.method public abstract c(Lcom/google/android/gms/drive/database/model/a;)I
.end method

.method public abstract c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;
.end method

.method public abstract c(J)Lcom/google/android/gms/drive/database/model/be;
.end method

.method public abstract c(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/f;
.end method

.method public abstract c()V
.end method

.method public abstract d(Lcom/google/android/gms/drive/database/model/a;)Ljava/util/Set;
.end method

.method public abstract d(J)V
.end method

.method public abstract d(Lcom/google/android/gms/drive/auth/g;)V
.end method

.method public abstract d(Ljava/lang/String;)V
.end method

.method public abstract d()Z
.end method

.method public abstract e(J)Lcom/google/android/gms/drive/database/model/ce;
.end method

.method public abstract e(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/l;
.end method

.method public abstract e()V
.end method

.method public abstract e(Ljava/lang/String;)V
.end method

.method public abstract f(J)Lcom/google/android/gms/drive/database/model/f;
.end method

.method public abstract f()V
.end method

.method public abstract f(Ljava/lang/String;)Z
.end method

.method public abstract g(J)Lcom/google/android/gms/drive/database/model/bl;
.end method

.method public abstract g()V
.end method

.method public abstract g(Ljava/lang/String;)V
.end method

.method public abstract h(J)Ljava/util/List;
.end method

.method public abstract h()V
.end method

.method public abstract i()V
.end method

.method public abstract j()J
.end method

.method public abstract k()Z
.end method

.method public abstract l()Lcom/google/android/gms/drive/database/b/d;
.end method

.method public abstract m()Lcom/google/android/gms/drive/database/b/d;
.end method

.method public abstract n()J
.end method

.method public abstract o()Lcom/google/android/gms/drive/database/b/d;
.end method

.method public abstract p()J
.end method

.method public abstract q()Lcom/google/android/gms/drive/database/b/d;
.end method

.method public abstract r()J
.end method

.method public abstract s()I
.end method

.method public abstract t()J
.end method

.method public abstract u()Lcom/google/android/gms/drive/database/t;
.end method

.method public abstract v()Lcom/google/android/gms/drive/database/b/d;
.end method

.method public abstract w()Ljava/util/Set;
.end method
