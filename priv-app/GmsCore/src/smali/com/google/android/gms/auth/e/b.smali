.class public final Lcom/google/android/gms/auth/e/b;
.super Lcom/google/android/gms/auth/e/a;
.source "SourceFile"


# instance fields
.field public d:Z

.field public e:Ljava/lang/String;

.field public f:J

.field public g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/e/a;-><init>(Landroid/content/Context;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/auth/b/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v1

    sget-object v0, Lcom/google/android/gms/auth/b/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/f/c;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/c;-><init>()V

    .line 34
    iget-boolean v1, p0, Lcom/google/android/gms/auth/e/b;->d:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/c;->a(Z)Lcom/google/android/gms/auth/f/c;

    .line 35
    iget-object v1, p0, Lcom/google/android/gms/auth/e/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/c;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/f/c;

    .line 36
    iget-wide v2, p0, Lcom/google/android/gms/auth/e/b;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/auth/f/c;->a(J)Lcom/google/android/gms/auth/f/c;

    .line 37
    iget v1, p0, Lcom/google/android/gms/auth/e/b;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/c;->a(I)Lcom/google/android/gms/auth/f/c;

    .line 39
    new-instance v1, Lcom/google/android/gms/auth/f/b;

    invoke-direct {v1}, Lcom/google/android/gms/auth/f/b;-><init>()V

    .line 40
    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/f/b;->a(I)Lcom/google/android/gms/auth/f/b;

    .line 41
    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/c;)Lcom/google/android/gms/auth/f/b;

    .line 43
    const-string v0, "AuthServer"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/e/b;->a(Ljava/lang/String;Lcom/google/android/gms/auth/f/b;)V

    goto :goto_0
.end method
