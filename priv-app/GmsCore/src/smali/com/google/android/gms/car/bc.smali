.class public final Lcom/google/android/gms/car/bc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[Lcom/google/android/gms/car/bm;

.field final b:Ljava/lang/Object;

.field final c:Ljava/util/LinkedList;

.field volatile d:Z


# virtual methods
.method final a(I)V
    .locals 4

    .prologue
    .line 407
    iget-object v1, p0, Lcom/google/android/gms/car/bc;->a:[Lcom/google/android/gms/car/bm;

    monitor-enter v1

    .line 408
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown stream type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 408
    :pswitch_1
    const/4 v0, 0x0

    .line 409
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/bc;->a:[Lcom/google/android/gms/car/bm;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 410
    iget-object v2, p0, Lcom/google/android/gms/car/bc;->a:[Lcom/google/android/gms/car/bm;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 412
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 408
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
