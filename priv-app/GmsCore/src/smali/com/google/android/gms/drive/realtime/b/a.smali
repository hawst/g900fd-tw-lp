.class public Lcom/google/android/gms/drive/realtime/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/drive/realtime/b/a;

.field private static b:Lcom/google/android/gms/drive/realtime/b/a;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/drive/realtime/cache/o;

.field private final e:Lcom/google/android/gms/drive/realtime/cache/i;

.field private final f:Lcom/google/android/gms/drive/realtime/f;

.field private final g:Lcom/google/android/gms/drive/realtime/cache/q;

.field private final h:Lcom/google/android/gms/drive/realtime/b;

.field private final i:Lcom/google/android/gms/drive/realtime/cache/c;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;)V
    .locals 8

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/b/a;->c:Landroid/content/Context;

    .line 66
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/i;

    new-instance v1, Lcom/google/android/gms/drive/realtime/c;

    invoke-direct {v1}, Lcom/google/android/gms/drive/realtime/c;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/i;-><init>(Lcom/google/c/a/a/b/d/b;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->e:Lcom/google/android/gms/drive/realtime/cache/i;

    .line 67
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/c;

    invoke-direct {v0}, Lcom/google/android/gms/drive/realtime/cache/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->i:Lcom/google/android/gms/drive/realtime/cache/c;

    .line 68
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/o;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/b/a;->c:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->g()Lcom/google/android/gms/drive/database/i;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/common/util/r;

    invoke-direct {v3}, Lcom/google/android/gms/common/util/r;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/drive/realtime/b/a;->e:Lcom/google/android/gms/drive/realtime/cache/i;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/drive/realtime/b/a;->i:Lcom/google/android/gms/drive/realtime/cache/c;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/realtime/cache/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/realtime/cache/c;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->d:Lcom/google/android/gms/drive/realtime/cache/o;

    .line 71
    new-instance v0, Lcom/google/android/gms/drive/realtime/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/b/a;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/b/a;->d:Lcom/google/android/gms/drive/realtime/cache/o;

    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/b/a;->e:Lcom/google/android/gms/drive/realtime/cache/i;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->e()Lcom/google/android/gms/drive/g/n;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->D()Lcom/google/android/gms/drive/realtime/cache/w;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/realtime/cache/w;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->f:Lcom/google/android/gms/drive/realtime/f;

    .line 75
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/q;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/b/a;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/b/a;->e:Lcom/google/android/gms/drive/realtime/cache/i;

    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/b/a;->d:Lcom/google/android/gms/drive/realtime/cache/o;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v4

    new-instance v5, Lcom/google/c/a/a/b/a/a/a;

    invoke-direct {v5}, Lcom/google/c/a/a/b/a/a/a;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->D()Lcom/google/android/gms/drive/realtime/cache/w;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->e()Lcom/google/android/gms/drive/g/n;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/realtime/cache/q;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/database/r;Lcom/google/c/a/a/b/a/a/a;Lcom/google/android/gms/drive/realtime/cache/w;Lcom/google/android/gms/drive/g/n;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->g:Lcom/google/android/gms/drive/realtime/cache/q;

    .line 79
    new-instance v0, Lcom/google/android/gms/drive/realtime/b;

    invoke-direct {v0}, Lcom/google/android/gms/drive/realtime/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->h:Lcom/google/android/gms/drive/realtime/b;

    .line 80
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;)Lcom/google/android/gms/drive/realtime/b/a;
    .locals 3

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/gms/drive/realtime/b/a;->a:Lcom/google/android/gms/drive/realtime/b/a;

    if-eqz v0, :cond_0

    .line 39
    sget-object v0, Lcom/google/android/gms/drive/realtime/b/a;->a:Lcom/google/android/gms/drive/realtime/b/a;

    .line 51
    :goto_0
    return-object v0

    .line 41
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 42
    const-class v1, Lcom/google/android/gms/drive/realtime/b/a;

    monitor-enter v1

    .line 43
    :try_start_0
    sget-object v2, Lcom/google/android/gms/drive/realtime/b/a;->b:Lcom/google/android/gms/drive/realtime/b/a;

    if-eqz v2, :cond_1

    .line 44
    sget-object v2, Lcom/google/android/gms/drive/realtime/b/a;->b:Lcom/google/android/gms/drive/realtime/b/a;

    iget-object v2, v2, Lcom/google/android/gms/drive/realtime/b/a;->c:Landroid/content/Context;

    if-eq v2, v0, :cond_2

    .line 45
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "attempted to initialize Singletons multiple times with different application context instances."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 49
    :cond_1
    :try_start_1
    new-instance v2, Lcom/google/android/gms/drive/realtime/b/a;

    invoke-direct {v2, v0, p1}, Lcom/google/android/gms/drive/realtime/b/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;)V

    sput-object v2, Lcom/google/android/gms/drive/realtime/b/a;->b:Lcom/google/android/gms/drive/realtime/b/a;

    .line 51
    :cond_2
    sget-object v0, Lcom/google/android/gms/drive/realtime/b/a;->b:Lcom/google/android/gms/drive/realtime/b/a;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/realtime/cache/o;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->d:Lcom/google/android/gms/drive/realtime/cache/o;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/realtime/f;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->f:Lcom/google/android/gms/drive/realtime/f;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/drive/realtime/cache/q;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->g:Lcom/google/android/gms/drive/realtime/cache/q;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/drive/realtime/b;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->h:Lcom/google/android/gms/drive/realtime/b;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/drive/realtime/cache/c;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/b/a;->i:Lcom/google/android/gms/drive/realtime/cache/c;

    return-object v0
.end method
