.class public final Lcom/google/android/gms/people/service/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/internal/f;


# instance fields
.field final a:Z

.field private final b:Lcom/google/android/gms/people/internal/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/internal/f;Z)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/people/service/d;->b:Lcom/google/android/gms/people/internal/f;

    .line 25
    iput-boolean p2, p0, Lcom/google/android/gms/people/service/d;->a:Z

    .line 26
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/people/service/d;->b:Lcom/google/android/gms/people/internal/f;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/people/internal/f;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 33
    return-void
.end method

.method public final a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Shouldn\'t be called"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/d;->a:Z

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/people/service/d;->b:Lcom/google/android/gms/people/internal/f;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/people/internal/f;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/d;->b:Lcom/google/android/gms/people/internal/f;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/people/internal/f;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/people/service/d;->b:Lcom/google/android/gms/people/internal/f;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/people/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    .line 39
    return-void
.end method

.method public final a(ILandroid/os/Bundle;[Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/people/service/d;->b:Lcom/google/android/gms/people/internal/f;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/people/internal/f;->a(ILandroid/os/Bundle;[Lcom/google/android/gms/common/data/DataHolder;)V

    .line 45
    return-void
.end method

.method public final asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/people/service/d;->b:Lcom/google/android/gms/people/internal/f;

    invoke-interface {v0}, Lcom/google/android/gms/people/internal/f;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
