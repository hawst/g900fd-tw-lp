.class final Lcom/google/android/gms/car/bq;
.super Lcom/google/android/gms/car/kz;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 364
    const-string v0, "CarBluetoothClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    const-string v0, "CarBluetoothClient"

    const-string v1, "onCarDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 368
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 372
    const-string v0, "CarBluetoothClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    const-string v0, "CarBluetoothClient"

    const-string v1, "onEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 376
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 380
    const-string v0, "CarBluetoothClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    const-string v0, "CarBluetoothClient"

    const-string v1, "onDisabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 384
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 388
    const-string v0, "CarBluetoothClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    const-string v0, "CarBluetoothClient"

    const-string v1, "onCarDelayedPairing"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 392
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 396
    const-string v0, "CarBluetoothClient"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    const-string v0, "CarBluetoothClient"

    const-string v1, "onPaired"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 400
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 404
    const-string v0, "CarBluetoothClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    const-string v0, "CarBluetoothClient"

    const-string v1, "onUnpaired"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 408
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 412
    const-string v0, "CarBluetoothClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    const-string v0, "CarBluetoothClient"

    const-string v1, "onHfpConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 416
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 420
    const-string v0, "CarBluetoothClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    const-string v0, "CarBluetoothClient"

    const-string v1, "onHfpDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/bq;->a:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 424
    return-void
.end method
