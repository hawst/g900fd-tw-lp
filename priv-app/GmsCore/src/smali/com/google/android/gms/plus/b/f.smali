.class public Lcom/google/android/gms/plus/b/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/plus/b/f;


# instance fields
.field private final b:Landroid/support/v4/g/h;

.field private final c:Landroid/support/v4/g/h;

.field private final d:Landroid/support/v4/g/h;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x14

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/support/v4/g/h;

    invoke-direct {v0, v1}, Landroid/support/v4/g/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    .line 33
    new-instance v0, Landroid/support/v4/g/h;

    invoke-direct {v0, v1}, Landroid/support/v4/g/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/f;->c:Landroid/support/v4/g/h;

    .line 36
    new-instance v0, Landroid/support/v4/g/h;

    invoke-direct {v0, v1}, Landroid/support/v4/g/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/f;->d:Landroid/support/v4/g/h;

    .line 49
    return-void
.end method

.method public static a()Lcom/google/android/gms/plus/b/f;
    .locals 2

    .prologue
    .line 39
    const-class v1, Lcom/google/android/gms/plus/b/f;

    monitor-enter v1

    .line 40
    :try_start_0
    sget-object v0, Lcom/google/android/gms/plus/b/f;->a:Lcom/google/android/gms/plus/b/f;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/google/android/gms/plus/b/f;

    invoke-direct {v0}, Lcom/google/android/gms/plus/b/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/b/f;->a:Lcom/google/android/gms/plus/b/f;

    .line 43
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/b/f;->a:Lcom/google/android/gms/plus/b/f;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 118
    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, "<<null account>>"

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 2

    .prologue
    .line 128
    if-nez p1, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 132
    :goto_0
    return-object v0

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/b/f;->c:Landroid/support/v4/g/h;

    monitor-enter v1

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/f;->c:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/b/h;
    .locals 3

    .prologue
    .line 99
    iget-object v1, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    monitor-enter v1

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    invoke-static {p1}, Lcom/google/android/gms/plus/b/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/b/h;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/plus/service/pos/j;
    .locals 7

    .prologue
    .line 75
    iget-object v5, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    monitor-enter v5

    .line 76
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/plus/b/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v6

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    invoke-virtual {v1, v6}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/b/h;

    .line 78
    if-nez v1, :cond_0

    .line 79
    const/4 v3, 0x0

    monitor-exit v5

    .line 88
    :goto_0
    return-object v3

    .line 81
    :cond_0
    iget-object v3, v1, Lcom/google/android/gms/plus/b/h;->a:Lcom/google/android/gms/plus/service/pos/j;

    .line 82
    if-eqz p3, :cond_1

    invoke-static {v3}, Lcom/google/android/gms/plus/b/g;->a(Lcom/google/android/gms/plus/service/pos/j;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    if-nez p3, :cond_3

    invoke-static {v3}, Lcom/google/android/gms/plus/b/g;->a(Lcom/google/android/gms/plus/service/pos/j;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 84
    :cond_2
    move-object v0, v3

    check-cast v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    move-object v2, v0

    invoke-static {v2, p3}, Lcom/google/android/gms/plus/b/g;->a(Lcom/google/android/gms/plus/service/pos/PlusonesEntity;Z)V

    .line 85
    move-object v0, v3

    check-cast v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    move-object v2, v0

    if-eqz p3, :cond_4

    const/4 v4, 0x1

    :goto_1
    invoke-static {v2, v4}, Lcom/google/android/gms/plus/b/g;->a(Lcom/google/android/gms/plus/service/pos/PlusonesEntity;I)V

    .line 86
    iget-object v2, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    invoke-virtual {v2, v6, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    :cond_3
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v1

    monitor-exit v5

    throw v1

    .line 85
    :cond_4
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;[B)V
    .locals 2

    .prologue
    .line 167
    iget-object v1, p0, Lcom/google/android/gms/plus/b/f;->d:Landroid/support/v4/g/h;

    monitor-enter v1

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/f;->d:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 143
    iget-object v1, p0, Lcom/google/android/gms/plus/b/f;->c:Landroid/support/v4/g/h;

    monitor-enter v1

    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/f;->c:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/b/h;)V
    .locals 3

    .prologue
    .line 59
    iget-object v1, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    monitor-enter v1

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    invoke-static {p1}, Lcom/google/android/gms/plus/b/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/net/Uri;)[B
    .locals 2

    .prologue
    .line 155
    iget-object v1, p0, Lcom/google/android/gms/plus/b/f;->d:Landroid/support/v4/g/h;

    monitor-enter v1

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/f;->d:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 112
    iget-object v1, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    monitor-enter v1

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/f;->b:Landroid/support/v4/g/h;

    invoke-static {p1}, Lcom/google/android/gms/plus/b/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/g/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
