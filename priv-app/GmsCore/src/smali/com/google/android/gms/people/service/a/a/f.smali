.class public final Lcom/google/android/gms/people/service/a/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/people/service/a/a/f;

.field public static b:Lcom/google/android/gms/people/service/a/a/f;

.field public static c:Lcom/google/android/gms/people/service/a/a/f;


# instance fields
.field public final d:Lcom/google/android/gms/people/service/b;

.field public final e:Lcom/google/android/gms/people/f/m;

.field public final f:I

.field public final g:I

.field public final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    const/4 v0, 0x0

    invoke-static {v0, v1, v1, v1}, Lcom/google/android/gms/people/service/a/a/f;->a(Lcom/google/android/gms/people/f/m;IIZ)Lcom/google/android/gms/people/service/a/a/f;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/service/a/a/f;->a:Lcom/google/android/gms/people/service/a/a/f;

    .line 47
    sget-object v0, Lcom/google/android/gms/people/service/b;->g:Lcom/google/android/gms/people/service/b;

    invoke-static {v0}, Lcom/google/android/gms/people/service/a/a/f;->a(Lcom/google/android/gms/people/service/b;)Lcom/google/android/gms/people/service/a/a/f;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/service/a/a/f;->b:Lcom/google/android/gms/people/service/a/a/f;

    .line 49
    sget-object v0, Lcom/google/android/gms/people/service/b;->f:Lcom/google/android/gms/people/service/b;

    invoke-static {v0}, Lcom/google/android/gms/people/service/a/a/f;->a(Lcom/google/android/gms/people/service/b;)Lcom/google/android/gms/people/service/a/a/f;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/service/a/a/f;->c:Lcom/google/android/gms/people/service/a/a/f;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/people/service/b;Lcom/google/android/gms/people/f/m;IIZ)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/gms/people/service/a/a/f;->d:Lcom/google/android/gms/people/service/b;

    .line 64
    iput-object p2, p0, Lcom/google/android/gms/people/service/a/a/f;->e:Lcom/google/android/gms/people/f/m;

    .line 65
    iput p3, p0, Lcom/google/android/gms/people/service/a/a/f;->f:I

    .line 66
    iput p4, p0, Lcom/google/android/gms/people/service/a/a/f;->g:I

    .line 67
    iput-boolean p5, p0, Lcom/google/android/gms/people/service/a/a/f;->h:Z

    .line 68
    return-void
.end method

.method public static a(Lcom/google/android/gms/people/f/m;IIZ)Lcom/google/android/gms/people/service/a/a/f;
    .locals 6

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/gms/people/service/a/a/f;

    sget-object v1, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/a/f;-><init>(Lcom/google/android/gms/people/service/b;Lcom/google/android/gms/people/f/m;IIZ)V

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/people/service/b;)Lcom/google/android/gms/people/service/a/a/f;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 71
    new-instance v0, Lcom/google/android/gms/people/service/a/a/f;

    const/4 v2, 0x0

    move-object v1, p0

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/a/f;-><init>(Lcom/google/android/gms/people/service/b;Lcom/google/android/gms/people/f/m;IIZ)V

    return-object v0
.end method
