.class final Lcom/google/android/gms/wallet/payform/b;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/payform/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/payform/a;)V
    .locals 0

    .prologue
    .line 1056
    iput-object p1, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1145
    return-void
.end method

.method public final a(Lcom/google/aa/a/a/a/f;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1060
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/aa/a/a/a/f;)V

    .line 1062
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    iput-boolean v2, v0, Lcom/google/android/gms/wallet/payform/a;->l:Z

    .line 1063
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    iput-object p1, v0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    .line 1064
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    if-nez v0, :cond_1

    .line 1096
    :cond_0
    :goto_0
    return-void

    .line 1069
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/android/gms/wallet/payform/a;Lcom/google/aa/a/a/a/f;)V

    .line 1071
    iget-object v0, p1, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    if-nez v0, :cond_2

    .line 1073
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/android/gms/wallet/payform/a;)V

    goto :goto_0

    .line 1077
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/payform/a;->b(Lcom/google/android/gms/wallet/payform/a;Lcom/google/aa/a/a/a/f;)V

    .line 1078
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/payform/a;->c(Lcom/google/android/gms/wallet/payform/a;Lcom/google/aa/a/a/a/f;)V

    .line 1079
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/payform/a;->d(Lcom/google/android/gms/wallet/payform/a;Lcom/google/aa/a/a/a/f;)V

    .line 1081
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    iget-object v0, v0, Lcom/google/android/gms/wallet/payform/a;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1082
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->b(Lcom/google/android/gms/wallet/payform/a;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1083
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1084
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1086
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->c(Lcom/google/android/gms/wallet/payform/a;)V

    .line 1088
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->d(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->e(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1090
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->d(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v1}, Lcom/google/android/gms/wallet/payform/a;->e(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "create_to_ui_populated"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 1092
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v1}, Lcom/google/android/gms/wallet/payform/a;->d(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 1093
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->f(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/i;

    .line 1094
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->g(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/h;

    goto :goto_0
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/j;J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1101
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    iget-object v0, v0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 1103
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 1104
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Got required actions"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    aget v0, v0, v2

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 1108
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    iget-object v0, v0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    iget-object v1, v1, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    .line 1110
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->i(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/gms/wallet/payform/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    invoke-static {v1}, Lcom/google/android/gms/wallet/payform/a;->h(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    iget-object v2, v2, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/payform/f;->a(Lcom/google/aa/b/a/a/a/a/i;Lcom/google/checkout/inapp/proto/j;)V

    .line 1118
    :goto_0
    return-void

    .line 1115
    :cond_0
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Expected a CVV challenge!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1117
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/android/gms/wallet/payform/a;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/l;)V
    .locals 0

    .prologue
    .line 1130
    return-void
.end method

.method public final a(Lcom/google/checkout/b/a/c;)V
    .locals 0

    .prologue
    .line 1140
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1122
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    iget-object v0, v0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 1123
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/b;->a:Lcom/google/android/gms/wallet/payform/a;

    const/16 v1, 0x19d

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/android/gms/wallet/payform/a;I)V

    .line 1124
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1135
    return-void
.end method
