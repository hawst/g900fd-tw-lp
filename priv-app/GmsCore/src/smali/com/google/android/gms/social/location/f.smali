.class public final Lcom/google/android/gms/social/location/f;
.super Lcom/google/android/gms/social/location/a;
.source "SourceFile"


# instance fields
.field private j:Lcom/google/android/gms/social/location/h;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/social/location/a;-><init>()V

    .line 31
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/social/location/f;)Lcom/google/android/gms/social/location/h;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/social/location/f;->j:Lcom/google/android/gms/social/location/h;

    return-object v0
.end method

.method private c()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Landroid/support/v4/app/m;->f:Landroid/app/Dialog;

    .line 79
    if-eqz v0, :cond_0

    .line 80
    sget v1, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/social/location/h;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/gms/social/location/f;->j:Lcom/google/android/gms/social/location/h;

    .line 43
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/f;->b()Landroid/content/Context;

    move-result-object v0

    .line 48
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 49
    sget v2, Lcom/google/android/gms/p;->qR:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 51
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/google/android/gms/l;->cF:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 53
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 55
    sget v0, Lcom/google/android/gms/p;->rv:I

    invoke-virtual {v1, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 57
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onPause()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/android/gms/social/location/a;->onPause()V

    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/social/location/f;->c()Landroid/widget/TextView;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-super {p0}, Lcom/google/android/gms/social/location/a;->onResume()V

    .line 74
    invoke-direct {p0}, Lcom/google/android/gms/social/location/f;->c()Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/f;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v0, Lcom/google/android/gms/p;->zi:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/gms/social/location/o;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->pK:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/gms/p;->qN:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v4, v6, v1

    const/4 v7, 0x1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Spanned;->length()I

    move-result v0

    const-class v6, Landroid/text/style/URLSpan;

    invoke-interface {v5, v1, v0, v6}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    array-length v7, v0

    :goto_0
    if-ge v1, v7, :cond_0

    aget-object v8, v0, v1

    invoke-virtual {v8}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v8}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v10

    invoke-interface {v5, v8}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    new-instance v11, Lcom/google/android/gms/social/location/g;

    invoke-direct {v11, p0, v9, v4}, Lcom/google/android/gms/social/location/g;-><init>(Lcom/google/android/gms/social/location/f;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v9, 0x21

    invoke-virtual {v6, v11, v10, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    sget v0, Lcom/google/android/gms/f;->k:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLinkTextColor(I)V

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 75
    :cond_1
    return-void
.end method
