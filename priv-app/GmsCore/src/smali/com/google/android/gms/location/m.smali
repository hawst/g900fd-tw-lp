.class public final Lcom/google/android/gms/location/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/er;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/location/internal/w;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)V
    .locals 2

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Lcom/google/android/gms/location/internal/w;

    const-string v1, "location"

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/gms/location/internal/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    .line 146
    return-void
.end method

.method public static a(Landroid/content/Intent;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 355
    if-eqz p1, :cond_1

    .line 356
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 358
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    .line 359
    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->k()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 361
    :cond_0
    const-string v0, "com.google.android.location.intent.extra.geofence_list"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 363
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/w;->a()V

    .line 619
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/es;)V
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/internal/w;->a(Lcom/google/android/gms/common/es;)V

    .line 639
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/et;)V
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/internal/w;->a(Lcom/google/android/gms/common/et;)V

    .line 654
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/w;->b()V

    .line 624
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/es;)Z
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/internal/w;->b(Lcom/google/android/gms/common/es;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/gms/common/es;)V
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/internal/w;->c(Lcom/google/android/gms/common/es;)V

    .line 649
    return-void
.end method

.method public final c_()Z
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/w;->c_()Z

    move-result v0

    return v0
.end method

.method public final n_()Z
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/w;->n_()Z

    move-result v0

    return v0
.end method
