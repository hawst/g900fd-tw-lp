.class public Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/o;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/o;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>()V

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;->j:Ljava/lang/String;

    .line 35
    iput p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;->a:I

    .line 36
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>(Landroid/os/Parcel;)V

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;->a:I

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;-><init>(ILjava/lang/String;)V

    .line 46
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 47
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 58
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    return-void
.end method
