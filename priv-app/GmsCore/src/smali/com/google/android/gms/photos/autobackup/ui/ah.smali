.class public final Lcom/google/android/gms/photos/autobackup/ui/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/ui/aj;


# instance fields
.field a:Lcom/google/android/gms/photos/autobackup/ui/ak;

.field private b:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/preference/PreferenceActivity;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/16 v1, 0x10

    const/4 v4, -0x2

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Landroid/widget/Switch;

    invoke-direct {v0, p1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/ah;->b:Landroid/widget/Switch;

    .line 24
    invoke-virtual {p1}, Landroid/preference/PreferenceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 25
    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 26
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/ah;->b:Landroid/widget/Switch;

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const v3, 0x800015

    invoke-direct {v2, v4, v4, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 31
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/ah;->b:Landroid/widget/Switch;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/ai;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/ai;-><init>(Lcom/google/android/gms/photos/autobackup/ui/ah;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 39
    sget v0, Lcom/google/android/gms/p;->sp:I

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 41
    invoke-virtual {p1}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/photos/autobackup/ui/ak;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/ah;->a:Lcom/google/android/gms/photos/autobackup/ui/ak;

    .line 47
    return-void
.end method

.method public final a(Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/ah;->b:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-ne p1, v0, :cond_0

    .line 63
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/ah;->a:Lcom/google/android/gms/photos/autobackup/ui/ak;

    .line 57
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/ah;->a:Lcom/google/android/gms/photos/autobackup/ui/ak;

    .line 59
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/ah;->b:Landroid/widget/Switch;

    invoke-virtual {v1, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 62
    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/ah;->a:Lcom/google/android/gms/photos/autobackup/ui/ak;

    goto :goto_0
.end method
