.class public final Lcom/google/android/gms/wallet/service/ow/y;
.super Lcom/google/android/gms/wallet/service/ow/w;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/wallet/service/ow/ak;

.field private final c:Lcom/google/android/gms/wallet/service/p;

.field private final d:Lcom/google/android/gms/wallet/service/ow/a;

.field private final e:Lcom/google/android/gms/wallet/common/ab;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/ak;Lcom/google/android/gms/wallet/service/ow/a;Lcom/google/android/gms/wallet/common/ab;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/w;-><init>()V

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->a:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/y;->b:Lcom/google/android/gms/wallet/service/ow/ak;

    .line 57
    new-instance v0, Lcom/google/android/gms/wallet/service/p;

    const-string v1, "NetworkOwInternalServic"

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/wallet/service/p;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->c:Lcom/google/android/gms/wallet/service/p;

    .line 58
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/y;->d:Lcom/google/android/gms/wallet/service/ow/a;

    .line 59
    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/y;->e:Lcom/google/android/gms/wallet/common/ab;

    .line 60
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/common/ab;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->e:Lcom/google/android/gms/wallet/common/ab;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/a;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->d:Lcom/google/android/gms/wallet/service/ow/a;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/ak;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->b:Lcom/google/android/gms/wallet/service/ow/ak;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/service/ow/y;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 9

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    sget-object v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    .line 220
    :goto_0
    return-object v0

    .line 175
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->c()Ljava/lang/String;

    move-result-object v6

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->d:Lcom/google/android/gms/wallet/service/ow/a;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/service/ow/a;->a(Ljava/lang/String;)Lcom/google/aa/b/a/a/a/a/b;

    move-result-object v4

    .line 177
    if-nez v4, :cond_1

    .line 178
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    sget-object v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    goto :goto_0

    .line 181
    :cond_1
    const/16 v0, 0x8

    new-array v0, v0, [B

    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    const-wide v2, 0xffffffffffffL

    and-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/16 v1, 0x30

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v7

    .line 182
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    .line 183
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v3

    .line 184
    iget-object v8, p0, Lcom/google/android/gms/wallet/service/ow/y;->c:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ab;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v2

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/wallet/service/ow/ab;-><init>(Lcom/google/android/gms/wallet/service/ow/y;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/i;Lcom/google/aa/b/a/a/a/a/b;Lcom/google/android/gms/wallet/shared/ApplicationParameters;Ljava/lang/String;Landroid/util/Pair;)V

    invoke-virtual {v8, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v2

    .line 220
    new-instance v1, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    iget-object v0, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v4, v5, v2}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(JLcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 250
    :goto_0
    return-object v0

    .line 229
    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ow/y;->c:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ac;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/ac;-><init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 289
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 313
    :goto_0
    return-object v0

    .line 291
    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ow/y;->c:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ae;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/ae;-><init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/y;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 282
    :goto_0
    return-object v0

    .line 259
    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ow/y;->c:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ad;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/ad;-><init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 114
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ow/y;->c:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/aa;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/aa;-><init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 165
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 65
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ow/y;->c:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/z;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/z;-><init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    const/16 v2, 0xf

    if-eq v1, v2, :cond_0

    .line 108
    :goto_0
    return-object v0

    .line 104
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/p;

    .line 105
    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/p;)Lcom/google/aa/a/a/a/f;

    move-result-object v1

    .line 107
    const/4 v0, 0x1

    iput v0, v1, Lcom/google/aa/a/a/a/f;->d:I

    .line 108
    new-instance v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v2, 0x13

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto :goto_0
.end method
