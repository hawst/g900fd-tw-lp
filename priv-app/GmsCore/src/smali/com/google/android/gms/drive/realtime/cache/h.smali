.class final Lcom/google/android/gms/drive/realtime/cache/h;
.super Lcom/google/android/gms/drive/realtime/cache/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/drive/realtime/cache/i;

.field private final c:Lcom/google/c/a/a/b/c/f;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/c/a/a/b/c/f;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/a;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;)V

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/h;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    .line 28
    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/cache/h;->c:Lcom/google/c/a/a/b/c/f;

    .line 29
    return-void
.end method


# virtual methods
.method protected final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 34
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/h;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/h;->c:Lcom/google/c/a/a/b/c/f;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 36
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/h;->c:Lcom/google/c/a/a/b/c/f;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/c/f;->d()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 37
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/h;->c:Lcom/google/c/a/a/b/c/f;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/c/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 39
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 40
    return-void
.end method
