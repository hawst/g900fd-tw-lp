.class public final Lcom/google/android/gms/wallet/common/ui/dc;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/bf;


# instance fields
.field a:Landroid/accounts/Account;

.field b:Lcom/google/android/gms/wallet/common/ui/dd;

.field c:[Ljava/lang/String;

.field d:I

.field public e:Lcom/google/android/gms/wallet/common/ui/de;

.field f:Lcom/google/android/gms/wallet/common/ui/bb;

.field g:Lcom/google/android/gms/wallet/common/ui/df;

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 138
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->d:I

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->h:Z

    .line 404
    return-void
.end method

.method public static varargs a(Landroid/accounts/Account;[Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dc;
    .locals 3

    .prologue
    .line 104
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/dc;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/dc;-><init>()V

    .line 105
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 106
    const-string v2, "account"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 107
    const-string v2, "tokenTypes"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 108
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dc;->setArguments(Landroid/os/Bundle;)V

    .line 109
    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/common/ui/de;->a(I)V

    .line 399
    :goto_0
    return-void

    .line 397
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/df;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/wallet/common/ui/df;-><init>(IB)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->g:Lcom/google/android/gms/wallet/common/ui/df;

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/wallet/common/ui/bb;)V
    .locals 3

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 384
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 387
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment.ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 390
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->c:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 248
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(I)V

    .line 257
    :goto_0
    return-void

    .line 250
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 251
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "No TokenTypes remain"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->d:I

    .line 256
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->f()V

    goto :goto_0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 272
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->d:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->c:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->d:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 283
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/dd;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/wallet/common/ui/dd;-><init>(Lcom/google/android/gms/wallet/common/ui/dc;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->b:Lcom/google/android/gms/wallet/common/ui/dd;

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->b:Lcom/google/android/gms/wallet/common/ui/dd;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dd;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 286
    return-void
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Lcom/google/android/gms/wallet/common/ui/bb;)V

    .line 375
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 350
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_1

    .line 351
    packed-switch p1, :pswitch_data_0

    .line 369
    :goto_0
    return-void

    .line 353
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/de;->f()V

    goto :goto_0

    .line 356
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/df;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/common/ui/df;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->g:Lcom/google/android/gms/wallet/common/ui/df;

    goto :goto_0

    .line 361
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->f()V

    goto :goto_0

    .line 364
    :cond_1
    const/16 v0, 0x3ea

    if-ne p2, v0, :cond_2

    .line 365
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(I)V

    goto :goto_0

    .line 367
    :cond_2
    const-string v0, "RetrieveAuthTokensFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/de;)V
    .locals 1

    .prologue
    .line 149
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->h:Z

    .line 150
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    .line 151
    return-void

    .line 149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 330
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->f()V

    .line 346
    :goto_0
    return-void

    .line 333
    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->a:Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/wallet/a/a;->b:Ljava/util/HashSet;

    monitor-enter v3

    :try_start_0
    iget-object v4, v0, Lcom/google/android/gms/wallet/a/a;->b:Ljava/util/HashSet;

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/a/a;->b(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/google/android/gms/wallet/a/a;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 336
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->c()V

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    if-eqz v0, :cond_2

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/de;->e()V

    goto :goto_0

    .line 341
    :cond_2
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/df;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/common/ui/df;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->g:Lcom/google/android/gms/wallet/common/ui/df;

    goto :goto_0
.end method

.method final b()V
    .locals 1

    .prologue
    .line 379
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/bb;->d()Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Lcom/google/android/gms/wallet/common/ui/bb;)V

    .line 380
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 208
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 211
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->d:I

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    .line 212
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->c()V

    .line 214
    :cond_0
    return-void

    .line 211
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x2

    .line 290
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 292
    const/16 v1, 0x1f5

    if-ne p1, v1, :cond_0

    .line 293
    packed-switch p2, :pswitch_data_0

    .line 320
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(I)V

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 295
    :pswitch_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 296
    :goto_1
    if-eqz v1, :cond_1

    const-string v0, "authtoken"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 298
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 295
    goto :goto_1

    .line 302
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    if-eqz v0, :cond_3

    .line 303
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/de;->f()V

    goto :goto_0

    .line 305
    :cond_3
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/df;

    invoke-direct {v0, v2}, Lcom/google/android/gms/wallet/common/ui/df;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->g:Lcom/google/android/gms/wallet/common/ui/df;

    goto :goto_0

    .line 310
    :pswitch_3
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(I)V

    goto :goto_0

    .line 313
    :pswitch_4
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/common/ui/dc;->a(I)V

    goto :goto_0

    .line 293
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 155
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 160
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->h:Z

    if-nez v1, :cond_0

    .line 162
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/de;

    move-object v1, v0

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :cond_0
    return-void

    .line 164
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement OnAuthTokensRetrievedListener or a listener must be explicitly set"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 186
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 187
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dc;->setRetainInstance(Z)V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 190
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "Fragment requires arguments!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Fragment requires account extra!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 194
    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->a:Landroid/accounts/Account;

    .line 196
    const-string v0, "tokenTypes"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Fragment requires tokenTypes extra!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 200
    new-instance v0, Ljava/util/HashSet;

    const-string v2, "tokenTypes"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->c:[Ljava/lang/String;

    .line 203
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->a:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dc;->c:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/accounts/Account;[Ljava/lang/String;)V

    .line 204
    return-void
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 179
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    .line 182
    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 218
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 219
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dc;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "RetrieveAuthTokensFragment.ERROR_DIALOG"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->f:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->g:Lcom/google/android/gms/wallet/common/ui/df;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->g:Lcom/google/android/gms/wallet/common/ui/df;

    iget v0, v0, Lcom/google/android/gms/wallet/common/ui/df;->a:I

    packed-switch v0, :pswitch_data_0

    .line 236
    const-string v0, "RetrieveAuthTokensFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown RetrieveAuthTokensResult: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dc;->g:Lcom/google/android/gms/wallet/common/ui/df;

    iget v2, v2, Lcom/google/android/gms/wallet/common/ui/df;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->g:Lcom/google/android/gms/wallet/common/ui/df;

    .line 241
    :cond_1
    return-void

    .line 227
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/de;->e()V

    goto :goto_0

    .line 230
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/de;->f()V

    goto :goto_0

    .line 233
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dc;->g:Lcom/google/android/gms/wallet/common/ui/df;

    iget v1, v1, Lcom/google/android/gms/wallet/common/ui/df;->b:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/de;->a(I)V

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dc;->e:Lcom/google/android/gms/wallet/common/ui/de;

    .line 175
    return-void
.end method
