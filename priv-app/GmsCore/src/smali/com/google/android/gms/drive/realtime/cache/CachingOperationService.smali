.class public Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;
.super Lcom/google/android/gms/common/service/c;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/common/service/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/common/service/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/service/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->b:Lcom/google/android/gms/common/service/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    const-string v0, "CachingOperationService"

    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->b:Lcom/google/android/gms/common/service/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/service/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/service/d;)V

    .line 29
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->b:Lcom/google/android/gms/common/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/service/d;->add(Ljava/lang/Object;)Z

    .line 24
    const-string v0, "com.google.android.gms.realtime.caching.EXECUTE"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 25
    return-void
.end method
