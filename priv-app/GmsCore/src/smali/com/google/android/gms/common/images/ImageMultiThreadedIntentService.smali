.class public final Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;
.super Lcom/google/android/gms/common/app/a;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/concurrent/ConcurrentLinkedQueue;


# instance fields
.field private b:Lcom/google/android/gms/common/images/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/app/a;-><init>(I)V

    .line 36
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;ILandroid/os/ResultReceiver;)V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/google/android/gms/common/images/i;

    invoke-direct {v0, p1, p3, p2}, Lcom/google/android/gms/common/images/i;-><init>(Landroid/net/Uri;Landroid/os/ResultReceiver;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/images/h;)V

    .line 127
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 138
    new-instance v0, Lcom/google/android/gms/common/images/j;

    const/4 v1, 0x3

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/gms/common/images/j;-><init>(Landroid/net/Uri;Ljava/util/ArrayList;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/images/h;)V

    .line 139
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/images/h;)V
    .locals 2

    .prologue
    .line 142
    sget-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 143
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 144
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/h;

    .line 47
    if-nez v0, :cond_0

    .line 48
    const-string v0, "ImageMultiThreadedInten"

    const-string v1, "No operation found when processing!"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->b:Lcom/google/android/gms/common/images/a;

    invoke-interface {v0, p0, v1}, Lcom/google/android/gms/common/images/h;->a(Landroid/content/Context;Lcom/google/android/gms/common/images/a;)V

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lcom/google/android/gms/common/app/a;->onCreate()V

    .line 41
    invoke-static {p0}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->b:Lcom/google/android/gms/common/images/a;

    .line 42
    return-void
.end method
