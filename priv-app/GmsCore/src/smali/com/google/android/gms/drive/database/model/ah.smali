.class public final Lcom/google/android/gms/drive/database/model/ah;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field private final b:Lcom/google/android/gms/drive/database/model/ao;

.field private final c:Lcom/google/android/gms/drive/database/i;

.field private final d:Lcom/google/android/gms/drive/database/r;

.field private e:Lcom/google/android/gms/drive/database/model/w;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/drive/database/model/ah;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;JLjava/lang/String;Lcom/google/android/gms/drive/database/t;)V
    .locals 7

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/gms/drive/database/model/ao;

    invoke-interface {p6}, Lcom/google/android/gms/drive/database/t;->a()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x1

    move-object v1, p1

    move-wide v2, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/ao;-><init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;J)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/drive/database/model/ah;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ao;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p5}, Lcom/google/android/gms/drive/database/model/ao;->b(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;JLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 94
    new-instance v0, Lcom/google/android/gms/drive/database/model/ao;

    const-wide/16 v5, 0x0

    move-object v1, p1

    move-wide v2, p3

    move-object v4, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/ao;-><init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;J)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/drive/database/model/ah;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ao;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p5}, Lcom/google/android/gms/drive/database/model/ao;->b(Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 112
    invoke-static {p1, p3}, Lcom/google/android/gms/drive/database/model/ao;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/ao;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/drive/database/model/ah;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ao;)V

    .line 113
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ao;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    .line 72
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/ah;->d:Lcom/google/android/gms/drive/database/r;

    .line 73
    iput-object p3, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    .line 74
    return-void
.end method

.method public static a(Ljava/lang/String;Z)I
    .locals 2

    .prologue
    .line 307
    invoke-static {p0}, Lcom/google/android/gms/drive/database/model/an;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/an;

    move-result-object v0

    .line 309
    sget-object v1, Lcom/google/android/gms/drive/database/model/an;->d:Lcom/google/android/gms/drive/database/model/an;

    if-ne v0, v1, :cond_0

    if-eqz p0, :cond_0

    .line 310
    invoke-static {p0}, Lcom/google/android/gms/drive/database/model/al;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/al;

    move-result-object v1

    .line 311
    if-eqz v1, :cond_0

    .line 312
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/al;->a()I

    move-result v0

    .line 316
    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/an;->b()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/an;->a()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 1

    .prologue
    .line 346
    new-instance v0, Lcom/google/android/gms/drive/database/model/ah;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/drive/database/model/ah;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Landroid/database/Cursor;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 285
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    invoke-static {p0}, Lcom/google/android/gms/drive/database/model/an;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/an;

    move-result-object v0

    .line 288
    sget-object v1, Lcom/google/android/gms/drive/database/model/an;->d:Lcom/google/android/gms/drive/database/model/an;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/an;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 289
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/an;->c()I

    move-result v0

    .line 295
    :goto_0
    return v0

    .line 291
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/drive/database/model/al;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/al;

    move-result-object v0

    .line 292
    if-eqz v0, :cond_1

    .line 293
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/al;->b()I

    move-result v0

    goto :goto_0

    .line 295
    :cond_1
    sget v0, Lcom/google/android/gms/p;->gq:I

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/drive/UserMetadata;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 358
    if-nez p1, :cond_0

    .line 359
    const/4 v0, 0x0

    .line 362
    :goto_0
    return-object v0

    .line 361
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/database/model/ce;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/drive/database/model/ce;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/UserMetadata;)V

    .line 362
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->d:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ce;)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Ljava/lang/Long;)Lcom/google/android/gms/drive/UserMetadata;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 351
    if-nez p1, :cond_0

    move-object v5, v0

    .line 353
    :goto_0
    if-nez v5, :cond_1

    :goto_1
    return-object v0

    .line 351
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->d:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/drive/database/r;->e(J)Lcom/google/android/gms/drive/database/model/ce;

    move-result-object v1

    move-object v5, v1

    goto :goto_0

    .line 353
    :cond_1
    new-instance v0, Lcom/google/android/gms/drive/UserMetadata;

    iget-object v1, v5, Lcom/google/android/gms/drive/database/model/ce;->a:Ljava/lang/String;

    iget-object v2, v5, Lcom/google/android/gms/drive/database/model/ce;->b:Ljava/lang/String;

    iget-object v3, v5, Lcom/google/android/gms/drive/database/model/ce;->c:Ljava/lang/String;

    iget-boolean v4, v5, Lcom/google/android/gms/drive/database/model/ce;->d:Z

    iget-object v5, v5, Lcom/google/android/gms/drive/database/model/ce;->g:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/UserMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->D()Z

    move-result v0

    return v0
.end method

.method public final B()J
    .locals 2

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->U()J

    move-result-wide v0

    return-wide v0
.end method

.method public final C()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->u()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->V()Z

    move-result v0

    return v0
.end method

.method public final E()Z
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->E()Z

    move-result v0

    return v0
.end method

.method public final F()Ljava/util/Date;
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->h()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final G()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->n()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final H()Ljava/util/Date;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->c()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final I()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->m()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final J()Ljava/util/Date;
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->l()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 1

    .prologue
    .line 661
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->ae()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final N()Z
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->q()Z

    move-result v0

    return v0
.end method

.method public final O()Z
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->s()Z

    move-result v0

    return v0
.end method

.method public final P()Z
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->r()Z

    move-result v0

    return v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final R()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->g()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->F()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final T()Z
    .locals 1

    .prologue
    .line 732
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->G()Z

    move-result v0

    return v0
.end method

.method public final U()Ljava/lang/String;
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->H()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->I()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final W()Z
    .locals 1

    .prologue
    .line 756
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->K()Z

    move-result v0

    return v0
.end method

.method public final X()Z
    .locals 1

    .prologue
    .line 764
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->L()Z

    move-result v0

    return v0
.end method

.method public final Y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->M()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Z()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->N()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/drive/database/model/EntrySpec;
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    .line 121
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 122
    const/4 v0, 0x0

    .line 124
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;
    .locals 8

    .prologue
    .line 147
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    new-instance v1, Lcom/google/android/gms/drive/metadata/internal/a;

    invoke-direct {v1}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>()V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->d:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/y;

    .line 150
    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/y;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/metadata/internal/a;->a(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;)Lcom/google/android/gms/drive/metadata/internal/a;

    goto :goto_0

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->e:Lcom/google/android/gms/drive/database/model/w;

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/drive/database/model/ah;->e:Lcom/google/android/gms/drive/database/model/w;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/a;->a()Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/metadata/internal/a;

    invoke-direct {v1}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>()V

    iget-boolean v3, v2, Lcom/google/android/gms/drive/database/model/w;->d:Z

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    iget-object v4, v2, Lcom/google/android/gms/drive/database/model/w;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v5

    iget-wide v6, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/drive/database/model/w;->a(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/metadata/internal/a;->a(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;)Lcom/google/android/gms/drive/metadata/internal/a;

    goto :goto_1

    :cond_2
    iget-object v0, v2, Lcom/google/android/gms/drive/database/model/w;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/x;

    iget-object v3, v0, Lcom/google/android/gms/drive/database/model/x;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/gms/drive/database/model/x;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-wide v6, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/x;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/metadata/internal/a;->a(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;)Lcom/google/android/gms/drive/metadata/internal/a;

    goto :goto_2

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/a;->a()Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v0

    :goto_3
    return-object v0

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/a;->a()Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v0

    goto :goto_3
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/drive/database/model/ao;->a(J)V

    .line 528
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/UserMetadata;)V
    .locals 2

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/model/ah;->c(Lcom/google/android/gms/drive/UserMetadata;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ao;->e(Ljava/lang/Long;)V

    .line 818
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;Ljava/lang/Long;)V
    .locals 4

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->e:Lcom/google/android/gms/drive/database/model/w;

    if-nez v0, :cond_0

    .line 165
    new-instance v0, Lcom/google/android/gms/drive/database/model/w;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/model/ah;->d:Lcom/google/android/gms/drive/database/r;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/w;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Z)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->e:Lcom/google/android/gms/drive/database/model/w;

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->e:Lcom/google/android/gms/drive/database/model/w;

    new-instance v1, Lcom/google/android/gms/drive/database/model/x;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/drive/database/model/x;-><init>(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;Ljava/lang/Long;)V

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/w;->c:Ljava/util/Map;

    invoke-static {v1}, Lcom/google/android/gms/drive/database/model/x;->a(Lcom/google/android/gms/drive/database/model/x;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    return-void
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 665
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ao;->l(Z)V

    .line 666
    return-void
.end method

.method public final a(Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->c(Ljava/lang/Long;)V

    .line 596
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 137
    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v4, v0}, Lcom/google/android/gms/drive/database/model/ao;->a(Ljava/lang/String;)V

    .line 138
    return-void

    .line 137
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->getType(I)I

    move-result v2

    const/16 v6, 0xf

    if-eq v2, v6, :cond_1

    const/16 v6, 0x10

    if-eq v2, v6, :cond_1

    const/16 v6, 0xd

    if-eq v2, v6, :cond_1

    const/16 v6, 0xe

    if-eq v2, v6, :cond_1

    const/16 v6, 0xc

    if-eq v2, v6, :cond_1

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_2

    move v2, v3

    :goto_3
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_1

    :cond_1
    move v2, v1

    goto :goto_2

    :cond_2
    const/16 v2, 0x20

    goto :goto_3

    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/drive/database/model/ao;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 696
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->a(Ljava/util/Collection;)V

    .line 721
    return-void
.end method

.method public final a(Ljava/util/Date;)V
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->b(Ljava/util/Date;)V

    .line 612
    return-void
.end method

.method public final a(Z)V
    .locals 19

    .prologue
    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 233
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ao;->U()J

    move-result-wide v14

    .line 234
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/i;->f()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/drive/database/model/ao;->c(J)V

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ao;->i()V

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    iget-wide v2, v2, Lcom/google/android/gms/drive/database/model/ad;->f:J

    .line 239
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->e:Lcom/google/android/gms/drive/database/model/w;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/drive/database/model/ah;->e:Lcom/google/android/gms/drive/database/model/w;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v11, Lcom/google/android/gms/drive/database/model/w;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, v11, Lcom/google/android/gms/drive/database/model/w;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/i;->c()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    iget-object v2, v11, Lcom/google/android/gms/drive/database/model/w;->b:Lcom/google/android/gms/drive/database/r;

    const/4 v3, 0x0

    invoke-interface {v2, v5, v3}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/database/model/y;

    invoke-static {v2}, Lcom/google/android/gms/drive/database/model/w;->a(Lcom/google/android/gms/drive/database/model/y;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    iget-object v3, v11, Lcom/google/android/gms/drive/database/model/w;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v2
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 242
    :catch_0
    move-exception v2

    .line 243
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v3, v14, v15}, Lcom/google/android/gms/drive/database/model/ao;->c(J)V

    .line 244
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 249
    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v2

    .line 239
    :cond_0
    :try_start_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->size()I

    move-result v2

    int-to-long v12, v2

    iget-object v2, v11, Lcom/google/android/gms/drive/database/model/w;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/database/model/x;

    iget-object v0, v2, Lcom/google/android/gms/drive/database/model/x;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    move-object/from16 v18, v0

    invoke-static {v2}, Lcom/google/android/gms/drive/database/model/x;->a(Lcom/google/android/gms/drive/database/model/x;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/android/gms/drive/database/model/y;

    move-object v10, v0

    iget-object v4, v11, Lcom/google/android/gms/drive/database/model/w;->a:Lcom/google/android/gms/drive/database/i;

    if-eqz v10, :cond_2

    iget-wide v8, v10, Lcom/google/android/gms/drive/database/model/ad;->f:J

    :goto_2
    new-instance v3, Lcom/google/android/gms/drive/database/model/y;

    iget-object v6, v2, Lcom/google/android/gms/drive/database/model/x;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    iget-object v7, v2, Lcom/google/android/gms/drive/database/model/x;->b:Ljava/lang/Long;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/drive/database/model/y;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/CustomProperty;Ljava/lang/Long;J)V

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    if-eqz v10, :cond_1

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v4, v10, Lcom/google/android/gms/drive/database/model/y;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/y;->i()V

    if-nez v10, :cond_b

    const-wide/16 v2, 0x1

    add-long/2addr v12, v2

    goto :goto_1

    :cond_2
    const-wide/16 v8, -0x1

    goto :goto_2

    :cond_3
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_b

    if-eqz v10, :cond_b

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/y;->j()V

    const-wide/16 v2, 0x1

    sub-long v2, v12, v2

    :goto_3
    move-wide v12, v2

    goto :goto_1

    :cond_4
    iget-boolean v2, v11, Lcom/google/android/gms/drive/database/model/w;->d:Z

    if-eqz v2, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v6, v12

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/database/model/y;

    iget-object v3, v11, Lcom/google/android/gms/drive/database/model/w;->c:Ljava/util/Map;

    invoke-static {v2}, Lcom/google/android/gms/drive/database/model/w;->a(Lcom/google/android/gms/drive/database/model/y;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_a

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/y;->j()V

    const-wide/16 v2, 0x1

    sub-long v2, v6, v2

    :goto_5
    move-wide v6, v2

    goto :goto_4

    :cond_5
    move-wide v12, v6

    :cond_6
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    sget-object v2, Lcom/google/android/gms/drive/database/model/as;->S:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v3, v11, Lcom/google/android/gms/drive/database/model/w;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/drive/database/i;->a(JLcom/google/android/gms/drive/database/model/ae;Landroid/content/ContentValues;Landroid/net/Uri;)V

    iget-object v2, v11, Lcom/google/android/gms/drive/database/model/w;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v2, v11, Lcom/google/android/gms/drive/database/model/w;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/i;->d()V

    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->e:Lcom/google/android/gms/drive/database/model/w;

    .line 240
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 241
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 251
    if-eqz p1, :cond_9

    .line 252
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/drive/database/model/ah;->e()V

    .line 254
    :cond_9
    return-void

    .line 245
    :catch_1
    move-exception v2

    .line 246
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v3, v14, v15}, Lcom/google/android/gms/drive/database/model/ao;->c(J)V

    .line 247
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_a
    move-wide v2, v6

    goto :goto_5

    :cond_b
    move-wide v2, v12

    goto/16 :goto_3
.end method

.method public final aa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->P()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 796
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->Q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ac()Z
    .locals 1

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->R()Z

    move-result v0

    return v0
.end method

.method public final ad()Lcom/google/android/gms/drive/UserMetadata;
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->aj()Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ah;->d(Ljava/lang/Long;)Lcom/google/android/gms/drive/UserMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final ae()Lcom/google/android/gms/drive/UserMetadata;
    .locals 1

    .prologue
    .line 822
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->ai()Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ah;->d(Ljava/lang/Long;)Lcom/google/android/gms/drive/UserMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final af()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->af()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final ag()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->ag()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final ah()Ljava/lang/String;
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->ad()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ai()I
    .locals 1

    .prologue
    .line 852
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->v()I

    move-result v0

    return v0
.end method

.method public final aj()Z
    .locals 1

    .prologue
    .line 856
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->t()Z

    move-result v0

    return v0
.end method

.method public final ak()Z
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->p()Z

    move-result v0

    return v0
.end method

.method public final al()J
    .locals 2

    .prologue
    .line 868
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public final am()Z
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->x()Z

    move-result v0

    return v0
.end method

.method public final an()Z
    .locals 1

    .prologue
    .line 880
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->w()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 178
    new-instance v0, Lcom/google/android/gms/drive/database/model/w;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/model/ah;->d:Lcom/google/android/gms/drive/database/r;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/w;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Z)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->e:Lcom/google/android/gms/drive/database/model/w;

    .line 179
    return-void
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 784
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/drive/database/model/ao;->b(J)V

    .line 785
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/UserMetadata;)V
    .locals 2

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/model/ah;->c(Lcom/google/android/gms/drive/UserMetadata;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ao;->d(Ljava/lang/Long;)V

    .line 827
    return-void
.end method

.method public final b(Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->b(Ljava/lang/Long;)V

    .line 620
    return-void
.end method

.method public final b(Ljava/util/Date;)V
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->a(Ljava/util/Date;)V

    .line 628
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->k(Z)V

    .line 568
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/auth/g;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 375
    .line 376
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v4, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->b()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    .line 380
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 414
    :goto_0
    return v0

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->d:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, p1, v3}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 388
    if-nez v0, :cond_2

    move v0, v1

    .line 389
    goto :goto_0

    :cond_1
    move-object v0, p0

    .line 394
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ao;->w()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 395
    iget-object v3, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v4, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->u()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    move v0, v2

    .line 397
    goto :goto_0

    :cond_3
    move v0, v1

    .line 399
    goto :goto_0

    .line 403
    :cond_4
    iget-object v3, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v4, Lcom/google/android/gms/drive/aa;->c:Lcom/google/android/gms/drive/aa;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v2

    .line 404
    goto :goto_0

    .line 408
    :cond_5
    iget-object v3, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v4, Lcom/google/android/gms/drive/aa;->a:Lcom/google/android/gms/drive/aa;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 409
    iget-object v3, p0, Lcom/google/android/gms/drive/database/model/ah;->d:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;)Ljava/util/Set;

    move-result-object v0

    .line 410
    if-eqz v0, :cond_6

    iget-wide v4, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 411
    goto :goto_0

    :cond_6
    move v0, v1

    .line 414
    goto :goto_0
.end method

.method public final c(Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->a(Ljava/lang/Long;)V

    .line 636
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 434
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->p()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 436
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->T()V

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->t(Ljava/lang/String;)V

    .line 438
    return-void
.end method

.method public final c(Ljava/util/Date;)V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->c(Ljava/util/Date;)V

    .line 645
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->e(Z)V

    .line 580
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->ad()Ljava/lang/String;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_0

    const-string v1, "owner"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->w()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->p(Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->f(Z)V

    .line 670
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ao;->W()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 202
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ao;->X()Z

    move-result v1

    if-nez v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ao;->s()Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ao;->x()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 271
    const/4 v0, 0x1

    .line 272
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ao;->Y()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 273
    const/4 v0, 0x3

    .line 274
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/model/ao;->m(Z)V

    .line 276
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    invoke-static {p0, v1}, Lcom/google/android/gms/drive/database/q;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/i;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    .line 277
    new-instance v2, Lcom/google/android/gms/drive/events/ChangeEvent;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/drive/events/ChangeEvent;-><init>(Lcom/google/android/gms/drive/DriveId;I)V

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->C()Lcom/google/android/gms/drive/events/k;

    move-result-object v0

    invoke-interface {v0, v2, p0}, Lcom/google/android/gms/drive/events/k;->a(Lcom/google/android/gms/drive/events/ChangeEvent;Lcom/google/android/gms/drive/database/model/ah;)V

    .line 279
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->q(Ljava/lang/String;)V

    .line 488
    return-void
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 681
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->b(Z)V

    .line 682
    return-void
.end method

.method public final f()Lcom/google/android/gms/drive/DriveId;
    .locals 6

    .prologue
    .line 339
    new-instance v0, Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    iget-wide v2, v2, Lcom/google/android/gms/drive/database/model/ad;->f:J

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ah;->c:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/i;->g()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/DriveId;-><init>(Ljava/lang/String;JJ)V

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->o(Ljava/lang/String;)V

    .line 496
    return-void
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->c(Z)V

    .line 686
    return-void
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->d(Ljava/lang/String;)V

    .line 504
    return-void
.end method

.method public final g(Z)V
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->g(Z)V

    .line 737
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->j()V

    .line 445
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->j(Ljava/lang/String;)V

    .line 512
    return-void
.end method

.method public final h(Z)V
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->h(Z)V

    .line 761
    return-void
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    return-wide v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->n(Ljava/lang/String;)V

    .line 520
    return-void
.end method

.method public final i(Z)V
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->i(Z)V

    .line 769
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->ah()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->s(Ljava/lang/String;)V

    .line 592
    return-void
.end method

.method public final j(Z)V
    .locals 1

    .prologue
    .line 808
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->j(Z)V

    .line 809
    return-void
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->ab()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->e(Ljava/lang/String;)V

    .line 649
    return-void
.end method

.method public final k(Z)V
    .locals 1

    .prologue
    .line 840
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->n(Z)V

    .line 841
    return-void
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->ac()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->b(Ljava/lang/String;)V

    .line 657
    return-void
.end method

.method public final l(Z)V
    .locals 1

    .prologue
    .line 860
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->d(Z)V

    .line 861
    return-void
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->aa()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->m(Ljava/lang/String;)V

    .line 678
    return-void
.end method

.method public final m(Z)V
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->a(Z)V

    .line 877
    return-void
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->c(Ljava/lang/String;)V

    .line 690
    return-void
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->O()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 728
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->f(Ljava/lang/String;)V

    .line 729
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->Z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->g(Ljava/lang/String;)V

    .line 745
    return-void
.end method

.method public final q()J
    .locals 2

    .prologue
    .line 523
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->J()J

    move-result-wide v0

    return-wide v0
.end method

.method public final q(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->h(Ljava/lang/String;)V

    .line 753
    return-void
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ao;->m(Z)V

    .line 532
    return-void
.end method

.method public final r(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->i(Ljava/lang/String;)V

    .line 777
    return-void
.end method

.method public final s(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->k(Ljava/lang/String;)V

    .line 793
    return-void
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->C()Z

    move-result v0

    return v0
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->B()V

    .line 540
    return-void
.end method

.method public final t(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->l(Ljava/lang/String;)V

    .line 801
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 332
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Entry for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 848
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ao;->r(Ljava/lang/String;)V

    .line 849
    return-void
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->A()Z

    move-result v0

    return v0
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->z()V

    .line 548
    return-void
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->y()V

    .line 556
    return-void
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->W()Z

    move-result v0

    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->S()Z

    move-result v0

    return v0
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ah;->b:Lcom/google/android/gms/drive/database/model/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ao;->k()Z

    move-result v0

    return v0
.end method
