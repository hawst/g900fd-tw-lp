.class final Lcom/google/android/gms/social/location/y;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V
    .locals 0

    .prologue
    .line 621
    iput-object p1, p0, Lcom/google/android/gms/social/location/y;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;B)V
    .locals 0

    .prologue
    .line 621
    invoke-direct {p0, p1}, Lcom/google/android/gms/social/location/y;-><init>(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 621
    check-cast p1, [Lcom/google/android/gms/social/location/TimeShare;

    new-instance v0, Lcom/google/android/gms/social/location/an;

    iget-object v1, p0, Lcom/google/android/gms/social/location/y;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->b(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/social/location/y;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/social/location/an;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    aget-object v1, p1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/an;->a(Lcom/google/android/gms/social/location/TimeShare;)Z

    move-result v0

    if-eqz v0, :cond_0

    aget-object v0, p1, v3

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 621
    check-cast p1, Lcom/google/android/gms/social/location/TimeShare;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/social/location/y;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->h(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/aa;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/social/location/aa;->e:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/social/location/aa;->d(II)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/social/location/y;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->i(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;)Lcom/google/android/gms/social/location/TimeShare;

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/y;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    const-string v1, "req_pending"

    invoke-static {v0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v5, :cond_1

    iput-boolean v4, v0, Lcom/google/android/gms/social/location/aa;->f:Z

    :cond_1
    invoke-virtual {v0, v2}, Lcom/google/android/gms/social/location/aa;->e(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/social/location/y;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    sget v1, Lcom/google/android/gms/p;->qu:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method protected final onPreExecute()V
    .locals 2

    .prologue
    .line 625
    iget-object v0, p0, Lcom/google/android/gms/social/location/y;->a:Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;

    sget v1, Lcom/google/android/gms/p;->qv:I

    invoke-static {v0, v1}, Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;->a(Lcom/google/android/gms/social/location/LocationSharingSettingsActivity;I)V

    .line 626
    return-void
.end method
