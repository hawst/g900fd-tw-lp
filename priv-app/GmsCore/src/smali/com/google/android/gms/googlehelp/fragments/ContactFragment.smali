.class public Lcom/google/android/gms/googlehelp/fragments/ContactFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:Ljava/util/Set;

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I


# instance fields
.field private f:Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

.field private g:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/TextView;

.field private k:[Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:[Landroid/widget/ImageView;

.field private o:[Landroid/widget/TextView;

.field private p:[Landroid/widget/TextView;

.field private q:[Landroid/widget/TextView;

.field private r:[Landroid/widget/TextView;

.field private s:[Landroid/widget/TextView;

.field private t:[Landroid/widget/TextView;

.field private u:Ljava/util/Map;

.field private v:Ljava/util/Map;

.field private w:Z

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 43
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "UNKNOWN_CONTACT_MODE"

    aput-object v1, v0, v5

    const-string v1, "CHAT"

    aput-object v1, v0, v4

    const-string v1, "PHONE"

    aput-object v1, v0, v6

    const/4 v1, 0x3

    const-string v2, "EMAIL"

    aput-object v2, v0, v1

    const-string v1, "HANGOUTS"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "ENTERPRISE_SUPPORT"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "C2C"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "FEEDBACK_MODE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a:[Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v6, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->b:Ljava/util/Set;

    .line 58
    const/16 v0, 0x8

    new-array v0, v0, [I

    aput v3, v0, v5

    sget v1, Lcom/google/android/gms/h;->aN:I

    aput v1, v0, v4

    sget v1, Lcom/google/android/gms/h;->aI:I

    aput v1, v0, v6

    const/4 v1, 0x3

    sget v2, Lcom/google/android/gms/h;->aP:I

    aput v2, v0, v1

    sget v1, Lcom/google/android/gms/h;->aW:I

    aput v1, v0, v7

    const/4 v1, 0x5

    aput v3, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/google/android/gms/h;->aK:I

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/google/android/gms/h;->aR:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->c:[I

    .line 70
    const/4 v0, 0x7

    new-array v0, v0, [I

    aput v3, v0, v5

    sget v1, Lcom/google/android/gms/h;->aM:I

    aput v1, v0, v4

    sget v1, Lcom/google/android/gms/h;->aJ:I

    aput v1, v0, v6

    const/4 v1, 0x3

    sget v2, Lcom/google/android/gms/h;->aQ:I

    aput v2, v0, v1

    sget v1, Lcom/google/android/gms/h;->aV:I

    aput v1, v0, v7

    const/4 v1, 0x5

    aput v3, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/google/android/gms/h;->aL:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->d:[I

    .line 82
    const/16 v0, 0x8

    new-array v0, v0, [I

    aput v3, v0, v5

    sget v1, Lcom/google/android/gms/p;->oR:I

    aput v1, v0, v4

    sget v1, Lcom/google/android/gms/p;->oX:I

    aput v1, v0, v6

    const/4 v1, 0x3

    sget v2, Lcom/google/android/gms/p;->oT:I

    aput v2, v0, v1

    sget v1, Lcom/google/android/gms/p;->oY:I

    aput v1, v0, v7

    const/4 v1, 0x5

    aput v3, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/google/android/gms/p;->oS:I

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/google/android/gms/p;->oU:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->e:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 102
    new-array v0, v1, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->n:[Landroid/widget/ImageView;

    .line 103
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->o:[Landroid/widget/TextView;

    .line 104
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->p:[Landroid/widget/TextView;

    .line 105
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->q:[Landroid/widget/TextView;

    .line 106
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->r:[Landroid/widget/TextView;

    .line 107
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->s:[Landroid/widget/TextView;

    .line 108
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->t:[Landroid/widget/TextView;

    return-void
.end method

.method private a(IIZ)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->n:[Landroid/widget/ImageView;

    aget-object v1, v0, p1

    if-eqz p3, :cond_0

    sget-object v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->d:[I

    aget v0, v0, p2

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 308
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->o:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    sget-object v1, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->e:[I

    aget v1, v1, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;I)V

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->u:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ad/a/a/p;

    .line 312
    invoke-direct {p0, p2}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(I)Z

    move-result v1

    .line 313
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(IZLcom/google/ad/a/a/p;)V

    .line 317
    if-eqz p3, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->B()Ljava/lang/String;

    move-result-object v1

    .line 320
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 321
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->r:[Landroid/widget/TextView;

    aget-object v4, v4, p1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->r:[Landroid/widget/TextView;

    aget-object v1, v1, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 329
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->s:[Landroid/widget/TextView;

    aget-object v1, v1, p1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 332
    if-eqz v0, :cond_3

    iget-object v1, v0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, "\n"

    invoke-static {v1}, Lcom/google/k/a/x;->a(Ljava/lang/String;)Lcom/google/k/a/x;

    move-result-object v1

    iget-object v0, v0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/k/a/x;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 335
    :goto_3
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->t:[Landroid/widget/TextView;

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->t:[Landroid/widget/TextView;

    aget-object v1, v1, p1

    iget-boolean v4, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->x:Z

    if-eqz v4, :cond_4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v0, v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v0, v0, p1

    new-instance v1, Lcom/google/android/gms/googlehelp/fragments/b;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/googlehelp/fragments/b;-><init>(Lcom/google/android/gms/googlehelp/fragments/ContactFragment;II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->v:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->g:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v1, "SHOWN_CONTACT_US"

    sget-object v2, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;I)V

    .line 352
    return-void

    .line 305
    :cond_0
    sget-object v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->c:[I

    aget v0, v0, p2

    goto/16 :goto_0

    .line 317
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 324
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->r:[Landroid/widget/TextView;

    aget-object v1, v1, p1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 332
    :cond_3
    const-string v0, ""

    goto :goto_3

    :cond_4
    move v0, v3

    .line 336
    goto :goto_4
.end method

.method private a(IZLcom/google/ad/a/a/p;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 382
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v2, v0, p1

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v0}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/view/View;Z)V

    .line 384
    if-eqz p2, :cond_1

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->p:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->q:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    sget v2, Lcom/google/android/gms/p;->oP:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->q:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 399
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 382
    goto :goto_0

    .line 389
    :cond_1
    if-eqz p3, :cond_2

    iget-object v0, p3, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 391
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->q:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    iget-object v2, p3, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->p:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->q:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 395
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->p:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->q:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/fragments/ContactFragment;Z)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 181
    iput-boolean p1, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->x:Z

    .line 183
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->j:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->x:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/p;->oO:I

    :goto_0
    invoke-static {v2, v0}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;I)V

    move v0, v1

    .line 185
    :goto_1
    const/4 v2, 0x5

    if-ge v0, v2, :cond_2

    .line 186
    iget-boolean v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->x:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->t:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 189
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->t:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 185
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 183
    :cond_0
    sget v0, Lcom/google/android/gms/p;->pn:I

    goto :goto_0

    .line 191
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->t:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 194
    :cond_2
    return-void
.end method

.method private a(I)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 356
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 376
    :goto_0
    return v0

    .line 358
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->g:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k()Lcom/google/android/gms/googlehelp/common/a;

    move-result-object v0

    .line 359
    if-nez v0, :cond_0

    const-string v0, ""

    .line 364
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    .line 359
    :cond_0
    const-string v3, "ongoing_chat_request_pool_id"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 364
    goto :goto_0

    .line 367
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->g:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k()Lcom/google/android/gms/googlehelp/common/a;

    move-result-object v0

    .line 368
    if-nez v0, :cond_2

    const-string v0, ""

    .line 373
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    .line 368
    :cond_2
    const-string v3, "ongoing_video_request_pool_id"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 373
    goto :goto_0

    .line 356
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/fragments/ContactFragment;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->x:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/fragments/ContactFragment;)Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->g:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 197
    .line 198
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->z()I

    move-result v8

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->y()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->u:Ljava/util/Map;

    .line 202
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->v:Ljava/util/Map;

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->A()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v1, v2

    move v3, v2

    move v4, v2

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 205
    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v7, v2

    .line 206
    :goto_1
    if-eqz v7, :cond_12

    .line 207
    if-eq v0, v8, :cond_1

    if-ne v7, v8, :cond_7

    :cond_1
    move v0, v5

    .line 209
    :goto_2
    invoke-direct {p0, v4, v7, v0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(IIZ)V

    .line 210
    if-eqz v0, :cond_2

    move v3, v5

    .line 213
    :cond_2
    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->t:[Landroid/widget/TextView;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v1, v5

    .line 217
    :cond_3
    add-int/lit8 v4, v4, 0x1

    move v0, v1

    move v1, v3

    move v3, v4

    :goto_3
    move v4, v3

    move v3, v1

    move v1, v0

    .line 219
    goto :goto_0

    .line 205
    :pswitch_1
    iget-object v7, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v7}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->p()I

    move-result v7

    if-ne v7, v11, :cond_0

    move v7, v0

    goto :goto_1

    :pswitch_2
    iget-object v7, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->v:Ljava/util/Map;

    const/4 v10, 0x2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->v:Ljava/util/Map;

    const/4 v10, 0x6

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_4
    move v7, v2

    goto :goto_1

    :cond_5
    iget-object v7, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v7}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->o()Z

    move-result v7

    if-eqz v7, :cond_6

    const/4 v7, 0x6

    goto :goto_1

    :cond_6
    iget-object v7, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v10, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->f:Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

    invoke-virtual {v7, v10}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v7, v0

    goto :goto_1

    :pswitch_3
    iget-object v7, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v7}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->u()Z

    move-result v7

    if-eqz v7, :cond_0

    move v7, v0

    goto :goto_1

    :pswitch_4
    iget-object v7, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v7}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->r()I

    move-result v7

    if-ne v7, v11, :cond_0

    move v7, v0

    goto :goto_1

    :cond_7
    move v0, v2

    .line 207
    goto :goto_2

    .line 222
    :cond_8
    iget-object v7, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->l:Landroid/view/View;

    if-eqz v3, :cond_9

    move v0, v2

    :goto_4
    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 224
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->m:Landroid/view/View;

    if-le v4, v5, :cond_a

    move v0, v2

    :goto_5
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    move v0, v4

    .line 227
    :goto_6
    if-ge v0, v12, :cond_b

    .line 228
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 227
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    move v0, v6

    .line 222
    goto :goto_4

    :cond_a
    move v0, v6

    .line 224
    goto :goto_5

    .line 232
    :cond_b
    if-eqz v1, :cond_c

    .line 233
    new-instance v0, Lcom/google/ad/a/a/p;

    invoke-direct {v0}, Lcom/google/ad/a/a/p;-><init>()V

    new-array v3, v5, [Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->f:Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

    sget v8, Lcom/google/android/gms/p;->oI:I

    invoke-virtual {v7, v8}, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v2

    iput-object v3, v0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->u:Ljava/util/Map;

    const/4 v7, 0x7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v3, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    :cond_c
    const/4 v0, 0x7

    invoke-direct {p0, v12, v0, v2}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(IIZ)V

    .line 241
    if-lez v4, :cond_e

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->w:Z

    if-nez v0, :cond_e

    move v0, v5

    .line 242
    :goto_7
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_f

    move v3, v2

    :goto_8
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 244
    if-eqz v0, :cond_10

    if-eqz v1, :cond_10

    move v0, v5

    .line 245
    :goto_9
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_d

    move v6, v2

    :cond_d
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 248
    if-nez v0, :cond_11

    :goto_a
    invoke-direct {p0, v5}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(Z)V

    .line 249
    return-void

    :cond_e
    move v0, v2

    .line 241
    goto :goto_7

    :cond_f
    move v3, v6

    .line 242
    goto :goto_8

    :cond_10
    move v0, v2

    .line 244
    goto :goto_9

    :cond_11
    move v5, v2

    .line 248
    goto :goto_a

    :cond_12
    move v0, v1

    move v1, v3

    move v3, v4

    goto/16 :goto_3

    .line 205
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(IZ)V
    .locals 3

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->v:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 403
    if-eqz v0, :cond_0

    .line 404
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->u:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ad/a/a/p;

    invoke-direct {p0, v1, p2, v0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(IZLcom/google/ad/a/a/p;)V

    .line 406
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 409
    sget-object v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 410
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(I)Z

    move-result v2

    .line 411
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(IZ)V

    goto :goto_0

    .line 413
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 174
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 175
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->f:Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->f:Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->e()Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->g:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->g:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->h:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 178
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 117
    sget v0, Lcom/google/android/gms/l;->cd:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 119
    sget v0, Lcom/google/android/gms/j;->hL:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->i:Landroid/view/View;

    .line 120
    sget v0, Lcom/google/android/gms/j;->jc:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->j:Landroid/widget/TextView;

    .line 121
    sget v0, Lcom/google/android/gms/j;->iI:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->l:Landroid/view/View;

    .line 122
    sget v0, Lcom/google/android/gms/j;->ia:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->m:Landroid/view/View;

    .line 123
    new-array v0, v8, [Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->hP:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    sget v3, Lcom/google/android/gms/j;->hQ:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v3, 0x2

    sget v5, Lcom/google/android/gms/j;->hR:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    aput-object v5, v0, v3

    const/4 v3, 0x3

    sget v5, Lcom/google/android/gms/j;->hS:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    aput-object v5, v0, v3

    const/4 v3, 0x4

    sget v5, Lcom/google/android/gms/j;->hZ:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    aput-object v5, v0, v3

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    .line 131
    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/gms/g;->bc:I

    invoke-virtual {v0, v3, v5, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    move v3, v2

    .line 135
    :goto_0
    if-ge v3, v8, :cond_0

    .line 136
    iget-object v6, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->n:[Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v0, v0, v3

    sget v7, Lcom/google/android/gms/j;->hT:I

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v6, v3

    .line 138
    iget-object v6, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->o:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v0, v0, v3

    sget v7, Lcom/google/android/gms/j;->hU:I

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v6, v3

    .line 140
    iget-object v6, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->p:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v0, v0, v3

    sget v7, Lcom/google/android/gms/j;->hV:I

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v6, v3

    .line 142
    iget-object v6, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->q:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v0, v0, v3

    sget v7, Lcom/google/android/gms/j;->hW:I

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v6, v3

    .line 144
    iget-object v6, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->r:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v0, v0, v3

    sget v7, Lcom/google/android/gms/j;->hO:I

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v6, v3

    .line 146
    iget-object v6, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->s:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v0, v0, v3

    sget v7, Lcom/google/android/gms/j;->hX:I

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v6, v3

    .line 148
    iget-object v6, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->t:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->k:[Landroid/view/View;

    aget-object v0, v0, v3

    sget v7, Lcom/google/android/gms/j;->ix:I

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v6, v3

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->t:[Landroid/widget/TextView;

    aget-object v0, v0, v3

    const/4 v6, 0x0

    invoke-virtual {v5}, Landroid/util/TypedValue;->getFloat()F

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 135
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 154
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->u:Ljava/util/Map;

    .line 155
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->v:Ljava/util/Map;

    .line 159
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->getId()I

    move-result v0

    sget v3, Lcom/google/android/gms/j;->hN:I

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->w:Z

    .line 161
    iput-boolean v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->x:Z

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->j:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/gms/googlehelp/fragments/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/fragments/a;-><init>(Lcom/google/android/gms/googlehelp/fragments/ContactFragment;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;Landroid/view/View$OnClickListener;)V

    .line 169
    return-object v4

    :cond_1
    move v0, v2

    .line 159
    goto :goto_1
.end method
