.class final Lcom/google/android/gms/kids/account/receiver/b;
.super Lcom/google/android/gms/kids/common/c/b;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/kids/common/c/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/kids/account/receiver/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    check-cast p1, Lcom/google/android/gms/kids/common/service/a;

    new-instance v0, Lcom/google/android/gms/kids/restrictions/c;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/kids/restrictions/c;-><init>(Z)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/kids/restrictions/c;->a(Landroid/content/Context;)V

    const-class v0, Lcom/google/android/gms/kids/restrictions/receiver/RestrictionChangedReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->b(Landroid/content/Context;Ljava/lang/Class;)V

    const-class v0, Lcom/google/android/gms/kids/restrictions/receiver/ManualSyncReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->b(Landroid/content/Context;Ljava/lang/Class;)V

    const-class v0, Lcom/google/android/gms/kids/account/receiver/ProfileOwnerSetupCompleteReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->b(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "device_policy"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    if-nez v0, :cond_0

    const-string v0, "UpdateProfileOwnerReceiver"

    const-string v1, "No DevicePolicyManager found."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->isProfileOwnerApp(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/kids/common/b;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->clearProfileOwner(Landroid/content/ComponentName;)V

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->removeActiveAdmin(Landroid/content/ComponentName;)V

    const-string v0, "UpdateProfileOwnerReceiver"

    const-string v1, "Profile owner revoked."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const-string v0, "UpdateProfileOwnerReceiver"

    const-string v1, "No need to revoke."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
