.class public final Lcom/google/android/gms/plus/service/pos/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public final b:Ljava/util/Set;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/pos/k;->b:Ljava/util/Set;

    .line 381
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/pos/j;
    .locals 7

    .prologue
    .line 432
    new-instance v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/pos/k;->b:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/pos/k;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/pos/k;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/pos/k;->d:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/plus/service/pos/k;->e:Z

    iget-object v6, p0, Lcom/google/android/gms/plus/service/pos/k;->f:Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/pos/k;
    .locals 2

    .prologue
    .line 405
    iput-object p1, p0, Lcom/google/android/gms/plus/service/pos/k;->d:Ljava/lang/String;

    .line 406
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/k;->b:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 407
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/plus/service/pos/k;
    .locals 2

    .prologue
    .line 414
    iput-boolean p1, p0, Lcom/google/android/gms/plus/service/pos/k;->e:Z

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/k;->b:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 416
    return-object p0
.end method
