.class public final Lcom/google/android/gms/drive/api/a/al;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/internal/ca;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/internal/ca;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/gms/drive/api/a/al;->a:Lcom/google/android/gms/drive/internal/ca;

    .line 53
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/internal/ca;B)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/a/al;-><init>(Lcom/google/android/gms/drive/internal/ca;)V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 61
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/al;->a:Lcom/google/android/gms/drive/internal/ca;

    new-instance v1, Lcom/google/android/gms/drive/internal/OnSyncMoreResponse;

    invoke-direct {v1, p1}, Lcom/google/android/gms/drive/internal/OnSyncMoreResponse;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnSyncMoreResponse;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    .line 64
    const-string v1, "SyncMoreOperation"

    const-string v2, "Unable to report sync more result."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
