.class public final Lcom/google/android/gms/googlehelp/common/a;
.super Lcom/google/android/gms/googlehelp/common/g;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/googlehelp/common/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 41
    return-void
.end method

.method public static a(Lcom/google/android/gms/googlehelp/common/b;Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-virtual {p2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/googlehelp/common/a;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/googlehelp/common/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/googlehelp/common/b;->a(Lcom/google/android/gms/googlehelp/common/a;)V

    .line 57
    :goto_0
    return-void

    .line 56
    :cond_0
    new-instance v1, Lcom/google/android/gms/googlehelp/common/a;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/googlehelp/common/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/common/a;->a:Ljava/lang/String;

    const-string v0, "is_account_in_prefs"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0, v1}, Lcom/google/android/gms/googlehelp/common/b;->a(Lcom/google/android/gms/googlehelp/common/a;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/googlehelp/common/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/googlehelp/common/c;-><init>(Lcom/google/android/gms/googlehelp/common/b;Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/c;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/a;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    const-string v0, "%s:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/common/a;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
