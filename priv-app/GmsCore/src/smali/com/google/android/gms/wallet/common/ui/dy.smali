.class final Lcom/google/android/gms/wallet/common/ui/dy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field final synthetic a:I

.field final synthetic b:F


# direct methods
.method constructor <init>(IF)V
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/dy;->a:I

    iput p2, p0, Lcom/google/android/gms/wallet/common/ui/dy;->b:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .locals 2

    .prologue
    .line 206
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, p1

    const v1, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/dy;->a:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/dy;->b:F

    mul-float/2addr v0, v1

    return v0
.end method
