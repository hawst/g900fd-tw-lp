.class final Lcom/google/android/gms/wearable/node/bluetooth/f;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

.field private final b:Lcom/google/android/gms/wearable/node/a/c;

.field private final c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

.field private volatile d:Landroid/bluetooth/BluetoothServerSocket;

.field private volatile e:Z

.field private volatile f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;Lcom/google/android/gms/wearable/ConnectionConfiguration;)V
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    .line 180
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    .line 181
    const-string v0, "WearableBtServerThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 177
    iput-boolean v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->e:Z

    .line 178
    iput-boolean v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->f:Z

    .line 182
    new-instance v1, Lcom/google/android/gms/wearable/node/a/c;

    const/16 v2, 0x64

    const/16 v3, 0x8

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/wearable/node/a/c;-><init>(IIJJ)V

    iput-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->b:Lcom/google/android/gms/wearable/node/a/c;

    .line 184
    iput-object p2, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    .line 185
    return-void
.end method

.method private static a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 334
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, v1}, Lcom/google/android/gms/wearable/node/a/b;->a(ILjava/lang/CharSequence;Ljava/lang/Throwable;)V

    .line 335
    return-void
.end method

.method private a(Landroid/bluetooth/BluetoothAdapter;Z)V
    .locals 6

    .prologue
    .line 299
    if-eqz p2, :cond_0

    const/16 v0, 0x15

    .line 302
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, " ("

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p2, :cond_1

    const-string v1, "enabled"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 304
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v2}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->b(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    const-string v2, "WearableBluetooth"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "set scan mode to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    :goto_2
    return-void

    .line 299
    :cond_0
    const/16 v0, 0x14

    goto :goto_0

    .line 302
    :cond_1
    const-string v1, "disabled"

    goto :goto_1

    .line 310
    :catch_0
    move-exception v2

    const-string v2, "WearableBluetooth"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error setting scan mode to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 338
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0, p1}, Lcom/google/android/gms/wearable/node/a/b;->a(ILjava/lang/CharSequence;Ljava/lang/Throwable;)V

    .line 339
    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 315
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->b:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->b()J

    move-result-wide v0

    .line 316
    const-string v2, "Waiting %.1f seconds to retry connection"

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    long-to-float v5, v0

    const/high16 v6, 0x447a0000    # 1000.0f

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V

    .line 319
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 320
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->d:Landroid/bluetooth/BluetoothServerSocket;

    .line 324
    if-eqz v0, :cond_0

    .line 326
    :try_start_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->f:Z

    .line 291
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->interrupt()V

    .line 292
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 295
    iget-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->e:Z

    return v0
.end method

.method public final run()V
    .locals 11

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 189
    const-string v0, "WearableBluetooth"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "WearableVerbose"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    :cond_0
    const-string v0, "WearableBluetooth"

    const-string v1, "BluetoothThread running"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_1
    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    .line 196
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_a

    .line 197
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/a/b;->a(Landroid/app/Service;)V

    .line 199
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->d()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :try_start_1
    const-string v0, "WearableBt"

    sget-object v1, Lcom/google/android/gms/wearable/node/bluetooth/e;->a:Ljava/util/UUID;

    invoke-virtual {v2, v0, v1}, Landroid/bluetooth/BluetoothAdapter;->listenUsingRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->d:Landroid/bluetooth/BluetoothServerSocket;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->b:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 219
    const/4 v0, 0x0

    .line 221
    const/4 v1, 0x2

    :try_start_3
    const-string v3, "Accepting connections"

    invoke-static {v1, v3}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V

    .line 222
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v1}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/bluetooth/h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 223
    const/4 v1, 0x1

    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(Landroid/bluetooth/BluetoothAdapter;Z)V

    .line 224
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->d:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothServerSocket;->accept()Landroid/bluetooth/BluetoothSocket;
    :try_end_3
    .catch Lcom/google/android/gms/wearable/node/bk; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Lcom/google/android/gms/wearable/node/i; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v0

    .line 225
    :try_start_4
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 226
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v3}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v3

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b(Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v1}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a()V

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->b:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/a/c;->a()V

    .line 229
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->d()V

    .line 230
    const/4 v1, 0x0

    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(Landroid/bluetooth/BluetoothAdapter;Z)V

    .line 231
    const/4 v1, 0x3

    const-string v3, "Connected, running sync loop"

    invoke-static {v1, v3}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V

    .line 232
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->a()Lcom/google/android/gms/wearable/node/bb;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/bb;->a()Lcom/google/android/gms/wearable/node/be;

    move-result-object v1

    .line 234
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->a()Lcom/google/android/gms/wearable/node/bb;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v4}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v5}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v5

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v3, v4, v5, v1, v6}, Lcom/google/android/gms/wearable/node/bb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/node/be;Lcom/google/android/gms/wearable/ConnectionConfiguration;)V

    .line 240
    const/4 v1, 0x1

    const-string v3, "Connection closed, waiting."

    invoke-static {v1, v3}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V
    :try_end_4
    .catch Lcom/google/android/gms/wearable/node/bk; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_b
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Lcom/google/android/gms/wearable/node/i; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 265
    :try_start_5
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v1}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b()V

    .line 266
    if-eqz v0, :cond_2

    .line 267
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V

    .line 270
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->c()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 273
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    :try_start_6
    const-string v1, "Connection thread interrupted, shutting down"

    invoke-static {v0, v1}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 277
    const-string v0, "BluetoothThread is finished"

    invoke-static {v7, v0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V

    .line 278
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->d()V

    .line 279
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    .line 281
    iput-boolean v7, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->e:Z

    .line 282
    const-string v0, "WearableBluetooth"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "WearableVerbose"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 284
    :cond_3
    const-string v0, "WearableBluetooth"

    const-string v1, "BluetoothThread shut down."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_4
    :goto_2
    return-void

    .line 205
    :catch_1
    move-exception v0

    .line 209
    :try_start_7
    new-instance v1, Ljava/io/IOException;

    const-string v3, "Caught RuntimeException when trying to  listen to the RFComm socket."

    invoke-direct {v1, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 213
    :catch_2
    move-exception v0

    .line 214
    :try_start_8
    const-string v1, "Error listening for connection"

    invoke-static {v1, v0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 215
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->c()V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 274
    :catch_3
    move-exception v0

    .line 275
    :try_start_9
    const-string v1, "Unexpected exception, shutting down"

    invoke-static {v1, v0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 277
    const-string v0, "BluetoothThread is finished"

    invoke-static {v7, v0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V

    .line 278
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->d()V

    .line 279
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    .line 281
    iput-boolean v7, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->e:Z

    .line 282
    const-string v0, "WearableBluetooth"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "WearableVerbose"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 284
    :cond_5
    const-string v0, "WearableBluetooth"

    const-string v1, "BluetoothThread shut down."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 242
    :catch_4
    move-exception v1

    const/4 v1, 0x4

    :try_start_a
    const-string v3, "Error: wire protocol version mismatch"

    invoke-static {v1, v3}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V

    .line 244
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->b:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/a/c;->d()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 265
    :try_start_b
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v1}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b()V

    .line 266
    if-eqz v0, :cond_2

    .line 267
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_1

    .line 277
    :catchall_0
    move-exception v0

    const-string v1, "BluetoothThread is finished"

    invoke-static {v7, v1}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V

    .line 278
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->d()V

    .line 279
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    .line 281
    iput-boolean v7, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->e:Z

    .line 282
    const-string v1, "WearableBluetooth"

    invoke-static {v1, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "WearableVerbose"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 284
    :cond_6
    const-string v1, "WearableBluetooth"

    const-string v2, "BluetoothThread shut down."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    throw v0

    .line 245
    :catch_5
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    .line 246
    :goto_3
    :try_start_c
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v3}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    .line 247
    const-string v3, "Error writing to device"

    invoke-static {v3, v0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 265
    :try_start_d
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v0}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b()V

    .line 266
    if-eqz v1, :cond_2

    .line 267
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_d
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_1

    .line 248
    :catch_6
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    .line 249
    :goto_4
    :try_start_e
    iget-boolean v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->f:Z

    if-eqz v3, :cond_9

    .line 251
    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 265
    :catchall_1
    move-exception v0

    :goto_5
    :try_start_f
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v2}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b()V

    .line 266
    if-eqz v1, :cond_8

    .line 267
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V

    :cond_8
    throw v0
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 253
    :cond_9
    :try_start_10
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 254
    const-string v3, "Connection closed, waiting"

    invoke-static {v3, v0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 265
    :try_start_11
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v0}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b()V

    .line 266
    if-eqz v1, :cond_2

    .line 267
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_1

    .line 257
    :catch_7
    move-exception v1

    const/4 v1, 0x4

    :try_start_12
    const-string v3, "Error: Connection attempted from incorrect client"

    invoke-static {v1, v3}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V

    .line 259
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->b:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/a/c;->d()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    .line 265
    :try_start_13
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v1}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b()V

    .line 266
    if-eqz v0, :cond_2

    .line 267
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_13
    .catch Ljava/lang/InterruptedException; {:try_start_13 .. :try_end_13} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_3
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto/16 :goto_1

    .line 260
    :catch_8
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    .line 261
    :goto_6
    :try_start_14
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v3}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/lang/String;)V

    .line 262
    const-string v3, "Unexpected exception"

    invoke-static {v3, v0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->b:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->d()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 265
    :try_start_15
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-static {v0}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;)Lcom/google/android/gms/wearable/node/bluetooth/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b()V

    .line 266
    if-eqz v1, :cond_2

    .line 267
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_15
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_15} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto/16 :goto_1

    .line 277
    :cond_a
    const-string v0, "BluetoothThread is finished"

    invoke-static {v7, v0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->a(ILjava/lang/String;)V

    .line 278
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/f;->d()V

    .line 279
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothServerService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    .line 281
    iput-boolean v7, p0, Lcom/google/android/gms/wearable/node/bluetooth/f;->e:Z

    .line 282
    const-string v0, "WearableBluetooth"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "WearableVerbose"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 284
    :cond_b
    const-string v0, "WearableBluetooth"

    const-string v1, "BluetoothThread shut down."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 265
    :catchall_2
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto/16 :goto_5

    :catchall_3
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto/16 :goto_5

    .line 260
    :catch_9
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto :goto_6

    .line 248
    :catch_a
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto/16 :goto_4

    .line 245
    :catch_b
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto/16 :goto_3
.end method
