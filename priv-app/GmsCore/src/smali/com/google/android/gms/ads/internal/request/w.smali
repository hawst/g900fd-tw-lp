.class public final Lcom/google/android/gms/ads/internal/request/w;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field public final a:Landroid/os/Bundle;

.field public final b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

.field public final c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field public final d:Ljava/lang/String;

.field public final e:Landroid/content/pm/ApplicationInfo;

.field public final f:Landroid/content/pm/PackageInfo;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Landroid/os/Bundle;

.field public final j:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field public final k:I

.field public final l:Ljava/util/List;

.field public final m:Landroid/os/Bundle;

.field public final n:Z

.field public final o:I

.field public final p:I

.field public final q:F

.field public final r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Landroid/os/Bundle;Ljava/util/List;Landroid/os/Bundle;ZIIFLjava/lang/String;)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/w;->a:Landroid/os/Bundle;

    .line 77
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/w;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    .line 78
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/w;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 79
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/request/w;->d:Ljava/lang/String;

    .line 80
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/request/w;->e:Landroid/content/pm/ApplicationInfo;

    .line 81
    iput-object p6, p0, Lcom/google/android/gms/ads/internal/request/w;->f:Landroid/content/pm/PackageInfo;

    .line 82
    iput-object p7, p0, Lcom/google/android/gms/ads/internal/request/w;->g:Ljava/lang/String;

    .line 83
    iput-object p8, p0, Lcom/google/android/gms/ads/internal/request/w;->h:Ljava/lang/String;

    .line 84
    iput-object p9, p0, Lcom/google/android/gms/ads/internal/request/w;->j:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    .line 85
    iput-object p10, p0, Lcom/google/android/gms/ads/internal/request/w;->i:Landroid/os/Bundle;

    .line 86
    iput-boolean p13, p0, Lcom/google/android/gms/ads/internal/request/w;->n:Z

    .line 87
    move/from16 v0, p14

    iput v0, p0, Lcom/google/android/gms/ads/internal/request/w;->o:I

    .line 88
    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/gms/ads/internal/request/w;->p:I

    .line 89
    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/gms/ads/internal/request/w;->q:F

    .line 91
    if-eqz p11, :cond_0

    invoke-interface {p11}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 92
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/gms/ads/internal/request/w;->k:I

    .line 93
    iput-object p11, p0, Lcom/google/android/gms/ads/internal/request/w;->l:Ljava/util/List;

    .line 99
    :goto_0
    iput-object p12, p0, Lcom/google/android/gms/ads/internal/request/w;->m:Landroid/os/Bundle;

    .line 100
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/w;->r:Ljava/lang/String;

    .line 101
    return-void

    .line 95
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/gms/ads/internal/request/w;->k:I

    .line 96
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/ads/internal/request/w;->l:Ljava/util/List;

    goto :goto_0
.end method
