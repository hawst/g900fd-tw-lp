.class public final Lcom/google/android/gms/auth/f/f;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:I

.field private e:Z

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1820
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 1825
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/f/f;->b:Ljava/lang/String;

    .line 1842
    iput v1, p0, Lcom/google/android/gms/auth/f/f;->d:I

    .line 1859
    iput v1, p0, Lcom/google/android/gms/auth/f/f;->f:I

    .line 1899
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/f/f;->g:I

    .line 1820
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1902
    iget v0, p0, Lcom/google/android/gms/auth/f/f;->g:I

    if-gez v0, :cond_0

    .line 1904
    invoke-virtual {p0}, Lcom/google/android/gms/auth/f/f;->b()I

    .line 1906
    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/f/f;->g:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1817
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Lcom/google/android/gms/auth/f/f;->a:Z

    iput-object v0, p0, Lcom/google/android/gms/auth/f/f;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v1, p0, Lcom/google/android/gms/auth/f/f;->c:Z

    iput v0, p0, Lcom/google/android/gms/auth/f/f;->d:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v1, p0, Lcom/google/android/gms/auth/f/f;->e:Z

    iput v0, p0, Lcom/google/android/gms/auth/f/f;->f:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 1888
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/f;->a:Z

    if-eqz v0, :cond_0

    .line 1889
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/auth/f/f;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1891
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/f;->c:Z

    if-eqz v0, :cond_1

    .line 1892
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/auth/f/f;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1894
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/f;->e:Z

    if-eqz v0, :cond_2

    .line 1895
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/auth/f/f;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1897
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 1911
    const/4 v0, 0x0

    .line 1912
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/f;->a:Z

    if-eqz v1, :cond_0

    .line 1913
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/auth/f/f;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1916
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/f;->c:Z

    if-eqz v1, :cond_1

    .line 1917
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/auth/f/f;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1920
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/f;->e:Z

    if-eqz v1, :cond_2

    .line 1921
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/auth/f/f;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1924
    :cond_2
    iput v0, p0, Lcom/google/android/gms/auth/f/f;->g:I

    .line 1925
    return v0
.end method
