.class final Lcom/google/android/gms/cast/media/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/media/p;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/p;)V
    .locals 0

    .prologue
    .line 2109
    iput-object p1, p0, Lcom/google/android/gms/cast/media/r;->a:Lcom/google/android/gms/cast/media/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2112
    iget-object v0, p0, Lcom/google/android/gms/cast/media/r;->a:Lcom/google/android/gms/cast/media/p;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/p;->c(Lcom/google/android/gms/cast/media/p;)Lcom/google/android/gms/cast/internal/k;

    move-result-object v0

    const-string v1, "onMetadataUpdated"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2113
    iget-object v0, p0, Lcom/google/android/gms/cast/media/r;->a:Lcom/google/android/gms/cast/media/p;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/p;->a:Lcom/google/android/gms/cast/media/o;

    iget-wide v0, v0, Lcom/google/android/gms/cast/media/o;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2115
    iget-object v0, p0, Lcom/google/android/gms/cast/media/r;->a:Lcom/google/android/gms/cast/media/p;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/p;->a:Lcom/google/android/gms/cast/media/o;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/o;->j()V

    .line 2119
    :goto_0
    return-void

    .line 2117
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/r;->a:Lcom/google/android/gms/cast/media/p;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/p;->d(Lcom/google/android/gms/cast/media/p;)Lcom/google/android/gms/cast/internal/k;

    move-result-object v0

    const-string v1, "ignoring callback; initial status request is outstanding"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
