.class public Lcom/google/android/gms/auth/devicesignals/LockScreenListener;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 36
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "DeviceSignals"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "LockScreenListener"

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a:Lcom/google/android/gms/auth/d/a;

    .line 38
    sput-boolean v4, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(JJ)J
    .locals 4

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 136
    sub-long v2, v0, p0

    cmp-long v2, p2, v2

    if-ltz v2, :cond_0

    .line 139
    :goto_0
    return-wide v0

    :cond_0
    add-long v0, p0, p2

    goto :goto_0
.end method

.method private static declared-synchronized a()V
    .locals 5

    .prologue
    .line 40
    const-class v1, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->b:Z

    if-nez v0, :cond_0

    .line 41
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->b:Z

    .line 42
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;

    invoke-direct {v2}, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;-><init>()V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.SCREEN_OFF"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/app/GmsApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    :cond_0
    monitor-exit v1

    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Lcom/google/android/gms/auth/devicesignals/b;Landroid/app/KeyguardManager;)Z
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 119
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    if-nez v0, :cond_2

    .line 120
    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Device does not have a secure lock screen."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/devicesignals/b;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lockScreenSecureDuration"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/auth/devicesignals/b;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastSecureUnlockTime"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 123
    const/4 v0, 0x0

    .line 130
    :goto_0
    return v0

    .line 125
    :cond_2
    sget-object v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Device has a secure lock screen."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 126
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/devicesignals/b;->b()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 128
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/devicesignals/b;->b(J)V

    .line 130
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v2, 0x3

    .line 50
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    new-instance v3, Lcom/google/android/gms/auth/devicesignals/b;

    invoke-direct {v3}, Lcom/google/android/gms/auth/devicesignals/b;-><init>()V

    .line 55
    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 58
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const/4 v1, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 74
    :cond_3
    :goto_2
    sget-object v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    sget-object v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 76
    sget-object v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getLastSecureUnlockTime: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/auth/devicesignals/b;->a()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 77
    sget-object v0, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getLockScreenSecureDuration: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/auth/devicesignals/b;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :sswitch_0
    const-string v5, "com.google.android.gms.INITIALIZE"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v5, "com.google.android.gms.GMS_UPDATED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v5, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v5, "android.intent.action.USER_PRESENT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v2

    goto :goto_1

    :sswitch_4
    const-string v5, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v1, 0x4

    goto/16 :goto_1

    .line 61
    :pswitch_0
    invoke-static {}, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a()V

    goto/16 :goto_2

    .line 64
    :pswitch_1
    invoke-static {v3, v0}, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a(Lcom/google/android/gms/auth/devicesignals/b;Landroid/app/KeyguardManager;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Lcom/google/android/gms/auth/devicesignals/b;->a()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3

    const-wide/16 v0, 0x0

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/auth/devicesignals/b;->a(J)V

    invoke-virtual {v3}, Lcom/google/android/gms/auth/devicesignals/b;->b()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a(JJ)J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/auth/devicesignals/b;->b(J)V

    goto/16 :goto_2

    .line 67
    :pswitch_2
    invoke-static {v3, v0}, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a(Lcom/google/android/gms/auth/devicesignals/b;Landroid/app/KeyguardManager;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {v3}, Lcom/google/android/gms/auth/devicesignals/b;->a()J

    move-result-wide v4

    cmp-long v6, v4, v6

    if-eqz v6, :cond_4

    invoke-virtual {v3}, Lcom/google/android/gms/auth/devicesignals/b;->b()J

    move-result-wide v6

    sub-long v4, v0, v4

    invoke-static {v6, v7, v4, v5}, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a(JJ)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/auth/devicesignals/b;->b(J)V

    :cond_4
    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/auth/devicesignals/b;->a(J)V

    goto/16 :goto_2

    .line 70
    :pswitch_3
    invoke-static {v3, v0}, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a(Lcom/google/android/gms/auth/devicesignals/b;Landroid/app/KeyguardManager;)Z

    goto/16 :goto_2

    .line 58
    :sswitch_data_0
    .sparse-switch
        -0x7ed8ea7f -> :sswitch_4
        -0x1eaaeb28 -> :sswitch_0
        0x82f641 -> :sswitch_1
        0x2f94f923 -> :sswitch_2
        0x311a1d6c -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
