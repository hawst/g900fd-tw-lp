.class public abstract enum Lcom/google/android/gms/drive/ui/picker/a/a/a;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/ui/picker/a/a/a;

.field public static final enum b:Lcom/google/android/gms/drive/ui/picker/a/a/a;

.field public static final enum c:Lcom/google/android/gms/drive/ui/picker/a/a/a;

.field public static final enum d:Lcom/google/android/gms/drive/ui/picker/a/a/a;

.field private static final synthetic f:[Lcom/google/android/gms/drive/ui/picker/a/a/a;


# instance fields
.field private final e:Lcom/google/android/gms/drive/metadata/k;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 13
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/b;

    const-string v1, "LAST_MODIFIED"

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/b;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/metadata/k;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;->a:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    .line 19
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/c;

    const-string v1, "LAST_OPENED"

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/l;->b:Lcom/google/android/gms/drive/metadata/internal/a/n;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/metadata/k;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;->b:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    .line 25
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/d;

    const-string v1, "MODIFIED_BY_ME"

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/l;->d:Lcom/google/android/gms/drive/metadata/internal/a/o;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/d;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/metadata/k;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;->c:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    .line 31
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/e;

    const-string v1, "SHARED_WITH_ME"

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/l;->e:Lcom/google/android/gms/drive/metadata/internal/a/q;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/e;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/metadata/k;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;->d:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    .line 12
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/drive/ui/picker/a/a/a;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/a;->a:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/a;->b:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/a;->c:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/a;->d:Lcom/google/android/gms/drive/ui/picker/a/a/a;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;->f:[Lcom/google/android/gms/drive/ui/picker/a/a/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/metadata/k;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput-object p3, p0, Lcom/google/android/gms/drive/ui/picker/a/a/a;->e:Lcom/google/android/gms/drive/metadata/k;

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/metadata/k;B)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/ui/picker/a/a/a;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/metadata/k;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/a/a/a;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/ui/picker/a/a/a;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/a;->f:[Lcom/google/android/gms/drive/ui/picker/a/a/a;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/ui/picker/a/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/ui/picker/a/a/a;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/metadata/k;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/a;->e:Lcom/google/android/gms/drive/metadata/k;

    return-object v0
.end method

.method public abstract a(Lcom/google/android/gms/drive/ui/picker/a/l;)Ljava/lang/Long;
.end method
