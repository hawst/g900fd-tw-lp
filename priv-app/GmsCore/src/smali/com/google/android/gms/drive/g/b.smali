.class public final Lcom/google/android/gms/drive/g/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/android/volley/a;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/drive/g/b;->b(Lcom/android/volley/a;)Lorg/json/JSONObject;

    move-result-object v1

    .line 38
    if-nez v1, :cond_0

    .line 44
    :goto_0
    return-object v0

    .line 41
    :cond_0
    const-string v2, "error"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "errors"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 44
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static b(Lcom/android/volley/a;)Lorg/json/JSONObject;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-object v0, p0, Lcom/android/volley/a;->a:Lcom/android/volley/m;

    iget-object v3, v0, Lcom/android/volley/m;->b:[B

    .line 54
    iget-object v0, p0, Lcom/android/volley/a;->a:Lcom/android/volley/m;

    iget-object v0, v0, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v2, "Content-Encoding"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 56
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    if-nez v0, :cond_0

    .line 59
    :try_start_0
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    :goto_0
    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    const-string v5, "utf-8"

    invoke-direct {v0, v2, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 70
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_5

    .line 71
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    const/16 v0, 0xa

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 70
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    goto :goto_1

    .line 60
    :cond_0
    :try_start_2
    const-string v2, "gzip"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 61
    new-instance v2, Ljava/util/zip/GZIPInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v0}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 76
    :goto_2
    :try_start_3
    const-string v3, "AuthFailureErrorDecoder"

    const-string v4, "Error while reading error response."

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 77
    if-eqz v2, :cond_1

    .line 85
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_3
    move-object v0, v1

    .line 88
    :goto_4
    return-object v0

    .line 62
    :cond_2
    :try_start_5
    const-string v2, "deflate"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 63
    new-instance v2, Ljava/util/zip/InflaterInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v0}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 78
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 79
    :goto_5
    :try_start_6
    const-string v3, "AuthFailureErrorDecoder"

    const-string v5, "Unable to parse error response."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3, v0, v5, v6}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 80
    const-string v0, "AuthFailureErrorDecoder"

    const-string v3, "Error response: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v6

    invoke-static {v0, v3, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 81
    if-eqz v2, :cond_3

    .line 85
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_3
    :goto_6
    move-object v0, v1

    .line 88
    goto :goto_4

    .line 65
    :cond_4
    :try_start_8
    const-string v2, "AuthFailureErrorDecoder"

    const-string v3, "Unsupported encoding: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v2, v3, v5}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-object v0, v1

    .line 66
    goto :goto_4

    .line 74
    :cond_5
    :try_start_9
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 83
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_4

    .line 86
    :catch_2
    move-exception v1

    .line 87
    const-string v2, "AuthFailureErrorDecoder"

    const-string v3, "Error while closing response input stream."

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_4

    .line 86
    :catch_3
    move-exception v0

    .line 87
    const-string v2, "AuthFailureErrorDecoder"

    const-string v3, "Error while closing response input stream."

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_3

    .line 86
    :catch_4
    move-exception v0

    .line 87
    const-string v2, "AuthFailureErrorDecoder"

    const-string v3, "Error while closing response input stream."

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_6

    .line 83
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_7
    if-eqz v2, :cond_6

    .line 85
    :try_start_b
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    .line 88
    :cond_6
    :goto_8
    throw v0

    .line 86
    :catch_5
    move-exception v1

    .line 87
    const-string v2, "AuthFailureErrorDecoder"

    const-string v3, "Error while closing response input stream."

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_8

    .line 83
    :catchall_1
    move-exception v0

    goto :goto_7

    .line 78
    :catch_6
    move-exception v0

    goto :goto_5

    .line 75
    :catch_7
    move-exception v0

    goto/16 :goto_2
.end method
