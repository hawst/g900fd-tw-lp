.class public final enum Lcom/google/android/gms/drive/ui/picker/a/a/y;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/ui/picker/a/a/y;

.field public static final enum b:Lcom/google/android/gms/drive/ui/picker/a/a/y;

.field public static final enum c:Lcom/google/android/gms/drive/ui/picker/a/a/y;

.field public static final enum d:Lcom/google/android/gms/drive/ui/picker/a/a/y;

.field public static final enum e:Lcom/google/android/gms/drive/ui/picker/a/a/y;

.field public static final enum f:Lcom/google/android/gms/drive/ui/picker/a/a/y;

.field public static final enum g:Lcom/google/android/gms/drive/ui/picker/a/a/y;

.field private static final synthetic l:[Lcom/google/android/gms/drive/ui/picker/a/a/y;


# instance fields
.field private final h:Lcom/google/android/gms/drive/ui/picker/a/a/r;

.field private final i:Lcom/google/android/gms/drive/ui/picker/a/a/r;

.field private final j:Lcom/google/android/gms/drive/ui/picker/a/a/r;

.field private final k:Lcom/google/android/gms/drive/ui/picker/a/a/r;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 15
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;

    const-string v1, "TODAY"

    sget v2, Lcom/google/android/gms/p;->gU:I

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/y;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->a:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    .line 16
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;

    const-string v1, "YESTERDAY"

    sget v2, Lcom/google/android/gms/p;->gV:I

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/y;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->b:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    .line 17
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;

    const-string v1, "THIS_WEEK"

    sget v2, Lcom/google/android/gms/p;->gS:I

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/y;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->c:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    .line 18
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;

    const-string v1, "THIS_MONTH"

    sget v2, Lcom/google/android/gms/p;->gR:I

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/y;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->d:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    .line 19
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;

    const-string v1, "THIS_YEAR"

    sget v2, Lcom/google/android/gms/p;->gT:I

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/y;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->e:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    .line 20
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;

    const-string v1, "LAST_YEAR"

    const/4 v2, 0x5

    sget v3, Lcom/google/android/gms/p;->gP:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/ui/picker/a/a/y;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->f:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    .line 21
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;

    const-string v1, "OLDER"

    const/4 v2, 0x6

    sget v3, Lcom/google/android/gms/p;->gQ:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/ui/picker/a/a/y;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->g:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    .line 14
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/gms/drive/ui/picker/a/a/y;

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/y;->a:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/y;->b:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/y;->c:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/y;->d:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/y;->e:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/y;->f:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/a/y;->g:Lcom/google/android/gms/drive/ui/picker/a/a/y;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->l:[Lcom/google/android/gms/drive/ui/picker/a/a/y;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 132
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    invoke-direct {v0, p3, v2, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/s;-><init>(IZZ)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->h:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 133
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    invoke-direct {v0, p3, v1, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/s;-><init>(IZZ)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->i:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 134
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    invoke-direct {v0, p3, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/s;-><init>(IZZ)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->j:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 135
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    invoke-direct {v0, p3, v2, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/s;-><init>(IZZ)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->k:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 136
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/a/a/y;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/ui/picker/a/a/y;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->l:[Lcom/google/android/gms/drive/ui/picker/a/a/y;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/ui/picker/a/a/y;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/ui/picker/a/a/y;

    return-object v0
.end method


# virtual methods
.method public final a(ZZ)Lcom/google/android/gms/drive/ui/picker/a/a/r;
    .locals 1

    .prologue
    .line 139
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->i:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 146
    :goto_0
    return-object v0

    .line 141
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->h:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    goto :goto_0

    .line 143
    :cond_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->j:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/y;->k:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    goto :goto_0
.end method
