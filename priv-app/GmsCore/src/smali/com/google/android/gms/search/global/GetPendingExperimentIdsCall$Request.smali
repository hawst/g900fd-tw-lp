.class public Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/i;


# instance fields
.field final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/search/global/i;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;->CREATOR:Lcom/google/android/gms/search/global/i;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;->a:I

    .line 56
    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput p1, p0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;->a:I

    .line 71
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;->CREATOR:Lcom/google/android/gms/search/global/i;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;->CREATOR:Lcom/google/android/gms/search/global/i;

    invoke-static {p0, p1}, Lcom/google/android/gms/search/global/i;->a(Lcom/google/android/gms/search/global/GetPendingExperimentIdsCall$Request;Landroid/os/Parcel;)V

    .line 84
    return-void
.end method
