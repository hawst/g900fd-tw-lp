.class final Lcom/google/android/gms/update/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/SystemUpdateService;

.field private final b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/SystemUpdateService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 501
    iput-object p2, p0, Lcom/google/android/gms/update/q;->b:Landroid/content/Context;

    .line 502
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/16 v10, 0xd

    const/16 v2, 0xa

    const/4 v9, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 506
    iget-object v3, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/update/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/update/h;->c()I

    move-result v3

    .line 507
    iget-object v4, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v4}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/update/h;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/update/h;->d()Z

    move-result v4

    .line 508
    iget-object v5, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v5}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "status"

    const/4 v7, -0x1

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 510
    iget-object v6, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v6}, Lcom/google/android/gms/update/SystemUpdateService;->c(Lcom/google/android/gms/update/SystemUpdateService;)Z

    move-result v6

    .line 511
    iget-object v7, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v7}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 512
    iget-object v8, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v8}, Lcom/google/android/gms/update/SystemUpdateService;->d(Lcom/google/android/gms/update/SystemUpdateService;)I

    move-result v8

    if-eq v8, v3, :cond_2

    .line 513
    const-string v8, "battery_state"

    invoke-interface {v7, v8, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 514
    iget-object v8, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v8, v3}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/SystemUpdateService;I)I

    .line 515
    if-eq v5, v9, :cond_0

    if-eq v5, v10, :cond_0

    const/16 v8, 0xe

    if-ne v5, v8, :cond_2

    .line 518
    :cond_0
    if-nez v3, :cond_7

    .line 519
    const-string v3, "status"

    invoke-interface {v7, v3, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 530
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/update/q;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/update/SystemUpdateService;->c(Landroid/content/Context;)I

    move-result v3

    const/4 v8, 0x6

    if-ne v3, v8, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v3}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v8, "update_battery_aware_automatic_timed"

    invoke-static {v3, v8, v0}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 537
    iget-object v0, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    new-instance v3, Landroid/content/Intent;

    iget-object v8, p0, Lcom/google/android/gms/update/q;->b:Landroid/content/Context;

    const-class v9, Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v3, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/update/SystemUpdateService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    move v0, v1

    .line 541
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3}, Lcom/google/android/gms/update/SystemUpdateService;->e(Lcom/google/android/gms/update/SystemUpdateService;)Z

    move-result v3

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3}, Lcom/google/android/gms/update/SystemUpdateService;->f(Lcom/google/android/gms/update/SystemUpdateService;)Z

    move-result v3

    if-eq v3, v6, :cond_5

    .line 543
    :cond_3
    const-string v3, "network_roaming"

    invoke-interface {v7, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 544
    iget-object v3, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3, v4}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/SystemUpdateService;Z)Z

    .line 545
    iget-object v3, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3, v6}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;Z)Z

    .line 546
    if-eq v5, v1, :cond_4

    if-ne v5, v2, :cond_5

    .line 548
    :cond_4
    const-string v3, "status"

    if-eqz v4, :cond_9

    if-nez v6, :cond_9

    move v0, v2

    :goto_1
    invoke-interface {v7, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move v0, v1

    .line 554
    :cond_5
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 555
    if-eqz v0, :cond_6

    .line 556
    iget-object v0, p0, Lcom/google/android/gms/update/q;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    .line 558
    :cond_6
    return-void

    .line 521
    :cond_7
    const/4 v8, 0x2

    if-ne v3, v8, :cond_8

    .line 522
    const-string v3, "status"

    invoke-interface {v7, v3, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 525
    :cond_8
    const-string v3, "status"

    const/16 v8, 0xe

    invoke-interface {v7, v3, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_9
    move v0, v1

    .line 548
    goto :goto_1
.end method
