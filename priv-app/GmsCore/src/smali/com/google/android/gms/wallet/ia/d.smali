.class final Lcom/google/android/gms/wallet/ia/d;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 0

    .prologue
    .line 796
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    .line 856
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/t;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 827
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    .line 829
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/checkout/inapp/proto/t;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 831
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Lcom/google/checkout/inapp/proto/t;)V

    .line 832
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Z)V

    .line 833
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->j:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 837
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->j(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    .line 839
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Z)V

    .line 842
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 844
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "create_to_ui_populated"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 846
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->k(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 848
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->m(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;

    .line 849
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;

    .line 851
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 860
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    .line 861
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 865
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z

    .line 866
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z

    .line 867
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    if-eqz v0, :cond_0

    .line 868
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    .line 872
    :goto_0
    return-void

    .line 870
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->s(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->c:Lcom/google/checkout/inapp/proto/t;

    if-eqz v0, :cond_0

    .line 877
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "settings_update_failure"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    .line 883
    return-void
.end method

.method public final f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 801
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Z)V

    .line 802
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->d(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z

    .line 804
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->e(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    .line 807
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "settings_update_success"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->g(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 815
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->g(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "click_to_activity_result"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 817
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->f(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 819
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->h(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/i;

    .line 820
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/d;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->i(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/apps/common/a/a/h;

    .line 822
    :cond_0
    return-void
.end method
