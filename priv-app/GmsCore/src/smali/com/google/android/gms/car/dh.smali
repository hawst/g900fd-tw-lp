.class final Lcom/google/android/gms/car/dh;
.super Lcom/google/android/gms/car/lf;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# virtual methods
.method public final a(Landroid/view/KeyEvent;)V
    .locals 5

    .prologue
    .line 645
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 646
    if-eqz v0, :cond_1

    .line 647
    const-string v1, "CAR.TEL.CarCallManager"

    const-string v2, "dispatchPhoneKeyEvent"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/cv;

    invoke-direct {v4, v0, v1, p1}, Lcom/google/android/gms/car/cv;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Landroid/view/KeyEvent;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 649
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;)V
    .locals 5

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 663
    if-eqz v0, :cond_1

    .line 664
    const-string v1, "CAR.TEL.CarCallManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onCallAdded "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/da;

    invoke-direct {v4, v0, v1, p1}, Lcom/google/android/gms/car/da;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 666
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;I)V
    .locals 5

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 679
    if-eqz v0, :cond_1

    .line 680
    const-string v1, "CAR.TEL.CarCallManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onStateChanged "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/dc;

    invoke-direct {v4, v0, v1, p1, p2}, Lcom/google/android/gms/car/dc;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;I)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 682
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall$Details;)V
    .locals 5

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 703
    if-eqz v0, :cond_1

    .line 704
    const-string v1, "CAR.TEL.CarCallManager"

    const-string v2, "onDetailsChanged"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/df;

    invoke-direct {v4, v0, v1, p1, p2}, Lcom/google/android/gms/car/df;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall$Details;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 706
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V
    .locals 5

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 687
    if-eqz v0, :cond_1

    .line 688
    const-string v1, "CAR.TEL.CarCallManager"

    const-string v2, "onParentChanged"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/dd;

    invoke-direct {v4, v0, v1, p1, p2}, Lcom/google/android/gms/car/dd;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 690
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 720
    if-eqz v0, :cond_1

    .line 721
    const-string v1, "CAR.TEL.CarCallManager"

    const-string v2, "onPostDialWait"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/cw;

    invoke-direct {v4, v0, v1, p1, p2}, Lcom/google/android/gms/car/cw;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 723
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .locals 5

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 695
    if-eqz v0, :cond_1

    .line 696
    const-string v1, "CAR.TEL.CarCallManager"

    const-string v2, "onChildrenChanged"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/de;

    invoke-direct {v4, v0, v1, p1, p2}, Lcom/google/android/gms/car/de;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 698
    :cond_1
    return-void
.end method

.method public final a(ZII)V
    .locals 8

    .prologue
    .line 654
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/cu;

    .line 655
    if-eqz v1, :cond_1

    .line 656
    const-string v0, "CAR.TEL.CarCallManager"

    const-string v2, "onAudioStateChanged isMuted=%b\troute=%d\tsupportedRoutes=%d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, v1, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v6

    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/car/ct;

    new-instance v0, Lcom/google/android/gms/car/cz;

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/cz;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;ZII)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 658
    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/CarCall;)V
    .locals 5

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 671
    if-eqz v0, :cond_1

    .line 672
    const-string v1, "CAR.TEL.CarCallManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onCallRemoved "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/db;

    invoke-direct {v4, v0, v1, p1}, Lcom/google/android/gms/car/db;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 674
    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .locals 5

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 712
    if-eqz v0, :cond_1

    .line 713
    const-string v1, "CAR.TEL.CarCallManager"

    const-string v2, "onCannedTextResponsesLoaded"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/dg;

    invoke-direct {v4, v0, v1, p1, p2}, Lcom/google/android/gms/car/dg;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 715
    :cond_1
    return-void
.end method

.method public final c(Lcom/google/android/gms/car/CarCall;)V
    .locals 5

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 728
    if-eqz v0, :cond_1

    .line 729
    const-string v1, "CAR.TEL.CarCallManager"

    const-string v2, "onCallDestroyed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/cx;

    invoke-direct {v4, v0, v1, p1}, Lcom/google/android/gms/car/cx;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 731
    :cond_1
    return-void
.end method

.method public final c(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    .locals 5

    .prologue
    .line 736
    iget-object v0, p0, Lcom/google/android/gms/car/dh;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cu;

    .line 737
    if-eqz v0, :cond_1

    .line 738
    const-string v1, "CAR.TEL.CarCallManager"

    const-string v2, "onConferenceableCallsChanged"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/ct;

    new-instance v4, Lcom/google/android/gms/car/cy;

    invoke-direct {v4, v0, v1, p1, p2}, Lcom/google/android/gms/car/cy;-><init>(Lcom/google/android/gms/car/cu;Lcom/google/android/gms/car/ct;Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 740
    :cond_1
    return-void
.end method
