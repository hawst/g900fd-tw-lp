.class public final Lcom/google/android/gms/drive/database/b/b;
.super Lcom/google/android/gms/drive/database/b/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/i;

.field private b:Landroid/database/Cursor;

.field private final c:Lcom/google/android/gms/drive/database/a/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)V
    .locals 1

    .prologue
    .line 32
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/b/a;-><init>(I)V

    .line 33
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/b/b;->a:Lcom/google/android/gms/drive/database/i;

    .line 34
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/b/b;->b:Landroid/database/Cursor;

    .line 35
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/a/c;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/b/b;->c:Lcom/google/android/gms/drive/database/a/c;

    .line 36
    return-void
.end method


# virtual methods
.method protected final a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/drive/database/b/b;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to move delegate cursor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/b/b;->c:Lcom/google/android/gms/drive/database/a/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/b/b;->a:Lcom/google/android/gms/drive/database/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/b/b;->b:Landroid/database/Cursor;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/database/a/c;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    .line 51
    const-string v1, "Result decoder returned null."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final a()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/database/b/b;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/b/b;->b:Landroid/database/Cursor;

    .line 42
    return-void
.end method
