.class public Lcom/google/android/gms/wallet/service/analytics/a/e;
.super Lcom/google/android/gms/wallet/service/analytics/a/h;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/k/f/a/a/q;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>()V

    .line 30
    new-instance v0, Lcom/google/k/f/a/a/q;

    invoke-direct {v0}, Lcom/google/k/f/a/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/wallet/common/ag;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    .line 35
    const-class v0, Lcom/google/k/f/a/a/q;

    new-instance v1, Lcom/google/k/f/a/a/q;

    invoke-direct {v1}, Lcom/google/k/f/a/a/q;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/k/f/a/a/q;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V
    .locals 4

    .prologue
    .line 51
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;

    if-eqz v0, :cond_0

    .line 52
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;

    .line 54
    iget-object v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/e;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/content/Context;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget-object v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget-wide v2, p2, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->e:D

    iput-wide v2, v0, Lcom/google/k/f/a/a/q;->i:D

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget-wide v2, p2, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->f:D

    iput-wide v2, v0, Lcom/google/k/f/a/a/q;->j:D

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget-wide v2, p2, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->g:J

    iput-wide v2, v0, Lcom/google/k/f/a/a/q;->g:J

    .line 61
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->d:Ljava/lang/String;

    .line 76
    :goto_0
    return-void

    .line 62
    :cond_0
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;

    if-eqz v0, :cond_1

    .line 63
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;

    .line 64
    iget-object v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/e;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/content/Context;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->d:I

    iput v1, v0, Lcom/google/k/f/a/a/q;->a:I

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->e:I

    iput v1, v0, Lcom/google/k/f/a/a/q;->b:I

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->f:I

    iput v1, v0, Lcom/google/k/f/a/a/q;->c:I

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->g:I

    iput v1, v0, Lcom/google/k/f/a/a/q;->f:I

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget-boolean v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->h:Z

    iput-boolean v1, v0, Lcom/google/k/f/a/a/q;->e:Z

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget-wide v2, p2, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->i:J

    iput-wide v2, v0, Lcom/google/k/f/a/a/q;->h:J

    .line 72
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->d:Ljava/lang/String;

    goto :goto_0

    .line 74
    :cond_1
    const-string v0, "OwFullWalletSessionState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to fill data for event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/wallet/common/ah;)V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Lcom/google/android/gms/wallet/common/ah;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;

    .line 82
    return-void
.end method

.method protected final a(Lcom/google/k/f/a/a/j;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iput-object v0, p1, Lcom/google/k/f/a/a/j;->n:Lcom/google/k/f/a/a/q;

    .line 47
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/e;->a:Lcom/google/k/f/a/a/q;

    iget v0, v0, Lcom/google/k/f/a/a/q;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
