.class final Lcom/google/android/gms/gcm/nts/o;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/gcm/nts/n;


# direct methods
.method constructor <init>(Lcom/google/android/gms/gcm/nts/n;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private a(Ljava/lang/String;II)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 167
    .line 168
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->g(Lcom/google/android/gms/gcm/nts/n;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 170
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 171
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/nts/k;

    .line 172
    invoke-virtual {v0}, Lcom/google/android/gms/gcm/nts/k;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v1}, Lcom/google/android/gms/gcm/nts/n;->g(Lcom/google/android/gms/gcm/nts/n;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/gcm/nts/a;

    .line 177
    iget-boolean v4, v1, Lcom/google/android/gms/gcm/nts/a;->e:Z

    if-nez v4, :cond_1

    .line 178
    const-string v4, "NetworkScheduler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error: Handling client callback for uncompleted task"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/gcm/nts/a;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const-string v4, "INVALID_COMPLETE"

    invoke-virtual {v1, v4}, Lcom/google/android/gms/gcm/nts/a;->a(Ljava/lang/String;)V

    .line 182
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 186
    :goto_0
    if-nez v0, :cond_3

    .line 187
    const-string v0, "NetworkScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Task completed but with no record in list of active tasks: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_2
    :goto_1
    return-void

    .line 191
    :cond_3
    packed-switch p2, :pswitch_data_0

    :cond_4
    move-object v1, v2

    .line 216
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->h(Lcom/google/android/gms/gcm/nts/n;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/nts/k;

    .line 217
    invoke-virtual {v0}, Lcom/google/android/gms/gcm/nts/k;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_1

    .line 193
    :pswitch_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/gcm/nts/s;->a(Lcom/google/android/gms/gcm/nts/k;I)V

    .line 195
    invoke-virtual {v0}, Lcom/google/android/gms/gcm/nts/k;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/k;)Lcom/google/android/gms/gcm/nts/k;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 200
    :pswitch_1
    invoke-static {v0, v7}, Lcom/google/android/gms/gcm/nts/s;->a(Lcom/google/android/gms/gcm/nts/k;I)V

    .line 202
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->b(Lcom/google/android/gms/gcm/nts/k;)Lcom/google/android/gms/gcm/nts/k;

    move-result-object v0

    move-object v1, v0

    .line 203
    goto :goto_2

    .line 205
    :pswitch_2
    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/gcm/nts/s;->a(Lcom/google/android/gms/gcm/nts/k;I)V

    .line 207
    invoke-virtual {v0}, Lcom/google/android/gms/gcm/nts/k;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 208
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/k;)Lcom/google/android/gms/gcm/nts/k;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 222
    :cond_6
    if-eqz v1, :cond_2

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0, v1, v7}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/n;Lcom/google/android/gms/gcm/nts/k;Z)V

    goto :goto_1

    :cond_7
    move-object v0, v2

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 115
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v1}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/n;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 116
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v1, Lcom/google/android/gms/gcm/nts/n;->d:Lcom/google/android/gms/common/a/d;

    invoke-static {v1}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v1

    mul-int/lit16 v3, v1, 0x3e8

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->g(Lcom/google/android/gms/gcm/nts/n;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->g(Lcom/google/android/gms/gcm/nts/n;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->g(Lcom/google/android/gms/gcm/nts/n;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/nts/k;

    iget-wide v8, v0, Lcom/google/android/gms/gcm/nts/k;->k:J

    int-to-long v10, v3

    sub-long v10, v4, v10

    cmp-long v1, v8, v10

    if-gtz v1, :cond_0

    const-string v1, "NetworkScheduler"

    const/4 v8, 0x3

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "NetworkScheduler"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Timeout: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v1}, Lcom/google/android/gms/gcm/nts/n;->g(Lcom/google/android/gms/gcm/nts/n;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/gcm/nts/a;

    const-string v8, "TIMED_OUT"

    invoke-virtual {v1, v8}, Lcom/google/android/gms/gcm/nts/a;->a(Ljava/lang/String;)V

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/gcm/nts/s;->a(Lcom/google/android/gms/gcm/nts/k;I)V

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->b(Lcom/google/android/gms/gcm/nts/k;)Lcom/google/android/gms/gcm/nts/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    const/4 v8, 0x0

    invoke-static {v1, v0, v8}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/n;Lcom/google/android/gms/gcm/nts/k;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 150
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v1}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/n;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    .line 116
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 117
    :cond_3
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 118
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 148
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->d(Lcom/google/android/gms/gcm/nts/n;)Lcom/google/android/gms/gcm/nts/v;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/gcm/nts/v;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/n;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 151
    return-void

    .line 121
    :pswitch_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->b(Lcom/google/android/gms/gcm/nts/n;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 122
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    iget-object v4, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v4}, Lcom/google/android/gms/gcm/nts/n;->c(Lcom/google/android/gms/gcm/nts/n;)Lcom/google/android/gms/gcm/nts/e;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/n;Lcom/google/android/gms/gcm/nts/d;)V

    .line 123
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1

    throw v0

    .line 124
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->d(Lcom/google/android/gms/gcm/nts/n;)Lcom/google/android/gms/gcm/nts/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/nts/v;->c()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_4

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->b(Lcom/google/android/gms/gcm/nts/n;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 129
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    iget-object v4, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v4}, Lcom/google/android/gms/gcm/nts/n;->e(Lcom/google/android/gms/gcm/nts/n;)Lcom/google/android/gms/gcm/nts/g;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/n;Lcom/google/android/gms/gcm/nts/d;)V

    .line 130
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_2

    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v1

    throw v0

    .line 135
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->b(Lcom/google/android/gms/gcm/nts/n;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 136
    :try_start_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/gms/gcm/nts/o;->a(Ljava/lang/String;II)V

    .line 140
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 143
    :try_start_8
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/o;->a:Lcom/google/android/gms/gcm/nts/n;

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/n;->f(Lcom/google/android/gms/gcm/nts/n;)V

    goto :goto_2

    .line 140
    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
