.class final Lcom/google/android/gms/wearable/node/bluetooth/c;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/bluetooth/c;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;B)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lcom/google/android/gms/wearable/node/bluetooth/c;-><init>(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;)V

    return-void
.end method

.method public static a()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 210
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 211
    const-string v1, "com.google.android.gms.wearable.node.bluetooth.RETRY_CONNECTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 216
    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 217
    return-object v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 200
    const-string v0, "WearableBluetooth"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    const-string v0, "WearableBluetooth"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ConnectionRetryReceiver onReceive "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.wearable.node.bluetooth.RETRY_CONNECTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/c;->a:Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;->a(Lcom/google/android/gms/wearable/node/bluetooth/BluetoothClientService;Ljava/lang/String;)V

    .line 207
    :cond_1
    return-void
.end method
