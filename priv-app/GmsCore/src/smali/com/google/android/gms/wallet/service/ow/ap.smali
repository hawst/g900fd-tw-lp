.class public final Lcom/google/android/gms/wallet/service/ow/ap;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lcom/google/android/gms/wallet/wobs/UriData;)Lcom/google/j/a/a/a/aa;
    .locals 2

    .prologue
    .line 328
    new-instance v0, Lcom/google/j/a/a/a/aa;

    invoke-direct {v0}, Lcom/google/j/a/a/a/aa;-><init>()V

    .line 329
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/UriData;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 330
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/UriData;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/j/a/a/a/aa;->a:Ljava/lang/String;

    .line 332
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/UriData;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 333
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/UriData;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/j/a/a/a/aa;->b:Ljava/lang/String;

    .line 335
    :cond_1
    return-object v0
.end method

.method static a(Lcom/google/android/gms/wallet/wobs/CommonWalletObject;)Lcom/google/j/a/a/a/ac;
    .locals 14

    .prologue
    .line 235
    new-instance v4, Lcom/google/j/a/a/a/ac;

    invoke-direct {v4}, Lcom/google/j/a/a/a/ac;-><init>()V

    .line 237
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->a:Ljava/lang/String;

    .line 240
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 241
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->b:Ljava/lang/String;

    .line 244
    :cond_1
    const-wide/16 v0, 0x1

    iput-wide v0, v4, Lcom/google/j/a/a/a/ac;->c:J

    .line 245
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->k()I

    move-result v0

    iput v0, v4, Lcom/google/j/a/a/a/ac;->d:I

    .line 246
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, Lcom/google/j/a/a/a/b;

    invoke-direct {v1}, Lcom/google/j/a/a/a/b;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->h()Ljava/lang/String;

    move-result-object v0

    const-string v2, "aztec"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v0, 0x2

    :goto_0
    iput v0, v1, Lcom/google/j/a/a/a/b;->a:I

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/j/a/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/j/a/a/a/b;->c:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/j/a/a/a/b;->d:Ljava/lang/String;

    :cond_3
    move-object v0, v1

    :cond_4
    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->e:Lcom/google/j/a/a/a/b;

    .line 247
    invoke-static {}, Lcom/google/j/a/a/a/ae;->a()[Lcom/google/j/a/a/a/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->l()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1a

    new-array v1, v5, [Lcom/google/j/a/a/a/ae;

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v5, :cond_19

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;

    new-instance v6, Lcom/google/j/a/a/a/ae;

    invoke-direct {v6}, Lcom/google/j/a/a/a/ae;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->b()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/j/a/a/a/ae;->a:Ljava/lang/String;

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->c()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/j/a/a/a/ae;->b:Ljava/lang/String;

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->d()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v7

    if-eqz v7, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->d()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/wallet/service/ow/ap;->a(Lcom/google/android/gms/wallet/wobs/TimeInterval;)Lcom/google/j/a/a/a/y;

    move-result-object v7

    iput-object v7, v6, Lcom/google/j/a/a/a/ae;->c:Lcom/google/j/a/a/a/y;

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->e()Lcom/google/android/gms/wallet/wobs/UriData;

    move-result-object v7

    if-eqz v7, :cond_8

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->e()Lcom/google/android/gms/wallet/wobs/UriData;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/wallet/service/ow/ap;->a(Lcom/google/android/gms/wallet/wobs/UriData;)Lcom/google/j/a/a/a/aa;

    move-result-object v7

    iput-object v7, v6, Lcom/google/j/a/a/a/ae;->d:Lcom/google/j/a/a/a/aa;

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->f()Lcom/google/android/gms/wallet/wobs/UriData;

    move-result-object v7

    if-eqz v7, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/WalletObjectMessage;->f()Lcom/google/android/gms/wallet/wobs/UriData;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ap;->b(Lcom/google/android/gms/wallet/wobs/UriData;)Lcom/google/j/a/a/a/h;

    move-result-object v0

    iput-object v0, v6, Lcom/google/j/a/a/a/ae;->e:Lcom/google/j/a/a/a/h;

    :cond_9
    aput-object v6, v1, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 246
    :cond_a
    const-string v2, "code39"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v0, 0x3

    goto/16 :goto_0

    :cond_b
    const-string v2, "code128"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v0, 0x5

    goto/16 :goto_0

    :cond_c
    const-string v2, "codabar"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v0, 0x6

    goto/16 :goto_0

    :cond_d
    const-string v2, "dataMatrix"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v0, 0x7

    goto/16 :goto_0

    :cond_e
    const-string v2, "ean8"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    const/16 v0, 0x8

    goto/16 :goto_0

    :cond_f
    const-string v2, "ean13"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v0, 0x9

    goto/16 :goto_0

    :cond_10
    const-string v2, "itf14"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v0, 0xa

    goto/16 :goto_0

    :cond_11
    const-string v2, "pdf417"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v0, 0xb

    goto/16 :goto_0

    :cond_12
    const-string v2, "qrCode"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    const/16 v0, 0xe

    goto/16 :goto_0

    :cond_13
    const-string v2, "upcA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    const/16 v0, 0xf

    goto/16 :goto_0

    :cond_14
    const-string v2, "upcE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    const/16 v0, 0x10

    goto/16 :goto_0

    :cond_15
    const-string v2, "pdf417Compact"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    const/16 v0, 0x12

    goto/16 :goto_0

    :cond_16
    const-string v2, "textOnly"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    const/16 v0, 0x13

    goto/16 :goto_0

    :cond_17
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_18
    const/4 v0, 0x1

    iput v0, v1, Lcom/google/j/a/a/a/b;->a:I

    goto/16 :goto_1

    :cond_19
    move-object v0, v1

    .line 247
    :cond_1a
    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->f:[Lcom/google/j/a/a/a/ae;

    .line 249
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->m()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 250
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->m()Lcom/google/android/gms/wallet/wobs/TimeInterval;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ap;->a(Lcom/google/android/gms/wallet/wobs/TimeInterval;)Lcom/google/j/a/a/a/y;

    move-result-object v0

    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->g:Lcom/google/j/a/a/a/y;

    .line 254
    :cond_1b
    invoke-static {}, Lcom/google/j/a/a/a/f;->a()[Lcom/google/j/a/a/a/f;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->n()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1d

    new-array v1, v5, [Lcom/google/j/a/a/a/f;

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    if-ge v2, v5, :cond_1c

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    new-instance v6, Lcom/google/j/a/a/a/f;

    invoke-direct {v6}, Lcom/google/j/a/a/a/f;-><init>()V

    iget-wide v8, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iput-wide v8, v6, Lcom/google/j/a/a/a/f;->a:D

    iget-wide v8, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iput-wide v8, v6, Lcom/google/j/a/a/a/f;->b:D

    aput-object v6, v1, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_1c
    move-object v0, v1

    :cond_1d
    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->h:[Lcom/google/j/a/a/a/f;

    .line 255
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->q()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->r()Z

    move-result v1

    if-eqz v1, :cond_29

    :cond_1e
    new-instance v1, Lcom/google/j/a/a/a/s;

    invoke-direct {v1}, Lcom/google/j/a/a/a/s;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1f

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/j/a/a/a/s;->a:Ljava/lang/String;

    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_20

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/j/a/a/a/s;->b:Ljava/lang/String;

    :cond_20
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->q()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_28

    new-array v7, v6, [Lcom/google/j/a/a/a/u;

    const/4 v0, 0x0

    move v3, v0

    :goto_4
    if-ge v3, v6, :cond_27

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/LabelValueRow;

    new-instance v8, Lcom/google/j/a/a/a/u;

    invoke-direct {v8}, Lcom/google/j/a/a/a/u;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_21

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lcom/google/j/a/a/a/u;->a:Ljava/lang/String;

    :cond_21
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_22

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v8, Lcom/google/j/a/a/a/u;->b:Ljava/lang/String;

    :cond_22
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValueRow;->d()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_26

    new-array v11, v10, [Lcom/google/j/a/a/a/t;

    const/4 v0, 0x0

    move v2, v0

    :goto_5
    if-ge v2, v10, :cond_25

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/LabelValue;

    new-instance v12, Lcom/google/j/a/a/a/t;

    invoke-direct {v12}, Lcom/google/j/a/a/a/t;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValue;->b()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_23

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValue;->b()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v12, Lcom/google/j/a/a/a/t;->a:Ljava/lang/String;

    :cond_23
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValue;->c()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_24

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/LabelValue;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v12, Lcom/google/j/a/a/a/t;->b:Ljava/lang/String;

    :cond_24
    aput-object v12, v11, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_25
    iput-object v11, v8, Lcom/google/j/a/a/a/u;->c:[Lcom/google/j/a/a/a/t;

    :cond_26
    aput-object v8, v7, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_27
    iput-object v7, v1, Lcom/google/j/a/a/a/s;->c:[Lcom/google/j/a/a/a/u;

    :cond_28
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->r()Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/j/a/a/a/s;->d:Z

    move-object v0, v1

    :cond_29
    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->j:Lcom/google/j/a/a/a/s;

    .line 256
    invoke-static {}, Lcom/google/j/a/a/a/r;->a()[Lcom/google/j/a/a/a/r;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->s()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2b

    new-array v1, v5, [Lcom/google/j/a/a/a/r;

    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v5, :cond_2a

    new-instance v0, Lcom/google/j/a/a/a/r;

    invoke-direct {v0}, Lcom/google/j/a/a/a/r;-><init>()V

    aput-object v0, v1, v2

    aget-object v6, v1, v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/UriData;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ap;->b(Lcom/google/android/gms/wallet/wobs/UriData;)Lcom/google/j/a/a/a/h;

    move-result-object v0

    iput-object v0, v6, Lcom/google/j/a/a/a/r;->a:Lcom/google/j/a/a/a/h;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    :cond_2a
    move-object v0, v1

    :cond_2b
    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->k:[Lcom/google/j/a/a/a/r;

    .line 257
    invoke-static {}, Lcom/google/j/a/a/a/w;->a()[Lcom/google/j/a/a/a/w;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->t()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2f

    new-array v1, v5, [Lcom/google/j/a/a/a/w;

    const/4 v0, 0x0

    move v2, v0

    :goto_7
    if-ge v2, v5, :cond_2e

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/TextModuleData;

    new-instance v6, Lcom/google/j/a/a/a/w;

    invoke-direct {v6}, Lcom/google/j/a/a/a/w;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/TextModuleData;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2c

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/TextModuleData;->b()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/j/a/a/a/w;->a:Ljava/lang/String;

    :cond_2c
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/TextModuleData;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2d

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/wobs/TextModuleData;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/j/a/a/a/w;->b:Ljava/lang/String;

    :cond_2d
    aput-object v6, v1, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    :cond_2e
    move-object v0, v1

    :cond_2f
    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->l:[Lcom/google/j/a/a/a/w;

    .line 258
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->u()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_31

    new-array v5, v3, [Lcom/google/j/a/a/a/aa;

    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-ge v1, v3, :cond_30

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/wobs/UriData;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ap;->a(Lcom/google/android/gms/wallet/wobs/UriData;)Lcom/google/j/a/a/a/aa;

    move-result-object v0

    aput-object v0, v5, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    :cond_30
    new-instance v0, Lcom/google/j/a/a/a/v;

    invoke-direct {v0}, Lcom/google/j/a/a/a/v;-><init>()V

    iput-object v5, v0, Lcom/google/j/a/a/a/v;->a:[Lcom/google/j/a/a/a/aa;

    :cond_31
    iput-object v0, v4, Lcom/google/j/a/a/a/ac;->m:Lcom/google/j/a/a/a/v;

    .line 260
    return-object v4
.end method

.method static a(Lcom/google/android/gms/wallet/wobs/TimeInterval;)Lcom/google/j/a/a/a/y;
    .locals 4

    .prologue
    .line 453
    new-instance v0, Lcom/google/j/a/a/a/y;

    invoke-direct {v0}, Lcom/google/j/a/a/a/y;-><init>()V

    .line 455
    new-instance v1, Lcom/google/j/a/a/a/d;

    invoke-direct {v1}, Lcom/google/j/a/a/a/d;-><init>()V

    .line 456
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/TimeInterval;->b()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/j/a/a/a/d;->a:J

    .line 457
    iput-object v1, v0, Lcom/google/j/a/a/a/y;->a:Lcom/google/j/a/a/a/d;

    .line 459
    new-instance v1, Lcom/google/j/a/a/a/d;

    invoke-direct {v1}, Lcom/google/j/a/a/a/d;-><init>()V

    .line 460
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/wobs/TimeInterval;->c()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/j/a/a/a/d;->a:J

    .line 461
    iput-object v1, v0, Lcom/google/j/a/a/a/y;->b:Lcom/google/j/a/a/a/d;

    .line 463
    return-object v0
.end method

.method private static b(Lcom/google/android/gms/wallet/wobs/UriData;)Lcom/google/j/a/a/a/h;
    .locals 2

    .prologue
    .line 340
    new-instance v0, Lcom/google/j/a/a/a/h;

    invoke-direct {v0}, Lcom/google/j/a/a/a/h;-><init>()V

    .line 341
    invoke-static {p0}, Lcom/google/android/gms/wallet/service/ow/ap;->a(Lcom/google/android/gms/wallet/wobs/UriData;)Lcom/google/j/a/a/a/aa;

    move-result-object v1

    iput-object v1, v0, Lcom/google/j/a/a/a/h;->a:Lcom/google/j/a/a/a/aa;

    .line 342
    return-object v0
.end method
