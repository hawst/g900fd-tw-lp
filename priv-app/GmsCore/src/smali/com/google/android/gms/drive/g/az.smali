.class final Lcom/google/android/gms/drive/g/az;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/g/ay;

.field private b:Landroid/widget/Toast;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/g/ay;)V
    .locals 1

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/drive/g/az;->a:Lcom/google/android/gms/drive/g/ay;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/g/az;->b:Landroid/widget/Toast;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/drive/g/az;->b:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/drive/g/az;->b:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 49
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/gms/drive/g/ba;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/g/ba;

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/drive/g/az;->a:Lcom/google/android/gms/drive/g/ay;

    iget-object v1, v1, Lcom/google/android/gms/drive/g/ay;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/drive/g/ba;->a:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/drive/g/az;->b:Landroid/widget/Toast;

    .line 52
    iget-object v1, p0, Lcom/google/android/gms/drive/g/az;->b:Landroid/widget/Toast;

    iget v0, v0, Lcom/google/android/gms/drive/g/ba;->b:I

    invoke-virtual {v1, v0, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/drive/g/az;->b:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 55
    :cond_1
    return-void
.end method
