.class public Lcom/google/android/gms/plus/apps/ag;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 21
    return-void
.end method

.method public static a(Lcom/google/android/gms/fitness/data/BleDevice;)Lcom/google/android/gms/plus/apps/ag;
    .locals 3

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/plus/apps/ag;

    invoke-direct {v0}, Lcom/google/android/gms/plus/apps/ag;-><init>()V

    .line 30
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 31
    const-string v2, "device"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 32
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/ag;->setArguments(Landroid/os/Bundle;)V

    .line 33
    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 39
    const-string v1, "device"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/BleDevice;

    .line 40
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/BleDevice;->b()Ljava/lang/String;

    move-result-object v0

    .line 42
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ag;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 43
    sget v2, Lcom/google/android/gms/p;->hm:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ag;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 45
    sget v3, Lcom/google/android/gms/p;->hk:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 47
    const v0, 0x104000a

    new-instance v2, Lcom/google/android/gms/plus/apps/ah;

    invoke-direct {v2, p0}, Lcom/google/android/gms/plus/apps/ah;-><init>(Lcom/google/android/gms/plus/apps/ag;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 54
    const/high16 v0, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 55
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
