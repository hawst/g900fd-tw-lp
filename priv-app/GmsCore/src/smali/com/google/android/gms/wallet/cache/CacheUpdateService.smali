.class public Lcom/google/android/gms/wallet/cache/CacheUpdateService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# static fields
.field public static volatile a:Landroid/os/PowerManager$WakeLock;


# instance fields
.field volatile b:Landroid/os/Handler;

.field private c:Lcom/google/android/gms/wallet/cache/j;

.field private d:Ljava/util/LinkedList;

.field private e:Lcom/google/android/gms/wallet/cache/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 42
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->d:Ljava/util/LinkedList;

    return-void
.end method

.method private static declared-synchronized a(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;
    .locals 4

    .prologue
    .line 142
    const-class v1, Lcom/google/android/gms/wallet/cache/CacheUpdateService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 143
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 145
    const/4 v2, 0x1

    const-string v3, "CacheUpdateService"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 146
    sput-object v0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a:Landroid/os/PowerManager$WakeLock;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 148
    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a:Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()Lcom/android/volley/s;
    .locals 1

    .prologue
    .line 109
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Lcom/google/android/gms/wallet/cache/b;)V
    .locals 1

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->e:Lcom/google/android/gms/wallet/cache/b;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :goto_0
    monitor-exit p0

    return-void

    .line 85
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->e:Lcom/google/android/gms/wallet/cache/b;

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->b:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->e:Lcom/google/android/gms/wallet/cache/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :goto_0
    :try_start_1
    const-string v0, "CacheUpdateService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]finished"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->stopSelfResult(I)Z

    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 105
    :cond_0
    monitor-exit p0

    return-void

    .line 95
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/cache/b;

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->e:Lcom/google/android/gms/wallet/cache/b;

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->e:Lcom/google/android/gms/wallet/cache/b;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    :try_start_3
    const-string v1, "CacheUpdateService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]finished"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->stopSelfResult(I)Z

    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 103
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 105
    :cond_2
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 92
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Lcom/google/android/gms/wallet/cache/j;
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->c:Lcom/google/android/gms/wallet/cache/j;

    if-nez v0, :cond_0

    .line 115
    new-instance v0, Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/cache/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->c:Lcom/google/android/gms/wallet/cache/j;

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->c:Lcom/google/android/gms/wallet/cache/j;

    return-object v0
.end method

.method public final c()Landroid/accounts/AccountManager;
    .locals 1

    .prologue
    .line 121
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 60
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "CacheUpdateService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 61
    invoke-virtual {v0, p0}, Landroid/os/HandlerThread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 62
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 63
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->b:Landroid/os/Handler;

    .line 64
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    .line 68
    const-string v0, "CacheUpdateService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]CacheUpdateService started"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const-string v0, "CacheUpdateService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "trigger="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gms.wallet.cache.CacheUpdateConstants.EXTRA_TRIGGER"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    .line 73
    :cond_0
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 75
    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/cache/b;

    invoke-direct {v0, p0, p1, p3}, Lcom/google/android/gms/wallet/cache/b;-><init>(Lcom/google/android/gms/wallet/cache/CacheUpdateService;Landroid/content/Intent;I)V

    .line 76
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a(Lcom/google/android/gms/wallet/cache/b;)V

    .line 77
    const/4 v0, 0x3

    return v0
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 126
    const-string v0, "CacheUpdateService"

    const-string v1, "uncaught exception"

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 129
    invoke-static {p0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->a(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 130
    :goto_0
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/cache/CacheUpdateService;->stopSelf()V

    .line 134
    return-void
.end method
