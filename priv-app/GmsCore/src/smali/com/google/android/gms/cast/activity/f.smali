.class public final Lcom/google/android/gms/cast/activity/f;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 22
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/f;->a:Landroid/os/Handler;

    .line 23
    new-instance v0, Lcom/google/android/gms/cast/activity/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/activity/g;-><init>(Lcom/google/android/gms/cast/activity/f;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/f;->b:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/activity/f;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/f;->b:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/activity/f;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/f;->a:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 38
    sget v0, Lcom/google/android/gms/l;->aw:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 39
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a()V

    .line 41
    sget v0, Lcom/google/android/gms/j;->rM:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/cast/activity/h;

    invoke-direct {v2, p0}, Lcom/google/android/gms/cast/activity/h;-><init>(Lcom/google/android/gms/cast/activity/f;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    .line 64
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/f;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/cast/activity/f;->b:Ljava/lang/Runnable;

    sget-wide v2, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 67
    return-void
.end method
