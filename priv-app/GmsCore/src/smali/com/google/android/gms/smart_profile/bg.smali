.class public final Lcom/google/android/gms/smart_profile/bg;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/smart_profile/ad;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Lcom/google/android/gms/smart_profile/ak;

.field public e:Lcom/google/android/gms/plus/internal/ab;

.field private f:Ljava/lang/ref/WeakReference;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Landroid/graphics/Bitmap;

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

.field private n:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 67
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/bg;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->n:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Lcom/google/android/gms/smart_profile/bg;
    .locals 3

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/gms/smart_profile/bg;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/bg;-><init>()V

    .line 106
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 107
    const-string v2, "viewerAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v2, "viewerPageId"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v2, "qualifiedId"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v2, "applicationId"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 111
    const-string v2, "themeColor"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 112
    const-string v2, "callingPackage"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/bg;->setArguments(Landroid/os/Bundle;)V

    .line 114
    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/smart_profile/bg;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/bg;->w()V

    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/bj;

    .line 449
    :goto_0
    if-eqz v0, :cond_0

    .line 450
    iget-boolean v1, p0, Lcom/google/android/gms/smart_profile/bg;->h:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/smart_profile/bj;->a(Z)V

    .line 452
    :cond_0
    return-void

    .line 447
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/smart_profile/bf;->a(ILandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->n:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Landroid/content/Intent;)V

    .line 469
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/bg;->j:Landroid/graphics/Bitmap;

    .line 456
    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/bj;)V
    .locals 1

    .prologue
    .line 311
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->f:Ljava/lang/ref/WeakReference;

    .line 312
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 427
    iput-boolean p1, p0, Lcom/google/android/gms/smart_profile/bg;->h:Z

    .line 428
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->s()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 429
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/bg;->w()V

    .line 431
    :cond_1
    return-void
.end method

.method public final b()Lcom/google/android/gms/smart_profile/IdentityPersonUtil;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    return-object v0
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->b(Landroid/content/Intent;)V

    .line 478
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Z)V

    .line 227
    :cond_0
    return-void
.end method

.method public final c()Lcom/google/android/gms/smart_profile/ak;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->d:Lcom/google/android/gms/smart_profile/ak;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->j(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/google/android/gms/smart_profile/bg;->c:I

    return v0
.end method

.method public final h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->h()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->k()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->i:Ljava/lang/String;

    .line 252
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->m()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 349
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    if-nez v0, :cond_0

    .line 352
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    .line 354
    new-instance v1, Lcom/google/android/gms/plus/internal/cn;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/bg;->a:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/plus.native"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/gms/plus/internal/cn;->f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    .line 360
    new-instance v1, Lcom/google/android/gms/smart_profile/bi;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/smart_profile/bi;-><init>(Lcom/google/android/gms/smart_profile/bg;B)V

    .line 361
    sget-object v2, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1, v1}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->n:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_1

    .line 366
    new-instance v0, Lcom/google/android/gms/people/ad;

    invoke-direct {v0}, Lcom/google/android/gms/people/ad;-><init>()V

    iget v1, p0, Lcom/google/android/gms/smart_profile/bg;->c:I

    iput v1, v0, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v0}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    .line 369
    new-instance v1, Lcom/google/android/gms/smart_profile/bh;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/smart_profile/bh;-><init>(Lcom/google/android/gms/smart_profile/bg;B)V

    .line 370
    new-instance v2, Lcom/google/android/gms/common/api/w;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/bg;->a:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->n:Lcom/google/android/gms/common/api/v;

    .line 378
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-nez v0, :cond_2

    .line 379
    new-instance v0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/bg;->n:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/bg;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/bg;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/bg;->g:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/gms/smart_profile/bg;->c:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    .line 382
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->d:Lcom/google/android/gms/smart_profile/ak;

    if-nez v0, :cond_3

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 384
    new-instance v1, Lcom/google/android/gms/smart_profile/ak;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/bg;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/bg;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/bg;->l:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/gms/smart_profile/ak;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/bg;->d:Lcom/google/android/gms/smart_profile/ak;

    .line 388
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/ad;)V

    .line 389
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 336
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 338
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 339
    const-string v1, "viewerAccountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/bg;->a:Ljava/lang/String;

    .line 340
    const-string v1, "viewerPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/bg;->b:Ljava/lang/String;

    .line 341
    const-string v1, "qualifiedId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/bg;->g:Ljava/lang/String;

    .line 342
    const-string v1, "applicationId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/smart_profile/bg;->c:I

    .line 343
    const-string v1, "themeColor"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/smart_profile/bg;->k:I

    .line 344
    const-string v1, "callingPackage"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->l:Ljava/lang/String;

    .line 345
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 393
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->n:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->n:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 402
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/bg;->h:Z

    if-nez v0, :cond_3

    .line 403
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a()V

    .line 408
    :cond_2
    :goto_0
    return-void

    .line 404
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 406
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/bg;->a(Z)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 412
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 413
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->n:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->n:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 414
    :cond_1
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Lcom/google/android/gms/smart_profile/bg;->k:I

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 439
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/bg;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->j:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final u()Ljava/util/List;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/bg;->m:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Landroid/content/Context;
    .locals 1

    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    return-object v0
.end method
