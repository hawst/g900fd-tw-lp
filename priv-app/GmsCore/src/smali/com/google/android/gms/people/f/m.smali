.class public final Lcom/google/android/gms/people/f/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Landroid/os/ParcelFileDescriptor;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/os/ParcelFileDescriptor;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/gms/people/f/m;->a:Landroid/os/ParcelFileDescriptor;

    .line 44
    invoke-static {}, Lcom/google/android/gms/people/f/l;->b()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 45
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/gms/people/f/m;->b:Z

    if-eqz v0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/f/m;->b:Z

    .line 76
    invoke-static {}, Lcom/google/android/gms/people/f/l;->b()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 80
    if-eqz p1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/people/f/m;->a:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/os/ParcelFileDescriptor;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/people/f/m;->a:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 67
    const-string v0, "PipeCreator"

    const-string v1, "This log should only show up during unit tests."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/f/m;->a(Z)V

    .line 69
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/f/m;->a(Z)V

    .line 61
    return-void
.end method
