.class public final Lcom/google/android/gms/people/service/a/a/j;
.super Lcom/google/android/gms/people/service/a/a/e;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/people/model/AvatarReference;

.field private final h:Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;

.field private l:Lcom/google/android/gms/people/service/a/a/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/a/a/e;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V

    .line 24
    iput-object p5, p0, Lcom/google/android/gms/people/service/a/a/j;->g:Lcom/google/android/gms/people/model/AvatarReference;

    .line 25
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/a/j;->h:Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;

    .line 26
    return-void
.end method


# virtual methods
.method protected final b(Z)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/j;->l:Lcom/google/android/gms/people/service/a/a/e;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/j;->l:Lcom/google/android/gms/people/service/a/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/service/a/a/e;->a(Z)V

    .line 43
    :cond_0
    return-void
.end method

.method protected final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[avref: ref="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/j;->g:Lcom/google/android/gms/people/model/AvatarReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " opts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/j;->h:Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final h()Lcom/google/android/gms/people/service/a/a/f;
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/16 v12, 0x9

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 32
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/a/j;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/a/j;->b:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/people/service/a/a/j;->c:I

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/a/j;->k:Lcom/google/android/gms/people/service/d;

    iget-object v10, p0, Lcom/google/android/gms/people/service/a/a/j;->g:Lcom/google/android/gms/people/model/AvatarReference;

    iget-object v11, p0, Lcom/google/android/gms/people/service/a/a/j;->h:Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;

    invoke-virtual {v10}, Lcom/google/android/gms/people/model/AvatarReference;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v1, "Unsupported avatar reference"

    invoke-static {v9, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    const/4 v1, 0x0

    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/google/android/gms/people/service/a/a/j;->l:Lcom/google/android/gms/people/service/a/a/e;

    .line 35
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/j;->l:Lcom/google/android/gms/people/service/a/a/e;

    invoke-virtual {v1}, Lcom/google/android/gms/people/service/a/a/e;->h()Lcom/google/android/gms/people/service/a/a/f;

    move-result-object v1

    return-object v1

    .line 32
    :pswitch_0
    new-instance v1, Lcom/google/android/gms/people/service/a/a/k;

    invoke-virtual {v10}, Lcom/google/android/gms/people/model/AvatarReference;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v11}, Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;->b()I

    move-result v7

    invoke-virtual {v11}, Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;->c()I

    move-result v8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/a/k;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;II)V

    goto :goto_0

    :pswitch_1
    invoke-static {v10}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, v10, Lcom/google/android/gms/people/model/AvatarReference;->a:I

    const/4 v6, 0x2

    if-ne v1, v6, :cond_1

    move v1, v8

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v1, v10, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    invoke-virtual {v1, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    iget-object v6, v10, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v6, v12, v7}, Ljava/lang/String;->indexOf(II)I

    move-result v6

    if-lez v1, :cond_2

    move v1, v8

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    if-lez v6, :cond_3

    :goto_3
    invoke-static {v8}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v1, v10, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    move-object v7, v11

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/people/service/a/a/m;->a(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)Lcom/google/android/gms/people/service/a/a/e;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/people/service/a/a/i;

    move-object v6, v10

    move-object v7, v11

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/people/service/a/a/i;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)V

    goto :goto_0

    :cond_1
    move v1, v9

    goto :goto_1

    :cond_2
    move v1, v9

    goto :goto_2

    :cond_3
    move v8, v9

    goto :goto_3

    :pswitch_2
    invoke-static {v10}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, v10, Lcom/google/android/gms/people/model/AvatarReference;->a:I

    if-ne v1, v13, :cond_4

    move v1, v8

    :goto_4
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v1, v10, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    invoke-virtual {v1, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-lez v6, :cond_5

    move v1, v8

    :goto_5
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v1, v10, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    move-object v7, v11

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/people/service/a/a/m;->a(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)Lcom/google/android/gms/people/service/a/a/e;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {v10}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, v10, Lcom/google/android/gms/people/model/AvatarReference;->a:I

    if-ne v1, v13, :cond_6

    move v1, v8

    :goto_6
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v1, v10, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    invoke-virtual {v1, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-lez v6, :cond_7

    move v1, v8

    :goto_7
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v1, v10, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    invoke-virtual {v1, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v11}, Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;->d()Z

    move-result v1

    if-nez v1, :cond_8

    :goto_8
    new-instance v1, Lcom/google/android/gms/people/service/a/a/l;

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/a/l;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;JZ)V

    goto/16 :goto_0

    :cond_4
    move v1, v9

    goto :goto_4

    :cond_5
    move v1, v9

    goto :goto_5

    :cond_6
    move v1, v9

    goto :goto_6

    :cond_7
    move v1, v9

    goto :goto_7

    :cond_8
    move v8, v9

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
