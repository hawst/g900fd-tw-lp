.class final Lcom/google/android/gms/wallet/common/ui/o;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2270
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2272
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/p;

    invoke-direct {v0, v1, p4, v1}, Lcom/google/android/gms/wallet/common/ui/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/o;->insert(Ljava/lang/Object;I)V

    .line 2273
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 2310
    const/4 v0, 0x0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2294
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/o;->isEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2295
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->b:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2296
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/o;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->b:Landroid/view/View;

    .line 2297
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2298
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->b:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2300
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->b:Landroid/view/View;

    .line 2305
    :goto_0
    return-object v0

    .line 2302
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->b:Landroid/view/View;

    if-ne p2, v0, :cond_2

    .line 2303
    const/4 p2, 0x0

    .line 2305
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2277
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/o;->isEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2278
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->a:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 2279
    const/4 v0, 0x0

    invoke-super {p0, v0, v1, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->a:Landroid/widget/TextView;

    .line 2281
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/o;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2282
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2284
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->a:Landroid/widget/TextView;

    .line 2289
    :goto_0
    return-object v0

    .line 2286
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/o;->a:Landroid/widget/TextView;

    if-ne p2, v0, :cond_2

    move-object p2, v1

    .line 2289
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 2315
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
