.class public final Lcom/google/android/gms/plus/audience/CircleCreationActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/plus/audience/bn;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

.field private c:Lcom/google/android/gms/people/m;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/android/gms/common/people/data/AudienceMember;

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 387
    return-void
.end method

.method private final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 7

    .prologue
    .line 296
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->e:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/a/n;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->f:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    .line 299
    return-void
.end method

.method private final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V
    .locals 3

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 307
    new-instance v1, Lcom/google/android/gms/common/server/y;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/plus/a/n;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/y;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    .line 314
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/CircleCreationActivity;Lcom/google/android/gms/people/m;)V
    .locals 5

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->c:Lcom/google/android/gms/people/m;

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->b()Z

    move-result v1

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/people/m;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 284
    invoke-static {p1, p2}, Lcom/google/android/gms/plus/audience/bj;->a(Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/plus/audience/bj;

    move-result-object v0

    .line 286
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "errorDialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 289
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 205
    new-instance v0, Lcom/google/android/gms/common/audience/a/h;

    invoke-direct {v0, v1, v2, v1, v2}, Lcom/google/android/gms/common/audience/a/h;-><init>(ILcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/audience/a/h;->a()Landroid/content/Intent;

    move-result-object v0

    .line 211
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a(ILandroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V

    .line 212
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->sH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/f/a;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/f/e;

    move-result-object v0

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "progressDialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 145
    return-void
.end method

.method public final a(ILandroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 226
    new-instance v0, Lcom/google/android/gms/common/audience/a/h;

    invoke-direct {v0, p2}, Lcom/google/android/gms/common/audience/a/h;-><init>(Landroid/content/Intent;)V

    .line 227
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->setResult(ILandroid/content/Intent;)V

    .line 229
    iget v0, v0, Lcom/google/android/gms/common/audience/a/h;->b:I

    packed-switch v0, :pswitch_data_0

    .line 243
    :goto_0
    return-void

    .line 231
    :pswitch_0
    new-instance v0, Lcom/google/android/gms/common/audience/a/h;

    invoke-direct {v0, p2}, Lcom/google/android/gms/common/audience/a/h;-><init>(Landroid/content/Intent;)V

    new-array v1, v5, [Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/h;->a:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/plus/a/m;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v1}, Lcom/google/android/gms/common/analytics/e;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    iget v0, v0, Lcom/google/android/gms/common/audience/a/h;->c:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/gms/plus/a/m;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v1}, Lcom/google/android/gms/common/analytics/e;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->finish()V

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/gms/plus/a/m;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->g:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v0, Lcom/google/android/gms/p;->vU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    sget v1, Lcom/google/android/gms/p;->vV:I

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    sget v0, Lcom/google/android/gms/p;->vT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 234
    :pswitch_4
    sget-object v0, Lcom/google/android/gms/plus/a/m;->q:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 235
    sget v0, Lcom/google/android/gms/p;->tg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    .line 240
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->finish()V

    goto :goto_0

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 231
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    .line 343
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->d()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 346
    new-instance v1, Lcom/google/android/gms/plus/audience/bk;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/plus/audience/bk;-><init>(Lcom/google/android/gms/plus/audience/CircleCreationActivity;Lcom/google/android/gms/common/api/Status;)V

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->c:Lcom/google/android/gms/people/m;

    .line 347
    return-void
.end method

.method public final b()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 149
    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->i:Z

    if-eqz v1, :cond_1

    .line 167
    :cond_0
    :goto_0
    return v0

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-nez v1, :cond_2

    .line 154
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->c()V

    move v0, v6

    .line 155
    goto :goto_0

    .line 158
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->d()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->h:I

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/circles/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v6

    .line 167
    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-eqz v0, :cond_0

    .line 332
    :goto_0
    return-void

    .line 325
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/j;->c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/audience/bi;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/audience/bi;-><init>(Lcom/google/android/gms/plus/audience/CircleCreationActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 339
    :cond_0
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progressDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    .line 218
    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {v0}, Lcom/google/android/gms/plus/f/e;->a()V

    .line 221
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/q;->finish()V

    .line 222
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 189
    const/16 v0, 0x7d0

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    if-eq p2, v1, :cond_0

    if-ne p2, v1, :cond_1

    .line 192
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->i:Z

    .line 193
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "CircleCreationFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/bl;

    .line 196
    if-eqz v0, :cond_1

    .line 197
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/bl;->a()V

    .line 202
    :goto_0
    return-void

    .line 201
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->c()V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->c()V

    .line 173
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.common.audience.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->d:Ljava/lang/String;

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.common.audience.EXTRA_PAGE_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->e:Ljava/lang/String;

    .line 86
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->f:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/audience/a/g;->b(Landroid/content/Intent;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->g:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "CircleCreationFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/bl;

    .line 96
    if-nez v0, :cond_0

    .line 97
    new-instance v0, Lcom/google/android/gms/plus/audience/bl;

    invoke-direct {v0}, Lcom/google/android/gms/plus/audience/bl;-><init>()V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const v2, 0x1020002

    const-string v3, "CircleCreationFragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 104
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->i:Z

    .line 105
    if-nez p1, :cond_2

    .line 106
    iput-object v4, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    .line 107
    iput-object v4, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->c:Lcom/google/android/gms/people/m;

    .line 125
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/audience/a/g;->a(Landroid/content/Intent;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->h:I

    .line 127
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    sget-object v1, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    new-instance v2, Lcom/google/android/gms/people/ad;

    invoke-direct {v2}, Lcom/google/android/gms/people/ad;-><init>()V

    iget v3, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->h:I

    iput v3, v2, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v2}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a:Lcom/google/android/gms/common/api/v;

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 133
    :goto_1
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string v1, "CircleCreationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SecurityException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->c()V

    goto :goto_1

    .line 109
    :cond_2
    const-string v0, "addToCircleConsentData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->b:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    .line 112
    const-string v0, "addToCircleConsentDataResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "addToCircleConsentDataResultIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const-string v0, "addToCircleConsentDataResultCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "addToCircleConsentDataResultIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v4, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 121
    new-instance v0, Lcom/google/android/gms/plus/audience/bk;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/audience/bk;-><init>(Lcom/google/android/gms/plus/audience/CircleCreationActivity;Lcom/google/android/gms/common/api/Status;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->c:Lcom/google/android/gms/people/m;

    goto :goto_0
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 179
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/CircleCreationActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 184
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 185
    return-void
.end method
