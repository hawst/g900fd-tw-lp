.class public final Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/fitness/apiary/lso/d;

.field private static final i:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 597
    new-instance v0, Lcom/google/android/gms/fitness/apiary/lso/d;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/lso/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/d;

    .line 614
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 617
    sput-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->i:Ljava/util/HashMap;

    const-string v1, "android_package_name"

    const-string v2, "android_package_name"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->i:Ljava/util/HashMap;

    const-string v1, "certificate"

    const-string v2, "certificate"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->i:Ljava/util/HashMap;

    const-string v1, "client_id"

    const-string v2, "client_id"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->i:Ljava/util/HashMap;

    const-string v1, "ios_app_store_id"

    const-string v2, "ios_app_store_id"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 621
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->i:Ljava/util/HashMap;

    const-string v1, "ios_bundle_name"

    const-string v2, "ios_bundle_name"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 622
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->i:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 660
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 661
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->b:I

    .line 662
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->a:Ljava/util/Set;

    .line 663
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 675
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 676
    iput-object p1, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->a:Ljava/util/Set;

    .line 677
    iput p2, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->b:I

    .line 678
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->c:Ljava/lang/String;

    .line 679
    iput-object p4, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->d:Ljava/lang/String;

    .line 680
    iput-object p5, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->e:Ljava/lang/String;

    .line 681
    iput-object p6, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->f:Ljava/lang/String;

    .line 682
    iput-object p7, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->g:Ljava/lang/String;

    .line 683
    iput-object p8, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->h:Ljava/lang/String;

    .line 684
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 627
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 832
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 833
    packed-switch v0, :pswitch_data_0

    .line 853
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 835
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->c:Ljava/lang/String;

    .line 856
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 857
    return-void

    .line 838
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->d:Ljava/lang/String;

    goto :goto_0

    .line 841
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->e:Ljava/lang/String;

    goto :goto_0

    .line 844
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->f:Ljava/lang/String;

    goto :goto_0

    .line 847
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->g:Ljava/lang/String;

    goto :goto_0

    .line 850
    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->h:Ljava/lang/String;

    goto :goto_0

    .line 833
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 798
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 812
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 800
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->c:Ljava/lang/String;

    .line 810
    :goto_0
    return-object v0

    .line 802
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->d:Ljava/lang/String;

    goto :goto_0

    .line 804
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->e:Ljava/lang/String;

    goto :goto_0

    .line 806
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->f:Ljava/lang/String;

    goto :goto_0

    .line 808
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->g:Ljava/lang/String;

    goto :goto_0

    .line 810
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->h:Ljava/lang/String;

    goto :goto_0

    .line 798
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 717
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->a:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 783
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/d;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 874
    instance-of v0, p1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;

    if-nez v0, :cond_0

    move v0, v1

    .line 905
    :goto_0
    return v0

    .line 879
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 880
    goto :goto_0

    .line 883
    :cond_1
    check-cast p1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;

    .line 884
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 885
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 886
    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 888
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 890
    goto :goto_0

    :cond_3
    move v0, v1

    .line 895
    goto :goto_0

    .line 898
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 900
    goto :goto_0

    :cond_5
    move v0, v2

    .line 905
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 861
    const/4 v0, 0x0

    .line 862
    sget-object v1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->i:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 863
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 864
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 865
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 867
    goto :goto_0

    .line 868
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 788
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/d;

    invoke-static {p0, p1}, Lcom/google/android/gms/fitness/apiary/lso/d;->a(Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;Landroid/os/Parcel;)V

    .line 789
    return-void
.end method
