.class public Lcom/google/android/gms/plus/activity/AccountSignUpActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:I

.field private e:Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:Landroid/app/PendingIntent;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/android/gms/plus/activity/l;

.field private k:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

.field private final l:[Lcom/google/android/gms/plus/activity/j;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 71
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 673
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/gms/plus/activity/j;

    new-instance v1, Lcom/google/android/gms/plus/activity/f;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/plus/activity/f;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/plus/activity/e;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/e;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/plus/activity/d;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/d;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/gms/plus/activity/b;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/b;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/gms/plus/activity/c;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/c;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/google/android/gms/plus/activity/i;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/i;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/gms/plus/activity/g;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/activity/g;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;I)I
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g:Landroid/app/PendingIntent;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lcom/google/android/gms/plus/activity/l;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->j:Lcom/google/android/gms/plus/activity/l;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 765
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 766
    const-string v0, "AccountSignUpActivity"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 767
    const-string v0, "AccountSignUpActivity"

    const-string v1, "Resolution intents must be called with startIntentSenderForResult"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/common/analytics/x;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 775
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    .line 776
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    .line 777
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 781
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 782
    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/common/analytics/x;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/y;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 786
    sget-object v0, Lcom/google/android/gms/common/analytics/y;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lcom/google/android/gms/common/analytics/y;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, p1, v0, v1, p2}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 791
    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 6

    .prologue
    .line 796
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 797
    const/4 v2, 0x0

    sget-object v4, Lcom/google/android/gms/common/analytics/y;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 801
    sget-object v0, Lcom/google/android/gms/common/analytics/y;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lcom/google/android/gms/common/analytics/y;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, p1, v0, v1, p2}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 806
    :cond_0
    return-void
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 286
    const-string v0, "authAccount"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string v0, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string v0, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {p0, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v0, "com.google.android.gms.plus.intent.extra.SIGN_UP_STATE"

    invoke-virtual {p0, v0, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 291
    if-eqz p6, :cond_0

    .line 292
    const-string v0, "request_visible_actions"

    invoke-virtual {p0, v0, p6}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 294
    :cond_0
    const-string v0, "com.google.android.gms.plus.intent.extra.AUTH_SCOPE_STRING"

    invoke-virtual {p0, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const-string v0, "com.google.android.gms.plus.intent.extra.AUTH_TOKEN_INTENT"

    invoke-virtual {p0, v0, p7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 296
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 813
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e()I

    move-result v0

    .line 814
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 815
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    aget-object v0, v1, v0

    .line 816
    iget-boolean v1, v0, Lcom/google/android/gms/plus/activity/j;->c:Z

    if-nez v1, :cond_0

    .line 817
    invoke-virtual {v0}, Lcom/google/android/gms/plus/activity/j;->a()V

    .line 828
    :cond_0
    :goto_0
    return-void

    .line 823
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/common/analytics/x;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 826
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->setResult(I)V

    .line 827
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->finish()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c()V

    return-void
.end method

.method private e()I
    .locals 2

    .prologue
    .line 831
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 832
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/activity/j;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 837
    :goto_1
    return v0

    .line 831
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 837
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    array-length v0, v0

    goto :goto_1
.end method

.method static synthetic e(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->k:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c:Z

    return v0
.end method


# virtual methods
.method public final T_()V
    .locals 0

    .prologue
    .line 871
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c()V

    .line 872
    return-void
.end method

.method public final W_()V
    .locals 2

    .prologue
    .line 876
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e()I

    move-result v0

    .line 877
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 878
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/activity/j;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 880
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/plus/activity/j;->c:Z

    .line 883
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 866
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a()V

    .line 867
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 843
    packed-switch p1, :pswitch_data_0

    .line 857
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid request code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 845
    :pswitch_0
    if-ne p2, v6, :cond_0

    const-string v0, "authAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/common/analytics/x;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/common/analytics/y;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/y;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    iput-boolean v7, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c()V

    .line 854
    :goto_0
    return-void

    .line 845
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a()V

    goto :goto_0

    .line 849
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    if-ne p2, v6, :cond_1

    sget-object v2, Lcom/google/android/gms/common/analytics/x;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    sget-object v3, Lcom/google/android/gms/common/analytics/m;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/y;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    if-ne p2, v6, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d:I

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c()V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/gms/common/analytics/x;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a()V

    goto :goto_0

    .line 853
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    if-ne p2, v6, :cond_4

    sget-object v2, Lcom/google/android/gms/common/analytics/x;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_2
    sget-object v3, Lcom/google/android/gms/common/analytics/y;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/y;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_3
    if-ne p2, v6, :cond_5

    iput-object v8, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g:Landroid/app/PendingIntent;

    iput-object v8, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e:Ljava/lang/String;

    iput-boolean v7, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c:Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c()V

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/gms/common/analytics/x;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a()V

    goto :goto_0

    .line 843
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 107
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 108
    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->requestWindowFeature(I)Z

    .line 109
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const-string v0, "AccountSignUpActivity"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a()V

    .line 177
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 117
    if-eqz p1, :cond_3

    .line 126
    const-string v0, "stateIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/activity/j;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 129
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    aget-object v0, v1, v0

    iput-boolean v2, v0, Lcom/google/android/gms/plus/activity/j;->c:Z

    .line 131
    :cond_1
    const-string v0, "shouldSetDefaultAccount"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c:Z

    move-object v0, p1

    .line 162
    :cond_2
    const-string v1, "com.google.android.gms.common.oob.OOB_SIGN_UP"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "com.google.android.gms.common.oob.OOB_SIGN_UP is only available to first-party clients"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_3
    if-nez v0, :cond_4

    .line 135
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 138
    :cond_4
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    .line 139
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 140
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a()V

    goto :goto_0

    .line 148
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 149
    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 150
    const-string v2, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 155
    if-eqz v1, :cond_6

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    if-eqz v2, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 157
    :cond_7
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Calling/Auth package may only be set by GmsCore"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.common.oob.EXTRA_BACK_BUTTON_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->i:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->k:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->k:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    const-string v2, "gpsi0"

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/plus/k;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->i:Ljava/lang/String;

    if-nez v1, :cond_d

    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a()V

    .line 164
    :goto_2
    if-nez p1, :cond_9

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_9
    new-instance v0, Lcom/google/android/gms/plus/activity/l;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f:[Ljava/lang/String;

    invoke-direct {v0, p0, p0, p0, v1}, Lcom/google/android/gms/plus/activity/l;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->j:Lcom/google/android/gms/plus/activity/l;

    .line 176
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c()V

    goto/16 :goto_0

    .line 162
    :cond_a
    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    :cond_b
    const-string v1, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "com.google.android.gms.plus.intent.extra.AUTHENTICATION_PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->i:Ljava/lang/String;

    :goto_3
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    goto :goto_1

    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->i:Ljava/lang/String;

    goto :goto_3

    :cond_d
    const-string v1, "com.google.android.gms.plus.intent.extra.SIGN_UP_STATE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "com.google.android.gms.plus.intent.extra.SIGN_UP_STATE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d:I

    :goto_4
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f:[Ljava/lang/String;

    const-string v1, "request_visible_actions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "request_visible_actions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f:[Ljava/lang/String;

    :cond_e
    const-string v1, "com.google.android.gms.plus.intent.extra.AUTH_SCOPE_STRING"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e:Ljava/lang/String;

    const-string v1, "com.google.android.gms.plus.intent.extra.AUTH_TOKEN_INTENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g:Landroid/app/PendingIntent;

    goto :goto_2

    :cond_f
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d:I

    goto :goto_4
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 330
    invoke-super {p0}, Landroid/support/v4/app/q;->onPause()V

    .line 331
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v0, "progress_dialog"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->d(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    .line 332
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 324
    invoke-super {p0}, Landroid/support/v4/app/q;->onResume()V

    .line 325
    sget v0, Lcom/google/android/gms/p;->tY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v2

    const-string v0, "progress_dialog"

    invoke-virtual {v2, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    if-nez v0, :cond_0

    invoke-static {v1}, Lcom/google/android/gms/plus/f/e;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/f/e;

    move-result-object v0

    const/4 v1, 0x1

    sget v3, Lcom/google/android/gms/q;->C:I

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/plus/f/e;->a(II)V

    invoke-virtual {v2}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    .line 326
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 300
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 301
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->i:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d:I

    iget-object v5, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f:[Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g:Landroid/app/PendingIntent;

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 307
    invoke-direct {p0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e()I

    move-result v0

    .line 308
    const-string v1, "stateIndex"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 314
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->l:[Lcom/google/android/gms/plus/activity/j;

    aget-object v0, v1, v0

    iget-boolean v0, v0, Lcom/google/android/gms/plus/activity/j;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 319
    const-string v0, "shouldSetDefaultAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 320
    return-void

    .line 314
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 336
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->j:Lcom/google/android/gms/plus/activity/l;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->j:Lcom/google/android/gms/plus/activity/l;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/activity/l;->a()V

    .line 340
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 344
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->j:Lcom/google/android/gms/plus/activity/l;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->j:Lcom/google/android/gms/plus/activity/l;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/activity/l;->b()V

    .line 348
    :cond_0
    return-void
.end method
