.class public final Lcom/google/android/gms/cast/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:I

.field private static final d:Z

.field private static e:Lcom/google/android/gms/cast/d/a;

.field private static final f:Ljava/lang/Object;


# instance fields
.field private final g:Lcom/google/android/gms/cast/e/h;

.field private h:Landroid/os/Handler;

.field private final i:Landroid/net/wifi/WifiManager;

.field private final j:Ljava/lang/Object;

.field private final k:Landroid/content/Context;

.field private final l:Ljava/util/ArrayList;

.field private m:Lcom/google/android/gms/cast/d/b;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/gms/cast/a/b;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/cast/d/a;->a:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/cast/d/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/session/create"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/d/a;->b:Ljava/lang/String;

    .line 49
    sget-object v0, Lcom/google/android/gms/cast/a/b;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/d/a;->c:I

    .line 51
    sget-object v0, Lcom/google/android/gms/cast/a/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/cast/d/a;->d:Z

    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/d/a;->f:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/d/a;->j:Ljava/lang/Object;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/d/a;->l:Ljava/util/ArrayList;

    .line 125
    if-nez p1, :cond_0

    .line 126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/cast/d/a;->k:Landroid/content/Context;

    .line 129
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastNearbySessionManager"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    .line 131
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/gms/cast/d/a;->i:Landroid/net/wifi/WifiManager;

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    const-string v1, "starting a wifi scan request"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    iput v3, p0, Lcom/google/android/gms/cast/d/a;->n:I

    .line 135
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/os/Handler;)Lcom/google/android/gms/cast/d/a;
    .locals 2

    .prologue
    .line 109
    sget-object v1, Lcom/google/android/gms/cast/d/a;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 110
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/d/a;->e:Lcom/google/android/gms/cast/d/a;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Lcom/google/android/gms/cast/d/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/d/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/cast/d/a;->e:Lcom/google/android/gms/cast/d/a;

    .line 113
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    sget-object v0, Lcom/google/android/gms/cast/d/a;->e:Lcom/google/android/gms/cast/d/a;

    iput-object p1, v0, Lcom/google/android/gms/cast/d/a;->h:Landroid/os/Handler;

    .line 115
    sget-object v0, Lcom/google/android/gms/cast/d/a;->e:Lcom/google/android/gms/cast/d/a;

    return-object v0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/d/a;)Lcom/google/android/gms/cast/e/h;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/gms/cast/d/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    const-string v1, "addToPending: %s %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object p4, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 255
    iget-object v1, p0, Lcom/google/android/gms/cast/d/a;->l:Ljava/util/ArrayList;

    monitor-enter v1

    .line 256
    :try_start_0
    new-instance v0, Lcom/google/android/gms/cast/d/e;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/cast/d/e;-><init>(Lcom/google/android/gms/cast/d/a;B)V

    .line 257
    iput-object p1, v0, Lcom/google/android/gms/cast/d/e;->b:Lcom/google/android/gms/cast/CastDevice;

    .line 258
    iput-object p3, v0, Lcom/google/android/gms/cast/d/e;->c:Lcom/google/android/gms/cast/d/d;

    .line 259
    iput-object p2, v0, Lcom/google/android/gms/cast/d/e;->a:Lcom/google/android/gms/cast/b/o;

    .line 260
    iput-object p4, v0, Lcom/google/android/gms/cast/d/e;->d:Ljava/lang/String;

    .line 261
    iget-object v2, p0, Lcom/google/android/gms/cast/d/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    const-string v2, "checkNeedUserPinAndShow"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/gms/cast/d/a;->j:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v0, p0, Lcom/google/android/gms/cast/d/a;->n:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    const-string v3, "checkNeedUserPinAndShow - launching PIN dialog."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.cast.session.CAST_NEARBY_PIN_REQUEST"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x10010000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gms/cast/d/a;->k:Landroid/content/Context;

    const-class v4, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gms/cast/d/a;->k:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    new-instance v0, Lcom/google/android/gms/cast/d/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/d/b;-><init>(Lcom/google/android/gms/cast/d/a;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/d/a;->m:Lcom/google/android/gms/cast/d/b;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.gms.cast.session.CAST_NEARBY_PIN_RESPONSE"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/cast/d/a;->k:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/cast/d/a;->m:Lcom/google/android/gms/cast/d/b;

    invoke-virtual {v3, v4, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/cast/d/a;->n:I

    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    .line 263
    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    const-string v3, "checkNeedUserPinAndShow - PIN dialog active already."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 264
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/d/a;Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/cast/d/a;->a(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/d/a;Ljava/lang/String;Z)V
    .locals 12

    .prologue
    .line 43
    iget-object v10, p0, Lcom/google/android/gms/cast/d/a;->l:Ljava/util/ArrayList;

    monitor-enter v10

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    const-string v2, "onReceivedCastNearbyPIN. # of pendingSessions: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/cast/d/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/gms/cast/d/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/cast/d/e;

    move-object v2, v0

    new-instance v1, Lcom/google/android/gms/cast/d/c;

    if-eqz p2, :cond_0

    const/4 v4, 0x3

    :goto_1
    iget-object v5, v2, Lcom/google/android/gms/cast/d/e;->b:Lcom/google/android/gms/cast/CastDevice;

    iget-object v6, v2, Lcom/google/android/gms/cast/d/e;->a:Lcom/google/android/gms/cast/b/o;

    iget-object v7, v2, Lcom/google/android/gms/cast/d/e;->c:Lcom/google/android/gms/cast/d/d;

    const/4 v8, 0x0

    iget-object v9, v2, Lcom/google/android/gms/cast/d/e;->d:Ljava/lang/String;

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/cast/d/c;-><init>(Lcom/google/android/gms/cast/d/a;Ljava/lang/String;ILcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;ZLjava/lang/String;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/d/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v10

    throw v1

    :cond_0
    const/4 v4, 0x2

    goto :goto_1

    :cond_1
    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic b()I
    .locals 1

    .prologue
    .line 43
    sget v0, Lcom/google/android/gms/cast/d/a;->c:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/d/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->k:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/cast/d/a;)V
    .locals 4

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/android/gms/cast/d/a;->l:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/d/e;

    iget-object v0, v0, Lcom/google/android/gms/cast/d/e;->c:Lcom/google/android/gms/cast/d/d;

    const/16 v3, 0x7d2

    invoke-interface {v0, v3}, Lcom/google/android/gms/cast/d/d;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lcom/google/android/gms/cast/d/a;->d:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/cast/d/a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->l:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/cast/d/a;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->j:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/cast/d/a;)I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/d/a;->n:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/cast/d/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->h:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/cast/d/a;)Landroid/net/wifi/WifiManager;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->i:Landroid/net/wifi/WifiManager;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v9, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    const-string v1, "startSession for device %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v9

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    if-nez p4, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    const-string v1, "No cached pinCode available - requesting from user"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/google/android/gms/cast/d/a;->a(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;Ljava/lang/String;)V

    .line 171
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/d/a;->g:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Using cached pinCode %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p4, v2, v9

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    new-instance v0, Lcom/google/android/gms/cast/d/c;

    move-object v1, p0

    move-object v2, p4

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, v3

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/cast/d/c;-><init>(Lcom/google/android/gms/cast/d/a;Ljava/lang/String;ILcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/b/o;Lcom/google/android/gms/cast/d/d;ZLjava/lang/String;)V

    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/d/c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
