.class public final Lcom/google/android/gms/auth/setup/d2d/ah;
.super Landroid/app/DialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/setup/d2d/j;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private b:Lcom/google/android/gms/auth/setup/d2d/al;

.field private c:Landroid/os/Handler;

.field private d:Lcom/google/android/gms/auth/setup/d2d/a/b;

.field private e:Lcom/google/ab/b/a/e/h;

.field private f:Landroid/app/ProgressDialog;

.field private g:Ljava/lang/CharSequence;

.field private h:Lcom/google/android/gms/auth/setup/d2d/k;

.field private i:Lcom/google/android/gms/auth/be/e;

.field private j:Z

.field private k:Lcom/google/android/gms/droidguard/b;

.field private l:J


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TargetBootstrapFragment"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/ah;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->j:Z

    .line 96
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/ah;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->f:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/auth/setup/d2d/b/k;)Lcom/google/android/gms/auth/setup/d2d/b/l;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 324
    iget-object v2, p1, Lcom/google/android/gms/auth/setup/d2d/b/k;->a:Lcom/google/android/gms/auth/setup/d2d/b/i;

    iget-object v2, v2, Lcom/google/android/gms/auth/setup/d2d/b/i;->a:[Lcom/google/android/gms/auth/setup/d2d/b/a;

    .line 326
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 327
    array-length v4, v2

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v2, v1

    .line 328
    iget-object v5, v5, Lcom/google/android/gms/auth/setup/d2d/b/a;->c:[B

    invoke-virtual {v3, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 327
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 330
    :cond_0
    const-string v1, "SHA-1"

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 332
    if-nez v1, :cond_1

    .line 333
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Could not get SHA-1 Message Digest."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 334
    const/4 v1, 0x0

    new-array v1, v1, [B

    .line 338
    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/util/m;->c([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 344
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->d()Lcom/google/android/gms/droidguard/b;

    move-result-object v2

    .line 345
    if-nez v2, :cond_2

    .line 365
    :goto_2
    return-object v0

    .line 336
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_1

    .line 339
    :catch_0
    move-exception v1

    .line 340
    sget-object v2, Lcom/google/android/gms/auth/setup/d2d/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Could not convert assertions to bytes."

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 349
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->getActivity()Landroid/app/Activity;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 350
    const-string v3, "androidId"

    const-string v4, "encryptedUserAssertionHash"

    invoke-static {v3, v0, v4, v1}, Lcom/google/k/c/bo;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bo;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/android/gms/droidguard/b;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 355
    new-instance v2, Lcom/google/android/gms/auth/setup/d2d/b/h;

    invoke-direct {v2}, Lcom/google/android/gms/auth/setup/d2d/b/h;-><init>()V

    .line 356
    iput-object v0, v2, Lcom/google/android/gms/auth/setup/d2d/b/h;->a:Ljava/lang/String;

    .line 357
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/auth/setup/d2d/b/h;->c:Ljava/lang/String;

    .line 358
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/auth/setup/d2d/b/h;->b:Ljava/lang/String;

    .line 359
    const-string v0, "6777000"

    iput-object v0, v2, Lcom/google/android/gms/auth/setup/d2d/b/h;->e:Ljava/lang/String;

    .line 361
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/auth/setup/d2d/b/h;->d:Ljava/lang/String;

    .line 362
    iput-object v1, v2, Lcom/google/android/gms/auth/setup/d2d/b/h;->f:Ljava/lang/String;

    .line 364
    iget-object v0, p1, Lcom/google/android/gms/auth/setup/d2d/b/k;->a:Lcom/google/android/gms/auth/setup/d2d/b/i;

    iput-object v2, v0, Lcom/google/android/gms/auth/setup/d2d/b/i;->d:Lcom/google/android/gms/auth/setup/d2d/b/h;

    .line 365
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->i:Lcom/google/android/gms/auth/be/e;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/e;->a(Lcom/google/android/gms/auth/setup/d2d/b/k;)Lcom/google/android/gms/auth/setup/d2d/b/l;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/ah;Lcom/google/android/gms/auth/setup/d2d/k;)Lcom/google/android/gms/auth/setup/d2d/k;
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->h:Lcom/google/android/gms/auth/setup/d2d/k;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/auth/setup/d2d/ah;)J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->l:J

    return-wide v0
.end method

.method static synthetic b()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/ah;->a:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->j:Z

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->b:Lcom/google/android/gms/auth/setup/d2d/al;

    invoke-interface {v0}, Lcom/google/android/gms/auth/setup/d2d/al;->l()V

    .line 196
    return-void
.end method

.method private d()Lcom/google/android/gms/droidguard/b;
    .locals 3

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->k:Lcom/google/android/gms/droidguard/b;

    if-nez v0, :cond_0

    .line 380
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->b:Lcom/google/android/gms/auth/setup/d2d/al;

    invoke-interface {v0}, Lcom/google/android/gms/auth/setup/d2d/al;->m()Lcom/google/android/gms/droidguard/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->k:Lcom/google/android/gms/droidguard/b;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->k:Lcom/google/android/gms/droidguard/b;

    :goto_0
    return-object v0

    .line 381
    :catch_0
    move-exception v0

    .line 382
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Could not get DroidGuard snapshot."

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 383
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->j:Z

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->b:Lcom/google/android/gms/auth/setup/d2d/al;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/al;->a(I)V

    .line 191
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/ab/b/a/e/h;)V
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->e:Lcom/google/ab/b/a/e/h;

    .line 375
    return-void
.end method

.method public final a(Lcom/google/android/gms/auth/setup/d2d/a/b;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->d:Lcom/google/android/gms/auth/setup/d2d/a/b;

    .line 370
    return-void
.end method

.method public final a(Lcom/google/android/gms/auth/setup/d2d/b/g;)V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v0, 0x0

    .line 174
    iget-object v1, p1, Lcom/google/android/gms/auth/setup/d2d/b/g;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 175
    iget-object v1, p1, Lcom/google/android/gms/auth/setup/d2d/b/g;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->g:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->c:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/auth/setup/d2d/aj;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/auth/setup/d2d/aj;-><init>(Lcom/google/android/gms/auth/setup/d2d/ah;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 177
    :cond_0
    iget-boolean v1, p1, Lcom/google/android/gms/auth/setup/d2d/b/g;->c:Z

    if-eqz v1, :cond_2

    .line 178
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->c()V

    .line 184
    :cond_1
    :goto_0
    return-void

    .line 179
    :cond_2
    iget-object v1, p1, Lcom/google/android/gms/auth/setup/d2d/b/g;->d:Lcom/google/android/gms/auth/setup/d2d/b/b;

    if-eqz v1, :cond_4

    .line 180
    iget-object v0, p1, Lcom/google/android/gms/auth/setup/d2d/b/g;->d:Lcom/google/android/gms/auth/setup/d2d/b/b;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->l:J

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->i:Lcom/google/android/gms/auth/be/e;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/be/e;->a(Lcom/google/android/gms/auth/setup/d2d/b/b;)Lcom/google/android/gms/auth/setup/d2d/b/c;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/auth/setup/d2d/b/c;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->b:Lcom/google/android/gms/auth/setup/d2d/al;

    invoke-interface {v0, v12}, Lcom/google/android/gms/auth/setup/d2d/al;->a(I)V

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/b/g;

    invoke-direct {v1}, Lcom/google/android/gms/auth/setup/d2d/b/g;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/auth/setup/d2d/b/g;->e:Lcom/google/android/gms/auth/setup/d2d/b/c;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->h:Lcom/google/android/gms/auth/setup/d2d/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->h:Lcom/google/android/gms/auth/setup/d2d/k;

    invoke-interface {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/k;->a(Lcom/google/android/gms/auth/setup/d2d/b/g;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/ah;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 181
    :cond_4
    iget-object v1, p1, Lcom/google/android/gms/auth/setup/d2d/b/g;->f:Lcom/google/android/gms/auth/setup/d2d/b/k;

    if-eqz v1, :cond_1

    .line 182
    iget-object v1, p1, Lcom/google/android/gms/auth/setup/d2d/b/g;->f:Lcom/google/android/gms/auth/setup/d2d/b/k;

    iget-object v2, v1, Lcom/google/android/gms/auth/setup/d2d/b/k;->a:Lcom/google/android/gms/auth/setup/d2d/b/i;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/auth/setup/d2d/b/i;->g:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/setup/d2d/ah;->a(Lcom/google/android/gms/auth/setup/d2d/b/k;)Lcom/google/android/gms/auth/setup/d2d/b/l;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v1, v3, Lcom/google/android/gms/auth/setup/d2d/b/l;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    if-eqz v1, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->b:Lcom/google/android/gms/auth/setup/d2d/al;

    invoke-interface {v0, v12}, Lcom/google/android/gms/auth/setup/d2d/al;->a(I)V

    goto :goto_0

    :cond_6
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v2, v3, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    array-length v5, v2

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_7

    aget-object v6, v2, v1

    iget-object v7, v6, Lcom/google/android/gms/auth/setup/d2d/b/q;->a:Ljava/lang/String;

    invoke-interface {v4, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    new-instance v5, Ljava/util/ArrayList;

    iget-object v1, v3, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    iget-object v1, v1, Lcom/google/android/gms/auth/setup/d2d/b/j;->a:[Lcom/google/android/gms/auth/setup/d2d/b/p;

    array-length v1, v1

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v6, Ljava/util/ArrayList;

    iget-object v1, v3, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    iget-object v1, v1, Lcom/google/android/gms/auth/setup/d2d/b/j;->a:[Lcom/google/android/gms/auth/setup/d2d/b/p;

    array-length v1, v1

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, v3, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    iget-object v7, v1, Lcom/google/android/gms/auth/setup/d2d/b/j;->a:[Lcom/google/android/gms/auth/setup/d2d/b/p;

    array-length v8, v7

    move v2, v0

    move v1, v0

    :goto_2
    if-ge v2, v8, :cond_b

    aget-object v0, v7, v2

    iget v9, v0, Lcom/google/android/gms/auth/setup/d2d/b/p;->b:I

    if-nez v9, :cond_a

    iget-object v9, v0, Lcom/google/android/gms/auth/setup/d2d/b/p;->a:Ljava/lang/String;

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    const-string v10, "name"

    iget-object v11, v0, Lcom/google/android/gms/auth/setup/d2d/b/p;->a:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, v0, Lcom/google/android/gms/auth/setup/d2d/b/p;->d:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_9

    const-string v10, "credential"

    iget-object v11, v0, Lcom/google/android/gms/auth/setup/d2d/b/p;->e:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    iget-object v0, v0, Lcom/google/android/gms/auth/setup/d2d/b/p;->a:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/setup/d2d/b/q;

    if-eqz v0, :cond_8

    const-string v10, "firstName"

    iget-object v11, v0, Lcom/google/android/gms/auth/setup/d2d/b/q;->b:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "lastName"

    iget-object v0, v0, Lcom/google/android/gms/auth/setup/d2d/b/q;->c:Ljava/lang/String;

    invoke-virtual {v9, v10, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    :cond_9
    const-string v10, "url"

    iget-object v11, v0, Lcom/google/android/gms/auth/setup/d2d/b/p;->d:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_a
    sget-object v9, Lcom/google/android/gms/auth/setup/d2d/ah;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Account not OK: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/auth/setup/d2d/b/p;->a:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    goto :goto_4

    :cond_b
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->h()Lcom/google/android/gms/auth/setup/d2d/a;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/auth/setup/d2d/ak;

    invoke-direct {v2, p0, v1}, Lcom/google/android/gms/auth/setup/d2d/ak;-><init>(Lcom/google/android/gms/auth/setup/d2d/ah;I)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/setup/d2d/a;->a(Lcom/google/android/gms/auth/setup/d2d/c;)V

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->b:Lcom/google/android/gms/auth/setup/d2d/al;

    invoke-interface {v0, v12}, Lcom/google/android/gms/auth/setup/d2d/al;->a(I)V

    goto/16 :goto_0

    :cond_c
    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/b/g;

    invoke-direct {v1}, Lcom/google/android/gms/auth/setup/d2d/b/g;-><init>()V

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/auth/setup/d2d/b/g;->g:[Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->j:Z

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->h:Lcom/google/android/gms/auth/setup/d2d/k;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->h:Lcom/google/android/gms/auth/setup/d2d/k;

    invoke-interface {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/k;->a(Lcom/google/android/gms/auth/setup/d2d/b/g;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_d
    :goto_5
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->b:Lcom/google/android/gms/auth/setup/d2d/al;

    iget-object v1, v3, Lcom/google/android/gms/auth/setup/d2d/b/l;->c:Ljava/lang/String;

    iget-object v2, v3, Lcom/google/android/gms/auth/setup/d2d/b/l;->d:Ljava/lang/String;

    invoke-interface {v0, v6, v1, v2}, Lcom/google/android/gms/auth/setup/d2d/al;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/ah;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_5
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 101
    check-cast p1, Lcom/google/android/gms/auth/setup/d2d/al;

    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->b:Lcom/google/android/gms/auth/setup/d2d/al;

    .line 102
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->c()V

    .line 147
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 107
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/ah;->setRetainInstance(Z)V

    .line 109
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->d()Lcom/google/android/gms/droidguard/b;

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->d:Lcom/google/android/gms/auth/setup/d2d/a/b;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->e:Lcom/google/ab/b/a/e/h;

    if-nez v0, :cond_0

    .line 112
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->c()V

    .line 115
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->c:Landroid/os/Handler;

    .line 117
    new-instance v0, Lcom/google/android/gms/auth/be/e;

    new-instance v1, Lcom/google/android/gms/auth/a/c;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/auth/a/c;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/gms/auth/be/h;->a()Lcom/google/android/gms/auth/be/h;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/be/e;-><init>(Lcom/google/android/gms/auth/a/c;Lcom/google/android/gms/auth/be/h;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->i:Lcom/google/android/gms/auth/be/e;

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->d:Lcom/google/android/gms/auth/setup/d2d/a/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->e:Lcom/google/ab/b/a/e/h;

    new-instance v2, Lcom/google/android/gms/auth/setup/d2d/ai;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/setup/d2d/ai;-><init>(Lcom/google/android/gms/auth/setup/d2d/ah;)V

    invoke-static {v0, p0, v1, v2}, Lcom/google/android/gms/auth/setup/d2d/f;->a(Lcom/google/android/gms/auth/setup/d2d/a/b;Lcom/google/android/gms/auth/setup/d2d/j;Lcom/google/ab/b/a/e/h;Lcom/google/android/gms/auth/setup/d2d/g;)V

    .line 131
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 135
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->f:Landroid/app/ProgressDialog;

    .line 136
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->f:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->g:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->g:Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->f:Landroid/app/ProgressDialog;

    return-object v0

    .line 136
    :cond_0
    sget v0, Lcom/google/android/gms/p;->aN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/ah;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 160
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->j:Z

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->h:Lcom/google/android/gms/auth/setup/d2d/k;

    if-eqz v0, :cond_0

    .line 164
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->h:Lcom/google/android/gms/auth/setup/d2d/k;

    invoke-interface {v0}, Lcom/google/android/gms/auth/setup/d2d/k;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ah;->h:Lcom/google/android/gms/auth/setup/d2d/k;

    .line 170
    :cond_0
    return-void

    .line 165
    :catch_0
    move-exception v0

    .line 166
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/ah;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/ah;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 155
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 156
    return-void
.end method
