.class public final Lcom/google/android/gms/drive/auth/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:J


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/drive/database/r;

.field private final d:Lcom/google/android/gms/drive/d/f;

.field private final e:Lcom/google/android/gms/drive/g/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 41
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/drive/auth/f;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/d/f;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/gms/drive/auth/f;->b:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/drive/auth/f;->c:Lcom/google/android/gms/drive/database/r;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/drive/auth/f;->e:Lcom/google/android/gms/drive/g/i;

    .line 51
    iput-object p4, p0, Lcom/google/android/gms/drive/auth/f;->d:Lcom/google/android/gms/drive/d/f;

    .line 52
    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/util/Set;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/g;Z)Lcom/google/android/gms/drive/auth/d;
    .locals 9

    .prologue
    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/auth/f;->d:Lcom/google/android/gms/drive/d/f;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/drive/auth/f;->e:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    sget-wide v4, Lcom/google/android/gms/drive/auth/f;->a:J

    add-long v5, v0, v4

    .line 158
    new-instance v0, Lcom/google/android/gms/drive/auth/g;

    move-object v1, p4

    move-object v4, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/auth/g;-><init>(Lcom/google/android/gms/drive/database/model/a;JLcom/google/android/gms/drive/auth/AppIdentity;JLjava/util/Set;)V

    .line 160
    iget-object v1, p0, Lcom/google/android/gms/drive/auth/f;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/database/r;->d(Lcom/google/android/gms/drive/auth/g;)V

    .line 161
    sget-object v1, Lcom/google/android/gms/drive/auth/e;->a:Lcom/google/android/gms/drive/auth/e;

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/auth/d;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 182
    :goto_0
    return-object v0

    .line 162
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 165
    if-eqz p6, :cond_0

    .line 166
    sget-object v0, Lcom/google/android/gms/drive/auth/e;->b:Lcom/google/android/gms/drive/auth/e;

    invoke-static {v0, p5}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto :goto_0

    .line 167
    :cond_0
    if-nez p5, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    sget-object v8, Lcom/google/android/gms/drive/auth/e;->b:Lcom/google/android/gms/drive/auth/e;

    new-instance v0, Lcom/google/android/gms/drive/auth/g;

    const-wide v2, 0xad91d7beddL

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/f;->e:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v1}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v5

    move-object v1, p4

    move-object v4, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/auth/g;-><init>(Lcom/google/android/gms/drive/database/model/a;JLcom/google/android/gms/drive/auth/AppIdentity;JLjava/util/Set;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/f;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/database/r;->d(Lcom/google/android/gms/drive/auth/g;)V

    invoke-static {v8, v0}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto :goto_0

    .line 173
    :cond_1
    sget-object v2, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    invoke-virtual {v1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lcom/android/volley/a;

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lcom/android/volley/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/g/b;->a(Lcom/android/volley/a;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/google/android/gms/drive/auth/c;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "server returned error: %s. See https://developers.google.com/drive/handle-errors for details."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-static {v2, v0}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v1, Lcom/google/android/gms/drive/auth/c;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "server returned response code %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, v0, Lcom/android/volley/a;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/google/android/gms/drive/auth/c;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/Exception;)V

    goto :goto_1

    .line 175
    :catch_1
    move-exception v0

    .line 176
    iget-object v1, p0, Lcom/google/android/gms/drive/auth/f;->c:Lcom/google/android/gms/drive/database/r;

    iget-wide v2, p4, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-interface {v1, v2, v3, p2}, Lcom/google/android/gms/drive/database/r;->b(JLcom/google/android/gms/drive/auth/AppIdentity;)V

    .line 177
    sget-object v1, Lcom/google/android/gms/drive/auth/e;->c:Lcom/google/android/gms/drive/auth/e;

    new-instance v2, Lcom/google/android/gms/drive/auth/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/Exception;Landroid/content/Intent;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto/16 :goto_0

    .line 180
    :catch_2
    move-exception v0

    .line 181
    iget-object v1, p0, Lcom/google/android/gms/drive/auth/f;->c:Lcom/google/android/gms/drive/database/r;

    iget-wide v2, p4, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-interface {v1, v2, v3, p2}, Lcom/google/android/gms/drive/database/r;->b(JLcom/google/android/gms/drive/auth/AppIdentity;)V

    .line 182
    sget-object v1, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    new-instance v2, Lcom/google/android/gms/drive/auth/c;

    const-string v3, "See https://developers.google.com/drive/android/auth for details on authorizing an application."

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/drive/auth/d;
    .locals 12

    .prologue
    const/4 v9, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 67
    const-class v2, Lcom/google/android/gms/drive/aa;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v3

    .line 68
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v5

    .line 69
    invoke-static {}, Lcom/google/android/gms/drive/aa;->values()[Lcom/google/android/gms/drive/aa;

    move-result-object v4

    array-length v6, v4

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_2

    aget-object v7, v4, v2

    .line 70
    invoke-virtual {v7}, Lcom/google/android/gms/drive/aa;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 71
    invoke-virtual {v7}, Lcom/google/android/gms/drive/aa;->c()Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/gms/drive/auth/f;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-static {v8, v5}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 73
    sget-object v2, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    new-instance v3, Lcom/google/android/gms/drive/auth/c;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Unsupported scope: %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/aa;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v1

    invoke-static {v4, v5, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    .line 133
    :goto_1
    return-object v0

    .line 77
    :cond_0
    invoke-interface {v3, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 69
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 82
    :cond_2
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 83
    sget-object v0, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    new-instance v1, Lcom/google/android/gms/drive/auth/c;

    const-string v2, "No valid Drive authorization scope provided."

    invoke-direct {v1, v2}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto :goto_1

    .line 87
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->a()I

    move-result v2

    .line 88
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 89
    sget-object v2, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    new-instance v3, Lcom/google/android/gms/drive/auth/c;

    const-string v4, "Auth package name \'%s\' did not match calling package name \'%s\'"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v5, v6, v1

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto :goto_1

    .line 94
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v4

    .line 95
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 96
    sget-object v2, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    new-instance v3, Lcom/google/android/gms/drive/auth/c;

    const-string v5, "Requested account name \'%s\' did not match resolved account name \'%s\'"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v4, v6, v1

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto :goto_1

    .line 101
    :cond_5
    iget-object v6, p0, Lcom/google/android/gms/drive/auth/f;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v6, v4}, Lcom/google/android/gms/drive/database/r;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/a;

    move-result-object v4

    .line 103
    iget-object v6, p0, Lcom/google/android/gms/drive/auth/f;->b:Landroid/content/Context;

    invoke-static {v6, v2, v5}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 104
    sget-object v3, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    new-instance v4, Lcom/google/android/gms/drive/auth/c;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Package %s is not valid for uid %d."

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v5, v8, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v0

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto/16 :goto_1

    .line 111
    :cond_6
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/auth/f;->b:Landroid/content/Context;

    invoke-static {v2, v5}, Lcom/google/android/gms/common/util/e;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 112
    if-nez v2, :cond_7

    .line 113
    sget-object v2, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    new-instance v3, Lcom/google/android/gms/drive/auth/c;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "No certificates for %s from package manager."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-static {v4, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_1

    .line 118
    :catch_0
    move-exception v2

    sget-object v2, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    new-instance v3, Lcom/google/android/gms/drive/auth/c;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Package not found: %s"

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v5, v0, v1

    invoke-static {v4, v6, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto/16 :goto_1

    .line 121
    :cond_7
    invoke-static {v5, v2}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v2

    .line 123
    iget-object v5, p0, Lcom/google/android/gms/drive/auth/f;->c:Lcom/google/android/gms/drive/database/r;

    iget-wide v6, v4, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-interface {v5, v6, v7, v2}, Lcom/google/android/gms/drive/database/r;->a(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v5

    .line 124
    if-eqz v5, :cond_8

    iget-object v6, v5, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    invoke-interface {v6, v3}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v6

    if-eqz v6, :cond_8

    move v6, v0

    .line 126
    :goto_2
    if-eqz v5, :cond_a

    iget-object v7, p0, Lcom/google/android/gms/drive/auth/f;->e:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v7}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v8

    iget-wide v10, v5, Lcom/google/android/gms/drive/auth/g;->d:J

    cmp-long v7, v10, v8

    if-gtz v7, :cond_9

    move v7, v0

    :goto_3
    if-nez v7, :cond_a

    .line 129
    :goto_4
    if-eqz v6, :cond_b

    if-eqz v0, :cond_b

    .line 130
    sget-object v0, Lcom/google/android/gms/drive/auth/e;->a:Lcom/google/android/gms/drive/auth/e;

    invoke-static {v0, v5}, Lcom/google/android/gms/drive/auth/d;->a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    move v6, v1

    .line 124
    goto :goto_2

    :cond_9
    move v7, v1

    .line 126
    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_4

    :cond_b
    move-object v0, p0

    move-object v1, p1

    .line 133
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/auth/f;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/util/Set;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/g;Z)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    goto/16 :goto_1
.end method
