.class public Lcom/google/android/gms/backup/SetBackupAccountService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "Backup"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "SetBackupAccountService"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/backup/SetBackupAccountService;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "SetBackupAccountService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v4, 0x3

    .line 43
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    sget-object v0, Lcom/google/android/gms/backup/SetBackupAccountService;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/backup/SetBackupAccountService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "SetBackupAccountService invoked"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 48
    :cond_2
    const-string v0, "backupAccount"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 49
    const-string v1, "backupUserHandle"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/UserHandle;

    .line 50
    if-eqz v0, :cond_0

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.google"

    iget-object v3, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x11

    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/os/UserHandle;->isOwner()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    :cond_3
    sget-object v1, Lcom/google/android/gms/backup/SetBackupAccountService;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/gms/backup/SetBackupAccountService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Setting backup account"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 58
    :cond_4
    new-instance v1, Lcom/google/android/gms/backup/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/backup/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/backup/b;->a(Landroid/accounts/Account;)V

    goto :goto_0
.end method
