.class public final Lcom/google/android/gms/cast/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 87
    const-string v0, "gms:cast:media:video_bitrate"

    const v1, 0x5b8d80

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/d;->a:Lcom/google/android/gms/common/a/d;

    .line 90
    const-string v0, "gms:cast:media:target_delay"

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/d;->b:Lcom/google/android/gms/common/a/d;

    .line 93
    const-string v0, "gms:cast:media:generic_player_app_id"

    const-string v1, "CC1AD845"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/d;->c:Lcom/google/android/gms/common/a/d;

    .line 97
    const-string v0, "gms:cast:media:use_tdls"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/d;->d:Lcom/google/android/gms/common/a/d;

    .line 100
    const-string v0, "gms:cast:media:stats_flags"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/d;->e:Lcom/google/android/gms/common/a/d;

    return-void
.end method
