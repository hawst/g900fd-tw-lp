.class final Lcom/google/android/gms/drive/ui/create/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/ui/create/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/ui/create/a;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/create/c;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/c;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/create/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/c;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v1}, Lcom/google/android/gms/drive/ui/create/a;->d(Lcom/google/android/gms/drive/ui/create/a;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/create/c;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v2}, Lcom/google/android/gms/drive/ui/create/a;->e(Lcom/google/android/gms/drive/ui/create/a;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/create/c;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v4}, Lcom/google/android/gms/drive/ui/create/a;->f(Lcom/google/android/gms/drive/ui/create/a;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "application/vnd.google-apps.folder"

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/i;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/c;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-static {v1}, Lcom/google/android/gms/drive/ui/create/a;->c(Lcom/google/android/gms/drive/ui/create/a;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/i;->a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/i;->a()Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/c;->a:Lcom/google/android/gms/drive/ui/create/a;

    sget v2, Lcom/google/android/gms/p;->fN:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/ui/create/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/i;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    .line 293
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/c;->a:Lcom/google/android/gms/drive/ui/create/a;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/gms/drive/ui/create/a;->startActivityForResult(Landroid/content/Intent;I)V

    .line 294
    return-void
.end method
