.class public final Lcom/google/android/gms/location/reporting/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/er;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/location/reporting/a/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcom/google/android/gms/location/reporting/a/f;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/gms/location/reporting/a/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    .line 71
    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/reporting/a/f;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/a/f;->a()V

    .line 163
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/es;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/reporting/a/f;->a(Lcom/google/android/gms/common/es;)V

    .line 184
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/et;)V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/reporting/a/f;->a(Lcom/google/android/gms/common/et;)V

    .line 201
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/a/f;->b()V

    .line 178
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/es;)Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/reporting/a/f;->b(Lcom/google/android/gms/common/es;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/gms/common/es;)V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/reporting/a/f;->c(Lcom/google/android/gms/common/es;)V

    .line 195
    return-void
.end method

.method public final c_()Z
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/a/f;->c_()Z

    move-result v0

    return v0
.end method

.method public final n_()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/i;->a:Lcom/google/android/gms/location/reporting/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/a/f;->c_()Z

    move-result v0

    return v0
.end method
