.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/database/r;

.field final b:Lcom/google/android/gms/drive/g/n;

.field final c:Lcom/google/android/gms/drive/g/ae;

.field final d:Lcom/google/android/gms/drive/c/b;

.field final e:Ljava/util/Set;

.field private final f:Lcom/google/android/gms/drive/g/aw;

.field private final g:Lcom/google/android/gms/drive/d/f;

.field private final h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;

.field private final i:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;)V
    .locals 2

    .prologue
    .line 118
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;

    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    invoke-direct {v1, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;)V

    .line 120
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->e:Ljava/util/Set;

    .line 124
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->f:Lcom/google/android/gms/drive/g/aw;

    .line 125
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    .line 126
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->e()Lcom/google/android/gms/drive/g/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->b:Lcom/google/android/gms/drive/g/n;

    .line 127
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->m()Lcom/google/android/gms/drive/g/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->c:Lcom/google/android/gms/drive/g/ae;

    .line 128
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->g:Lcom/google/android/gms/drive/d/f;

    .line 129
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;

    .line 130
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->j()Lcom/google/android/gms/drive/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->d:Lcom/google/android/gms/drive/c/b;

    .line 131
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->i:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    .line 132
    return-void
.end method

.method private a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 341
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->f:Lcom/google/android/gms/drive/g/aw;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    .line 342
    invoke-interface {p2, v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Landroid/content/SyncResult;)V

    .line 343
    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a(Landroid/content/SyncResult;)V

    .line 345
    invoke-interface {p2, p1, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;->a(Landroid/content/SyncResult;Z)V

    .line 348
    return v2
.end method


# virtual methods
.method final a(ZZLcom/google/android/gms/drive/database/model/a;Landroid/content/SyncResult;ILcom/google/android/gms/drive/database/model/c;ILcom/google/android/gms/drive/c/a;)Z
    .locals 10

    .prologue
    .line 238
    sget-object v0, Lcom/google/android/gms/drive/ai;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 241
    invoke-static {p3}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/g;->b()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->g:Lcom/google/android/gms/drive/d/f;

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/drive/database/model/c;->g()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    add-int/lit8 v4, v8, 0x1

    int-to-long v4, v4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;JJ)Lcom/google/android/gms/drive/d/a;

    move-result-object v2

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->i:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a(Lcom/google/android/gms/drive/database/model/a;)V

    .line 252
    invoke-interface {v2}, Lcom/google/android/gms/drive/d/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    const-string v0, "SyncManager"

    const-string v1, "remainingChangestamps is missing so failing sync."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const/4 v0, 0x0

    .line 335
    :goto_0
    return v0

    .line 257
    :cond_0
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/drive/database/model/c;->a()Z

    move-result v3

    .line 258
    invoke-interface {v2}, Lcom/google/android/gms/drive/d/a;->b()J

    move-result-wide v6

    .line 259
    int-to-long v0, v8

    cmp-long v0, v6, v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    move v1, v0

    .line 260
    :goto_1
    if-nez v3, :cond_1

    if-eqz v1, :cond_4

    :cond_1
    const/4 v5, 0x1

    .line 261
    :goto_2
    if-nez v5, :cond_2

    if-nez p1, :cond_5

    :cond_2
    const/4 v0, 0x1

    .line 267
    :goto_3
    if-eqz v0, :cond_9

    .line 268
    const-string v0, "SyncManager"

    const-string v4, "Starting full sync. forceFullSync:%s, restartFullSync:%s, fullSyncComplete:%s, needCatchup:%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-static {v0, v4, v6}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 271
    if-eqz v3, :cond_6

    const/4 v0, 0x3

    move v7, v0

    .line 277
    :goto_4
    invoke-interface {v2}, Lcom/google/android/gms/drive/d/a;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v6

    .line 279
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;

    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;

    iget-object v1, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;->a:Lcom/google/android/gms/drive/g/aw;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    move-object v3, p3

    move v4, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;Lcom/google/android/gms/drive/database/model/a;IZI)V

    .line 281
    invoke-direct {p0, p4, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;)Z

    move v1, v7

    .line 314
    :goto_5
    iget-object v0, p4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    move-object/from16 v0, p8

    move/from16 v4, p7

    move v5, p5

    move v6, v8

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/drive/c/a;->a(IJIII)Lcom/google/android/gms/drive/c/a;

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 326
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p3, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v0

    .line 328
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/c;->a(Ljava/util/Date;)V

    .line 329
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/c;->i()V

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 335
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 259
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_1

    .line 260
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 261
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 271
    :cond_6
    if-eqz v1, :cond_7

    const/4 v0, 0x2

    move v7, v0

    goto :goto_4

    :cond_7
    if-eqz p2, :cond_8

    const/4 v0, 0x0

    move v7, v0

    goto :goto_4

    :cond_8
    const/4 v0, 0x5

    move v7, v0

    goto :goto_4

    .line 283
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;

    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;

    iget-object v1, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;->a:Lcom/google/android/gms/drive/g/aw;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    invoke-direct {v2, v1, p3, p5, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/a;ILcom/google/android/gms/drive/metadata/sync/syncadapter/g;)V

    .line 285
    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->a()Z

    move-result v0

    .line 287
    const-wide/16 v4, 0x0

    cmp-long v1, v6, v4

    if-lez v1, :cond_b

    .line 288
    const-string v1, "SyncManager"

    const-string v3, "Starting changelog sync. remainingChangestamps:%s, needAppDataSync:%s, "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 291
    const/4 v1, 0x1

    .line 292
    if-eqz v0, :cond_a

    invoke-direct {p0, p4, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;)Z

    .line 294
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;

    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;

    iget-object v3, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;->a:Lcom/google/android/gms/drive/g/aw;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/g;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    invoke-direct {v2, v3, v0, p3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/a;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;Lcom/google/android/gms/drive/database/model/a;)V

    .line 300
    invoke-direct {p0, p4, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;)Z

    goto/16 :goto_5

    .line 306
    :cond_b
    if-eqz v0, :cond_c

    .line 307
    const-string v0, "SyncManager"

    const-string v1, "No changelog sync required. Only syncing appdata for recently installed apps."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    const/4 v1, 0x4

    .line 310
    invoke-direct {p0, p4, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;)Z

    goto/16 :goto_5

    .line 312
    :cond_c
    const-string v0, "SyncManager"

    const-string v1, "Nothing to sync. Bailing out."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const/4 v1, 0x6

    goto/16 :goto_5

    .line 332
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/p;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0
.end method
