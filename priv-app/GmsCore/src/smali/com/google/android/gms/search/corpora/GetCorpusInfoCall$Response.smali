.class public Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ap;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/corpora/e;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lcom/google/android/gms/search/corpora/e;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->CREATOR:Lcom/google/android/gms/search/corpora/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->c:I

    .line 114
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput p1, p0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->c:I

    .line 131
    iput-object p2, p0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 132
    iput-object p3, p0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->b:Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    .line 133
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->CREATOR:Lcom/google/android/gms/search/corpora/e;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->CREATOR:Lcom/google/android/gms/search/corpora/e;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/corpora/e;->a(Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;Landroid/os/Parcel;I)V

    .line 145
    return-void
.end method
