.class public final Lcom/google/android/gms/drive/d/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/d/a;


# instance fields
.field private final a:Lcom/google/android/gms/drive/internal/model/About;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/internal/model/About;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/model/About;

    iput-object v0, p0, Lcom/google/android/gms/drive/d/a/a;->a:Lcom/google/android/gms/drive/internal/model/About;

    .line 19
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/a;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->e()Z

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/a;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 29
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/a;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/a;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    const-wide/16 v0, 0x0

    .line 37
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/d/a/a;->a:Lcom/google/android/gms/drive/internal/model/About;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/About;->b()J

    move-result-wide v0

    goto :goto_0
.end method
