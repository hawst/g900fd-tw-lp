.class public final Lcom/google/android/gms/audiomodem/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/audiomodem/ba;
.implements Lcom/google/android/gms/audiomodem/x;


# instance fields
.field final a:Lcom/google/android/gms/audiomodem/af;

.field private final b:[Z

.field private final c:Lcom/google/android/gms/audiomodem/t;

.field private final d:Lcom/google/android/gms/audiomodem/aw;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->b:[Z

    .line 41
    new-instance v0, Lcom/google/android/gms/audiomodem/t;

    invoke-direct {v0, p1, p0}, Lcom/google/android/gms/audiomodem/t;-><init>(Landroid/content/Context;Lcom/google/android/gms/audiomodem/x;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->c:Lcom/google/android/gms/audiomodem/t;

    .line 42
    new-instance v0, Lcom/google/android/gms/audiomodem/af;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/af;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->a:Lcom/google/android/gms/audiomodem/af;

    .line 43
    new-instance v0, Lcom/google/android/gms/audiomodem/aw;

    invoke-direct {v0, p1}, Lcom/google/android/gms/audiomodem/aw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->d:Lcom/google/android/gms/audiomodem/aw;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->b:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_1

    .line 195
    const-string v0, "audioModem"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenEncoderManager: Retrying playback on track "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/audiomodem/bk;->b(I)V

    .line 200
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x2

    .line 186
    if-eqz p1, :cond_0

    .line 187
    iget v1, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    if-ne v1, v3, :cond_1

    iput v2, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    :goto_0
    if-ge v0, v3, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/audiomodem/bk;->b(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_0
    iget v1, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    if-ne v1, v2, :cond_1

    iput v3, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    :goto_1
    if-ge v0, v3, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bk;->a:Lcom/google/android/gms/audiomodem/af;

    iget-object v1, v1, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/gms/audiomodem/p;->a(I)V

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bk;->c:Lcom/google/android/gms/audiomodem/t;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/audiomodem/t;->b(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 191
    :cond_1
    return-void
.end method

.method final b(I)V
    .locals 14

    .prologue
    const/4 v2, 0x2

    const-wide/16 v12, -0x1

    const/4 v7, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->a:Lcom/google/android/gms/audiomodem/af;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    aget-object v10, v0, p1

    .line 96
    if-nez v10, :cond_1

    .line 97
    const-string v0, "audioModem"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenEncoderManager: Cannot play, current broadcastRequest on track "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget v0, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    :goto_1
    if-eq v0, v8, :cond_7

    .line 105
    const-string v0, "audioModem"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenEncoderManager: Cannot play, mPlaybackCapabilityRequestState = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_2
    iget-object v0, v10, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/audiomodem/p;->a(I)V

    goto :goto_0

    .line 104
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->d:Lcom/google/android/gms/audiomodem/aw;

    iget-object v1, v0, Lcom/google/android/gms/audiomodem/aw;->c:Lcom/google/android/gms/audiomodem/ba;

    if-eqz v1, :cond_4

    const-string v1, "audioModem"

    const/4 v3, 0x5

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "PlaybackCapabilityHelper: requestStartPlayback() called during outstanding request"

    const-string v3, "audioModem"

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iput-object p0, v0, Lcom/google/android/gms/audiomodem/aw;->c:Lcom/google/android/gms/audiomodem/ba;

    iget-boolean v1, v0, Lcom/google/android/gms/audiomodem/aw;->e:Z

    if-nez v1, :cond_5

    iget-object v1, v0, Lcom/google/android/gms/audiomodem/aw;->a:Landroid/content/Context;

    iget-object v3, v0, Lcom/google/android/gms/audiomodem/aw;->g:Landroid/content/BroadcastReceiver;

    iget-object v4, v0, Lcom/google/android/gms/audiomodem/aw;->f:Landroid/content/IntentFilter;

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/google/android/gms/audiomodem/aw;->b:Landroid/os/Handler;

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iput-boolean v8, v0, Lcom/google/android/gms/audiomodem/aw;->e:Z

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/aw;->a()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/audiomodem/aw;->d:Z

    iget-boolean v0, v0, Lcom/google/android/gms/audiomodem/aw;->d:Z

    if-eqz v0, :cond_6

    move v0, v8

    :goto_2
    iput v0, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    iget v0, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    .line 114
    :cond_7
    iget-object v0, v10, Lcom/google/android/gms/audiomodem/bl;->b:Lcom/google/android/gms/audiomodem/Encoding;

    .line 117
    new-instance v1, Lcom/google/android/gms/audiomodem/TokenEncoder;

    invoke-direct {v1, v0}, Lcom/google/android/gms/audiomodem/TokenEncoder;-><init>(Lcom/google/android/gms/audiomodem/Encoding;)V

    .line 118
    iget-object v0, v10, Lcom/google/android/gms/audiomodem/bl;->d:[B

    invoke-virtual {v1, v0}, Lcom/google/android/gms/audiomodem/TokenEncoder;->a([B)Lcom/google/whispernet/Data$EncodeResults;

    move-result-object v0

    .line 119
    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/TokenEncoder;->a()V

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bk;->c:Lcom/google/android/gms/audiomodem/t;

    iget-object v4, v0, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    iget-object v5, v0, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    iget-object v0, v10, Lcom/google/android/gms/audiomodem/bl;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/Encoding;->e()F

    move-result v0

    float-to-int v3, v0

    iget v0, v10, Lcom/google/android/gms/audiomodem/bl;->c:I

    packed-switch v0, :pswitch_data_0

    if-ltz v0, :cond_a

    iget-object v6, p0, Lcom/google/android/gms/audiomodem/bk;->c:Lcom/google/android/gms/audiomodem/t;

    iget-object v6, v6, Lcom/google/android/gms/audiomodem/t;->b:Landroid/media/AudioManager;

    invoke-virtual {v6, v7}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v6

    if-gt v0, v6, :cond_a

    :goto_3
    iget-object v6, v1, Lcom/google/android/gms/audiomodem/t;->f:[Z

    aget-boolean v6, v6, p1

    if-eqz v6, :cond_b

    const-string v0, "audioModem"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "AudioPlayer: Playback already in progress."

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    move v2, v8

    .line 128
    :cond_9
    :goto_4
    packed-switch v2, :pswitch_data_1

    goto/16 :goto_0

    .line 131
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->b:[Z

    aput-boolean v9, v0, p1

    .line 132
    iget-object v0, v10, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    invoke-virtual {v0, v9}, Lcom/google/android/gms/audiomodem/p;->a(I)V

    goto/16 :goto_0

    .line 121
    :pswitch_1
    const/4 v0, -0x1

    goto :goto_3

    :pswitch_2
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3

    :pswitch_3
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3

    :cond_a
    const/4 v0, -0x1

    goto :goto_3

    :cond_b
    if-ltz p1, :cond_c

    iget-object v6, v1, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    array-length v6, v6

    if-lt p1, v6, :cond_d

    :cond_c
    const-string v0, "audioModem"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "AudioPlayer: Invalid track"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_d
    iget-object v6, v1, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    aget-object v6, v6, p1

    if-nez v6, :cond_f

    iget-object v6, v1, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    invoke-static {v3}, Lcom/google/android/gms/audiomodem/t;->a(I)Landroid/media/AudioTrack;

    move-result-object v3

    aput-object v3, v6, p1

    :cond_e
    :goto_5
    iget-object v3, v1, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    aget-object v3, v3, p1

    if-nez v3, :cond_10

    const-string v0, "audioModem"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "AudioPlayer: Could not create AudioTrack"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_f
    iget-object v6, v1, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Landroid/media/AudioTrack;->getSampleRate()I

    move-result v6

    if-eq v6, v3, :cond_e

    iget-object v6, v1, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Landroid/media/AudioTrack;->release()V

    iget-object v6, v1, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    invoke-static {v3}, Lcom/google/android/gms/audiomodem/t;->a(I)Landroid/media/AudioTrack;

    move-result-object v3

    aput-object v3, v6, p1

    goto :goto_5

    :cond_10
    iget-object v2, v1, Lcom/google/android/gms/audiomodem/t;->a:[Landroid/media/AudioTrack;

    aget-object v2, v2, p1

    iget-object v3, v1, Lcom/google/android/gms/audiomodem/t;->f:[Z

    aput-boolean v8, v3, p1

    cmp-long v3, v12, v12

    if-nez v3, :cond_11

    iput-wide v12, v1, Lcom/google/android/gms/audiomodem/t;->h:J

    :goto_6
    iget-object v3, v1, Lcom/google/android/gms/audiomodem/t;->i:[I

    aput v0, v3, p1

    iget-object v0, v1, Lcom/google/android/gms/audiomodem/t;->g:[Z

    aput-boolean v9, v0, p1

    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/t;->a()V

    new-instance v6, Lcom/google/android/gms/audiomodem/v;

    invoke-direct {v6, v1, p1}, Lcom/google/android/gms/audiomodem/v;-><init>(Lcom/google/android/gms/audiomodem/t;I)V

    iget-object v0, v1, Lcom/google/android/gms/audiomodem/t;->d:[Landroid/os/Handler;

    aget-object v11, v0, p1

    new-instance v0, Lcom/google/android/gms/audiomodem/w;

    iget-object v7, v1, Lcom/google/android/gms/audiomodem/t;->c:Landroid/os/Handler;

    move v3, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/audiomodem/w;-><init>(Lcom/google/android/gms/audiomodem/t;Landroid/media/AudioTrack;I[B[BLjava/lang/Runnable;Landroid/os/Handler;)V

    invoke-virtual {v11, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move v2, v9

    goto/16 :goto_4

    :cond_11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long/2addr v6, v12

    iput-wide v6, v1, Lcom/google/android/gms/audiomodem/t;->h:J

    goto :goto_6

    .line 137
    :pswitch_4
    const-string v0, "audioModem"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 138
    const-string v0, "TokenEncoderManager: Playing failed; will retry."

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_12
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->b:[Z

    aput-boolean v8, v0, p1

    goto/16 :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch

    .line 128
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method final c(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->c:Lcom/google/android/gms/audiomodem/t;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/audiomodem/t;->b(I)V

    .line 146
    iget-object v2, p0, Lcom/google/android/gms/audiomodem/bk;->a:Lcom/google/android/gms/audiomodem/af;

    move v0, v1

    :goto_0
    iget-object v3, v2, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, v2, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    aget-object v3, v3, v0

    if-nez v3, :cond_0

    iget-object v3, v2, Lcom/google/android/gms/audiomodem/af;->b:[Ljava/util/Queue;

    aget-object v3, v3, v0

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bk;->d:Lcom/google/android/gms/audiomodem/aw;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/audiomodem/aw;->c:Lcom/google/android/gms/audiomodem/ba;

    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/audiomodem/aw;->a:Landroid/content/Context;

    iget-object v3, v0, Lcom/google/android/gms/audiomodem/aw;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/gms/audiomodem/aw;->e:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :goto_2
    iput v1, p0, Lcom/google/android/gms/audiomodem/bk;->e:I

    .line 150
    :cond_1
    return-void

    .line 146
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method
