.class final Lcom/google/android/gms/cast/media/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/media/aa;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    .prologue
    .line 283
    invoke-static {}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b()Lcom/google/android/gms/cast/internal/k;

    move-result-object v0

    const-string v1, "onDeviceLost, device=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->findRemoteDisplay(Ljava/lang/String;)Lcom/android/media/remotedisplay/RemoteDisplay;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_0

    .line 287
    iget-object v1, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->removeDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    .line 290
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;I)V
    .locals 4

    .prologue
    .line 294
    invoke-static {}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b()Lcom/google/android/gms/cast/internal/k;

    move-result-object v0

    const-string v1, "onDeviceStartedMirroring, device=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)Lcom/google/android/gms/cast/e/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/e/g;->a()V

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->findRemoteDisplay(Ljava/lang/String;)Lcom/android/media/remotedisplay/RemoteDisplay;

    move-result-object v0

    .line 298
    if-eqz v0, :cond_0

    .line 299
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    .line 300
    invoke-virtual {v0, p2}, Lcom/android/media/remotedisplay/RemoteDisplay;->setPresentationDisplayId(I)V

    .line 301
    iget-object v1, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->updateDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    .line 304
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 256
    invoke-static {}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b()Lcom/google/android/gms/cast/internal/k;

    move-result-object v0

    const-string v1, "onDeviceFound, device: %s applicationName: %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object p2, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 260
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->e()Ljava/lang/String;

    move-result-object v0

    .line 266
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->findRemoteDisplay(Ljava/lang/String;)Lcom/android/media/remotedisplay/RemoteDisplay;

    move-result-object v1

    .line 267
    if-nez v1, :cond_2

    .line 268
    new-instance v1, Lcom/android/media/remotedisplay/RemoteDisplay;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v3}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)Lcom/google/android/gms/cast/media/a;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/gms/cast/media/a;->b(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/android/media/remotedisplay/RemoteDisplay;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-virtual {v1, v0}, Lcom/android/media/remotedisplay/RemoteDisplay;->setDescription(Ljava/lang/String;)V

    .line 271
    invoke-virtual {v1, v5}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->addDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    .line 279
    :cond_0
    :goto_1
    return-void

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->dV:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 274
    :cond_2
    invoke-virtual {v1}, Lcom/android/media/remotedisplay/RemoteDisplay;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    invoke-virtual {v1, v0}, Lcom/android/media/remotedisplay/RemoteDisplay;->setDescription(Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->updateDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;Z)V
    .locals 4

    .prologue
    .line 308
    invoke-static {}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b()Lcom/google/android/gms/cast/internal/k;

    move-result-object v0

    const-string v1, "onDeviceStoppedMirroring, device=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)Lcom/google/android/gms/cast/e/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/e/g;->b()V

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->findRemoteDisplay(Ljava/lang/String;)Lcom/android/media/remotedisplay/RemoteDisplay;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_0

    .line 313
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    .line 314
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/media/remotedisplay/RemoteDisplay;->setPresentationDisplayId(I)V

    .line 315
    iget-object v1, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->updateDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ac;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    .line 318
    :cond_0
    return-void
.end method
