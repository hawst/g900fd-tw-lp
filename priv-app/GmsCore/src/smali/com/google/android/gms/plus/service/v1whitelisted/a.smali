.class public final Lcom/google/android/gms/plus/service/v1whitelisted/a;
.super Lcom/google/android/gms/common/server/j;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/common/server/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/n;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/common/server/j;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a:Lcom/google/android/gms/common/server/n;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;
    .locals 6

    .prologue
    const/4 v2, 0x2

    .line 589
    const-string v0, "authCategories/%1$s/acl/%2$s"

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 590
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    move-object v1, p1

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;
    .locals 6

    .prologue
    const/4 v2, 0x2

    .line 473
    const-string v0, "applications/%1$s/acl/%2$s"

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz p4, :cond_0

    const-string v0, "language"

    invoke-static {p4}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    move-object v1, p1

    move-object v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 6

    .prologue
    .line 105
    const-string v0, "applications/%1$s/acl/%2$s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-eqz p4, :cond_0

    const-string v0, "language"

    invoke-static {p4}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a:Lcom/google/android/gms/common/server/n;

    const-class v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    move-object v1, p1

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 108
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 6

    .prologue
    .line 224
    const-string v0, "authCategories/%1$s/acl/%2$s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-eqz p4, :cond_0

    const-string v0, "language"

    invoke-static {p4}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/a;->a:Lcom/google/android/gms/common/server/n;

    const-class v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    move-object v1, p1

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 227
    return-void
.end method
