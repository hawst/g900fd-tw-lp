.class public abstract Lcom/google/android/gms/plus/audience/a/e;
.super Lcom/google/android/gms/common/ui/k;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/w;


# static fields
.field protected static final e:Lcom/google/android/gms/common/api/Status;

.field protected static final f:Lcom/google/android/gms/common/api/Status;


# instance fields
.field private final d:Lcom/google/android/gms/plus/internal/ad;

.field final g:Ljava/lang/String;

.field final h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:I

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/plus/audience/a/e;->e:Lcom/google/android/gms/common/api/Status;

    .line 35
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/plus/audience/a/e;->f:Lcom/google/android/gms/common/api/Status;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    .line 53
    sget-object v7, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/audience/a/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/plus/internal/ad;)V

    .line 55
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/plus/internal/ad;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/k;-><init>(Landroid/content/Context;)V

    .line 63
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "PeopleClientLoader"

    const-string v1, "Caller should set application ID"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    sget-object p4, Lcom/google/android/gms/common/analytics/a;->a:Ljava/lang/String;

    .line 68
    :cond_0
    iput-object p2, p0, Lcom/google/android/gms/plus/audience/a/e;->g:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lcom/google/android/gms/plus/audience/a/e;->h:Ljava/lang/String;

    .line 70
    iput-object p4, p0, Lcom/google/android/gms/plus/audience/a/e;->j:Ljava/lang/String;

    .line 71
    iput-object p5, p0, Lcom/google/android/gms/plus/audience/a/e;->k:Ljava/lang/String;

    .line 72
    iput p6, p0, Lcom/google/android/gms/plus/audience/a/e;->l:I

    .line 73
    iput-object p7, p0, Lcom/google/android/gms/plus/audience/a/e;->d:Lcom/google/android/gms/plus/internal/ad;

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/a/e;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/a/e;->c()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/a/e;Z)Z
    .locals 0

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/a/e;->u:Z

    return p1
.end method

.method private c()V
    .locals 6

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/e;->u:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/audience/a/e;->l:I

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/e;->u:Z

    .line 94
    sget-object v0, Lcom/google/android/gms/people/x;->m:Lcom/google/android/gms/people/v;

    iget-object v1, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/a/e;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/a/e;->h:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/plus/audience/a/e;->l:I

    move-object v2, p0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/v;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/audience/a/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/audience/a/f;-><init>(Lcom/google/android/gms/plus/audience/a/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 104
    :cond_0
    return-void
.end method


# virtual methods
.method public final Z_()V
    .locals 0

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a/e;->j()V

    .line 144
    return-void
.end method

.method protected final a(Landroid/content/Context;)Lcom/google/android/gms/common/api/v;
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/e;->d:Lcom/google/android/gms/plus/internal/ad;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a/e;->j:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/analytics/a;->a(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/e;->k:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 125
    new-instance v1, Lcom/google/android/gms/plus/audience/a/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/audience/a/g;-><init>(Lcom/google/android/gms/plus/audience/a/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 137
    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/plus/audience/a/e;
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/a/e;->i:Ljava/lang/String;

    .line 149
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a/e;->j()V

    .line 150
    return-object p0
.end method

.method protected final e()V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0}, Lcom/google/android/gms/common/ui/k;->e()V

    .line 87
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/a/e;->c()V

    .line 88
    return-void
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/e;->u:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/people/x;->m:Lcom/google/android/gms/people/v;

    iget-object v1, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/people/v;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;)Lcom/google/android/gms/common/api/am;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/e;->u:Z

    .line 109
    invoke-super {p0}, Lcom/google/android/gms/common/ui/k;->f()V

    .line 110
    return-void
.end method
