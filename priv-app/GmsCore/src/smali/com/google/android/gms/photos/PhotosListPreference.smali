.class public Lcom/google/android/gms/photos/PhotosListPreference;
.super Landroid/preference/DialogPreference;
.source "SourceFile"


# instance fields
.field private a:[Ljava/lang/CharSequence;

.field private b:[Ljava/lang/CharSequence;

.field private c:[Ljava/lang/CharSequence;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Landroid/view/LayoutInflater;

.field private g:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/photos/PhotosListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    sget-object v0, Lcom/google/android/gms/r;->ag:[I

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 44
    sget v1, Lcom/google/android/gms/r;->ah:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/photos/PhotosListPreference;->a:[Ljava/lang/CharSequence;

    .line 45
    sget v1, Lcom/google/android/gms/r;->ai:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    .line 46
    sget v1, Lcom/google/android/gms/r;->aj:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/photos/PhotosListPreference;->c:[Ljava/lang/CharSequence;

    .line 47
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->f:Landroid/view/LayoutInflater;

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/PhotosListPreference;I)I
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/google/android/gms/photos/PhotosListPreference;->e:I

    return p1
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/gms/photos/PhotosListPreference;->d:Ljava/lang/String;

    .line 155
    invoke-virtual {p0, p1}, Lcom/google/android/gms/photos/PhotosListPreference;->persistString(Ljava/lang/String;)Z

    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/photos/PhotosListPreference;->notifyChanged()V

    .line 157
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/PhotosListPreference;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->a:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method private b()I
    .locals 3

    .prologue
    .line 208
    iget-object v1, p0, Lcom/google/android/gms/photos/PhotosListPreference;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/gms/photos/PhotosListPreference;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/photos/PhotosListPreference;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->f:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/photos/PhotosListPreference;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/photos/PhotosListPreference;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->c:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/photos/PhotosListPreference;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->e:I

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    aget-object v0, v0, p1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/photos/PhotosListPreference;->a(Ljava/lang/String;)V

    .line 168
    :cond_0
    return-void
.end method

.method public final a()[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 213
    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 214
    if-eqz v0, :cond_0

    .line 215
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 217
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindView(Landroid/view/View;)V

    .line 218
    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 2

    .prologue
    .line 337
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    .line 339
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->e:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    iget v1, p0, Lcom/google/android/gms/photos/PhotosListPreference;->e:I

    aget-object v0, v0, v1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 341
    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/PhotosListPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    invoke-direct {p0, v0}, Lcom/google/android/gms/photos/PhotosListPreference;->a(Ljava/lang/String;)V

    .line 345
    :cond_0
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 349
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 222
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->a:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->b:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->c:[Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 225
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ListPreference requires an entries array and an entryValues array."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/photos/PhotosListPreference;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->e:I

    .line 230
    new-instance v0, Lcom/google/android/gms/photos/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/k;-><init>(Lcom/google/android/gms/photos/PhotosListPreference;)V

    iget v1, p0, Lcom/google/android/gms/photos/PhotosListPreference;->e:I

    new-instance v2, Lcom/google/android/gms/photos/l;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/l;-><init>(Lcom/google/android/gms/photos/PhotosListPreference;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 332
    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 333
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 372
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/photos/PhotosListPreference$SavedState;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 374
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 381
    :goto_0
    return-void

    .line 378
    :cond_1
    check-cast p1, Lcom/google/android/gms/photos/PhotosListPreference$SavedState;

    .line 379
    invoke-virtual {p1}, Lcom/google/android/gms/photos/PhotosListPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 380
    iget-object v0, p1, Lcom/google/android/gms/photos/PhotosListPreference$SavedState;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/photos/PhotosListPreference;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 359
    invoke-super {p0}, Landroid/preference/DialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 360
    invoke-virtual {p0}, Lcom/google/android/gms/photos/PhotosListPreference;->isPersistent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 367
    :goto_0
    return-object v0

    .line 365
    :cond_0
    new-instance v1, Lcom/google/android/gms/photos/PhotosListPreference$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/photos/PhotosListPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->d:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/photos/PhotosListPreference$SavedState;->a:Ljava/lang/String;

    move-object v0, v1

    .line 367
    goto :goto_0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1

    .prologue
    .line 354
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/PhotosListPreference;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/PhotosListPreference;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :goto_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/photos/PhotosListPreference;->a(Ljava/lang/String;)V

    .line 355
    return-void

    .line 354
    :cond_0
    check-cast p2, Ljava/lang/String;

    goto :goto_0
.end method
