.class public Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/f;

    invoke-direct {v0}, Lcom/google/android/gms/photos/autobackup/model/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;->a:I

    .line 41
    iput-boolean p2, p0, Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;->b:Z

    .line 42
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;-><init>(IZ)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;->b:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/google/android/gms/photos/autobackup/model/f;->a(Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;Landroid/os/Parcel;)V

    .line 59
    return-void
.end method
