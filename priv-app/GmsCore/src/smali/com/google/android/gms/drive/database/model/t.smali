.class public final Lcom/google/android/gms/drive/database/model/t;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 25
    invoke-static {}, Lcom/google/android/gms/drive/database/model/u;->a()Lcom/google/android/gms/drive/database/model/u;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 26
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/t;->a:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/gms/drive/database/model/v;->a:Lcom/google/android/gms/drive/database/model/v;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/v;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/t;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ContentFileDeletionLock [filename=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/t;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
