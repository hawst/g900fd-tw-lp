.class final Lcom/google/android/gms/cast/activity/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/google/android/gms/cast/activity/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/activity/b;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/gms/cast/activity/c;->b:Lcom/google/android/gms/cast/activity/b;

    iput-object p2, p0, Lcom/google/android/gms/cast/activity/c;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/c;->b:Lcom/google/android/gms/cast/activity/b;

    iget-object v0, v0, Lcom/google/android/gms/cast/activity/b;->a:Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    invoke-static {v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    invoke-static {}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "Number of tokens received: %d"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/cast/activity/c;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/DecodedToken;

    .line 84
    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/DecodedToken;->b()[B

    move-result-object v0

    .line 85
    if-eqz v0, :cond_2

    array-length v2, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 87
    invoke-static {v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 90
    invoke-static {}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v2

    const-string v3, "Decoded pincode = %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    invoke-static {v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/cast/activity/c;->b:Lcom/google/android/gms/cast/activity/b;

    iget-object v1, v1, Lcom/google/android/gms/cast/activity/b;->a:Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    invoke-static {v1}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->d(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/cast/activity/c;->b:Lcom/google/android/gms/cast/activity/b;

    iget-object v2, v2, Lcom/google/android/gms/cast/activity/b;->a:Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    invoke-static {v2}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->c(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 94
    iget-object v1, p0, Lcom/google/android/gms/cast/activity/c;->b:Lcom/google/android/gms/cast/activity/b;

    iget-object v1, v1, Lcom/google/android/gms/cast/activity/b;->a:Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    invoke-static {v1}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/cast/activity/c;->b:Lcom/google/android/gms/cast/activity/b;

    iget-object v1, v1, Lcom/google/android/gms/cast/activity/b;->a:Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;Ljava/lang/String;)V

    .line 97
    invoke-static {}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "Valid audio token received - closing dialog"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/c;->b:Lcom/google/android/gms/cast/activity/b;

    iget-object v0, v0, Lcom/google/android/gms/cast/activity/b;->a:Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    invoke-static {v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)V

    goto :goto_0

    .line 101
    :cond_3
    invoke-static {}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v2, "Invalid audio token received"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
