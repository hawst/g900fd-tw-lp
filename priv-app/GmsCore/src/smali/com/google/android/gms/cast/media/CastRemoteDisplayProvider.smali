.class public Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;
.super Lcom/android/media/remotedisplay/RemoteDisplayProvider;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/cast/internal/k;

.field private static b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;


# instance fields
.field private final c:Lcom/google/android/gms/cast/media/a;

.field private final d:Lcom/google/android/gms/cast/e/g;

.field private final e:Landroid/support/v4/g/a;

.field private f:Lcom/google/android/gms/cast/media/ad;

.field private g:Lcom/android/media/remotedisplay/RemoteDisplay;

.field private h:Z

.field private i:Landroid/app/PendingIntent;

.field private final j:Lcom/google/android/gms/cast/media/aa;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastRemoteDisplayProvider"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Lcom/google/android/gms/cast/internal/k;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/cast/media/a;)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/android/media/remotedisplay/RemoteDisplayProvider;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v0, Landroid/support/v4/g/a;

    invoke-direct {v0}, Landroid/support/v4/g/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Landroid/support/v4/g/a;

    .line 252
    new-instance v0, Lcom/google/android/gms/cast/media/ac;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/media/ac;-><init>(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->j:Lcom/google/android/gms/cast/media/aa;

    .line 61
    iput-object p2, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Lcom/google/android/gms/cast/media/a;

    .line 62
    new-instance v0, Lcom/google/android/gms/cast/e/g;

    const-string v1, "CastRemoteDisplayProvider"

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/cast/e/g;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d:Lcom/google/android/gms/cast/e/g;

    .line 63
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;
    .locals 4

    .prologue
    .line 49
    const-class v1, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    monitor-enter v1

    .line 50
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/gms/cast/media/a;->a(Landroid/content/Context;)Lcom/google/android/gms/cast/media/a;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;-><init>(Landroid/content/Context;Lcom/google/android/gms/cast/media/a;)V

    sput-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    .line 54
    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    iget-object v2, v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->j:Lcom/google/android/gms/cast/media/aa;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/aa;)V

    .line 55
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)Lcom/google/android/gms/cast/media/a;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Lcom/google/android/gms/cast/media/a;

    return-object v0
.end method

.method private static a(Lcom/android/media/remotedisplay/RemoteDisplay;)Z
    .locals 2

    .prologue
    .line 230
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/android/media/remotedisplay/RemoteDisplay;->getStatus()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/media/remotedisplay/RemoteDisplay;->getStatus()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Lcom/google/android/gms/cast/internal/k;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Lcom/google/android/gms/cast/internal/k;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)Lcom/google/android/gms/cast/e/g;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d:Lcom/google/android/gms/cast/e/g;

    return-object v0
.end method

.method static synthetic c()Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->b:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    return-object v0
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const v8, 0x1080038

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/google/android/gms/cast/media/ad;

    if-nez v0, :cond_1

    .line 163
    iput-object v3, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->g:Lcom/android/media/remotedisplay/RemoteDisplay;

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getDisplays()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/media/remotedisplay/RemoteDisplay;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lcom/android/media/remotedisplay/RemoteDisplay;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v3, v0

    .line 168
    :cond_3
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/android/media/remotedisplay/RemoteDisplay;->getStatus()I

    move-result v0

    const/4 v4, 0x3

    if-ne v0, v4, :cond_6

    move v0, v1

    .line 170
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->g:Lcom/android/media/remotedisplay/RemoteDisplay;

    if-ne v4, v3, :cond_4

    iget-boolean v4, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->h:Z

    if-eq v4, v0, :cond_0

    .line 171
    :cond_4
    iget-object v4, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->g:Lcom/android/media/remotedisplay/RemoteDisplay;

    if-eqz v4, :cond_5

    .line 172
    iget-object v4, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/google/android/gms/cast/media/ad;

    invoke-interface {v4}, Lcom/google/android/gms/cast/media/ad;->a()V

    .line 175
    :cond_5
    iput-object v3, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->g:Lcom/android/media/remotedisplay/RemoteDisplay;

    .line 176
    iput-boolean v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->h:Z

    .line 178
    if-eqz v3, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 180
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 181
    if-eqz v0, :cond_7

    .line 182
    new-instance v0, Landroid/support/v4/app/bk;

    invoke-direct {v0, v4}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/gms/p;->dL:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->dK:I

    new-array v6, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/android/media/remotedisplay/RemoteDisplay;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getSettingsPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v0, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    sget v2, Lcom/google/android/gms/h;->bW:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0, v7, v1}, Landroid/support/v4/app/bk;->a(IZ)V

    sget v1, Lcom/google/android/gms/p;->dN:I

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v8, v1, v2}, Landroid/support/v4/app/bk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/google/android/gms/cast/media/ad;

    sget v2, Lcom/google/android/gms/p;->dL:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/cast/media/ad;->a(ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 168
    goto :goto_1

    .line 199
    :cond_7
    new-instance v0, Landroid/support/v4/app/bk;

    invoke-direct {v0, v4}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/gms/p;->dJ:I

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->dI:I

    new-array v6, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/android/media/remotedisplay/RemoteDisplay;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getSettingsPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v0, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    sget v2, Lcom/google/android/gms/h;->bX:I

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0, v7, v1}, Landroid/support/v4/app/bk;->a(IZ)V

    sget v1, Lcom/google/android/gms/p;->dN:I

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v8, v1, v2}, Landroid/support/v4/app/bk;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    .line 212
    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/google/android/gms/cast/media/ad;

    sget v2, Lcom/google/android/gms/p;->dJ:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/cast/media/ad;->a(ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->g:Lcom/android/media/remotedisplay/RemoteDisplay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->g:Lcom/android/media/remotedisplay/RemoteDisplay;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->onDisconnect(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    :cond_0
    return-void
.end method

.method private e()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->i:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 238
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.cast.media.ACTION_DISCONNECT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 239
    const-class v2, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider$Receiver;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 240
    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->i:Landroid/app/PendingIntent;

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->i:Landroid/app/PendingIntent;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->j:Lcom/google/android/gms/cast/media/aa;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/a;->b(Lcom/google/android/gms/cast/media/aa;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getDisplays()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 74
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/media/remotedisplay/RemoteDisplay;

    .line 76
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lcom/android/media/remotedisplay/RemoteDisplay;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->removeDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    goto :goto_0

    .line 80
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/media/ad;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/google/android/gms/cast/media/ad;

    if-eq v0, p1, :cond_0

    .line 84
    iput-object p1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->f:Lcom/google/android/gms/cast/media/ad;

    .line 85
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d()V

    .line 87
    :cond_0
    return-void
.end method

.method public onAdjustVolume(Lcom/android/media/remotedisplay/RemoteDisplay;I)V
    .locals 5

    .prologue
    .line 153
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onAdjustVolume, display=%s, delta=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Landroid/support/v4/g/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 156
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0, p2}, Lcom/google/android/gms/cast/media/o;->b(I)V

    .line 159
    :cond_0
    return-void
.end method

.method public onConnect(Lcom/android/media/remotedisplay/RemoteDisplay;)V
    .locals 4

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onConnect, display=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    invoke-virtual {p1}, Lcom/android/media/remotedisplay/RemoteDisplay;->getId()Ljava/lang/String;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Lcom/google/android/gms/cast/media/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/a;->a(Ljava/lang/String;)Landroid/support/v7/c/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 107
    if-eqz v0, :cond_0

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Landroid/support/v4/g/a;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/o;->d()V

    .line 111
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    .line 112
    invoke-virtual {p0, p1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->updateDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V

    .line 113
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d()V

    .line 115
    :cond_0
    return-void
.end method

.method public onDisconnect(Lcom/android/media/remotedisplay/RemoteDisplay;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 119
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onDisconnect, display=%s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Landroid/support/v4/g/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 122
    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/o;->e()V

    .line 126
    :cond_0
    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/android/media/remotedisplay/RemoteDisplay;->setStatus(I)V

    .line 127
    invoke-virtual {p0, p1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->updateDisplay(Lcom/android/media/remotedisplay/RemoteDisplay;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->d()V

    .line 139
    return-void

    .line 136
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "not updating display status because it\'s already lost, display=%s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onDiscoveryModeChanged(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 91
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onDiscoveryModeChanged, mode=%d"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    if-eq p1, v4, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Lcom/google/android/gms/cast/media/a;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/cast/media/a;->a(Z)V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->c:Lcom/google/android/gms/cast/media/a;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/media/a;->a(Z)V

    goto :goto_0
.end method

.method public onSetVolume(Lcom/android/media/remotedisplay/RemoteDisplay;I)V
    .locals 5

    .prologue
    .line 143
    sget-object v0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onSetVolume, display=%s, volume=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->e:Landroid/support/v4/g/a;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 146
    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {v0, p2}, Lcom/google/android/gms/cast/media/o;->a(I)V

    .line 149
    :cond_0
    return-void
.end method
