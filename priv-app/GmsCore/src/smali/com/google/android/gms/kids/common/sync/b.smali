.class public final Lcom/google/android/gms/kids/common/sync/b;
.super Lcom/google/android/gms/common/f/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    const-string v0, "com.google.android.gms.kids.common.sync"

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/common/f/a;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/kids/common/sync/b;->a:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 41
    const-string v0, "KidsDataSyncAdapter"

    const-string v1, "onPerformSync"

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    invoke-static {}, Lcom/google/android/gms/kids/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 44
    const-string v0, "KidsDataSyncAdapter"

    const-string v1, "Skipping"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    if-eqz p2, :cond_2

    const-string v0, "is_uct"

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    if-nez v0, :cond_3

    .line 51
    const-string v0, "KidsDataSyncAdapter"

    const-string v1, "skipping unmatched request"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v0, v3

    .line 50
    goto :goto_1

    .line 55
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/kids/common/sync/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/kids/common/d;->a(Landroid/content/Context;)Lcom/google/android/gms/kids/account/b;

    move-result-object v0

    .line 56
    if-eqz p1, :cond_4

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    if-nez v1, :cond_5

    move v0, v3

    :goto_2
    if-nez v0, :cond_6

    .line 57
    :cond_4
    const-string v0, "KidsDataSyncAdapter"

    const-string v1, "Not a valid account"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 56
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/kids/account/b;->b()Landroid/accounts/Account;

    move-result-object v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2

    .line 61
    :cond_6
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/kids/common/sync/a;

    .line 62
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_9

    invoke-virtual {p2, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_9

    move v1, v2

    :goto_4
    if-eqz v1, :cond_7

    .line 64
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/kids/common/sync/b;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p4}, Lcom/google/android/gms/kids/common/sync/a;->a(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/ContentProviderClient;)V

    .line 65
    invoke-virtual {v0}, Lcom/google/android/gms/kids/common/sync/a;->a()Landroid/content/Intent;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_8

    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/kids/common/sync/b;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 69
    :cond_8
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numUpdates:J

    add-long/2addr v6, v8

    iput-wide v6, v0, Landroid/content/SyncStats;->numUpdates:J
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_3

    .line 70
    :catch_0
    move-exception v0

    .line 71
    const-string v1, "KidsDataSyncAdapter"

    const-string v5, "failed: %s"

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v0, v6, v3

    invoke-static {v1, v5, v6}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    iget-object v0, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v6, v8

    iput-wide v6, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_3

    :cond_9
    move v1, v3

    .line 62
    goto :goto_4

    .line 73
    :catch_1
    move-exception v0

    .line 74
    iget-object v1, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v6, v8

    iput-wide v6, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 75
    const-string v1, "KidsDataSyncAdapter"

    const-string v5, "failed: %s"

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v0, v6, v3

    invoke-static {v1, v5, v6}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 76
    :catch_2
    move-exception v0

    .line 77
    iput-boolean v2, p5, Landroid/content/SyncResult;->databaseError:Z

    .line 78
    const-string v1, "KidsDataSyncAdapter"

    const-string v5, "failed: %s"

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v0, v6, v3

    invoke-static {v1, v5, v6}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 79
    :catch_3
    move-exception v0

    .line 80
    const-string v1, "KidsDataSyncAdapter"

    const-string v5, "Failed with unexpected exception"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v5, v6}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method
