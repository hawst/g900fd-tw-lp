.class public final Lcom/google/android/gms/audiomodem/am;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public d:F

.field public e:I

.field public f:F

.field public g:I

.field public h:I

.field public i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x4

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/audiomodem/am;->j:I

    .line 209
    iput-boolean v2, p0, Lcom/google/android/gms/audiomodem/am;->a:Z

    .line 210
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/android/gms/audiomodem/am;->b:I

    .line 211
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/audiomodem/am;->c:I

    .line 212
    const v0, 0x473b8000    # 48000.0f

    iput v0, p0, Lcom/google/android/gms/audiomodem/am;->d:F

    .line 213
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/gms/audiomodem/am;->e:I

    .line 214
    const v0, 0x46908800    # 18500.0f

    iput v0, p0, Lcom/google/android/gms/audiomodem/am;->f:F

    .line 215
    iput v1, p0, Lcom/google/android/gms/audiomodem/am;->g:I

    .line 216
    iput v1, p0, Lcom/google/android/gms/audiomodem/am;->h:I

    .line 217
    iput v1, p0, Lcom/google/android/gms/audiomodem/am;->i:I

    .line 218
    iput v2, p0, Lcom/google/android/gms/audiomodem/am;->k:I

    .line 219
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/audiomodem/Encoding;
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v13, 0x0

    .line 300
    iget v0, p0, Lcom/google/android/gms/audiomodem/am;->e:I

    iget v2, p0, Lcom/google/android/gms/audiomodem/am;->i:I

    rem-int/2addr v0, v2

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "upsamplingFactor must be divisible by basebandDecimationFactor"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 303
    iget v0, p0, Lcom/google/android/gms/audiomodem/am;->b:I

    const/4 v2, 0x5

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/gms/audiomodem/am;->b:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_3

    :cond_0
    iget v0, p0, Lcom/google/android/gms/audiomodem/am;->c:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/audiomodem/am;->c:I

    const/4 v2, 0x2

    if-gt v0, v2, :cond_2

    move v0, v1

    :goto_1
    const-string v2, "codeNumber must be in the range [0, 3], unless numberOfTapsLfsr is 5 or 6,in which case it must be in [0, 2]"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 309
    iget v0, p0, Lcom/google/android/gms/audiomodem/am;->f:F

    iget v2, p0, Lcom/google/android/gms/audiomodem/am;->d:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_5

    :goto_2
    const-string v0, "desiredCarrierFrequency cannot exceed coderSampleRate / 2 (Nyquist rate)"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 312
    new-instance v0, Lcom/google/android/gms/audiomodem/DsssEncoding;

    iget v1, p0, Lcom/google/android/gms/audiomodem/am;->j:I

    iget v2, p0, Lcom/google/android/gms/audiomodem/am;->k:I

    invoke-static {v2}, Lcom/google/android/gms/audiomodem/Encoding;->b(I)Z

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/gms/audiomodem/am;->a:Z

    iget v4, p0, Lcom/google/android/gms/audiomodem/am;->b:I

    iget v5, p0, Lcom/google/android/gms/audiomodem/am;->c:I

    iget v6, p0, Lcom/google/android/gms/audiomodem/am;->d:F

    iget v7, p0, Lcom/google/android/gms/audiomodem/am;->e:I

    iget v8, p0, Lcom/google/android/gms/audiomodem/am;->f:F

    iget v9, p0, Lcom/google/android/gms/audiomodem/am;->g:I

    iget v10, p0, Lcom/google/android/gms/audiomodem/am;->h:I

    iget v11, p0, Lcom/google/android/gms/audiomodem/am;->i:I

    iget v12, p0, Lcom/google/android/gms/audiomodem/am;->k:I

    invoke-static {v12}, Lcom/google/android/gms/audiomodem/Encoding;->c(I)I

    move-result v12

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/audiomodem/DsssEncoding;-><init>(IZZIIFIFIIII)V

    .line 317
    new-instance v1, Lcom/google/android/gms/audiomodem/Encoding;

    invoke-direct {v1, v0, v13}, Lcom/google/android/gms/audiomodem/Encoding;-><init>(Lcom/google/android/gms/audiomodem/DsssEncoding;B)V

    return-object v1

    :cond_1
    move v0, v13

    .line 300
    goto :goto_0

    :cond_2
    move v0, v13

    .line 303
    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/google/android/gms/audiomodem/am;->c:I

    if-ltz v0, :cond_4

    iget v0, p0, Lcom/google/android/gms/audiomodem/am;->c:I

    const/4 v2, 0x3

    if-gt v0, v2, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v13

    goto :goto_1

    :cond_5
    move v1, v13

    .line 309
    goto :goto_2
.end method

.method public final a(I)Lcom/google/android/gms/audiomodem/am;
    .locals 2

    .prologue
    .line 226
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "tokenLengthBytes must be greater than zero"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 228
    iput p1, p0, Lcom/google/android/gms/audiomodem/am;->j:I

    .line 229
    return-object p0

    .line 226
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Lcom/google/android/gms/audiomodem/am;
    .locals 2

    .prologue
    .line 293
    invoke-static {p1}, Lcom/google/android/gms/audiomodem/Encoding;->a(I)Z

    move-result v0

    const-string v1, "Not a valid ErrorControlScheme"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 295
    iput p1, p0, Lcom/google/android/gms/audiomodem/am;->k:I

    .line 296
    return-object p0
.end method
