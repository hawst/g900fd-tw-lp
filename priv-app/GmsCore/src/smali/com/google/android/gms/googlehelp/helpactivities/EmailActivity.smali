.class public Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;
.super Lcom/google/android/gms/googlehelp/helpactivities/r;
.source "SourceFile"


# instance fields
.field private b:Ljava/lang/String;

.field private c:Landroid/widget/ScrollView;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/RelativeLayout;

.field private f:Landroid/view/MenuItem;

.field private g:Ljava/util/List;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 304
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 305
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 307
    sget v2, Lcom/google/android/gms/g;->ba:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v4, v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 309
    return-object v1
.end method

.method private a(Ljava/lang/String;Z)Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 201
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    .line 202
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->a(Landroid/content/Context;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 203
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Ljava/util/Map;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 316
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 317
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/googlehelp/c/af;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v1, v0}, Lcom/google/android/gms/googlehelp/c/af;->a(Ljava/util/List;)V

    goto :goto_0

    .line 321
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/googlehelp/c/ad;->a(Ljava/util/List;)V

    .line 322
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 383
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->d:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 384
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->e:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->f:Landroid/view/MenuItem;

    if-nez p1, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 386
    return-void

    :cond_2
    move v0, v2

    .line 383
    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->a(Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;)V
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/b;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/helpactivities/b;-><init>()V

    sget v1, Lcom/google/android/gms/p;->pm:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/b;->b(I)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->oG:I

    new-instance v2, Lcom/google/android/gms/googlehelp/helpactivities/j;

    invoke-direct {v2, p0}, Lcom/google/android/gms/googlehelp/helpactivities/j;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/helpactivities/b;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/b;->c(I)Lcom/google/android/gms/googlehelp/helpactivities/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "error_handler_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/helpactivities/b;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->e()V

    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    .line 362
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->a(Z)V

    .line 363
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->v()Lcom/google/ad/a/a/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ad/a/a/g;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/e/n;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/e;

    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/e;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/helpactivities/h;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;)V

    new-instance v1, Lcom/google/android/gms/googlehelp/helpactivities/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/helpactivities/i;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;)V

    invoke-static {p0, v2, v3, v0, v1}, Lcom/google/android/gms/googlehelp/a/p;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/util/List;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 380
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    .line 89
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onCreate(Landroid/os/Bundle;)V

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->g:Ljava/util/List;

    .line 93
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->aX:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v1, v1, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->d:Landroid/widget/LinearLayout;

    .line 94
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setGravity(I)V

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->e:Landroid/widget/RelativeLayout;

    .line 96
    new-instance v0, Landroid/widget/ScrollView;

    invoke-direct {v0, p0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->c:Landroid/widget/ScrollView;

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->c:Landroid/widget/ScrollView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->c:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 101
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v1, Lcom/google/android/gms/f;->M:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->c:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 103
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 104
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->setContentView(Landroid/view/View;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->v()Lcom/google/ad/a/a/g;

    move-result-object v1

    .line 107
    iget-object v0, v1, Lcom/google/ad/a/a/g;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v1, Lcom/google/ad/a/a/g;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->b:Ljava/lang/String;

    .line 111
    iget-object v0, v1, Lcom/google/ad/a/a/g;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->d:Landroid/widget/LinearLayout;

    iget-object v2, v1, Lcom/google/ad/a/a/g;->h:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v4, 0x11

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/k;->v:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 114
    :cond_0
    iget-object v0, v1, Lcom/google/ad/a/a/g;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->d:Landroid/widget/LinearLayout;

    iget-object v2, v1, Lcom/google/ad/a/a/g;->i:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->a(Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 118
    :cond_1
    new-instance v4, Lcom/google/android/gms/googlehelp/c/aa;

    invoke-direct {v4}, Lcom/google/android/gms/googlehelp/c/aa;-><init>()V

    .line 120
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 122
    iget-object v6, v1, Lcom/google/ad/a/a/g;->c:[Lcom/google/ad/a/a/h;

    array-length v7, v6

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v7, :cond_e

    aget-object v8, v6, v3

    .line 123
    new-instance v2, Lcom/google/android/gms/googlehelp/c/d;

    invoke-direct {v2, p0}, Lcom/google/android/gms/googlehelp/c/d;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->a(Landroid/content/Context;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/c/d;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/c/d;->setOrientation(I)V

    iget-object v0, v8, Lcom/google/ad/a/a/h;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/util/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, v8, Lcom/google/ad/a/a/h;->c:Z

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/googlehelp/common/v;->b(Landroid/content/Context;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/c/d;->addView(Landroid/view/View;)V

    iget-object v0, v8, Lcom/google/ad/a/a/h;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, v8, Lcom/google/ad/a/a/h;->w:Z

    if-nez v0, :cond_4

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-nez v1, :cond_8

    const/4 v0, 0x0

    .line 124
    :goto_3
    if-eqz v0, :cond_d

    .line 125
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 130
    iget-object v1, v8, Lcom/google/ad/a/a/h;->r:[Ljava/lang/String;

    if-eqz v1, :cond_d

    iget-object v1, v8, Lcom/google/ad/a/a/h;->r:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_d

    .line 131
    iget-object v1, v8, Lcom/google/ad/a/a/h;->r:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->d:Landroid/widget/LinearLayout;

    invoke-static {v1, v2}, Lcom/google/android/gms/googlehelp/c/b;->a([Ljava/lang/String;Landroid/widget/LinearLayout;)Lcom/google/android/gms/googlehelp/c/b;

    move-result-object v1

    .line 133
    new-instance v2, Lcom/google/android/gms/googlehelp/c/ad;

    iget-object v8, v1, Lcom/google/android/gms/googlehelp/c/b;->b:Ljava/util/List;

    invoke-direct {v2, v0, v8}, Lcom/google/android/gms/googlehelp/c/ad;-><init>(Lcom/google/android/gms/googlehelp/c/d;Ljava/util/List;)V

    .line 135
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/b;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/af;

    .line 137
    invoke-virtual {v4, v0, v2}, Lcom/google/android/gms/googlehelp/c/aa;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_4

    .line 107
    :cond_3
    sget v0, Lcom/google/android/gms/p;->oD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 123
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->v()Lcom/google/ad/a/a/g;

    move-result-object v0

    iget v1, v8, Lcom/google/ad/a/a/h;->d:I

    packed-switch v1, :pswitch_data_0

    :cond_5
    :pswitch_0
    const/4 v1, 0x0

    goto :goto_2

    :pswitch_1
    iget-object v1, v8, Lcom/google/ad/a/a/h;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/ad/a/a/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v8, Lcom/google/ad/a/a/h;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->h:Ljava/lang/String;

    :cond_6
    iget-object v0, v8, Lcom/google/ad/a/a/h;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v8, Lcom/google/ad/a/a/h;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v1

    goto :goto_2

    :pswitch_2
    new-instance v1, Lcom/google/android/gms/googlehelp/c/n;

    invoke-direct {v1, p0, v2, v8}, Lcom/google/android/gms/googlehelp/c/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V

    goto :goto_2

    :pswitch_3
    new-instance v1, Lcom/google/android/gms/googlehelp/c/x;

    invoke-direct {v1, p0, v2, v8}, Lcom/google/android/gms/googlehelp/c/x;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V

    goto :goto_2

    :pswitch_4
    new-instance v1, Lcom/google/android/gms/googlehelp/c/i;

    invoke-direct {v1, p0, v2, v8}, Lcom/google/android/gms/googlehelp/c/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V

    goto :goto_2

    :pswitch_5
    new-instance v1, Lcom/google/android/gms/googlehelp/c/q;

    invoke-direct {v1, p0, v2, v8}, Lcom/google/android/gms/googlehelp/c/q;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V

    goto/16 :goto_2

    :pswitch_6
    new-instance v1, Lcom/google/android/gms/googlehelp/c/w;

    invoke-direct {v1, p0, v2, v8}, Lcom/google/android/gms/googlehelp/c/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;)V

    goto/16 :goto_2

    :pswitch_7
    new-instance v1, Lcom/google/android/gms/googlehelp/c/j;

    invoke-direct {v1, p0, v8}, Lcom/google/android/gms/googlehelp/c/j;-><init>(Landroid/support/v4/app/q;Lcom/google/ad/a/a/h;)V

    goto/16 :goto_2

    :pswitch_8
    iget-object v1, v8, Lcom/google/ad/a/a/h;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/ad/a/a/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v8, Lcom/google/ad/a/a/h;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->h:Ljava/lang/String;

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_8
    instance-of v0, v1, Lcom/google/android/gms/googlehelp/c/e;

    if-eqz v0, :cond_9

    iget-object v9, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->g:Ljava/util/List;

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/googlehelp/c/e;

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    instance-of v0, v1, Lcom/google/android/gms/googlehelp/c/j;

    if-eqz v0, :cond_c

    const/4 v0, -0x2

    :goto_5
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x2

    invoke-direct {v9, v0, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v10, Lcom/google/android/gms/g;->aW:I

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v0, v10, v0, v11}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/googlehelp/c/d;->addView(Landroid/view/View;)V

    iget-object v0, v8, Lcom/google/ad/a/a/h;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, v8, Lcom/google/ad/a/a/h;->i:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/c/d;->addView(Landroid/view/View;)V

    :cond_a
    iget-object v0, v8, Lcom/google/ad/a/a/h;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, v8, Lcom/google/ad/a/a/h;->j:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/c/d;->addView(Landroid/view/View;)V

    :cond_b
    move-object v0, v2

    goto/16 :goto_3

    :cond_c
    const/4 v0, -0x1

    goto :goto_5

    .line 122
    :cond_d
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    .line 141
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->d:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/gms/p;->oF:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->a(Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 143
    invoke-static {v4, v5}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->a(Ljava/util/Map;Ljava/util/List;)V

    .line 144
    return-void

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_8
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    .line 327
    sget v0, Lcom/google/android/gms/m;->v:I

    sget v1, Lcom/google/android/gms/m;->u:I

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/gms/googlehelp/helpactivities/a;->a(IILandroid/view/Menu;Landroid/view/MenuInflater;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 334
    sget v0, Lcom/google/android/gms/j;->hJ:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->f:Landroid/view/MenuItem;

    .line 335
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/e;

    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/e;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 336
    :cond_0
    new-instance v0, Lcom/google/android/gms/googlehelp/c/ac;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->f:Landroid/view/MenuItem;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/googlehelp/c/ac;-><init>(Ljava/util/List;Landroid/view/MenuItem;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/ac;->a()V

    .line 338
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 352
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 353
    sget v1, Lcom/google/android/gms/j;->hJ:I

    if-ne v0, v1, :cond_0

    .line 354
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;->e()V

    .line 355
    const/4 v0, 0x1

    .line 358
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 148
    const-string v0, "EXTRA_HELP_CONFIG"

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 149
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 150
    return-void
.end method
