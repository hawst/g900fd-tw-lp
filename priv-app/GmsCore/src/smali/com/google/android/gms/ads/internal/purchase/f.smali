.class public final Lcom/google/android/gms/ads/internal/purchase/f;
.super Lcom/google/android/gms/ads/internal/purchase/a/h;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/gms/ads/internal/purchase/i;

.field private c:Landroid/content/Context;

.field private d:Lcom/google/android/gms/ads/internal/purchase/a/a;

.field private e:Lcom/google/android/gms/ads/internal/purchase/b;

.field private f:Lcom/google/android/gms/ads/internal/purchase/g;

.field private g:Lcom/google/android/gms/ads/internal/purchase/l;

.field private h:Lcom/google/android/gms/ads/internal/purchase/m;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/purchase/a/h;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->i:Ljava/lang/String;

    .line 68
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/purchase/i;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/internal/purchase/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->b:Lcom/google/android/gms/ads/internal/purchase/i;

    .line 70
    return-void
.end method

.method public static a(Landroid/content/Context;ZLcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;)V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 61
    const-string v1, "com.google.android.gms.ads.purchase.InAppPurchaseActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    const-string v1, "com.google.android.gms.ads.internal.purchase.useClientJar"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 63
    invoke-static {v0, p2}, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->a(Landroid/content/Intent;Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;)V

    .line 64
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 65
    return-void
.end method

.method private a(Ljava/lang/String;ZILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->g:Lcom/google/android/gms/ads/internal/purchase/l;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->g:Lcom/google/android/gms/ads/internal/purchase/l;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/purchase/f;->f:Lcom/google/android/gms/ads/internal/purchase/g;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/purchase/l;->a(Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/ads/internal/purchase/g;)V

    .line 204
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->a(Landroid/content/Intent;)Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;

    move-result-object v0

    .line 93
    iget-object v1, v0, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->e:Lcom/google/android/gms/ads/internal/purchase/l;

    iput-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->g:Lcom/google/android/gms/ads/internal/purchase/l;

    .line 95
    iget-object v1, v0, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->b:Lcom/google/android/gms/ads/internal/purchase/m;

    iput-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->h:Lcom/google/android/gms/ads/internal/purchase/m;

    .line 96
    iget-object v1, v0, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->c:Lcom/google/android/gms/ads/internal/purchase/a/a;

    iput-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->d:Lcom/google/android/gms/ads/internal/purchase/a/a;

    .line 97
    new-instance v1, Lcom/google/android/gms/ads/internal/purchase/b;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/ads/internal/purchase/b;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    .line 98
    iget-object v0, v0, Lcom/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel;->d:Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->c:Landroid/content/Context;

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 100
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 107
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 108
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 110
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 124
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_1

    .line 125
    const/4 v0, 0x0

    .line 127
    :try_start_0
    invoke-static {p3}, Lcom/google/android/gms/ads/internal/purchase/k;->a(Landroid/content/Intent;)I

    move-result v1

    .line 128
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    if-nez v1, :cond_2

    .line 130
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/f;->h:Lcom/google/android/gms/ads/internal/purchase/m;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/purchase/f;->i:Ljava/lang/String;

    invoke-virtual {v2, v3, p3}, Lcom/google/android/gms/ads/internal/purchase/m;->a(Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    const/4 v0, 0x1

    .line 138
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/f;->d:Lcom/google/android/gms/ads/internal/purchase/a/a;

    invoke-interface {v2, v1}, Lcom/google/android/gms/ads/internal/purchase/a/a;->b(I)V

    .line 139
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 140
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->d:Lcom/google/android/gms/ads/internal/purchase/a/a;

    invoke-interface {v1}, Lcom/google/android/gms/ads/internal/purchase/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0, p2, p3}, Lcom/google/android/gms/ads/internal/purchase/f;->a(Ljava/lang/String;ZILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    iput-object v4, p0, Lcom/google/android/gms/ads/internal/purchase/f;->i:Ljava/lang/String;

    .line 150
    :cond_1
    :goto_1
    return-void

    .line 136
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/f;->b:Lcom/google/android/gms/ads/internal/purchase/i;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/purchase/f;->f:Lcom/google/android/gms/ads/internal/purchase/g;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/internal/purchase/i;->a(Lcom/google/android/gms/ads/internal/purchase/g;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "Fail to process purchase result."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 147
    iput-object v4, p0, Lcom/google/android/gms/ads/internal/purchase/f;->i:Ljava/lang/String;

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v4, p0, Lcom/google/android/gms/ads/internal/purchase/f;->i:Ljava/lang/String;

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/purchase/b;->a:Ljava/lang/Object;

    .line 117
    return-void
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 8

    .prologue
    .line 160
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    invoke-virtual {v1, p2}, Lcom/google/android/gms/ads/internal/purchase/b;->a(Landroid/os/IBinder;)V

    .line 162
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->h:Lcom/google/android/gms/ads/internal/purchase/m;

    invoke-static {}, Lcom/google/android/gms/ads/internal/util/g;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->i:Ljava/lang/String;

    .line 163
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/purchase/f;->d:Lcom/google/android/gms/ads/internal/purchase/a/a;

    invoke-interface {v3}, Lcom/google/android/gms/ads/internal/purchase/a/a;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/purchase/f;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/ads/internal/purchase/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 167
    const-string v1, "BUY_INTENT"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/app/PendingIntent;

    move-object v2, v0

    .line 168
    if-nez v2, :cond_0

    .line 171
    invoke-static {v3}, Lcom/google/android/gms/ads/internal/purchase/k;->a(Landroid/os/Bundle;)I

    move-result v1

    .line 172
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/f;->d:Lcom/google/android/gms/ads/internal/purchase/a/a;

    invoke-interface {v2, v1}, Lcom/google/android/gms/ads/internal/purchase/a/a;->b(I)V

    .line 173
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/f;->d:Lcom/google/android/gms/ads/internal/purchase/a/a;

    invoke-interface {v2}, Lcom/google/android/gms/ads/internal/purchase/a/a;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/google/android/gms/ads/internal/purchase/f;->a(Ljava/lang/String;ZILandroid/content/Intent;)V

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 193
    :goto_0
    return-void

    .line 178
    :cond_0
    new-instance v1, Lcom/google/android/gms/ads/internal/purchase/g;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/purchase/f;->d:Lcom/google/android/gms/ads/internal/purchase/a/a;

    invoke-interface {v3}, Lcom/google/android/gms/ads/internal/purchase/a/a;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/purchase/f;->i:Ljava/lang/String;

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/ads/internal/purchase/g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->f:Lcom/google/android/gms/ads/internal/purchase/g;

    .line 180
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->b:Lcom/google/android/gms/ads/internal/purchase/i;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/purchase/f;->f:Lcom/google/android/gms/ads/internal/purchase/g;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/ads/internal/purchase/i;->b(Lcom/google/android/gms/ads/internal/purchase/g;)V

    .line 181
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v2

    const/16 v3, 0x3e9

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual/range {v1 .. v7}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 189
    :catch_0
    move-exception v1

    .line 190
    :goto_1
    const-string v2, "Error when connecting in-app billing service"

    invoke-static {v2, v1}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 191
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/f;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 189
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 154
    const-string v0, "In-app billing service disconnected."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/f;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/purchase/b;->a:Ljava/lang/Object;

    .line 156
    return-void
.end method
