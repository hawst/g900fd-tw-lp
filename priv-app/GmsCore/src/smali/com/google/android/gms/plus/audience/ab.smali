.class public final Lcom/google/android/gms/plus/audience/ab;
.super Lcom/google/android/gms/common/ui/widget/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/audience/o;

.field private final c:Ljava/util/List;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 1103
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    .line 1104
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/c;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    .line 1105
    iput-object p2, p0, Lcom/google/android/gms/plus/audience/ab;->c:Ljava/util/List;

    .line 1106
    iput p3, p0, Lcom/google/android/gms/plus/audience/ab;->d:I

    .line 1107
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1111
    iget v0, p0, Lcom/google/android/gms/plus/audience/ab;->d:I

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/o;->e(Lcom/google/android/gms/plus/audience/o;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/o;->e(Lcom/google/android/gms/plus/audience/o;)I

    move-result v1

    div-int/2addr v0, v1

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 15

    .prologue
    .line 1118
    move-object/from16 v0, p2

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 1119
    check-cast p2, Landroid/view/ViewGroup;

    .line 1125
    :goto_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v2}, Lcom/google/android/gms/plus/audience/o;->e(Lcom/google/android/gms/plus/audience/o;)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1126
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/o;->g(Lcom/google/android/gms/plus/audience/o;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->dA:I

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 1121
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/o;->f(Lcom/google/android/gms/plus/audience/o;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->dB:I

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move-object/from16 p2, v1

    goto :goto_0

    .line 1129
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v2}, Lcom/google/android/gms/plus/audience/o;->e(Lcom/google/android/gms/plus/audience/o;)I

    move-result v2

    if-le v1, v2, :cond_2

    .line 1130
    const/4 v1, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v3}, Lcom/google/android/gms/plus/audience/o;->e(Lcom/google/android/gms/plus/audience/o;)I

    move-result v3

    sub-int/2addr v2, v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->removeViews(II)V

    .line 1133
    :cond_2
    const/4 v1, 0x0

    move v14, v1

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/o;->e(Lcom/google/android/gms/plus/audience/o;)I

    move-result v1

    if-ge v14, v1, :cond_4

    .line 1134
    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 1135
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/o;->e(Lcom/google/android/gms/plus/audience/o;)I

    move-result v1

    mul-int v1, v1, p1

    add-int/2addr v1, v14

    .line 1136
    iget v2, p0, Lcom/google/android/gms/plus/audience/ab;->d:I

    if-ge v1, v2, :cond_3

    .line 1137
    iget-object v2, p0, Lcom/google/android/gms/plus/audience/ab;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 1138
    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "selectionSource"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1141
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/o;->d(Lcom/google/android/gms/plus/audience/o;)Lcom/google/android/gms/plus/audience/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    .line 1143
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "secondaryText"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v7

    const-string v9, "contactsAvatarUri"

    invoke-virtual {v7, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    sget v9, Lcom/google/android/gms/l;->dA:I

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-virtual/range {v1 .. v13}, Lcom/google/android/gms/plus/audience/o;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    move-result-object v1

    .line 1152
    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->f()V

    .line 1153
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1133
    :goto_2
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    goto :goto_1

    .line 1155
    :cond_3
    const/4 v1, 0x4

    invoke-virtual {v10, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 1159
    :cond_4
    return-object p2
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1169
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 1164
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ab;->a:Lcom/google/android/gms/plus/audience/o;

    iget v0, v0, Lcom/google/android/gms/plus/audience/o;->k:I

    return v0
.end method
