.class public Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/ProgressBar;

.field private d:Landroid/view/View;

.field private e:J

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lcom/google/android/gms/googlehelp/f/a;

.field private final j:Ljava/lang/Runnable;

.field private final k:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a:Landroid/os/Handler;

    .line 51
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->e:J

    .line 73
    new-instance v0, Lcom/google/android/gms/googlehelp/fragments/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/fragments/c;-><init>(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->j:Ljava/lang/Runnable;

    .line 82
    new-instance v0, Lcom/google/android/gms/googlehelp/fragments/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/fragments/d;-><init>(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->k:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;J)J
    .locals 1

    .prologue
    .line 40
    iput-wide p1, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->e:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->f:Z

    return v0
.end method

.method private b()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/ay;->f(Landroid/view/View;)F

    move-result v0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 107
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b:Landroid/view/View;

    const-string v1, "alpha"

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->d:Landroid/view/View;

    const-string v2, "alpha"

    new-array v3, v4, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 111
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 112
    new-array v3, v4, [Landroid/animation/Animator;

    aput-object v0, v3, v5

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 113
    const-wide/16 v0, 0x1f4

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 114
    new-instance v0, Lcom/google/android/gms/googlehelp/fragments/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/fragments/e;-><init>(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 121
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 123
    :cond_3
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 124
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 125
    new-instance v2, Lcom/google/android/gms/googlehelp/fragments/f;

    invoke-direct {v2, p0}, Lcom/google/android/gms/googlehelp/fragments/f;-><init>(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 140
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 108
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 109
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->g:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->h:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->c:Landroid/widget/ProgressBar;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/f/a;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 197
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->e:J

    .line 198
    iput-boolean v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->f:Z

    .line 199
    iput-boolean v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->h:Z

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 201
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->i:Lcom/google/android/gms/googlehelp/f/a;

    .line 202
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->g:Z

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->k:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->g:Z

    .line 206
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 173
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->i:Lcom/google/android/gms/googlehelp/f/a;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->i:Lcom/google/android/gms/googlehelp/f/a;

    instance-of v2, v2, Lcom/google/android/gms/googlehelp/f/j;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->i:Lcom/google/android/gms/googlehelp/f/a;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/f/a;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 188
    :goto_0
    return v0

    .line 179
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->g:Z

    .line 180
    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->h:Z

    .line 181
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->k:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 182
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->i:Lcom/google/android/gms/googlehelp/f/a;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/f/a;->a(Z)Z

    .line 183
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->i:Lcom/google/android/gms/googlehelp/f/a;

    .line 185
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->d:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 186
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public dismiss()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v6, 0x1

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->g:Z

    .line 152
    iput-boolean v6, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->h:Z

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->i:Lcom/google/android/gms/googlehelp/f/a;

    .line 155
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->e:J

    sub-long/2addr v0, v2

    .line 156
    cmp-long v2, v0, v8

    if-gez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->e:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 160
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b()V

    .line 170
    :cond_1
    :goto_0
    return-void

    .line 165
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->f:Z

    if-nez v2, :cond_1

    .line 166
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->j:Ljava/lang/Runnable;

    sub-long v0, v8, v0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 167
    iput-boolean v6, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->f:Z

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 210
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->il:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->d:Landroid/view/View;

    .line 212
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 66
    sget v0, Lcom/google/android/gms/l;->ce:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 67
    sget v0, Lcom/google/android/gms/j;->qJ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->b:Landroid/view/View;

    .line 68
    sget v0, Lcom/google/android/gms/j;->iK:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->c:Landroid/widget/ProgressBar;

    .line 70
    return-object v1
.end method
