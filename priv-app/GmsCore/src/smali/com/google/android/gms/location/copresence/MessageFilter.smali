.class public Lcom/google/android/gms/location/copresence/MessageFilter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lcom/google/android/gms/location/copresence/p;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/p;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/MessageFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/util/List;)V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput p1, p0, Lcom/google/android/gms/location/copresence/MessageFilter;->a:I

    .line 143
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter;->b:Ljava/util/List;

    .line 145
    return-void

    .line 143
    :cond_0
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 148
    const/4 v1, 0x1

    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/location/copresence/MessageFilter;-><init>(ILjava/util/List;)V

    .line 149
    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;B)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/gms/location/copresence/MessageFilter;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter;->a:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/location/copresence/Message;)Z
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    .line 204
    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->a(Lcom/google/android/gms/location/copresence/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x1

    .line 208
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter;->b:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 4

    .prologue
    .line 188
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    .line 190
    iget-object v3, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 191
    const/4 v0, 0x0

    .line 195
    :goto_1
    return-object v0

    .line 193
    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->f:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 195
    goto :goto_1
.end method

.method public final d()J
    .locals 10

    .prologue
    .line 217
    const-wide/16 v0, 0x0

    .line 218
    iget-object v2, p0, Lcom/google/android/gms/location/copresence/MessageFilter;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    .line 219
    iget-wide v6, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->d:J

    const-wide/16 v8, -0x1

    cmp-long v1, v6, v8

    if-nez v1, :cond_1

    .line 220
    const-wide v2, 0x7fffffffffffffffL

    .line 225
    :cond_0
    return-wide v2

    .line 221
    :cond_1
    iget-wide v6, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->d:J

    cmp-long v1, v6, v2

    if-lez v1, :cond_2

    .line 222
    iget-wide v0, v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->d:J

    :goto_1
    move-wide v2, v0

    .line 224
    goto :goto_0

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 337
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 338
    const-string v0, "MessageFilter(count="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/MessageFilter;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/MessageFilter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;

    .line 341
    const-string v3, "[0]: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/MessageFilter$FilterPrimitive;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 344
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 332
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/p;->a(Lcom/google/android/gms/location/copresence/MessageFilter;Landroid/os/Parcel;)V

    .line 333
    return-void
.end method
