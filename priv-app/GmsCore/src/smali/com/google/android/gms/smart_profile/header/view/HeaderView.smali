.class public Lcom/google/android/gms/smart_profile/header/view/HeaderView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/view/View;

.field public final c:Landroid/widget/TextView;

.field public final d:Ljava/util/ArrayList;

.field private final e:Landroid/widget/ImageView;

.field private final f:Lcom/google/android/gms/smart_profile/header/view/a;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/widget/LinearLayout;

.field private final i:Landroid/widget/LinearLayout;

.field private final j:Landroid/widget/LinearLayout;

.field private k:Landroid/widget/FrameLayout;

.field private l:Landroid/widget/ImageView;

.field private final m:Landroid/widget/FrameLayout;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/view/View;

.field private final p:Landroid/view/ViewGroup;

.field private final q:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

.field private r:Lcom/google/android/gms/smart_profile/header/view/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 87
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d:Ljava/util/ArrayList;

    .line 89
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 91
    sget v1, Lcom/google/android/gms/l;->fk:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 93
    sget v0, Lcom/google/android/gms/j;->jB:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->m:Landroid/widget/FrameLayout;

    .line 94
    sget v0, Lcom/google/android/gms/j;->jA:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->i:Landroid/widget/LinearLayout;

    .line 95
    sget v0, Lcom/google/android/gms/j;->jD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->j:Landroid/widget/LinearLayout;

    .line 96
    sget v0, Lcom/google/android/gms/j;->bP:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->k:Landroid/widget/FrameLayout;

    .line 97
    sget v0, Lcom/google/android/gms/j;->by:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->l:Landroid/widget/ImageView;

    .line 98
    sget v0, Lcom/google/android/gms/j;->jJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b:Landroid/view/View;

    .line 100
    new-instance v0, Lcom/google/android/gms/smart_profile/header/view/a;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/smart_profile/header/view/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->f:Lcom/google/android/gms/smart_profile/header/view/a;

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->f:Lcom/google/android/gms/smart_profile/header/view/a;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/header/view/a;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 102
    sget v0, Lcom/google/android/gms/j;->bP:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 103
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 105
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->f:Lcom/google/android/gms/smart_profile/header/view/a;

    invoke-virtual {v0, v2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    sget v0, Lcom/google/android/gms/j;->by:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->e:Landroid/widget/ImageView;

    .line 108
    sget v0, Lcom/google/android/gms/j;->mj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a:Landroid/view/View;

    .line 109
    sget v0, Lcom/google/android/gms/j;->ex:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->c:Landroid/widget/TextView;

    .line 110
    sget v0, Lcom/google/android/gms/j;->jV:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->g:Landroid/widget/ImageView;

    .line 112
    sget v0, Lcom/google/android/gms/j;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->h:Landroid/widget/LinearLayout;

    .line 113
    sget v0, Lcom/google/android/gms/j;->T:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->n:Landroid/widget/TextView;

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->aw:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->at:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->n:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bf;->a(ILandroid/widget/TextView;)V

    .line 117
    sget v0, Lcom/google/android/gms/j;->W:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->o:Landroid/view/View;

    .line 118
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d:Ljava/util/ArrayList;

    sget v0, Lcom/google/android/gms/j;->b:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d:Ljava/util/ArrayList;

    sget v0, Lcom/google/android/gms/j;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2, v0}, Lcom/google/android/gms/smart_profile/bf;->a(ILandroid/widget/TextView;)V

    .line 120
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124
    :cond_0
    sget v0, Lcom/google/android/gms/j;->dj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->p:Landroid/view/ViewGroup;

    .line 125
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->q:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    .line 128
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v4, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 130
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->p:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->q:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    return-void

    .line 125
    :cond_1
    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->q:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->n:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 237
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 265
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    :goto_0
    return-void

    .line 269
    :cond_0
    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->e:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 166
    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/header/view/b;)V
    .locals 2

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->r:Lcom/google/android/gms/smart_profile/header/view/b;

    .line 155
    if-eqz p1, :cond_0

    move-object v0, p0

    .line 156
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    return-void

    .line 155
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 194
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 174
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->e:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    return-void

    .line 174
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 282
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-static {v1, v0}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/graphics/drawable/GradientDrawable$Orientation;Landroid/content/res/Resources;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    .line 202
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 207
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->f:Lcom/google/android/gms/smart_profile/header/view/a;

    invoke-static {v0, p1}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 184
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->n:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 213
    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/gms/h;->cb:I

    .line 214
    :goto_0
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 219
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 213
    goto :goto_0

    .line 217
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1, v1, v0, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 293
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 294
    invoke-virtual {v0, v2, v2, p1, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 298
    :goto_0
    return-void

    .line 296
    :cond_0
    invoke-virtual {v0, p1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 225
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->n:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->o:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 228
    return-void

    :cond_0
    move v0, v1

    .line 225
    goto :goto_0

    .line 227
    :cond_1
    const/4 v1, 0x4

    goto :goto_1
.end method

.method public final d()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->i:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->f:Lcom/google/android/gms/smart_profile/header/view/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/smart_profile/header/view/a;->setBackgroundColor(I)V

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->m:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 327
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 306
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->h:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 307
    return-void

    .line 306
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final e()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->j:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 315
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->p:Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 316
    return-void

    .line 315
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final f()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->k:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/smart_profile/header/view/a;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->f:Lcom/google/android/gms/smart_profile/header/view/a;

    return-object v0
.end method

.method public final h()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->h:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final i()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->m:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public final j()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 135
    const/4 v0, -0x1

    .line 136
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->n:Landroid/widget/TextView;

    if-ne p1, v1, :cond_0

    .line 137
    const/4 v0, 0x1

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->r:Lcom/google/android/gms/smart_profile/header/view/b;

    if-eqz v1, :cond_1

    if-ltz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->r:Lcom/google/android/gms/smart_profile/header/view/b;

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/header/view/b;->b()V

    .line 142
    :cond_1
    return-void
.end method
