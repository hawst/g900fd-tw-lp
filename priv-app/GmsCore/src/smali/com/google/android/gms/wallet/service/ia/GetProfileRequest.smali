.class public Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private b:Lcom/google/checkout/inapp/proto/ah;

.field private c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/k;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/service/ia/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/ah;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->a:Landroid/accounts/Account;

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->b:Lcom/google/checkout/inapp/proto/ah;

    .line 24
    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;[B)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->a:Landroid/accounts/Account;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->c:[B

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Landroid/accounts/Account;[BB)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;-><init>(Landroid/accounts/Account;[B)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public final b()Lcom/google/checkout/inapp/proto/ah;
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->b:Lcom/google/checkout/inapp/proto/ah;

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->c:[B

    const-class v1, Lcom/google/checkout/inapp/proto/ah;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/ah;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->b:Lcom/google/checkout/inapp/proto/ah;

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->b:Lcom/google/checkout/inapp/proto/ah;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->a:Landroid/accounts/Account;

    invoke-virtual {v0, p1, p2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->c:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->b:Lcom/google/checkout/inapp/proto/ah;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->c:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->c:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 72
    return-void
.end method
