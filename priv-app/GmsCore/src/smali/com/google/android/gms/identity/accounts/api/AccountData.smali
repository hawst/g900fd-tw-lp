.class public final Lcom/google/android/gms/identity/accounts/api/AccountData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/identity/accounts/api/a;

    invoke-direct {v0}, Lcom/google/android/gms/identity/accounts/api/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/identity/accounts/api/AccountData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-string v0, "Account name must not be empty."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 46
    iput p1, p0, Lcom/google/android/gms/identity/accounts/api/AccountData;->a:I

    .line 47
    iput-object p2, p0, Lcom/google/android/gms/identity/accounts/api/AccountData;->b:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/google/android/gms/identity/accounts/api/AccountData;->c:Ljava/lang/String;

    .line 49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/identity/accounts/api/AccountData;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/identity/accounts/api/AccountData;
    .locals 2

    .prologue
    .line 78
    const-string v0, "Account name must not be empty."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 80
    new-instance v0, Lcom/google/android/gms/identity/accounts/api/AccountData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/identity/accounts/api/AccountData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/identity/accounts/api/AccountData;
    .locals 1

    .prologue
    .line 64
    const-string v0, "Account name must not be empty."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 65
    const-string v0, "+Page ID must not be empty."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 67
    new-instance v0, Lcom/google/android/gms/identity/accounts/api/AccountData;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/identity/accounts/api/AccountData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/google/android/gms/identity/accounts/api/AccountData;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/api/AccountData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/api/AccountData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 113
    invoke-static {p0, p1}, Lcom/google/android/gms/identity/accounts/api/a;->a(Lcom/google/android/gms/identity/accounts/api/AccountData;Landroid/os/Parcel;)V

    .line 114
    return-void
.end method
