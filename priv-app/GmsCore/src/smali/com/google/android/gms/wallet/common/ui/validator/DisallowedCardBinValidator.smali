.class public Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;
.super Lcom/google/android/gms/wallet/common/ui/validator/r;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/f;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;-><init>(Ljava/util/HashMap;)V

    .line 24
    return-void
.end method

.method private constructor <init>(Ljava/util/HashMap;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/validator/r;-><init>(Ljava/lang/CharSequence;)V

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;->a:Ljava/util/HashMap;

    .line 29
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/HashMap;B)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;-><init>(Ljava/util/HashMap;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    return-void
.end method

.method public final a(Landroid/widget/TextView;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 33
    instance-of v0, p1, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-nez v0, :cond_0

    move v0, v1

    .line 45
    :goto_0
    return v0

    .line 36
    :cond_0
    check-cast p1, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    .line 37
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_1

    .line 39
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 40
    if-eqz v0, :cond_1

    .line 41
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;->b:Ljava/lang/CharSequence;

    .line 42
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 45
    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/DisallowedCardBinValidator;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 61
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method
