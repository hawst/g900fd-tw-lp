.class public final Lcom/google/android/gms/ads/internal/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/a/o;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Ljava/util/WeakHashMap;

.field private final c:Ljava/util/ArrayList;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field private final f:Lcom/google/android/gms/ads/internal/i/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/ads/internal/i/a;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/c;->a:Ljava/lang/Object;

    .line 24
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/c;->b:Ljava/util/WeakHashMap;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/c;->c:Ljava/util/ArrayList;

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/c;->d:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/a/c;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/a/c;->f:Lcom/google/android/gms/ads/internal/i/a;

    .line 37
    return-void
.end method

.method private b(Lcom/google/android/gms/ads/internal/o/a;)Z
    .locals 2

    .prologue
    .line 65
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/c;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/a/d;

    .line 67
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/a/d;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/o/a;)Lcom/google/android/gms/ads/internal/a/d;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p2, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/ads/internal/a/c;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/o/a;Landroid/view/View;)Lcom/google/android/gms/ads/internal/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/o/a;Landroid/view/View;)Lcom/google/android/gms/ads/internal/a/d;
    .locals 7

    .prologue
    .line 50
    iget-object v6, p0, Lcom/google/android/gms/ads/internal/a/c;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 51
    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/ads/internal/a/c;->b(Lcom/google/android/gms/ads/internal/o/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/c;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/a/d;

    monitor-exit v6

    .line 60
    :goto_0
    return-object v0

    .line 55
    :cond_0
    new-instance v0, Lcom/google/android/gms/ads/internal/a/d;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/a/c;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/a/c;->f:Lcom/google/android/gms/ads/internal/i/a;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/a/d;-><init>(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/o/a;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Landroid/view/View;Lcom/google/android/gms/ads/internal/i/a;)V

    .line 57
    invoke-virtual {v0, p0}, Lcom/google/android/gms/ads/internal/a/d;->a(Lcom/google/android/gms/ads/internal/a/o;)V

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/c;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/a/d;)V
    .locals 2

    .prologue
    .line 86
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 87
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/a/d;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 90
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/o/a;)V
    .locals 2

    .prologue
    .line 76
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/c;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/a/d;

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/a/d;->c()V

    .line 81
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
