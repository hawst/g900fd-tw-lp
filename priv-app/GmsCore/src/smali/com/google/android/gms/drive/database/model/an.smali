.class final enum Lcom/google/android/gms/drive/database/model/an;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum f:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum g:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum h:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum i:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum j:Lcom/google/android/gms/drive/database/model/an;

.field public static final enum k:Lcom/google/android/gms/drive/database/model/an;

.field private static final p:Ljava/util/Map;

.field private static final synthetic q:[Lcom/google/android/gms/drive/database/model/an;


# instance fields
.field private final l:Ljava/lang/String;

.field private final m:I

.field private final n:I

.field private final o:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/google/android/gms/drive/database/model/an;

    const-string v1, "COLLECTION"

    const-string v3, "folder"

    sget v4, Lcom/google/android/gms/h;->bo:I

    sget v5, Lcom/google/android/gms/h;->bv:I

    sget v6, Lcom/google/android/gms/p;->gb:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/an;->a:Lcom/google/android/gms/drive/database/model/an;

    .line 21
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "DOCUMENT"

    const-string v6, "document"

    sget v7, Lcom/google/android/gms/h;->bk:I

    sget v8, Lcom/google/android/gms/p;->gc:I

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->b:Lcom/google/android/gms/drive/database/model/an;

    .line 23
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "DRAWING"

    const-string v6, "drawing"

    sget v7, Lcom/google/android/gms/h;->bl:I

    sget v8, Lcom/google/android/gms/p;->gd:I

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->c:Lcom/google/android/gms/drive/database/model/an;

    .line 26
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "FILE"

    const-string v6, "file"

    sget v7, Lcom/google/android/gms/h;->bn:I

    sget v8, Lcom/google/android/gms/p;->ga:I

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->d:Lcom/google/android/gms/drive/database/model/an;

    .line 27
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "FORM"

    const-string v6, "form"

    sget v7, Lcom/google/android/gms/h;->bp:I

    sget v8, Lcom/google/android/gms/p;->ge:I

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->e:Lcom/google/android/gms/drive/database/model/an;

    .line 28
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "MAP"

    const/4 v5, 0x5

    const-string v6, "map"

    sget v7, Lcom/google/android/gms/h;->bs:I

    sget v8, Lcom/google/android/gms/p;->gf:I

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->f:Lcom/google/android/gms/drive/database/model/an;

    .line 31
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "MAP_DOGFOOD"

    const/4 v5, 0x6

    const-string v6, "drive-sdk.796396377186"

    sget v7, Lcom/google/android/gms/h;->bs:I

    sget v8, Lcom/google/android/gms/p;->gf:I

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->g:Lcom/google/android/gms/drive/database/model/an;

    .line 33
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "PRESENTATION"

    const/4 v5, 0x7

    const-string v6, "presentation"

    sget v7, Lcom/google/android/gms/h;->bx:I

    sget v8, Lcom/google/android/gms/p;->gg:I

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->h:Lcom/google/android/gms/drive/database/model/an;

    .line 35
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "SPREADSHEET"

    const/16 v5, 0x8

    const-string v6, "spreadsheet"

    sget v7, Lcom/google/android/gms/h;->bw:I

    sget v8, Lcom/google/android/gms/p;->gh:I

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->i:Lcom/google/android/gms/drive/database/model/an;

    .line 37
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "TABLE"

    const/16 v5, 0x9

    const-string v6, "table"

    sget v7, Lcom/google/android/gms/h;->bq:I

    sget v8, Lcom/google/android/gms/p;->gi:I

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->j:Lcom/google/android/gms/drive/database/model/an;

    .line 42
    new-instance v3, Lcom/google/android/gms/drive/database/model/an;

    const-string v4, "UNKNOWN"

    const/16 v5, 0xa

    const-string v6, "unknown"

    sget v7, Lcom/google/android/gms/h;->bn:I

    sget v8, Lcom/google/android/gms/p;->gq:I

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v3, Lcom/google/android/gms/drive/database/model/an;->k:Lcom/google/android/gms/drive/database/model/an;

    .line 17
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/an;

    sget-object v1, Lcom/google/android/gms/drive/database/model/an;->a:Lcom/google/android/gms/drive/database/model/an;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/database/model/an;->b:Lcom/google/android/gms/drive/database/model/an;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/an;->c:Lcom/google/android/gms/drive/database/model/an;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/an;->d:Lcom/google/android/gms/drive/database/model/an;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/gms/drive/database/model/an;->e:Lcom/google/android/gms/drive/database/model/an;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v3, Lcom/google/android/gms/drive/database/model/an;->f:Lcom/google/android/gms/drive/database/model/an;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    sget-object v3, Lcom/google/android/gms/drive/database/model/an;->g:Lcom/google/android/gms/drive/database/model/an;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    sget-object v3, Lcom/google/android/gms/drive/database/model/an;->h:Lcom/google/android/gms/drive/database/model/an;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    sget-object v3, Lcom/google/android/gms/drive/database/model/an;->i:Lcom/google/android/gms/drive/database/model/an;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    sget-object v3, Lcom/google/android/gms/drive/database/model/an;->j:Lcom/google/android/gms/drive/database/model/an;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    sget-object v3, Lcom/google/android/gms/drive/database/model/an;->k:Lcom/google/android/gms/drive/database/model/an;

    aput-object v3, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/an;->q:[Lcom/google/android/gms/drive/database/model/an;

    .line 84
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 85
    invoke-static {}, Lcom/google/android/gms/drive/database/model/an;->values()[Lcom/google/android/gms/drive/database/model/an;

    move-result-object v1

    array-length v3, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v1, v2

    .line 86
    iget-object v5, v4, Lcom/google/android/gms/drive/database/model/an;->l:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 87
    iget-object v5, v4, Lcom/google/android/gms/drive/database/model/an;->l:Ljava/lang/String;

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 90
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/database/model/an;->p:Ljava/util/Map;

    .line 91
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 7

    .prologue
    .line 58
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/an;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    .line 59
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;III)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput-object p3, p0, Lcom/google/android/gms/drive/database/model/an;->l:Ljava/lang/String;

    .line 52
    iput p4, p0, Lcom/google/android/gms/drive/database/model/an;->m:I

    .line 53
    iput p5, p0, Lcom/google/android/gms/drive/database/model/an;->n:I

    .line 54
    iput p6, p0, Lcom/google/android/gms/drive/database/model/an;->o:I

    .line 55
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/an;
    .locals 2

    .prologue
    .line 102
    if-nez p0, :cond_1

    .line 104
    sget-object v0, Lcom/google/android/gms/drive/database/model/an;->k:Lcom/google/android/gms/drive/database/model/an;

    .line 108
    :cond_0
    :goto_0
    return-object v0

    .line 105
    :cond_1
    const-string v0, "application/vnd.google-apps."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 106
    sget-object v0, Lcom/google/android/gms/drive/database/model/an;->d:Lcom/google/android/gms/drive/database/model/an;

    goto :goto_0

    .line 108
    :cond_2
    const/16 v0, 0x1c

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/an;->p:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/an;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/database/model/an;->k:Lcom/google/android/gms/drive/database/model/an;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/an;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/gms/drive/database/model/an;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/an;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/an;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/gms/drive/database/model/an;->q:[Lcom/google/android/gms/drive/database/model/an;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/an;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/an;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/gms/drive/database/model/an;->m:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/gms/drive/database/model/an;->n:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/gms/drive/database/model/an;->o:I

    return v0
.end method
