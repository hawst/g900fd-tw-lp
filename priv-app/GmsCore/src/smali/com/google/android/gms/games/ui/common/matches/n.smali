.class public final Lcom/google/android/gms/games/ui/common/matches/n;
.super Lcom/google/android/gms/games/ui/card/b;
.source "SourceFile"


# instance fields
.field private final h:Lcom/google/android/gms/games/ui/common/matches/o;

.field private final i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/o;)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/common/matches/n;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/o;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/o;I)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/b;-><init>(Landroid/content/Context;)V

    .line 89
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/n;->h:Lcom/google/android/gms/games/ui/common/matches/o;

    .line 90
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->i:Z

    .line 92
    sget v0, Lcom/google/android/gms/k;->m:I

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/ui/common/matches/n;->e(II)V

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/matches/n;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->i:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/matches/n;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/matches/n;)Lcom/google/android/gms/games/ui/common/matches/o;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/n;->h:Lcom/google/android/gms/games/ui/common/matches/o;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/d;)V
    .locals 2

    .prologue
    .line 108
    if-nez p1, :cond_0

    .line 109
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/d;)V

    .line 118
    :goto_0
    return-void

    .line 111
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/common/data/a;

    const-string v1, "dataBuffer should be AbstractDataBuffer."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 113
    new-instance v0, Lcom/google/android/gms/common/data/s;

    check-cast p1, Lcom/google/android/gms/common/data/a;

    const-string v1, "external_match_id"

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/common/data/s;-><init>(Lcom/google/android/gms/common/data/a;Ljava/lang/String;)V

    .line 116
    invoke-super {p0, v0}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/d;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/n;->j:Ljava/lang/String;

    .line 103
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/matches/n;->k:Ljava/lang/String;

    .line 104
    return-void
.end method

.method protected final b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/p;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/p;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final w()I
    .locals 1

    .prologue
    .line 127
    sget v0, Lcom/google/android/gms/j;->gE:I

    return v0
.end method

.method protected final x()I
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x3

    return v0
.end method
