.class public final Lcom/google/android/gms/wallet/shared/common/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/16 v0, 0x400

    iput v0, p0, Lcom/google/android/gms/wallet/shared/common/d/a;->a:I

    .line 31
    return-void
.end method

.method public constructor <init>(B)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/wallet/shared/common/d/a;-><init>()V

    .line 38
    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 101
    if-eqz p0, :cond_0

    .line 102
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 104
    :cond_0
    if-eqz p1, :cond_1

    .line 106
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :cond_1
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    const-string v1, "HttpDownloader"

    const-string v2, "Exception closing output stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/common/d/c;Ljava/io/OutputStream;I)Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 52
    .line 55
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/wallet/shared/common/d/c;->a:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :try_start_1
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_0

    .line 57
    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/shared/common/d/a;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    move v0, v1

    .line 81
    :goto_0
    return v0

    .line 59
    :cond_0
    :try_start_2
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/wallet/shared/common/d/a;->a:I

    invoke-direct {v5, v2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 61
    new-instance v3, Ljava/io/BufferedOutputStream;

    iget v2, p0, Lcom/google/android/gms/wallet/shared/common/d/a;->a:I

    invoke-direct {v3, p2, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 62
    :try_start_3
    iget v2, p0, Lcom/google/android/gms/wallet/shared/common/d/a;->a:I

    div-int/lit8 v2, v2, 0x8

    new-array v6, v2, [B

    .line 65
    if-gtz p3, :cond_4

    .line 66
    const p3, 0x7fffffff

    move v2, v1

    move v4, v1

    .line 68
    :cond_1
    :goto_1
    const/4 v7, -0x1

    if-eq v4, v7, :cond_3

    .line 69
    invoke-virtual {v5, v6}, Ljava/io/BufferedInputStream;->read([B)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v4

    .line 70
    add-int/2addr v2, v4

    .line 71
    if-le v2, p3, :cond_2

    .line 72
    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/shared/common/d/a;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    move v0, v1

    goto :goto_0

    .line 74
    :cond_2
    if-lez v4, :cond_1

    .line 75
    const/4 v7, 0x0

    :try_start_4
    invoke-virtual {v3, v6, v7, v4}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    .line 79
    :catch_0
    move-exception v2

    move-object v4, v0

    move-object v0, v2

    move-object v2, v3

    .line 80
    :goto_2
    :try_start_5
    const-string v3, "HttpDownloader"

    const-string v5, "Exception downloading image"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 81
    invoke-static {v4, v2}, Lcom/google/android/gms/wallet/shared/common/d/a;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    move v0, v1

    goto :goto_0

    .line 78
    :cond_3
    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/shared/common/d/a;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    const/4 v0, 0x1

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    move-object v1, v4

    :goto_3
    invoke-static {v1, v4}, Lcom/google/android/gms/wallet/shared/common/d/a;->a(Ljava/net/HttpURLConnection;Ljava/io/OutputStream;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v4, v3

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_3

    :catchall_3
    move-exception v0

    move-object v1, v4

    move-object v4, v2

    goto :goto_3

    .line 79
    :catch_1
    move-exception v0

    move-object v2, v4

    goto :goto_2

    :catch_2
    move-exception v2

    move-object v8, v2

    move-object v2, v4

    move-object v4, v0

    move-object v0, v8

    goto :goto_2

    :cond_4
    move v2, v1

    move v4, v1

    goto :goto_1
.end method
