.class public final Lcom/google/android/gms/smart_profile/card/a/f;
.super Lcom/google/android/gms/smart_profile/card/a/b;
.source "SourceFile"


# static fields
.field private static final e:Landroid/net/Uri;


# instance fields
.field private final f:Ljava/util/List;

.field private g:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    sget-object v0, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "allow_voicemails"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/card/a/f;->e:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/b;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/f;->f:Ljava/util/List;

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 294
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 295
    :cond_0
    const/4 v0, 0x0

    .line 297
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 281
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 282
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 286
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 287
    const/high16 v3, 0x43340000    # 180.0f

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 288
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v6, v6, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 290
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, p0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method private d()[Lcom/google/ac/c/a/a/a/ag;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/ai;->b:[Lcom/google/ac/c/a/a/a/ag;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/ai;->b:[Lcom/google/ac/c/a/a/a/ag;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 202
    invoke-super {p0, p1}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Landroid/os/Bundle;)V

    .line 203
    const-string v0, "is_expanded"

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/f;->g:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 204
    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V
    .locals 21

    .prologue
    .line 65
    invoke-super/range {p0 .. p3}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V

    .line 67
    if-nez p3, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 73
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/smart_profile/card/a/f;->d()[Lcom/google/ac/c/a/a/a/ag;

    move-result-object v14

    .line 74
    if-eqz v14, :cond_0

    .line 78
    const/4 v2, 0x0

    .line 79
    if-eqz p2, :cond_d

    .line 80
    const-string v2, "is_expanded"

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    move v3, v2

    .line 83
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/card/a/f;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v2, Landroid/support/v7/widget/CardView;

    invoke-virtual {v2}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 85
    const/4 v2, 0x0

    move v4, v2

    :goto_2
    array-length v2, v14

    if-ge v4, v2, :cond_c

    .line 86
    aget-object v12, v14, v4

    .line 87
    const/4 v8, -0x1

    .line 88
    const/4 v2, 0x0

    .line 90
    const/4 v6, 0x0

    .line 91
    const/4 v7, 0x0

    .line 92
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    const-string v5, ""

    .line 94
    iget-object v9, v12, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    move v9, v8

    move-object v8, v2

    move-object/from16 v20, v6

    move v6, v7

    move-object/from16 v7, v20

    .line 150
    :goto_3
    iget-object v2, v12, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, v12, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    .line 152
    :goto_4
    iget-object v2, v12, Lcom/google/ac/c/a/a/a/ag;->g:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, v12, Lcom/google/ac/c/a/a/a/ag;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v11, v2

    .line 154
    :goto_5
    iget-object v2, v12, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, v12, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v12, v2

    .line 156
    :goto_6
    if-eqz v10, :cond_2

    .line 157
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_2
    if-eqz v11, :cond_3

    .line 160
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_3
    if-eqz v12, :cond_4

    .line 163
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    :cond_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    new-instance v5, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v5}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    const/4 v2, -0x1

    if-ne v9, v2, :cond_b

    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v5, v2}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    iget-object v5, v2, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    const/4 v9, 0x0

    iput-object v9, v5, Lcom/google/android/gms/smart_profile/card/view/g;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v12}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    iget-object v5, v2, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object v8, v5, Lcom/google/android/gms/smart_profile/card/view/g;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v7}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    iget-object v5, v2, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-boolean v6, v5, Lcom/google/android/gms/smart_profile/card/view/g;->j:Z

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/card/view/h;->a()Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/gms/smart_profile/card/view/h;->d(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    .line 181
    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_2

    .line 96
    :pswitch_0
    sget v8, Lcom/google/android/gms/h;->cP:I

    .line 97
    const-string v5, "android.intent.action.VIEW"

    iget-object v6, v12, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/gms/smart_profile/card/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 98
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/smart_profile/card/a/f;->f:Ljava/util/List;

    const-string v9, "7"

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    sget v5, Lcom/google/android/gms/p;->we:I

    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    sget v5, Lcom/google/android/gms/p;->wW:I

    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move v9, v8

    move-object v8, v2

    move-object/from16 v20, v6

    move v6, v7

    move-object/from16 v7, v20

    .line 102
    goto/16 :goto_3

    .line 104
    :pswitch_1
    sget v5, Lcom/google/android/gms/p;->wg:I

    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    sget v8, Lcom/google/android/gms/h;->cJ:I

    .line 107
    iget-object v5, v12, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    if-eqz v5, :cond_5

    .line 108
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/smart_profile/bg;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v2, v12, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    iget-object v2, v2, Lcom/google/ac/c/a/a/a/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget v6, Lcom/google/android/gms/h;->cI:I

    packed-switch v2, :pswitch_data_1

    const/4 v2, 0x0

    .line 112
    :cond_5
    :goto_8
    iget-object v5, v12, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    if-eqz v5, :cond_7

    iget-object v5, v12, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    iget-object v5, v5, Lcom/google/ac/c/a/a/a/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_7

    iget-object v5, v12, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    if-eqz v5, :cond_6

    const-string v6, "tel:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    :goto_9
    if-nez v5, :cond_7

    .line 115
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/google/android/gms/smart_profile/card/a/f;->e:Landroid/net/Uri;

    iget-object v9, v12, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    iget-object v9, v9, Lcom/google/ac/c/a/a/a/ae;->c:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    int-to-long v10, v9

    invoke-static {v6, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v5

    const-string v6, "EXTRA_VOICEMAIL_URI"

    iget-object v9, v12, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v6

    .line 120
    sget v5, Lcom/google/android/gms/p;->wm:I

    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 125
    :goto_a
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/smart_profile/card/a/f;->f:Ljava/util/List;

    const-string v10, "8"

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v9, v8

    move-object v8, v2

    move-object/from16 v20, v6

    move v6, v7

    move-object/from16 v7, v20

    .line 126
    goto/16 :goto_3

    .line 108
    :pswitch_2
    invoke-static {v5, v6}, Lcom/google/android/gms/smart_profile/card/a/f;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v6, Lcom/google/android/gms/f;->ao:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_8

    :pswitch_3
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v6, Lcom/google/android/gms/f;->ao:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_8

    :pswitch_4
    invoke-static {v5, v6}, Lcom/google/android/gms/smart_profile/card/a/f;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    sget v9, Lcom/google/android/gms/f;->ap:I

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v6, v5, v9}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_8

    :pswitch_5
    sget v2, Lcom/google/android/gms/h;->cX:I

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sget v6, Lcom/google/android/gms/f;->aq:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_8

    .line 112
    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_9

    .line 122
    :cond_7
    const-string v5, "android.intent.action.DIAL"

    iget-object v6, v12, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/gms/smart_profile/card/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 123
    sget v5, Lcom/google/android/gms/p;->wf:I

    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_a

    .line 128
    :pswitch_6
    sget v8, Lcom/google/android/gms/h;->cO:I

    .line 129
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/smart_profile/bg;->v()Landroid/content/Context;

    move-result-object v5

    iget-object v6, v12, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v9

    new-instance v7, Landroid/content/Intent;

    const-string v10, "com.google.android.gm.intent.VIEW_PLID"

    invoke-direct {v7, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v10, "com.google.android.gm"

    invoke-virtual {v7, v10}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v10, "permalink"

    invoke-virtual {v7, v10, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v9}, Lcom/google/android/gms/identity/accounts/api/AccountData;->a(Ljava/lang/String;)Lcom/google/android/gms/identity/accounts/api/AccountData;

    move-result-object v6

    invoke-static {v5, v7, v6}, Lcom/google/android/gms/identity/accounts/api/b;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/identity/accounts/api/AccountData;)Z

    .line 133
    iget-object v5, v12, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v18, 0x3e8

    mul-long v10, v10, v18

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/smart_profile/bg;->v()Landroid/content/Context;

    move-result-object v5

    invoke-static {v10, v11, v5}, Lcom/google/android/gms/smart_profile/n;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v12, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    .line 135
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/smart_profile/card/a/f;->f:Ljava/util/List;

    const-string v6, "1"

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    const/4 v6, 0x1

    .line 137
    sget v5, Lcom/google/android/gms/p;->wS:I

    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    sget v5, Lcom/google/android/gms/p;->wW:I

    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move v9, v8

    move-object v8, v2

    .line 140
    goto/16 :goto_3

    .line 142
    :pswitch_7
    sget v8, Lcom/google/android/gms/h;->cK:I

    .line 143
    const-string v5, "android.intent.action.VIEW"

    iget-object v6, v12, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/gms/smart_profile/card/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 144
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/smart_profile/card/a/f;->f:Ljava/util/List;

    const-string v9, "3"

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    sget v5, Lcom/google/android/gms/p;->xn:I

    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    sget v5, Lcom/google/android/gms/p;->xm:I

    invoke-virtual {v15, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move v9, v8

    move-object v8, v2

    move-object/from16 v20, v6

    move v6, v7

    move-object/from16 v7, v20

    goto/16 :goto_3

    .line 150
    :cond_8
    const/4 v2, 0x0

    move-object v10, v2

    goto/16 :goto_4

    .line 152
    :cond_9
    const/4 v2, 0x0

    move-object v11, v2

    goto/16 :goto_5

    .line 154
    :cond_a
    const/4 v2, 0x0

    move-object v12, v2

    goto/16 :goto_6

    .line 166
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v2, Landroid/support/v7/widget/CardView;

    invoke-virtual {v2}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v17, Lcom/google/android/gms/f;->aw:I

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v9}, Lcom/google/android/gms/smart_profile/bg;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto/16 :goto_7

    .line 183
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v2, Landroid/support/v7/widget/CardView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/support/v7/widget/CardView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/smart_profile/card/a/f;->g:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    .line 184
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v4

    sget-object v2, Lcom/google/android/gms/smart_profile/a/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/smart_profile/card/a/f;->g:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    const/4 v5, 0x0

    invoke-interface {v13, v5, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    sget-object v2, Lcom/google/android/gms/smart_profile/a/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v6

    invoke-virtual {v4, v5, v2, v3, v6}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;IZI)V

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/card/a/f;->g:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/smart_profile/card/a/b;->c:Lcom/google/android/gms/smart_profile/card/h;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/smart_profile/card/h;)V

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/card/a/f;->g:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    sget-object v3, Lcom/google/android/gms/smart_profile/c;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v2, Landroid/support/v7/widget/CardView;

    sget v3, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 195
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v3, Landroid/support/v7/widget/CardView;

    invoke-virtual {v3}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->wo:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_d
    move v3, v2

    goto/16 :goto_1

    .line 94
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_7
    .end packed-switch

    .line 108
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/f;->d()[Lcom/google/ac/c/a/a/a/ag;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 214
    sget-object v0, Lcom/google/android/gms/smart_profile/c;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/f;->f:Ljava/util/List;

    return-object v0
.end method
