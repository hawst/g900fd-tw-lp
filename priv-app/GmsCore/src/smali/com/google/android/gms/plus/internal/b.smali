.class public abstract Lcom/google/android/gms/plus/internal/b;
.super Lcom/google/android/gms/plus/internal/g;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/g;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public final a(ILandroid/os/Bundle;I)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;)V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Settings;)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public final a(ILandroid/os/Bundle;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public a(ILandroid/os/Bundle;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public b(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 47
    return-void
.end method
