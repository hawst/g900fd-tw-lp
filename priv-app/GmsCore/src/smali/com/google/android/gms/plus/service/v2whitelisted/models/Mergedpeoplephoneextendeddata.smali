.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/i;

.field private static final g:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;

.field e:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$ShortCode;

.field f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/i;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/i;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 40
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->g:Ljava/util/HashMap;

    const-string v1, "formattedType"

    const-string v2, "formattedType"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->g:Ljava/util/HashMap;

    const-string v1, "phoneNumber"

    const-string v2, "phoneNumber"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->g:Ljava/util/HashMap;

    const-string v1, "shortCode"

    const-string v2, "shortCode"

    const/4 v3, 0x4

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$ShortCode;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->g:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 80
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->b:I

    .line 81
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->a:Ljava/util/Set;

    .line 82
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$ShortCode;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->a:Ljava/util/Set;

    .line 94
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->b:I

    .line 95
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->c:Ljava/lang/String;

    .line 96
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;

    .line 97
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->e:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$ShortCode;

    .line 98
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->f:Ljava/lang/String;

    .line 99
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 1011
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1012
    packed-switch v0, :pswitch_data_0

    .line 1020
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1014
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;

    .line 1024
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1025
    return-void

    .line 1017
    :pswitch_1
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$ShortCode;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->e:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$ShortCode;

    goto :goto_0

    .line 1012
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 993
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 994
    packed-switch v0, :pswitch_data_0

    .line 1002
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 996
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->c:Ljava/lang/String;

    .line 1005
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1006
    return-void

    .line 999
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->f:Ljava/lang/String;

    goto :goto_0

    .line 994
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 958
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 963
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 973
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 965
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->c:Ljava/lang/String;

    .line 971
    :goto_0
    return-object v0

    .line 967
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;

    goto :goto_0

    .line 969
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->e:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$ShortCode;

    goto :goto_0

    .line 971
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->f:Ljava/lang/String;

    goto :goto_0

    .line 963
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 948
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/i;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1042
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;

    if-nez v0, :cond_0

    move v0, v1

    .line 1073
    :goto_0
    return v0

    .line 1047
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 1048
    goto :goto_0

    .line 1051
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;

    .line 1052
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1053
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1054
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1056
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1058
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1063
    goto :goto_0

    .line 1066
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1068
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1073
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1029
    const/4 v0, 0x0

    .line 1030
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1031
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1032
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 1033
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 1035
    goto :goto_0

    .line 1036
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 953
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/i;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/i;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata;Landroid/os/Parcel;I)V

    .line 954
    return-void
.end method
