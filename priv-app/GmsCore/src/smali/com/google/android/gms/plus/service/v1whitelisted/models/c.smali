.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public final d:Ljava/util/Set;

.field private e:Ljava/util/List;

.field private f:Z

.field private g:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;

.field private h:Z

.field private i:Ljava/util/List;

.field private j:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 596
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->d:Ljava/util/Set;

    .line 597
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 11

    .prologue
    .line 699
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->d:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->e:Ljava/util/List;

    iget-boolean v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->f:Z

    iget-object v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->g:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;

    iget-boolean v6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->h:Z

    iget-object v7, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->i:Ljava/util/List;

    iget-object v8, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->b:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;

    iget-object v10, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField;-><init>(Ljava/util/Set;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$LabelEntity;ZLjava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/i;)Lcom/google/android/gms/plus/service/v1whitelisted/models/c;
    .locals 2

    .prologue
    .line 681
    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;

    .line 682
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->d:Ljava/util/Set;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 683
    return-object p0
.end method
