.class public final Lcom/google/android/gms/wallet/dynamite/service/a/b;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V
    .locals 6

    .prologue
    .line 38
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 29
    invoke-static {p1}, Lcom/google/android/gms/wallet/c/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/wallet/c/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 66
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/c/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/c/a;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 2

    .prologue
    .line 59
    const v0, 0x6768a8

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/gms/common/internal/bj;->k(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;)V

    .line 62
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/f;)V
    .locals 3

    .prologue
    .line 110
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/c/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/c/a;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/f;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 113
    const/16 v0, 0x8

    const/4 v1, 0x0

    :try_start_1
    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v0, v1, v2}, Lcom/google/android/gms/wallet/c/f;->a(IILandroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 115
    :catch_1
    move-exception v0

    .line 116
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 3

    .prologue
    .line 83
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/c/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/c/a;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 86
    const/16 v0, 0x8

    const/4 v1, 0x0

    :try_start_1
    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v0, v1, v2}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 87
    :catch_1
    move-exception v0

    .line 88
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 3

    .prologue
    .line 96
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/c/a;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/wallet/c/a;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 100
    const/16 v0, 0x8

    const/4 v1, 0x0

    :try_start_1
    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p4, v0, v1, v2}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "com.google.android.gms.wallet.service.BIND"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 74
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/c/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/c/a;->b(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "com.google.android.gms.wallet.internal.IOwService"

    return-object v0
.end method
