.class public Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/wearable/node/emulator/a;

.field private static final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 96
    return-void
.end method

.method private static a()V
    .locals 2

    .prologue
    .line 88
    sget-object v1, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 89
    :try_start_0
    sget-object v0, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->a:Lcom/google/android/gms/wearable/node/emulator/a;

    if-eqz v0, :cond_0

    .line 90
    sget-object v0, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->a:Lcom/google/android/gms/wearable/node/emulator/a;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/emulator/a;->a()V

    .line 91
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->a:Lcom/google/android/gms/wearable/node/emulator/a;

    .line 93
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wearable/service/y;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 83
    invoke-static {}, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->a()V

    .line 84
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 85
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 64
    const-string v0, "run_as_server"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 65
    const-string v0, "connection_config"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/ConnectionConfiguration;

    .line 67
    const-string v2, "connection_remove"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    sget-object v2, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 70
    :try_start_0
    sget-object v3, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->a:Lcom/google/android/gms/wearable/node/emulator/a;

    if-nez v3, :cond_0

    .line 71
    new-instance v3, Lcom/google/android/gms/wearable/node/emulator/a;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v1, v0, v4}, Lcom/google/android/gms/wearable/node/emulator/a;-><init>(Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;ZLcom/google/android/gms/wearable/ConnectionConfiguration;B)V

    .line 72
    sput-object v3, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->a:Lcom/google/android/gms/wearable/node/emulator/a;

    invoke-virtual {v3}, Lcom/google/android/gms/wearable/node/emulator/a;->start()V

    .line 74
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :goto_0
    const/4 v0, 0x3

    return v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 76
    :cond_1
    invoke-static {}, Lcom/google/android/gms/wearable/node/emulator/NetworkConnectionService;->a()V

    goto :goto_0
.end method
