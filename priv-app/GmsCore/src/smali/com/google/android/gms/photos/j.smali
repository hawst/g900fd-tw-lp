.class public final enum Lcom/google/android/gms/photos/j;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/photos/j;

.field public static final enum b:Lcom/google/android/gms/photos/j;

.field public static final enum c:Lcom/google/android/gms/photos/j;

.field public static final enum d:Lcom/google/android/gms/photos/j;

.field private static final synthetic f:[Lcom/google/android/gms/photos/j;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 45
    new-instance v0, Lcom/google/android/gms/photos/j;

    const-string v1, "AutoBackupDeclineButton"

    const/16 v2, 0x21fd

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/gms/photos/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/photos/j;->a:Lcom/google/android/gms/photos/j;

    .line 46
    new-instance v0, Lcom/google/android/gms/photos/j;

    const-string v1, "AutoBackupEnableDialog"

    const/16 v2, 0x21fa

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/photos/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/photos/j;->b:Lcom/google/android/gms/photos/j;

    .line 47
    new-instance v0, Lcom/google/android/gms/photos/j;

    const-string v1, "AutoBackupEnableWifiOnlyButton"

    const/16 v2, 0x21fb

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/gms/photos/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/photos/j;->c:Lcom/google/android/gms/photos/j;

    .line 48
    new-instance v0, Lcom/google/android/gms/photos/j;

    const-string v1, "AutoBackupEnableWifiOrMobileDataButton"

    const/16 v2, 0x21fc

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/photos/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/photos/j;->d:Lcom/google/android/gms/photos/j;

    .line 44
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/photos/j;

    sget-object v1, Lcom/google/android/gms/photos/j;->a:Lcom/google/android/gms/photos/j;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/photos/j;->b:Lcom/google/android/gms/photos/j;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/photos/j;->c:Lcom/google/android/gms/photos/j;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/photos/j;->d:Lcom/google/android/gms/photos/j;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/photos/j;->f:[Lcom/google/android/gms/photos/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput p3, p0, Lcom/google/android/gms/photos/j;->e:I

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/j;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/photos/j;->e:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/photos/j;
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/google/android/gms/photos/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/j;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/photos/j;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/gms/photos/j;->f:[Lcom/google/android/gms/photos/j;

    invoke-virtual {v0}, [Lcom/google/android/gms/photos/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/photos/j;

    return-object v0
.end method
