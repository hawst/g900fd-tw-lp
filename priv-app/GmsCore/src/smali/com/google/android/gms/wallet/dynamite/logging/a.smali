.class public final Lcom/google/android/gms/wallet/dynamite/logging/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:J


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/logging/a;->a:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/google/android/gms/wallet/dynamite/logging/a;->b:Ljava/lang/String;

    .line 48
    iput-wide p3, p0, Lcom/google/android/gms/wallet/dynamite/logging/a;->c:J

    .line 49
    iput-wide p5, p0, Lcom/google/android/gms/wallet/dynamite/logging/a;->d:J

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 30
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p1, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v6

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/wallet/dynamite/logging/a;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 32
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/wallet/dynamite/logging/a;
    .locals 10

    .prologue
    const-wide/16 v6, -0x1

    const/4 v9, 0x3

    const/4 v5, 0x1

    const/4 v0, 0x0

    const/4 v8, 0x2

    .line 95
    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 102
    array-length v2, v1

    if-gt v2, v8, :cond_0

    .line 117
    :goto_0
    return-object v0

    .line 105
    :cond_0
    array-length v2, v1

    if-lez v2, :cond_4

    .line 106
    const/4 v2, 0x0

    aget-object v2, v1, v2

    const-string v3, "@"

    const-string v4, "\n"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 108
    :goto_1
    array-length v3, v1

    if-le v3, v5, :cond_3

    .line 109
    aget-object v3, v1, v5

    invoke-static {v3}, Lcom/google/android/gms/wallet/dynamite/logging/a;->b(Ljava/lang/String;)J

    move-result-wide v4

    .line 111
    :goto_2
    array-length v3, v1

    if-le v3, v8, :cond_1

    .line 112
    aget-object v3, v1, v8

    invoke-static {v3}, Lcom/google/android/gms/wallet/dynamite/logging/a;->b(Ljava/lang/String;)J

    move-result-wide v6

    .line 114
    :cond_1
    array-length v3, v1

    if-le v3, v9, :cond_2

    .line 115
    aget-object v3, v1, v9

    .line 117
    :goto_3
    new-instance v1, Lcom/google/android/gms/wallet/dynamite/logging/a;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/wallet/dynamite/logging/a;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v3, v0

    goto :goto_3

    :cond_3
    move-wide v4, v6

    goto :goto_2

    :cond_4
    move-object v2, v0

    goto :goto_1
.end method

.method private static b(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 122
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 124
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/logging/a;->a:Ljava/lang/String;

    const-string v1, "\n"

    const-string v2, "@"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget-wide v2, p0, Lcom/google/android/gms/wallet/dynamite/logging/a;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    iget-wide v2, p0, Lcom/google/android/gms/wallet/dynamite/logging/a;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/logging/a;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/logging/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    :cond_0
    const-string v0, "/\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
