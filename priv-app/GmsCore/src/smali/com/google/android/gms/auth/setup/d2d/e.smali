.class public abstract Lcom/google/android/gms/auth/setup/d2d/e;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/android/setupwizard/navigationbar/a;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final b:Lcom/google/android/gms/auth/d/a;


# instance fields
.field a:Lcom/google/android/gms/auth/f/e;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:J

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Lcom/google/android/setupwizard/util/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "BaseActivity"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/e;->b:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 46
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->d:Z

    iget-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/e;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a(ZZ)V

    .line 108
    return-void
.end method

.method protected final c()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 87
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    const-string v1, "theme"

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/e;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v1, "useImmersiveMode"

    iget-boolean v2, p0, Lcom/google/android/gms/auth/setup/d2d/e;->d:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 90
    const-string v1, "startTime"

    iget-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/e;->e:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 91
    const-string v1, "event"

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/e;->a:Lcom/google/android/gms/auth/f/e;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/f/e;->g()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 92
    return-object v0
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/e;->setResult(I)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/e;->finish()V

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->g:Lcom/google/android/setupwizard/util/c;

    invoke-virtual {v0}, Lcom/google/android/setupwizard/util/c;->a()V

    .line 121
    return-void
.end method

.method protected final e()Lcom/google/android/setupwizard/util/c;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->g:Lcom/google/android/setupwizard/util/c;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->a:Lcom/google/android/gms/auth/f/e;

    return-object v0
.end method

.method protected final g()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 148
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/e;->setResult(I)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/e;->finish()V

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->g:Lcom/google/android/setupwizard/util/c;

    iget-object v0, v0, Lcom/google/android/setupwizard/util/c;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/gms/b;->q:I

    sget v2, Lcom/google/android/gms/b;->r:I

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 115
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    if-eqz p1, :cond_3

    .line 54
    :goto_0
    new-instance v0, Lcom/google/android/gms/auth/f/e;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->a:Lcom/google/android/gms/auth/f/e;

    .line 55
    if-eqz p1, :cond_4

    .line 56
    const-string v0, "theme"

    const-string v2, "material_light"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->c:Ljava/lang/String;

    .line 57
    const-string v0, "useImmersiveMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->d:Z

    .line 58
    const-string v0, "startTime"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/e;->e:J

    .line 59
    const-string v0, "event"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    .line 62
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/e;->a:Lcom/google/android/gms/auth/f/e;

    array-length v3, v0

    invoke-virtual {v2, v0, v3}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :cond_0
    :goto_1
    new-instance v0, Lcom/google/android/setupwizard/util/c;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/c;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->g:Lcom/google/android/setupwizard/util/c;

    .line 74
    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/e;->c:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    :goto_2
    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 75
    :goto_3
    if-eqz v0, :cond_2

    .line 76
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/e;->setTheme(I)V

    .line 78
    :cond_2
    return-void

    .line 53
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/e;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    sget-object v2, Lcom/google/android/gms/auth/setup/d2d/e;->b:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 68
    :cond_4
    const-string v0, "material_light"

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->c:Ljava/lang/String;

    .line 69
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/e;->e:J

    goto :goto_1

    .line 74
    :sswitch_0
    const-string v3, "material"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_2

    :sswitch_1
    const-string v3, "material_light"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_2

    :pswitch_0
    sget v0, Lcom/google/android/gms/q;->z:I

    goto :goto_3

    :pswitch_1
    sget v0, Lcom/google/android/gms/q;->A:I

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        -0x4bb9bc02 -> :sswitch_1
        0x11d36527 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/e;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/b/a;->S:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->a:Lcom/google/android/gms/auth/f/e;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/auth/setup/d2d/e;->e:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/auth/f/e;->a(J)Lcom/google/android/gms/auth/f/e;

    new-instance v0, Lcom/google/android/gms/auth/f/b;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/b;-><init>()V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/b;->a(I)Lcom/google/android/gms/auth/f/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/e;->a:Lcom/google/android/gms/auth/f/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/e;)Lcom/google/android/gms/auth/f/b;

    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/e;->b:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Sending PlayLog event."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/playlog/a;

    const/16 v2, 0x19

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/playlog/a;-><init>(Landroid/content/Context;I)V

    const-string v2, "D2D"

    invoke-virtual {v0}, Lcom/google/android/gms/auth/f/b;->g()[B

    move-result-object v0

    new-array v3, v6, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/playlog/a;->a()V

    .line 102
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 103
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/e;->c()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 84
    return-void
.end method
