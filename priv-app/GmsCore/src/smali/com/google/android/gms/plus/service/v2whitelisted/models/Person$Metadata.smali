.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/bc;

.field private static final u:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/util/List;

.field d:Ljava/util/List;

.field e:Ljava/util/List;

.field f:Z

.field g:Ljava/util/List;

.field h:Ljava/util/List;

.field i:Ljava/lang/String;

.field j:Z

.field k:Ljava/util/List;

.field l:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$IdentityInfo;

.field m:Z

.field n:Ljava/util/List;

.field o:J

.field p:Ljava/lang/String;

.field q:Ljava/lang/String;

.field r:Ljava/util/List;

.field s:Ljava/lang/String;

.field t:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$ProfileOwnerStats;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 5675
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/bc;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/bc;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/bc;

    .line 5782
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 5785
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "affinities"

    const-string v2, "affinities"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$Affinities;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5787
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "attributions"

    const-string v2, "attributions"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5788
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "blockTypes"

    const-string v2, "blockTypes"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5789
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "blocked"

    const-string v2, "blocked"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5790
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "circles"

    const-string v2, "circles"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5791
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "contacts"

    const-string v2, "contacts"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5792
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "customResponseMaskingType"

    const-string v2, "customResponseMaskingType"

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5793
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "deleted"

    const-string v2, "deleted"

    const/16 v3, 0x9

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5794
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "groups"

    const-string v2, "groups"

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5795
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "identityInfo"

    const-string v2, "identityInfo"

    const/16 v3, 0xb

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$IdentityInfo;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5798
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "inViewerDomain"

    const-string v2, "inViewerDomain"

    const/16 v3, 0xc

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5799
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "incomingBlockTypes"

    const-string v2, "incomingBlockTypes"

    const/16 v3, 0xd

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5800
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "lastUpdateTimeMicros"

    const-string v2, "lastUpdateTimeMicros"

    const/16 v3, 0xe

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5801
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "objectType"

    const-string v2, "objectType"

    const/16 v3, 0xf

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5802
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "ownerId"

    const-string v2, "ownerId"

    const/16 v3, 0x10

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5803
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "ownerUserTypes"

    const-string v2, "ownerUserTypes"

    const/16 v3, 0x11

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5804
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "plusPageType"

    const-string v2, "plusPageType"

    const/16 v3, 0x12

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5805
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    const-string v1, "profileOwnerStats"

    const-string v2, "profileOwnerStats"

    const/16 v3, 0x13

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$ProfileOwnerStats;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5808
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5947
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 5948
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->b:I

    .line 5949
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    .line 5950
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/List;Ljava/util/List;Ljava/util/List;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;ZLjava/util/List;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$IdentityInfo;ZLjava/util/List;JLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$ProfileOwnerStats;)V
    .locals 3

    .prologue
    .line 5974
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 5975
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    .line 5976
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->b:I

    .line 5977
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->c:Ljava/util/List;

    .line 5978
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d:Ljava/util/List;

    .line 5979
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->e:Ljava/util/List;

    .line 5980
    iput-boolean p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f:Z

    .line 5981
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g:Ljava/util/List;

    .line 5982
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->h:Ljava/util/List;

    .line 5983
    iput-object p9, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->i:Ljava/lang/String;

    .line 5984
    iput-boolean p10, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->j:Z

    .line 5985
    iput-object p11, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->k:Ljava/util/List;

    .line 5986
    iput-object p12, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->l:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$IdentityInfo;

    .line 5987
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->m:Z

    .line 5988
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n:Ljava/util/List;

    .line 5989
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->o:J

    .line 5990
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->p:Ljava/lang/String;

    .line 5991
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q:Ljava/lang/String;

    .line 5992
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->r:Ljava/util/List;

    .line 5993
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->s:Ljava/lang/String;

    .line 5994
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->t:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$ProfileOwnerStats;

    .line 5995
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 5812
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 7115
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 7116
    packed-switch v0, :pswitch_data_0

    .line 7121
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a long."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7118
    :pswitch_0
    iput-wide p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->o:J

    .line 7124
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 7125
    return-void

    .line 7116
    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 7209
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 7210
    sparse-switch v0, :sswitch_data_0

    .line 7218
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7212
    :sswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$IdentityInfo;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->l:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$IdentityInfo;

    .line 7222
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 7223
    return-void

    .line 7215
    :sswitch_1
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$ProfileOwnerStats;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->t:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$ProfileOwnerStats;

    goto :goto_0

    .line 7210
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x13 -> :sswitch_1
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 7151
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 7152
    sparse-switch v0, :sswitch_data_0

    .line 7166
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7154
    :sswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->i:Ljava/lang/String;

    .line 7169
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 7170
    return-void

    .line 7157
    :sswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->p:Ljava/lang/String;

    goto :goto_0

    .line 7160
    :sswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q:Ljava/lang/String;

    goto :goto_0

    .line 7163
    :sswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->s:Ljava/lang/String;

    goto :goto_0

    .line 7152
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0xf -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 7228
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 7229
    packed-switch v0, :pswitch_data_0

    .line 7234
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7231
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->c:Ljava/util/List;

    .line 7238
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 7239
    return-void

    .line 7229
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 7130
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 7131
    sparse-switch v0, :sswitch_data_0

    .line 7142
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7133
    :sswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f:Z

    .line 7145
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 7146
    return-void

    .line 7136
    :sswitch_1
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->j:Z

    goto :goto_0

    .line 7139
    :sswitch_2
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->m:Z

    goto :goto_0

    .line 7131
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_1
        0xc -> :sswitch_2
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 7052
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 7057
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 7095
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7059
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->c:Ljava/util/List;

    .line 7093
    :goto_0
    return-object v0

    .line 7061
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d:Ljava/util/List;

    goto :goto_0

    .line 7063
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->e:Ljava/util/List;

    goto :goto_0

    .line 7065
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 7067
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g:Ljava/util/List;

    goto :goto_0

    .line 7069
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->h:Ljava/util/List;

    goto :goto_0

    .line 7071
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->i:Ljava/lang/String;

    goto :goto_0

    .line 7073
    :pswitch_7
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 7075
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->k:Ljava/util/List;

    goto :goto_0

    .line 7077
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->l:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$IdentityInfo;

    goto :goto_0

    .line 7079
    :pswitch_a
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->m:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 7081
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n:Ljava/util/List;

    goto :goto_0

    .line 7083
    :pswitch_c
    iget-wide v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->o:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 7085
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->p:Ljava/lang/String;

    goto :goto_0

    .line 7087
    :pswitch_e
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q:Ljava/lang/String;

    goto :goto_0

    .line 7089
    :pswitch_f
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->r:Ljava/util/List;

    goto :goto_0

    .line 7091
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->s:Ljava/lang/String;

    goto :goto_0

    .line 7093
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->t:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata$ProfileOwnerStats;

    goto :goto_0

    .line 7057
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 6048
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->c:Ljava/util/List;

    return-object v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 7175
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 7176
    packed-switch v0, :pswitch_data_0

    .line 7199
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an array of String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 7178
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d:Ljava/util/List;

    .line 7203
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 7204
    return-void

    .line 7181
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->e:Ljava/util/List;

    goto :goto_0

    .line 7184
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g:Ljava/util/List;

    goto :goto_0

    .line 7187
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->h:Ljava/util/List;

    goto :goto_0

    .line 7190
    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->k:Ljava/util/List;

    goto :goto_0

    .line 7193
    :pswitch_6
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n:Ljava/util/List;

    goto :goto_0

    .line 7196
    :pswitch_7
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->r:Ljava/util/List;

    goto :goto_0

    .line 7176
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 6098
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f:Z

    return v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 6113
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->g:Ljava/util/List;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 7042
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/bc;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 6120
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 7256
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    if-nez v0, :cond_0

    move v0, v1

    .line 7287
    :goto_0
    return v0

    .line 7261
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 7262
    goto :goto_0

    .line 7265
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    .line 7266
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 7267
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 7268
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 7270
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 7272
    goto :goto_0

    :cond_3
    move v0, v1

    .line 7277
    goto :goto_0

    .line 7280
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 7282
    goto :goto_0

    :cond_5
    move v0, v2

    .line 7287
    goto :goto_0
.end method

.method public final f()Ljava/util/List;
    .locals 1

    .prologue
    .line 6128
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->h:Ljava/util/List;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6140
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 6147
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 7243
    const/4 v0, 0x0

    .line 7244
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->u:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 7245
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 7246
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 7247
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 7249
    goto :goto_0

    .line 7250
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 6155
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->j:Z

    return v0
.end method

.method public final m()Ljava/util/List;
    .locals 1

    .prologue
    .line 6171
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->k:Ljava/util/List;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 6201
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->m:Z

    return v0
.end method

.method public final o()Ljava/util/List;
    .locals 1

    .prologue
    .line 6216
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n:Ljava/util/List;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6248
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6264
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/util/List;
    .locals 1

    .prologue
    .line 6284
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->r:Ljava/util/List;

    return-object v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 6291
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->a:Ljava/util/Set;

    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 7047
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/bc;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/bc;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Landroid/os/Parcel;I)V

    .line 7048
    return-void
.end method
