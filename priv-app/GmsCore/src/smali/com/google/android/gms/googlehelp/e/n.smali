.class public final Lcom/google/android/gms/googlehelp/e/n;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/googlehelp/e/n;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1118
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1119
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/e/n;->cachedSize:I

    .line 1120
    return-void
.end method

.method public static a()[Lcom/google/android/gms/googlehelp/e/n;
    .locals 2

    .prologue
    .line 1101
    sget-object v0, Lcom/google/android/gms/googlehelp/e/n;->c:[Lcom/google/android/gms/googlehelp/e/n;

    if-nez v0, :cond_1

    .line 1102
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1104
    :try_start_0
    sget-object v0, Lcom/google/android/gms/googlehelp/e/n;->c:[Lcom/google/android/gms/googlehelp/e/n;

    if-nez v0, :cond_0

    .line 1105
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/googlehelp/e/n;

    sput-object v0, Lcom/google/android/gms/googlehelp/e/n;->c:[Lcom/google/android/gms/googlehelp/e/n;

    .line 1107
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1109
    :cond_1
    sget-object v0, Lcom/google/android/gms/googlehelp/e/n;->c:[Lcom/google/android/gms/googlehelp/e/n;

    return-object v0

    .line 1107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1179
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1180
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1181
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1184
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1185
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1188
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1131
    if-ne p1, p0, :cond_1

    .line 1152
    :cond_0
    :goto_0
    return v0

    .line 1134
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/googlehelp/e/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 1135
    goto :goto_0

    .line 1137
    :cond_2
    check-cast p1, Lcom/google/android/gms/googlehelp/e/n;

    .line 1138
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1139
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1140
    goto :goto_0

    .line 1142
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1143
    goto :goto_0

    .line 1145
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1146
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1147
    goto :goto_0

    .line 1149
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1150
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1157
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1160
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1162
    return v0

    .line 1157
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1160
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1095
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1169
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/n;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1171
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1172
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1174
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1175
    return-void
.end method
