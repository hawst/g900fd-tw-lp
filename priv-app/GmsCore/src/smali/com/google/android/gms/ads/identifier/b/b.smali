.class public final Lcom/google/android/gms/ads/identifier/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/ads/identifier/b/b;


# instance fields
.field private final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/ads/identifier/b/b;

    invoke-direct {v0}, Lcom/google/android/gms/ads/identifier/b/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/identifier/b/b;->a:Lcom/google/android/gms/ads/identifier/b/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/b/b;->b:Ljava/lang/Object;

    .line 49
    return-void
.end method

.method public static a()Lcom/google/android/gms/ads/identifier/b/b;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/gms/ads/identifier/b/b;->a:Lcom/google/android/gms/ads/identifier/b/b;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 88
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "adid_key"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 89
    return-void
.end method

.method private static g()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 52
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "adid_settings"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 56
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "adid_settings"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/app/GmsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 143
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/b/b;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 144
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "enable_limit_ad_tracking"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 146
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(I)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 110
    iget-object v2, p0, Lcom/google/android/gms/ads/identifier/b/b;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 113
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/identifier/b/b;->f()I

    move-result v3

    .line 114
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 115
    if-ne v3, v1, :cond_0

    move v0, v1

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :goto_0
    return v0

    .line 121
    :cond_1
    if-eq p1, v5, :cond_2

    if-eq p1, v1, :cond_2

    .line 124
    monitor-exit v2

    goto :goto_0

    .line 126
    :cond_2
    :try_start_1
    new-instance v3, Lcom/google/android/gms/ads/settings/c/b;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/ads/settings/c/b;-><init>(Landroid/content/Context;)V

    .line 127
    invoke-virtual {v3}, Lcom/google/android/gms/ads/settings/c/b;->a()V

    .line 128
    invoke-virtual {v3, p1}, Lcom/google/android/gms/ads/settings/c/b;->b(I)V

    .line 129
    invoke-virtual {v3}, Lcom/google/android/gms/ads/settings/c/b;->b()V

    .line 133
    if-ne p1, v5, :cond_3

    move v0, v1

    .line 136
    :cond_3
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "enable_limit_ad_tracking"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 138
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 64
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/b/b;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 65
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 66
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 152
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/b/b;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 153
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "enable_limit_ad_tracking"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "enable_limit_ad_tracking"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 157
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 70
    const-string v0, ""

    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/b/b;->a(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 74
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "adid_key"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    :goto_0
    return-object v0

    .line 78
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/b/b;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 92
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/b/b;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 93
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "enable_limit_ad_tracking"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()I
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/b/b;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "enable_limit_ad_tracking"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 100
    const/4 v0, -0x1

    monitor-exit v1

    .line 105
    :goto_0
    return v0

    .line 102
    :cond_0
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->g()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "enable_limit_ad_tracking"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 105
    if-eqz v2, :cond_1

    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 105
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
