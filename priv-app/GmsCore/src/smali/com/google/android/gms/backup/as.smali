.class final Lcom/google/android/gms/backup/as;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:[Ljava/lang/String;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 192
    const v0, 0x1090003

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 193
    iput-object p2, p0, Lcom/google/android/gms/backup/as;->b:[Ljava/lang/String;

    .line 194
    if-eqz p2, :cond_0

    array-length v0, p2

    :goto_0
    iput v0, p0, Lcom/google/android/gms/backup/as;->e:I

    .line 195
    iput-object p3, p0, Lcom/google/android/gms/backup/as;->c:Landroid/graphics/drawable/Drawable;

    .line 196
    iput-object p4, p0, Lcom/google/android/gms/backup/as;->d:Landroid/graphics/drawable/Drawable;

    .line 197
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/backup/as;->a:Landroid/view/LayoutInflater;

    .line 199
    return-void

    .line 194
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 205
    if-nez p2, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/backup/as;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 207
    new-instance v1, Lcom/google/android/gms/backup/at;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/android/gms/backup/at;-><init>(B)V

    .line 208
    sget v0, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/backup/at;->b:Landroid/widget/TextView;

    .line 209
    sget v0, Lcom/google/android/gms/j;->jV:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/backup/at;->a:Landroid/widget/ImageView;

    .line 210
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 215
    :goto_0
    iget-object v1, v0, Lcom/google/android/gms/backup/at;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/backup/as;->b:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget v1, p0, Lcom/google/android/gms/backup/as;->e:I

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_1

    .line 217
    iget-object v0, v0, Lcom/google/android/gms/backup/at;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/backup/as;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 222
    :goto_1
    return-object p2

    .line 212
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/at;

    goto :goto_0

    .line 219
    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/backup/at;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/backup/as;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
