.class public final Lcom/google/android/gms/fitness/l/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/fitness/l/z;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/l/z;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/fitness/l/f;->a:Lcom/google/android/gms/fitness/l/z;

    .line 23
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 33
    :try_start_0
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/f;->a:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/l/z;->o_()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 35
    iget-object v3, p0, Lcom/google/android/gms/fitness/l/f;->a:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v3, v0}, Lcom/google/android/gms/fitness/l/z;->b(Lcom/google/android/gms/fitness/data/DataSource;)I

    move-result v3

    .line 36
    if-le v3, v1, :cond_0

    .line 37
    iget-object v3, p0, Lcom/google/android/gms/fitness/l/f;->a:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v3, v0, v1}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSource;I)J

    move-result-wide v4

    .line 38
    iget-object v3, p0, Lcom/google/android/gms/fitness/l/f;->a:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v3, v0, v4, v5}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSource;J)I

    move-result v3

    .line 39
    const-string v4, "Deleted %d old data points from %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v0, v5, v3

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    const-string v1, "Transient error trimming data store, skipping this round.."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 49
    :goto_1
    return-void

    .line 43
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 44
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/f;->a:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/fitness/l/z;->b(J)I

    move-result v0

    .line 45
    const-string v1, "deleted %d changelogs"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
