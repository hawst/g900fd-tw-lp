.class final Lcom/google/android/gms/ads/internal/i/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/g/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/util/a/a;

.field final synthetic b:Lcom/google/android/gms/ads/internal/i/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/i/a;Lcom/google/android/gms/ads/internal/util/a/a;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/i/d;->a:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/i/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 94
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->a:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/util/a/a;->b()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->a:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/util/a/a;->b()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 96
    :cond_0
    monitor-exit v1

    .line 112
    :goto_0
    return-void

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->a:Lcom/google/android/gms/ads/internal/util/a/a;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/i/a;->d:Lcom/google/android/gms/ads/internal/a/p;

    invoke-interface {v0, v2}, Lcom/google/android/gms/ads/internal/util/a/a;->a(Ljava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->a:Lcom/google/android/gms/ads/internal/util/a/a;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/i/a;->b:Lcom/google/android/gms/ads/internal/util/a/e;

    new-instance v3, Lcom/google/android/gms/ads/internal/i/e;

    invoke-direct {v3, p0}, Lcom/google/android/gms/ads/internal/i/e;-><init>(Lcom/google/android/gms/ads/internal/i/d;)V

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/ads/internal/util/a/a;->a(Lcom/google/android/gms/ads/internal/util/a/e;Lcom/google/android/gms/ads/internal/util/a/b;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    const/4 v2, 0x0

    iput v2, v0, Lcom/google/android/gms/ads/internal/i/a;->e:I

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->a:Lcom/google/android/gms/ads/internal/util/a/a;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    if-eq v0, v2, :cond_2

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/ads/internal/i/a;->c(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/i/d;->a:Lcom/google/android/gms/ads/internal/util/a/a;

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/i/d;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/i/a;->c:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/ads/internal/i/a;->b(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 112
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
