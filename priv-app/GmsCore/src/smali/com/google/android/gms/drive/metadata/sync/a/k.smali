.class public final Lcom/google/android/gms/drive/metadata/sync/a/k;
.super Lcom/google/android/gms/drive/metadata/sync/a/c;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->c:Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/metadata/sync/a/c;-><init>(Lcom/google/android/gms/drive/metadata/sync/a/e;)V

    .line 16
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/k;->a:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/k;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 21
    if-ne p0, p1, :cond_0

    .line 22
    const/4 v0, 0x1

    .line 29
    :goto_0
    return v0

    .line 24
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/drive/metadata/sync/a/k;

    if-nez v0, :cond_1

    .line 25
    const/4 v0, 0x0

    goto :goto_0

    .line 28
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/metadata/sync/a/k;

    .line 29
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/k;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/drive/metadata/sync/a/k;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/k;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
