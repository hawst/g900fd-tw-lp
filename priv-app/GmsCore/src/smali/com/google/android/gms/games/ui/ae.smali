.class public abstract Lcom/google/android/gms/games/ui/ae;
.super Landroid/support/v7/widget/cs;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/br;


# instance fields
.field protected final l:Landroid/content/Context;

.field protected m:Lcom/google/android/gms/games/ui/ac;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 12

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const/4 v8, 0x0

    .line 211
    invoke-direct {p0, p1}, Landroid/support/v7/widget/cs;-><init>(Landroid/view/View;)V

    .line 212
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/ae;->l:Landroid/content/Context;

    .line 214
    instance-of v0, p1, Landroid/support/v7/widget/CardView;

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p1

    .line 225
    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->c()F

    move-result v1

    move-object v0, p1

    .line 226
    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->b()F

    move-result v0

    .line 231
    float-to-double v2, v1

    invoke-static {}, Lcom/google/android/gms/games/ui/ac;->v()D

    move-result-wide v4

    sub-double v4, v10, v4

    float-to-double v6, v0

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 232
    float-to-double v4, v1

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v4, v6

    invoke-static {}, Lcom/google/android/gms/games/ui/ac;->v()D

    move-result-wide v6

    sub-double v6, v10, v6

    float-to-double v0, v0

    mul-double/2addr v0, v6

    add-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v1, v0

    .line 235
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ao;

    .line 239
    iget v3, v0, Landroid/support/v7/widget/ao;->leftMargin:I

    sub-int/2addr v3, v2

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, v0, Landroid/support/v7/widget/ao;->leftMargin:I

    .line 240
    iget v3, v0, Landroid/support/v7/widget/ao;->rightMargin:I

    sub-int v2, v3, v2

    invoke-static {v2, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/ao;->rightMargin:I

    .line 241
    iget v2, v0, Landroid/support/v7/widget/ao;->topMargin:I

    sub-int/2addr v2, v1

    invoke-static {v2, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/ao;->topMargin:I

    .line 242
    iget v2, v0, Landroid/support/v7/widget/ao;->bottomMargin:I

    sub-int v1, v2, v1

    invoke-static {v1, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/ao;->bottomMargin:I

    .line 245
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 247
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/ui/ae;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;ILcom/google/android/gms/common/images/g;)V

    .line 291
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;ILcom/google/android/gms/common/images/g;)V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ae;->m:Lcom/google/android/gms/games/ui/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/ae;->m:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 310
    :cond_0
    if-eqz p4, :cond_1

    .line 311
    invoke-virtual {p1, p4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Lcom/google/android/gms/common/images/g;)V

    .line 313
    :cond_1
    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 317
    :goto_0
    return-void

    .line 315
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/games/ui/ac;I)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/google/android/gms/games/ui/ae;->m:Lcom/google/android/gms/games/ui/ac;

    .line 268
    return-void
.end method

.method public final q()Lcom/google/android/gms/games/ui/ac;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/gms/games/ui/ae;->m:Lcom/google/android/gms/games/ui/ac;

    return-object v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x1

    return v0
.end method

.method public final s()I
    .locals 2

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ae;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/ae;->m:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/ac;->t()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method
