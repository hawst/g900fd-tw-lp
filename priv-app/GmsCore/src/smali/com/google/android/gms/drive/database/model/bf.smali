.class final Lcom/google/android/gms/drive/database/model/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/database/a/c;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/r;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/r;)V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bf;->a:Lcom/google/android/gms/drive/database/r;

    .line 116
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/gms/drive/database/model/bh;->a:Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bh;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->c(Landroid/database/Cursor;)J

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/drive/database/model/bh;->b:Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bh;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/drive/database/model/bh;->c:Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bh;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->c(Landroid/database/Cursor;)J

    move-result-wide v0

    long-to-int v6, v0

    sget-object v0, Lcom/google/android/gms/drive/database/model/bh;->d:Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bh;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    const/4 v5, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v5

    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/database/model/be;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/be;-><init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;I)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->a()Lcom/google/android/gms/drive/database/model/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bg;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/be;->d(J)V

    return-object v0
.end method
