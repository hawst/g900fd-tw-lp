.class public Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/f/n;
.implements Lcom/google/android/gms/googlehelp/helpactivities/n;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private d:Lcom/google/android/gms/googlehelp/common/a;

.field private e:Ljava/util/List;

.field private f:Lcom/google/android/gms/googlehelp/d/b;

.field private g:Lcom/google/android/gms/googlehelp/search/a;

.field private h:Lcom/google/android/gms/googlehelp/d/d;

.field private i:Ljava/util/List;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Lcom/google/android/gms/googlehelp/fragments/h;

.field private m:Lcom/google/android/gms/googlehelp/fragments/l;

.field private n:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

.field private o:Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;

.field private p:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

.field private q:Lcom/google/android/gms/googlehelp/c/a/d;

.field private r:Landroid/util/SparseArray;

.field private s:Ljava/util/Stack;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;-><init>()V

    .line 113
    iput-object p0, v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 114
    return-object v0
.end method

.method private a(II)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 451
    packed-switch p1, :pswitch_data_0

    .line 469
    :goto_0
    return-void

    .line 453
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    if-nez p2, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/fragments/h;->g:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->b()V

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->c:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 457
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->m:Lcom/google/android/gms/googlehelp/fragments/l;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/l;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    .line 461
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->n:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    if-nez p2, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 465
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->o:Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;

    if-nez p2, :cond_2

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->a(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    .line 451
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)Lcom/google/android/gms/googlehelp/common/a;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d:Lcom/google/android/gms/googlehelp/common/a;

    return-object v0
.end method

.method private c(I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_7

    .line 428
    :goto_1
    return-void

    .line 422
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    if-ne p1, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    if-eq p1, v1, :cond_2

    if-ne p1, v4, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :pswitch_1
    if-eq p1, v4, :cond_4

    if-ne p1, v3, :cond_5

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0

    :pswitch_2
    if-ne p1, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_0

    .line 426
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(II)V

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(II)V

    goto :goto_1

    .line 422
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private l()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 705
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->S()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    .line 707
    :goto_0
    iget-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 709
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/feedback/ErrorReport;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 715
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->I()Lcom/google/android/gms/feedback/ThemeSettings;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    .line 716
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 719
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->D()Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v2

    .line 720
    if-eqz v2, :cond_1

    .line 721
    const-string v3, "com.android.feedback.SCREENSHOT_EXTRA"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 726
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 727
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    .line 729
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->C()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->C()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 731
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->C()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    .line 733
    :cond_3
    const-string v2, "com.android.feedback.SAFEPARCELABLE_REPORT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 734
    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->startActivity(Landroid/content/Intent;)V

    .line 735
    return-void

    .line 705
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->S()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 640
    packed-switch p1, :pswitch_data_0

    .line 665
    :goto_0
    :pswitch_0
    return-void

    .line 642
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->q:Lcom/google/android/gms/googlehelp/c/a/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->a(Z)V

    goto :goto_0

    .line 646
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/googlehelp/helpactivities/ClickToCallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXTRA_HELP_CONFIG"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 650
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/googlehelp/helpactivities/EmailActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "EXTRA_HELP_CONFIG"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->b()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 654
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->t()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v2, :cond_0

    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/helpactivities/l;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/helpactivities/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/n;)Lcom/google/android/gms/googlehelp/helpactivities/l;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->t()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/l;->a(Ljava/util/List;)Lcom/google/android/gms/googlehelp/helpactivities/l;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "pick_support_phone_number_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/helpactivities/l;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 658
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->q:Lcom/google/android/gms/googlehelp/c/a/d;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/c/a/d;->a(Z)V

    goto :goto_0

    .line 662
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l()V

    goto :goto_0

    .line 640
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 3

    .prologue
    .line 774
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d:Lcom/google/android/gms/googlehelp/common/a;

    .line 775
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/b;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-interface {v0, v2}, Lcom/google/android/gms/googlehelp/common/b;->a(Lcom/google/android/gms/googlehelp/common/a;)V

    goto :goto_0

    .line 776
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/b;)V
    .locals 1

    .prologue
    .line 787
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e:Ljava/util/List;

    if-nez v0, :cond_0

    .line 788
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e:Ljava/util/List;

    .line 790
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 791
    return-void
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/k;Lcom/google/android/gms/googlehelp/common/k;ZLjava/lang/String;ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 481
    if-nez p2, :cond_2

    .line 482
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->p:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->dismiss()V

    .line 483
    if-eqz p3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 484
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/q;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    .line 490
    :goto_0
    if-eqz p3, :cond_0

    .line 491
    const-string v0, "FETCH_FROM_NETWORK_FAILED"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1, p5, p6}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 511
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->supportInvalidateOptionsMenu()V

    .line 512
    return-void

    .line 486
    :cond_1
    sget v0, Lcom/google/android/gms/p;->oJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 487
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 496
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->n:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b()V

    .line 497
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c(I)V

    .line 498
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->n:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    move-object v1, p2

    move-object v2, p4

    move v3, p5

    move-object v4, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a(Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/lang/String;Z)V

    .line 500
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->p:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->dismiss()V

    .line 503
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BROWSE_ALL_ARTICLES_CLICKED"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/k;->a(Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->d:Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a(Lcom/google/android/gms/googlehelp/common/k;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/t;Lcom/google/android/gms/googlehelp/common/t;ZZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 524
    if-eqz p3, :cond_4

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->m:Lcom/google/android/gms/googlehelp/fragments/l;

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/fragments/l;->d:Lcom/google/android/gms/googlehelp/fragments/i;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/googlehelp/fragments/i;->a(Lcom/google/android/gms/googlehelp/common/t;)V

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/fragments/l;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v2, "SHOWN_SEARCH_RESULTS"

    iget-object v3, v0, Lcom/google/android/gms/googlehelp/fragments/l;->b:Lcom/google/android/gms/googlehelp/search/c;

    if-eqz v3, :cond_3

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/l;->b:Lcom/google/android/gms/googlehelp/search/c;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, p1, v0}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/t;Ljava/lang/String;)V

    .line 526
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c(I)V

    .line 540
    :cond_0
    :goto_1
    if-eqz p4, :cond_1

    .line 541
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->p:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->dismiss()V

    .line 544
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/t;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 545
    new-instance v0, Lcom/google/android/gms/googlehelp/f/q;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->f:Lcom/google/android/gms/googlehelp/d/b;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/googlehelp/f/q;-><init>(Lcom/google/android/gms/googlehelp/d/b;Lcom/google/android/gms/googlehelp/common/t;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/q;->a([Ljava/lang/Object;)V

    .line 547
    new-instance v0, Lcom/google/android/gms/googlehelp/f/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/googlehelp/f/i;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/t;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/i;->a([Ljava/lang/Object;)V

    .line 549
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->supportInvalidateOptionsMenu()V

    .line 550
    return-void

    .line 525
    :cond_3
    const-string v0, ""

    goto :goto_0

    .line 531
    :cond_4
    if-nez p4, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 533
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/fragments/h;->a(Lcom/google/android/gms/googlehelp/common/t;)V

    .line 534
    if-eqz p2, :cond_0

    .line 535
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/googlehelp/fragments/h;->b(Lcom/google/android/gms/googlehelp/common/t;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/googlehelp/f/a;)V
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->p:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a(Lcom/google/android/gms/googlehelp/f/a;)V

    .line 473
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 553
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 554
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->m:Lcom/google/android/gms/googlehelp/fragments/l;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/googlehelp/fragments/l;->a(Ljava/lang/CharSequence;Z)V

    .line 558
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/fragments/h;->b:Lcom/google/android/gms/googlehelp/search/c;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->b:Lcom/google/android/gms/googlehelp/search/c;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/googlehelp/search/c;->a(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 608
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 609
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 610
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/f/a;

    .line 611
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/f/a;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 612
    :cond_1
    invoke-virtual {v0, v5}, Lcom/google/android/gms/googlehelp/f/a;->a(Z)Z

    .line 613
    const-string v2, "GOOGLEHELP_HelpFragment"

    const-string v3, "Remove %s from the list."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 617
    :cond_2
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 384
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->p:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 414
    :goto_0
    return v0

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 411
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v3, 0x8

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(II)V

    .line 412
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(II)V

    .line 413
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->supportInvalidateOptionsMenu()V

    move v0, v1

    .line 414
    goto :goto_0

    :pswitch_0
    move v0, v2

    .line 390
    goto :goto_0

    .line 393
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->m:Lcom/google/android/gms/googlehelp/fragments/l;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/l;->d:Lcom/google/android/gms/googlehelp/fragments/i;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/i;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 394
    goto :goto_0

    .line 399
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->n:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 400
    goto :goto_0

    .line 405
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->P()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 406
    goto :goto_0

    .line 388
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 761
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 770
    :goto_0
    :pswitch_0
    return-void

    .line 763
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->g:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(IZ)V

    goto :goto_0

    .line 767
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->o:Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->a()Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a(IZ)V

    goto :goto_0

    .line 761
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lcom/google/android/gms/googlehelp/f/a;)V
    .locals 1

    .prologue
    .line 515
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/f/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->p:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a(Lcom/google/android/gms/googlehelp/f/a;)V

    .line 518
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 561
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->m:Lcom/google/android/gms/googlehelp/fragments/l;

    invoke-virtual {v0, p1, v6}, Lcom/google/android/gms/googlehelp/fragments/l;->a(Ljava/lang/CharSequence;Z)V

    .line 566
    :cond_0
    invoke-virtual {p0, v5}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Z)V

    .line 568
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 569
    new-instance v0, Lcom/google/android/gms/googlehelp/f/g;

    move-object v1, p0

    move-object v2, p1

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/f/g;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/util/Map;Lcom/google/android/gms/googlehelp/common/t;Z)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/g;->a([Ljava/lang/Object;)V

    .line 577
    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/googlehelp/d/d;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->h:Lcom/google/android/gms/googlehelp/d/d;

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/googlehelp/f/a;)V
    .locals 4

    .prologue
    .line 597
    const-string v0, "GOOGLEHELP_HelpFragment"

    const-string v1, "Add %s to the list."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 599
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 695
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 696
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tel:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 697
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->startActivity(Landroid/content/Intent;)V

    .line 698
    return-void
.end method

.method public final d()Lcom/google/android/gms/googlehelp/d/b;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->f:Lcom/google/android/gms/googlehelp/d/b;

    return-object v0
.end method

.method public final d(Lcom/google/android/gms/googlehelp/f/a;)V
    .locals 4

    .prologue
    .line 603
    const-string v0, "GOOGLEHELP_HelpFragment"

    const-string v1, "Remove %s from the list."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 605
    return-void
.end method

.method public final e()Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/googlehelp/search/a;
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g:Lcom/google/android/gms/googlehelp/search/a;

    return-object v0
.end method

.method public final g()Landroid/content/Context;
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->f:Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->a()V

    .line 739
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 743
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->supportInvalidateOptionsMenu()V

    .line 744
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->g:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a()V

    .line 745
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->o:Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->a()Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a()V

    .line 746
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 758
    :goto_0
    :pswitch_0
    return-void

    .line 751
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->g:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->b()V

    goto :goto_0

    .line 755
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->o:Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->a()Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->b()V

    goto :goto_0

    .line 749
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final k()Lcom/google/android/gms/googlehelp/common/a;
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d:Lcom/google/android/gms/googlehelp/common/a;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 140
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 143
    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    .line 146
    check-cast v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->f()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/googlehelp/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Lcom/google/ad/a/a/u;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Landroid/content/Context;Lcom/google/ad/a/a/u;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/helpactivities/k;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/common/b;)V

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    new-instance v3, Lcom/google/android/gms/googlehelp/common/g;

    invoke-direct {v3, v1, v2}, Lcom/google/android/gms/googlehelp/common/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/g;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Ljava/util/Map;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 155
    invoke-virtual {p0, v5}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->setHasOptionsMenu(Z)V

    .line 157
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/c;->l:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b:Z

    .line 161
    new-instance v0, Lcom/google/android/gms/googlehelp/d/b;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/googlehelp/d/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->f:Lcom/google/android/gms/googlehelp/d/b;

    .line 162
    new-instance v0, Lcom/google/android/gms/googlehelp/search/a;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/googlehelp/search/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g:Lcom/google/android/gms/googlehelp/search/a;

    .line 163
    new-instance v0, Lcom/google/android/gms/googlehelp/d/d;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/googlehelp/d/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->h:Lcom/google/android/gms/googlehelp/d/d;

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->i:Ljava/util/List;

    .line 166
    new-instance v0, Lcom/google/android/gms/googlehelp/fragments/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/fragments/h;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    .line 167
    new-instance v0, Lcom/google/android/gms/googlehelp/fragments/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/fragments/l;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->m:Lcom/google/android/gms/googlehelp/fragments/l;

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 170
    sget v0, Lcom/google/android/gms/j;->ij:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->n:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    .line 172
    sget v0, Lcom/google/android/gms/j;->iV:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->o:Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;

    .line 174
    sget v0, Lcom/google/android/gms/j;->iq:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->p:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    .line 177
    new-instance v0, Lcom/google/android/gms/googlehelp/c/a/d;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->q:Lcom/google/android/gms/googlehelp/c/a/d;

    .line 178
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->q:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->j:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->ib:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No hangout status section found!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->ii:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->e:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->id:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->d:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->ih:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->f:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->if:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->g:Landroid/widget/TextView;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->ie:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->j:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->ig:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->h:Landroid/view/View;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->c:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->ic:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->i:Landroid/widget/TextView;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->dD:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/googlehelp/c/a/d;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/q;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->m:Landroid/app/NotificationManager;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/c/a/d;->j(Z)Landroid/content/BroadcastReceiver;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v5}, Lcom/google/android/gms/googlehelp/c/a/d;->j(Z)Landroid/content/BroadcastReceiver;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/c/a/d;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/c/a/d;->a()Z

    .line 179
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 120
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    .line 121
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 126
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 275
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 276
    sget v0, Lcom/google/android/gms/m;->w:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 278
    new-instance v0, Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->H()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->r:Landroid/util/SparseArray;

    .line 280
    const/4 v0, 0x1

    .line 281
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->H()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;

    .line 282
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v5, v1, v5, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 283
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->r:Landroid/util/SparseArray;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v4, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v1, v2

    .line 284
    goto :goto_0

    .line 285
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 132
    sget v0, Lcom/google/android/gms/l;->ch:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 133
    sget v1, Lcom/google/android/gms/j;->in:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->j:Landroid/view/View;

    .line 134
    sget v1, Lcom/google/android/gms/j;->il:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k:Landroid/view/View;

    .line 135
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 581
    const-string v0, "GOOGLEHELP_HelpFragment"

    const-string v1, "Cancel all tasks before onDestroy is called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Z)V

    .line 583
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->f:Lcom/google/android/gms/googlehelp/d/b;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->f:Lcom/google/android/gms/googlehelp/d/b;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->h:Lcom/google/android/gms/googlehelp/d/d;

    if-eqz v0, :cond_1

    .line 587
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->h:Lcom/google/android/gms/googlehelp/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    .line 589
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g:Lcom/google/android/gms/googlehelp/search/a;

    if-eqz v0, :cond_2

    .line 590
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->f:Lcom/google/android/gms/googlehelp/d/b;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    .line 592
    :cond_2
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 593
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 290
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 291
    sget v3, Lcom/google/android/gms/j;->is:I

    if-ne v0, v3, :cond_1

    .line 292
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l()V

    .line 316
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 293
    :cond_1
    sget v3, Lcom/google/android/gms/j;->it:I

    if-ne v0, v3, :cond_2

    .line 294
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c(I)V

    goto :goto_0

    .line 295
    :cond_2
    sget v3, Lcom/google/android/gms/j;->iu:I

    if-ne v0, v3, :cond_3

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/google/android/gms/p;->pd:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/google/android/gms/p;->pe:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 297
    :cond_3
    sget v3, Lcom/google/android/gms/j;->iv:I

    if-ne v0, v3, :cond_5

    .line 298
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/fragments/h;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/fragments/h;->d:Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a()Lcom/google/android/gms/googlehelp/common/t;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->e:Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a()Lcom/google/android/gms/googlehelp/common/t;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1, v0}, Lcom/google/android/gms/googlehelp/fragments/o;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/t;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v5, v0}, Lcom/google/android/gms/googlehelp/fragments/o;->a(Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/t;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v2, :cond_4

    invoke-static {v1, v2}, Lcom/google/android/gms/googlehelp/fragments/o;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/t;)Ljava/lang/String;

    move-result-object v0

    const-string v5, " & "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0, v2}, Lcom/google/android/gms/googlehelp/fragments/o;->a(Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/t;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/googlehelp/fragments/o;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->m:Lcom/google/android/gms/googlehelp/fragments/l;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/l;->a()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->n:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->d()V

    goto/16 :goto_0

    .line 299
    :cond_5
    sget v3, Lcom/google/android/gms/j;->ir:I

    if-ne v0, v3, :cond_6

    .line 300
    new-instance v0, Lcom/google/android/gms/googlehelp/f/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/f/b;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/b;->a([Ljava/lang/Object;)V

    .line 301
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/e;->b(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    .line 303
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/fragments/h;->b(Lcom/google/android/gms/googlehelp/common/t;)V

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/fragments/h;->e:Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->b()V

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->f:Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;->b()V

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->m:Lcom/google/android/gms/googlehelp/fragments/l;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/l;->d:Lcom/google/android/gms/googlehelp/fragments/i;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/i;->c()V

    goto/16 :goto_0

    .line 305
    :cond_6
    sget v3, Lcom/google/android/gms/j;->iw:I

    if-ne v0, v3, :cond_7

    .line 306
    new-instance v0, Lcom/google/android/gms/googlehelp/helpactivities/s;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/helpactivities/s;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/s;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/helpactivities/s;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "version_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/helpactivities/s;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 310
    :cond_7
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->r:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;

    .line 311
    if-eqz v0, :cond_0

    .line 312
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;->b()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v3, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_a

    move v0, v1

    :goto_1
    const-string v6, "http"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    const-string v6, "https"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    :cond_9
    :goto_2
    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-class v1, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v3, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_1

    :cond_b
    move v1, v2

    goto :goto_2

    .line 298
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 328
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->q:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-object v0, v3, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/c/a/d;->c()Z

    move-result v4

    if-nez v4, :cond_0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {v3, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->b(Z)V

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/c/a/d;->b()Z

    move-result v1

    if-nez v1, :cond_2

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {v3, v2}, Lcom/google/android/gms/googlehelp/c/a/d;->b(Z)V

    .line 330
    :cond_3
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 331
    return-void

    :cond_4
    move v0, v2

    .line 328
    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 246
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    move v1, v2

    .line 249
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 250
    if-eqz v1, :cond_4

    if-eq v0, v2, :cond_0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    :cond_0
    move v4, v2

    .line 253
    :goto_1
    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->p()I

    move-result v0

    if-eq v0, v5, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->o()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->u()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->r()I

    move-result v0

    if-ne v0, v5, :cond_5

    :cond_1
    move v0, v2

    :goto_2
    if-eqz v0, :cond_6

    move v0, v2

    .line 255
    :goto_3
    sget v1, Lcom/google/android/gms/j;->is:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v4, :cond_7

    if-nez v0, :cond_7

    move v1, v2

    :goto_4
    invoke-interface {v5, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 257
    sget v1, Lcom/google/android/gms/j;->it:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v4, :cond_8

    if-eqz v0, :cond_8

    move v0, v2

    :goto_5
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 259
    sget v0, Lcom/google/android/gms/j;->iu:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b:Z

    if-nez v0, :cond_9

    move v0, v2

    :goto_6
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 260
    sget v0, Lcom/google/android/gms/j;->iv:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v0, v4, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->s:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_2
    move v0, v3

    :goto_7
    if-eqz v0, :cond_c

    :goto_8
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 262
    return-void

    :cond_3
    move v1, v3

    .line 248
    goto/16 :goto_0

    :cond_4
    move v4, v3

    .line 250
    goto/16 :goto_1

    :cond_5
    move v0, v3

    .line 253
    goto :goto_2

    :cond_6
    move v0, v3

    goto :goto_3

    :cond_7
    move v1, v3

    .line 255
    goto :goto_4

    :cond_8
    move v0, v3

    .line 257
    goto :goto_5

    :cond_9
    move v0, v3

    .line 259
    goto :goto_6

    .line 260
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/h;->e:Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a()Lcom/google/android/gms/googlehelp/common/t;

    move-result-object v0

    if-eqz v0, :cond_a

    move v0, v2

    goto :goto_7

    :cond_a
    move v0, v3

    goto :goto_7

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->m:Lcom/google/android/gms/googlehelp/fragments/l;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/l;->d:Lcom/google/android/gms/googlehelp/fragments/i;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/i;->b()Lcom/google/android/gms/googlehelp/common/t;

    move-result-object v0

    if-eqz v0, :cond_b

    move v0, v2

    goto :goto_7

    :cond_b
    move v0, v3

    goto :goto_7

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->n:Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->c()Z

    move-result v0

    goto :goto_7

    :cond_c
    move v2, v3

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->q:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iget-object v1, v1, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->a(Lcom/google/android/gms/googlehelp/common/a;)V

    .line 323
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 324
    return-void

    .line 321
    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    new-instance v2, Lcom/google/android/gms/googlehelp/c/a/g;

    invoke-direct {v2, v0}, Lcom/google/android/gms/googlehelp/c/a/g;-><init>(Lcom/google/android/gms/googlehelp/c/a/d;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/common/b;)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 198
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->P()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->p:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->i()V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/googlehelp/f/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/f/e;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/e;->a([Ljava/lang/Object;)V

    :cond_0
    new-instance v0, Lcom/google/android/gms/googlehelp/f/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/f/d;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/d;->a([Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/googlehelp/f/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/f/h;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/h;->a([Ljava/lang/Object;)V

    .line 200
    :cond_1
    return-void

    .line 199
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->o:Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->a(Z)V

    new-instance v0, Lcom/google/android/gms/googlehelp/f/j;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->l:Lcom/google/android/gms/googlehelp/fragments/h;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/googlehelp/f/j;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Lcom/google/android/gms/googlehelp/f/k;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/j;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
