.class public final Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/circles/d;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 100
    if-eqz p1, :cond_1

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->a:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/j;->d(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    .line 104
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->setResult(I)V

    .line 112
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->finish()V

    .line 113
    return-void

    .line 106
    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->setResult(I)V

    goto :goto_0

    .line 109
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->setResult(I)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 38
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    .line 39
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 41
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->setResult(I)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->finish()V

    .line 45
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 46
    const-string v0, "com.google.android.gms.plus.circles.EXTRA_APPLICATION_ID"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 48
    const-string v2, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 49
    if-nez v2, :cond_4

    .line 53
    :goto_0
    if-nez v0, :cond_2

    .line 54
    const/16 v0, 0x50

    .line 57
    :cond_2
    new-instance v2, Lcom/google/android/gms/common/api/w;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    iput-object v1, v2, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    new-instance v4, Lcom/google/android/gms/people/ad;

    invoke-direct {v4}, Lcom/google/android/gms/people/ad;-><init>()V

    iput v0, v4, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v4}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->a:Lcom/google/android/gms/common/api/v;

    .line 64
    const-string v0, "com.google.android.gms.plus.circles.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->b:Ljava/lang/String;

    .line 65
    const-string v0, "com.google.android.gms.plus.circles.EXTRA_PAGE_ID"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->c:Ljava/lang/String;

    .line 67
    const-string v0, "com.google.android.gms.plus.circles.EXTRA_CONSENT_HTML"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    const-string v1, "com.google.android.gms.plus.circles.EXTRA_TITLE_TEXT"

    invoke-virtual {v3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 69
    const-string v2, "com.google.android.gms.plus.circles.EXTRA_BUTTON_TEXT"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v3

    const-string v4, "consentDialog"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/circles/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/circles/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "consentDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/circles/c;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 71
    :cond_3
    return-void

    :cond_4
    move-object v1, v2

    goto :goto_0
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 87
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 92
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 93
    return-void
.end method
