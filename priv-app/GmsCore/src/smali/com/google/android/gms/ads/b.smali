.class public final Lcom/google/android/gms/ads/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/ads/b;

.field public static final b:Lcom/google/android/gms/ads/b;

.field public static final c:Lcom/google/android/gms/ads/b;

.field public static final d:Lcom/google/android/gms/ads/b;

.field public static final e:Lcom/google/android/gms/ads/b;

.field public static final f:Lcom/google/android/gms/ads/b;

.field public static final g:Lcom/google/android/gms/ads/b;


# instance fields
.field private final h:I

.field private final i:I

.field private final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x140

    .line 31
    new-instance v0, Lcom/google/android/gms/ads/b;

    const/16 v1, 0x32

    const-string v2, "320x50_mb"

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/gms/ads/b;-><init>(IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/ads/b;->a:Lcom/google/android/gms/ads/b;

    .line 36
    new-instance v0, Lcom/google/android/gms/ads/b;

    const/16 v1, 0x1d4

    const/16 v2, 0x3c

    const-string v3, "468x60_as"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/ads/b;-><init>(IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/ads/b;->b:Lcom/google/android/gms/ads/b;

    .line 41
    new-instance v0, Lcom/google/android/gms/ads/b;

    const/16 v1, 0x64

    const-string v2, "320x100_as"

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/gms/ads/b;-><init>(IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/ads/b;->c:Lcom/google/android/gms/ads/b;

    .line 46
    new-instance v0, Lcom/google/android/gms/ads/b;

    const/16 v1, 0x2d8

    const/16 v2, 0x5a

    const-string v3, "728x90_as"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/ads/b;-><init>(IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/ads/b;->d:Lcom/google/android/gms/ads/b;

    .line 52
    new-instance v0, Lcom/google/android/gms/ads/b;

    const/16 v1, 0x12c

    const/16 v2, 0xfa

    const-string v3, "300x250_as"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/ads/b;-><init>(IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/ads/b;->e:Lcom/google/android/gms/ads/b;

    .line 58
    new-instance v0, Lcom/google/android/gms/ads/b;

    const/16 v1, 0xa0

    const/16 v2, 0x258

    const-string v3, "160x600_as"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/ads/b;-><init>(IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/ads/b;->f:Lcom/google/android/gms/ads/b;

    .line 61
    new-instance v0, Lcom/google/android/gms/ads/b;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const-string v3, "smart_banner"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/ads/b;-><init>(IILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/ads/b;->g:Lcom/google/android/gms/ads/b;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 78
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const-string v0, "FULL"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v0, -0x2

    if-ne p2, v0, :cond_1

    const-string v0, "AUTO"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_as"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/ads/b;-><init>(IILjava/lang/String;)V

    .line 83
    return-void

    .line 78
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private constructor <init>(IILjava/lang/String;)V
    .locals 3

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    if-gez p1, :cond_0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid width for AdSize: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    if-gez p2, :cond_1

    const/4 v0, -0x2

    if-eq p2, v0, :cond_1

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid height for AdSize: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_1
    iput p1, p0, Lcom/google/android/gms/ads/b;->h:I

    .line 96
    iput p2, p0, Lcom/google/android/gms/ads/b;->i:I

    .line 97
    iput-object p3, p0, Lcom/google/android/gms/ads/b;->j:Ljava/lang/String;

    .line 98
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/gms/ads/b;->i:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/gms/ads/b;->h:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 105
    if-ne p1, p0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/ads/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 109
    goto :goto_0

    .line 111
    :cond_2
    check-cast p1, Lcom/google/android/gms/ads/b;

    .line 112
    iget v2, p0, Lcom/google/android/gms/ads/b;->h:I

    iget v3, p1, Lcom/google/android/gms/ads/b;->h:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/gms/ads/b;->i:I

    iget v3, p1, Lcom/google/android/gms/ads/b;->i:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/ads/b;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/ads/b;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/gms/ads/b;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/ads/b;->j:Ljava/lang/String;

    return-object v0
.end method
