.class public final Lcom/google/android/gms/security/verifier/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Iterable;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/gms/security/verifier/j;

.field private c:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "package_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "version_code"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "sha256_digest"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "state"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "last_update_time"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "upload_attempts"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/security/verifier/h;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Lcom/google/android/gms/security/verifier/j;

    invoke-direct {v0, p1}, Lcom/google/android/gms/security/verifier/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/h;->b:Lcom/google/android/gms/security/verifier/j;

    .line 92
    return-void
.end method

.method private declared-synchronized a(J)Lcom/google/android/gms/security/verifier/ApkUploadEntry;
    .locals 15

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 180
    const/4 v9, 0x0

    .line 182
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "upload_queue"

    sget-object v2, Lcom/google/android/gms/security/verifier/h;->a:[Ljava/lang/String;

    const-string v3, "id > ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "id"

    const-string v8, "1"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v13

    .line 187
    :try_start_2
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 188
    if-eqz v13, :cond_0

    .line 195
    :try_start_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 197
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return-object v1

    .line 190
    :cond_1
    :try_start_4
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    .line 191
    const/4 v0, 0x0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v0, 0x1

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x2

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v0, 0x3

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    const/4 v0, 0x4

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const/4 v0, 0x5

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/4 v0, 0x6

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/4 v0, 0x7

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    new-instance v1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    invoke-direct/range {v1 .. v12}, Lcom/google/android/gms/security/verifier/ApkUploadEntry;-><init>(JLjava/lang/String;I[BJIJI)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 194
    if-eqz v13, :cond_2

    .line 195
    :try_start_5
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 197
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 194
    :catchall_1
    move-exception v0

    move-object v1, v9

    :goto_1
    if-eqz v1, :cond_3

    .line 195
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 197
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 194
    :catchall_2
    move-exception v0

    move-object v1, v13

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/security/verifier/h;J)Lcom/google/android/gms/security/verifier/ApkUploadEntry;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/security/verifier/h;->a(J)Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 98
    invoke-static {}, Lcom/google/android/gms/security/verifier/ag;->a()V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/h;->b:Lcom/google/android/gms/security/verifier/j;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/j;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    .line 100
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 103
    invoke-static {}, Lcom/google/android/gms/security/verifier/ag;->a()V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to close a closed database"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/h;->b:Lcom/google/android/gms/security/verifier/j;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/j;->close()V

    .line 111
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V
    .locals 8

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 166
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "upload_queue"

    const-string v2, "id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-wide v6, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 169
    monitor-exit p0

    return-void

    .line 168
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 164
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;J)V
    .locals 8

    .prologue
    .line 230
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 232
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 233
    const-string v1, "last_update_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 234
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "upload_queue"

    const-string v3, "id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 238
    monitor-exit p0

    return-void

    .line 237
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 230
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 281
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 283
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "upload_queue"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "count(*)"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 285
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 286
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    .line 288
    :goto_0
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return v0

    :cond_0
    move v0, v9

    .line 286
    goto :goto_0

    .line 288
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 281
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V
    .locals 8

    .prologue
    .line 202
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 205
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 206
    const-string v1, "timestamp"

    iget-wide v2, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 207
    const-string v1, "upload_attempts"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "upload_queue"

    const-string v3, "package_name = ? AND version_code = ? AND hex(sha256_digest) = ?"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->d:[B

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/google/android/gms/common/util/e;->a([BZ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 215
    if-nez v1, :cond_0

    .line 216
    const-string v1, "package_name"

    iget-object v2, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    const-string v1, "version_code"

    iget v2, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 218
    const-string v1, "sha256_digest"

    iget-object v2, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->d:[B

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 219
    const-string v1, "state"

    iget v2, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 220
    const-string v1, "last_update_time"

    iget-wide v2, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 222
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "upload_queue"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    :cond_0
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 226
    monitor-exit p0

    return-void

    .line 225
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 202
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/gms/security/verifier/ApkUploadEntry;J)V
    .locals 8

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 244
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 245
    const-string v1, "state"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 246
    const-string v1, "last_update_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 247
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "upload_queue"

    const-string v3, "id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 250
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 251
    monitor-exit p0

    return-void

    .line 250
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 242
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V
    .locals 8

    .prologue
    .line 255
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 257
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 258
    const-string v1, "upload_attempts"

    iget v2, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->h:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 259
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/h;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "upload_queue"

    const-string v3, "id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p1, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 262
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 263
    monitor-exit p0

    return-void

    .line 262
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/h;->c()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 255
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/google/android/gms/security/verifier/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/security/verifier/i;-><init>(Lcom/google/android/gms/security/verifier/h;)V

    return-object v0
.end method
