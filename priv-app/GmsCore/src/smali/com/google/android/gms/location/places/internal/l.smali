.class public final Lcom/google/android/gms/location/places/internal/l;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/location/places/internal/PlacesParams;

.field private final g:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Ljava/lang/String;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/s;)V
    .locals 6

    .prologue
    .line 67
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p6, v5, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 68
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/location/places/internal/l;->g:Ljava/util/Locale;

    .line 69
    new-instance v0, Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/l;->g:Ljava/util/Locale;

    iget-object v2, p8, Lcom/google/android/gms/location/places/s;->a:Ljava/lang/String;

    invoke-direct {v0, p3, v1, p7, v2}, Lcom/google/android/gms/location/places/internal/PlacesParams;-><init>(Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    .line 71
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 48
    invoke-static {p1}, Lcom/google/android/gms/location/places/internal/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/places/internal/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 165
    const-string v0, "callbackIntent == null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/internal/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/a;

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/location/places/internal/a;->b(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 168
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 169
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/location/places/NearbyAlertRequest;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 156
    const-string v0, "request == null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    const-string v0, "callbackIntent == null"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/internal/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/a;

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-interface {v0, p2, v1, p3}, Lcom/google/android/gms/location/places/internal/a;->a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    .line 160
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 161
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 2

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/gms/common/internal/GetServiceRequest;

    const/16 v1, 0x41

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/GetServiceRequest;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v1, v1, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v0

    .line 93
    invoke-interface {p1, p2, v0}, Lcom/google/android/gms/common/internal/bj;->b(Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/common/internal/GetServiceRequest;)V

    .line 94
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/al;Lcom/google/android/gms/location/places/PlaceFilter;)V
    .locals 2

    .prologue
    .line 146
    if-nez p2, :cond_0

    .line 147
    invoke-static {}, Lcom/google/android/gms/location/places/PlaceFilter;->h()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object p2

    .line 150
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/internal/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/a;

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-interface {v0, p2, v1, p1}, Lcom/google/android/gms/location/places/internal/a;->a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 151
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/al;Lcom/google/android/gms/location/places/UserAddedPlace;)V
    .locals 2

    .prologue
    .line 191
    const-string v0, "userAddedPlace == null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/internal/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/a;

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-interface {v0, p2, v1, p1}, Lcom/google/android/gms/location/places/internal/a;->a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 193
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/al;Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/internal/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/a;

    iget-object v4, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/location/places/internal/a;->a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 142
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/al;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/lang/String;ILcom/google/android/gms/location/places/PlaceFilter;)V
    .locals 7

    .prologue
    .line 99
    const-string v0, "bounds == null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    const-string v0, "callback == null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    if-lez p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "maxResults should be > 0"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 103
    if-nez p3, :cond_2

    .line 104
    const-string v3, ""

    .line 107
    :goto_1
    if-nez p5, :cond_1

    .line 108
    invoke-static {}, Lcom/google/android/gms/location/places/PlaceFilter;->h()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v4

    .line 111
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/internal/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/a;

    iget-object v5, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-object v1, p2

    move v2, p4

    move-object v6, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/location/places/internal/a;->a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 112
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v4, p5

    goto :goto_2

    :cond_2
    move-object v3, p3

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/location/places/al;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V
    .locals 6

    .prologue
    .line 116
    const-string v0, "query == null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const-string v0, "bounds == null"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    const-string v0, "callback == null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    if-nez p4, :cond_0

    .line 121
    invoke-static {}, Lcom/google/android/gms/location/places/AutocompleteFilter;->a()Lcom/google/android/gms/location/places/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/a;->a()Lcom/google/android/gms/location/places/AutocompleteFilter;

    move-result-object v3

    .line 124
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/internal/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/a;

    iget-object v4, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    move-object v1, p2

    move-object v2, p3

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/location/places/internal/a;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 125
    return-void

    :cond_0
    move-object v3, p4

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/location/places/al;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/gms/location/places/internal/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/a;

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/l;->a:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-interface {v0, p2, v1, p1}, Lcom/google/android/gms/location/places/internal/a;->a(Ljava/util/List;Lcom/google/android/gms/location/places/internal/PlacesParams;Lcom/google/android/gms/location/places/internal/d;)V

    .line 136
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string v0, "com.google.android.gms.location.places.PlacesApi"

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const-string v0, "com.google.android.gms.location.places.internal.IGooglePlacesService"

    return-object v0
.end method
