.class public final Lcom/google/android/gms/reminders/service/a/d;
.super Lcom/google/android/gms/reminders/service/a/a;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/reminders/model/TaskIdEntity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskIdEntity;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/gms/reminders/service/a/a;-><init>(Lcom/google/android/gms/reminders/internal/b;)V

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/reminders/service/a/d;->b:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/google/android/gms/reminders/service/a/d;->c:Lcom/google/android/gms/reminders/model/TaskIdEntity;

    .line 30
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 21
    check-cast p1, Lcom/google/android/gms/reminders/service/RemindersIntentService;

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/d;->c:Lcom/google/android/gms/reminders/model/TaskIdEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskIdEntity;->a()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "server_assigned_id=?"

    new-array v0, v5, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/reminders/service/a/d;->c:Lcom/google/android/gms/reminders/model/TaskIdEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/reminders/model/TaskIdEntity;->a()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "deleted"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "is_dirty"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2, v1, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/d;->a:Lcom/google/android/gms/reminders/internal/b;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, v6}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/api/Status;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/d;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/reminders/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/reminders/a/d;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/d;->a:Lcom/google/android/gms/reminders/internal/b;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x1770

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1

    :cond_1
    const-string v1, "client_assigned_id=? AND account_id=?"

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/reminders/service/a/d;->c:Lcom/google/android/gms/reminders/model/TaskIdEntity;

    invoke-virtual {v3}, Lcom/google/android/gms/reminders/model/TaskIdEntity;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    iget-wide v2, v2, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    goto :goto_0
.end method
