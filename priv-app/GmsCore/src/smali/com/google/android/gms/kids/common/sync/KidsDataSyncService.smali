.class public Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/kids/common/sync/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a:Lcom/google/android/gms/kids/common/sync/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Bundle;Ljava/util/List;)Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 106
    if-nez p0, :cond_0

    .line 107
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 109
    :cond_0
    if-eqz p1, :cond_1

    .line 110
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/kids/common/sync/a;

    .line 111
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 114
    :cond_1
    const-string v0, "is_uct"

    invoke-virtual {p0, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 115
    return-object p0
.end method

.method public static a(Landroid/accounts/Account;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 81
    if-nez p0, :cond_0

    .line 82
    const-string v1, "KidsDataSyncService"

    const-string v2, "No account found."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/kids/common/f;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    :goto_0
    return v0

    .line 85
    :cond_0
    const-string v2, "KidsDataSyncService"

    const-string v3, "schedulePeriodicSync: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/kids/common/f;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->f()Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a(Landroid/os/Bundle;Ljava/util/List;)Landroid/os/Bundle;

    move-result-object v2

    .line 87
    const-string v3, "com.google.android.gms.kids.common.sync"

    sget-object v0, Lcom/google/android/gms/kids/common/b/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p0, v3, v2, v4, v5}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    move v0, v1

    .line 90
    goto :goto_0
.end method

.method public static a(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/util/List;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 63
    if-nez p0, :cond_0

    .line 64
    const-string v1, "KidsDataSyncService"

    const-string v2, "No account found."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/kids/common/f;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    :goto_0
    return v0

    .line 67
    :cond_0
    const-string v2, "KidsDataSyncService"

    const-string v3, "requestImmediateSync: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/kids/common/f;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    const-string v0, "com.google.android.gms.kids.common.sync"

    .line 69
    invoke-static {p1, p2}, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a(Landroid/os/Bundle;Ljava/util/List;)Landroid/os/Bundle;

    move-result-object v2

    .line 73
    const-string v3, "expedited"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 75
    const-string v3, "force"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 76
    invoke-static {p0, v0, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    move v0, v1

    .line 77
    goto :goto_0
.end method

.method public static b(Landroid/accounts/Account;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 94
    if-nez p0, :cond_0

    .line 95
    const-string v1, "KidsDataSyncService"

    const-string v2, "No account found."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/kids/common/f;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    :goto_0
    return v0

    .line 98
    :cond_0
    const-string v2, "KidsDataSyncService"

    const-string v3, "initializeSyncAdapter: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/kids/common/f;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    const-string v0, "com.google.android.gms.kids.common.sync"

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    move v0, v1

    .line 100
    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a:Lcom/google/android/gms/kids/common/sync/b;

    invoke-virtual {v0}, Lcom/google/android/gms/kids/common/sync/b;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 40
    const-string v0, "KidsDataSyncService"

    const-string v1, "onCreate"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    monitor-enter p0

    .line 42
    :try_start_0
    sget-object v0, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a:Lcom/google/android/gms/kids/common/sync/b;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/google/android/gms/kids/common/sync/b;

    invoke-virtual {p0}, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/kids/common/sync/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a:Lcom/google/android/gms/kids/common/sync/b;

    .line 45
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
