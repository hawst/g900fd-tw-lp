.class final Lcom/google/android/gms/plus/audience/bu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/audience/bl;

.field private b:Lcom/google/android/gms/common/people/data/AudienceMember;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/audience/bl;Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 581
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/bu;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582
    iput-object p2, p0, Lcom/google/android/gms/plus/audience/bu;->b:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 583
    iput-object p3, p0, Lcom/google/android/gms/plus/audience/bu;->c:Ljava/lang/String;

    .line 584
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 575
    check-cast p1, Lcom/google/android/gms/people/n;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/people/n;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bu;->b:Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bu;->b:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bu;->b:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/people/n;->b()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bu;->a:Lcom/google/android/gms/plus/audience/bl;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bu;->b:Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/bl;->a(Lcom/google/android/gms/plus/audience/bl;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/gms/common/audience/a/h;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bu;->b:Lcom/google/android/gms/common/people/data/AudienceMember;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/bu;->c:Ljava/lang/String;

    invoke-direct {v0, v3, v1, v3, v2}, Lcom/google/android/gms/common/audience/a/h;-><init>(ILcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/audience/a/h;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/bu;->a:Lcom/google/android/gms/plus/audience/bl;

    invoke-interface {p1}, Lcom/google/android/gms/people/n;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/plus/audience/bl;->a(Lcom/google/android/gms/plus/audience/bl;Landroid/content/Intent;Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
