.class public final enum Lcom/google/android/gms/drive/database/model/ag;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum f:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum g:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum h:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum i:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum j:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum k:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum l:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum m:Lcom/google/android/gms/drive/database/model/ag;

.field public static final enum n:Lcom/google/android/gms/drive/database/model/ag;

.field private static final synthetic p:[Lcom/google/android/gms/drive/database/model/ag;


# instance fields
.field private final o:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/16 v9, 0x19

    const/4 v8, 0x1

    .line 56
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "CONTENT_ETAG"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "contentETag"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->a:Lcom/google/android/gms/drive/database/model/ag;

    .line 60
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "CONTENT_TYPE"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "contentType"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->b:Lcom/google/android/gms/drive/database/model/ag;

    .line 64
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "ENCRYPTION_KEY"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "encryptionKey"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->d:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->c:Lcom/google/android/gms/drive/database/model/ag;

    .line 68
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "ENCRYPTION_ALGORITHM"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "encryptionAlgorithm"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->d:Lcom/google/android/gms/drive/database/model/ag;

    .line 72
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "OWNED_FILE_PATH"

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "filePath"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->e:Lcom/google/android/gms/drive/database/model/ag;

    .line 80
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "NOT_OWNED_FILE_PATH"

    const/4 v2, 0x5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "notOwnedFilePath"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->f:Lcom/google/android/gms/drive/database/model/ag;

    .line 84
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "LAST_OPENED_TIME"

    const/4 v2, 0x6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "lastOpenedTime"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->g:Lcom/google/android/gms/drive/database/model/ag;

    .line 88
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "LAST_MODIFIED_TIME"

    const/4 v2, 0x7

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "lastModifiedTime"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->h:Lcom/google/android/gms/drive/database/model/ag;

    .line 97
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "SERVER_SIDE_LAST_MODIFIED_TIME"

    const/16 v2, 0x8

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "serverSideLastModifiedTime"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->i:Lcom/google/android/gms/drive/database/model/ag;

    .line 105
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "MD5_CHECKSUM"

    const/16 v2, 0x9

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "md5Checksum"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->j:Lcom/google/android/gms/drive/database/model/ag;

    .line 114
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "IS_TEMPORARY"

    const/16 v2, 0xa

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "isTemporary"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->k:Lcom/google/android/gms/drive/database/model/ag;

    .line 118
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "REFERENCED_CONTENT_ID"

    const/16 v2, 0xb

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "referencedContentId"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->a()Lcom/google/android/gms/drive/database/model/af;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/gms/drive/database/model/av;->b:Lcom/google/android/gms/drive/database/model/av;

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;Lcom/google/android/gms/drive/database/model/av;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->l:Lcom/google/android/gms/drive/database/model/ag;

    .line 132
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "IS_DIRTY"

    const/16 v2, 0xc

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "isDirty"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    iput-boolean v8, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->m:Lcom/google/android/gms/drive/database/model/ag;

    .line 140
    new-instance v0, Lcom/google/android/gms/drive/database/model/ag;

    const-string v1, "CREATOR_IDENTITY"

    const/16 v2, 0xd

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->d()Lcom/google/android/gms/drive/database/model/af;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "creatorIdentity"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    iput-boolean v8, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ag;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->n:Lcom/google/android/gms/drive/database/model/ag;

    .line 55
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/ag;

    sget-object v1, Lcom/google/android/gms/drive/database/model/ag;->a:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/ag;->b:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/ag;->c:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/gms/drive/database/model/ag;->d:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->e:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->f:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->g:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->h:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->i:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->j:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->k:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->l:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->m:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/gms/drive/database/model/ag;->n:Lcom/google/android/gms/drive/database/model/ag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/ag;->p:[Lcom/google/android/gms/drive/database/model/ag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ag;->o:Lcom/google/android/gms/drive/database/model/ab;

    .line 150
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ag;
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/google/android/gms/drive/database/model/ag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ag;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/ag;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/gms/drive/database/model/ag;->p:[Lcom/google/android/gms/drive/database/model/ag;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/ag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/ag;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ag;->o:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
