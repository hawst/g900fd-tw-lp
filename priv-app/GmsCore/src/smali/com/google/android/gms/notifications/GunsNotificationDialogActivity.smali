.class public Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "GnotsDialogFrag"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/notifications/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/notifications/h;->dismiss()V

    .line 87
    :cond_0
    const/4 v0, 0x0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 89
    const-string v2, "com.google.android.gms.notifications.intents.ICON_FILE_PATH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    const-string v0, "com.google.android.gms.notifications.intents.ICON_FILE_PATH"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 93
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 97
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->finish()V

    .line 98
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.gms.notifications.intents.accountName"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/notifications/g;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 74
    invoke-virtual {p0, v0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 76
    invoke-direct {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->c()V

    .line 77
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->c()V

    .line 81
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 29
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 32
    invoke-static {v1}, Lcom/google/android/gms/notifications/k;->a(Landroid/content/Intent;)Lcom/google/c/e/a/a/a/a/c;

    move-result-object v2

    .line 34
    if-eqz v2, :cond_1

    iget-object v0, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/f;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/f;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 35
    const-string v0, "GnotsDialogActivity"

    const-string v1, "Intent doesn\'t contain enough data to show the dialog."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->finish()V

    .line 47
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v6

    .line 34
    goto :goto_0

    .line 40
    :cond_2
    invoke-static {v2}, Lcom/google/android/gms/notifications/k;->a(Lcom/google/c/e/a/a/a/a/c;)I

    move-result v4

    .line 42
    const/4 v5, 0x0

    .line 43
    const-string v0, "com.google.android.gms.notifications.intents.ICON_FILE_PATH"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 44
    const-string v0, "com.google.android.gms.notifications.intents.ICON_FILE_PATH"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 46
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "GnotsDialogFrag"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/notifications/h;

    if-nez v0, :cond_0

    iget-object v0, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/f;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/f;->f:Ljava/lang/String;

    :goto_2
    iget-object v1, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->e:Ljava/lang/String;

    :goto_3
    iget-object v3, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v3, v3, Lcom/google/c/e/a/a/a/a/f;->g:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/f;->g:Ljava/lang/String;

    :goto_4
    const/high16 v3, 0x1040000

    invoke-virtual {p0, v3}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/notifications/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/gms/notifications/h;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/gms/notifications/h;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "GnotsDialogFrag"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_1

    :cond_4
    iget-object v0, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v0, v0, Lcom/google/c/e/a/a/a/a/f;->c:Ljava/lang/String;

    goto :goto_2

    :cond_5
    iget-object v1, v2, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->d:Ljava/lang/String;

    goto :goto_3

    :cond_6
    const v2, 0x104000a

    invoke-virtual {p0, v2}, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_4
.end method
