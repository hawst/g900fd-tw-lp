.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;

.field public final d:Ljava/util/Set;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 532
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->d:Ljava/util/Set;

    .line 533
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;
    .locals 7

    .prologue
    .line 569
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->d:Ljava/util/Set;

    iget-boolean v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a:Z

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity;-><init>(Ljava/util/Set;ZLjava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/j;
    .locals 2

    .prologue
    .line 554
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->e:Ljava/lang/String;

    .line 555
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->d:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 556
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/j;
    .locals 2

    .prologue
    .line 560
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->f:Ljava/lang/String;

    .line 561
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->d:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 562
    return-object p0
.end method
