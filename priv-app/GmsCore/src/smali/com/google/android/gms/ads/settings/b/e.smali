.class final Lcom/google/android/gms/ads/settings/b/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/settings/c/h;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x3

    .line 146
    packed-switch p2, :pswitch_data_0

    .line 183
    :pswitch_0
    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/ads/settings/c/f;->a(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 152
    :pswitch_2
    if-eqz p1, :cond_1

    .line 153
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 154
    const-string v2, "com.google.android.youtube"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 156
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/ads/settings/b/d;->a(Ljava/lang/String;)Z
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 163
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    :try_start_2
    invoke-static {v0}, Lcom/google/android/gms/ads/settings/c/f;->a(I)V

    .line 166
    if-ne p2, v4, :cond_0

    .line 167
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/i;->e()V

    .line 168
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->a()Lcom/google/android/gms/ads/identifier/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/b/b;->b()V

    .line 169
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/b;->f()V

    .line 170
    invoke-static {}, Lcom/google/android/gms/ads/settings/b/a;->a()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 187
    :catch_0
    move-exception v0

    invoke-static {v4}, Lcom/google/android/gms/ads/settings/c/f;->a(I)V

    goto :goto_0

    .line 163
    :cond_2
    const/4 v0, 0x4

    goto :goto_2

    .line 174
    :pswitch_3
    const/4 v0, 0x2

    :try_start_3
    invoke-static {v0}, Lcom/google/android/gms/ads/settings/c/f;->a(I)V

    goto :goto_0

    .line 179
    :pswitch_4
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/ads/settings/c/f;->a(I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1

    .line 162
    :catch_2
    move-exception v1

    goto :goto_1

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
