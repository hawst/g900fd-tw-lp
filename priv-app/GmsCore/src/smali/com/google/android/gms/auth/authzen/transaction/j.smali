.class final Lcom/google/android/gms/auth/authzen/transaction/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/authzen/transaction/h;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/authzen/transaction/h;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/google/android/gms/auth/authzen/transaction/j;->a:Lcom/google/android/gms/auth/authzen/transaction/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6

    .prologue
    .line 163
    invoke-static {p2}, Lcom/google/android/gms/auth/firstparty/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/auth/firstparty/a/a;

    move-result-object v0

    .line 165
    :try_start_0
    invoke-interface {v0}, Lcom/google/android/gms/auth/firstparty/a/a;->a()J

    move-result-wide v0

    .line 166
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/j;->a:Lcom/google/android/gms/auth/authzen/transaction/h;

    iget-wide v2, v2, Lcom/google/android/gms/auth/authzen/transaction/h;->c:J

    sub-long/2addr v2, v0

    .line 167
    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/j;->a:Lcom/google/android/gms/auth/authzen/transaction/h;

    iget-object v0, v0, Lcom/google/android/gms/auth/authzen/transaction/h;->a:Lcom/google/ab/b/a/a/q;

    iget-wide v0, v0, Lcom/google/ab/b/a/a/q;->e:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/j;->a:Lcom/google/android/gms/auth/authzen/transaction/h;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/authzen/transaction/h;->a(Lcom/google/android/gms/auth/authzen/transaction/h;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/j;->a:Lcom/google/android/gms/auth/authzen/transaction/h;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/authzen/transaction/h;->c()V

    .line 176
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 181
    return-void
.end method
