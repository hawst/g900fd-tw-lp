.class final Lcom/google/android/gms/backup/a;
.super Lcom/google/android/gms/backup/al;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/backup/al;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 44
    invoke-static {}, Lcom/google/android/gms/backup/BackupAccountManagerService;->a()Landroid/content/Intent;

    move-result-object v1

    .line 45
    if-nez v1, :cond_0

    .line 46
    invoke-static {}, Lcom/google/android/gms/backup/BackupAccountManagerService;->b()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    const-string v2, "Could not resolve service intent!"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 66
    :goto_0
    return-object v0

    .line 50
    :cond_0
    new-instance v2, Lcom/google/android/gms/common/b;

    invoke-direct {v2}, Lcom/google/android/gms/common/b;-><init>()V

    .line 52
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v1, v2, v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 56
    invoke-virtual {v2}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/backup/al;->a(Landroid/os/IBinder;)Lcom/google/android/gms/backup/ak;

    move-result-object v1

    .line 58
    invoke-interface {v1}, Lcom/google/android/gms/backup/ak;->a()Landroid/accounts/Account;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 62
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_0

    .line 59
    :catch_0
    move-exception v1

    .line 60
    :goto_1
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/backup/BackupAccountManagerService;->b()Lcom/google/android/gms/auth/d/a;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    throw v0

    .line 66
    :cond_1
    new-instance v0, Lcom/google/android/gms/backup/an;

    invoke-direct {v0}, Lcom/google/android/gms/backup/an;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/backup/an;->a()Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0

    .line 59
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 5

    .prologue
    .line 73
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-static {}, Lcom/google/android/gms/backup/BackupAccountManagerService;->a()Landroid/content/Intent;

    move-result-object v0

    .line 75
    if-nez v0, :cond_3

    .line 76
    invoke-static {}, Lcom/google/android/gms/backup/BackupAccountManagerService;->b()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Could not resolve service intent!"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 96
    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/gms/backup/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 97
    iget-object v1, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    .line 98
    :cond_1
    new-instance v0, Lcom/google/android/gms/backup/an;

    invoke-direct {v0}, Lcom/google/android/gms/backup/an;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/backup/an;->a(Landroid/accounts/Account;)V

    .line 100
    :cond_2
    return-void

    .line 78
    :cond_3
    new-instance v1, Lcom/google/android/gms/common/b;

    invoke-direct {v1}, Lcom/google/android/gms/common/b;-><init>()V

    .line 80
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 84
    invoke-virtual {v1}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/backup/al;->a(Landroid/os/IBinder;)Lcom/google/android/gms/backup/ak;

    move-result-object v0

    .line 87
    invoke-interface {v0, p1}, Lcom/google/android/gms/backup/ak;->a(Landroid/accounts/Account;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    :goto_1
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/backup/BackupAccountManagerService;->b()Lcom/google/android/gms/auth/d/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/backup/a;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    throw v0

    .line 88
    :catch_1
    move-exception v0

    goto :goto_1
.end method
