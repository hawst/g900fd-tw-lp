.class public final enum Lcom/google/android/gms/photos/i;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/photos/i;

.field private static final synthetic c:[Lcom/google/android/gms/photos/i;


# instance fields
.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/gms/photos/i;

    const-string v1, "Tap"

    invoke-direct {v0, v1}, Lcom/google/android/gms/photos/i;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/photos/i;->a:Lcom/google/android/gms/photos/i;

    .line 60
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/photos/i;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/photos/i;->a:Lcom/google/android/gms/photos/i;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/photos/i;->c:[Lcom/google/android/gms/photos/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 66
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/photos/i;->b:I

    .line 67
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/i;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/gms/photos/i;->b:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/photos/i;
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/google/android/gms/photos/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/i;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/photos/i;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/gms/photos/i;->c:[Lcom/google/android/gms/photos/i;

    invoke-virtual {v0}, [Lcom/google/android/gms/photos/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/photos/i;

    return-object v0
.end method
