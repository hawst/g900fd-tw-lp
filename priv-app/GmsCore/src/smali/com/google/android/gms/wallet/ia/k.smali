.class final Lcom/google/android/gms/wallet/ia/k;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 0

    .prologue
    .line 963
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    .line 1065
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 3

    .prologue
    .line 988
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 992
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 993
    const-string v1, "serverResponse"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 995
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    .line 996
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 1001
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 1002
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    .line 1009
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v0, v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v0, v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1013
    :cond_1
    const-string v0, "CreateProfileActivity"

    const-string v1, "Unexpected signup error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    .line 1060
    :goto_0
    return-void

    .line 1017
    :cond_2
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_3

    aget v4, v2, v0

    .line 1018
    packed-switch v4, :pswitch_data_0

    .line 1025
    const-string v0, "CreateProfileActivity"

    const-string v1, "Unexpected signup address error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1021
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v4, v4, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b()V

    .line 1017
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1030
    :cond_3
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_5

    aget v4, v2, v0

    .line 1031
    packed-switch v4, :pswitch_data_1

    .line 1053
    :pswitch_1
    const-string v0, "CreateProfileActivity"

    const-string v1, "Unexpected signup instrument error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1054
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1035
    :pswitch_2
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v4, v4, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v4, v4, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_4

    .line 1038
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v4, v4, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b()V

    .line 1030
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1040
    :cond_4
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v4, v4, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b()V

    goto :goto_3

    .line 1044
    :pswitch_3
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    sget v5, Lcom/google/android/gms/p;->Bo:I

    invoke-static {v4, v5}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;I)V

    .line 1045
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v4, v4, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/ui/an;->b()V

    goto :goto_3

    .line 1048
    :pswitch_4
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    sget v5, Lcom/google/android/gms/p;->Bn:I

    invoke-static {v4, v5}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;I)V

    goto :goto_3

    .line 1058
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->g()Z

    .line 1059
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Z)V

    goto/16 :goto_0

    .line 1018
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 1031
    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ag;)V
    .locals 8

    .prologue
    .line 967
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Lcom/google/checkout/inapp/proto/ag;)Lcom/google/checkout/inapp/proto/ag;

    .line 968
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 970
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d:Z

    if-nez v0, :cond_0

    .line 984
    :goto_0
    return-void

    .line 973
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/ag;->a:[Lcom/google/checkout/inapp/proto/aj;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 977
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ag;->a:[Lcom/google/checkout/inapp/proto/aj;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 978
    new-instance v5, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    iget-object v6, v4, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    iget-object v7, v4, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    iget-object v4, v4, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    invoke-direct {v5, v6, v7, v4}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 977
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 983
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    .line 1070
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Lcom/google/checkout/inapp/proto/ag;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1075
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->e(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    .line 1076
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 1080
    :goto_0
    return-void

    .line 1078
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1084
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/k;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(ILandroid/content/Intent;)V

    .line 1085
    return-void
.end method
