.class public final Lcom/google/android/gms/people/service/q;
.super Lcom/google/android/gms/people/internal/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Lcom/google/android/gms/people/service/c;

.field private final g:Lcom/google/android/gms/people/service/c;

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/service/c;Lcom/google/android/gms/people/service/c;Z)V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/android/gms/people/internal/j;-><init>()V

    .line 176
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    .line 177
    iput-object p2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    .line 178
    iput-object p3, p0, Lcom/google/android/gms/people/service/q;->c:Ljava/lang/String;

    .line 179
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/service/q;->d:I

    .line 180
    iput-object p4, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    .line 181
    iput-object p5, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    .line 182
    iput-object p6, p0, Lcom/google/android/gms/people/service/q;->g:Lcom/google/android/gms/people/service/c;

    .line 183
    iput-boolean p7, p0, Lcom/google/android/gms/people/service/q;->h:Z

    .line 184
    return-void
.end method

.method private a(Lcom/google/android/gms/people/internal/f;)Lcom/google/android/gms/people/service/d;
    .locals 2

    .prologue
    .line 217
    new-instance v0, Lcom/google/android/gms/people/service/d;

    iget-boolean v1, p0, Lcom/google/android/gms/people/service/q;->h:Z

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/people/service/d;-><init>(Lcom/google/android/gms/people/internal/f;Z)V

    return-object v0
.end method

.method private static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 197
    invoke-static {}, Lcom/google/android/gms/people/internal/at;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    .line 198
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    const-string v0, "("

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string v1, ""

    .line 201
    array-length v3, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 202
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 205
    const-string v1, ", "

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    :cond_0
    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    const-string v0, "PeopleCall"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "account"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_0
    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method private static d()V
    .locals 2

    .prologue
    .line 859
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 861
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Contacts sync not supported on this platform."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 863
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 779
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 780
    const-string v0, "syncRawContact"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 782
    :cond_0
    const-string v0, "rawContactUri"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 784
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/people/service/a/am;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/people/service/q;->d:I

    invoke-direct {v2, v3, v4, v5, p1}, Lcom/google/android/gms/people/service/a/am;-><init>(Landroid/content/Context;Ljava/lang/String;ILandroid/net/Uri;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 786
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 927
    invoke-static {v5}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 928
    const-string v2, "registerDataChangedListener"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v1

    aput-object p3, v3, v0

    aput-object p4, v3, v5

    const/4 v4, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 931
    :cond_0
    const-string v2, "callbacks"

    invoke-static {p1, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 933
    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v2

    .line 935
    if-eqz p2, :cond_2

    .line 936
    if-eqz p5, :cond_1

    :goto_0
    const-string v1, "scopes"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 938
    invoke-virtual {v2, p1, p3, p4, p5}, Lcom/google/android/gms/people/service/h;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;I)V

    .line 943
    :goto_1
    const/4 v0, 0x0

    return-object v0

    :cond_1
    move v0, v1

    .line 936
    goto :goto_0

    .line 940
    :cond_2
    invoke-virtual {v2, p1}, Lcom/google/android/gms/people/service/h;->a(Lcom/google/android/gms/people/internal/f;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 949
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 950
    const-string v0, "startSync"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 952
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 954
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/people/service/q;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x0

    .line 969
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 970
    const-string v0, "requestSyncOld19"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v6

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    .line 972
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;Ljava/lang/String;JZ)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JZ)Landroid/os/Bundle;
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v7, 0x0

    .line 978
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 979
    const-string v0, "requestSyncOld25"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v7

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    .line 981
    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;
    .locals 13

    .prologue
    .line 989
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 990
    const-string v0, "requestSync"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 993
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 995
    const-string v0, "account"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 1001
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v12, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/ah;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->c()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v5, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object v6, p1

    move-object v7, p2

    move-wide/from16 v8, p3

    move/from16 v10, p5

    move/from16 v11, p6

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/people/service/a/ah;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JZZ)V

    invoke-interface {v0, v12, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 1005
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Lcom/google/android/gms/common/data/DataHolder;IIJ)Lcom/google/android/gms/common/internal/bd;
    .locals 15

    .prologue
    .line 1105
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1106
    const-string v2, "sendAutocompleteFeedback"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1109
    :cond_0
    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v2

    move/from16 v0, p3

    if-ge v0, v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Invalid index"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1113
    new-instance v3, Lcom/google/android/gms/people/service/a/ai;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->c()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/google/android/gms/people/service/q;->d:I

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move/from16 v10, p3

    move/from16 v11, p4

    move-wide/from16 v12, p5

    invoke-direct/range {v3 .. v13}, Lcom/google/android/gms/people/service/a/ai;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Lcom/google/android/gms/common/data/DataHolder;IIJ)V

    .line 1118
    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v2, v4, v3}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 1119
    return-object v3

    .line 1110
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Lcom/google/android/gms/people/identity/internal/AccountToken;Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;)Lcom/google/android/gms/common/internal/bd;
    .locals 9

    .prologue
    const/4 v1, 0x2

    .line 1083
    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1084
    const-string v0, "identityList"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1087
    :cond_0
    new-instance v0, Lcom/google/android/gms/people/service/a/r;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/gms/people/identity/internal/AccountToken;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/android/gms/people/identity/internal/AccountToken;->c()Ljava/lang/String;

    move-result-object v7

    move-object v5, p1

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/people/service/a/r;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/identity/internal/ParcelableListOptions;)V

    .line 1098
    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 1099
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)Lcom/google/android/gms/common/internal/bd;
    .locals 7

    .prologue
    const/4 v1, 0x2

    .line 471
    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    const-string v0, "loadAvatarByReference"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 474
    :cond_0
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    const-string v0, "avatarReference"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    const-string v0, "options"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    new-instance v0, Lcom/google/android/gms/people/service/a/a/j;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;)Lcom/google/android/gms/people/service/d;

    move-result-object v4

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/service/a/a/j;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)V

    .line 482
    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->g:Lcom/google/android/gms/people/service/c;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 483
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;I)Lcom/google/android/gms/common/internal/bd;
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 489
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 490
    const-string v2, "sendInteractionFeedback"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 492
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 494
    new-instance v0, Lcom/google/android/gms/people/service/a/aj;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    sget-object v4, Lcom/google/android/gms/people/internal/at;->g:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p2}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v5

    move-object v4, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/service/a/aj;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;[Ljava/lang/String;I)V

    .line 498
    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 499
    return-object v0

    :cond_1
    move v0, v1

    .line 492
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/internal/bd;
    .locals 15

    .prologue
    .line 443
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 444
    const-string v1, "loadAutocompleteList"

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    const/4 v3, 0x4

    aput-object p6, v2, v3

    const/4 v3, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    invoke-static/range {p10 .. p10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 447
    :cond_0
    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 449
    if-nez p4, :cond_2

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Directory search not supported yet"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 450
    if-eqz p7, :cond_1

    const/4 v1, 0x1

    move/from16 v0, p7

    if-ne v0, v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    :goto_1
    const-string v2, "Unsupported autocomplete type"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 454
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    const-string v2, "Query mustn\'t be empty"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 455
    if-lez p9, :cond_5

    const/4 v1, 0x1

    :goto_3
    const-string v2, "Invalid numberOfResults"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 457
    new-instance v1, Lcom/google/android/gms/people/service/a/w;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/people/service/q;->d:I

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move/from16 v11, p7

    move/from16 v12, p8

    move/from16 v13, p9

    move/from16 v14, p10

    invoke-direct/range {v1 .. v14}, Lcom/google/android/gms/people/service/a/w;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IIIZ)V

    .line 462
    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v2, v3, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 463
    return-object v1

    .line 449
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 450
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 454
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 455
    :cond_5
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;JZ)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 555
    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    const-string v0, "loadContactImageLegacy"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 558
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/q;->b(Lcom/google/android/gms/people/internal/f;JZ)Lcom/google/android/gms/common/internal/bd;

    .line 559
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1045
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1046
    const-string v0, "loadLog"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1049
    :cond_0
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 1050
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Lcom/google/android/gms/people/identity/internal/AccountToken;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;)V
    .locals 12

    .prologue
    .line 1065
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1066
    const-string v0, "identityGetByIds"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1068
    :cond_0
    iget-object v10, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v11, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/people/service/a/f;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/gms/people/identity/internal/AccountToken;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/android/gms/people/identity/internal/AccountToken;->c()Ljava/lang/String;

    move-result-object v7

    move-object v5, p1

    move-object v8, p3

    move-object/from16 v9, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/people/service/a/f;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;)V

    invoke-interface {v10, v11, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 1078
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 531
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 532
    const-string v0, "loadRemoteImageLegacy"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 534
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/people/service/q;->b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;)Lcom/google/android/gms/common/internal/bd;

    .line 535
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;II)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 505
    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    const-string v0, "loadAvatarLegacy"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 508
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/q;->b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/bd;

    .line 509
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 345
    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    const-string v0, "loadContactsGaiaIds24"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 348
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;I)V

    .line 351
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    .line 356
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    const-string v0, "loadContactsGaiaIds"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    :cond_0
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    iget-object v8, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v9, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/people/service/a/z;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/service/a/z;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v8, v9, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 365
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 387
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    const-string v0, "loadOwnerAvatarLegacy"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390
    :cond_0
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/gms/people/service/q;->b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/bd;

    .line 391
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    .line 1013
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1014
    const-string v0, "requestSync"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    aput-object p3, v1, v5

    aput-object p4, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1016
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 1021
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V

    .line 1022
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 13

    .prologue
    .line 1027
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1028
    const-string v1, "setAvatar"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1030
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 1032
    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1033
    const-string v1, "account"

    invoke-static {p2, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 1034
    const-string v1, "uri"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1036
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1038
    iget-object v11, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v12, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/ak;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v5, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object v6, p1

    move-object v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p5

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/people/service/a/ak;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V

    invoke-interface {v11, v12, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 1041
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 673
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 674
    const-string v1, "removeCircle"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 676
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 678
    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 679
    const-string v1, "account"

    invoke-static {p2, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 680
    const-string v1, "circleId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 682
    iget-object v11, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v12, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/b/f;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->c:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v6, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object v7, p1

    move-object v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/people/service/a/b/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v12, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 685
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 15
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 257
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    const-string v1, "loadCirclesOld"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p6, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 260
    :cond_0
    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 262
    iget-object v13, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v14, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/y;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/people/service/q;->d:I

    const/4 v12, 0x0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    invoke-direct/range {v1 .. v12}, Lcom/google/android/gms/people/service/a/y;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    invoke-interface {v13, v14, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 265
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 15

    .prologue
    .line 270
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271
    const-string v1, "loadCircles"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p6, v2, v3

    const/4 v3, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    :cond_0
    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 277
    iget-object v13, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v14, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/y;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/people/service/q;->d:I

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    move/from16 v12, p7

    invoke-direct/range {v1 .. v12}, Lcom/google/android/gms/people/service/a/y;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    invoke-interface {v13, v14, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 281
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;)V
    .locals 11

    .prologue
    .line 815
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816
    const-string v0, "loadPeopleForAggregation201"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    const/4 v2, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    aput-object p9, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 820
    :cond_0
    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)V

    .line 822
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)V
    .locals 13

    .prologue
    .line 828
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 829
    const-string v0, "loadPeopleForAggregation202"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    const/4 v2, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    aput-object p9, v1, v2

    const/16 v2, 0x8

    invoke-static/range {p10 .. p10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 833
    :cond_0
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-virtual/range {v0 .. v12}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)V

    .line 836
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)V
    .locals 20

    .prologue
    .line 843
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 844
    const-string v2, "loadPeopleForAggregation"

    const/16 v3, 0xb

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    const/4 v4, 0x2

    aput-object p4, v3, v4

    const/4 v4, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x7

    aput-object p9, v3, v4

    const/16 v4, 0x8

    invoke-static/range {p10 .. p10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x9

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xa

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 849
    :cond_0
    const-string v2, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 850
    const-string v2, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 852
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    move-object/from16 v19, v0

    new-instance v2, Lcom/google/android/gms/people/service/a/ab;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/gms/people/service/q;->d:I

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p5

    move/from16 v11, p6

    move/from16 v12, p7

    move/from16 v13, p8

    move-object/from16 v14, p9

    move/from16 v15, p10

    move/from16 v16, p11

    move/from16 v17, p12

    invoke-direct/range {v2 .. v17}, Lcom/google/android/gms/people/service/a/ab;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 856
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 656
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 657
    const-string v1, "addCircle"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 659
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 661
    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 662
    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 663
    const-string v1, "circleName"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 665
    iget-object v12, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v13, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/b/a;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->c:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v6, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/people/service/a/b/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v12, v13, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 668
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 16

    .prologue
    .line 691
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 692
    const-string v1, "updateCircle"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    const/4 v3, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    aput-object p7, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 695
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 697
    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 698
    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 699
    const-string v1, "circleId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 701
    if-nez p5, :cond_1

    if-nez p6, :cond_1

    if-eqz p7, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    const-string v2, "Nothing is changing"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 705
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/b/i;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/q;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/gms/people/service/q;->d:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    if-nez p6, :cond_3

    const/4 v12, 0x0

    :goto_1
    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v13, p7

    invoke-direct/range {v1 .. v13}, Lcom/google/android/gms/people/service/a/b/i;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    invoke-interface {v14, v15, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 710
    return-void

    .line 701
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 705
    :cond_3
    const/4 v7, 0x1

    move/from16 v0, p6

    if-ne v0, v7, :cond_4

    const/4 v7, 0x0

    :goto_2
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    goto :goto_1

    :cond_4
    const/4 v7, 0x1

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 13

    .prologue
    .line 630
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 631
    const-string v1, "addPeopleToCircle"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 633
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 635
    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    const-string v1, "account"

    invoke-static {p2, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 637
    const-string v1, "circleId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 638
    const-string v1, "qualifiedPersonIds"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 639
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 640
    if-eqz p5, :cond_2

    .line 641
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 642
    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    const-string v5, "Duplicate qualified person ID"

    invoke-static {v2, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 643
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 642
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 646
    :cond_2
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    const-string v2, "No qualified person IDs"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 648
    iget-object v11, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v12, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/b/b;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v5, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object v6, p1

    move-object v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/people/service/a/b/b;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v11, v12, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 651
    return-void

    .line 646
    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJ)V
    .locals 12

    .prologue
    .line 287
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    const-string v0, "loadPeopleOld"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    const/4 v2, 0x3

    aput-object p5, v1, v2

    const/4 v2, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291
    :cond_0
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-wide/from16 v8, p8

    invoke-virtual/range {v0 .. v11}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;I)V

    .line 293
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;I)V
    .locals 14

    .prologue
    .line 299
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    const-string v0, "loadPeople20"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    const/4 v2, 0x3

    aput-object p5, v1, v2

    const/4 v2, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    aput-object p10, v1, v2

    const/16 v2, 0x8

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    :cond_0
    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v12}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;II)V

    .line 305
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;II)V
    .locals 14

    .prologue
    .line 311
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    const-string v0, "loadPeople400"

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    const/4 v2, 0x3

    aput-object p5, v1, v2

    const/4 v2, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    aput-object p10, v1, v2

    const/16 v2, 0x8

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 315
    :cond_0
    const/4 v13, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    move/from16 v11, p11

    move/from16 v12, p12

    invoke-virtual/range {v0 .. v13}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V

    .line 318
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V
    .locals 22

    .prologue
    .line 325
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 326
    const-string v2, "loadPeople"

    const/16 v3, 0xb

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object p3, v3, v4

    const/4 v4, 0x2

    aput-object p4, v3, v4

    const/4 v4, 0x3

    aput-object p5, v3, v4

    const/4 v4, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x7

    aput-object p10, v3, v4

    const/16 v4, 0x8

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x9

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xa

    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    :cond_0
    const-string v2, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    const-string v2, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 331
    const v2, 0x1fffff

    and-int v2, v2, p6

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    const-string v3, "projection"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 332
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 333
    if-eqz p11, :cond_3

    const/4 v2, 0x1

    :goto_1
    const-string v3, "searchFields"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 336
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    move-object/from16 v20, v0

    new-instance v3, Lcom/google/android/gms/people/service/a/ad;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/gms/people/service/q;->d:I

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v12, p6

    move/from16 v13, p7

    move-wide/from16 v14, p8

    move-object/from16 v16, p10

    move/from16 v17, p11

    move/from16 v18, p13

    move/from16 v19, p12

    invoke-direct/range {v3 .. v19}, Lcom/google/android/gms/people/service/a/ad;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V

    move-object/from16 v0, v20

    invoke-interface {v2, v0, v3}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 340
    return-void

    .line 331
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 333
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    .line 582
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    const-string v0, "updatePersonCirclesOld"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    aput-object p4, v1, v3

    const/4 v2, 0x3

    aput-object p5, v1, v2

    const/4 v2, 0x4

    aput-object p6, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 586
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 588
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 590
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 16

    .prologue
    .line 596
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 597
    const-string v1, "updatePersonCircles"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    const/4 v3, 0x4

    aput-object p6, v2, v3

    const/4 v3, 0x5

    aput-object p7, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 600
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 602
    const-string v1, "callbacks"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 603
    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 604
    const-string v1, "qualifiedId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 608
    if-eqz p5, :cond_2

    .line 609
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 610
    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    const-string v5, "Duplicate circle ID"

    invoke-static {v2, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 611
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 610
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 614
    :cond_2
    if-eqz p6, :cond_4

    .line 615
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 616
    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_3
    const-string v5, "Duplicate circle ID"

    invoke-static {v2, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 617
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 616
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 620
    :cond_4
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_5

    const/4 v1, 0x1

    :goto_4
    const-string v2, "No circle IDs"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 622
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/b/j;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/q;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/gms/people/service/q;->d:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    invoke-direct/range {v1 .. v13}, Lcom/google/android/gms/people/service/a/b/j;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-interface {v14, v15, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 625
    return-void

    .line 620
    :cond_5
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 14

    .prologue
    .line 746
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 747
    const-string v1, "blockPerson"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 749
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 751
    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 752
    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 753
    const-string v1, "gaiaId"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    iget-object v12, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v13, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/b/d;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->c:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v6, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move/from16 v11, p5

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/people/service/a/b/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v12, v13, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 758
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v7, 0x0

    .line 792
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 793
    const-string v0, "loadPeopleForAggregation8"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v7

    const/4 v2, 0x1

    aput-object p3, v1, v2

    aput-object p4, v1, v3

    const/4 v2, 0x3

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    .line 796
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZII)V

    .line 798
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZII)V
    .locals 10

    .prologue
    .line 803
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 804
    const-string v0, "loadPeopleForAggregation200"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    aput-object p4, v1, v2

    const/4 v2, 0x3

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 807
    :cond_0
    const/4 v5, 0x7

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;)V

    .line 809
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v4, 0x2

    .line 908
    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 909
    const-string v0, "setSyncToContactsSettings"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    aput-object p4, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 911
    :cond_0
    invoke-static {}, Lcom/google/android/gms/people/f/k;->a()Lcom/google/android/gms/people/f/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/f/k;->a(Ljava/lang/String;)V

    .line 912
    invoke-static {}, Lcom/google/android/gms/people/service/q;->d()V

    .line 914
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 915
    const-string v0, "account"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 919
    iget-object v8, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v9, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/people/service/a/al;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/service/a/al;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Z[Ljava/lang/String;)V

    invoke-interface {v8, v9, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 922
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    .line 223
    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    const-string v0, "loadOwners1"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    aput-object p4, v1, v4

    const/4 v2, 0x3

    aput-object p5, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 226
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;ZZLjava/lang/String;Ljava/lang/String;I)V

    .line 228
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;ZZLjava/lang/String;Ljava/lang/String;I)V
    .locals 13

    .prologue
    .line 234
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235
    const-string v1, "loadOwners"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    aput-object p5, v2, v3

    const/4 v3, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 238
    :cond_0
    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    if-eqz p6, :cond_1

    const/4 v1, 0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 241
    if-eqz p2, :cond_2

    .line 242
    const-string v1, "account"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 245
    :cond_2
    iget-object v11, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v12, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/aa;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/people/service/q;->d:I

    move-object v5, p1

    move v6, p2

    move/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/people/service/a/aa;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;ZZLjava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v11, v12, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 248
    return-void

    .line 239
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 867
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 868
    const-string v0, "setSyncToContactsEnabled"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 870
    :cond_0
    invoke-static {}, Lcom/google/android/gms/people/f/k;->a()Lcom/google/android/gms/people/f/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/f/k;->a(Ljava/lang/String;)V

    .line 871
    invoke-static {}, Lcom/google/android/gms/people/service/q;->d()V

    .line 873
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 875
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/a/c;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 876
    if-ne v0, p1, :cond_2

    .line 884
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 890
    :cond_1
    :goto_0
    return-void

    .line 882
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/a/c;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 884
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 886
    if-eqz p1, :cond_1

    .line 887
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/people/service/a/ae;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/people/service/q;->d:I

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gms/people/service/a/ae;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    goto :goto_0

    .line 884
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 894
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 895
    const-string v0, "isSyncToContactsEnabled"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 897
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 899
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/a/c;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 901
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return v0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 960
    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 961
    const-string v0, "requestSyncOld"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 963
    :cond_0
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/people/internal/f;JZ)Lcom/google/android/gms/common/internal/bd;
    .locals 10

    .prologue
    const/4 v1, 0x2

    .line 564
    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565
    const-string v0, "loadContactImage"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 567
    :cond_0
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    new-instance v1, Lcom/google/android/gms/people/service/a/a/l;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/people/service/q;->d:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;)Lcom/google/android/gms/people/service/d;

    move-result-object v5

    move-wide v6, p2

    move v8, p4

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/a/l;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;JZ)V

    .line 574
    iget-object v0, p0, Lcom/google/android/gms/people/service/q;->g:Lcom/google/android/gms/people/service/c;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 575
    return-object v1
.end method

.method public final b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;)Lcom/google/android/gms/common/internal/bd;
    .locals 6

    .prologue
    .line 539
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    const-string v0, "loadRemoteImage"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 542
    :cond_0
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    const-string v0, "url"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 545
    new-instance v0, Lcom/google/android/gms/people/service/a/a/p;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;)Lcom/google/android/gms/people/service/d;

    move-result-object v4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/a/p;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;)V

    .line 548
    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->g:Lcom/google/android/gms/people/service/c;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 549
    return-object v0
.end method

.method public final b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/bd;
    .locals 8

    .prologue
    const/4 v4, 0x2

    .line 514
    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    const-string v0, "loadAvatar"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 517
    :cond_0
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    const-string v0, "avatarUrl"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 519
    const-string v0, "avatarSize"

    invoke-static {p3, v0}, Lcom/google/android/gms/people/ag;->a(ILjava/lang/String;)V

    .line 521
    new-instance v0, Lcom/google/android/gms/people/service/a/a/k;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;)Lcom/google/android/gms/people/service/d;

    move-result-object v4

    move-object v5, p2

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/service/a/a/k;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;II)V

    .line 525
    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->g:Lcom/google/android/gms/people/service/c;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 526
    return-object v0
.end method

.method public final b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/bd;
    .locals 9

    .prologue
    const/4 v3, 0x2

    .line 396
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    const-string v0, "loadOwnerAvatar"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 399
    :cond_0
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    const-string v0, "account"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 401
    const-string v0, "avatarSize"

    invoke-static {p4, v0}, Lcom/google/android/gms/people/ag;->a(ILjava/lang/String;)V

    .line 403
    new-instance v0, Lcom/google/android/gms/people/service/a/a/n;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;)Lcom/google/android/gms/people/service/d;

    move-result-object v4

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    move v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/people/service/a/a/n;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;Ljava/lang/String;II)V

    .line 407
    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->g:Lcom/google/android/gms/people/service/c;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 408
    return-object v0
.end method

.method public final b(Lcom/google/android/gms/people/internal/f;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1055
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1056
    const-string v0, "internalCall"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1058
    :cond_0
    iget-object v6, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v7, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/people/service/a/u;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/u;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Landroid/os/Bundle;)V

    invoke-interface {v6, v7, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 1060
    return-void
.end method

.method public final b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x2

    .line 715
    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    const-string v0, "loadAddToCircleConsent"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 718
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 720
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 721
    const-string v0, "account"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 723
    iget-object v8, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v9, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/people/service/a/b/e;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/service/a/b/e;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v9, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 725
    return-void
.end method

.method public final b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 414
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    const-string v0, "loadOwnerCoverPhotoLegacy"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 417
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/q;->c(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/internal/bd;

    .line 418
    return-void
.end method

.method public final b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 14

    .prologue
    .line 370
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 371
    const-string v1, "loadPeopleLive"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    const/4 v3, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p6, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 375
    const-string v1, "callbacks"

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    const-string v1, "account"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 377
    const-string v1, "query"

    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 379
    iget-object v12, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v13, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/people/service/a/ac;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/people/service/q;->d:I

    move-object v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/people/service/a/ac;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v12, v13, v1}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 382
    return-void
.end method

.method public final b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11

    .prologue
    .line 763
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 764
    const-string v0, "starPerson"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p4, v1, v2

    const/4 v2, 0x2

    invoke-static/range {p5 .. p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 766
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 768
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 769
    const-string v0, "account"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 770
    const-string v0, "peopleV2PersonId"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 772
    iget-object v9, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v10, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/people/service/a/b/h;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->c()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object v5, p1

    move-object v6, p2

    move-object v7, p4

    move/from16 v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/people/service/a/b/h;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v9, v10, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 775
    return-void
.end method

.method public final c(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/internal/bd;
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 423
    invoke-static {v5}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 424
    const-string v2, "loadOwnerCoverPhoto"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v1

    aput-object p3, v3, v0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 426
    :cond_0
    const-string v2, "callbacks"

    invoke-static {p1, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    const-string v2, "account"

    invoke-static {p2, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 428
    if-ltz p4, :cond_1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 430
    new-instance v0, Lcom/google/android/gms/people/service/a/a/o;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/q;->a(Lcom/google/android/gms/people/internal/f;)Lcom/google/android/gms/people/service/d;

    move-result-object v4

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/service/a/a/o;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Ljava/lang/String;Ljava/lang/String;I)V

    .line 434
    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->g:Lcom/google/android/gms/people/service/c;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 435
    return-object v0

    :cond_1
    move v0, v1

    .line 428
    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x2

    .line 730
    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 731
    const-string v0, "setHasShownAddToCircleConsent"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/q;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 733
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/service/q;->b()V

    .line 735
    const-string v0, "callbacks"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 736
    const-string v0, "account"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 738
    iget-object v8, p0, Lcom/google/android/gms/people/service/q;->f:Lcom/google/android/gms/people/service/c;

    iget-object v9, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/people/service/a/b/g;

    iget-object v1, p0, Lcom/google/android/gms/people/service/q;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/q;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/q;->d:I

    iget-object v4, p0, Lcom/google/android/gms/people/service/q;->e:Ljava/lang/String;

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/service/a/b/g;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v9, v0}, Lcom/google/android/gms/people/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/a;)V

    .line 741
    return-void
.end method
