.class public final Lcom/google/android/gms/auth/m;
.super Lcom/google/android/gms/common/f/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 28
    sget v0, Lcom/google/android/gms/p;->aA:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/common/f/a;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 29
    return-void
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 46
    if-eqz p2, :cond_0

    const-string v0, "initialize"

    invoke-virtual {p2, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const-string v0, "CredentialStateSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Initializing CredentialStateSyncAdapter for account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p3, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    invoke-static {p1, p3, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 50
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    sget-object v0, Lcom/google/android/gms/auth/b/a;->an:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {p1, p3, v1, v2, v3}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/gms/auth/m;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "credential-state"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "credential-state"

    aput-object v3, v2, v5

    invoke-static {v0, p1, p3, v1, v2}, Lcom/google/android/gsf/n;->a(Landroid/content/ContentResolver;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    .line 55
    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/m;->a(Z)V

    .line 57
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/m;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/w;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;-><init>(Ljava/lang/String;Z)V

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;->a(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/ah;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ah;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;
    :try_end_1
    .catch Lcom/google/android/gms/auth/firstparty/dataservice/aq; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/auth/firstparty/dataservice/ar; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    :goto_0
    invoke-virtual {p0, v5}, Lcom/google/android/gms/auth/m;->a(Z)V

    .line 63
    return-void

    .line 58
    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "CredentialStateSyncAdapter"

    const-string v2, "Credential State sync was canceled!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    invoke-virtual {p0, v5}, Lcom/google/android/gms/auth/m;->a(Z)V

    throw v0

    .line 58
    :catch_1
    move-exception v0

    :try_start_3
    const-string v1, "CredentialStateSyncAdapter"

    const-string v2, "Credential State failed with RemoteException!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
