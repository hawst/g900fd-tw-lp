.class public final Lcom/google/android/gms/drive/database/model/bb;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/drive/metadata/sync/a/d;

.field public b:Ljava/lang/Long;

.field private final c:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;JLcom/google/android/gms/drive/metadata/sync/a/c;Ljava/lang/String;Ljava/lang/Long;J)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->a()Lcom/google/android/gms/drive/database/model/bc;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {p0, p1, v0, v3}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 55
    cmp-long v0, p2, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 56
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    cmp-long v0, p7, v4

    if-ltz v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 59
    invoke-static {p5, p6, p7, p8}, Lcom/google/android/gms/drive/database/model/bb;->a(Ljava/lang/String;Ljava/lang/Long;J)V

    .line 61
    iput-wide p2, p0, Lcom/google/android/gms/drive/database/model/bb;->c:J

    .line 62
    invoke-static {p4, p5, p7, p8}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a(Lcom/google/android/gms/drive/metadata/sync/a/c;Ljava/lang/String;J)Lcom/google/android/gms/drive/metadata/sync/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    .line 63
    iput-object p6, p0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    .line 64
    return-void

    :cond_0
    move v0, v2

    .line 55
    goto :goto_0

    :cond_1
    move v1, v2

    .line 57
    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/bb;
    .locals 9

    .prologue
    .line 156
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->a:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 157
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->d:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    .line 158
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->e:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v6

    .line 160
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->f:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->c(Landroid/database/Cursor;)J

    move-result-wide v7

    .line 163
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->b:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 164
    sget-object v1, Lcom/google/android/gms/drive/database/model/bd;->c:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 166
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/sync/a/e;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/a/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/a/e;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/a/c;

    move-result-object v4

    .line 168
    new-instance v0, Lcom/google/android/gms/drive/database/model/bb;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/database/model/bb;-><init>(Lcom/google/android/gms/drive/database/i;JLcom/google/android/gms/drive/metadata/sync/a/c;Ljava/lang/String;Ljava/lang/Long;J)V

    .line 170
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->a()Lcom/google/android/gms/drive/database/model/bc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bc;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/bb;->d(J)V

    .line 172
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Long;J)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41
    if-nez p0, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-lez v2, :cond_0

    move v3, v0

    :goto_0
    if-nez p1, :cond_1

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_2

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid nextUri="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", clipTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 44
    return-void

    :cond_0
    move v3, v1

    .line 41
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->a:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/bb;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 134
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->b:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/a/c;->c:Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/a/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->c:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/a/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->d:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->e:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 141
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->f:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-wide v2, v1, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 143
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-wide v0, v0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-static {p1, p2, v0, v1}, Lcom/google/android/gms/drive/database/model/bb;->a(Ljava/lang/String;Ljava/lang/Long;J)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-static {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a(Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    .line 124
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    .line 125
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 147
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "PartialFeed[accountSqlId=%s, clipTime=%s, sqlId=%s, feedState=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/bb;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
