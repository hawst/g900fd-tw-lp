.class public final Lcom/google/android/gms/photos/autobackup/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field private b:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ad;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/ad;->b:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ad;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ad;->b:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V

    .line 40
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 3

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    const-class v0, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/ac;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ad;->b:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ad;->b:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/autobackup/ac;->a(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ad;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ad;->b:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ad;->b:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/autobackup/ac;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
