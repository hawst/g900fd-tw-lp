.class public final Lcom/google/android/gms/photos/autobackup/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/s;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/s;->b:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/s;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILcom/google/android/gms/photos/autobackup/model/SyncSettings;)V

    .line 47
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 6

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/s;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/s;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILcom/google/android/gms/photos/autobackup/model/SyncSettings;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/gms/photos/autobackup/model/g;

    invoke-direct {v1}, Lcom/google/android/gms/photos/autobackup/model/g;-><init>()V

    invoke-static {}, Lcom/google/android/libraries/social/autobackup/s;->a()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/android/gms/photos/autobackup/model/g;->a:Z

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/autobackup/s;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/android/gms/photos/autobackup/model/g;->b:Z

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/s;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;

    const/4 v4, 0x1

    iget-boolean v5, v1, Lcom/google/android/gms/photos/autobackup/model/g;->a:Z

    iget-boolean v1, v1, Lcom/google/android/gms/photos/autobackup/model/g;->b:Z

    invoke-direct {v3, v4, v5, v1}, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;-><init>(IZZ)V

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILcom/google/android/gms/photos/autobackup/model/SyncSettings;)V

    goto :goto_0
.end method
