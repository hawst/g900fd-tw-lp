.class final Lcom/google/android/gms/googlehelp/search/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/googlehelp/search/SearchView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/search/l;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 488
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/l;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-static {v1}, Lcom/google/android/gms/googlehelp/search/SearchView;->h(Lcom/google/android/gms/googlehelp/search/SearchView;)Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->isPopupShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/l;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-static {v1}, Lcom/google/android/gms/googlehelp/search/SearchView;->h(Lcom/google/android/gms/googlehelp/search/SearchView;)Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getListSelection()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 490
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/l;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-static {v0, p2, p3}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Lcom/google/android/gms/googlehelp/search/SearchView;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 505
    :goto_0
    return v0

    .line 495
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/l;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-static {v1}, Lcom/google/android/gms/googlehelp/search/SearchView;->h(Lcom/google/android/gms/googlehelp/search/SearchView;)Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->a(Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p3}, Landroid/support/v4/view/z;->b(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 496
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 497
    const/16 v1, 0x42

    if-ne p2, v1, :cond_1

    .line 498
    invoke-virtual {p1}, Landroid/view/View;->cancelLongPress()V

    .line 500
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/l;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-static {v1}, Lcom/google/android/gms/googlehelp/search/SearchView;->j(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    goto :goto_0

    .line 505
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
