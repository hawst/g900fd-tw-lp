.class public Lcom/google/android/gms/plus/apps/ManageDeviceActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/apps/ai;


# instance fields
.field private a:Landroid/support/v4/app/m;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/apps/ManageDeviceActivity;)Landroid/support/v4/app/m;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->a:Landroid/support/v4/app/m;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 76
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 77
    const-string v1, "deviceAddress"

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->setResult(ILandroid/content/Intent;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->finish()V

    .line 80
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 37
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 40
    const-string v1, "device"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/BleDevice;

    .line 41
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/BleDevice;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->b:Ljava/lang/String;

    .line 42
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/BleDevice;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->c:Ljava/lang/String;

    .line 43
    invoke-static {v0}, Lcom/google/android/gms/plus/apps/ag;->a(Lcom/google/android/gms/fitness/data/BleDevice;)Lcom/google/android/gms/plus/apps/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->a:Landroid/support/v4/app/m;

    .line 45
    sget v0, Lcom/google/android/gms/l;->dR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->setContentView(I)V

    .line 46
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v7/app/a;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->el:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/j;->pd:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    sget v0, Lcom/google/android/gms/p;->hl:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/j;->es:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/apps/aw;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/apps/aw;-><init>(Lcom/google/android/gms/plus/apps/ManageDeviceActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 84
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 90
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 87
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;->finish()V

    .line 88
    const/4 v0, 0x1

    goto :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
