.class public final Lcom/google/android/gms/games/ui/common/players/g;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private j:Lcom/google/android/gms/games/Player;

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/games/Player;Z)Lcom/google/android/gms/games/ui/common/players/g;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/games/ui/common/players/g;->a(Lcom/google/android/gms/games/Player;ZZ)Lcom/google/android/gms/games/ui/common/players/g;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/games/Player;ZZ)Lcom/google/android/gms/games/ui/common/players/g;
    .locals 4

    .prologue
    .line 68
    new-instance v1, Lcom/google/android/gms/games/ui/common/players/g;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/common/players/g;-><init>()V

    .line 69
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 70
    const-string v3, "com.google.android.gms.games.PLAYER"

    invoke-interface {p0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 71
    const-string v0, "isSelf"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 72
    const-string v0, "finishOnExit"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 73
    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/common/players/g;->setArguments(Landroid/os/Bundle;)V

    .line 74
    return-object v1
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->a_(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 98
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 99
    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 113
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_0

    instance-of v0, v0, Lcom/google/android/gms/games/Player;

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    .line 117
    if-nez v1, :cond_1

    .line 118
    const-string v0, "ProfileSummaryDFrag"

    const-string v1, "onClick: getActivity() returned null"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-virtual {v1}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 124
    :goto_1
    if-nez v0, :cond_3

    .line 125
    const-string v0, "ProfileSummaryDFrag"

    const-string v1, "No account name specified!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_2
    instance-of v0, v1, Lcom/google/android/gms/games/ui/q;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 130
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/common/players/g;->k:Z

    if-eqz v3, :cond_4

    .line 131
    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;Ljava/lang/String;ZLandroid/os/Bundle;)V

    .line 137
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/g;->l:Z

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    goto :goto_0

    .line 134
    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/players/g;->j:Lcom/google/android/gms/games/Player;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;)V

    const/16 v4, 0x44e

    invoke-static {v4, v0}, Lcom/google/android/gms/games/ui/a/c;->a(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.android.gms.games.OTHER_PLAYER"

    const-string v0, "The given player cannot be null"

    invoke-static {v3, v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v7

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/content/Context;)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v6, v5, v0, v7, v3}, Lcom/google/android/gms/common/internal/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "GamesDestApi"

    const-string v2, "Failed to downgrade player safely! Aborting."

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_3
    if-nez v0, :cond_6

    const-string v0, "GamesDestApi"

    const-string v1, "viewProfileComparison - Failed to add the player to the Intent"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    invoke-virtual {v4, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move v0, v2

    goto :goto_3

    :cond_6
    invoke-static {v1, v4}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_2
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 88
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/g;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 90
    const-string v0, "com.google.android.gms.games.PLAYER"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/g;->j:Lcom/google/android/gms/games/Player;

    .line 91
    const-string v0, "isSelf"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/g;->k:Z

    .line 92
    const-string v0, "finishOnExit"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/g;->l:Z

    .line 93
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    .line 81
    sget v0, Lcom/google/android/gms/l;->bv:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 82
    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/players/g;->j:Lcom/google/android/gms/games/Player;

    sget v0, Lcom/google/android/gms/j;->oM:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    sget v0, Lcom/google/android/gms/j;->oL:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    sget v0, Lcom/google/android/gms/j;->pL:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    sget v0, Lcom/google/android/gms/j;->bD:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/j;->oQ:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/j;->oO:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget v9, Lcom/google/android/gms/j;->nf:I

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/g;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->v_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    sget v1, Lcom/google/android/gms/g;->af:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    sget v1, Lcom/google/android/gms/g;->ag:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    invoke-static {v10, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/res/Resources;I)I

    move-result v0

    const/4 v1, 0x3

    new-array v1, v1, [F

    invoke-static {v0, v1}, Landroid/graphics/Color;->colorToHSV(I[F)V

    const/4 v0, 0x2

    aget v2, v1, v0

    const/high16 v11, 0x3f000000    # 0.5f

    mul-float/2addr v2, v11

    aput v2, v1, v0

    invoke-static {v1}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->aQ:I

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/g;->k:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/gms/p;->lS:I

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->aQ:I

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/g;->aP:I

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    sget v2, Lcom/google/android/gms/g;->aO:I

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x106000b

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundResource(I)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 83
    return-object v4

    .line 82
    :cond_0
    const/16 v11, 0x8

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_1
    sget v0, Lcom/google/android/gms/p;->lO:I

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onDismiss(Landroid/content/DialogInterface;)V

    .line 105
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/g;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    .line 108
    :cond_0
    return-void
.end method
