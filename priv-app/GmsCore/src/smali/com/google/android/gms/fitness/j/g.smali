.class final Lcom/google/android/gms/fitness/j/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/j/f;

.field private final b:Lcom/google/android/gms/fitness/data/DataPoint;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/j/f;Lcom/google/android/gms/fitness/data/DataPoint;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p2, p0, Lcom/google/android/gms/fitness/j/g;->b:Lcom/google/android/gms/fitness/data/DataPoint;

    .line 120
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/j/f;Lcom/google/android/gms/fitness/data/DataPoint;B)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/j/g;-><init>(Lcom/google/android/gms/fitness/j/f;Lcom/google/android/gms/fitness/data/DataPoint;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v0}, Lcom/google/android/gms/fitness/j/f;->a(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/j/e;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/fitness/j/g;->b:Lcom/google/android/gms/fitness/data/DataPoint;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    iget-wide v6, v0, Lcom/google/android/gms/fitness/j/e;->d:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 125
    const-string v0, "Registration timed-out. Unregistering."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/j/f;->a()V

    .line 187
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 124
    goto :goto_0

    .line 130
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v0}, Lcom/google/android/gms/fitness/j/f;->a(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/j/e;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/fitness/j/g;->b:Lcom/google/android/gms/fitness/data/DataPoint;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    iget-boolean v3, v0, Lcom/google/android/gms/fitness/j/e;->b:Z

    if-eqz v3, :cond_3

    iget-wide v6, v0, Lcom/google/android/gms/fitness/j/e;->c:J

    sub-long v6, v4, v6

    iget-wide v8, v0, Lcom/google/android/gms/fitness/j/e;->a:J

    cmp-long v3, v6, v8

    if-ltz v3, :cond_4

    :cond_3
    iput-boolean v2, v0, Lcom/google/android/gms/fitness/j/e;->b:Z

    iput-wide v4, v0, Lcom/google/android/gms/fitness/j/e;->c:J

    move v0, v2

    :goto_2
    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v0}, Lcom/google/android/gms/fitness/j/f;->c(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/d/d;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v3}, Lcom/google/android/gms/fitness/j/f;->b(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/internal/a;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/fitness/internal/a;->b:Ljava/lang/String;

    new-array v4, v2, [Lcom/google/android/gms/fitness/data/DataType;

    iget-object v5, p0, Lcom/google/android/gms/fitness/j/g;->b:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-virtual {v5}, Lcom/google/android/gms/fitness/data/DataPoint;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v0, v3, v4, v5}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v0

    .line 140
    iget-boolean v3, v0, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v3, :cond_6

    .line 141
    iget-object v0, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    if-eqz v0, :cond_5

    .line 142
    const-string v0, "Package [%s] does not have permissions to read data point [%s]. Unregistering"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v4}, Lcom/google/android/gms/fitness/j/f;->b(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/internal/a;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/fitness/internal/a;->b:Ljava/lang/String;

    aput-object v4, v3, v1

    iget-object v1, p0, Lcom/google/android/gms/fitness/j/g;->b:Lcom/google/android/gms/fitness/data/DataPoint;

    aput-object v1, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/j/f;->a()V

    goto :goto_1

    :cond_4
    move v0, v1

    .line 130
    goto :goto_2

    .line 150
    :cond_5
    const-string v0, "Auth check failed, but the issue may be transient. Continuing delivering data points."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 156
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v0}, Lcom/google/android/gms/fitness/j/f;->d(Lcom/google/android/gms/fitness/j/f;)Landroid/app/PendingIntent;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v0}, Lcom/google/android/gms/fitness/j/f;->e(Lcom/google/android/gms/fitness/j/f;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 157
    const-string v0, "Application uninstalled with registered listener %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v1

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/j/f;->b()V

    goto/16 :goto_1

    .line 164
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v0}, Lcom/google/android/gms/fitness/j/f;->f(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/location/n/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/n/ae;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 169
    const-string v0, "Dropping sensor data in background user for listener %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v1

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_1

    .line 173
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v0}, Lcom/google/android/gms/fitness/j/f;->b(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/internal/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/fitness/internal/a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/fitness/internal/aj;->a(Ljava/lang/String;)V

    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    iget-object v1, p0, Lcom/google/android/gms/fitness/j/g;->b:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/j/f;->a(Lcom/google/android/gms/fitness/j/f;Lcom/google/android/gms/fitness/data/DataPoint;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    invoke-static {}, Lcom/google/android/gms/fitness/internal/aj;->a()V

    goto/16 :goto_1

    .line 177
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "Found dead listener %s, removing."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v3}, Lcom/google/android/gms/fitness/j/f;->g(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/j/a;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/j/f;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    invoke-static {}, Lcom/google/android/gms/fitness/internal/aj;->a()V

    goto/16 :goto_1

    .line 179
    :catch_1
    move-exception v0

    :try_start_2
    const-string v1, "Cannot send event to client."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 186
    invoke-static {}, Lcom/google/android/gms/fitness/internal/aj;->a()V

    goto/16 :goto_1

    .line 182
    :catch_2
    move-exception v0

    :try_start_3
    const-string v0, "Found dead intent listener %s, removing."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/j/f;->b()V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/g;->a:Lcom/google/android/gms/fitness/j/f;

    invoke-static {v0}, Lcom/google/android/gms/fitness/j/f;->h(Lcom/google/android/gms/fitness/j/f;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 186
    invoke-static {}, Lcom/google/android/gms/fitness/internal/aj;->a()V

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/fitness/internal/aj;->a()V

    throw v0
.end method
