.class final Lcom/google/android/gms/auth/setup/d2d/ab;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 202
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->a(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v2}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->h(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)I

    move-result v2

    const-wide/16 v4, 0x1e

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v4, v5, v6}, Ljava/util/concurrent/Semaphore;->tryAcquire(IJLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    .line 213
    array-length v0, v5

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->d()V

    .line 251
    :goto_1
    return-object v11

    .line 206
    :catch_0
    move-exception v0

    .line 207
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->h()Lcom/google/android/gms/auth/d/a;

    move-result-object v2

    const-string v4, "Interrupted. Finishing this activity."

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 220
    :cond_0
    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v6

    .line 222
    array-length v7, v5

    move v4, v3

    move v0, v3

    :goto_2
    if-ge v4, v7, :cond_3

    aget-object v8, v5, v4

    .line 223
    iget-object v2, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v9}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->i(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 226
    :cond_1
    array-length v9, v6

    move v2, v3

    :goto_3
    if-ge v2, v9, :cond_2

    aget-object v10, v6, v2

    .line 227
    iget-object v10, v10, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v8, v10, v1}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 226
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 222
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 234
    :cond_3
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 235
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->i(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->j(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 238
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->h()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Restore account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v4}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->i(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 239
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->h()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Restore token: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v4}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->j(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 240
    const-string v0, "restoreAccount"

    iget-object v3, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v3}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->i(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    const-string v0, "restoreToken"

    iget-object v3, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v3}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->j(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->f()Lcom/google/android/gms/auth/f/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/e;->a(Z)Lcom/google/android/gms/auth/f/e;

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->f()Lcom/google/android/gms/auth/f/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v1}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->k(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/e;->b(I)Lcom/google/android/gms/auth/f/e;

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->f()Lcom/google/android/gms/auth/f/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-static {v1}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->l(Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/e;->d(I)Lcom/google/android/gms/auth/f/e;

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->setResult(ILandroid/content/Intent;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->finish()V

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/ab;->a:Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/TargetAddAccountsActivity;->e()Lcom/google/android/setupwizard/util/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/setupwizard/util/c;->a()V

    goto/16 :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/ab;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
