.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/h;

.field private static final f:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:D


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 400
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/h;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/h;

    .line 420
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 423
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->f:Ljava/util/HashMap;

    const-string v1, "loggingId"

    const-string v2, "loggingId"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->f:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->f:Ljava/util/HashMap;

    const-string v1, "value"

    const-string v2, "value"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->d(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 463
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 464
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->b:I

    .line 465
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->a:Ljava/util/Set;

    .line 466
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;D)V
    .locals 1

    .prologue
    .line 475
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 476
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->a:Ljava/util/Set;

    .line 477
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->b:I

    .line 478
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->c:Ljava/lang/String;

    .line 479
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->d:Ljava/lang/String;

    .line 480
    iput-wide p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->e:D

    .line 481
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 430
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;D)V
    .locals 5

    .prologue
    .line 590
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 591
    packed-switch v0, :pswitch_data_0

    .line 596
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a double."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 593
    :pswitch_0
    iput-wide p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->e:D

    .line 599
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 600
    return-void

    .line 591
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 605
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 606
    packed-switch v0, :pswitch_data_0

    .line 614
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 608
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->c:Ljava/lang/String;

    .line 617
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 618
    return-void

    .line 611
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->d:Ljava/lang/String;

    goto :goto_0

    .line 606
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 557
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 562
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 570
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 564
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->c:Ljava/lang/String;

    .line 568
    :goto_0
    return-object v0

    .line 566
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->d:Ljava/lang/String;

    goto :goto_0

    .line 568
    :pswitch_2
    iget-wide v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->e:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 562
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 534
    iget-wide v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->e:D

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 547
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/h;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 635
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;

    if-nez v0, :cond_0

    move v0, v1

    .line 666
    :goto_0
    return v0

    .line 640
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 641
    goto :goto_0

    .line 644
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;

    .line 645
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 646
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 647
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 649
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 651
    goto :goto_0

    :cond_3
    move v0, v1

    .line 656
    goto :goto_0

    .line 659
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 661
    goto :goto_0

    :cond_5
    move v0, v2

    .line 666
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 622
    const/4 v0, 0x0

    .line 623
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 624
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 625
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 626
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 628
    goto :goto_0

    .line 629
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 552
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/h;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/h;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;Landroid/os/Parcel;)V

    .line 553
    return-void
.end method
