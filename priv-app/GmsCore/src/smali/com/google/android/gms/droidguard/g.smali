.class public final Lcom/google/android/gms/droidguard/g;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lcom/google/protobuf/a/a;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Lcom/google/protobuf/a/a;

.field public g:Z

.field public h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 469
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 474
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/android/gms/droidguard/g;->b:Lcom/google/protobuf/a/a;

    .line 491
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/droidguard/g;->d:Ljava/lang/String;

    .line 508
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/android/gms/droidguard/g;->f:Lcom/google/protobuf/a/a;

    .line 525
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/droidguard/g;->h:I

    .line 569
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/droidguard/g;->i:I

    .line 469
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 572
    iget v0, p0, Lcom/google/android/gms/droidguard/g;->i:I

    if-gez v0, :cond_0

    .line 574
    invoke-virtual {p0}, Lcom/google/android/gms/droidguard/g;->b()I

    .line 576
    :cond_0
    iget v0, p0, Lcom/google/android/gms/droidguard/g;->i:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 466
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    iput-boolean v1, p0, Lcom/google/android/gms/droidguard/g;->a:Z

    iput-object v0, p0, Lcom/google/android/gms/droidguard/g;->b:Lcom/google/protobuf/a/a;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Lcom/google/android/gms/droidguard/g;->c:Z

    iput-object v0, p0, Lcom/google/android/gms/droidguard/g;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    iput-boolean v1, p0, Lcom/google/android/gms/droidguard/g;->e:Z

    iput-object v0, p0, Lcom/google/android/gms/droidguard/g;->f:Lcom/google/protobuf/a/a;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v1, p0, Lcom/google/android/gms/droidguard/g;->g:Z

    iput v0, p0, Lcom/google/android/gms/droidguard/g;->h:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 555
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/g;->a:Z

    if-eqz v0, :cond_0

    .line 556
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/droidguard/g;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 558
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/g;->c:Z

    if-eqz v0, :cond_1

    .line 559
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/droidguard/g;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 561
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/g;->e:Z

    if-eqz v0, :cond_2

    .line 562
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/droidguard/g;->f:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 564
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/g;->g:Z

    if-eqz v0, :cond_3

    .line 565
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/droidguard/g;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(II)V

    .line 567
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 581
    const/4 v0, 0x0

    .line 582
    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/g;->a:Z

    if-eqz v1, :cond_0

    .line 583
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/droidguard/g;->b:Lcom/google/protobuf/a/a;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 586
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/g;->c:Z

    if-eqz v1, :cond_1

    .line 587
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/droidguard/g;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 590
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/g;->e:Z

    if-eqz v1, :cond_2

    .line 591
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/droidguard/g;->f:Lcom/google/protobuf/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v1

    add-int/2addr v0, v1

    .line 594
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/g;->g:Z

    if-eqz v1, :cond_3

    .line 595
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/droidguard/g;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 598
    :cond_3
    iput v0, p0, Lcom/google/android/gms/droidguard/g;->i:I

    .line 599
    return v0
.end method
