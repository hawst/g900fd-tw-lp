.class public final Lcom/google/android/gms/droidguard/b/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:[I

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I


# instance fields
.field a:Lcom/google/android/gms/droidguard/b/m;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/16 v0, 0x19

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/droidguard/b/g;->b:[I

    .line 22
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x66

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/droidguard/b/g;->c:[I

    .line 24
    const/16 v0, 0x28

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/gms/droidguard/b/g;->d:[I

    .line 28
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/gms/droidguard/b/g;->e:[I

    .line 30
    const/16 v0, 0x1a

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/gms/droidguard/b/g;->f:[I

    return-void

    .line 19
    nop

    :array_0
    .array-data 4
        0xde
        0x66
        0xc4
        0x9d
        0xd3
        0xc4
        0xcc
        0xe0
        0xba
        0xca
        0xb7
        0x26
        0xc9
        0xcd
        0xce
        0x66
        0xca
        0xd7
        0x75
        0xc3
        0x85
        0x66
        0xb7
        0xb6
        0x75
    .end array-data

    .line 24
    :array_1
    .array-data 4
        0xd2
        0x66
        0xcb
        0xde
        0xc4
        0x9d
        0xc4
        0xdb
        0xba
        0xb7
        0x26
        0xd8
        0xd2
        0x66
        0x75
        0xda
        0xc3
        0xd2
        0x85
        0xd9
        0x66
        0xdb
        0xb7
        0xb6
        0x75
        0xe0
        0xd0
        0x66
        0xb7
        0xb6
        0xd6
        0xd9
        0x75
        0xb5
        0xcb
        0xbc
        0x25
        0xe4
        0xda
        0xb6
    .end array-data

    .line 28
    :array_2
    .array-data 4
        0xd3
        0x7f
        0x4
        0xcc
        0xce
        0xde
        0xc5
        0x13
        0x35
    .end array-data

    .line 30
    :array_3
    .array-data 4
        0xcd
        0xdd
        0x66
        0xd9
        0xb0
        0xce
        0xb7
        0x95
        0xc0
        0xbe
        0x9a
        0xd6
        0xc9
        0xd5
        0x66
        0xd1
        0x75
        0xc3
        0x85
        0x66
        0xd4
        0xcb
        0xd5
        0xb7
        0xb6
        0x75
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/gms/droidguard/b/m;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/droidguard/b/g;->a:Lcom/google/android/gms/droidguard/b/m;

    .line 40
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .locals 8

    .prologue
    .line 68
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 86
    :cond_0
    return-void

    .line 73
    :cond_1
    new-instance v1, Lcom/google/android/gms/droidguard/b/h;

    invoke-direct {v1, p0}, Lcom/google/android/gms/droidguard/b/h;-><init>(Lcom/google/android/gms/droidguard/b/g;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 79
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 81
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/droidguard/b/g;->a:Lcom/google/android/gms/droidguard/b/m;

    sget-object v6, Lcom/google/android/gms/droidguard/b/g;->c:[I

    invoke-virtual {v5, v6}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/droidguard/b/g;->a:Lcom/google/android/gms/droidguard/b/m;

    sget-object v6, Lcom/google/android/gms/droidguard/b/g;->c:[I

    invoke-virtual {v5, v6}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/droidguard/b/g;->a:Lcom/google/android/gms/droidguard/b/m;

    sget-object v6, Lcom/google/android/gms/droidguard/b/g;->e:[I

    invoke-virtual {v5, v6}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/gms/common/util/al;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 50
    iget-object v1, p0, Lcom/google/android/gms/droidguard/b/g;->a:Lcom/google/android/gms/droidguard/b/m;

    sget-object v2, Lcom/google/android/gms/droidguard/b/g;->f:[I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/droidguard/b/g;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/droidguard/b/g;->a:Lcom/google/android/gms/droidguard/b/m;

    sget-object v2, Lcom/google/android/gms/droidguard/b/g;->b:[I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/droidguard/b/g;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 52
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 54
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/gms/droidguard/b/g;->a:Lcom/google/android/gms/droidguard/b/m;

    sget-object v3, Lcom/google/android/gms/droidguard/b/g;->d:[I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/droidguard/b/g;->a:Lcom/google/android/gms/droidguard/b/m;

    sget-object v3, Lcom/google/android/gms/droidguard/b/g;->e:[I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/util/al;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/droidguard/b/g;->a:Lcom/google/android/gms/droidguard/b/m;

    sget-object v2, Lcom/google/android/gms/droidguard/b/g;->e:[I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/al;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
