.class public Lcom/google/android/gms/drive/api/a/p;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/DeleteResourceRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/DeleteResourceRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x7

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/p;->e:Lcom/google/android/gms/drive/internal/DeleteResourceRequest;

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 3

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/p;->e:Lcom/google/android/gms/drive/internal/DeleteResourceRequest;

    const-string v1, "Invalid delete request."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/p;->e:Lcom/google/android/gms/drive/internal/DeleteResourceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/DeleteResourceRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid delete request."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/p;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/p;->e:Lcom/google/android/gms/drive/internal/DeleteResourceRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/DeleteResourceRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/p;->c:Lcom/google/android/gms/drive/c/a;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/api/d;->b(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/c/a;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/p;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 37
    return-void
.end method
