.class public final Lcom/google/android/gms/fitness/service/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/service/aq;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/service/aq;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/at;->a:Lcom/google/android/gms/fitness/service/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 138
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    .line 140
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 141
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    .line 142
    sub-long/2addr v2, v0

    .line 144
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/at;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/aq;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 148
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/a/o;

    .line 150
    iget-object v6, p0, Lcom/google/android/gms/fitness/service/at;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v6, v6, Lcom/google/android/gms/fitness/service/aq;->a:Landroid/content/Context;

    invoke-static {v6, v1}, Lcom/google/android/gms/common/util/a;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    .line 151
    if-eqz v6, :cond_0

    .line 152
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/fitness/a/o;->a(J)V

    goto :goto_0

    .line 154
    :cond_0
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 158
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 159
    iget-object v2, p0, Lcom/google/android/gms/fitness/service/at;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v2, v2, Lcom/google/android/gms/fitness/service/aq;->a:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 160
    iget-object v2, p0, Lcom/google/android/gms/fitness/service/at;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v2, v2, Lcom/google/android/gms/fitness/service/aq;->d:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/at;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/aq;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/at;->a:Lcom/google/android/gms/fitness/service/aq;

    iget-object v1, v0, Lcom/google/android/gms/fitness/service/aq;->b:Landroid/os/Handler;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 168
    :cond_3
    return-void
.end method
