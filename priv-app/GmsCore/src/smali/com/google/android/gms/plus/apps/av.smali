.class public final Lcom/google/android/gms/plus/apps/av;
.super Lcom/google/android/gms/common/ui/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/ah;


# instance fields
.field b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/plus/internal/ad;

.field private final d:Landroid/accounts/Account;

.field private final e:I

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 28
    sget-object v5, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/apps/av;-><init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V

    .line 29
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/h;-><init>(Landroid/content/Context;)V

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/plus/apps/av;->d:Landroid/accounts/Account;

    .line 36
    iput p3, p0, Lcom/google/android/gms/plus/apps/av;->e:I

    .line 37
    iput-object p4, p0, Lcom/google/android/gms/plus/apps/av;->f:Ljava/lang/String;

    .line 38
    iput-object p5, p0, Lcom/google/android/gms/plus/apps/av;->c:Lcom/google/android/gms/plus/internal/ad;

    .line 39
    return-void
.end method


# virtual methods
.method protected final synthetic a(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/common/er;
    .locals 2

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/av;->c:Lcom/google/android/gms/plus/internal/ad;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/av;->d:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2, p3, v1}, Lcom/google/android/gms/plus/apps/c;->b(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/a/b;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 56
    iput-object p3, p0, Lcom/google/android/gms/plus/apps/av;->b:Ljava/lang/String;

    .line 57
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/apps/av;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/data/a;)V

    .line 58
    return-void
.end method

.method protected final bridge synthetic a(Lcom/google/android/gms/common/er;)V
    .locals 2

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/gms/plus/internal/ab;

    iget v0, p0, Lcom/google/android/gms/plus/apps/av;->e:I

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/av;->f:Ljava/lang/String;

    invoke-interface {p1, p0, v0, v1}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ah;ILjava/lang/String;)V

    return-void
.end method
