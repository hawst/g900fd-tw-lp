.class public Lcom/google/android/gms/appdatasearch/ResultClickInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ap;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:[Lcom/google/android/gms/appdatasearch/DocumentId;

.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/gms/appdatasearch/ap;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ap;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/ap;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->a:I

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->b:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->c:[Lcom/google/android/gms/appdatasearch/DocumentId;

    .line 37
    iput p4, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->d:I

    .line 38
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()[Lcom/google/android/gms/appdatasearch/DocumentId;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->c:[Lcom/google/android/gms/appdatasearch/DocumentId;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/ap;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/ap;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ap;->a(Lcom/google/android/gms/appdatasearch/ResultClickInfo;Landroid/os/Parcel;I)V

    .line 66
    return-void
.end method
