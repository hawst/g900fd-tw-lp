.class public final Lcom/google/android/gms/kids/timeouts/c;
.super Lcom/google/android/gms/kids/common/c/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/kids/common/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/kids/common/service/a;)V
    .locals 13

    .prologue
    .line 41
    const/4 v6, 0x0

    .line 42
    const/4 v7, 0x0

    .line 43
    invoke-static {}, Lcom/google/android/gms/kids/timeouts/d;->e()Lcom/google/android/gms/kids/timeouts/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/kids/timeouts/d;->d()Landroid/net/Uri;

    move-result-object v1

    .line 45
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/kids/common/service/a;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/kids/timeouts/TimeoutsChangedReceiver;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v9

    .line 51
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move v0, v7

    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_5

    .line 52
    add-int/lit8 v10, v0, 0x1

    .line 53
    new-instance v11, Lcom/google/s/a/z;

    invoke-direct {v11}, Lcom/google/s/a/z;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v11, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_2
    .catch Lcom/google/protobuf/nano/i; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 60
    :try_start_3
    iget-object v0, v11, Lcom/google/s/a/z;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    const-string v0, "TimeoutsChangedReceiver"

    const-string v2, "Invalid timeout.type"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move v0, v10

    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    const-string v2, "TimeoutsChangedReceiver"

    const-string v3, "Could not parse %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/kids/common/f;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 63
    :catchall_0
    move-exception v0

    move-object v1, v9

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    .line 60
    :cond_1
    :try_start_4
    iget-object v0, v11, Lcom/google/s/a/z;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "TimeoutsChangedReceiver"

    const-string v2, "unmatched timeout type"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_1
    const-string v0, "TimeoutsAlarmRegistry"

    const-string v2, "Schedule Timeout"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v11}, Lcom/google/android/gms/kids/timeouts/a;->a(Lcom/google/s/a/z;)Landroid/util/Pair;

    move-result-object v12

    iget-object v0, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/gms/kids/common/a;->b()Landroid/app/AlarmManager;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->b()Landroid/content/Context;

    move-result-object v0

    const/4 v3, 0x0

    iget-object v6, v11, Lcom/google/s/a/z;->a:Ljava/lang/String;

    iget-object v7, v11, Lcom/google/s/a/z;->c:Ljava/lang/Integer;

    invoke-static {v6, v7}, Lcom/google/android/gms/kids/timeouts/b;->a(Ljava/lang/String;Ljava/lang/Integer;)Landroid/content/Intent;

    move-result-object v6

    const/high16 v7, 0x10000000

    invoke-static {v0, v3, v6, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    iget-object v0, v11, Lcom/google/s/a/z;->g:[I

    if-eqz v0, :cond_3

    iget-object v0, v11, Lcom/google/s/a/z;->g:[I

    array-length v0, v0

    if-lez v0, :cond_3

    const-string v0, "TimeoutsAlarmRegistry"

    const-string v3, "Activate repeating alarm"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v3, v6}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v3, 0x2

    sget-wide v6, Lcom/google/android/gms/kids/timeouts/a;->a:J

    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    :cond_2
    :goto_3
    iget-object v0, v12, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, v12, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {}, Lcom/google/android/gms/kids/common/a;->b()Landroid/app/AlarmManager;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->b()Landroid/content/Context;

    move-result-object v0

    const/4 v3, 0x1

    iget-object v6, v11, Lcom/google/s/a/z;->a:Ljava/lang/String;

    iget-object v7, v11, Lcom/google/s/a/z;->c:Ljava/lang/Integer;

    invoke-static {v6, v7}, Lcom/google/android/gms/kids/timeouts/b;->a(Ljava/lang/String;Ljava/lang/Integer;)Landroid/content/Intent;

    move-result-object v6

    const/high16 v7, 0x10000000

    invoke-static {v0, v3, v6, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    iget-object v0, v11, Lcom/google/s/a/z;->g:[I

    if-eqz v0, :cond_4

    iget-object v0, v11, Lcom/google/s/a/z;->g:[I

    array-length v0, v0

    if-lez v0, :cond_4

    const-string v0, "TimeoutsAlarmRegistry"

    const-string v3, "Deactivate repeating alarm"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v3, v6}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v3, 0x2

    sget-wide v6, Lcom/google/android/gms/kids/timeouts/a;->a:J

    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto/16 :goto_1

    :cond_3
    const-string v0, "TimeoutsAlarmRegistry"

    const-string v3, "Activate one time alarm"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v3, v6}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x2

    invoke-virtual {v2, v0, v4, v5, v8}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_3

    :cond_4
    const-string v0, "TimeoutsAlarmRegistry"

    const-string v3, "Deactivate one time alarm"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v3, v6}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x2

    invoke-virtual {v2, v0, v4, v5, v8}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto/16 :goto_1

    :pswitch_2
    const-string v0, "TimeoutsChangedReceiver"

    const-string v2, "unspecified timeout"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 63
    :cond_5
    invoke-static {v9}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 65
    const-string v2, "TimeoutsChangedReceiver"

    const-string v3, "Processed %d entries"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    invoke-virtual {p1}, Lcom/google/android/gms/kids/common/service/a;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "is_deleted != 0"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 80
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/kids/timeouts/TimeoutsSystemAlertService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/kids/common/service/a;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 81
    return-void

    .line 63
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto/16 :goto_2

    .line 60
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
