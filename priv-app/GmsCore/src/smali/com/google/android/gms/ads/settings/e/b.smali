.class public final Lcom/google/android/gms/ads/settings/e/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(I[BI)V
    .locals 2

    .prologue
    .line 186
    and-int/lit16 v0, p0, 0xff

    int-to-byte v0, v0

    aput-byte v0, p1, p2

    .line 187
    add-int/lit8 v0, p2, 0x1

    const v1, 0xff00

    and-int/2addr v1, p0

    ushr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 188
    add-int/lit8 v0, p2, 0x2

    const/high16 v1, 0xff0000

    and-int/2addr v1, p0

    ushr-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 189
    add-int/lit8 v0, p2, 0x3

    const/high16 v1, -0x1000000

    and-int/2addr v1, p0

    ushr-int/lit8 v1, v1, 0x18

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 190
    return-void
.end method

.method public static a([B)V
    .locals 8

    .prologue
    const v7, 0xfff1

    const/16 v6, 0x2c

    const/4 v3, 0x0

    .line 53
    move v0, v3

    move v1, v3

    move v2, v3

    .line 62
    :goto_0
    add-int/lit8 v4, v0, 0x10

    if-gt v4, v6, :cond_1

    move v4, v2

    move v2, v1

    move v1, v3

    .line 63
    :goto_1
    const/16 v5, 0x10

    if-ge v1, v5, :cond_0

    .line 64
    add-int v5, v0, v1

    aget-byte v5, p0, v5

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v4, v5

    .line 65
    add-int/2addr v2, v4

    .line 63
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 67
    :cond_0
    add-int/lit8 v0, v0, 0x10

    move v1, v2

    move v2, v4

    goto :goto_0

    .line 69
    :cond_1
    :goto_2
    if-ge v0, v6, :cond_2

    .line 70
    aget-byte v3, p0, v0

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v2, v3

    .line 71
    add-int/2addr v1, v2

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 74
    :cond_2
    rem-int v0, v2, v7

    .line 75
    rem-int/2addr v1, v7

    .line 76
    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 79
    invoke-static {v0, p0, v6}, Lcom/google/android/gms/ads/settings/e/b;->a(I[BI)V

    .line 80
    return-void
.end method
