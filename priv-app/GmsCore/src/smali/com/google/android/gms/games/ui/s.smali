.class public abstract Lcom/google/android/gms/games/ui/s;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/games/ui/d/f;
.implements Lcom/google/android/gms/games/ui/d/q;
.implements Lcom/google/android/gms/games/ui/d/r;
.implements Lcom/google/android/gms/games/ui/d/s;


# instance fields
.field protected d:Lcom/google/android/gms/games/ui/q;

.field protected e:Landroid/view/LayoutInflater;

.field protected f:Landroid/view/View;

.field protected g:Landroid/support/v7/widget/RecyclerView;

.field protected h:Lcom/google/android/gms/games/ui/d/p;

.field protected i:Z

.field protected j:Landroid/support/v4/widget/SwipeRefreshLayout;

.field protected k:Landroid/support/v4/widget/bp;

.field private final l:Landroid/os/Handler;

.field private final m:Ljava/lang/Runnable;

.field private n:Z

.field private o:Landroid/support/v7/widget/ch;

.field private final p:Lcom/google/android/gms/games/ui/co;

.field private q:Landroid/database/ContentObserver;

.field private r:Z

.field private s:Z

.field private t:I

.field private final u:Landroid/support/v7/widget/ch;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 107
    sget v0, Lcom/google/android/gms/l;->bC:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/s;-><init>(I)V

    .line 108
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/p;-><init>(I)V

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->l:Landroid/os/Handler;

    .line 73
    new-instance v0, Lcom/google/android/gms/games/ui/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/t;-><init>(Lcom/google/android/gms/games/ui/s;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->m:Ljava/lang/Runnable;

    .line 90
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/s;->i:Z

    .line 94
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/s;->n:Z

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/s;->s:Z

    .line 104
    iput v1, p0, Lcom/google/android/gms/games/ui/s;->t:I

    .line 496
    new-instance v0, Lcom/google/android/gms/games/ui/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/w;-><init>(Lcom/google/android/gms/games/ui/s;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->u:Landroid/support/v7/widget/ch;

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->p:Lcom/google/android/gms/games/ui/co;

    .line 114
    return-void
.end method

.method private C()Lcom/google/android/gms/games/ui/ac;
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    .line 307
    if-nez v0, :cond_0

    .line 308
    const/4 v0, 0x0

    .line 311
    :goto_0
    return-object v0

    .line 310
    :cond_0
    instance-of v1, v0, Lcom/google/android/gms/games/ui/ac;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 311
    check-cast v0, Lcom/google/android/gms/games/ui/ac;

    goto :goto_0
.end method

.method private D()V
    .locals 3

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/s;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_1

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 330
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;->C()Lcom/google/android/gms/games/ui/ac;

    move-result-object v0

    .line 331
    if-eqz v0, :cond_0

    .line 335
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->b(Z)V

    .line 336
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/s;->r:Z

    if-nez v1, :cond_0

    .line 337
    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lcom/google/android/gms/games/ui/u;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/games/ui/u;-><init>(Lcom/google/android/gms/games/ui/s;Lcom/google/android/gms/games/ui/ac;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private E()V
    .locals 3

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 457
    :goto_0
    return-void

    .line 393
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/s;->getView()Landroid/view/View;

    move-result-object v0

    .line 394
    if-nez v0, :cond_1

    .line 395
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Content view not yet created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 397
    :cond_1
    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_4

    .line 398
    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    .line 412
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->a()V

    .line 414
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->c(Landroid/view/View;)V

    .line 415
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/s;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 416
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->u:Landroid/support/v7/widget/ch;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/ch;)V

    .line 422
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->c()Landroid/support/v7/widget/ce;

    move-result-object v0

    if-nez v0, :cond_3

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/am;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v1, v2}, Landroid/support/v7/widget/am;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/ce;)V

    .line 427
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/google/android/gms/games/ui/v;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/v;-><init>(Lcom/google/android/gms/games/ui/s;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cc;)V

    .line 456
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 400
    :cond_4
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 401
    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_6

    .line 402
    if-nez v0, :cond_5

    .line 403
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your content must have a RecyclerView whose id attribute is android.R.id.list\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 406
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Content has view with id attribute \'android.R.id.list\' that is not a RecyclerView class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 409
    :cond_6
    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/s;)Landroid/support/v7/widget/ch;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->o:Landroid/support/v7/widget/ch;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/games/ui/cn;)Lcom/google/android/gms/games/ui/cp;
    .locals 4

    .prologue
    .line 750
    new-instance v0, Lcom/google/android/gms/games/ui/cp;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    invoke-interface {p1, v2}, Lcom/google/android/gms/games/ui/cn;->a(Landroid/content/Context;)I

    move-result v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/cp;-><init>(Landroid/content/Context;IZ)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/s;Z)Z
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/s;->n:Z

    return p1
.end method

.method private b(II)Lcom/google/android/gms/games/ui/cp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 764
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/s;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 768
    if-eqz p1, :cond_1

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 771
    :goto_0
    if-eqz p2, :cond_0

    .line 772
    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 773
    sub-int v0, v2, v0

    .line 776
    :cond_0
    new-instance v2, Lcom/google/android/gms/games/ui/cp;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/gms/games/ui/cp;-><init>(Landroid/content/Context;IZ)V

    return-object v2

    :cond_1
    move v0, v1

    .line 768
    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 168
    sget v0, Lcom/google/android/gms/j;->sm:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/s;->i:Z

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lcom/google/android/gms/f;->y:I

    aput v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lcom/google/android/gms/f;->A:I

    aput v3, v1, v2

    const/4 v2, 0x2

    sget v3, Lcom/google/android/gms/f;->B:I

    aput v3, v1, v2

    const/4 v2, 0x3

    sget v3, Lcom/google/android/gms/f;->z:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a([I)V

    .line 174
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/s;->i:Z

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->k:Landroid/support/v4/widget/bp;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->k:Landroid/support/v4/widget/bp;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/support/v4/widget/bp;)V

    .line 181
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/cn;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_1

    .line 183
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/s;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    .line 184
    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/cn;->a(Landroid/content/Context;)I

    move-result v0

    .line 185
    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 186
    sget v2, Lcom/google/android/gms/g;->aF:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v1, v0

    .line 188
    iget-object v2, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(II)V

    .line 190
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/s;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/s;->n:Z

    return v0
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->h:Lcom/google/android/gms/games/ui/d/p;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/p;->a()I

    move-result v0

    .line 782
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 785
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/cn;

    move-result-object v0

    .line 786
    if-eqz v0, :cond_1

    .line 787
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cn;->s()F

    move-result v0

    .line 788
    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->h:Lcom/google/android/gms/games/ui/d/p;

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/d/p;->a(I)V

    .line 791
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 211
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/p;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 212
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/s;->b(Landroid/view/View;)V

    .line 213
    return-object v0
.end method

.method protected a(Landroid/view/View;)Lcom/google/android/gms/games/ui/d/p;
    .locals 11

    .prologue
    .line 154
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/cn;

    move-result-object v7

    .line 155
    if-eqz v7, :cond_0

    .line 156
    new-instance v0, Lcom/google/android/gms/games/ui/d/p;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    const v2, 0x102000a

    sget v3, Lcom/google/android/gms/j;->ln:I

    sget v4, Lcom/google/android/gms/j;->fw:I

    sget v5, Lcom/google/android/gms/j;->mv:I

    sget v6, Lcom/google/android/gms/j;->hc:I

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/s;->getActivity()Landroid/support/v4/app/q;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/google/android/gms/games/ui/cn;->a(Landroid/content/Context;)I

    move-result v10

    move-object v7, p0

    move-object v8, p0

    move-object v9, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/ui/d/p;-><init>(Landroid/view/View;IIIIILcom/google/android/gms/games/ui/d/r;Lcom/google/android/gms/games/ui/d/q;Lcom/google/android/gms/games/ui/d/s;I)V

    .line 162
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/d/p;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    invoke-direct {v0, v1, p0, p0, p0}, Lcom/google/android/gms/games/ui/d/p;-><init>(Landroid/view/View;Lcom/google/android/gms/games/ui/d/r;Lcom/google/android/gms/games/ui/d/q;Lcom/google/android/gms/games/ui/d/s;)V

    goto :goto_0
.end method

.method protected final a(II)V
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->fw:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 639
    sget v0, Lcom/google/android/gms/j;->fu:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 640
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 641
    sget v0, Lcom/google/android/gms/j;->fp:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 642
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 645
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->mv:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 646
    sget v0, Lcom/google/android/gms/j;->mu:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 647
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 648
    sget v0, Lcom/google/android/gms/j;->fD:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 649
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 652
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->hc:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 653
    sget v0, Lcom/google/android/gms/j;->hb:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 654
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 655
    sget v0, Lcom/google/android/gms/j;->ha:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 656
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 657
    return-void
.end method

.method protected final a(III)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 611
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->fu:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 614
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 615
    invoke-virtual {v0, v2, p1, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 618
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->fp:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 619
    if-lez p3, :cond_1

    .line 620
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 621
    invoke-virtual {p0, p3}, Lcom/google/android/gms/games/ui/s;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 622
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/widget/bp;)V
    .locals 2

    .prologue
    .line 694
    iput-object p1, p0, Lcom/google/android/gms/games/ui/s;->k:Landroid/support/v4/widget/bp;

    .line 695
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 696
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->k:Landroid/support/v4/widget/bp;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/support/v4/widget/bp;)V

    .line 698
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/bv;)V
    .locals 4

    .prologue
    .line 252
    instance-of v0, p1, Lcom/google/android/gms/games/ui/ac;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 253
    check-cast p1, Lcom/google/android/gms/games/ui/ac;

    .line 254
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;->E()V

    .line 256
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/ac;->u()I

    move-result v1

    .line 260
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/games/ui/cn;

    move-result-object v2

    .line 261
    if-eqz v2, :cond_2

    .line 263
    instance-of v0, p1, Lcom/google/android/gms/games/ui/bw;

    if-eqz v0, :cond_4

    .line 264
    if-nez v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/s;->t:I

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 265
    check-cast v0, Lcom/google/android/gms/games/ui/bw;

    iget v3, p0, Lcom/google/android/gms/games/ui/s;->t:I

    invoke-direct {p0, v1, v3}, Lcom/google/android/gms/games/ui/s;->b(II)Lcom/google/android/gms/games/ui/cp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/bw;->b(Lcom/google/android/gms/games/ui/ac;)V

    :cond_1
    move-object v0, p1

    .line 268
    check-cast v0, Lcom/google/android/gms/games/ui/bw;

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/s;->a(Lcom/google/android/gms/games/ui/cn;)Lcom/google/android/gms/games/ui/cp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/bw;->b(Lcom/google/android/gms/games/ui/ac;)V

    .line 285
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->c()Landroid/support/v7/widget/ce;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/am;

    .line 286
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/ac;->r()I

    move-result v1

    .line 287
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->a(I)V

    .line 288
    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/bv;)V

    .line 289
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/ac;->q()Landroid/support/v7/widget/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->a(Landroid/support/v7/widget/ap;)V

    .line 292
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 293
    new-instance v0, Lcom/google/android/gms/games/ui/cs;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/cs;-><init>()V

    .line 294
    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/by;)V

    .line 295
    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/ac;->a(Landroid/support/v7/widget/by;)V

    .line 296
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;->D()V

    .line 298
    :cond_3
    return-void

    .line 271
    :cond_4
    new-instance v0, Lcom/google/android/gms/games/ui/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/by;-><init>()V

    .line 272
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/s;->a(Lcom/google/android/gms/games/ui/cn;)Lcom/google/android/gms/games/ui/cp;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 273
    if-nez v1, :cond_5

    iget v2, p0, Lcom/google/android/gms/games/ui/s;->t:I

    if-eqz v2, :cond_6

    .line 274
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/ui/s;->t:I

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/ui/s;->b(II)Lcom/google/android/gms/games/ui/cp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 277
    :cond_6
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 278
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/by;->a()Lcom/google/android/gms/games/ui/bw;

    move-result-object p1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0

    .prologue
    .line 194
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/s;->b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 195
    return-void
.end method

.method public final b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->u:Landroid/support/v7/widget/ch;

    invoke-virtual {p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/support/v7/widget/ch;)V

    .line 207
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->onActivityCreated(Landroid/os/Bundle;)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/s;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/q;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    .line 120
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/gms/games/ui/s;->e:Landroid/view/LayoutInflater;

    .line 140
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/p;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/s;->b(Landroid/view/View;)V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/s;->a(Landroid/view/View;)Lcom/google/android/gms/games/ui/d/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->h:Lcom/google/android/gms/games/ui/d/p;

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->h:Lcom/google/android/gms/games/ui/d/p;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->f:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/games/ui/a;

    if-eqz v1, :cond_0

    .line 228
    check-cast v0, Lcom/google/android/gms/games/ui/a;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/a;->b()V

    .line 230
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    .line 232
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->onDestroyView()V

    .line 233
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->onStart()V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->a(Lcom/google/android/gms/common/api/x;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->a(Lcom/google/android/gms/common/api/y;)V

    .line 127
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->b(Lcom/google/android/gms/common/api/x;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->b(Lcom/google/android/gms/common/api/y;)V

    .line 133
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->onStop()V

    .line 134
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 218
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/p;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 219
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;->E()V

    .line 220
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 377
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 381
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;->C()Lcom/google/android/gms/games/ui/ac;

    move-result-object v0

    .line 382
    if-eqz v0, :cond_0

    .line 383
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->r()I

    move-result v1

    .line 384
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->c()Landroid/support/v7/widget/ce;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/am;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/am;->a(I)V

    .line 386
    :cond_0
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 473
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->setUserVisibleHint(Z)V

    .line 478
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/s;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 479
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;->E()V

    .line 480
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    .line 481
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/games/ui/bq;

    if-eqz v1, :cond_0

    .line 482
    check-cast v0, Lcom/google/android/gms/games/ui/bq;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/ui/bq;->a(ZZ)V

    .line 486
    :cond_0
    return-void
.end method

.method protected final t()V
    .locals 4

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->q:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 565
    :goto_0
    return-void

    .line 549
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/x;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/x;-><init>(Lcom/google/android/gms/games/ui/s;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->q:Landroid/database/ContentObserver;

    .line 562
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 563
    sget-object v1, Lcom/google/android/gms/games/internal/c/a;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/ui/s;->q:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method protected final u()V
    .locals 2

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->q:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 573
    iget-object v1, p0, Lcom/google/android/gms/games/ui/s;->q:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 574
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/s;->q:Landroid/database/ContentObserver;

    .line 576
    :cond_0
    return-void
.end method

.method protected v()V
    .locals 0

    .prologue
    .line 587
    return-void
.end method

.method public w()V
    .locals 0

    .prologue
    .line 596
    return-void
.end method

.method public final x()V
    .locals 0

    .prologue
    .line 600
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;->D()V

    .line 601
    return-void
.end method

.method public final y()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 670
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/s;->i:Z

    .line 671
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 674
    :cond_0
    return-void
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/google/android/gms/games/ui/s;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Z)V

    .line 711
    :cond_0
    return-void
.end method
