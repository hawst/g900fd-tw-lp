.class final Lcom/google/android/gms/ads/internal/request/a/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/g/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/request/a/k;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/a/k;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/a/m;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 6

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/m;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/request/a/k;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 68
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/m;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/a/k;->d:Lcom/google/android/gms/ads/internal/util/j;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/util/j;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    monitor-exit v1

    .line 92
    :goto_0
    return-void

    .line 71
    :cond_0
    new-instance v2, Lcom/google/android/gms/ads/internal/request/a/o;

    const/4 v0, -0x2

    invoke-direct {v2, v0, p2}, Lcom/google/android/gms/ads/internal/request/a/o;-><init>(ILjava/util/Map;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/m;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/a/k;->b:Ljava/lang/String;

    iget-object v3, v2, Lcom/google/android/gms/ads/internal/request/a/o;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 75
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 77
    :cond_1
    :try_start_1
    iget-object v3, v2, Lcom/google/android/gms/ads/internal/request/a/o;->f:Ljava/lang/String;

    .line 78
    if-nez v3, :cond_2

    .line 79
    const-string v0, "URL missing in loadAdUrl GMSG."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 80
    monitor-exit v1

    goto :goto_0

    .line 82
    :cond_2
    const-string v0, "%40mediation_adapters%40"

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v0, "check_adapters"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/request/a/m;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/request/a/k;->c:Ljava/lang/String;

    invoke-static {v4, v0, v5}, Lcom/google/android/gms/ads/internal/o/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87
    const-string v4, "%40mediation_adapters%40"

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    iput-object v0, v2, Lcom/google/android/gms/ads/internal/request/a/o;->f:Ljava/lang/String;

    .line 89
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ad request URL modified to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 91
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/m;->a:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/a/k;->d:Lcom/google/android/gms/ads/internal/util/j;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/ads/internal/util/j;->a(Ljava/lang/Object;)V

    .line 92
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
