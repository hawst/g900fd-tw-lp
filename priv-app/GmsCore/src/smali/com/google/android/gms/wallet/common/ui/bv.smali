.class final Lcom/google/android/gms/wallet/common/ui/bv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/bk;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/bq;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/bq;)V
    .locals 0

    .prologue
    .line 492
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 0

    .prologue
    .line 494
    return-void
.end method

.method public final onAnimationEnd()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 521
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->d(Lcom/google/android/gms/wallet/common/ui/bq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->j()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 523
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/bq;->a(Lcom/google/android/gms/wallet/common/ui/bq;Z)V

    .line 528
    :cond_0
    :goto_0
    return-void

    .line 524
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->j()I

    move-result v0

    if-nez v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method

.method public final onAnimationStart()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 498
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->d(Lcom/google/android/gms/wallet/common/ui/bq;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 499
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 500
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/bq;->a(Lcom/google/android/gms/wallet/common/ui/bq;Z)V

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->j()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 502
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/common/ui/bq;->b(Lcom/google/android/gms/wallet/common/ui/bq;Z)V

    .line 503
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 504
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 505
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 517
    :cond_1
    :goto_0
    return-void

    .line 508
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->j()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 509
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 510
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 511
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 512
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 514
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bv;->a:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/bq;->b(Lcom/google/android/gms/wallet/common/ui/bq;Z)V

    goto :goto_0
.end method
