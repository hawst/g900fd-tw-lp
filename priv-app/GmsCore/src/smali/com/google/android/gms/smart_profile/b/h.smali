.class public final Lcom/google/android/gms/smart_profile/b/h;
.super Landroid/support/v4/a/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/ao;


# instance fields
.field private a:[Lcom/google/c/f/c/a/i;

.field private b:Lcom/google/android/gms/plus/internal/ab;

.field private c:Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/internal/ab;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/support/v4/a/j;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/b/h;->b:Lcom/google/android/gms/plus/internal/ab;

    .line 41
    iput p3, p0, Lcom/google/android/gms/smart_profile/b/h;->d:I

    .line 42
    iput-object p4, p0, Lcom/google/android/gms/smart_profile/b/h;->c:Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;

    .line 43
    return-void
.end method

.method private a([Lcom/google/c/f/c/a/i;)V
    .locals 1

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/b/h;->a:[Lcom/google/c/f/c/a/i;

    .line 81
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 82
    invoke-super {p0, p1}, Landroid/support/v4/a/j;->b(Ljava/lang/Object;)V

    .line 84
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    .line 60
    invoke-super {p0}, Landroid/support/v4/a/j;->a()V

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->b:Lcom/google/android/gms/plus/internal/ab;

    iget v1, p0, Lcom/google/android/gms/smart_profile/b/h;->d:I

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/b/h;->c:Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ao;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)V

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;)V
    .locals 2

    .prologue
    .line 89
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;->b()[B

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 92
    invoke-static {v0}, Lcom/google/android/gms/smart_profile/ar;->c([B)Lcom/google/c/f/c/a/b/c;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_0

    .line 95
    iget-object v0, v0, Lcom/google/c/f/c/a/b/c;->a:Lcom/google/c/f/c/a/b/b;

    iget-object v0, v0, Lcom/google/c/f/c/a/b/b;->b:[Lcom/google/c/f/c/a/i;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->a:[Lcom/google/c/f/c/a/i;

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->a:[Lcom/google/c/f/c/a/i;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/b/h;->a([Lcom/google/c/f/c/a/i;)V

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->a:[Lcom/google/c/f/c/a/i;

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->a:[Lcom/google/c/f/c/a/i;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/b/h;->a([Lcom/google/c/f/c/a/i;)V

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, [Lcom/google/c/f/c/a/i;

    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/b/h;->a([Lcom/google/c/f/c/a/i;)V

    return-void
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Landroid/support/v4/a/j;->e()V

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->a:[Lcom/google/c/f/c/a/i;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->a:[Lcom/google/c/f/c/a/i;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/b/h;->a([Lcom/google/c/f/c/a/i;)V

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/h;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->a:[Lcom/google/c/f/c/a/i;

    if-nez v0, :cond_2

    .line 54
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 56
    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Landroid/support/v4/a/j;->g()V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/h;->f()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/h;->a:[Lcom/google/c/f/c/a/i;

    .line 75
    return-void
.end method
