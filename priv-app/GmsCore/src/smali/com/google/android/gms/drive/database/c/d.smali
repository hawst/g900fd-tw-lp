.class public final Lcom/google/android/gms/drive/database/c/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/drive/database/c/a;

.field public b:Z

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/util/Collection;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-boolean v1, p0, Lcom/google/android/gms/drive/database/c/d;->c:Z

    .line 23
    new-instance v0, Lcom/google/android/gms/drive/database/c/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->a:Lcom/google/android/gms/drive/database/c/a;

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->d:Ljava/lang/String;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->e:Ljava/util/Collection;

    .line 26
    iput-boolean v1, p0, Lcom/google/android/gms/drive/database/c/d;->b:Z

    .line 29
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->d:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/gms/drive/database/c/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/database/c/d;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/database/c/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;
    .locals 6

    .prologue
    .line 120
    iget-object v2, p0, Lcom/google/android/gms/drive/database/c/d;->a:Lcom/google/android/gms/drive/database/c/a;

    iget-object v0, p1, Lcom/google/android/gms/drive/database/c/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/drive/database/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    goto :goto_0

    .line 121
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/drive/database/c/b;)Lcom/google/android/gms/drive/database/c/d;
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->e:Ljava/util/Collection;

    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 129
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/drive/g/ak;)Lcom/google/android/gms/drive/database/c/d;
    .locals 1

    .prologue
    .line 82
    invoke-interface {p1}, Lcom/google/android/gms/drive/g/ak;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/drive/database/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->a:Lcom/google/android/gms/drive/database/c/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/drive/database/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    .line 90
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 137
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "SELECT "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 138
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/c/d;->c:Z

    if-eqz v0, :cond_0

    .line 139
    const-string v0, "DISTINCT "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/c/d;->b:Z

    if-eqz v0, :cond_1

    .line 142
    const-string v0, "*, "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->a:Lcom/google/android/gms/drive/database/c/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/c/a;->a()Lcom/google/android/gms/drive/database/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/c/a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :goto_0
    const-string v0, " FROM "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/drive/database/c/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->e:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/c/b;

    .line 150
    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/c/b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/c/d;->a:Lcom/google/android/gms/drive/database/c/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/c/a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 153
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
