.class final Lcom/google/android/gms/wallet/service/ow/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Ljava/util/concurrent/Semaphore;

.field b:[Lcom/google/android/gms/wallet/service/ow/j;


# direct methods
.method varargs constructor <init>([Lcom/google/android/gms/wallet/service/ow/j;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/o;->a:Ljava/util/concurrent/Semaphore;

    .line 289
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/wallet/service/ow/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/o;->b:[Lcom/google/android/gms/wallet/service/ow/j;

    .line 290
    array-length v2, p1

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v1, p1, v0

    .line 291
    invoke-virtual {v1, p0}, Lcom/google/android/gms/wallet/service/ow/j;->a(Ljava/lang/Runnable;)V

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 293
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wallet/service/ow/j;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/o;->b:[Lcom/google/android/gms/wallet/service/ow/j;

    array-length v0, v0

    .line 300
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/o;->b:[Lcom/google/android/gms/wallet/service/ow/j;

    aget-object v1, v1, v3

    move v4, v0

    .line 301
    :cond_0
    :goto_0
    if-lez v4, :cond_3

    .line 304
    const-string v0, "FullWalletRequester"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Acquiring semaphore, numRemainingTasks = "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/o;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/o;->b:[Lcom/google/android/gms/wallet/service/ow/j;

    array-length v5, v0

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_0

    .line 307
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/o;->b:[Lcom/google/android/gms/wallet/service/ow/j;

    aget-object v0, v0, v2

    .line 308
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/j;->f()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 309
    const-string v5, "FullWalletRequester"

    const-string v6, "one task done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    add-int/lit8 v4, v4, -0x1

    .line 311
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/j;->i()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 319
    :goto_2
    return-object v0

    .line 314
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/o;->b:[Lcom/google/android/gms/wallet/service/ow/j;

    const/4 v5, 0x0

    aput-object v5, v0, v2

    goto :goto_0

    .line 306
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 319
    goto :goto_2
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/o;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 325
    const-string v0, "FullWalletRequester"

    const-string v1, "Released semaphore"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    return-void
.end method
