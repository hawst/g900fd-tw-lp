.class public final Lcom/google/android/gms/plus/service/a/l;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/c;

.field private final c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;Lcom/google/android/gms/plus/internal/c;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/l;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/l;->c:Ljava/util/List;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/l;->b:Lcom/google/android/gms/plus/internal/c;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/l;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/l;->c:Ljava/util/List;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/l;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3

    .line 61
    :goto_0
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 46
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v4, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 47
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/l;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-static {v5, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/l;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/l;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-static {v5, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/l;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :catch_3
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/l;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/plus/internal/c;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/l;->b:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/l;->b:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V

    .line 69
    :cond_0
    return-void
.end method
