.class final Lcom/google/android/gms/plus/service/j;
.super Lcom/google/android/gms/plus/internal/j;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/plus/service/k;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:Lcom/google/android/gms/plus/service/i;

.field private final e:[Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/i;[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 576
    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/j;-><init>()V

    .line 577
    iput-object p1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    .line 578
    iput-object p3, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    .line 579
    iput-object p4, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    .line 580
    iput-object p5, p0, Lcom/google/android/gms/plus/service/j;->e:[Ljava/lang/String;

    .line 581
    new-instance v0, Lcom/google/android/gms/plus/service/k;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/service/k;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/i;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/j;->a:Lcom/google/android/gms/plus/service/k;

    .line 583
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 846
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->a:Lcom/google/android/gms/plus/service/k;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/k;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;)V
    .locals 2

    .prologue
    .line 841
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->a:Lcom/google/android/gms/plus/service/k;

    new-instance v1, Lcom/google/android/gms/plus/service/h;

    invoke-direct {v1, p1}, Lcom/google/android/gms/plus/service/h;-><init>(Lcom/google/android/gms/plus/internal/f;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/k;->a(Lcom/google/android/gms/plus/internal/c;)V

    .line 842
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;IILjava/lang/String;)V
    .locals 7

    .prologue
    .line 717
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->b()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 718
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/plus/service/a/ah;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/service/a/ah;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v6, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 721
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)V
    .locals 3

    .prologue
    .line 882
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 884
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/ad;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p3, p1}, Lcom/google/android/gms/plus/service/a/ad;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 886
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/z;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p3, p1}, Lcom/google/android/gms/plus/service/a/z;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 712
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->a:Lcom/google/android/gms/plus/service/k;

    new-instance v1, Lcom/google/android/gms/plus/service/h;

    invoke-direct {v1, p1}, Lcom/google/android/gms/plus/service/h;-><init>(Lcom/google/android/gms/plus/internal/f;)V

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/gms/plus/service/k;->a(Lcom/google/android/gms/plus/internal/c;Landroid/net/Uri;Landroid/os/Bundle;)V

    .line 837
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 3

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/i;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p1}, Lcom/google/android/gms/plus/service/a/i;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 641
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)V
    .locals 3

    .prologue
    .line 872
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 874
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/p;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p1}, Lcom/google/android/gms/plus/service/a/p;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 876
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 2

    .prologue
    .line 603
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 605
    new-instance v0, Lcom/google/android/gms/plus/service/a/r;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/gms/plus/service/a/r;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Comment;)V

    .line 607
    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 608
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 2

    .prologue
    .line 613
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 615
    new-instance v0, Lcom/google/android/gms/plus/service/a/q;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/gms/plus/service/a/q;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V

    .line 617
    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 618
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->a:Lcom/google/android/gms/plus/service/k;

    new-instance v1, Lcom/google/android/gms/plus/service/h;

    invoke-direct {v1, p1}, Lcom/google/android/gms/plus/service/h;-><init>(Lcom/google/android/gms/plus/internal/f;)V

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/plus/service/k;->a(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V

    .line 588
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 798
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 800
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 801
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The appId parameter is required."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 803
    new-instance v0, Lcom/google/android/gms/plus/service/a/t;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/service/a/t;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/f;)V

    .line 805
    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 806
    return-void

    .line 801
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 786
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 788
    new-instance v0, Lcom/google/android/gms/plus/service/a/u;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    const-string v6, "me"

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v7, p2

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/service/a/u;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/f;)V

    .line 791
    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 792
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 3

    .prologue
    .line 666
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/an;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p3, p1}, Lcom/google/android/gms/plus/service/a/an;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 668
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;)V
    .locals 3

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/h;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p1, p3}, Lcom/google/android/gms/plus/service/a/h;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 661
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZ)V
    .locals 11

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v10, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/plus/service/a/am;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/service/a/am;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZLcom/google/android/gms/plus/internal/f;)V

    invoke-static {v10, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 677
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 3

    .prologue
    .line 852
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/ar;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p3, p1}, Lcom/google/android/gms/plus/service/a/ar;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 854
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->a:Lcom/google/android/gms/plus/service/k;

    new-instance v1, Lcom/google/android/gms/plus/service/h;

    invoke-direct {v1, p1}, Lcom/google/android/gms/plus/service/h;-><init>(Lcom/google/android/gms/plus/internal/f;)V

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/gms/plus/service/k;->a(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 9

    .prologue
    .line 741
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 742
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v8, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/plus/service/a/j;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/j;->e:[Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/plus/service/f;->a:Lcom/google/android/gms/plus/service/f;

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/service/a/j;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/f;[Ljava/lang/String;Lcom/google/android/gms/plus/service/f;)V

    invoke-static {v8, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 746
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 7

    .prologue
    .line 693
    if-eqz p5, :cond_0

    .line 694
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/e;

    invoke-direct {v1, p5}, Lcom/google/android/gms/plus/service/a/e;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/plus/service/a/g;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/service/a/g;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/plus/internal/f;)V

    invoke-static {v6, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 699
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 682
    if-eqz p4, :cond_0

    .line 683
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/e;

    invoke-direct {v1, p4}, Lcom/google/android/gms/plus/service/a/e;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 686
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/g;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p3, p1}, Lcom/google/android/gms/plus/service/a/g;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 688
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/f;ZZ)V
    .locals 2

    .prologue
    .line 704
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not implemented."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 729
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 730
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/aq;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/plus/service/a/aq;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 732
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/internal/f;)V
    .locals 2

    .prologue
    .line 763
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 765
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 766
    new-instance v0, Lcom/google/android/gms/plus/service/a/k;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/plus/service/a/k;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/f;)V

    .line 768
    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 769
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/internal/f;IILjava/lang/String;)V
    .locals 6

    .prologue
    .line 860
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 862
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 863
    new-instance v0, Lcom/google/android/gms/plus/service/a/w;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/service/a/w;-><init>(Lcom/google/android/gms/common/server/ClientContext;IILjava/lang/String;Lcom/google/android/gms/plus/internal/f;)V

    .line 865
    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 866
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 3

    .prologue
    .line 646
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/ap;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p1}, Lcom/google/android/gms/plus/service/a/ap;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 648
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    .prologue
    .line 623
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 625
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/ag;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/plus/service/a/ag;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 627
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->a:Lcom/google/android/gms/plus/service/k;

    new-instance v1, Lcom/google/android/gms/plus/service/h;

    invoke-direct {v1, p1}, Lcom/google/android/gms/plus/service/h;-><init>(Lcom/google/android/gms/plus/internal/f;)V

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/plus/service/k;->b(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V

    .line 598
    return-void
.end method

.method public final c(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 3

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/ao;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p2, p1}, Lcom/google/android/gms/plus/service/a/ao;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 655
    return-void
.end method

.method public final c(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    .prologue
    .line 631
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 632
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/af;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/plus/service/a/af;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 634
    return-void
.end method

.method public final c(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 774
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 777
    new-instance v0, Lcom/google/android/gms/plus/service/a/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/a/ab;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/f;)V

    .line 779
    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 780
    return-void
.end method

.method public final d(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 755
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 756
    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/ak;

    invoke-direct {v1, p2, p1}, Lcom/google/android/gms/plus/service/a/ak;-><init>(Ljava/lang/String;Lcom/google/android/gms/plus/internal/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 758
    return-void
.end method

.method public final e(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 824
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 826
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 827
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The momentId parameter is required."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 829
    new-instance v0, Lcom/google/android/gms/plus/service/a/ai;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/a/ai;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/f;)V

    .line 831
    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 832
    return-void

    .line 827
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 811
    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    .line 813
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 814
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The momentId parameter is required."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 816
    new-instance v0, Lcom/google/android/gms/plus/service/a/ac;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/a/ac;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/f;)V

    .line 818
    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->d:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/j;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 819
    return-void

    .line 814
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
