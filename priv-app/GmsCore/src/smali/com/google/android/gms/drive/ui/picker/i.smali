.class public final Lcom/google/android/gms/drive/ui/picker/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Intent;

.field private final b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->b:Ljava/util/ArrayList;

    .line 56
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 58
    const-string v0, "Account Name not specified"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 59
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "Authorized app not specified"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    const-string v1, "callerPackagingId"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    const-string v1, "callerPackageName"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/ui/picker/i;
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    const-string v1, "showNewFolder"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 82
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/ui/picker/i;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    const-string v1, "driveId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 77
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/i;
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    const-string v1, "dialogTitle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    return-object p0
.end method

.method public final a([Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/i;
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    const-string v1, "mimeTypes"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    return-object p0
.end method
