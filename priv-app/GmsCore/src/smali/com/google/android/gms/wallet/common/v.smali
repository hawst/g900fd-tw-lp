.class final Lcom/google/android/gms/wallet/common/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    new-instance v3, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v3}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->a:Z

    const-class v0, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    const-class v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    const-class v0, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    const-class v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lcom/google/checkout/inapp/proto/a/b;

    const-class v0, Lcom/google/aa/b/a/h;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/h;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Lcom/google/aa/b/a/h;

    return-object v3

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 129
    new-array v0, p1, [Lcom/google/android/gms/wallet/common/PaymentModel;

    return-object v0
.end method
