.class public final enum Lcom/google/android/gms/drive/database/model/v;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/v;

.field private static final synthetic c:[Lcom/google/android/gms/drive/database/model/v;


# instance fields
.field private final b:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 47
    new-instance v0, Lcom/google/android/gms/drive/database/model/v;

    const-string v1, "FILENAME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/u;->d()Lcom/google/android/gms/drive/database/model/u;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/4 v3, 0x2

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "filename"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    new-array v5, v7, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    iput-boolean v8, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/model/v;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/v;->a:Lcom/google/android/gms/drive/database/model/v;

    .line 43
    new-array v0, v8, [Lcom/google/android/gms/drive/database/model/v;

    sget-object v1, Lcom/google/android/gms/drive/database/model/v;->a:Lcom/google/android/gms/drive/database/model/v;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/gms/drive/database/model/v;->c:[Lcom/google/android/gms/drive/database/model/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/v;->b:Lcom/google/android/gms/drive/database/model/ab;

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/v;
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/gms/drive/database/model/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/v;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/v;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/gms/drive/database/model/v;->c:[Lcom/google/android/gms/drive/database/model/v;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/v;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/v;->b:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/v;->b:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
