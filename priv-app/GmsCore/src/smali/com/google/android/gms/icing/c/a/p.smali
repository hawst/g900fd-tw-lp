.class public final Lcom/google/android/gms/icing/c/a/p;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1945
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1946
    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/p;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/p;->cachedSize:I

    .line 1947
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2006
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2007
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/p;->a:Z

    if-eqz v1, :cond_0

    .line 2008
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/icing/c/a/p;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2011
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/p;->b:Z

    if-eqz v1, :cond_1

    .line 2012
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/icing/c/a/p;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2015
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/p;->c:Z

    if-eqz v1, :cond_2

    .line 2016
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/icing/c/a/p;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2019
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1960
    if-ne p1, p0, :cond_1

    .line 1961
    const/4 v0, 0x1

    .line 1976
    :cond_0
    :goto_0
    return v0

    .line 1963
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/p;

    if-eqz v1, :cond_0

    .line 1966
    check-cast p1, Lcom/google/android/gms/icing/c/a/p;

    .line 1967
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/p;->a:Z

    iget-boolean v2, p1, Lcom/google/android/gms/icing/c/a/p;->a:Z

    if-ne v1, v2, :cond_0

    .line 1970
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/p;->b:Z

    iget-boolean v2, p1, Lcom/google/android/gms/icing/c/a/p;->b:Z

    if-ne v1, v2, :cond_0

    .line 1973
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/p;->c:Z

    iget-boolean v2, p1, Lcom/google/android/gms/icing/c/a/p;->c:Z

    if-ne v1, v2, :cond_0

    .line 1976
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/p;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 1981
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1983
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 1984
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/gms/icing/c/a/p;->c:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1985
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/p;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1986
    return v0

    :cond_0
    move v0, v2

    .line 1981
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1983
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1984
    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1919
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/p;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->c:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1992
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->a:Z

    if-eqz v0, :cond_0

    .line 1993
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/p;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1995
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->b:Z

    if-eqz v0, :cond_1

    .line 1996
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/p;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1998
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/p;->c:Z

    if-eqz v0, :cond_2

    .line 1999
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/p;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2001
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2002
    return-void
.end method
