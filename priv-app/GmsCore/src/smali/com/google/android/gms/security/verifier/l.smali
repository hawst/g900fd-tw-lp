.class public final Lcom/google/android/gms/security/verifier/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/android/gms/security/verifier/n;

.field public c:Lcom/google/android/gms/security/verifier/m;

.field public d:[B

.field public e:Lcom/google/android/gms/security/verifier/v;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1974
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1975
    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->d:[B

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/verifier/l;->cachedSize:I

    .line 1976
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2073
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2074
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2075
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2078
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    if-eqz v1, :cond_1

    .line 2079
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2082
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    if-eqz v1, :cond_2

    .line 2083
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2086
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->d:[B

    if-eqz v1, :cond_3

    .line 2087
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->d:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2090
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    if-eqz v1, :cond_4

    .line 2091
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2094
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1990
    if-ne p1, p0, :cond_1

    .line 2033
    :cond_0
    :goto_0
    return v0

    .line 1993
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/verifier/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 1994
    goto :goto_0

    .line 1996
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/verifier/l;

    .line 1997
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    if-nez v2, :cond_3

    .line 1998
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1999
    goto :goto_0

    .line 2001
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2002
    goto :goto_0

    .line 2003
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    if-nez v2, :cond_5

    .line 2004
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2005
    goto :goto_0

    .line 2008
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/security/verifier/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2009
    goto :goto_0

    .line 2012
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    if-nez v2, :cond_7

    .line 2013
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    if-eqz v2, :cond_8

    move v0, v1

    .line 2014
    goto :goto_0

    .line 2017
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/security/verifier/m;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 2018
    goto :goto_0

    .line 2021
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->d:[B

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/l;->d:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 2022
    goto :goto_0

    .line 2024
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    if-nez v2, :cond_a

    .line 2025
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2026
    goto :goto_0

    .line 2029
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/security/verifier/v;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2030
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2038
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2040
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2042
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2044
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->d:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 2045
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2047
    return v0

    .line 2038
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 2040
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/n;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2042
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/m;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2045
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    invoke-virtual {v1}, Lcom/google/android/gms/security/verifier/v;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1826
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/security/verifier/n;

    invoke-direct {v0}, Lcom/google/android/gms/security/verifier/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/security/verifier/m;

    invoke-direct {v0}, Lcom/google/android/gms/security/verifier/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->d:[B

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/security/verifier/v;

    invoke-direct {v0}, Lcom/google/android/gms/security/verifier/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2053
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2054
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2056
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    if-eqz v0, :cond_1

    .line 2057
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2059
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    if-eqz v0, :cond_2

    .line 2060
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->c:Lcom/google/android/gms/security/verifier/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2062
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->d:[B

    if-eqz v0, :cond_3

    .line 2063
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->d:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 2065
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    if-eqz v0, :cond_4

    .line 2066
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2068
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2069
    return-void
.end method
