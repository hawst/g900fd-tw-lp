.class public final Lcom/google/android/gms/people/sync/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/people/a/c;

.field private final c:Ljava/lang/Object;

.field private final d:Lcom/google/android/gms/common/util/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/u;->c:Ljava/lang/Object;

    .line 153
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/util/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/u;->d:Lcom/google/android/gms/common/util/p;

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    .line 156
    return-void
.end method

.method static a(Ljava/lang/Exception;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 764
    instance-of v1, p0, Lcom/android/volley/ac;

    if-eqz v1, :cond_0

    .line 765
    check-cast p0, Lcom/android/volley/ac;

    .line 766
    iget-object v1, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    .line 768
    :cond_0
    return v0
.end method

.method public static a(ZZ)Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 458
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 462
    const-string v1, "ignore_settings"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 464
    if-eqz p0, :cond_0

    .line 465
    const-string v1, "ignore_backoff"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 468
    :cond_0
    if-eqz p1, :cond_1

    .line 469
    const-string v1, "expedited"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 472
    :cond_1
    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 722
    if-eqz p0, :cond_0

    const-string v0, "feed"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/os/Bundle;)I
    .locals 2

    .prologue
    .line 726
    invoke-static {p0}, Lcom/google/android/gms/people/sync/u;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 727
    const-string v1, "plusfeed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 728
    const/4 v0, 0x1

    .line 734
    :goto_0
    return v0

    .line 730
    :cond_0
    const-string v1, "pluspageadmin"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 731
    const/4 v0, 0x2

    goto :goto_0

    .line 734
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/accounts/Account;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 989
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;

    invoke-static {p1, p2}, Lcom/google/android/gms/people/sync/a;->a(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/os/Bundle;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 747
    if-eqz p0, :cond_2

    .line 748
    invoke-static {p0}, Lcom/google/android/gms/people/sync/u;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 749
    const/4 v0, 0x1

    .line 755
    :cond_0
    :goto_0
    return v0

    .line 751
    :cond_1
    const-string v1, "periodic_sync"

    invoke-virtual {p0, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 755
    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 1050
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;

    invoke-static {}, Lcom/google/android/gms/people/sync/a;->b()Z

    move-result v0

    return v0
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 488
    iget-object v2, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v2

    .line 490
    :try_start_0
    const-string v3, "SELECT last_successful_sync_time FROM owners WHERE _id=?"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 497
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)Ljava/util/List;
    .locals 8

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    .line 609
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 610
    const-string v2, "SELECT page_gaia_id FROM owners WHERE account_name=? AND page_gaia_id IS NOT NULL  AND last_successful_sync_time<? ORDER BY last_successful_sync_time"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/u;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, p2

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 620
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 624
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 626
    return-object v1
.end method

.method public final a()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-string v2, "PeopleSync"

    const-string v3, "requestAllFullSyncForDbUpgrade"

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/people/debug/c;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/o;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 674
    const-string v5, "com.google.android.gms.people"

    invoke-static {v1, v1}, Lcom/google/android/gms/people/sync/u;->a(ZZ)Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 673
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 677
    :cond_0
    return-void
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 300
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    sget-object v0, Lcom/google/android/gms/people/a/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 305
    const-string v1, "com.google.android.gms.people"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/people/sync/a;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 308
    const-string v1, "com.google.android.gms.people"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "periodic_sync"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    int-to-long v4, v0

    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;

    invoke-static {p1, v1, v2, v4, v5}, Lcom/google/android/gms/people/sync/a;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.google.android.gms.people"

    const-string v2, "plusupdates"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "plusfeed"

    aput-object v5, v3, v4

    const-string v4, "pluspageadmin"

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;

    invoke-static {v0, p1, v1, v2, v3}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/ContentResolver;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    .line 315
    const-string v1, "PeopleSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting subscription: result="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 317
    if-nez v0, :cond_0

    .line 318
    const-string v0, "PeopleSync"

    const-string v1, "Unable to subscribe to feed."

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :cond_0
    return-void
.end method

.method public final a(Landroid/accounts/Account;ILandroid/os/Bundle;ILjava/lang/Exception;Ljava/util/List;JLcom/google/android/gms/people/d/p;)V
    .locals 19

    .prologue
    .line 858
    const/4 v1, 0x1

    move/from16 v0, p4

    if-eq v0, v1, :cond_1

    .line 859
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/a/c;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const/4 v3, 0x1

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v2, Landroid/support/v4/app/bk;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    const v3, 0x1080078

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v2

    const-string v3, "People Details sync failed"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    const-string v3, "[%d] %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    iput-object v1, v2, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-virtual {v2}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-string v3, "notification"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 861
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/ah;->k()Lcom/google/android/gms/people/f/b;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/people/sync/u;->c(Landroid/os/Bundle;)I

    move-result v5

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/people/sync/u;->b(Landroid/os/Bundle;)I

    move-result v6

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/people/sync/z;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {p5 .. p5}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/Exception;)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lcom/google/android/gms/people/a/c;->b(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v8}, Lcom/google/android/gms/people/a/c;->a(Ljava/lang/String;)I

    move-result v4

    int-to-long v12, v4

    move/from16 v4, p2

    move/from16 v8, p4

    move-object/from16 v10, p5

    move-object/from16 v14, p6

    move-wide/from16 v15, p7

    move-object/from16 v17, p9

    invoke-virtual/range {v1 .. v17}, Lcom/google/android/gms/people/f/b;->a(Landroid/content/Context;Ljava/lang/String;IIILjava/lang/String;IILjava/lang/Throwable;IJLjava/util/List;JLcom/google/android/gms/people/d/p;)V

    .line 868
    return-void

    .line 859
    :pswitch_0
    const/4 v1, 0x0

    goto/16 :goto_0

    :pswitch_1
    sget-object v1, Lcom/google/android/gms/people/a/a;->aw:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    return-void

    .line 176
    :cond_0
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method final a(Landroid/accounts/Account;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/people/sync/a;->a(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 1026
    return-void
.end method

.method public final a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/people/sync/a;->b(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1043
    return-void
.end method

.method final a(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/a;->a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/people/sync/a;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 1008
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-string v1, "PeopleSync"

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    .line 634
    const-string v1, "SELECT count(1) FROM owners WHERE account_name=? AND page_gaia_id IS NOT NULL"

    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/people/a/a;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    .line 519
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/people/sync/u;->f(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 520
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 521
    const-string v1, "PeopleSync"

    const-string v2, "requestSync: Owner doesn\'t exist"

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    :cond_0
    :goto_0
    return v0

    .line 525
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/people/sync/u;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, p3

    sub-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 527
    :goto_1
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 528
    const-string v1, "PeopleSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "areDataFresh, last successful sync="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " fresh="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " account="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " page="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " allowance="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 525
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;)Z
    .locals 11

    .prologue
    .line 393
    const/4 v2, 0x0

    .line 394
    if-eqz p9, :cond_10

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/people/sync/u;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 395
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 396
    const-string v2, "PeopleSync"

    const-string v3, "Not performing requestSync since background sync is enabled."

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :cond_0
    const/4 v2, 0x1

    move v4, v2

    .line 402
    :goto_0
    const/4 v2, 0x1

    .line 403
    const-wide/16 v6, 0x0

    cmp-long v3, p3, v6

    if-eqz v3, :cond_f

    .line 404
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 405
    const/4 v2, 0x0

    move v3, v2

    .line 409
    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-string v6, "PeopleSync"

    const-string v7, "###Sync requested: allowance=%d, needFreshData=%d, ignoreBackoff=%d, skipBecauseOfBackgroundSync=%d, isDisabledByBackgroundSync=%d, client=%s/%s, reason=%s responseMasking=%s"

    const/16 v2, 0x9

    new-array v8, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v2

    const/4 v9, 0x1

    if-eqz v3, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x2

    if-eqz p7, :cond_4

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x3

    if-eqz v4, :cond_5

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v9, 0x4

    if-eqz p9, :cond_6

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v2, 0x5

    aput-object p5, v8, v2

    const/4 v2, 0x6

    aput-object p6, v8, v2

    const/4 v2, 0x7

    aput-object p10, v8, v2

    const/16 v2, 0x8

    sget-object v9, Lcom/google/android/gms/people/a/a;->aH:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v9}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v6, p1, p2, v2}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    iget-object v2, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/ah;->k()Lcom/google/android/gms/people/f/b;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/people/a/a;->aH:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    sget-object v2, Lcom/google/android/gms/people/internal/at;->k:Ljava/security/SecureRandom;

    invoke-virtual {v2}, Ljava/security/SecureRandom;->nextFloat()F

    move-result v8

    sget-object v2, Lcom/google/android/gms/people/a/a;->au:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v2, v8, v2

    if-gez v2, :cond_1

    new-instance v2, Lcom/google/android/gms/people/d/n;

    invoke-direct {v2}, Lcom/google/android/gms/people/d/n;-><init>()V

    invoke-static {v6, p1, p2}, Lcom/google/android/gms/people/f/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/d/r;

    move-result-object v8

    iput-object v8, v2, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    invoke-static/range {p5 .. p5}, Lcom/google/android/gms/people/f/b;->a(Ljava/lang/String;)I

    move-result v8

    iput v8, v2, Lcom/google/android/gms/people/d/n;->b:I

    invoke-static/range {p6 .. p6}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v2, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    iput-wide p3, v2, Lcom/google/android/gms/people/d/n;->c:J

    move/from16 v0, p7

    iput-boolean v0, v2, Lcom/google/android/gms/people/d/n;->d:Z

    move/from16 v0, p8

    iput-boolean v0, v2, Lcom/google/android/gms/people/d/n;->e:Z

    move/from16 v0, p9

    iput-boolean v0, v2, Lcom/google/android/gms/people/d/n;->f:Z

    iput-boolean v3, v2, Lcom/google/android/gms/people/d/n;->g:Z

    iput-boolean v4, v2, Lcom/google/android/gms/people/d/n;->h:Z

    iput-boolean v7, v2, Lcom/google/android/gms/people/d/n;->j:Z

    new-instance v7, Lcom/google/android/gms/people/d/l;

    invoke-direct {v7}, Lcom/google/android/gms/people/d/l;-><init>()V

    invoke-static {}, Lcom/google/android/gms/people/f/b;->a()Lcom/google/android/gms/people/d/s;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    iput-object v2, v7, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    invoke-static {v6}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, v7, Lcom/google/android/gms/people/d/l;->d:Z

    const-string v2, "sync_request"

    invoke-virtual {v5, v2, p1, v7}, Lcom/google/android/gms/people/f/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d/l;)V

    .line 425
    :cond_1
    if-eqz v3, :cond_2

    if-eqz v4, :cond_7

    .line 426
    :cond_2
    const/4 v2, 0x0

    .line 451
    :goto_6
    return v2

    .line 409
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 430
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->a()V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    const-string v4, "SELECT COUNT(*)  FROM owner_sync_requests JOIN owners ON owner_sync_requests.account_name=owners.account_name AND ((owner_sync_requests.page_gaia_id IS NULL AND owners.page_gaia_id IS NULL ) OR owner_sync_requests.page_gaia_id=owners.page_gaia_id) WHERE owners._id=? AND owner_sync_requests.sync_requested_time>owners.last_sync_start_time"

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_c

    const/4 v2, 0x0

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_c

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 433
    :goto_7
    invoke-static/range {p7 .. p8}, Lcom/google/android/gms/people/sync/u;->a(ZZ)Landroid/os/Bundle;

    move-result-object v3

    .line 434
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 435
    const-string v2, "gms.people.request_app_id"

    move-object/from16 v0, p5

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :cond_8
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 438
    const-string v2, "gms.people.request_package"

    move-object/from16 v0, p6

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :cond_9
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 441
    const-string v2, "gms.people.sync_reason"

    move-object/from16 v0, p10

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :cond_a
    const-string v4, "page_only"

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    const/4 v2, 0x1

    :goto_8
    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 445
    iget-object v2, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    const-string v4, "com.google.android.gms.people"

    invoke-virtual {p0, v2, v4, v3}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 447
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 448
    const-string v2, "PeopleSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sync requested for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " page="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ignoreBackoff="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :cond_b
    const/4 v2, 0x1

    goto/16 :goto_6

    .line 430
    :cond_c
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/people/sync/u;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    const-string v2, "REPLACE INTO owner_sync_requests (account_name ,page_gaia_id ,sync_requested_time) VALUES (?,?,?)"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, p2, v4}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v3}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/google/android/gms/people/c/e;->a(Z)V

    goto/16 :goto_7

    :catchall_0
    move-exception v2

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v2

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v2

    .line 443
    :cond_e
    const/4 v2, 0x0

    goto :goto_8

    :cond_f
    move v3, v2

    goto/16 :goto_1

    :cond_10
    move v4, v2

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 327
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12

    .prologue
    .line 376
    const-wide/16 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object/from16 v11, p5

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Z
    .locals 12

    .prologue
    .line 367
    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object/from16 v7, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Z)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 184
    iget-object v2, p0, Lcom/google/android/gms/people/sync/u;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 185
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    iget-object v3, v3, Lcom/google/android/gms/people/a/c;->a:Landroid/content/SharedPreferences;

    const-string v4, "is_first_sync"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 186
    monitor-exit v2

    .line 197
    :goto_0
    return v0

    .line 188
    :cond_0
    const-string v3, "First sync"

    const/4 v4, 0x0

    invoke-virtual {p0, v4, v3}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v3, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/people/service/o;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    const-string v6, "com.google.android.gms.people"

    const/4 v7, 0x1

    invoke-virtual {p0, v5, v6, v7}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v6, "com.android.contacts"

    invoke-direct {p0, v5, v6}, Lcom/google/android/gms/people/sync/u;->b(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v6

    const/4 v7, 0x4

    invoke-static {v7}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    const-string v7, "com.google.android.gms.people"

    invoke-virtual {p0, v5, v7, v6}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    if-eqz v6, :cond_1

    invoke-virtual {p0, v5}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 192
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/people/a/c;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "is_first_sync"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 194
    if-eqz p1, :cond_3

    .line 195
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/u;->a()V

    .line 197
    :cond_3
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    .line 649
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 650
    const-string v2, "SELECT owner_sync_requests.page_gaia_id,last_sync_start_time FROM owner_sync_requests JOIN owners ON owner_sync_requests.account_name=owners.account_name AND ((owner_sync_requests.page_gaia_id IS NULL AND owners.page_gaia_id IS NULL ) OR owner_sync_requests.page_gaia_id=owners.page_gaia_id) WHERE owner_sync_requests.account_name=? AND owner_sync_requests.page_gaia_id IS NOT NULL AND owner_sync_requests.sync_requested_time>last_sync_start_time ORDER BY owner_sync_requests.sync_requested_time"

    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 656
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 660
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 662
    return-object v1
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 913
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/u;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/people/a/c;->b(J)V

    .line 914
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 262
    if-eqz p2, :cond_1

    .line 268
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/u;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/sync/u;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final c()J
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 933
    :goto_0
    sget-object v0, Lcom/google/android/gms/people/a/a;->aK:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 934
    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    move-wide v0, v2

    .line 954
    :goto_1
    return-wide v0

    .line 938
    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    iget-object v4, v4, Lcom/google/android/gms/people/a/c;->a:Landroid/content/SharedPreferences;

    const-string v5, "last_known_mutation_time"

    invoke-interface {v4, v5, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 940
    cmp-long v6, v4, v2

    if-nez v6, :cond_1

    move-wide v0, v2

    .line 942
    goto :goto_1

    .line 945
    :cond_1
    iget-object v6, p0, Lcom/google/android/gms/people/sync/u;->d:Lcom/google/android/gms/common/util/p;

    invoke-interface {v6}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    .line 946
    const-wide/16 v8, 0x3e8

    mul-long/2addr v0, v8

    add-long/2addr v0, v4

    .line 948
    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 951
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/people/a/c;->b(J)V

    goto :goto_0

    .line 954
    :cond_2
    sub-long/2addr v0, v6

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 848
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 849
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->b:Lcom/google/android/gms/people/a/c;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/people/a/c;->b(Ljava/lang/String;I)V

    .line 850
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/u;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/sync/u;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 281
    :cond_0
    const/4 v0, 0x0

    .line 288
    :goto_0
    return v0

    .line 284
    :cond_1
    if-nez p2, :cond_2

    .line 285
    const/4 v0, 0x1

    goto :goto_0

    .line 288
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 506
    iget-object v2, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v2

    .line 507
    const-string v3, "SELECT last_sync_status FROM owners WHERE _id=?"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 961
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "com.google.android.gms.people"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/sync/u;->b(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 687
    iget-object v0, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    const-string v1, "PeopleSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Contacts sync requested. reason="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    invoke-static {v4, v4}, Lcom/google/android/gms/people/sync/u;->a(ZZ)Landroid/os/Bundle;

    move-result-object v0

    .line 693
    const-string v1, "gms.people.contacts_sync"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 694
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 695
    const-string v1, "gms.people.sync_reason"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/u;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    const-string v2, "com.google.android.gms.people"

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/gms/people/sync/u;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 700
    return-void
.end method
