.class final Lcom/google/android/gms/drive/realtime/cache/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/realtime/cache/i;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/realtime/cache/i;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/cache/k;->a:Lcom/google/android/gms/drive/realtime/cache/i;

    .line 34
    return-void
.end method

.method private a(Landroid/database/Cursor;J)Ljava/util/List;
    .locals 4

    .prologue
    .line 134
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 135
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/k;->a:Lcom/google/android/gms/drive/realtime/cache/i;

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 137
    instance-of v2, v0, Lcom/google/c/a/a/b/c/e;

    if-eqz v2, :cond_1

    .line 139
    check-cast v0, Lcom/google/c/a/a/b/c/e;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 140
    :cond_1
    :try_start_1
    instance-of v2, v0, Lcom/google/c/b/a/c;

    if-eqz v2, :cond_0

    .line 142
    check-cast v0, Lcom/google/c/b/a/c;

    .line 143
    new-instance v2, Lcom/google/c/a/a/b/c/e;

    invoke-direct {v2, p2, p3, v0}, Lcom/google/c/a/a/b/c/e;-><init>(JLcom/google/c/b/a/c;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 148
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-object v1
.end method

.method private c(Landroid/database/sqlite/SQLiteDatabase;)Lcom/google/c/a/a/b/c/f;
    .locals 8

    .prologue
    .line 89
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;

    move-result-object v0

    .line 90
    if-nez v0, :cond_0

    .line 91
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/p;

    const-string v1, "Missing snapshot, can\'t load from cache"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/p;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/k;->a:Lcom/google/android/gms/drive/realtime/cache/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/c/a/a/b/c/f;

    .line 95
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 97
    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 99
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a()Lcom/google/android/gms/drive/realtime/cache/a/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v1

    .line 101
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/k;->a:Lcom/google/android/gms/drive/realtime/cache/i;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    .line 103
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 108
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->c(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v4

    .line 109
    new-instance v0, Lcom/google/c/a/a/b/c/f;

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->c()Z

    move-result v3

    invoke-virtual {v6}, Lcom/google/c/a/a/b/c/f;->f()Ljava/util/Collection;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/c/a/a/b/c/f;-><init>(Ljava/lang/String;Ljava/lang/String;ZILjava/util/List;Ljava/util/Collection;Ljava/lang/String;)V

    return-object v0
.end method

.method private d(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;
    .locals 5

    .prologue
    .line 117
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/e;->a()Lcom/google/android/gms/drive/realtime/cache/a/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/cache/a/e;->b(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/gms/drive/realtime/cache/k;->a(Landroid/database/Cursor;J)Ljava/util/List;

    move-result-object v0

    .line 119
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/e;->a()Lcom/google/android/gms/drive/realtime/cache/a/e;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/realtime/cache/a/e;->c(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/Cursor;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/gms/drive/realtime/cache/k;->a(Landroid/database/Cursor;J)Ljava/util/List;

    move-result-object v1

    .line 121
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 123
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 124
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 125
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)Lcom/google/c/a/a/b/a/d;
    .locals 6

    .prologue
    .line 42
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/k;->c(Landroid/database/sqlite/SQLiteDatabase;)Lcom/google/c/a/a/b/c/f;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/f;->d()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/f;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/k;->d(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;

    move-result-object v4

    new-instance v5, Lcom/google/c/a/a/b/a/f;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/google/c/a/a/b/a/f;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    .line 46
    new-instance v1, Lcom/google/c/a/a/b/a/d;

    invoke-direct {v1, v0, v5}, Lcom/google/c/a/a/b/a/d;-><init>(Lcom/google/c/a/a/b/c/f;Lcom/google/c/a/a/b/a/f;)V
    :try_end_0
    .catch Lcom/google/c/a/a/b/d/a; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 48
    :catch_0
    move-exception v0

    .line 49
    new-instance v1, Lcom/google/android/gms/drive/realtime/cache/p;

    const-string v2, "Error parsing cached json"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/realtime/cache/p;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)Lcom/google/c/a/a/b/a/f;
    .locals 5

    .prologue
    .line 67
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;

    move-result-object v0

    .line 68
    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/p;

    const-string v1, "Missing snapshot, can\'t load from cache"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/p;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/c/a/a/b/d/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    new-instance v1, Lcom/google/android/gms/drive/realtime/cache/p;

    const-string v2, "Error parsing cached json"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/realtime/cache/p;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 74
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/k;->a:Lcom/google/android/gms/drive/realtime/cache/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/c/f;

    .line 75
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->c(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v1

    .line 76
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/k;->d(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;

    move-result-object v2

    .line 78
    new-instance v3, Lcom/google/c/a/a/b/a/f;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/f;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v1, v0, v2}, Lcom/google/c/a/a/b/a/f;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V
    :try_end_1
    .catch Lcom/google/c/a/a/b/d/a; {:try_start_1 .. :try_end_1} :catch_0

    return-object v3
.end method
