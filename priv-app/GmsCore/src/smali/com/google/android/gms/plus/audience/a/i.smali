.class public final Lcom/google/android/gms/plus/audience/a/i;
.super Lcom/google/android/gms/plus/audience/a/e;
.source "SourceFile"


# instance fields
.field private d:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:Lcom/google/android/gms/common/data/c;

.field private u:Lcom/google/android/gms/plus/audience/a/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 30
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/audience/a/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 32
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/a/i;)Lcom/google/android/gms/common/data/c;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->l:Lcom/google/android/gms/common/data/c;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/a/i;Lcom/google/android/gms/common/data/c;)Lcom/google/android/gms/common/data/c;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/a/i;->l:Lcom/google/android/gms/common/data/c;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/a/i;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/a/i;->j:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/a/i;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/plus/audience/a/i;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/common/data/d;Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/a/i;)Lcom/google/android/gms/plus/audience/a/j;
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->u:Lcom/google/android/gms/plus/audience/a/j;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Lcom/google/android/gms/plus/audience/a/i;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->d:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 69
    :cond_0
    iput-object v1, p0, Lcom/google/android/gms/plus/audience/a/i;->j:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/google/android/gms/plus/audience/a/i;->l:Lcom/google/android/gms/common/data/c;

    .line 77
    :cond_1
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/android/gms/plus/audience/a/i;->k:I

    .line 78
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/a/i;->d:Ljava/lang/String;

    .line 79
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/a/e;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/audience/a/e;

    .line 81
    :goto_0
    return-object p0

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->u:Lcom/google/android/gms/plus/audience/a/j;

    if-eqz v0, :cond_1

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/api/v;)V
    .locals 5

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->u:Lcom/google/android/gms/plus/audience/a/j;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->u:Lcom/google/android/gms/plus/audience/a/j;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/plus/audience/a/j;->a:Z

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->u:Lcom/google/android/gms/plus/audience/a/j;

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/e;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    :goto_0
    return-void

    .line 46
    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/audience/a/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/audience/a/j;-><init>(Lcom/google/android/gms/plus/audience/a/i;B)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->u:Lcom/google/android/gms/plus/audience/a/j;

    .line 48
    sget-object v0, Lcom/google/android/gms/people/x;->k:Lcom/google/android/gms/people/s;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a/e;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/e;->h:Ljava/lang/String;

    new-instance v3, Lcom/google/android/gms/people/t;

    invoke-direct {v3}, Lcom/google/android/gms/people/t;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/a/e;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/t;->a(Ljava/lang/String;)Lcom/google/android/gms/people/t;

    move-result-object v3

    iget v4, p0, Lcom/google/android/gms/plus/audience/a/i;->k:I

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/t;->a(I)Lcom/google/android/gms/people/t;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/a/i;->j:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/t;->b(Ljava/lang/String;)Lcom/google/android/gms/people/t;

    move-result-object v3

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/google/android/gms/people/s;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/t;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a/i;->u:Lcom/google/android/gms/plus/audience/a/j;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a/i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/i;->d:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/a/i;->a(Ljava/lang/String;I)Lcom/google/android/gms/plus/audience/a/i;

    .line 64
    :cond_0
    return-void
.end method
