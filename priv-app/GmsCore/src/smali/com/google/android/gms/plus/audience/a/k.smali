.class public final Lcom/google/android/gms/plus/audience/a/k;
.super Landroid/support/v4/a/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/plus/internal/ap;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Lcom/google/android/gms/plus/internal/ad;

.field private c:Lcom/google/android/gms/plus/internal/ab;

.field private d:Z

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:I

.field private j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 9

    .prologue
    .line 43
    sget-object v8, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/audience/a/k;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V

    .line 45
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/support/v4/a/j;-><init>(Landroid/content/Context;)V

    .line 52
    iput-object p2, p0, Lcom/google/android/gms/plus/audience/a/k;->e:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lcom/google/android/gms/plus/audience/a/k;->f:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Lcom/google/android/gms/plus/audience/a/k;->g:Ljava/lang/String;

    .line 55
    iput p5, p0, Lcom/google/android/gms/plus/audience/a/k;->h:I

    .line 56
    iput p6, p0, Lcom/google/android/gms/plus/audience/a/k;->i:I

    .line 57
    iput-object p7, p0, Lcom/google/android/gms/plus/audience/a/k;->a:Ljava/lang/String;

    .line 58
    iput-object p8, p0, Lcom/google/android/gms/plus/audience/a/k;->b:Lcom/google/android/gms/plus/internal/ad;

    .line 59
    return-void
.end method

.method private a(Lcom/google/android/gms/plus/internal/ab;)V
    .locals 3

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/gms/plus/audience/a/k;->h:I

    iget v1, p0, Lcom/google/android/gms/plus/audience/a/k;->i:I

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/k;->a:Ljava/lang/String;

    invoke-interface {p1, p0, v0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ap;IILjava/lang/String;)V

    .line 79
    return-void
.end method


# virtual methods
.method public final T_()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/k;->d:Z

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/a/k;->a(Lcom/google/android/gms/plus/internal/ab;)V

    .line 138
    return-void
.end method

.method public final W_()V
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a/k;->e()V

    .line 146
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/k;->d:Z

    .line 147
    return-void
.end method

.method protected final a()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Landroid/support/v4/a/j;->a()V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/a/k;->a(Lcom/google/android/gms/plus/internal/ab;)V

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/k;->d:Z

    if-nez v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/k;->d:Z

    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/a/k;->b(Ljava/lang/Object;)V

    .line 132
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/a/k;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    .line 84
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/a/k;->b(Ljava/lang/Object;)V

    .line 85
    return-void
.end method

.method protected final e()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->c:Lcom/google/android/gms/plus/internal/ab;

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/internal/cn;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/k;->g:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/plus.peopleapi.readwrite"

    aput-object v3, v2, v5

    const-string v3, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v3, v2, v6

    const/4 v3, 0x2

    const-string v4, "https://www.googleapis.com/auth/plus.me"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "service_googleme"

    aput-object v3, v2, v5

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->d:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/k;->f:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/k;->e:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->e:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/k;->b:Lcom/google/android/gms/plus/internal/ad;

    invoke-interface {v2, v0, v1, p0, p0}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->c:Lcom/google/android/gms/plus/internal/ab;

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/a/k;->b(Ljava/lang/Object;)V

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a/k;->i()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    if-nez v0, :cond_3

    .line 98
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 100
    :cond_3
    return-void
.end method

.method protected final f()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->c:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/k;->d:Z

    if-eqz v0, :cond_2

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/k;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/k;->d:Z

    .line 119
    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 0

    .prologue
    .line 123
    invoke-super {p0}, Landroid/support/v4/a/j;->g()V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a/k;->f()V

    .line 126
    return-void
.end method
