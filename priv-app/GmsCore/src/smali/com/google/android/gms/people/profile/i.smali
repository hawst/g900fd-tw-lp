.class final Lcom/google/android/gms/people/profile/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/gms/people/profile/AvatarView;

.field private b:F

.field private c:F

.field private d:F

.field private e:J

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/profile/AvatarView;)V
    .locals 0

    .prologue
    .line 1263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1264
    iput-object p1, p0, Lcom/google/android/gms/people/profile/i;->a:Lcom/google/android/gms/people/profile/AvatarView;

    .line 1265
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1294
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/i;->g:Z

    if-eqz v0, :cond_1

    .line 1320
    :cond_0
    :goto_0
    return-void

    .line 1298
    :cond_1
    iget v0, p0, Lcom/google/android/gms/people/profile/i;->c:F

    iget v1, p0, Lcom/google/android/gms/people/profile/i;->b:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_6

    .line 1299
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1300
    iget-wide v0, p0, Lcom/google/android/gms/people/profile/i;->e:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    iget-wide v0, p0, Lcom/google/android/gms/people/profile/i;->e:J

    sub-long v0, v2, v0

    .line 1301
    :goto_1
    iget v4, p0, Lcom/google/android/gms/people/profile/i;->d:F

    long-to-float v0, v0

    mul-float/2addr v0, v4

    .line 1302
    iget v1, p0, Lcom/google/android/gms/people/profile/i;->c:F

    iget v4, p0, Lcom/google/android/gms/people/profile/i;->b:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_2

    iget v1, p0, Lcom/google/android/gms/people/profile/i;->c:F

    add-float/2addr v1, v0

    iget v4, p0, Lcom/google/android/gms/people/profile/i;->b:F

    cmpl-float v1, v1, v4

    if-gtz v1, :cond_3

    :cond_2
    iget v1, p0, Lcom/google/android/gms/people/profile/i;->c:F

    iget v4, p0, Lcom/google/android/gms/people/profile/i;->b:F

    cmpl-float v1, v1, v4

    if-lez v1, :cond_4

    iget v1, p0, Lcom/google/android/gms/people/profile/i;->c:F

    add-float/2addr v1, v0

    iget v4, p0, Lcom/google/android/gms/people/profile/i;->b:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_4

    .line 1306
    :cond_3
    iget v0, p0, Lcom/google/android/gms/people/profile/i;->b:F

    iget v1, p0, Lcom/google/android/gms/people/profile/i;->c:F

    sub-float/2addr v0, v1

    .line 1308
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/people/profile/i;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-static {v1, v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;F)V

    .line 1309
    iget v1, p0, Lcom/google/android/gms/people/profile/i;->c:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/people/profile/i;->c:F

    .line 1310
    iget v0, p0, Lcom/google/android/gms/people/profile/i;->c:F

    iget v1, p0, Lcom/google/android/gms/people/profile/i;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    .line 1311
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/i;->f:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/i;->g:Z

    .line 1313
    :cond_5
    iput-wide v2, p0, Lcom/google/android/gms/people/profile/i;->e:J

    .line 1316
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/i;->g:Z

    if-nez v0, :cond_0

    .line 1319
    iget-object v0, p0, Lcom/google/android/gms/people/profile/i;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1300
    :cond_7
    const-wide/16 v0, 0x0

    goto :goto_1
.end method
