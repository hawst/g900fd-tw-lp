.class public final Lcom/google/android/gms/plus/service/v2whitelisted/b;
.super Lcom/google/android/gms/common/server/j;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/common/server/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/n;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/common/server/j;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a:Lcom/google/android/gms/common/server/n;

    .line 35
    return-void
.end method

.method public static a(Lcom/google/android/gms/plus/service/v2whitelisted/c;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 426
    const-string v0, "people/%1$s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 427
    if-eqz p0, :cond_0

    .line 428
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 430
    :cond_0
    if-eqz p5, :cond_1

    .line 440
    const-string v1, "includeLinkedPeople"

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 442
    :cond_1
    if-eqz p7, :cond_2

    .line 446
    const-string v1, "includeProfilesWithState"

    const-string v2, "&includeProfilesWithState="

    invoke-static {v2, p7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 448
    :cond_2
    if-eqz p8, :cond_3

    .line 449
    const-string v1, "onBehalfOf"

    invoke-static {p8}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 451
    :cond_3
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/d;)Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;
    .locals 7

    .prologue
    .line 1198
    const-string v1, "people/%1$s/people/%2$s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-eqz p12, :cond_0

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, Lcom/google/android/gms/plus/service/v2whitelisted/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_0
    if-eqz p4, :cond_1

    const-string v1, "customResponseMaskingType"

    invoke-static {p4}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_1
    if-eqz p5, :cond_2

    const-string v1, "includeAffinity"

    const-string v2, "&includeAffinity="

    invoke-static {v2, p5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_2
    if-eqz p6, :cond_3

    const-string v1, "includeOthers"

    invoke-static {p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_3
    if-eqz p7, :cond_4

    const-string v1, "maxResults"

    invoke-static {p7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_4
    if-eqz p8, :cond_5

    const-string v1, "onBehalfOf"

    invoke-static {p8}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_5
    if-eqz p9, :cond_6

    const-string v1, "orderBy"

    invoke-static/range {p9 .. p9}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_6
    if-eqz p10, :cond_7

    const-string v1, "pageToken"

    invoke-static/range {p10 .. p10}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_7
    if-eqz p11, :cond_8

    const-string v1, "syncToken"

    invoke-static/range {p11 .. p11}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1199
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-class v6, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;

    return-object v1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/c;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;
    .locals 10

    .prologue
    .line 618
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p6

    move-object v1, p2

    move-object v5, p3

    move-object v7, p4

    move-object v8, p5

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Lcom/google/android/gms/plus/service/v2whitelisted/c;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 619
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    return-object v0
.end method
