.class final Lcom/google/android/gms/people/service/a/h;
.super Lcom/google/android/gms/people/service/a/g;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/service/a/f;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/service/a/f;)V
    .locals 1

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/gms/people/service/a/h;->b:Lcom/google/android/gms/people/service/a/f;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/service/a/g;-><init>(Lcom/google/android/gms/people/service/a/f;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/service/a/f;B)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/a/h;-><init>(Lcom/google/android/gms/people/service/a/f;)V

    return-void
.end method


# virtual methods
.method final a(Landroid/os/Bundle;Lcom/google/android/gms/people/c/e;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 169
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    const-string v0, "PeopleService"

    const-string v1, "People Query: SELECT DISTINCT M.gaia_id, M.contact_id FROM gaia_id_map AS M JOIN temp_gaia_ordinal AS O ON O.gaia_id = M.gaia_id WHERE M.owner_id = ? ORDER BY O.ordinal"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_0
    const-string v0, "SELECT DISTINCT M.gaia_id, M.contact_id FROM gaia_id_map AS M JOIN temp_gaia_ordinal AS O ON O.gaia_id = M.gaia_id WHERE M.owner_id = ? ORDER BY O.ordinal"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    .line 192
    const-string v1, "gaia_map"

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/h;->b:Lcom/google/android/gms/people/service/a/f;

    new-instance v3, Lcom/google/android/gms/common/data/DataHolder;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 194
    return-void
.end method
