.class public Lcom/google/android/gms/security/settings/SecuritySettingsActivity;
.super Lcom/google/android/gms/common/widget/a/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/widget/a/p;


# static fields
.field private static final a:Landroid/content/IntentFilter;


# instance fields
.field private b:Lcom/google/android/gms/common/widget/a/r;

.field private c:Lcom/google/android/gms/common/widget/a/r;

.field private d:Z

.field private e:Lcom/google/android/gms/common/widget/a/r;

.field private f:Lcom/google/android/gms/common/widget/a/r;

.field private g:Z

.field private h:Z

.field private i:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.mdm.DEVICE_ADMIN_CHANGE_INTENT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/common/widget/a/b;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->h:Z

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private a(II)Lcom/google/android/gms/common/widget/a/r;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/google/android/gms/common/widget/a/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/widget/a/r;-><init>(Landroid/content/Context;)V

    .line 97
    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/widget/a/r;->a(I)V

    .line 98
    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/widget/a/r;->c(I)V

    .line 99
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/widget/a/r;->a(Lcom/google/android/gms/common/widget/a/p;)V

    .line 100
    return-object v0
.end method

.method private a(III)Lcom/google/android/gms/common/widget/a/r;
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a(II)Lcom/google/android/gms/common/widget/a/r;

    move-result-object v0

    .line 117
    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/widget/a/r;->d(I)V

    .line 118
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/security/settings/SecuritySettingsActivity;)Lcom/google/android/gms/common/widget/a/r;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->c:Lcom/google/android/gms/common/widget/a/r;

    return-object v0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    if-nez v0, :cond_0

    .line 322
    :goto_0
    return-void

    .line 315
    :cond_0
    if-eqz p1, :cond_1

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/r;->a(Z)V

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    sget v1, Lcom/google/android/gms/p;->zg:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/r;->d(I)V

    goto :goto_0

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/r;->a(Z)V

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    sget v1, Lcom/google/android/gms/p;->zf:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/r;->d(I)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 67
    invoke-static {p0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/gms/mdm/b/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 73
    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f()Z
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/gms/security/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/widget/a/l;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 90
    sget v0, Lcom/google/android/gms/p;->eY:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/widget/a/l;->f(I)Lcom/google/android/gms/common/widget/a/i;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->c(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->d:Z

    iget-boolean v1, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->d:Z

    if-eqz v1, :cond_2

    sget v1, Lcom/google/android/gms/p;->rg:I

    invoke-direct {p0, v3, v1}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a(II)Lcom/google/android/gms/common/widget/a/r;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    sget v1, Lcom/google/android/gms/p;->rj:I

    sget v2, Lcom/google/android/gms/p;->ri:I

    invoke-direct {p0, v4, v1, v2}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a(III)Lcom/google/android/gms/common/widget/a/r;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->c:Lcom/google/android/gms/common/widget/a/r;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/gms/common/widget/a/e;

    iget-object v2, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->c:Lcom/google/android/gms/common/widget/a/r;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/i;->a([Lcom/google/android/gms/common/widget/a/e;)V

    new-instance v0, Lcom/google/android/gms/security/settings/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/security/settings/a;-><init>(Lcom/google/android/gms/security/settings/SecuritySettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_device_admin"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->a(Landroid/content/Context;)V

    :cond_0
    const-string v1, "show_modal_request"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Lcom/google/android/gms/mdm/c/a;

    invoke-direct {v0}, Lcom/google/android/gms/mdm/c/a;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "activate_device_admin_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/mdm/c/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    :cond_1
    if-eqz p2, :cond_2

    const-string v0, "verify_google_location"

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->h:Z

    .line 91
    :cond_2
    sget v0, Lcom/google/android/gms/p;->zm:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/widget/a/l;->f(I)Lcom/google/android/gms/common/widget/a/i;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->g:Z

    iget-boolean v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->g:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/gms/p;->zo:I

    sget v2, Lcom/google/android/gms/p;->zn:I

    invoke-direct {p0, v3, v0, v2}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a(III)Lcom/google/android/gms/common/widget/a/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->e:Lcom/google/android/gms/common/widget/a/r;

    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->e:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    sget-object v0, Lcom/google/android/gms/security/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/gms/p;->zh:I

    invoke-direct {p0, v4, v0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a(II)Lcom/google/android/gms/common/widget/a/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 92
    :cond_3
    return-void
.end method

.method public onClick(Landroid/view/View;Lcom/google/android/gms/common/widget/a/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->e:Lcom/google/android/gms/common/widget/a/r;

    if-ne p2, v0, :cond_4

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->e:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/r;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->e:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    :try_start_0
    const-string v3, "android.provider.Settings$Global"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v4, "putInt"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/content/ContentResolver;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v3, 0x1

    const-string v7, "package_verifier_enable"

    aput-object v7, v6, v3

    const/4 v7, 0x2

    if-eqz v0, :cond_2

    move v3, v1

    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a(Z)V

    .line 256
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 248
    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "package_verifier_enable"

    if-eqz v0, :cond_3

    :goto_4
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_4

    .line 249
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    if-ne p2, v0, :cond_8

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/r;->isChecked()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    iget-object v3, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    :try_start_1
    const-string v3, "android.provider.Settings$Global"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v4, "putInt"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/content/ContentResolver;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v3, 0x1

    const-string v7, "upload_apk_enable"

    aput-object v7, v6, v3

    const/4 v7, 0x2

    if-eqz v0, :cond_6

    move v3, v1

    :goto_6
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "upload_apk_enable"

    if-eqz v0, :cond_7

    :goto_7
    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v3, v2

    goto :goto_6

    :cond_7
    move v1, v2

    goto :goto_7

    .line 251
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    if-ne p2, v0, :cond_b

    .line 252
    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/r;->isChecked()Z

    move-result v0

    if-nez v0, :cond_9

    :goto_8
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->d:Lcom/google/android/gms/common/a/r;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_9
    move v1, v2

    goto :goto_8

    :cond_a
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->startActivity(Landroid/content/Intent;)V

    iput-boolean v1, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->h:Z

    goto/16 :goto_3

    .line 253
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->c:Lcom/google/android/gms/common/widget/a/r;

    if-ne p2, v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->c:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/r;->isChecked()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->a(Landroid/content/Context;)V

    goto/16 :goto_3

    :cond_c
    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->b(Landroid/content/Context;)V

    goto/16 :goto_3
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 85
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 86
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/m;->D:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 261
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 266
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->qZ:I

    if-ne v0, v1, :cond_0

    .line 267
    sget-object v0, Lcom/google/android/gms/common/a/b;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 269
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 274
    const-string v2, "isMdmVisible"

    iget-boolean v3, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->d:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const-string v2, "isVerifyAppsVisible"

    iget-boolean v3, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->g:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v2, "android_security"

    invoke-static {v2}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v2

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/os/Bundle;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a()Landroid/content/Intent;

    move-result-object v0

    .line 280
    new-instance v1, Lcom/google/android/gms/googlehelp/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/b;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/b;->a(Landroid/content/Intent;)V

    .line 281
    const/4 v0, 0x1

    .line 283
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->d:Z

    if-eqz v0, :cond_0

    .line 178
    invoke-static {p0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;)V

    .line 180
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/common/widget/a/b;->onPause()V

    .line 181
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 185
    invoke-super {p0}, Lcom/google/android/gms/common/widget/a/b;->onResume()V

    .line 186
    iget-boolean v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->d:Z

    if-eqz v0, :cond_2

    .line 187
    invoke-static {p0}, Lcom/google/android/gms/mdm/f/d;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0}, Lcom/google/android/gms/common/util/y;->a(Landroid/content/Context;)Z

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->h:Z

    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    sget-object v4, Lcom/google/android/gms/mdm/e/a;->d:Lcom/google/android/gms/common/a/r;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    :cond_0
    if-eqz v0, :cond_5

    if-nez v3, :cond_1

    const/16 v4, 0x13

    invoke-static {v4}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v4, v1}, Lcom/google/android/gms/common/widget/a/r;->a(Z)V

    iget-object v4, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    sget v5, Lcom/google/android/gms/p;->rf:I

    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/widget/a/r;->d(I)V

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->c:Lcom/google/android/gms/common/widget/a/r;

    invoke-static {p0}, Lcom/google/android/gms/mdm/f/c;->c(Landroid/content/Context;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    iget-object v4, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    if-eqz v0, :cond_7

    if-eqz v3, :cond_7

    sget-object v0, Lcom/google/android/gms/mdm/e/a;->d:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    invoke-static {p0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->i:Landroid/content/BroadcastReceiver;

    sget-object v4, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a:Landroid/content/IntentFilter;

    invoke-virtual {v0, v3, v4}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 190
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->g:Z

    if-eqz v0, :cond_4

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->e:Lcom/google/android/gms/common/widget/a/r;

    invoke-static {p0}, Lcom/google/android/gms/security/settings/b;->a(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    invoke-static {p0}, Lcom/google/android/gms/security/settings/b;->b(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    invoke-static {p0}, Lcom/google/android/gms/security/settings/b;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->a(Z)V

    :cond_3
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    if-eqz v0, :cond_8

    const-string v3, "ensure_verify_apps"

    invoke-virtual {v0, v3}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    :goto_2
    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->e:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/widget/a/r;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/widget/a/r;->a(Z)V

    .line 193
    :cond_4
    return-void

    .line 187
    :cond_5
    iget-object v4, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/common/widget/a/r;->a(Z)V

    if-nez v0, :cond_6

    iget-object v4, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    sget v5, Lcom/google/android/gms/p;->rh:I

    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/widget/a/r;->d(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->b:Lcom/google/android/gms/common/widget/a/r;

    sget v5, Lcom/google/android/gms/p;->re:I

    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/widget/a/r;->d(I)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    move v1, v2

    .line 191
    goto :goto_2
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 288
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->g:Z

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->e:Lcom/google/android/gms/common/widget/a/r;

    invoke-static {p0}, Lcom/google/android/gms/security/settings/b;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/security/settings/SecuritySettingsActivity;->f:Lcom/google/android/gms/common/widget/a/r;

    invoke-static {p0}, Lcom/google/android/gms/security/settings/b;->b(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    .line 295
    :cond_0
    return-void
.end method
