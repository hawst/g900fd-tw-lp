.class public final Lcom/google/android/gms/drive/realtime/cache/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/a/e;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/drive/realtime/cache/n;

.field private final c:Lcom/google/android/gms/drive/realtime/cache/o;

.field private final d:Lcom/google/android/gms/drive/realtime/cache/i;

.field private final e:Lcom/google/android/gms/drive/realtime/cache/w;

.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/realtime/cache/w;)V
    .locals 7

    .prologue
    .line 32
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/realtime/cache/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/realtime/cache/w;Z)V

    .line 34
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/realtime/cache/w;Z)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/cache/d;->a:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/d;->b:Lcom/google/android/gms/drive/realtime/cache/n;

    .line 51
    iput-object p4, p0, Lcom/google/android/gms/drive/realtime/cache/d;->c:Lcom/google/android/gms/drive/realtime/cache/o;

    .line 52
    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/cache/d;->d:Lcom/google/android/gms/drive/realtime/cache/i;

    .line 53
    iput-object p5, p0, Lcom/google/android/gms/drive/realtime/cache/d;->e:Lcom/google/android/gms/drive/realtime/cache/w;

    .line 54
    iput-boolean p6, p0, Lcom/google/android/gms/drive/realtime/cache/d;->f:Z

    .line 55
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/d;->f:Z

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/d;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/realtime/cache/f;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/d;->b:Lcom/google/android/gms/drive/realtime/cache/n;

    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/cache/d;->e:Lcom/google/android/gms/drive/realtime/cache/w;

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/drive/realtime/cache/f;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/w;Z)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 98
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/a/b;)V
    .locals 4

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/d;->b:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/n;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot write a local mutation to a closing cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/d;->b:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/n;->d()V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/d;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/realtime/cache/j;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/d;->b:Lcom/google/android/gms/drive/realtime/cache/n;

    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/cache/d;->d:Lcom/google/android/gms/drive/realtime/cache/i;

    invoke-direct {v1, v2, v3, p1}, Lcom/google/android/gms/drive/realtime/cache/j;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/c/a/a/b/a/b;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 83
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/a/c;)V
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/d;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/realtime/cache/b;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/d;->b:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/drive/realtime/cache/b;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/c/a/a/b/a/c;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 90
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/a/g;)V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/d;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/realtime/cache/y;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/d;->b:Lcom/google/android/gms/drive/realtime/cache/n;

    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/cache/d;->d:Lcom/google/android/gms/drive/realtime/cache/i;

    invoke-direct {v1, v2, v3, p1}, Lcom/google/android/gms/drive/realtime/cache/y;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/c/a/a/b/a/g;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 71
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/c/f;)V
    .locals 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/d;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/realtime/cache/h;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/d;->b:Lcom/google/android/gms/drive/realtime/cache/n;

    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/cache/d;->d:Lcom/google/android/gms/drive/realtime/cache/i;

    invoke-direct {v1, v2, v3, p1}, Lcom/google/android/gms/drive/realtime/cache/h;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/c/a/a/b/c/f;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 63
    return-void
.end method
