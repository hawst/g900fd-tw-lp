.class public final Lcom/google/android/gms/wearable/node/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/d/c;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcom/google/android/gms/wearable/node/o;

.field final c:Landroid/content/SharedPreferences;

.field final d:Ljava/lang/Object;

.field e:Z

.field f:J

.field g:I

.field h:J

.field i:Lcom/google/android/gms/wearable/node/aa;

.field j:Lcom/google/android/gms/wearable/node/s;

.field private final k:Lcom/google/android/gms/wearable/node/s;

.field private final l:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/wearable/node/o;Landroid/content/SharedPreferences;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Lcom/google/android/gms/wearable/node/s;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/wearable/node/s;-><init>(Lcom/google/android/gms/wearable/node/r;B)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/r;->k:Lcom/google/android/gms/wearable/node/s;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/r;->l:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/r;->d:Ljava/lang/Object;

    .line 72
    iput-wide v4, p0, Lcom/google/android/gms/wearable/node/r;->h:J

    .line 79
    iput-object p2, p0, Lcom/google/android/gms/wearable/node/r;->b:Lcom/google/android/gms/wearable/node/o;

    .line 80
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/r;->a:Ljava/lang/String;

    .line 81
    iput-boolean v2, p0, Lcom/google/android/gms/wearable/node/r;->e:Z

    .line 82
    iput-object p3, p0, Lcom/google/android/gms/wearable/node/r;->c:Landroid/content/SharedPreferences;

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/r;->f:J

    .line 84
    iput v2, p0, Lcom/google/android/gms/wearable/node/r;->g:I

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->k:Lcom/google/android/gms/wearable/node/s;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/s;->a(Z)V

    .line 86
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 117
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/r;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 118
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/r;->e:Z

    .line 120
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/gms/wearable/node/r;->h:J

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/r;->i:Lcom/google/android/gms/wearable/node/aa;

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->j:Lcom/google/android/gms/wearable/node/s;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wearable/node/s;->a(Z)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->l:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/r;->j:Lcom/google/android/gms/wearable/node/s;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v2, 0x14

    if-le v0, v2, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->l:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 127
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/gms/wearable/node/r;->j:Lcom/google/android/gms/wearable/node/s;

    .line 128
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/util/ad;ZZ)V
    .locals 4

    .prologue
    .line 311
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "connection to peer node: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/r;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 313
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/r;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 314
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "peer seqId: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/gms/wearable/node/r;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "peerStatePersistenceAttempt: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/wearable/node/r;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->print(Ljava/lang/String;)V

    .line 317
    const-string v0, "Total"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->print(Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->k:Lcom/google/android/gms/wearable/node/s;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wearable/node/s;->a(Lcom/google/android/gms/common/util/ad;)V

    .line 319
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->println()V

    .line 321
    const-string v0, "Current "

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->print(Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->j:Lcom/google/android/gms/wearable/node/s;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->j:Lcom/google/android/gms/wearable/node/s;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wearable/node/s;->a(Lcom/google/android/gms/common/util/ad;)V

    .line 327
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->println()V

    .line 328
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->println()V

    .line 332
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/r;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 333
    const-string v0, "Historic "

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->print(Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/r;->l:Ljava/util/ArrayList;

    sub-int v3, v2, v1

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/s;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wearable/node/s;->a(Lcom/google/android/gms/common/util/ad;)V

    .line 335
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->println()V

    .line 332
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 325
    :cond_0
    :try_start_1
    const-string v0, "[not connected]"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->print(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 328
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 337
    :cond_1
    return-void
.end method

.method final a(Lcom/google/android/gms/wearable/node/aa;Lcom/google/android/gms/wearable/c/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 284
    const-string v0, "datatransport"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    const-string v0, "datatransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendDataItemToPeer(SetDataItem): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_0
    new-instance v0, Lcom/google/android/gms/wearable/c/m;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/c/m;-><init>()V

    .line 288
    iput-object p2, v0, Lcom/google/android/gms/wearable/c/m;->i:Lcom/google/android/gms/wearable/c/r;

    .line 290
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/r;->j:Lcom/google/android/gms/wearable/node/s;

    if-eqz v1, :cond_1

    .line 291
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/r;->j:Lcom/google/android/gms/wearable/node/s;

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/s;->a()V

    .line 293
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/r;->k:Lcom/google/android/gms/wearable/node/s;

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/s;->a()V

    .line 294
    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/gms/wearable/node/aa;->a(ILcom/google/android/gms/wearable/c/m;Lcom/google/android/gms/wearable/node/y;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 307
    :cond_2
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 297
    const-string v1, "datatransport"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 298
    const-string v1, "datatransport"

    const-string v2, "  exception while sending dataItem to peer"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 300
    :catch_1
    move-exception v0

    .line 302
    const-string v1, "datatransport"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 303
    const-string v1, "datatransport"

    const-string v2, "  exception while sending dataItem to peer"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 305
    :cond_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method
