.class public final Lcom/google/android/gms/wallet/service/ow/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/service/ow/w;

.field private final b:Lcom/google/android/gms/wallet/service/ow/k;

.field private final c:Landroid/support/v4/g/h;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/service/ow/k;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/m;->d:Landroid/content/Context;

    .line 56
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/m;->b:Lcom/google/android/gms/wallet/service/ow/k;

    .line 57
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/m;->a:Lcom/google/android/gms/wallet/service/ow/w;

    .line 58
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/service/ow/n;-><init>(Lcom/google/android/gms/wallet/service/ow/m;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/m;->c:Landroid/support/v4/g/h;

    .line 65
    return-void
.end method

.method private static a(Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/p;
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168
    .line 169
    const/4 v3, 0x0

    .line 172
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v4

    .line 173
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v5

    .line 174
    iget-object v2, v4, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    if-eqz v2, :cond_0

    .line 175
    iget-object v2, v4, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    iget-object v2, v2, Lcom/google/aa/b/a/d;->a:Ljava/lang/String;

    .line 176
    iget-object v4, v5, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    if-eqz v4, :cond_3

    iget-object v4, v5, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    iget-object v4, v4, Lcom/google/aa/b/a/d;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v4, v0

    .line 201
    :goto_0
    new-instance v5, Lcom/google/android/gms/wallet/service/ow/p;

    invoke-direct {v5, v1}, Lcom/google/android/gms/wallet/service/ow/p;-><init>(B)V

    .line 202
    iput-boolean v4, v5, Lcom/google/android/gms/wallet/service/ow/p;->a:Z

    .line 203
    iput-object v3, v5, Lcom/google/android/gms/wallet/service/ow/p;->b:Ljava/lang/Long;

    .line 204
    iput-object v2, v5, Lcom/google/android/gms/wallet/service/ow/p;->c:Ljava/lang/String;

    .line 205
    iput-boolean v0, v5, Lcom/google/android/gms/wallet/service/ow/p;->d:Z

    .line 206
    return-object v5

    .line 182
    :cond_0
    iget-object v2, v4, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    iget-object v2, v2, Lcom/google/aa/b/a/e;->b:Ljava/lang/String;

    .line 183
    iget-object v6, v5, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    if-eqz v6, :cond_2

    iget-object v6, v5, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    iget-object v6, v6, Lcom/google/aa/b/a/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 187
    :try_start_0
    new-instance v6, Ljava/math/BigDecimal;

    iget-object v4, v4, Lcom/google/aa/b/a/a/a/a/i;->q:Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 189
    new-instance v4, Ljava/math/BigDecimal;

    iget-object v5, v5, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    iget-object v5, v5, Lcom/google/aa/b/a/e;->a:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 191
    invoke-virtual {v6, v4}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    .line 192
    sget-object v4, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v4, v5}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-gtz v4, :cond_1

    move v4, v0

    .line 193
    :goto_1
    const/4 v6, 0x3

    :try_start_1
    invoke-virtual {v5, v6}, Ljava/math/BigDecimal;->movePointRight(I)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    goto :goto_0

    :cond_1
    move v4, v1

    .line 192
    goto :goto_1

    .line 194
    :catch_0
    move-exception v4

    move-object v5, v4

    move v4, v1

    .line 195
    :goto_2
    const-string v6, "FullWalletRequester"

    const-string v7, "Exception parsing amount from full wallet request"

    invoke-static {v6, v7, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    move v0, v1

    move v4, v1

    .line 198
    goto :goto_0

    .line 194
    :catch_1
    move-exception v5

    goto :goto_2

    :cond_3
    move v4, v1

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/wallet/common/b;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/p;Lcom/google/android/gms/wallet/service/ow/j;Lcom/google/android/gms/wallet/service/ow/j;IJLjava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 213
    if-nez p4, :cond_1

    .line 214
    const-string v0, "FullWalletRequester"

    const-string v1, "GetFullWallet cache miss (no entry) :("

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    const-string v0, "get_full_wallet"

    const-string v1, "cache_miss"

    const-string v2, "null_entry"

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    invoke-virtual {p4}, Lcom/google/android/gms/wallet/service/ow/j;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    const-string v0, ""

    .line 234
    if-ne p4, p5, :cond_4

    .line 235
    const-string v0, "FullWalletRequester"

    const-string v1, "GetFullWallet cache hit!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const-string v1, "cache_hit"

    .line 238
    const/4 v2, 0x0

    .line 239
    invoke-virtual {p4}, Lcom/google/android/gms/wallet/service/ow/j;->c()Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v0

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    if-eqz v0, :cond_3

    .line 240
    const-string v0, "billing_agreement"

    .line 261
    :goto_1
    const-string v3, "get_full_wallet"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v5, p3, Lcom/google/android/gms/wallet/service/ow/p;->c:Ljava/lang/String;

    aput-object v5, v4, v0

    iget-object v0, p3, Lcom/google/android/gms/wallet/service/ow/p;->b:Ljava/lang/Long;

    iget-object v5, p1, Lcom/google/android/gms/wallet/common/b;->a:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, ":"

    invoke-static {v5, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v3, v1, v4, v0}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 271
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/m;->d:Landroid/content/Context;

    move-object v1, p2

    move v3, p6

    move-wide/from16 v4, p7

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;IIJLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :cond_3
    const-string v0, "one_time_card"

    goto :goto_1

    .line 246
    :cond_4
    const-string v1, "cache_miss"

    .line 247
    iget-boolean v2, p3, Lcom/google/android/gms/wallet/service/ow/p;->a:Z

    if-nez v2, :cond_5

    .line 249
    const-string v0, "FullWalletRequester"

    const-string v2, "GetFullWallet cache miss (amount insufficient) :("

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const-string v0, "prefetch_insufficient"

    .line 251
    const/4 v2, 0x2

    goto :goto_1

    .line 252
    :cond_5
    iget-boolean v2, p3, Lcom/google/android/gms/wallet/service/ow/p;->d:Z

    if-nez v2, :cond_6

    .line 253
    const/4 v2, 0x3

    goto :goto_1

    .line 257
    :cond_6
    const-string v0, "prefetch_failed"

    .line 258
    const/4 v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/common/b;)Landroid/util/Pair;
    .locals 14

    .prologue
    .line 73
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/m;->c:Landroid/support/v4/g/h;

    monitor-enter v1

    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/m;->c:Landroid/support/v4/g/h;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v2

    iget-object v2, v2, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .line 75
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    monitor-enter v13

    .line 79
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/l;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/l;-><init>(Lcom/google/aa/b/a/a/a/a/i;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/m;->b:Lcom/google/android/gms/wallet/service/ow/k;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/service/ow/k;->a(Lcom/google/android/gms/wallet/service/ow/l;)Lcom/google/android/gms/wallet/service/ow/j;

    move-result-object v5

    const/4 v10, 0x0

    const/4 v4, 0x0

    const-wide/16 v8, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v0

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v1

    iget-object v1, v1, Lcom/google/aa/b/a/a/a/a/i;->d:Lcom/google/aa/b/a/e;

    iget-object v1, v1, Lcom/google/aa/b/a/e;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->scaleByPowerOfTen(I)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v8

    :cond_0
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/service/ow/j;->g()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/service/ow/j;->h()V

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/service/ow/j;->a()J

    move-result-wide v6

    sget-object v0, Lcom/google/android/gms/wallet/b/c;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v0

    add-long/2addr v0, v6

    sub-long/2addr v0, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v0, v6

    if-lez v4, :cond_1

    :try_start_2
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v0, v1, v4}, Lcom/google/android/gms/wallet/service/ow/j;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    :goto_0
    :try_start_3
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/service/ow/j;->c()Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/service/ow/m;->a(Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/p;

    move-result-object v4

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/service/ow/j;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, v4, Lcom/google/android/gms/wallet/service/ow/p;->a:Z

    if-eqz v0, :cond_2

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/service/ow/j;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v2

    long-to-int v7, v0

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/service/ow/j;->d()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v0

    iget-object v11, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v1, p0

    move-object/from16 v2, p3

    move-object v6, v5

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/wallet/service/ow/m;->a(Lcom/google/android/gms/wallet/common/b;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/p;Lcom/google/android/gms/wallet/service/ow/j;Lcom/google/android/gms/wallet/service/ow/j;IJLjava/lang/String;Ljava/lang/String;)V

    const-string v0, "used"

    invoke-virtual {v5, v0}, Lcom/google/android/gms/wallet/service/ow/j;->a(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/service/ow/j;->d()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/service/ow/j;->e()Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    :goto_1
    :try_start_4
    monitor-exit v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 82
    :goto_2
    return-object v0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 79
    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 81
    :catch_1
    move-exception v0

    :try_start_6
    const-string v0, "FullWalletRequester"

    const-string v1, "getFullWallet interrupted!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    sget-object v2, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v1, v2}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    monitor-exit v13
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    .line 85
    :catchall_1
    move-exception v0

    monitor-exit v13

    throw v0

    .line 79
    :catch_2
    move-exception v0

    :try_start_7
    const-string v1, "FullWalletRequester"

    const-string v4, "Unexpected exception in getFullWallet"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    iget-boolean v0, v4, Lcom/google/android/gms/wallet/service/ow/p;->a:Z

    if-nez v0, :cond_3

    const-string v0, "unused(insufficient_prefetch_amount)"

    invoke-virtual {v5, v0}, Lcom/google/android/gms/wallet/service/ow/j;->a(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/m;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/m;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v6

    invoke-static {v0, v1, v6, p1}, Lcom/google/android/gms/wallet/service/ow/j;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/j;

    move-result-object v1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    if-eqz v5, :cond_4

    iget-boolean v0, v4, Lcom/google/android/gms/wallet/service/ow/p;->a:Z

    if-nez v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/wallet/service/ow/j;

    const/4 v6, 0x0

    aput-object v1, v0, v6

    move-object v12, v0

    :goto_3
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/o;

    invoke-direct {v0, v12}, Lcom/google/android/gms/wallet/service/ow/o;-><init>([Lcom/google/android/gms/wallet/service/ow/j;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/o;->a()Lcom/google/android/gms/wallet/service/ow/j;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v2

    long-to-int v7, v0

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/service/ow/j;->d()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v0

    iget-object v11, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v1, p0

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/wallet/service/ow/m;->a(Lcom/google/android/gms/wallet/common/b;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/p;Lcom/google/android/gms/wallet/service/ow/j;Lcom/google/android/gms/wallet/service/ow/j;IJLjava/lang/String;Ljava/lang/String;)V

    array-length v1, v12

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v1, :cond_7

    aget-object v2, v12, v0

    if-ne v2, v6, :cond_6

    const-string v3, "used"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/service/ow/j;->a(Ljava/lang/String;)V

    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/wallet/service/ow/j;

    const/4 v6, 0x0

    aput-object v1, v0, v6

    const/4 v1, 0x1

    aput-object v5, v0, v1

    move-object v12, v0

    goto :goto_3

    :cond_6
    const-string v3, "unused(other_reason)"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/service/ow/j;->a(Ljava/lang/String;)V

    goto :goto_5

    :cond_7
    invoke-virtual {v6}, Lcom/google/android/gms/wallet/service/ow/j;->d()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/service/ow/j;->e()Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v0

    goto/16 :goto_1
.end method
