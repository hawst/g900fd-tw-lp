.class public abstract Lcom/google/android/gms/plus/audience/al;
.super Lcom/google/android/gms/plus/audience/an;
.source "SourceFile"


# instance fields
.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field n:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/an;-><init>()V

    return-void
.end method

.method protected static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 40
    const-string v0, "Account name must not be empty."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 42
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 43
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v1, "plusPageId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v1, "client_application_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic b()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 15
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/an;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method protected c()Landroid/view/View;
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract e()V
.end method

.method protected abstract f()Landroid/widget/BaseAdapter;
.end method

.method protected final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/al;->j:Ljava/lang/String;

    return-object v0
.end method

.method protected final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/al;->k:Ljava/lang/String;

    return-object v0
.end method

.method protected final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/al;->l:Ljava/lang/String;

    return-object v0
.end method

.method protected final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/al;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/an;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/an;->onActivityCreated(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/al;->c()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/al;->a()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/al;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/al;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/al;->e()V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/al;->f()Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/al;->a(Landroid/widget/ListAdapter;)V

    .line 60
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/an;->onCreate(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/al;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 87
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/al;->j:Ljava/lang/String;

    .line 88
    const-string v1, "plusPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/al;->k:Ljava/lang/String;

    .line 89
    const-string v1, "client_application_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/al;->l:Ljava/lang/String;

    .line 90
    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/al;->m:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/al;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    sget-object v0, Lcom/google/android/gms/common/analytics/a;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/al;->l:Ljava/lang/String;

    .line 95
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/an;->onStart()V

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/al;->n:Z

    .line 75
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/al;->n:Z

    .line 80
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/an;->onStop()V

    .line 81
    return-void
.end method
