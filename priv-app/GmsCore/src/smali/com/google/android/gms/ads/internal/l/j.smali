.class public final Lcom/google/android/gms/ads/internal/l/j;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/ads/internal/p/a;

.field public final b:Lcom/google/android/gms/ads/internal/c/a;

.field c:Landroid/util/DisplayMetrics;

.field public d:F

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:[I

.field private final k:Landroid/content/Context;

.field private final l:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/p/a;Landroid/content/Context;Lcom/google/android/gms/ads/internal/c/a;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x43200000    # 160.0f

    const/4 v0, -0x1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput v0, p0, Lcom/google/android/gms/ads/internal/l/j;->e:I

    .line 46
    iput v0, p0, Lcom/google/android/gms/ads/internal/l/j;->f:I

    .line 52
    iput v0, p0, Lcom/google/android/gms/ads/internal/l/j;->h:I

    .line 53
    iput v0, p0, Lcom/google/android/gms/ads/internal/l/j;->i:I

    .line 54
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/j;->j:[I

    .line 60
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/l/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    .line 61
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/l/j;->k:Landroid/content/Context;

    .line 62
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/l/j;->b:Lcom/google/android/gms/ads/internal/c/a;

    .line 63
    const-string v0, "window"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/j;->l:Landroid/view/WindowManager;

    .line 64
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/j;->c:Landroid/util/DisplayMetrics;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/j;->l:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/j;->c:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/j;->c:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/gms/ads/internal/l/j;->d:F

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/l/j;->g:I

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/j;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->c(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/j;->c:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    div-float v1, v3, v1

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/l/j;->c:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/ads/internal/l/j;->e:I

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/l/j;->c:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/l/j;->f:I

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/j;->j:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->getLocationOnScreen([I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0, v4, v4}, Lcom/google/android/gms/ads/internal/p/a;->measure(II)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/j;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    div-float v0, v3, v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/ads/internal/l/j;->h:I

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/p/a;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/l/j;->i:I

    .line 67
    return-void
.end method
