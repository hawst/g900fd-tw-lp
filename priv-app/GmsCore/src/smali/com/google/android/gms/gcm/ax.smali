.class final Lcom/google/android/gms/gcm/ax;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/gcm/aw;


# direct methods
.method constructor <init>(Lcom/google/android/gms/gcm/aw;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/gms/gcm/ax;->a:Lcom/google/android/gms/gcm/aw;

    .line 57
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 58
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 94
    const-string v0, "CREATE TABLE registrations (_id INTEGER PRIMARY KEY,package_name STRING,sender_id STRING,uid INTEGER,reg_id STRING,timestamp_ms INTEGER);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 101
    const-string v0, "CREATE INDEX registration_index ON registrations(package_name, sender_id, uid)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 103
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 69
    invoke-static {p1}, Lcom/google/android/gms/gcm/ax;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 70
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x3

    if-ge p2, v0, :cond_0

    .line 84
    const-string v0, "DROP TABLE registrations"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/gcm/ax;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 86
    :cond_0
    return-void
.end method
