.class final Lcom/google/android/gms/cast/media/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/internal/DeviceStatus;

.field final synthetic b:Lcom/google/android/gms/cast/media/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/d;Lcom/google/android/gms/cast/internal/DeviceStatus;)V
    .locals 0

    .prologue
    .line 919
    iput-object p1, p0, Lcom/google/android/gms/cast/media/n;->b:Lcom/google/android/gms/cast/media/d;

    iput-object p2, p0, Lcom/google/android/gms/cast/media/n;->a:Lcom/google/android/gms/cast/internal/DeviceStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/android/gms/cast/media/n;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/y;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 940
    :cond_0
    :goto_0
    return-void

    .line 926
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/n;->a:Lcom/google/android/gms/cast/internal/DeviceStatus;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/DeviceStatus;->b()D

    move-result-wide v2

    .line 927
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    cmpl-double v0, v2, v0

    if-eqz v0, :cond_0

    .line 928
    const/4 v0, 0x0

    .line 930
    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/gms/cast/media/n;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v4, v4, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    iget-object v4, v4, Lcom/google/android/gms/cast/media/y;->e:Ljava/util/List;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 933
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 934
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/media/o;->a(D)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 935
    goto :goto_1

    .line 936
    :cond_2
    if-eqz v1, :cond_0

    .line 937
    iget-object v0, p0, Lcom/google/android/gms/cast/media/n;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->f(Lcom/google/android/gms/cast/media/a;)V

    goto :goto_0
.end method
