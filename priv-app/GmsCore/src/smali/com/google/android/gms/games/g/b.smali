.class public final Lcom/google/android/gms/games/g/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[Ljava/lang/String;

.field public c:I

.field public d:[Lcom/google/android/gms/games/g/c;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3677
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3678
    iput v1, p0, Lcom/google/android/gms/games/g/b;->a:I

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/b;->c:I

    invoke-static {}, Lcom/google/android/gms/games/g/c;->a()[Lcom/google/android/gms/games/g/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/b;->cachedSize:I

    .line 3679
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3758
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3759
    iget v1, p0, Lcom/google/android/gms/games/g/b;->a:I

    if-eqz v1, :cond_0

    .line 3760
    const/4 v1, 0x1

    iget v3, p0, Lcom/google/android/gms/games/g/b;->a:I

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3763
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    move v4, v2

    .line 3766
    :goto_0
    iget-object v5, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_2

    .line 3767
    iget-object v5, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 3768
    if-eqz v5, :cond_1

    .line 3769
    add-int/lit8 v4, v4, 0x1

    .line 3770
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 3766
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3774
    :cond_2
    add-int/2addr v0, v3

    .line 3775
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 3777
    :cond_3
    iget v1, p0, Lcom/google/android/gms/games/g/b;->c:I

    if-eqz v1, :cond_4

    .line 3778
    const/4 v1, 0x3

    iget v3, p0, Lcom/google/android/gms/games/g/b;->c:I

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3781
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 3782
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    array-length v1, v1

    if-ge v2, v1, :cond_6

    .line 3783
    iget-object v1, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    aget-object v1, v1, v2

    .line 3784
    if-eqz v1, :cond_5

    .line 3785
    const/4 v3, 0x4

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3782
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3790
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3692
    if-ne p1, p0, :cond_1

    .line 3713
    :cond_0
    :goto_0
    return v0

    .line 3695
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 3696
    goto :goto_0

    .line 3698
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/b;

    .line 3699
    iget v2, p0, Lcom/google/android/gms/games/g/b;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/b;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 3700
    goto :goto_0

    .line 3702
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3704
    goto :goto_0

    .line 3706
    :cond_4
    iget v2, p0, Lcom/google/android/gms/games/g/b;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/b;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 3707
    goto :goto_0

    .line 3709
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    iget-object v3, p1, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3711
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 3718
    iget v0, p0, Lcom/google/android/gms/games/g/b;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 3720
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3722
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/b;->c:I

    add-int/2addr v0, v1

    .line 3723
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3725
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3639
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/b;->a:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/b;->c:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/games/g/c;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/games/g/c;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/games/g/c;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3731
    iget v0, p0, Lcom/google/android/gms/games/g/b;->a:I

    if-eqz v0, :cond_0

    .line 3732
    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/b;->a:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3734
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 3735
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 3736
    iget-object v2, p0, Lcom/google/android/gms/games/g/b;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 3737
    if-eqz v2, :cond_1

    .line 3738
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3735
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3742
    :cond_2
    iget v0, p0, Lcom/google/android/gms/games/g/b;->c:I

    if-eqz v0, :cond_3

    .line 3743
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/b;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3745
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 3746
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 3747
    iget-object v0, p0, Lcom/google/android/gms/games/g/b;->d:[Lcom/google/android/gms/games/g/c;

    aget-object v0, v0, v1

    .line 3748
    if-eqz v0, :cond_4

    .line 3749
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3746
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3753
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3754
    return-void
.end method
