.class public final Lcom/google/android/gms/plus/apps/al;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/plus/internal/aw;


# instance fields
.field private a:Lcom/google/android/gms/plus/internal/ad;

.field private b:Lcom/google/android/gms/plus/internal/ab;

.field private c:Landroid/accounts/Account;

.field private d:Z

.field private e:Lcom/google/android/gms/plus/apps/am;

.field private f:Z

.field private g:Lcom/google/android/gms/plus/model/a/a;

.field private h:Z

.field private i:Lcom/google/android/gms/common/c;

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 66
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/al;->a:Lcom/google/android/gms/plus/internal/ad;

    .line 67
    return-void
.end method

.method public static a(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/al;
    .locals 3

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    new-instance v1, Lcom/google/android/gms/plus/apps/al;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/al;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/plus/apps/al;->a:Lcom/google/android/gms/plus/internal/ad;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/al;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final T_()V
    .locals 6

    .prologue
    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->d:Z

    .line 157
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->f:Z

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/al;->b:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/al;->g:Lcom/google/android/gms/plus/model/a/a;

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/a/a;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/al;->g:Lcom/google/android/gms/plus/model/a/a;

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/gms/plus/apps/al;->h:Z

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/al;->g:Lcom/google/android/gms/plus/model/a/a;

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/a/a;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/al;->g:Lcom/google/android/gms/plus/model/a/a;

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/a/a;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v5, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    :goto_0
    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/aw;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 163
    :cond_0
    return-void

    .line 158
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final W_()V
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->f:Z

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/al;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->d:Z

    .line 173
    :goto_0
    return-void

    .line 171
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->d:Z

    goto :goto_0
.end method

.method protected final a()V
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->j:Z

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/al;->i:Lcom/google/android/gms/common/c;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/al;->b(Lcom/google/android/gms/common/c;)V

    .line 201
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 143
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/al;->d:Z

    .line 144
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/al;->i:Lcom/google/android/gms/common/c;

    .line 145
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->j:Z

    .line 146
    iget-boolean v2, p0, Lcom/google/android/gms/plus/apps/al;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/al;->e:Lcom/google/android/gms/plus/apps/am;

    if-eqz v2, :cond_0

    .line 147
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/al;->e:Lcom/google/android/gms/plus/apps/am;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/al;->g:Lcom/google/android/gms/plus/model/a/a;

    invoke-interface {v2, p1, v3}, Lcom/google/android/gms/plus/apps/am;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/a/a;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->j:Z

    .line 149
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->j:Z

    if-nez v0, :cond_1

    .line 150
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/al;->f:Z

    .line 152
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 147
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/model/a/a;Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 119
    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/al;->f:Z

    if-eqz v1, :cond_2

    .line 120
    const-string v0, "DisconnectSource"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const-string v0, "DisconnectSource"

    const-string v1, "Can only disconnect one app at a time."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_0
    const/4 v0, 0x0

    .line 138
    :cond_1
    :goto_0
    return v0

    .line 126
    :cond_2
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->f:Z

    .line 128
    invoke-static {p1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/al;->g:Lcom/google/android/gms/plus/model/a/a;

    .line 129
    iput-boolean p2, p0, Lcom/google/android/gms/plus/apps/al;->h:Z

    .line 131
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/al;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 132
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/al;->T_()V

    goto :goto_0

    .line 133
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/al;->d:Z

    if-nez v1, :cond_1

    .line 134
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/al;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 135
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->d:Z

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 177
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/al;->i:Lcom/google/android/gms/common/c;

    .line 178
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->j:Z

    .line 179
    iget-boolean v2, p0, Lcom/google/android/gms/plus/apps/al;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/al;->e:Lcom/google/android/gms/plus/apps/am;

    if-eqz v2, :cond_0

    .line 180
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/al;->e:Lcom/google/android/gms/plus/apps/am;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/al;->g:Lcom/google/android/gms/plus/model/a/a;

    invoke-interface {v2, p1, v3}, Lcom/google/android/gms/plus/apps/am;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/a/a;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->j:Z

    .line 182
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->j:Z

    if-nez v0, :cond_1

    .line 183
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/al;->f:Z

    .line 185
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 180
    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 79
    instance-of v0, p1, Lcom/google/android/gms/plus/apps/am;

    if-eqz v0, :cond_0

    .line 80
    check-cast p1, Lcom/google/android/gms/plus/apps/am;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/al;->e:Lcom/google/android/gms/plus/apps/am;

    return-void

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DisconnectSourceFragment must be hosted by an activity that implements DisconnectSourceCallbacks."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 96
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/al;->setRetainInstance(Z)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/al;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/al;->c:Landroid/accounts/Account;

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/al;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/al;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/al;->c:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1, p0, p0, v2}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/al;->b:Lcom/google/android/gms/plus/internal/ab;

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->j:Z

    .line 101
    return-void
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 105
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/al;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/al;->d:Z

    if-eqz v0, :cond_1

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/al;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 111
    :cond_1
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/al;->b:Lcom/google/android/gms/plus/internal/ab;

    .line 112
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/al;->d:Z

    .line 113
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/al;->f:Z

    .line 114
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/al;->g:Lcom/google/android/gms/plus/model/a/a;

    .line 116
    return-void
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/al;->e:Lcom/google/android/gms/plus/apps/am;

    .line 91
    return-void
.end method
