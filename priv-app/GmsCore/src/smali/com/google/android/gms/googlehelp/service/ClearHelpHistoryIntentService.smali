.class public Lcom/google/android/gms/googlehelp/service/ClearHelpHistoryIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "ClearHelpHistoryIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/googlehelp/service/ClearHelpHistoryIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 54
    const-string v1, "app_package_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 56
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 42
    const-string v0, "app_package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    new-instance v1, Lcom/google/android/gms/googlehelp/d/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/d/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/d/d;->a(Ljava/lang/String;)I

    .line 48
    new-instance v1, Lcom/google/android/gms/googlehelp/d/b;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/googlehelp/d/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/d/b;->d()I

    .line 49
    new-instance v1, Lcom/google/android/gms/googlehelp/search/a;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/googlehelp/search/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/search/a;->e()I

    goto :goto_0
.end method
