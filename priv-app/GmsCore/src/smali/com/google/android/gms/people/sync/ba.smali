.class final Lcom/google/android/gms/people/sync/ba;
.super Lcom/google/android/gms/people/sync/az;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/sync/ap;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/sync/ap;)V
    .locals 1

    .prologue
    .line 2730
    iput-object p1, p0, Lcom/google/android/gms/people/sync/ba;->b:Lcom/google/android/gms/people/sync/ap;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/sync/az;-><init>(Lcom/google/android/gms/people/sync/ap;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/sync/ap;B)V
    .locals 0

    .prologue
    .line 2730
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/ba;-><init>(Lcom/google/android/gms/people/sync/ap;)V

    return-void
.end method

.method private static a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)V
    .locals 4

    .prologue
    .line 2756
    const-string v0, "affinity1"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v1

    const-string v2, "emailAutocomplete"

    invoke-static {v1, v2}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2758
    const-string v0, "logging_id"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v1

    const-string v2, "emailAutocomplete"

    invoke-static {v1, v2}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2761
    const-string v0, "affinity2"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2762
    const-string v0, "affinity3"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2763
    const-string v0, "affinity4"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2764
    const-string v0, "affinity5"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2766
    const-string v0, "logging_id2"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2767
    const-string v0, "logging_id3"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2768
    const-string v0, "logging_id4"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2769
    const-string v0, "logging_id5"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2770
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 2733
    const/4 v0, 0x1

    return v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2730
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Landroid/content/ContentValues;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2730
    check-cast p2, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    const-string v0, "value"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "value_type"

    invoke-static {p2}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "custom_label"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/google/android/gms/people/sync/ba;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)V

    return-void
.end method

.method protected final a(Lcom/google/android/gms/people/sync/y;)V
    .locals 1

    .prologue
    .line 2774
    iget v0, p1, Lcom/google/android/gms/people/sync/y;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/google/android/gms/people/sync/y;->H:I

    .line 2775
    return-void
.end method

.method protected final synthetic b(Landroid/content/ContentValues;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2730
    check-cast p2, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    invoke-static {p1, p2}, Lcom/google/android/gms/people/sync/ba;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)V

    return-void
.end method

.method protected final b(Lcom/google/android/gms/people/sync/y;)V
    .locals 1

    .prologue
    .line 2779
    iget v0, p1, Lcom/google/android/gms/people/sync/y;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/google/android/gms/people/sync/y;->I:I

    .line 2780
    return-void
.end method
