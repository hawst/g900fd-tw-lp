.class final Lcom/google/android/gms/plus/audience/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/audience/ac;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/audience/ac;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/aj;->a:Lcom/google/android/gms/plus/audience/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/audience/ac;B)V
    .locals 0

    .prologue
    .line 430
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/audience/aj;-><init>(Lcom/google/android/gms/plus/audience/ac;)V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 8

    .prologue
    .line 433
    if-eqz p2, :cond_0

    const-string v0, "page_token"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 434
    :goto_0
    new-instance v0, Lcom/google/android/gms/plus/audience/a/k;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aj;->a:Lcom/google/android/gms/plus/audience/ac;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/ac;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/aj;->a:Lcom/google/android/gms/plus/audience/ac;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/audience/ac;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/aj;->a:Lcom/google/android/gms/plus/audience/ac;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/audience/ac;->n()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/aj;->a:Lcom/google/android/gms/plus/audience/ac;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/audience/ac;->k()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/gms/plus/c/a;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/audience/a/k;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    return-object v0

    .line 433
    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 464
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 430
    check-cast p2, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    if-eqz p2, :cond_0

    check-cast p1, Lcom/google/android/gms/plus/audience/a/k;

    iget-object v0, p1, Lcom/google/android/gms/plus/audience/a/k;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aj;->a:Lcom/google/android/gms/plus/audience/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/ac;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "page_token"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aj;->a:Lcom/google/android/gms/plus/audience/ac;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/ac;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v1

    const/4 v2, 0x3

    new-instance v3, Lcom/google/android/gms/plus/audience/aj;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/aj;->a:Lcom/google/android/gms/plus/audience/ac;

    invoke-direct {v3, v4}, Lcom/google/android/gms/plus/audience/aj;-><init>(Lcom/google/android/gms/plus/audience/ac;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aj;->a:Lcom/google/android/gms/plus/audience/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/ac;->o()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/o;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/audience/o;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    goto :goto_0
.end method
