.class final Lcom/google/android/gms/photos/autobackup/ui/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/o;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/o;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->g(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    .line 168
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->a(Z)V

    .line 169
    sget-object v1, Lcom/google/android/gms/photos/autobackup/d;->c:Lcom/google/android/gms/photos/autobackup/f;

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/o;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v2}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->d(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/o;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v3}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->a(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/photos/autobackup/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/photos/autobackup/f;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/o;->a:Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;->h(Lcom/google/android/gms/photos/autobackup/ui/AutoBackupSettingsActivity;)Lcom/google/android/gms/common/api/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 173
    const/4 v0, 0x1

    return v0
.end method
