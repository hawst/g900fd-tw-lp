.class public final Lcom/google/android/gms/drive/internal/model/About;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/drive/internal/model/a;

.field private static final e:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:J

.field d:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/drive/internal/model/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/model/About;->CREATOR:Lcom/google/android/gms/drive/internal/model/a;

    .line 198
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 201
    sput-object v0, Lcom/google/android/gms/drive/internal/model/About;->e:Ljava/util/HashMap;

    const-string v1, "largestChangeId"

    const-string v2, "largestChangeId"

    const/16 v3, 0xc

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/google/android/gms/drive/internal/model/About;->e:Ljava/util/HashMap;

    const-string v1, "remainingChangeIds"

    const-string v2, "remainingChangeIds"

    const/16 v3, 0x16

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 235
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/model/About;->b:I

    .line 236
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/About;->a:Ljava/util/Set;

    .line 237
    return-void
.end method

.method constructor <init>(Ljava/util/Set;IJJ)V
    .locals 1

    .prologue
    .line 245
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 246
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/About;->a:Ljava/util/Set;

    .line 247
    iput p2, p0, Lcom/google/android/gms/drive/internal/model/About;->b:I

    .line 248
    iput-wide p3, p0, Lcom/google/android/gms/drive/internal/model/About;->c:J

    .line 249
    iput-wide p5, p0, Lcom/google/android/gms/drive/internal/model/About;->d:J

    .line 250
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 207
    sget-object v0, Lcom/google/android/gms/drive/internal/model/About;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 381
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 382
    sparse-switch v0, :sswitch_data_0

    .line 390
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a long."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 384
    :sswitch_0
    iput-wide p3, p0, Lcom/google/android/gms/drive/internal/model/About;->c:J

    .line 393
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/About;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 394
    return-void

    .line 387
    :sswitch_1
    iput-wide p3, p0, Lcom/google/android/gms/drive/internal/model/About;->d:J

    goto :goto_0

    .line 382
    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0x16 -> :sswitch_1
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/About;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 271
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/model/About;->c:J

    return-wide v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 344
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 350
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :sswitch_0
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/model/About;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 348
    :goto_0
    return-object v0

    :sswitch_1
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/model/About;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 344
    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0x16 -> :sswitch_1
    .end sparse-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/About;->a:Ljava/util/Set;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x0

    return v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 286
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/model/About;->d:J

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 318
    sget-object v0, Lcom/google/android/gms/drive/internal/model/About;->CREATOR:Lcom/google/android/gms/drive/internal/model/a;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/About;->a:Ljava/util/Set;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 411
    instance-of v0, p1, Lcom/google/android/gms/drive/internal/model/About;

    if-nez v0, :cond_0

    move v0, v1

    .line 442
    :goto_0
    return v0

    .line 416
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 417
    goto :goto_0

    .line 420
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/internal/model/About;

    .line 421
    sget-object v0, Lcom/google/android/gms/drive/internal/model/About;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 422
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/About;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 423
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/About;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 425
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/About;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/About;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 427
    goto :goto_0

    :cond_3
    move v0, v1

    .line 432
    goto :goto_0

    .line 435
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/About;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 437
    goto :goto_0

    :cond_5
    move v0, v2

    .line 442
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 398
    const/4 v0, 0x0

    .line 399
    sget-object v1, Lcom/google/android/gms/drive/internal/model/About;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 400
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/About;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 401
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 402
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/About;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 404
    goto :goto_0

    .line 405
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 323
    sget-object v0, Lcom/google/android/gms/drive/internal/model/About;->CREATOR:Lcom/google/android/gms/drive/internal/model/a;

    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/model/a;->a(Lcom/google/android/gms/drive/internal/model/About;Landroid/os/Parcel;)V

    .line 324
    return-void
.end method
