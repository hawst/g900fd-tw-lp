.class public final Lcom/google/android/gms/drive/realtime/cache/a/a;
.super Lcom/google/android/gms/drive/database/model/ae;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/drive/realtime/cache/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/realtime/cache/a/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/a;->a:Lcom/google/android/gms/drive/realtime/cache/a/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ae;-><init>(I)V

    .line 36
    return-void
.end method

.method public static a()Lcom/google/android/gms/drive/realtime/cache/a/a;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/a/a;->a:Lcom/google/android/gms/drive/realtime/cache/a/a;

    return-object v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 154
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 155
    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/b;->a:Lcom/google/android/gms/drive/realtime/cache/a/b;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/a/b;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/b;->b:Lcom/google/android/gms/drive/realtime/cache/a/b;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/a/b;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/drive/realtime/cache/a/a;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 159
    return-void
.end method

.method private c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/drive/realtime/cache/a/a;->e()Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/a/b;->b:Lcom/google/android/gms/drive/realtime/cache/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/a/b;->name()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/google/android/gms/drive/realtime/cache/a/b;->a:Lcom/google/android/gms/drive/realtime/cache/a/b;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/realtime/cache/a/b;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " = ?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    aput-object p2, v4, v6

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 172
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eq v0, v8, :cond_0

    .line 173
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 178
    :goto_0
    return-object v5

    .line 175
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 176
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 178
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic d()Lcom/google/android/gms/drive/realtime/cache/a/a;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/a/a;->a:Lcom/google/android/gms/drive/realtime/cache/a/a;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string v0, "snapshot"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/realtime/cache/a/a;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 2

    .prologue
    .line 109
    const-string v0, "revision"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 78
    const-string v0, "snapshot"

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    const-string v0, "DocumentStore"

    return-object v0
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 95
    const-string v0, "save"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    const-string v0, "session"

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public final c(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 1

    .prologue
    .line 116
    const-string v0, "revision"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/realtime/cache/a/a;->c(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic c()[Lcom/google/android/gms/drive/g/ak;
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/b;->values()[Lcom/google/android/gms/drive/realtime/cache/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 124
    const-string v0, "request"

    const-string v1, "0"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-void
.end method
