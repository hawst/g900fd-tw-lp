.class public final Lcom/google/android/gms/plus/service/a/r;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/f;

.field private final c:Lcom/google/android/gms/plus/model/posts/Comment;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/r;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/r;->b:Lcom/google/android/gms/plus/internal/f;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/r;->c:Lcom/google/android/gms/plus/model/posts/Comment;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/r;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/r;->c:Lcom/google/android/gms/plus/model/posts/Comment;

    iget-object v2, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/plus/b/n;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/model/posts/Comment;)Lcom/google/android/gms/plus/model/posts/Comment;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/r;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 52
    :goto_0
    return-void

    .line 45
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/r;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/r;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x4

    invoke-interface {v1, v2, v0, v4}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V

    goto :goto_0

    .line 48
    :catch_1
    move-exception v0

    .line 49
    const-string v1, "InsertCommentOperation"

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/r;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v4, v4}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/r;->b:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/r;->b:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V

    .line 59
    :cond_0
    return-void
.end method
