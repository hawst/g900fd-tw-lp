.class public final enum Lcom/google/android/gms/drive/g/o;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/g/o;

.field public static final enum b:Lcom/google/android/gms/drive/g/o;

.field public static final enum c:Lcom/google/android/gms/drive/g/o;

.field private static final synthetic e:[Lcom/google/android/gms/drive/g/o;


# instance fields
.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 16
    new-instance v0, Lcom/google/android/gms/drive/g/o;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/gms/drive/g/o;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/drive/g/o;->a:Lcom/google/android/gms/drive/g/o;

    .line 17
    new-instance v0, Lcom/google/android/gms/drive/g/o;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/gms/drive/g/o;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/drive/g/o;->b:Lcom/google/android/gms/drive/g/o;

    .line 18
    new-instance v0, Lcom/google/android/gms/drive/g/o;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/g/o;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/drive/g/o;->c:Lcom/google/android/gms/drive/g/o;

    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/drive/g/o;

    sget-object v1, Lcom/google/android/gms/drive/g/o;->a:Lcom/google/android/gms/drive/g/o;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/g/o;->b:Lcom/google/android/gms/drive/g/o;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/g/o;->c:Lcom/google/android/gms/drive/g/o;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/drive/g/o;->e:[Lcom/google/android/gms/drive/g/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    iput-boolean p3, p0, Lcom/google/android/gms/drive/g/o;->d:Z

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/g/o;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/android/gms/drive/g/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/o;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/g/o;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/gms/drive/g/o;->e:[Lcom/google/android/gms/drive/g/o;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/g/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/g/o;

    return-object v0
.end method
