.class public abstract Lcom/google/android/gms/drive/database/b/a;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/database/b/d;


# instance fields
.field private a:Z

.field private final b:I


# direct methods
.method protected constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 18
    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/b/a;->a:Z

    .line 26
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Size must be nonnegative."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 27
    iput p1, p0, Lcom/google/android/gms/drive/database/b/a;->b:I

    .line 28
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/b/a;->a:Z

    if-eqz v0, :cond_0

    .line 75
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Result list is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(I)Ljava/lang/Object;
.end method

.method protected abstract a()V
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/b/a;->c()V

    .line 67
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/b/a;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/b/a;->close()V

    throw v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/b/a;->a:Z

    if-nez v0, :cond_0

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/database/b/a;->a:Z

    .line 34
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/b/a;->a()V

    .line 36
    :cond_0
    return-void
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/b/a;->c()V

    .line 46
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/b/a;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 47
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 49
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/b/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/b/a;->c()V

    .line 60
    iget v0, p0, Lcom/google/android/gms/drive/database/b/a;->b:I

    return v0
.end method
