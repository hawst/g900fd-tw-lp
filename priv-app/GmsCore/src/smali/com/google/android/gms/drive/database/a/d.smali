.class public final Lcom/google/android/gms/drive/database/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/database/a/c;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/model/ah;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/ah;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/gms/drive/database/a/d;->a:Lcom/google/android/gms/drive/database/model/ah;

    .line 21
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/drive/database/a/d;->a:Lcom/google/android/gms/drive/database/model/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->z()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    sget-object v0, Lcom/google/android/gms/drive/database/model/ak;->a:Lcom/google/android/gms/drive/database/model/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ak;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/drive/database/model/ak;->b:Lcom/google/android/gms/drive/database/model/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ak;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v0, Lcom/google/android/gms/drive/database/model/ai;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/model/ai;-><init>(Lcom/google/android/gms/drive/database/i;JJ)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/aj;->a()Lcom/google/android/gms/drive/database/model/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/aj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/ai;->d(J)V

    iget-object v1, p0, Lcom/google/android/gms/drive/database/a/d;->a:Lcom/google/android/gms/drive/database/model/ah;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v2

    iget-wide v4, v0, Lcom/google/android/gms/drive/database/model/ai;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
