.class public final Lcom/google/android/gms/cast_mirroring/a;
.super Lcom/google/android/gms/cast_mirroring/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/cast_mirroring/e;-><init>(Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V

    .line 134
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 138
    sget-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast_mirroring/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast_mirroring/b;-><init>(Lcom/google/android/gms/cast_mirroring/a;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 153
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/a;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 156
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/a;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    const/16 v1, 0xe

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast_mirroring/b/a;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 159
    :catch_1
    move-exception v0

    goto :goto_0
.end method
