.class public final Lcom/google/android/gms/wallet/service/q;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/c/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/c/a;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/q;->a:Lcom/google/android/gms/wallet/c/a;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/internal/bg;I)V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/q;->a:Lcom/google/android/gms/wallet/c/a;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/c/a;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    const-string v0, "ServiceBroker"

    const-string v1, "Client died while brokering Wallet service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final k(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/q;->a:Lcom/google/android/gms/wallet/c/a;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/c/a;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    const-string v0, "ServiceBroker"

    const-string v1, "Client died while brokering Wallet service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
