.class public final enum Lcom/google/android/gms/drive/auth/a;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/auth/a;

.field public static final enum b:Lcom/google/android/gms/drive/auth/a;

.field private static final synthetic c:[Lcom/google/android/gms/drive/auth/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/google/android/gms/drive/auth/a;

    const-string v1, "AUTHORIZED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/auth/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/auth/a;->a:Lcom/google/android/gms/drive/auth/a;

    .line 19
    new-instance v0, Lcom/google/android/gms/drive/auth/a;

    const-string v1, "UNAUTHORIZED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/drive/auth/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/auth/a;->b:Lcom/google/android/gms/drive/auth/a;

    .line 8
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/auth/a;

    sget-object v1, Lcom/google/android/gms/drive/auth/a;->a:Lcom/google/android/gms/drive/auth/a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/auth/a;->b:Lcom/google/android/gms/drive/auth/a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/drive/auth/a;->c:[Lcom/google/android/gms/drive/auth/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/auth/a;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/android/gms/drive/auth/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/auth/a;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/gms/drive/auth/a;->c:[Lcom/google/android/gms/drive/auth/a;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/auth/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/auth/a;

    return-object v0
.end method
