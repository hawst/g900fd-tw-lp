.class public final Lcom/google/android/gms/audiomodem/bb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/media/AudioRecord;

.field final b:I

.field final c:Z

.field final d:I

.field final e:I

.field private final f:I


# direct methods
.method private constructor <init>(IIZII)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/google/android/gms/audiomodem/bb;->f:I

    .line 44
    iput p2, p0, Lcom/google/android/gms/audiomodem/bb;->b:I

    .line 45
    iput-boolean p3, p0, Lcom/google/android/gms/audiomodem/bb;->c:Z

    .line 46
    iput p4, p0, Lcom/google/android/gms/audiomodem/bb;->d:I

    .line 47
    iput p5, p0, Lcom/google/android/gms/audiomodem/bb;->e:I

    .line 48
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 251
    const-string v0, "HOTWORD"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const/16 v0, 0x7cf

    .line 263
    :goto_0
    return v0

    .line 256
    :cond_0
    :try_start_0
    const-class v0, Landroid/media/MediaRecorder$AudioSource;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 257
    :catch_0
    move-exception v0

    .line 258
    :goto_1
    const-string v2, "audioModem"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 259
    const-string v2, "RecordingConfiguration: Unable to parse audio source"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lcom/google/android/gms/audiomodem/as;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    move v0, v1

    .line 263
    goto :goto_0

    .line 257
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method private static a(II)Lcom/google/android/gms/audiomodem/bb;
    .locals 4

    .prologue
    const v3, 0xac44

    const/4 v2, 0x3

    .line 125
    const/16 v0, 0x10

    const/4 v1, 0x0

    invoke-static {p1, p0, v0, v1}, Lcom/google/android/gms/audiomodem/bb;->a(IIIZ)Lcom/google/android/gms/audiomodem/bb;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_1

    .line 128
    const-string v1, "audioModem"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecordingConfiguration: Recording in mono with source "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/audiomodem/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", channelConfig: CHANNEL_IN_MONO"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "audioModem"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_0
    :goto_0
    return-object v0

    .line 136
    :cond_1
    if-eq p0, v3, :cond_3

    .line 137
    const-string v0, "audioModem"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RecordingConfiguration: Failed to create "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "hz channel. Trying fallback..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :cond_2
    invoke-static {v3, p1}, Lcom/google/android/gms/audiomodem/bb;->a(II)Lcom/google/android/gms/audiomodem/bb;

    move-result-object v0

    .line 142
    if-eqz v0, :cond_3

    .line 143
    const-string v1, "audioModem"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RecordingConfiguration: Recording in mono with source "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/audiomodem/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", channelConfig: CHANNEL_IN_MONO, sampleRateHz: 44100"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "audioModem"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 154
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to create a mono recording configuration for source "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/audiomodem/bb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(IIIZ)Lcom/google/android/gms/audiomodem/bb;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x2

    .line 174
    invoke-static {p1, p2, v4}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v7

    .line 175
    if-gtz v7, :cond_0

    move-object v0, v6

    .line 188
    :goto_0
    return-object v0

    .line 178
    :cond_0
    new-instance v0, Landroid/media/AudioRecord;

    invoke-static {v7}, Lcom/google/android/gms/audiomodem/bb;->b(I)I

    move-result v5

    move v1, p0

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    .line 184
    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    move-object v0, v6

    .line 185
    goto :goto_0

    .line 187
    :cond_1
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 188
    new-instance v0, Lcom/google/android/gms/audiomodem/bb;

    move v1, p2

    move v2, p0

    move v3, p3

    move v4, p1

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/audiomodem/bb;-><init>(IIZII)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/audiomodem/bb;
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/bb;->a(Ljava/lang/String;)I

    move-result v1

    .line 58
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 62
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v5, "android.permission.CAPTURE_AUDIO_HOTWORD"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v3

    .line 65
    :goto_0
    const/16 v5, 0x7cf

    if-ne v1, v5, :cond_1

    if-nez v0, :cond_1

    .line 66
    const-string v0, "audioModem"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const-string v0, "RecordingConfiguration: Falling back to default audio source"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v1, v2

    .line 72
    :cond_1
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    .line 73
    invoke-static {v4, v1}, Lcom/google/android/gms/audiomodem/bb;->a(II)Lcom/google/android/gms/audiomodem/bb;

    move-result-object v0

    .line 117
    :cond_2
    :goto_1
    return-object v0

    :cond_3
    move v0, v2

    .line 62
    goto :goto_0

    .line 81
    :cond_4
    sget-object v0, Lcom/google/android/gms/audiomodem/m;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "\\|"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move v0, v2

    :goto_2
    array-length v6, v5

    if-ge v2, v6, :cond_5

    aget-object v6, v5, v2

    invoke-static {v6}, Lcom/google/android/gms/audiomodem/bb;->b(Ljava/lang/String;)I

    move-result v6

    or-int/2addr v0, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    invoke-static {v1, v4, v0, v3}, Lcom/google/android/gms/audiomodem/bb;->a(IIIZ)Lcom/google/android/gms/audiomodem/bb;

    move-result-object v0

    .line 86
    if-nez v0, :cond_2

    .line 91
    const/4 v0, 0x6

    const/16 v2, 0x30

    invoke-static {v0, v4, v2, v3}, Lcom/google/android/gms/audiomodem/bb;->a(IIIZ)Lcom/google/android/gms/audiomodem/bb;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_6

    .line 95
    const-string v1, "audioModem"

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 96
    const-string v1, "RecordingConfiguration: Recording in stereo with source VOICE_RECOGNITION, channelConfig: CHANNEL_IN_FRONT | CHANNEL_IN_BACK"

    const-string v2, "audioModem"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 104
    :cond_6
    const/16 v0, 0xc

    invoke-static {v8, v4, v0, v3}, Lcom/google/android/gms/audiomodem/bb;->a(IIIZ)Lcom/google/android/gms/audiomodem/bb;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_7

    .line 107
    const-string v1, "audioModem"

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 108
    const-string v1, "RecordingConfiguration: Recording in stereo with source CAMCORDER, channelConfig: CHANNEL_IN_STEREO"

    const-string v2, "audioModem"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 114
    :cond_7
    const-string v0, "audioModem"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 115
    const-string v0, "RecordingConfiguration: All stereo recording configurations failed. Falling back to mono"

    const-string v2, "audioModem"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_8
    invoke-static {v4, v1}, Lcom/google/android/gms/audiomodem/bb;->a(II)Lcom/google/android/gms/audiomodem/bb;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    sparse-switch p0, :sswitch_data_0

    .line 209
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 199
    :sswitch_0
    const-string v0, "MIC"

    goto :goto_0

    .line 201
    :sswitch_1
    const-string v0, "VOICE_RECOGNITION"

    goto :goto_0

    .line 203
    :sswitch_2
    const-string v0, "CAMCORDER"

    goto :goto_0

    .line 205
    :sswitch_3
    const-string v0, "DEFAULT"

    goto :goto_0

    .line 207
    :sswitch_4
    const-string v0, "HOTWORD"

    goto :goto_0

    .line 197
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x1 -> :sswitch_0
        0x5 -> :sswitch_2
        0x6 -> :sswitch_1
        0x7cf -> :sswitch_4
    .end sparse-switch
.end method

.method private static b(I)I
    .locals 2

    .prologue
    .line 164
    int-to-float v1, p0

    sget-object v0, Lcom/google/android/gms/audiomodem/m;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static b(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 280
    :try_start_0
    const-class v0, Landroid/media/AudioFormat;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 287
    :goto_0
    return v0

    .line 281
    :catch_0
    move-exception v0

    .line 282
    :goto_1
    const-string v2, "audioModem"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 283
    const-string v2, "RecordingConfiguration: Unable to parse channel config"

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/audiomodem/as;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    move v0, v1

    .line 287
    goto :goto_0

    .line 281
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a()Landroid/media/AudioRecord;
    .locals 6

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bb;->a:Landroid/media/AudioRecord;

    if-nez v0, :cond_0

    .line 215
    new-instance v0, Landroid/media/AudioRecord;

    iget v1, p0, Lcom/google/android/gms/audiomodem/bb;->b:I

    iget v2, p0, Lcom/google/android/gms/audiomodem/bb;->d:I

    iget v3, p0, Lcom/google/android/gms/audiomodem/bb;->f:I

    const/4 v4, 0x2

    iget v5, p0, Lcom/google/android/gms/audiomodem/bb;->e:I

    invoke-static {v5}, Lcom/google/android/gms/audiomodem/bb;->b(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/bb;->a:Landroid/media/AudioRecord;

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bb;->a:Landroid/media/AudioRecord;

    return-object v0
.end method
