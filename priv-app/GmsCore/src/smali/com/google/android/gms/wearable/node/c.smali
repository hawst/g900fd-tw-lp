.class final Lcom/google/android/gms/wearable/node/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:[Lcom/google/android/gms/wearable/node/c;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 87
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/wearable/node/c;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/wearable/node/c;

    const-string v3, "com.google.android.wearable.app"

    const-string v4, "a197f9212f2fed64f0ff9c2a4edf24b9c8801c8c"

    const-string v5, "77f9bed9359a2a59a121fd52c7c46731049ac147"

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gms/wearable/node/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/gms/wearable/node/c;

    const-string v3, "com.google.glass.companion"

    const-string v4, "24bb24c05e47e0aefa68a58a766179d9b613a600"

    const-string v5, "4ba713dfece93d47572dc5e845a7a82c4a891f2f"

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gms/wearable/node/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/wearable/node/c;

    const-string v3, "com.google.android.gms"

    const-string v4, "38918a453d07199354f8b19af05ec6562ced5788"

    const-string v5, "58e1c4133f7441ec3d2c270270a14802da47ba0e"

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gms/wearable/node/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/gms/wearable/node/c;

    const-string v3, "com.google.android.apps.fitness"

    const-string v4, "24bb24c05e47e0aefa68a58a766179d9b613a600"

    const-string v5, "4ba713dfece93d47572dc5e845a7a82c4a891f2f"

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gms/wearable/node/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/wearable/node/c;->d:[Lcom/google/android/gms/wearable/node/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/c;->a:Ljava/lang/String;

    .line 108
    iput-object p2, p0, Lcom/google/android/gms/wearable/node/c;->b:Ljava/lang/String;

    .line 109
    iput-object p3, p0, Lcom/google/android/gms/wearable/node/c;->c:Ljava/lang/String;

    .line 110
    return-void
.end method

.method static synthetic a()[Lcom/google/android/gms/wearable/node/c;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/gms/wearable/node/c;->d:[Lcom/google/android/gms/wearable/node/c;

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/c;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/c;->c:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
