.class public final Lcom/google/android/gms/wallet/common/ui/ck;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/de;


# instance fields
.field protected a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field protected b:Landroid/accounts/Account;

.field private c:Ljava/util/HashSet;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ck;->c:Ljava/util/HashSet;

    return-void
.end method

.method public static a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;
    .locals 3

    .prologue
    .line 53
    const-string v0, "LoginRequiredFragment"

    invoke-virtual {p0, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ck;

    .line 55
    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ck;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/ck;-><init>()V

    .line 57
    invoke-virtual {p0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "LoginRequiredFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    .line 61
    :cond_0
    return-object v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ck;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/app/q;->setResult(ILandroid/content/Intent;)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ck;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    .line 132
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 113
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 116
    const-string v0, "LoginRequiredSecureFrag"

    const-string v1, "onAuthTokensError: Network failed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/common/ui/ck;->b(I)V

    .line 122
    :goto_0
    return-void

    .line 119
    :cond_0
    const-string v0, "LoginRequiredSecureFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAuthTokensError: Status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/common/ui/ck;->b(I)V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ck;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ck;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 109
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ck;->b(I)V

    .line 127
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ck;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 69
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Activity requires buyFlowConfig extra!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 71
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ck;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 72
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "Activity requires account extra!"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 74
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ck;->b:Landroid/accounts/Account;

    .line 76
    const-string v0, "com.google.android.gms.wallet.localMode"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 78
    if-nez v0, :cond_1

    .line 79
    if-eqz p1, :cond_0

    .line 80
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 84
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ck;->c:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ck;->c:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ck;->b:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ck;->c:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ck;->b:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ck;->b:Landroid/accounts/Account;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ck;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Lcom/google/android/gms/wallet/common/ui/de;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ck;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 90
    :cond_1
    :goto_0
    return-void

    .line 88
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ck;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dc;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Lcom/google/android/gms/wallet/common/ui/de;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ck;->c:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 98
    return-void
.end method
