.class public Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;
.super Landroid/widget/ScrollView;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/plus/audience/av;

.field private b:Lcom/google/android/gms/plus/audience/au;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/audience/au;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->b:Lcom/google/android/gms/plus/audience/au;

    .line 45
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/audience/av;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->a:Lcom/google/android/gms/plus/audience/av;

    .line 41
    return-void
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onOverScrolled(IIZZ)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->b:Lcom/google/android/gms/plus/audience/au;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->b:Lcom/google/android/gms/plus/audience/au;

    invoke-interface {v0, p2, p4}, Lcom/google/android/gms/plus/audience/au;->a(IZ)V

    .line 62
    :cond_0
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->a:Lcom/google/android/gms/plus/audience/av;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->a:Lcom/google/android/gms/plus/audience/av;

    invoke-interface {v0, p0, p2}, Lcom/google/android/gms/plus/audience/av;->a(Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;I)V

    .line 53
    :cond_0
    return-void
.end method
