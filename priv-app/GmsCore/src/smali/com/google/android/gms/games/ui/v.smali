.class final Lcom/google/android/gms/games/ui/v;
.super Landroid/support/v7/widget/cc;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/s;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/s;)V
    .locals 0

    .prologue
    .line 428
    iput-object p1, p0, Lcom/google/android/gms/games/ui/v;->a:Lcom/google/android/gms/games/ui/s;

    invoke-direct {p0}, Landroid/support/v7/widget/cc;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 432
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->c()Landroid/support/v7/widget/ce;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/am;

    .line 433
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ao;

    .line 435
    invoke-virtual {v1}, Landroid/support/v7/widget/ao;->a()I

    move-result v4

    .line 436
    invoke-virtual {v1}, Landroid/support/v7/widget/ao;->b()I

    move-result v1

    add-int v5, v4, v1

    .line 437
    if-nez v4, :cond_1

    move v1, v2

    .line 438
    :goto_0
    invoke-virtual {v0}, Landroid/support/v7/widget/am;->b()I

    move-result v0

    if-ne v5, v0, :cond_2

    .line 440
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/v;->a:Lcom/google/android/gms/games/ui/s;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ae;

    .line 442
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ae;->q()Lcom/google/android/gms/games/ui/ac;

    move-result-object v0

    .line 443
    if-nez v0, :cond_3

    .line 454
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v1, v3

    .line 437
    goto :goto_0

    :cond_2
    move v2, v3

    .line 438
    goto :goto_1

    .line 446
    :cond_3
    invoke-static {}, Lcom/google/android/gms/games/ui/ac;->s()V

    .line 450
    if-eqz v1, :cond_4

    iget v0, p1, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    cmpg-float v0, v0, v6

    if-ltz v0, :cond_5

    :cond_4
    if-eqz v2, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    cmpg-float v0, v0, v6

    if-gez v0, :cond_0

    .line 452
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/v;->a:Lcom/google/android/gms/games/ui/s;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/s;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setClipToPadding(Z)V

    goto :goto_2
.end method
