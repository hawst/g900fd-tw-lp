.class public final Lcom/google/android/gms/plus/sharebox/bh;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field protected a:[Ljava/lang/String;

.field protected b:I

.field private c:Lcom/google/android/gms/plus/sharebox/bi;

.field private d:Lcom/google/android/gms/common/account/a;

.field private e:Landroid/widget/Spinner;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->k:I

    return-void
.end method

.method private a()V
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v9

    .line 248
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/bh;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v3, v9, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v10

    .line 257
    iget v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->k:I

    packed-switch v0, :pswitch_data_0

    .line 274
    :pswitch_0
    iget-object v0, v9, Lcom/google/android/gms/plus/sharebox/bg;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    .line 275
    iget-object v0, v9, Lcom/google/android/gms/plus/sharebox/bg;->e:Ljava/lang/String;

    .line 279
    :goto_1
    sget v3, Lcom/google/android/gms/h;->cD:I

    move v4, v3

    move v5, v1

    move-object v7, v0

    move v3, v1

    move v0, v1

    .line 282
    :goto_2
    iget-object v11, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    invoke-virtual {v11, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 283
    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    invoke-virtual {v5, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 284
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 285
    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    invoke-virtual {v5, v1, v1, v4, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 289
    :goto_3
    if-eqz v0, :cond_5

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 300
    :goto_4
    if-eqz v3, :cond_6

    if-eqz v10, :cond_6

    .line 303
    sget v0, Lcom/google/android/gms/p;->vu:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/bh;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v3, v1

    .line 313
    :goto_5
    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/bh;->e:Landroid/widget/Spinner;

    if-eqz v3, :cond_a

    move v4, v1

    :goto_6
    invoke-virtual {v5, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 314
    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/bh;->g:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->a:[Ljava/lang/String;

    array-length v0, v0

    if-eq v0, v2, :cond_0

    if-nez v3, :cond_b

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->j:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 325
    :goto_7
    return-void

    :pswitch_1
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v6

    move-object v7, v8

    .line 260
    goto :goto_2

    .line 262
    :pswitch_2
    sget v0, Lcom/google/android/gms/p;->vH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/bh;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 263
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/gms/h;->cC:I

    :goto_8
    move v4, v0

    move v5, v1

    move-object v7, v3

    move v3, v2

    move v0, v1

    .line 267
    goto :goto_2

    .line 263
    :cond_1
    sget v0, Lcom/google/android/gms/h;->cy:I

    goto :goto_8

    :pswitch_3
    move v0, v2

    move v3, v2

    move v4, v1

    move v5, v6

    move-object v7, v8

    .line 272
    goto :goto_2

    :cond_2
    move v0, v1

    .line 274
    goto :goto_0

    .line 277
    :cond_3
    sget v0, Lcom/google/android/gms/p;->vN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/bh;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_1

    .line 287
    :cond_4
    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    invoke-virtual {v5, v4, v1, v1, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_3

    .line 293
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->i:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 306
    :cond_6
    invoke-virtual {v9}, Lcom/google/android/gms/plus/sharebox/bg;->c()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    .line 307
    :goto_9
    iget-object v3, v9, Lcom/google/android/gms/plus/sharebox/bg;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    move v3, v2

    :goto_a
    if-eqz v3, :cond_9

    .line 308
    iget-object v3, v9, Lcom/google/android/gms/plus/sharebox/bg;->c:Ljava/lang/String;

    move-object v12, v3

    move v3, v0

    move-object v0, v12

    goto :goto_5

    :cond_7
    move v0, v1

    .line 306
    goto :goto_9

    :cond_8
    move v3, v1

    .line 307
    goto :goto_a

    .line 310
    :cond_9
    sget v3, Lcom/google/android/gms/p;->sI:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/plus/sharebox/bh;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v12, v3

    move v3, v0

    move-object v0, v12

    goto/16 :goto_5

    .line 313
    :cond_a
    const/16 v4, 0x8

    goto/16 :goto_6

    .line 322
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->j:Landroid/view/View;

    sget v1, Lcom/google/android/gms/h;->L:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_7

    .line 257
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 242
    iput p1, p0, Lcom/google/android/gms/plus/sharebox/bh;->k:I

    .line 243
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/bh;->a()V

    .line 244
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 234
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 236
    return-void

    .line 234
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 215
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/bh;->a:[Ljava/lang/String;

    .line 216
    iput p2, p0, Lcom/google/android/gms/plus/sharebox/bh;->b:I

    .line 219
    new-instance v0, Lcom/google/android/gms/common/account/a;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/bh;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->eL:I

    sget v3, Lcom/google/android/gms/j;->f:I

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/bh;->a:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/account/a;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->d:Lcom/google/android/gms/common/account/a;

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->e:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->d:Lcom/google/android/gms/common/account/a;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->e:Landroid/widget/Spinner;

    iget v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->e:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 227
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/bh;->a()V

    .line 228
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 86
    instance-of v0, p1, Lcom/google/android/gms/plus/sharebox/bi;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/sharebox/bi;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/sharebox/bi;

    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    .line 91
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->rw:I

    if-ne v0, v1, :cond_2

    .line 178
    iget v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->k:I

    packed-switch v0, :pswitch_data_0

    .line 186
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/bh;->a(Z)V

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->q()V

    .line 193
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->sS:I

    if-ne v0, v1, :cond_1

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->performClick()Z

    .line 197
    :cond_1
    return-void

    .line 180
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->r()V

    goto :goto_0

    .line 183
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->s()V

    goto :goto_0

    .line 190
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->tD:I

    if-ne v0, v1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->s()V

    goto :goto_0

    .line 178
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 105
    if-eqz p1, :cond_0

    .line 106
    const-string v0, "account_names"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->a:[Ljava/lang/String;

    .line 107
    const-string v0, "selected_position"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->b:I

    .line 108
    const-string v0, "button_action"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->k:I

    .line 110
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 115
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 117
    sget v0, Lcom/google/android/gms/l;->eU:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 119
    sget v0, Lcom/google/android/gms/j;->i:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->e:Landroid/widget/Spinner;

    .line 120
    sget v0, Lcom/google/android/gms/j;->sX:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->g:Landroid/widget/TextView;

    .line 121
    sget v0, Lcom/google/android/gms/j;->sS:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->j:Landroid/view/View;

    .line 122
    sget v0, Lcom/google/android/gms/j;->tD:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->h:Landroid/view/View;

    .line 123
    sget v0, Lcom/google/android/gms/j;->tE:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->i:Landroid/view/View;

    .line 126
    sget v0, Lcom/google/android/gms/j;->rw:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    if-eqz p3, :cond_2

    const-string v0, "button_enabled"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->d:I

    if-lez v0, :cond_3

    :goto_1
    if-eqz v1, :cond_0

    .line 134
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/bh;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bi;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_0

    .line 137
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bi;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 139
    sget v0, Lcom/google/android/gms/j;->sY:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 140
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 150
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/bg;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    .line 152
    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/PlusPage;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    sget v0, Lcom/google/android/gms/j;->po:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 154
    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/PlusPage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/bh;->a()V

    .line 160
    return-object v3

    :cond_2
    move v0, v2

    .line 128
    goto :goto_0

    :cond_3
    move v1, v2

    .line 132
    goto :goto_1

    .line 142
    :catch_0
    move-exception v0

    .line 143
    const-string v1, "ShareBox"

    const-string v4, "Unable to override the app icon."

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 144
    :catch_1
    move-exception v0

    .line 145
    const-string v1, "ShareBox"

    const-string v4, "Unable to override the app icon."

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 201
    iget v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->b:I

    if-eq v0, p3, :cond_0

    .line 202
    iput p3, p0, Lcom/google/android/gms/plus/sharebox/bh;->b:I

    .line 203
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->d:Lcom/google/android/gms/common/account/a;

    iget v2, p0, Lcom/google/android/gms/plus/sharebox/bh;->b:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/account/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/gms/plus/sharebox/bi;->b(Ljava/lang/String;)V

    .line 205
    :cond_0
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    const-string v0, "account_names"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->a:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 97
    const-string v0, "selected_position"

    iget v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    const-string v0, "button_action"

    iget v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    const-string v0, "button_enabled"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->f:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 100
    return-void
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 165
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->a:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->b:I

    if-ltz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->a:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/bh;->a([Ljava/lang/String;I)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bi;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->d()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bh;->c:Lcom/google/android/gms/plus/sharebox/bi;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/bi;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/au;->e()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/bh;->a([Ljava/lang/String;I)V

    goto :goto_0
.end method
