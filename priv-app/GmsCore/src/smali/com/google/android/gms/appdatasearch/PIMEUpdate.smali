.class public Lcom/google/android/gms/appdatasearch/PIMEUpdate;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/aa;


# instance fields
.field final a:I

.field final b:[B

.field final c:[B

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field final h:Landroid/os/Bundle;

.field public final i:J

.field public final j:J

.field public final k:Landroid/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/appdatasearch/aa;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/aa;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lcom/google/android/gms/appdatasearch/aa;

    return-void
.end method

.method constructor <init>(I[B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJLandroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput p1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:I

    .line 88
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->b:[B

    .line 89
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->c:[B

    .line 90
    iput p4, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->d:I

    .line 91
    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->e:Ljava/lang/String;

    .line 92
    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->f:Ljava/lang/String;

    .line 93
    iput-boolean p7, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->g:Z

    .line 94
    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->h:Landroid/os/Bundle;

    .line 95
    iput-wide p9, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->i:J

    .line 96
    iput-wide p11, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->j:J

    .line 97
    iput-object p13, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->k:Landroid/accounts/Account;

    .line 98
    return-void
.end method

.method constructor <init>([B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJLandroid/accounts/Account;)V
    .locals 16

    .prologue
    .line 110
    const/4 v2, 0x1

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-wide/from16 v10, p8

    move-wide/from16 v12, p10

    move-object/from16 v14, p12

    invoke-direct/range {v1 .. v14}, Lcom/google/android/gms/appdatasearch/PIMEUpdate;-><init>(I[B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJLandroid/accounts/Account;)V

    .line 112
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 257
    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lcom/google/android/gms/appdatasearch/aa;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lcom/google/android/gms/appdatasearch/aa;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/aa;->a(Lcom/google/android/gms/appdatasearch/PIMEUpdate;Landroid/os/Parcel;I)V

    .line 264
    return-void
.end method
