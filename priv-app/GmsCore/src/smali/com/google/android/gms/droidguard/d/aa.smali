.class final Lcom/google/android/gms/droidguard/d/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/droidguard/d/z;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxW77dCKJ8mhEIfXXdeidi7/7LMNM/fzwI+wj1Ed8xIKgTYWCnekRko3JxQb4Cv/gEL5hEA8e9lFs3V67VUL6hCo1FxysXj7Q8n3Kp7hARDkbiZ0mdk8bSanqrPAXTPx6pEL2ZOzfFCHEtJdhz5Ozp2C4XTKF1SBv/YbpsqSUJwdhG7ZPGjyCMRloMww6ITpGdVQ8lChklkCek0WPbz2UrY5RC1qIJKmmcB6KNxxE776Dn6QoYbhN5jPeVBp7lDD3UxjfVzTxKKDAome6fUVBop3dpcLM6rq3+nNT2YArgqTD1qtsVM9vHlcLaAYaPg82vtIN80iDUseMlVHgK+nf6wIDAQAB"

    iput-object v0, p0, Lcom/google/android/gms/droidguard/d/aa;->a:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public final a([B[B)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 47
    :try_start_0
    const-string v1, "RSA"

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->e(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 48
    if-nez v1, :cond_0

    .line 49
    const-string v1, "DG"

    const-string v2, "Failed to get key factory object."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :goto_0
    return v0

    .line 52
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/droidguard/d/aa;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 53
    new-instance v3, Ljava/security/spec/X509EncodedKeySpec;

    invoke-direct {v3, v2}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 54
    const-string v2, "SHA256withRSA"

    invoke-static {v2}, Lcom/google/android/gms/common/util/e;->d(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v2

    .line 55
    if-nez v2, :cond_1

    .line 56
    const-string v1, "DG"

    const-string v2, "Failed to get signature object."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v1

    .line 63
    const-string v2, "DG"

    const-string v3, "Error during signature verification."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 59
    :cond_1
    :try_start_1
    invoke-virtual {v1, v3}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 60
    invoke-virtual {v2, p1}, Ljava/security/Signature;->update([B)V

    .line 61
    invoke-virtual {v2, p2}, Ljava/security/Signature;->verify([B)Z
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_0
.end method
