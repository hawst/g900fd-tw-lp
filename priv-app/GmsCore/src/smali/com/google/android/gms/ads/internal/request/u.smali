.class public final Lcom/google/android/gms/ads/internal/request/u;
.super Lcom/google/android/gms/ads/internal/request/s;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/ads/internal/request/r;

.field private final b:Lcom/google/android/gms/ads/internal/request/v;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/r;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/ads/internal/request/s;-><init>(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/r;)V

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/u;->c:Ljava/lang/Object;

    .line 90
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/u;->a:Lcom/google/android/gms/ads/internal/request/r;

    .line 91
    new-instance v0, Lcom/google/android/gms/ads/internal/request/v;

    iget-object v1, p2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->k:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->d:I

    invoke-direct {v0, p1, p0, p0, v1}, Lcom/google/android/gms/ads/internal/request/v;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;I)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/u;->b:Lcom/google/android/gms/ads/internal/request/v;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/u;->b:Lcom/google/android/gms/ads/internal/request/v;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/request/v;->a()V

    .line 94
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/u;->a:Lcom/google/android/gms/ads/internal/request/r;

    new-instance v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/request/r;->a(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V

    .line 123
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/u;->e()V

    .line 129
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/u;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 111
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/u;->b:Lcom/google/android/gms/ads/internal/request/v;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/request/v;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/u;->b:Lcom/google/android/gms/ads/internal/request/v;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/request/v;->n_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/u;->b:Lcom/google/android/gms/ads/internal/request/v;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/request/v;->b()V

    .line 114
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Lcom/google/android/gms/ads/internal/request/z;
    .locals 2

    .prologue
    .line 98
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/u;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/u;->b:Lcom/google/android/gms/ads/internal/request/v;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/request/v;->c()Lcom/google/android/gms/ads/internal/request/z;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 102
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 133
    const-string v0, "Disconnected from remote ad request service."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 134
    return-void
.end method
