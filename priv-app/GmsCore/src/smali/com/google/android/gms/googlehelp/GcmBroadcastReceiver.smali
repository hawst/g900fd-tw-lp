.class public Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;
.super Landroid/support/v4/a/z;
.source "SourceFile"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v4/a/z;-><init>()V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a:Z

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v3, 0x3

    .line 53
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GoogleHelp_GcmBcastRcvr"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleHelp_GcmBcastRcvr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Intent received: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p2, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/gcm/ag;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/ag;

    invoke-static {p2}, Lcom/google/android/gms/gcm/ag;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "gcm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v0, "GoogleHelp_GcmBcastRcvr"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GoogleHelp_GcmBcastRcvr"

    const-string v1, "Handling intent."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v0, "t"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ucs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "uvs"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v1, :cond_2

    if-eqz v2, :cond_6

    :cond_2
    const-string v0, "hv"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "hqp"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const-string v2, "hi"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_5

    invoke-static {v4, v5, v0, v2}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(JILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    const-string v1, "GoogleHelp_GcmBcastRcvr"

    const-string v2, "Starting wakeful service to handle Hangout status update."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, v0}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    iput-boolean v7, p0, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a:Z

    invoke-virtual {p0, v6}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->setResultCode(I)V

    .line 54
    :cond_3
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a:Z

    if-eqz v0, :cond_4

    if-eqz p2, :cond_4

    invoke-static {p2}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 55
    :cond_4
    return-void

    .line 53
    :catch_0
    move-exception v1

    const-string v1, "GoogleHelp_GcmBcastRcvr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Either the version or the queue position were not the expected type: {v: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", qp: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    invoke-static {v4, v5, v0, v2}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->a(JILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_6
    const-string v1, "cu"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "vu"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_7
    const-string v1, "GoogleHelp_GcmBcastRcvr"

    const-string v2, "Unavailable notification received."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "cu"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->f()Landroid/content/Intent;

    move-result-object v0

    :goto_2
    invoke-static {p1, v0}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/ComponentName;

    iput-boolean v7, p0, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->a:Z

    invoke-virtual {p0, v6}, Lcom/google/android/gms/googlehelp/GcmBroadcastReceiver;->setResultCode(I)V

    goto :goto_1

    :cond_8
    invoke-static {}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->f()Landroid/content/Intent;

    move-result-object v0

    goto :goto_2

    :cond_9
    const-string v0, "GoogleHelp_GcmBcastRcvr"

    const-string v1, "GCM Message received, but unknown notification type used."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_a
    const-string v1, "GoogleHelp_GcmBcastRcvr"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "GoogleHelp_GcmBcastRcvr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received message with unhandled type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
