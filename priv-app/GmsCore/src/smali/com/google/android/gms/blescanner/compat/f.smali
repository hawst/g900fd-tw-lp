.class final Lcom/google/android/gms/blescanner/compat/f;
.super Lcom/google/android/gms/blescanner/compat/v;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/blescanner/compat/c;


# direct methods
.method constructor <init>(Lcom/google/android/gms/blescanner/compat/c;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/android/gms/blescanner/compat/f;->a:Lcom/google/android/gms/blescanner/compat/c;

    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/v;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Serial scan: onScanFailed("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    .line 270
    return-void
.end method

.method public final a(ILcom/google/android/gms/blescanner/compat/ScanResult;)V
    .locals 2

    .prologue
    .line 253
    invoke-virtual {p2}, Lcom/google/android/gms/blescanner/compat/ScanResult;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/blescanner/compat/ScanResult;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 255
    :goto_0
    if-eqz v0, :cond_1

    .line 256
    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/f;->a:Lcom/google/android/gms/blescanner/compat/c;

    invoke-static {v1, v0, p2}, Lcom/google/android/gms/blescanner/compat/c;->a(Lcom/google/android/gms/blescanner/compat/c;Ljava/lang/String;Lcom/google/android/gms/blescanner/compat/ScanResult;)V

    .line 260
    :goto_1
    return-void

    .line 253
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :cond_1
    const-string v0, "Serial scan: onScanResult(): Invalid device received"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 264
    const-string v0, "Serial scan: Unexpected call to onBatchScanResults()"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    .line 265
    return-void
.end method
