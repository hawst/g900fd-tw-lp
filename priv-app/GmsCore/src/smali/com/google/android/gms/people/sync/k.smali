.class public abstract Lcom/google/android/gms/people/sync/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

.field private final b:Lcom/google/android/gms/people/sync/j;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Lcom/google/android/gms/people/sync/j;)V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    iput-object p1, p0, Lcom/google/android/gms/people/sync/k;->a:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    .line 222
    iput-object p2, p0, Lcom/google/android/gms/people/sync/k;->b:Lcom/google/android/gms/people/sync/j;

    .line 223
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/people/sync/k;->b:Lcom/google/android/gms/people/sync/j;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/k;->a:Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    invoke-interface {v0, v1}, Lcom/google/android/gms/people/sync/j;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 227
    iget-object v2, p0, Lcom/google/android/gms/people/sync/k;->b:Lcom/google/android/gms/people/sync/j;

    invoke-interface {v2, v1}, Lcom/google/android/gms/people/sync/j;->a(Ljava/lang/Object;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v2

    .line 228
    invoke-static {v2}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)I

    move-result v3

    invoke-static {v2}, Lcom/google/android/gms/people/sync/e;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/gms/people/sync/k;->a(Ljava/lang/Object;ILjava/lang/String;)V

    goto :goto_0

    .line 230
    :cond_0
    return-void
.end method

.method public abstract a(Ljava/lang/Object;ILjava/lang/String;)V
.end method
