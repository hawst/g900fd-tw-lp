.class final enum Lcom/google/android/gms/appstate/provider/b;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/appstate/provider/b;

.field public static final enum b:Lcom/google/android/gms/appstate/provider/b;

.field public static final enum c:Lcom/google/android/gms/appstate/provider/b;

.field public static final enum d:Lcom/google/android/gms/appstate/provider/b;

.field public static final enum e:Lcom/google/android/gms/appstate/provider/b;

.field public static final enum f:Lcom/google/android/gms/appstate/provider/b;

.field private static final synthetic g:[Lcom/google/android/gms/appstate/provider/b;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    new-instance v0, Lcom/google/android/gms/appstate/provider/b;

    const-string v1, "CLIENT_CONTEXTS"

    const-string v2, "client_contexts"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/appstate/provider/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/appstate/provider/b;->a:Lcom/google/android/gms/appstate/provider/b;

    .line 48
    new-instance v0, Lcom/google/android/gms/appstate/provider/b;

    const-string v1, "CLIENT_CONTEXTS_ID"

    const-string v2, "client_contexts/#"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/gms/appstate/provider/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/appstate/provider/b;->b:Lcom/google/android/gms/appstate/provider/b;

    .line 50
    new-instance v0, Lcom/google/android/gms/appstate/provider/b;

    const-string v1, "APP_STATES"

    const-string v2, "app_states/#"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/appstate/provider/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/appstate/provider/b;->c:Lcom/google/android/gms/appstate/provider/b;

    .line 51
    new-instance v0, Lcom/google/android/gms/appstate/provider/b;

    const-string v1, "APP_STATES_ID"

    const-string v2, "app_states/internal_id/#"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/appstate/provider/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/appstate/provider/b;->d:Lcom/google/android/gms/appstate/provider/b;

    .line 52
    new-instance v0, Lcom/google/android/gms/appstate/provider/b;

    const-string v1, "APP_STATES_APP_ID"

    const-string v2, "app_states/#/app_id/*"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/appstate/provider/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/appstate/provider/b;->e:Lcom/google/android/gms/appstate/provider/b;

    .line 53
    new-instance v0, Lcom/google/android/gms/appstate/provider/b;

    const-string v1, "APP_STATES_ACCOUNT_NAME"

    const/4 v2, 0x5

    const-string v3, "app_states/account_name/*"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/appstate/provider/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/appstate/provider/b;->f:Lcom/google/android/gms/appstate/provider/b;

    .line 46
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/gms/appstate/provider/b;

    sget-object v1, Lcom/google/android/gms/appstate/provider/b;->a:Lcom/google/android/gms/appstate/provider/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/appstate/provider/b;->b:Lcom/google/android/gms/appstate/provider/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/appstate/provider/b;->c:Lcom/google/android/gms/appstate/provider/b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/appstate/provider/b;->d:Lcom/google/android/gms/appstate/provider/b;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/appstate/provider/b;->e:Lcom/google/android/gms/appstate/provider/b;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/appstate/provider/b;->f:Lcom/google/android/gms/appstate/provider/b;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/appstate/provider/b;->g:[Lcom/google/android/gms/appstate/provider/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    invoke-static {}, Lcom/google/android/gms/appstate/provider/AppStateContentProvider;->b()Landroid/content/UriMatcher;

    move-result-object v0

    const-string v1, "com.google.android.gms.appstate"

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/provider/b;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, p3, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/appstate/provider/b;
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/gms/appstate/provider/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appstate/provider/b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/appstate/provider/b;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/gms/appstate/provider/b;->g:[Lcom/google/android/gms/appstate/provider/b;

    invoke-virtual {v0}, [Lcom/google/android/gms/appstate/provider/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appstate/provider/b;

    return-object v0
.end method
