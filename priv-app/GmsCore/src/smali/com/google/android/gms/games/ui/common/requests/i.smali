.class final Lcom/google/android/gms/games/ui/common/requests/i;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 162
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 157
    check-cast p3, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V

    invoke-virtual {p3}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/requests/i;->e(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/i;->l:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->md:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/i;->f(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/common/requests/i;->d(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/i;->t()V

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->e()Landroid/net/Uri;

    move-result-object v1

    sget v3, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gms/games/ui/common/requests/i;->a(Landroid/net/Uri;I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/i;->l:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->mc:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    aput-object v0, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/i;->a(Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/p;->kL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/i;->h(I)V

    sget v0, Lcom/google/android/gms/p;->kT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/i;->i(I)V

    sget v0, Lcom/google/android/gms/m;->k:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/i;->g(I)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/i;->l:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->me:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/i;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/g;

    .line 234
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/i;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    .line 235
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 236
    sget v3, Lcom/google/android/gms/j;->lQ:I

    if-ne v2, v3, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/i;->p()Lcom/google/android/gms/common/data/d;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/data/x;

    invoke-interface {v2, v1}, Lcom/google/android/gms/common/data/x;->b(Ljava/lang/Object;)V

    .line 239
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/g;->b(Lcom/google/android/gms/games/ui/common/requests/g;)Lcom/google/android/gms/games/ui/common/requests/h;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/requests/h;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V

    .line 240
    const/4 v0, 0x1

    .line 242
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs u()V
    .locals 0

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/i;->y()V

    .line 220
    return-void
.end method

.method public final y()V
    .locals 3

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/i;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/g;

    .line 225
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/i;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    .line 226
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/g;->b(Lcom/google/android/gms/games/ui/common/requests/g;)Lcom/google/android/gms/games/ui/common/requests/h;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/g;->a(Lcom/google/android/gms/games/ui/common/requests/g;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lcom/google/android/gms/games/ui/common/requests/h;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V

    .line 228
    return-void
.end method
