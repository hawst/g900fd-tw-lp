.class public abstract Lcom/google/android/gms/games/ui/b/a/a;
.super Lcom/google/android/gms/games/ui/b/b;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v5, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/a;->b()Ljava/lang/String;

    move-result-object v3

    .line 45
    const-string v1, "signedInAccountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    if-nez v1, :cond_1

    .line 49
    sget v1, Lcom/google/android/gms/p;->hQ:I

    .line 51
    sget v4, Lcom/google/android/gms/p;->hO:I

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    const-string v3, "signedInAccountName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    const-string v3, "newAccountName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    invoke-static {v2, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 68
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 69
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 70
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 71
    const v0, 0x1080027

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 72
    const v0, 0x104000a

    invoke-virtual {v3, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    sget v0, Lcom/google/android/gms/p;->hM:I

    invoke-virtual {v3, v0, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 77
    :cond_0
    const/high16 v0, 0x1040000

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 78
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 57
    :cond_1
    sget v1, Lcom/google/android/gms/p;->hP:I

    .line 59
    sget v4, Lcom/google/android/gms/p;->hN:I

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v6

    const-string v3, "signedInAccountName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    const-string v3, "newAccountName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v2, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected abstract c()V
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 83
    packed-switch p2, :pswitch_data_0

    .line 90
    :goto_0
    :pswitch_0
    return-void

    .line 85
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/a;->c()V

    .line 87
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/a;->e()V

    goto :goto_0

    .line 83
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
