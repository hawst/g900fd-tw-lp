.class public abstract Lcom/google/android/gms/games/ui/common/quests/a;
.super Lcom/google/android/gms/games/ui/s;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/bp;
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/ui/ci;


# instance fields
.field protected l:Lcom/google/android/gms/games/ui/common/quests/d;

.field private m:Lcom/google/android/gms/games/ui/common/quests/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/v;Z)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->m:Lcom/google/android/gms/games/ui/common/quests/l;

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->z()V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    .line 70
    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/games/ui/common/quests/a;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/bh;Z)V

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->h:Lcom/google/android/gms/games/ui/d/p;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    .line 72
    return-void
.end method


# virtual methods
.method public final B()V
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/quests/a;->b(Z)V

    .line 125
    return-void
.end method

.method public final L_()V
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    :goto_0
    return-void

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/a;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/games/ui/d/n;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/games/ui/d/n;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/d/n;->O()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/games/ui/d/n;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/d/n;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/d/n;->O()V

    goto :goto_0

    :cond_2
    const-string v0, "BaseQuestFragment"

    const-string v1, "No valid listener to update the inbox counts"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/a;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/common/quests/a;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/bh;Z)V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/gms/games/quest/g;

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/g;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/quest/g;->c()Lcom/google/android/gms/games/quest/b;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/a;->z()V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/a;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->w_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/q;->b(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->w_()V

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/quests/a;->m:Lcom/google/android/gms/games/ui/common/quests/l;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->A()V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/quests/a;->l:Lcom/google/android/gms/games/ui/common/quests/d;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/common/quests/d;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/quests/a;->h:Lcom/google/android/gms/games/ui/d/p;

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->c()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/ui/d/p;->a(IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->J()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->w_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/common/quests/a;->a(Lcom/google/android/gms/common/api/v;Z)V

    .line 63
    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/bh;Z)V
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/a;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/games/ui/common/quests/a;->a(Lcom/google/android/gms/common/api/v;Z)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onActivityCreated(Landroid/os/Bundle;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/quests/j;

    const-string v1, "Parent activity is not a QuestInboxHelperProvider"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/quests/j;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/quests/j;->Q_()Lcom/google/android/gms/games/ui/common/quests/i;

    move-result-object v1

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/quests/l;

    const-string v2, "Parent activity did not implement QuestUiConfiguration"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/quests/l;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->m:Lcom/google/android/gms/games/ui/common/quests/l;

    .line 50
    sget v0, Lcom/google/android/gms/h;->aA:I

    sget v2, Lcom/google/android/gms/p;->kD:I

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/ui/common/quests/a;->a(III)V

    .line 53
    new-instance v0, Lcom/google/android/gms/games/ui/common/quests/d;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/quests/a;->d:Lcom/google/android/gms/games/ui/q;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/quests/a;->m:Lcom/google/android/gms/games/ui/common/quests/l;

    invoke-interface {v3}, Lcom/google/android/gms/games/ui/common/quests/l;->U()Z

    move-result v3

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/gms/games/ui/common/quests/d;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/quests/g;Z)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->l:Lcom/google/android/gms/games/ui/common/quests/d;

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/a;->l:Lcom/google/android/gms/games/ui/common/quests/d;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/quests/a;->a(Landroid/support/v7/widget/bv;)V

    .line 57
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/common/quests/a;->a(Landroid/support/v4/widget/bp;)V

    .line 58
    return-void
.end method
