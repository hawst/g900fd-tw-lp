.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/r;

.field private final b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

.field private final c:I

.field private final d:Z

.field private final e:I

.field private final f:Lcom/google/android/gms/drive/database/model/a;

.field private final g:Lcom/google/android/gms/drive/auth/g;

.field private final h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

.field private i:Lcom/google/android/gms/drive/metadata/sync/b/e;

.field private j:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;Lcom/google/android/gms/drive/database/model/a;IZI)V
    .locals 8

    .prologue
    .line 78
    new-instance v7, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    invoke-direct {v7, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;Lcom/google/android/gms/drive/database/model/a;IZILcom/google/android/gms/drive/metadata/sync/syncadapter/c;)V

    .line 80
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;Lcom/google/android/gms/drive/database/model/a;IZILcom/google/android/gms/drive/metadata/sync/syncadapter/c;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->j:I

    .line 90
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    .line 91
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->f:Lcom/google/android/gms/drive/database/model/a;

    .line 92
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    .line 93
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    .line 94
    iput p4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->c:I

    .line 95
    iput-boolean p5, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->d:Z

    .line 96
    iput p6, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->e:I

    .line 97
    invoke-static {p3}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->g:Lcom/google/android/gms/drive/auth/g;

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->f:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/metadata/sync/b/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    .line 99
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/metadata/sync/b/e;
    .locals 6

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 278
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->b:Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-interface {p0, p1, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/e;)Ljava/util/List;

    move-result-object v3

    .line 280
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 281
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 282
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/a/j;

    .line 283
    iget-boolean v0, v0, Lcom/google/android/gms/drive/metadata/sync/a/j;->a:Z

    if-eqz v0, :cond_1

    .line 284
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    .line 285
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/model/bb;

    .line 299
    :goto_1
    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/b/e;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/e;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    :cond_0
    move v0, v2

    .line 281
    goto :goto_0

    .line 287
    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    .line 288
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/model/bb;

    goto :goto_1

    .line 292
    :cond_2
    const/4 v0, 0x0

    invoke-interface {p0, p1, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;)Ljava/util/Set;

    move-result-object v0

    .line 293
    new-instance v3, Lcom/google/android/gms/drive/metadata/sync/a/j;

    invoke-direct {v3, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/a/j;-><init>(ZLjava/util/Set;)V

    invoke-interface {p0, p1, v3, v4, v5}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/c;J)Lcom/google/android/gms/drive/database/model/bb;

    move-result-object v2

    .line 295
    new-instance v3, Lcom/google/android/gms/drive/metadata/sync/a/j;

    invoke-direct {v3, v1, v0}, Lcom/google/android/gms/drive/metadata/sync/a/j;-><init>(ZLjava/util/Set;)V

    invoke-interface {p0, p1, v3, v4, v5}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/c;J)Lcom/google/android/gms/drive/database/model/bb;

    move-result-object v0

    move-object v1, v2

    goto :goto_1
.end method

.method private a(Landroid/content/SyncResult;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 215
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->f:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, v2, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v3

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->g:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/c;->e()J

    move-result-wide v4

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, v2, v4, v5}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;J)I

    move-result v0

    .line 219
    if-eqz p1, :cond_0

    .line 220
    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numDeletes:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numDeletes:J

    .line 223
    :cond_0
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->c:I

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_4

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/e;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    if-eqz v0, :cond_2

    new-instance v2, Ljava/util/Date;

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/e;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 232
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/e;->d:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    if-eqz v0, :cond_3

    new-instance v1, Ljava/util/Date;

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/e;->d:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    move-object v0, v1

    .line 235
    :goto_2
    invoke-virtual {v3, v2}, Lcom/google/android/gms/drive/database/model/c;->c(Ljava/util/Date;)V

    .line 236
    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/database/model/c;->b(Ljava/util/Date;)V

    .line 241
    :goto_3
    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/c;->i()V

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 245
    return-void

    .line 223
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 229
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 232
    goto :goto_2

    .line 238
    :cond_4
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/database/model/c;->c(Ljava/util/Date;)V

    .line 239
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/database/model/c;->b(Ljava/util/Date;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 244
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0
.end method

.method private a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Lcom/google/android/gms/drive/database/model/bb;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V
    .locals 6

    .prologue
    .line 137
    iget-object v0, p2, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    .line 138
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 140
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    invoke-static {p4, v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;J)Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    move-result-object v0

    .line 144
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/e;

    invoke-direct {v1, p2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/e;-><init>(Lcom/google/android/gms/drive/database/model/bb;Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;)V

    .line 147
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->c:I

    int-to-long v2, v0

    iget-object v0, p2, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-wide v4, v0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    .line 148
    if-lez v0, :cond_0

    iget-object v2, p2, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 149
    iget-object v2, p2, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {p1, v2, p3, v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a(Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;ILcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 151
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->j:I

    .line 153
    :cond_0
    return-void

    .line 138
    :cond_1
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;Z)V
    .locals 8

    .prologue
    .line 255
    if-eqz p2, :cond_0

    .line 256
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a(Landroid/content/SyncResult;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/e;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/a/j;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/a/j;->b:Ljava/util/Set;

    .line 263
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->f:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v2, v1, Lcom/google/android/gms/drive/database/model/a;->b:J

    .line 264
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 265
    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v4, v2, v3, v6, v7}, Lcom/google/android/gms/drive/database/r;->a(JJ)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 266
    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v4, v0}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/i;

    move-result-object v0

    .line 267
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/google/android/gms/drive/database/model/i;->c:Z

    .line 268
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/i;->i()V

    goto :goto_0

    .line 271
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->f:Lcom/google/android/gms/drive/database/model/a;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;->a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    move-result-object v1

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->f:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    .line 162
    iget-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->d:Z

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->f:Lcom/google/android/gms/drive/database/model/a;

    invoke-interface {v0, v3}, Lcom/google/android/gms/drive/database/r;->c(Lcom/google/android/gms/drive/database/model/a;)I

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->f:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/metadata/sync/b/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/e;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    invoke-direct {p0, p1, v0, v2, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Lcom/google/android/gms/drive/database/model/bb;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/e;->d:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    invoke-direct {p0, p1, v0, v2, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Lcom/google/android/gms/drive/database/model/bb;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/e;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-wide v0, v0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->i:Lcom/google/android/gms/drive/metadata/sync/b/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/e;->d:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-wide v0, v0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->d:Z

    if-eqz v0, :cond_3

    .line 176
    :cond_2
    new-instance v0, Ljava/util/Date;

    const-wide v2, 0x7fffffffffffffffL

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 177
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 179
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->f:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, v2, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/database/r;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v1

    .line 181
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/c;->c(Ljava/util/Date;)V

    .line 182
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/c;->b(Ljava/util/Date;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->j()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/model/c;->b(J)V

    .line 185
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/c;->b()V

    .line 186
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->e:I

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/model/c;->c(J)V

    .line 188
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/c;->i()V

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 195
    :cond_3
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->j:I

    if-nez v0, :cond_4

    .line 196
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a(Landroid/content/SyncResult;)V

    .line 198
    :cond_4
    return-void

    .line 191
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/d;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0
.end method
