.class final Lcom/google/android/gms/plus/internal/bf;
.super Lcom/google/android/gms/plus/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/ay;

.field private final b:Lcom/google/android/gms/plus/internal/ag;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ag;)V
    .locals 0

    .prologue
    .line 1137
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/bf;->a:Lcom/google/android/gms/plus/internal/ay;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/b;-><init>()V

    .line 1138
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/bf;->b:Lcom/google/android/gms/plus/internal/ag;

    .line 1139
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 5

    .prologue
    .line 1143
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 1145
    :goto_0
    new-instance v1, Lcom/google/android/gms/common/c;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 1146
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/bf;->a:Lcom/google/android/gms/plus/internal/ay;

    new-instance v2, Lcom/google/android/gms/plus/internal/bg;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/bf;->a:Lcom/google/android/gms/plus/internal/ay;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/bf;->b:Lcom/google/android/gms/plus/internal/ag;

    invoke-direct {v2, v3, v4, v1, p3}, Lcom/google/android/gms/plus/internal/bg;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ag;Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/internal/ay;->a(Lcom/google/android/gms/common/internal/al;)V

    .line 1147
    return-void

    .line 1143
    :cond_0
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    goto :goto_0
.end method
