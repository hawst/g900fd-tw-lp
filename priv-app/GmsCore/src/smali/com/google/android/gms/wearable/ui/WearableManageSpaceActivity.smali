.class public Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/ListView;

.field private f:Lcom/google/android/gms/wearable/ui/e;

.field private g:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 220
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->g:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;Lcom/google/android/gms/wearable/ui/e;)Lcom/google/android/gms/wearable/ui/e;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->f:Lcom/google/android/gms/wearable/ui/e;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->d:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;)Lcom/google/android/gms/wearable/ui/e;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->f:Lcom/google/android/gms/wearable/ui/e;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->d:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 107
    new-instance v0, Lcom/google/android/gms/wearable/ui/d;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/wearable/ui/d;-><init>(Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;B)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/ui/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 109
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 56
    sget v0, Lcom/google/android/gms/l;->hG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->setContentView(I)V

    .line 58
    sget v0, Lcom/google/android/gms/j;->lD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->a:Landroid/view/View;

    .line 59
    sget v0, Lcom/google/android/gms/j;->ue:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->b:Landroid/view/View;

    .line 61
    sget v0, Lcom/google/android/gms/j;->tj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->c:Landroid/widget/TextView;

    .line 62
    sget v0, Lcom/google/android/gms/j;->qp:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->d:Landroid/widget/Button;

    .line 63
    sget v0, Lcom/google/android/gms/j;->aL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->e:Landroid/widget/ListView;

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->e:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/wearable/z;->g:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->g:Lcom/google/android/gms/common/api/v;

    .line 73
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/ui/a;

    .line 94
    if-nez v0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    if-ltz p3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ui/a;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    .line 100
    invoke-virtual {v0, p3}, Lcom/google/android/gms/wearable/ui/a;->a(I)Lcom/google/android/gms/wearable/internal/PackageStorageInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wearable/internal/PackageStorageInfo;->b:Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "package"

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->f:Lcom/google/android/gms/wearable/ui/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/ui/e;->cancel(Z)Z

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->f:Lcom/google/android/gms/wearable/ui/e;

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 88
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 89
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 79
    new-instance v0, Lcom/google/android/gms/wearable/ui/e;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/wearable/ui/e;-><init>(Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->f:Lcom/google/android/gms/wearable/ui/e;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/wearable/ui/WearableManageSpaceActivity;->f:Lcom/google/android/gms/wearable/ui/e;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/ui/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 81
    return-void
.end method
