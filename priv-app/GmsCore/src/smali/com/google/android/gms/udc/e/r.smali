.class public final Lcom/google/android/gms/udc/e/r;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/udc/e/r;


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/udc/e/s;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/udc/e/r;->a:I

    iput-object v1, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/r;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/r;->cachedSize:I

    .line 31
    return-void
.end method

.method public static a()[Lcom/google/android/gms/udc/e/r;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/gms/udc/e/r;->c:[Lcom/google/android/gms/udc/e/r;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/gms/udc/e/r;->c:[Lcom/google/android/gms/udc/e/r;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/udc/e/r;

    sput-object v0, Lcom/google/android/gms/udc/e/r;->c:[Lcom/google/android/gms/udc/e/r;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/gms/udc/e/r;->c:[Lcom/google/android/gms/udc/e/r;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 89
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 90
    iget v1, p0, Lcom/google/android/gms/udc/e/r;->a:I

    if-eqz v1, :cond_0

    .line 91
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/udc/e/r;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    if-eqz v1, :cond_1

    .line 95
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 43
    if-ne p1, p0, :cond_1

    .line 44
    const/4 v0, 0x1

    .line 62
    :cond_0
    :goto_0
    return v0

    .line 46
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/r;

    if-eqz v1, :cond_0

    .line 49
    check-cast p1, Lcom/google/android/gms/udc/e/r;

    .line 50
    iget v1, p0, Lcom/google/android/gms/udc/e/r;->a:I

    iget v2, p1, Lcom/google/android/gms/udc/e/r;->a:I

    if-ne v1, v2, :cond_0

    .line 53
    iget-object v1, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_3

    .line 54
    iget-object v1, p1, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 62
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/r;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 58
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 67
    iget v0, p0, Lcom/google/android/gms/udc/e/r;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 69
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 71
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/r;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    return v0

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/r;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/udc/e/r;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/gms/udc/e/r;->a:I

    if-eqz v0, :cond_0

    .line 79
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/udc/e/r;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_1

    .line 82
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 84
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 85
    return-void
.end method
