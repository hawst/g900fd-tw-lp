.class public final Lcom/google/android/gms/feedback/an;
.super Lcom/google/android/gms/googlehelp/a/i;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/toolbox/aa;)V
    .locals 7

    .prologue
    .line 48
    invoke-virtual {p2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/a/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Landroid/accounts/Account;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 49
    iput-object p4, p0, Lcom/google/android/gms/feedback/an;->i:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 6

    .prologue
    .line 113
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->oe:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "hl"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 114
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v5

    .line 115
    new-instance v0, Lcom/google/android/gms/feedback/an;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/an;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/toolbox/aa;)V

    .line 117
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 119
    :try_start_0
    sget-object v0, Lcom/google/android/gms/feedback/a/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v0, v1, v2}, Lcom/android/volley/toolbox/aa;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1

    .line 124
    :goto_0
    return-object v0

    .line 122
    :catch_0
    move-exception v0

    .line 123
    :goto_1
    const-string v1, "GFEEDBACK_SuggestionsRequest"

    const-string v2, "Fetching suggestion failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 124
    const/4 v0, 0x0

    goto :goto_0

    .line 122
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 60
    :try_start_0
    new-instance v3, Ljava/lang/String;

    iget-object v0, p1, Lcom/android/volley/m;->b:[B

    iget-object v2, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    invoke-static {v2}, Lcom/android/volley/toolbox/i;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 63
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "id"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "unknownId"

    :goto_0
    invoke-static {v0, v2}, Lcom/google/android/gms/googlehelp/common/k;->b(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_4

    .line 69
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    .line 70
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->m()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v2, v0

    .line 76
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    :goto_2
    invoke-static {v0, v2}, Lcom/google/android/gms/feedback/as;->a(Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    .line 78
    if-nez v0, :cond_3

    .line 79
    const-string v0, "GFEEDBACK_SuggestionsRequest"

    const-string v1, "Unsupported/empty response received"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Unsupported/empty response received"

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    .line 85
    :goto_3
    return-object v0

    .line 63
    :cond_1
    const-string v0, "id"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 76
    :cond_2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "renderingUrl"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 82
    :cond_3
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_3

    .line 83
    :catch_0
    move-exception v0

    .line 84
    :goto_4
    const-string v1, "GFEEDBACK_SuggestionsRequest"

    const-string v2, "Parsing suggestion response data failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 85
    new-instance v1, Lcom/android/volley/ac;

    invoke-direct {v1, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_3

    .line 83
    :catch_1
    move-exception v0

    goto :goto_4

    :cond_4
    move-object v2, v1

    goto :goto_1
.end method

.method protected final a(Lcom/google/android/gms/googlehelp/a/l;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/feedback/an;->i:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/a/l;->c:Ljava/lang/String;

    .line 55
    return-void
.end method
