.class public final Lcom/google/android/gms/plus/apps/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/plus/apps/a;


# instance fields
.field private final b:Landroid/support/v4/g/h;

.field private final c:Landroid/content/pm/PackageManager;

.field private final d:Landroid/content/res/Resources;

.field private e:Landroid/graphics/drawable/Drawable;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/a;->c:Landroid/content/pm/PackageManager;

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/a;->d:Landroid/content/res/Resources;

    .line 37
    new-instance v0, Landroid/support/v4/g/h;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Landroid/support/v4/g/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/a;->b:Landroid/support/v4/g/h;

    .line 38
    return-void
.end method

.method private a()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/a;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/a;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/h;->ck:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/a;->e:Landroid/graphics/drawable/Drawable;

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/a;->e:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/a;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/gms/plus/apps/a;->a:Lcom/google/android/gms/plus/apps/a;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/google/android/gms/plus/apps/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/apps/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/plus/apps/a;->a:Lcom/google/android/gms/plus/apps/a;

    .line 47
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/a;->a:Lcom/google/android/gms/plus/apps/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/apps/b;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v4

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/a;->b:Landroid/support/v4/g/h;

    invoke-virtual {v0, v4}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/b;

    .line 99
    if-eqz v0, :cond_0

    .line 105
    :goto_0
    return-object v0

    .line 103
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/a;->c:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/a;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_1
    new-instance v3, Lcom/google/android/gms/plus/apps/b;

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/a;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-ne v0, v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-direct {v3, v5, v0, v1, v2}, Lcom/google/android/gms/plus/apps/b;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;ZB)V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/a;->b:Landroid/support/v4/g/h;

    invoke-virtual {v0, v4, v3}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v3

    .line 105
    goto :goto_0

    .line 103
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/plus/model/a/a;Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/plus/apps/b;
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/apps/a;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/apps/b;

    move-result-object v0

    .line 113
    if-eqz p2, :cond_0

    .line 114
    iput-object p2, v0, Lcom/google/android/gms/plus/apps/b;->b:Landroid/graphics/drawable/Drawable;

    .line 116
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/plus/apps/b;->c:Z

    .line 117
    return-object v0
.end method
