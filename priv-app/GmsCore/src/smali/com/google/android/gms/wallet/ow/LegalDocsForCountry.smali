.class public Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:[Lcom/google/aa/b/a/a/a/a/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/gms/wallet/ow/s;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/ow/s;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Lcom/google/aa/b/a/a/a/a/s;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->a:Ljava/lang/String;

    .line 20
    if-eqz p2, :cond_0

    .line 21
    iput-object p2, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Lcom/google/aa/b/a/a/a/a/s;

    .line 25
    :goto_0
    return-void

    .line 23
    :cond_0
    invoke-static {}, Lcom/google/aa/b/a/a/a/a/s;->a()[Lcom/google/aa/b/a/a/a/a/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Lcom/google/aa/b/a/a/a/a/s;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()[Lcom/google/aa/b/a/a/a/a/s;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Lcom/google/aa/b/a/a/a/a/s;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Lcom/google/aa/b/a/a/a/a/s;

    array-length v1, v0

    .line 44
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 45
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 46
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b:[Lcom/google/aa/b/a/a/a/a/s;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method
