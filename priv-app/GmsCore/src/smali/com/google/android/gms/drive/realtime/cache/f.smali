.class public final Lcom/google/android/gms/drive/realtime/cache/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/drive/realtime/cache/n;

.field private final b:Lcom/google/android/gms/drive/realtime/cache/w;

.field private final c:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/w;Z)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/cache/f;->a:Lcom/google/android/gms/drive/realtime/cache/n;

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/f;->b:Lcom/google/android/gms/drive/realtime/cache/w;

    .line 30
    iput-boolean p3, p0, Lcom/google/android/gms/drive/realtime/cache/f;->c:Z

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 3

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/f;->a:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/n;->i()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/f;->a:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/n;->j()Lcom/google/android/gms/drive/database/model/br;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a()Lcom/google/android/gms/drive/realtime/cache/a/c;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/realtime/cache/a/c;->b(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/drive/database/model/br;->h:I

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/br;->i()V

    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/e;->a()Lcom/google/android/gms/drive/realtime/cache/a/e;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/realtime/cache/a/e;->a(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v2

    iget v1, v1, Lcom/google/android/gms/drive/database/model/br;->h:I

    sget-object v0, Lcom/google/android/gms/drive/ai;->M:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/f;->a:Lcom/google/android/gms/drive/realtime/cache/n;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/n;->h()V

    iget-boolean v1, p0, Lcom/google/android/gms/drive/realtime/cache/f;->c:Z

    if-eqz v1, :cond_1

    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/f;->b:Lcom/google/android/gms/drive/realtime/cache/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/w;->b()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
