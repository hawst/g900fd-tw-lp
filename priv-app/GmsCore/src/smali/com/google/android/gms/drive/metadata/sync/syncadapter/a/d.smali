.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/concurrent/CountDownLatch;

.field b:I

.field private final c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 20
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;

    .line 21
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    .line 30
    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b()V

    .line 33
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;)V

    .line 56
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 39
    monitor-enter p0

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->a:Ljava/util/concurrent/CountDownLatch;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->a:Ljava/util/concurrent/CountDownLatch;

    .line 42
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/e;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;B)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;)V

    .line 44
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 46
    iget v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->b:I

    return v0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
