.class public final Lcom/google/android/gms/plus/b/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/plus/service/v2whitelisted/c;

.field private static final b:Ljava/util/List;


# instance fields
.field private final c:Lcom/google/android/gms/plus/service/v2whitelisted/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/c;-><init>()V

    const-string v1, "genders/value"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/k;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/c;

    sput-object v0, Lcom/google/android/gms/plus/b/m;->a:Lcom/google/android/gms/plus/service/v2whitelisted/c;

    .line 43
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "disabled"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/b/m;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/n;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v2whitelisted/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/m;->c:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/plus/b/m;->c:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    const-string v2, "me"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/plus/b/m;->b:Ljava/util/List;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/gms/plus/b/m;->a:Lcom/google/android/gms/plus/service/v2whitelisted/c;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/c;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->f()Ljava/util/List;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 81
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Genders;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Genders;->b()Ljava/lang/String;

    move-result-object v0

    .line 82
    const-string v1, "male"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v7

    .line 91
    :goto_0
    return v0

    .line 84
    :cond_0
    const-string v1, "female"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    const/4 v0, 0x1

    goto :goto_0

    .line 86
    :cond_1
    const-string v1, "other"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    const/4 v0, 0x2

    goto :goto_0

    .line 91
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method
