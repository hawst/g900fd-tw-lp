.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$DeepLinkEntity;

.field public d:Ljava/lang/String;

.field public e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;

.field public f:Ljava/lang/String;

.field public final g:Ljava/util/Set;

.field private h:Ljava/util/List;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 693
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->g:Ljava/util/Set;

    .line 694
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ai;
    .locals 10

    .prologue
    .line 780
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->g:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$DeepLinkEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;

    iget-object v7, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->f:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->h:Ljava/util/List;

    iget-object v9, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->i:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ActionEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$DeepLinkEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/an;
    .locals 2

    .prologue
    .line 771
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->i:Ljava/lang/String;

    .line 772
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->g:Ljava/util/Set;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 773
    return-object p0
.end method
