.class final Lcom/google/android/gms/auth/frp/e;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/gms/auth/frp/e;->a:Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/w;

    iget-object v1, p0, Lcom/google/android/gms/auth/frp/e;->a:Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/w;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/frp/e;->a:Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;

    invoke-static {v1}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->a(Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceResponse;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceResponse;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a()V

    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 73
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/frp/e;->a:Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/frp/e;->a:Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->finish()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
