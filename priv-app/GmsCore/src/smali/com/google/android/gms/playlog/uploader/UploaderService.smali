.class public Lcom/google/android/gms/playlog/uploader/UploaderService;
.super Lcom/google/android/gms/gcm/ae;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/playlog/store/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/gcm/ae;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/gcm/be;)I
    .locals 10

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/gms/playlog/a/e;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 35
    if-eqz v7, :cond_0

    .line 36
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 37
    const-string v2, "UploaderService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "--> uploading all staged data: now="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/gcm/be;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/playlog/uploader/UploaderService;->a:Lcom/google/android/gms/playlog/store/f;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    new-instance v5, Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-static {v1}, Lcom/google/android/gms/playlog/c/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-direct {v5, v1, v0, v2}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    new-instance v0, Lcom/google/android/gms/playlog/uploader/b;

    invoke-static {}, Lcom/google/android/gms/playlog/uploader/a;->a()Lcom/google/android/gms/playlog/uploader/a;

    move-result-object v2

    new-instance v4, Lcom/google/android/gms/playlog/uploader/c;

    invoke-direct {v4}, Lcom/google/android/gms/playlog/uploader/c;-><init>()V

    sget-object v6, Lcom/google/android/gms/playlog/a/e;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, v4, Lcom/google/android/gms/playlog/uploader/c;->a:Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/playlog/a/e;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lcom/google/android/gms/playlog/uploader/c;->b:I

    sget-object v6, Lcom/google/android/gms/playlog/a/e;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iput-wide v8, v4, Lcom/google/android/gms/playlog/uploader/c;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/playlog/uploader/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/playlog/uploader/a;Lcom/google/android/gms/playlog/store/f;Lcom/google/android/gms/playlog/uploader/c;Lcom/google/android/gms/http/GoogleHttpClient;)V

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/uploader/b;->a()Z

    move-result v1

    .line 40
    if-eqz v7, :cond_1

    .line 41
    const-string v0, "UploaderService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Uploader.upload() returns "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/playlog/uploader/UploaderService;->a:Lcom/google/android/gms/playlog/store/f;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/store/f;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 44
    const-string v0, "UploaderService"

    const-string v2, "Storage is full; enforce austerity!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/playlog/uploader/UploaderService;->a:Lcom/google/android/gms/playlog/store/f;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/store/f;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :goto_1
    return v0

    .line 47
    :catch_0
    move-exception v0

    .line 48
    const-string v2, "UploaderService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not enforce austerity: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 51
    :cond_3
    const/4 v0, 0x2

    goto :goto_1
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0}, Lcom/google/android/gms/gcm/ae;->onCreate()V

    .line 23
    invoke-static {}, Lcom/google/android/gms/playlog/store/f;->a()Lcom/google/android/gms/playlog/store/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/playlog/uploader/UploaderService;->a:Lcom/google/android/gms/playlog/store/f;

    .line 24
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/playlog/uploader/UploaderService;->a:Lcom/google/android/gms/playlog/store/f;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/store/f;->close()V

    .line 29
    invoke-super {p0}, Lcom/google/android/gms/gcm/ae;->onDestroy()V

    .line 30
    return-void
.end method
