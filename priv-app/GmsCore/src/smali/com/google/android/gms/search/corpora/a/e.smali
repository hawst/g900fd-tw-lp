.class public abstract Lcom/google/android/gms/search/corpora/a/e;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/search/corpora/a/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/search/corpora/a/e;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 113
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 45
    :sswitch_0
    const-string v0, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 46
    goto :goto_0

    .line 50
    :sswitch_1
    const-string v2, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/j;

    invoke-static {p2}, Lcom/google/android/gms/search/corpora/j;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;

    move-result-object v0

    .line 59
    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/search/corpora/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/search/corpora/a/a;

    move-result-object v2

    .line 60
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/search/corpora/a/e;->a(Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V

    .line 61
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 62
    goto :goto_0

    .line 66
    :sswitch_2
    const-string v2, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 69
    sget-object v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/a;

    invoke-static {p2}, Lcom/google/android/gms/search/corpora/a;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;

    move-result-object v0

    .line 75
    :cond_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/search/corpora/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/search/corpora/a/a;

    move-result-object v2

    .line 76
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/search/corpora/a/e;->a(Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V

    .line 77
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 78
    goto :goto_0

    .line 82
    :sswitch_3
    const-string v2, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    .line 85
    sget-object v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/g;

    invoke-static {p2}, Lcom/google/android/gms/search/corpora/g;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;

    move-result-object v0

    .line 91
    :cond_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/search/corpora/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/search/corpora/a/a;

    move-result-object v2

    .line 92
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/search/corpora/a/e;->a(Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V

    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 94
    goto :goto_0

    .line 98
    :sswitch_4
    const-string v2, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3

    .line 101
    sget-object v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/d;

    invoke-static {p2}, Lcom/google/android/gms/search/corpora/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;

    move-result-object v0

    .line 107
    :cond_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/search/corpora/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/search/corpora/a/a;

    move-result-object v2

    .line 108
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/search/corpora/a/e;->a(Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;Lcom/google/android/gms/search/corpora/a/a;)V

    .line 109
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 110
    goto/16 :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
