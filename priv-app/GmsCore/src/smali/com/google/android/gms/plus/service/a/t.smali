.class public final Lcom/google/android/gms/plus/service/a/t;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/f;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/t;->c:Ljava/lang/String;

    .line 36
    iput p3, p0, Lcom/google/android/gms/plus/service/a/t;->d:I

    .line 37
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/t;->e:Ljava/lang/String;

    .line 38
    iput-object p5, p0, Lcom/google/android/gms/plus/service/a/t;->b:Lcom/google/android/gms/plus/internal/f;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 44
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/t;->c:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/plus/service/a/t;->d:I

    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/t;->e:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/b/n;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/t;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 58
    :goto_0
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 48
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 49
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 50
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/t;->b:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v0, v8, v1, v6}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0

    .line 53
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/t;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/t;->b:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v1, v8, v0, v6}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0

    .line 56
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/t;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v6, v6}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/t;->b:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/t;->b:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    .line 65
    :cond_0
    return-void
.end method
