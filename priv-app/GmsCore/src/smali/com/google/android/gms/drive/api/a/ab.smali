.class public final Lcom/google/android/gms/drive/api/a/ab;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/api/k;

.field private final f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/internal/OpenContentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 2

    .prologue
    .line 43
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-direct {p0, p1, p4, v0, v1}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;IZ)V

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/ab;->e:Lcom/google/android/gms/drive/api/k;

    .line 46
    iput-object p3, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/api/a/ab;Lcom/google/android/gms/drive/auth/g;Z)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/api/a/ab;->a(Lcom/google/android/gms/drive/auth/g;Z)V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/auth/g;Z)V
    .locals 6

    .prologue
    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ab;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/api/d;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v2

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->e:Lcom/google/android/gms/drive/api/k;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->c()I

    move-result v3

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v4

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ab;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v1}, Lcom/google/android/gms/drive/internal/ca;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/drive/api/k;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;IILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;
    :try_end_0
    .catch Lcom/google/android/gms/common/service/h; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 130
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ab;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v3, Lcom/google/android/gms/drive/internal/OnContentsResponse;

    invoke-direct {v3, v0, p2}, Lcom/google/android/gms/drive/internal/OnContentsResponse;-><init>(Lcom/google/android/gms/drive/Contents;Z)V

    invoke-interface {v1, v3}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnContentsResponse;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/common/service/h; {:try_start_1 .. :try_end_1} :catch_1

    .line 135
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->c:Lcom/google/android/gms/drive/c/a;

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->d()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->a()V

    .line 143
    :goto_1
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 132
    const-string v1, "OpenContentsOperation"

    const-string v3, "Error returning opened contents."

    invoke-static {v1, v0, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_2
    .catch Lcom/google/android/gms/common/service/h; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 136
    :catch_1
    move-exception v0

    .line 138
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ab;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-virtual {v0}, Lcom/google/android/gms/common/service/h;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 139
    :catch_2
    move-exception v0

    .line 140
    const-string v1, "OpenContentsOperation"

    const-string v2, "Error opening contents."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 5

    .prologue
    const/high16 v4, 0x30000000

    const/high16 v3, 0x20000000

    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    const-string v2, "Invalid open contents request: no request"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v2, "Invalid open contents request: no id"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    const/high16 v2, 0x10000000

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-ne v0, v4, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "Invalid open contents request: invalid mode"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->c:Lcom/google/android/gms/drive/c/a;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/c/a;->a(I)Lcom/google/android/gms/drive/c/a;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/api/d;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->E()Z

    move-result v0

    if-nez v0, :cond_3

    .line 73
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v2, 0xa

    const-string v3, "The user cannot edit the resource."

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_2
    move v0, v1

    .line 60
    goto :goto_0

    .line 79
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 82
    invoke-direct {p0, p2, v1}, Lcom/google/android/gms/drive/api/a/ab;->a(Lcom/google/android/gms/drive/auth/g;Z)V

    .line 118
    :goto_1
    return-void

    .line 85
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ab;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ab;->f:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/api/a/ac;

    invoke-direct {v2, p0, p2}, Lcom/google/android/gms/drive/api/a/ac;-><init>(Lcom/google/android/gms/drive/api/a/ab;Lcom/google/android/gms/drive/auth/g;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;)V

    goto :goto_1
.end method
