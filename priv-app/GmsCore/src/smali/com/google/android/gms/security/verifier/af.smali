.class public final Lcom/google/android/gms/security/verifier/af;
.super Lcom/android/volley/p;
.source "SourceFile"


# instance fields
.field private final f:Lcom/android/volley/x;

.field private final g:Lcom/google/android/gms/security/verifier/l;

.field private final h:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/volley/w;Lcom/android/volley/x;Lcom/google/android/gms/security/verifier/l;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 30
    invoke-direct {p0, v3, p1, p2}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 31
    iput-object p3, p0, Lcom/google/android/gms/security/verifier/af;->f:Lcom/android/volley/x;

    .line 32
    iput-object p4, p0, Lcom/google/android/gms/security/verifier/af;->g:Lcom/google/android/gms/security/verifier/l;

    .line 33
    iput p5, p0, Lcom/google/android/gms/security/verifier/af;->h:I

    .line 35
    new-instance v1, Lcom/android/volley/f;

    sget-object v0, Lcom/google/android/gms/security/verifier/x;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v0, v3, v2}, Lcom/android/volley/f;-><init>(IIF)V

    iput-object v1, p0, Lcom/android/volley/p;->d:Lcom/android/volley/z;

    .line 36
    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    iget-object v0, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v1, "X-Goog-Upload-Status"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    if-eqz v0, :cond_0

    const-string v1, "active"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 63
    :cond_0
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v0, v1, v2

    .line 64
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Unexpected status"

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    .line 67
    :cond_1
    iget-object v0, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v1, "X-Goog-Upload-URL"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 68
    invoke-static {v0}, Lcom/google/af/c/b/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 69
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Server did not provide an upload location"

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0

    .line 73
    :cond_2
    new-array v1, v3, [Ljava/lang/Object;

    aput-object v0, v1, v2

    .line 74
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 21
    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/af;->f:Lcom/android/volley/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/af;->f:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final i()Ljava/util/Map;
    .locals 3

    .prologue
    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 51
    const-string v1, "X-Goog-Upload-Protocol"

    const-string v2, "resumable"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    const-string v1, "X-Goog-Upload-Command"

    const-string v2, "start"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v1, "X-Goog-Upload-Http-Method"

    const-string v2, "POST"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v1, "X-Goog-Upload-Header-Content-Length"

    iget v2, p0, Lcom/google/android/gms/security/verifier/af;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, "application/octet-stream"

    return-object v0
.end method

.method public final m()[B
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/af;->g:Lcom/google/android/gms/security/verifier/l;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    return-object v0
.end method
