.class public final Lcom/google/android/gms/drive/metadata/a/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final b:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final c:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final d:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final e:Lcom/google/android/gms/drive/metadata/a/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/ap;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    const-string v2, "modifiedDate"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/ap;-><init>(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/ao;->a:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 40
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/aq;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/l;->d:Lcom/google/android/gms/drive/metadata/internal/a/o;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/aq;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/ao;->b:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 59
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/ar;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/l;->a:Lcom/google/android/gms/drive/metadata/internal/a/m;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/ar;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/ao;->c:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 77
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/as;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/l;->e:Lcom/google/android/gms/drive/metadata/internal/a/q;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/as;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/ao;->d:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 99
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/at;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/l;->b:Lcom/google/android/gms/drive/metadata/internal/a/n;

    const-string v2, "lastViewedByMeDate"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/at;-><init>(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/ao;->e:Lcom/google/android/gms/drive/metadata/a/ay;

    return-void
.end method
