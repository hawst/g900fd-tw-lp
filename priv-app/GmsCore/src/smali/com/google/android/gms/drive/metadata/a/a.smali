.class public final Lcom/google/android/gms/drive/metadata/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final B:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final C:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final D:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final E:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final F:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final G:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final a:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final b:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final c:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final d:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final e:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final f:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final g:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final h:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final i:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final j:Lcom/google/android/gms/drive/metadata/a/al;

.field public static final k:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final l:Lcom/google/android/gms/drive/metadata/a/al;

.field public static final m:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final n:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final o:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final p:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final q:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final r:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final s:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final t:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final u:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final v:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final w:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final x:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final y:Lcom/google/android/gms/drive/metadata/a/ay;

.field public static final z:Lcom/google/android/gms/drive/metadata/a/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/b;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->a:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/b;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->a:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 97
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/m;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    const-string v2, "title"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/m;-><init>(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->b:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 115
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/x;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    const-string v2, "mimeType"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/x;-><init>(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->c:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 134
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/ac;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->y:Lcom/google/android/gms/drive/metadata/internal/a/h;

    const-string v2, "starred"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/ac;-><init>(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->d:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 156
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/ad;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->B:Lcom/google/android/gms/drive/metadata/internal/a/j;

    const-string v2, "trashed"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/ad;-><init>(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->e:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 180
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/ae;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->l:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/ae;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->f:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 198
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/af;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->m:Lcom/google/android/gms/drive/metadata/internal/a/d;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/af;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->g:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 216
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/ag;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->j:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/ag;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->h:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 236
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/ah;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->o:Lcom/google/android/gms/drive/metadata/f;

    const-string v2, "sharedWithMe"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/ah;-><init>(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->i:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 255
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/c;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    const-string v2, "parents"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/c;-><init>(Lcom/google/android/gms/drive/metadata/b;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->j:Lcom/google/android/gms/drive/metadata/a/al;

    .line 331
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/d;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->b:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/d;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->k:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 349
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/e;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->t:Lcom/google/android/gms/drive/metadata/b;

    const-string v2, "owners"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/e;-><init>(Lcom/google/android/gms/drive/metadata/b;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->l:Lcom/google/android/gms/drive/metadata/a/al;

    .line 410
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/f;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->d:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/f;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->m:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 428
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/g;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->k:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/g;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->n:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 446
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/h;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->e:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/h;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->o:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 464
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/i;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->f:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/i;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->p:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 482
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/j;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->g:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/j;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->q:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 500
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/k;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->q:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/k;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->r:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 521
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/l;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->n:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/l;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->s:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 539
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/n;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->s:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/n;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->t:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 558
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/o;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->x:Lcom/google/android/gms/drive/metadata/internal/a/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/o;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->u:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 577
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/p;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->C:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/p;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->v:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 596
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/q;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->D:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/q;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->w:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 614
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/r;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->i:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/r;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->x:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 635
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/s;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->h:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/s;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->y:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 653
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/t;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->z:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/t;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->z:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 678
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/u;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->p:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/u;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->A:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 697
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/v;

    invoke-direct {v0}, Lcom/google/android/gms/drive/metadata/a/v;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->B:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 737
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/w;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->v:Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v2, "sharingUser"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/w;-><init>(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->C:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 752
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/y;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->u:Lcom/google/android/gms/drive/metadata/internal/n;

    const-string v2, "lastModifyingUser"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/a/y;-><init>(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->D:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 766
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/z;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/z;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->E:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 792
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/aa;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->F:Lcom/google/android/gms/drive/metadata/internal/c;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/aa;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->F:Lcom/google/android/gms/drive/metadata/a/ay;

    .line 810
    new-instance v0, Lcom/google/android/gms/drive/metadata/a/ab;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->G:Lcom/google/android/gms/drive/metadata/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/a/ab;-><init>(Lcom/google/android/gms/drive/metadata/f;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/a/a;->G:Lcom/google/android/gms/drive/metadata/a/ay;

    return-void
.end method
