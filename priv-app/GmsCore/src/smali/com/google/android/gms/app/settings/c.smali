.class final Lcom/google/android/gms/app/settings/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/common/api/x;


# instance fields
.field final synthetic a:Lcom/google/android/gms/app/settings/GoogleSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/app/settings/GoogleSettingsActivity;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/google/android/gms/app/settings/c;->a:Lcom/google/android/gms/app/settings/GoogleSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/app/settings/GoogleSettingsActivity;B)V
    .locals 0

    .prologue
    .line 449
    invoke-direct {p0, p1}, Lcom/google/android/gms/app/settings/c;-><init>(Lcom/google/android/gms/app/settings/GoogleSettingsActivity;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 2

    .prologue
    .line 449
    check-cast p1, Lcom/google/android/gms/photos/autobackup/k;

    invoke-interface {p1}, Lcom/google/android/gms/photos/autobackup/k;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/photos/autobackup/k;->b()Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/app/settings/c;->a:Lcom/google/android/gms/app/settings/GoogleSettingsActivity;

    iget-object v1, p0, Lcom/google/android/gms/app/settings/c;->a:Lcom/google/android/gms/app/settings/GoogleSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->b(Lcom/google/android/gms/app/settings/GoogleSettingsActivity;)Lcom/google/android/gms/common/widget/a/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/common/widget/a/i;)V

    :cond_0
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 454
    sget-object v0, Lcom/google/android/gms/photos/autobackup/d;->c:Lcom/google/android/gms/photos/autobackup/f;

    iget-object v1, p0, Lcom/google/android/gms/app/settings/c;->a:Lcom/google/android/gms/app/settings/GoogleSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;->a(Lcom/google/android/gms/app/settings/GoogleSettingsActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/f;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 457
    return-void
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 461
    return-void
.end method
