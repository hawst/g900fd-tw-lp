.class public final Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/identity/accounts/service/i;

.field private static final b:Landroid/content/Intent;

.field private static final c:Ljava/util/ArrayList;

.field private static final d:Lcom/google/android/gms/identity/accounts/service/c;


# instance fields
.field private final e:Lcom/google/android/gms/identity/accounts/service/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.identity.accounts.INTENT_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->b:Landroid/content/Intent;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->c:Ljava/util/ArrayList;

    .line 69
    new-instance v0, Lcom/google/android/gms/identity/accounts/service/a;

    invoke-direct {v0}, Lcom/google/android/gms/identity/accounts/service/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->d:Lcom/google/android/gms/identity/accounts/service/c;

    .line 88
    new-instance v0, Lcom/google/android/gms/identity/accounts/service/b;

    invoke-direct {v0}, Lcom/google/android/gms/identity/accounts/service/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->a:Lcom/google/android/gms/identity/accounts/service/i;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->d:Lcom/google/android/gms/identity/accounts/service/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;-><init>(Lcom/google/android/gms/identity/accounts/service/c;)V

    .line 107
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/identity/accounts/service/c;)V
    .locals 1

    .prologue
    .line 101
    const-string v0, "AccountDataUtil"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 102
    iput-object p1, p0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->e:Lcom/google/android/gms/identity/accounts/service/c;

    .line 103
    return-void
.end method

.method static synthetic a()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->b:Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->e:Lcom/google/android/gms/identity/accounts/service/c;

    invoke-interface {v0}, Lcom/google/android/gms/identity/accounts/service/c;->a()Ljava/util/List;

    move-result-object v2

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/AccountDataIntentService;->e:Lcom/google/android/gms/identity/accounts/service/c;

    invoke-interface {v0, p0}, Lcom/google/android/gms/identity/accounts/service/c;->a(Landroid/content/Context;)Lcom/google/android/gms/identity/accounts/security/a;

    move-result-object v3

    .line 113
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 114
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/accounts/service/d;

    .line 116
    :try_start_0
    invoke-interface {v0, v3}, Lcom/google/android/gms/identity/accounts/service/d;->a(Lcom/google/android/gms/identity/accounts/security/a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v5

    .line 118
    const-string v6, "AccountDataUtil"

    const/4 v7, 0x5

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 119
    const-string v6, "AccountDataUtil"

    const-string v7, "Problem in operation."

    invoke-static {v6, v7, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 124
    :cond_0
    :try_start_1
    invoke-interface {v0}, Lcom/google/android/gms/identity/accounts/service/d;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    .line 130
    :cond_1
    return-void
.end method
