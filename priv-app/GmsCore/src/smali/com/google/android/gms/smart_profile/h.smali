.class public final Lcom/google/android/gms/smart_profile/h;
.super Landroid/support/v4/app/ar;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/smart_profile/e;


# instance fields
.field private i:I

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/google/android/gms/smart_profile/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/ar;-><init>()V

    .line 96
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/smart_profile/h;
    .locals 3

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/gms/smart_profile/h;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/h;-><init>()V

    .line 48
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 49
    const-string v2, "viewerAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v2, "viewerPageId"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v2, "qualifiedId"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v2, "applicationId"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/h;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v0
.end method


# virtual methods
.method public final ai_()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/h;->m:Lcom/google/android/gms/smart_profile/d;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/d;->b:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    .line 60
    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/google/android/gms/smart_profile/i;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/smart_profile/i;-><init>(Landroid/support/v4/app/Fragment;Lcom/google/android/gms/smart_profile/SmartProfilePerson;)V

    .line 62
    invoke-virtual {p0, v1}, Lcom/google/android/gms/smart_profile/h;->a(Landroid/widget/ListAdapter;)V

    .line 63
    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/i;->notifyDataSetChanged()V

    .line 64
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->onActivityCreated(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/h;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 80
    new-instance v0, Lcom/google/android/gms/smart_profile/d;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/h;->j:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/h;->l:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/h;->k:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/gms/smart_profile/h;->i:I

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/smart_profile/d;-><init>(Landroid/support/v4/app/Fragment;Lcom/google/android/gms/smart_profile/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/h;->m:Lcom/google/android/gms/smart_profile/d;

    .line 82
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/h;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 70
    const-string v1, "applicationId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/smart_profile/h;->i:I

    .line 71
    const-string v1, "viewerAccountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/h;->j:Ljava/lang/String;

    .line 72
    const-string v1, "qualifiedId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/h;->k:Ljava/lang/String;

    .line 73
    const-string v1, "viewerPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/h;->l:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Landroid/support/v4/app/ar;->onStart()V

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/h;->m:Lcom/google/android/gms/smart_profile/d;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/d;->b()V

    .line 88
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Landroid/support/v4/app/ar;->onStop()V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/h;->m:Lcom/google/android/gms/smart_profile/d;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/d;->c()V

    .line 94
    return-void
.end method
