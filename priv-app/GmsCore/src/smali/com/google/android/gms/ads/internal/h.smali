.class final Lcom/google/android/gms/ads/internal/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field A:Ljava/util/HashSet;

.field public final a:Lcom/google/android/gms/ads/internal/g;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/content/Context;

.field public final d:Lcom/google/android/a/w;

.field public final e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field public f:Lcom/google/android/gms/ads/internal/util/a;

.field public g:Lcom/google/android/gms/ads/internal/client/c;

.field public h:Lcom/google/android/gms/ads/internal/client/f;

.field public i:Lcom/google/android/gms/ads/internal/util/a;

.field public j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field public k:Lcom/google/android/gms/ads/internal/o/a;

.field public l:Lcom/google/android/gms/ads/internal/o/b;

.field public m:Lcom/google/android/gms/ads/internal/o/c;

.field public n:Lcom/google/android/gms/ads/internal/client/s;

.field public o:Lcom/google/android/gms/ads/internal/purchase/a/n;

.field public p:Lcom/google/android/gms/ads/internal/purchase/a/d;

.field public q:Lcom/google/android/gms/ads/internal/f/a/g;

.field public r:Lcom/google/android/gms/ads/internal/f/a/j;

.field public s:Lcom/google/android/gms/ads/internal/e/a/d;

.field public t:Ljava/util/List;

.field public u:Lcom/google/android/gms/ads/internal/purchase/m;

.field public v:Lcom/google/android/gms/ads/internal/o/h;

.field public w:Landroid/view/View;

.field public x:I

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/h;->v:Lcom/google/android/gms/ads/internal/o/h;

    .line 188
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/h;->w:Landroid/view/View;

    .line 189
    iput v0, p0, Lcom/google/android/gms/ads/internal/h;->x:I

    .line 190
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/h;->y:Z

    .line 191
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/h;->z:Z

    .line 192
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/h;->A:Ljava/util/HashSet;

    .line 199
    iget-boolean v0, p2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_0

    .line 200
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    .line 207
    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/h;->j:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 208
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/h;->b:Ljava/lang/String;

    .line 209
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/h;->c:Landroid/content/Context;

    .line 210
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/h;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    .line 211
    new-instance v0, Lcom/google/android/a/w;

    new-instance v1, Lcom/google/android/gms/ads/internal/m;

    invoke-direct {v1, p0}, Lcom/google/android/gms/ads/internal/m;-><init>(Lcom/google/android/gms/ads/internal/h;)V

    invoke-direct {v0, v1}, Lcom/google/android/a/w;-><init>(Lcom/google/android/a/p;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/h;->d:Lcom/google/android/a/w;

    .line 212
    return-void

    .line 202
    :cond_0
    new-instance v0, Lcom/google/android/gms/ads/internal/g;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/internal/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    iget v1, p2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/g;->setMinimumWidth(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    iget v1, p2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/g;->setMinimumHeight(I)V

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h;->a:Lcom/google/android/gms/ads/internal/g;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/g;->setVisibility(I)V

    goto :goto_0
.end method
