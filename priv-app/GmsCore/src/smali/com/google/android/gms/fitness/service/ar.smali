.class final Lcom/google/android/gms/fitness/service/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/fitness/l/f;

.field final synthetic c:Lcom/google/android/gms/fitness/service/aq;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/service/aq;Ljava/lang/String;Lcom/google/android/gms/fitness/l/f;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/ar;->c:Lcom/google/android/gms/fitness/service/aq;

    iput-object p2, p0, Lcom/google/android/gms/fitness/service/ar;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/fitness/service/ar;->b:Lcom/google/android/gms/fitness/l/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/ar;->c:Lcom/google/android/gms/fitness/service/aq;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/aq;->c:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/ar;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/ar;->b:Lcom/google/android/gms/fitness/l/f;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/ar;->c:Lcom/google/android/gms/fitness/service/aq;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/aq;->e:Lcom/google/android/gms/fitness/service/au;

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/ar;->c:Lcom/google/android/gms/fitness/service/aq;

    new-instance v1, Lcom/google/android/gms/fitness/service/au;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/ar;->c:Lcom/google/android/gms/fitness/service/aq;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/fitness/service/au;-><init>(Lcom/google/android/gms/fitness/service/aq;B)V

    iput-object v1, v0, Lcom/google/android/gms/fitness/service/aq;->e:Lcom/google/android/gms/fitness/service/au;

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/ar;->c:Lcom/google/android/gms/fitness/service/aq;

    iget-object v1, v0, Lcom/google/android/gms/fitness/service/aq;->b:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/ar;->c:Lcom/google/android/gms/fitness/service/aq;

    iget-object v2, v0, Lcom/google/android/gms/fitness/service/aq;->e:Lcom/google/android/gms/fitness/service/au;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 49
    :cond_0
    return-void
.end method
