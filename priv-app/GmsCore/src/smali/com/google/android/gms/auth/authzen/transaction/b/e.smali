.class public final Lcom/google/android/gms/auth/authzen/transaction/b/e;
.super Lcom/google/android/gms/auth/authzen/transaction/b/c;
.source "SourceFile"


# instance fields
.field private final l:Lcom/google/ab/b/a/a/r;


# direct methods
.method public constructor <init>(Lcom/google/ab/b/a/a/r;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/b/c;-><init>()V

    .line 120
    iput-object p1, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->l:Lcom/google/ab/b/a/a/r;

    .line 121
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a()V

    .line 122
    return-void
.end method

.method private static a(Lcom/google/ab/b/a/a/q;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 293
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/q;->f:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v1}, Lcom/google/ab/b/a/a/e;->c()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v1, v0}, Lcom/google/ab/b/a/a/e;->a(I)Lcom/google/ab/b/a/a/l;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/ab/b/a/a/l;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v1, v0}, Lcom/google/ab/b/a/a/e;->a(I)Lcom/google/ab/b/a/a/l;

    move-result-object v1

    iget v1, v1, Lcom/google/ab/b/a/a/l;->b:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static a(Lcom/google/ab/b/a/a/r;)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 27
    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->f:Lcom/google/ab/b/a/a/f;

    .line 28
    iget-object v3, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    .line 29
    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v0

    .line 32
    :cond_1
    iget-object v4, v3, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    .line 33
    if-eqz v4, :cond_0

    iget-boolean v5, v4, Lcom/google/ab/b/a/a/e;->a:Z

    if-eqz v5, :cond_0

    .line 36
    iget v4, v4, Lcom/google/ab/b/a/a/e;->b:I

    .line 41
    iget-boolean v5, v3, Lcom/google/ab/b/a/a/q;->d:Z

    if-eqz v5, :cond_2

    invoke-virtual {v2}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/ab/b/a/a/m;->I:Z

    if-eqz v5, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/ab/b/a/a/m;->K:Z

    if-eqz v5, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/ab/b/a/a/m;->M:Z

    if-eqz v5, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/ab/b/a/a/m;->O:Z

    if-eqz v5, :cond_0

    .line 53
    :cond_2
    iget-boolean v5, v2, Lcom/google/ab/b/a/a/f;->c:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v2, Lcom/google/ab/b/a/a/f;->a:Z

    if-eqz v5, :cond_0

    .line 57
    invoke-virtual {v2}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v5

    if-lt v5, v6, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/ab/b/a/a/m;->a:Z

    if-eqz v5, :cond_0

    invoke-virtual {v2, v1}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/ab/b/a/a/m;->a:Z

    if-eqz v5, :cond_0

    .line 63
    if-eq v4, v1, :cond_3

    .line 65
    if-ne v4, v6, :cond_4

    .line 69
    invoke-static {v3}, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a(Lcom/google/ab/b/a/a/q;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 72
    invoke-virtual {v2, v0}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v4

    .line 73
    iget-boolean v5, v4, Lcom/google/ab/b/a/a/m;->G:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v4, Lcom/google/ab/b/a/a/m;->m:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v4, Lcom/google/ab/b/a/a/m;->i:Z

    if-eqz v5, :cond_0

    iget-boolean v4, v4, Lcom/google/ab/b/a/a/m;->k:Z

    if-eqz v4, :cond_0

    .line 102
    :cond_3
    invoke-static {v3}, Lcom/google/android/gms/auth/authzen/transaction/b/e;->c(Lcom/google/ab/b/a/a/q;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 106
    invoke-virtual {v2, v1}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v2

    .line 107
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->e:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->c:Z

    if-eqz v3, :cond_0

    iget-boolean v2, v2, Lcom/google/ab/b/a/a/m;->A:Z

    if-eqz v2, :cond_0

    move v0, v1

    .line 113
    goto/16 :goto_0

    .line 79
    :cond_4
    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 83
    invoke-static {v3}, Lcom/google/android/gms/auth/authzen/transaction/b/e;->b(Lcom/google/ab/b/a/a/q;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 86
    invoke-virtual {v2, v0}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v4

    .line 87
    iget-boolean v5, v4, Lcom/google/ab/b/a/a/m;->g:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v4, Lcom/google/ab/b/a/a/m;->q:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v4, Lcom/google/ab/b/a/a/m;->u:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v4, Lcom/google/ab/b/a/a/m;->s:Z

    if-eqz v5, :cond_0

    iget-boolean v4, v4, Lcom/google/ab/b/a/a/m;->w:Z

    if-nez v4, :cond_3

    goto/16 :goto_0
.end method

.method private static b(Lcom/google/ab/b/a/a/q;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 301
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/q;->f:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v1}, Lcom/google/ab/b/a/a/e;->c()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v1, v0}, Lcom/google/ab/b/a/a/e;->a(I)Lcom/google/ab/b/a/a/l;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/ab/b/a/a/l;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v1, v0}, Lcom/google/ab/b/a/a/e;->a(I)Lcom/google/ab/b/a/a/l;

    move-result-object v1

    iget v1, v1, Lcom/google/ab/b/a/a/l;->b:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static c(Lcom/google/ab/b/a/a/q;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 309
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/q;->f:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v1}, Lcom/google/ab/b/a/a/e;->c()I

    move-result v1

    if-le v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v1, v0}, Lcom/google/ab/b/a/a/e;->a(I)Lcom/google/ab/b/a/a/l;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/ab/b/a/a/l;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v1, v0}, Lcom/google/ab/b/a/a/e;->a(I)Lcom/google/ab/b/a/a/l;

    move-result-object v1

    iget v1, v1, Lcom/google/ab/b/a/a/l;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->l:Lcom/google/ab/b/a/a/r;

    iget-object v0, v0, Lcom/google/ab/b/a/a/r;->f:Lcom/google/ab/b/a/a/f;

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->l:Lcom/google/ab/b/a/a/r;

    iget-object v1, v1, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    .line 128
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    invoke-virtual {v0}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v2

    if-lez v2, :cond_5

    .line 134
    invoke-virtual {v0, v6}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v2

    .line 135
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->I:Z

    if-eqz v3, :cond_2

    .line 136
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/e;->b:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/ab/b/a/a/m;->J:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_2
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->K:Z

    if-eqz v3, :cond_3

    .line 141
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/e;->c:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/ab/b/a/a/m;->L:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_3
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->M:Z

    if-eqz v3, :cond_4

    .line 146
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/e;->d:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/ab/b/a/a/m;->N:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_4
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->O:Z

    if-eqz v3, :cond_5

    .line 151
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/e;->e:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/ab/b/a/a/m;->P:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_5
    iget-boolean v2, v0, Lcom/google/ab/b/a/a/f;->c:Z

    if-eqz v2, :cond_6

    .line 159
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/a/d;->b:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/ab/b/a/a/f;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_6
    iget-boolean v2, v0, Lcom/google/ab/b/a/a/f;->a:Z

    if-eqz v2, :cond_7

    .line 164
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/a/d;->c:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/ab/b/a/a/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_7
    invoke-virtual {v0}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v2

    if-lez v2, :cond_8

    invoke-virtual {v0, v6}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/ab/b/a/a/m;->a:Z

    if-eqz v2, :cond_8

    .line 170
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/a/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ab/b/a/a/m;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_8
    invoke-virtual {v0}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v2

    if-le v2, v7, :cond_9

    invoke-virtual {v0, v7}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/ab/b/a/a/m;->a:Z

    if-eqz v2, :cond_9

    .line 176
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/a/d;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v4

    iget-object v4, v4, Lcom/google/ab/b/a/a/m;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_9
    invoke-static {v1}, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a(Lcom/google/ab/b/a/a/q;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 183
    invoke-virtual {v0}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v2

    if-lez v2, :cond_d

    .line 184
    invoke-virtual {v0, v6}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v2

    .line 185
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->G:Z

    if-eqz v3, :cond_a

    .line 186
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/c;->b:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/ab/b/a/a/m;->H:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_a
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->m:Z

    if-eqz v3, :cond_b

    .line 191
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/c;->c:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/ab/b/a/a/m;->n:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_b
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->i:Z

    if-eqz v3, :cond_c

    .line 196
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/c;->d:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/ab/b/a/a/m;->j:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_c
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->k:Z

    if-eqz v3, :cond_d

    .line 201
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/c;->e:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/ab/b/a/a/m;->l:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_d
    invoke-static {v1}, Lcom/google/android/gms/auth/authzen/transaction/b/e;->b(Lcom/google/ab/b/a/a/q;)Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v0}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v2

    if-lez v2, :cond_f

    .line 212
    invoke-virtual {v0, v6}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v2

    .line 213
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->g:Z

    if-eqz v3, :cond_e

    .line 214
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/b;->b:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/ab/b/a/a/m;->h:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_e
    iget-boolean v3, v2, Lcom/google/ab/b/a/a/m;->q:Z

    if-eqz v3, :cond_f

    .line 219
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v4, Lcom/google/android/gms/auth/authzen/transaction/a/b;->c:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/ab/b/a/a/m;->r:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_f
    invoke-static {v1}, Lcom/google/android/gms/auth/authzen/transaction/b/e;->c(Lcom/google/ab/b/a/a/q;)Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-virtual {v0}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v1

    if-le v1, v7, :cond_13

    .line 229
    invoke-virtual {v0, v7}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v1

    .line 230
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/m;->e:Z

    if-eqz v2, :cond_10

    .line 231
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/a/a;->b:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/ab/b/a/a/m;->f:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_10
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/m;->c:Z

    if-eqz v2, :cond_11

    .line 236
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/a/a;->c:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/ab/b/a/a/m;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_11
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/m;->A:Z

    if-eqz v2, :cond_12

    .line 241
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/a/a;->e:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/ab/b/a/a/m;->B:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_12
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/m;->C:Z

    if-eqz v2, :cond_13

    .line 246
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/a/a;->d:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/ab/b/a/a/m;->D:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_13
    invoke-virtual {v0}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v1

    if-lez v1, :cond_17

    .line 252
    invoke-virtual {v0, v6}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v1

    .line 253
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/m;->u:Z

    if-eqz v2, :cond_14

    .line 254
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->c:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/ab/b/a/a/m;->v:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_14
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/m;->s:Z

    if-eqz v2, :cond_15

    .line 259
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->d:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/ab/b/a/a/m;->t:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_15
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/m;->w:Z

    if-eqz v2, :cond_16

    .line 264
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->e:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/ab/b/a/a/m;->x:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_16
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/m;->y:Z

    if-eqz v2, :cond_17

    .line 269
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->f:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/ab/b/a/a/m;->z:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_17
    invoke-virtual {v0}, Lcom/google/ab/b/a/a/f;->c()I

    move-result v1

    if-lez v1, :cond_0

    .line 277
    invoke-virtual {v0, v6}, Lcom/google/ab/b/a/a/f;->a(I)Lcom/google/ab/b/a/a/m;

    move-result-object v0

    .line 278
    iget-boolean v1, v0, Lcom/google/ab/b/a/a/m;->E:Z

    if-eqz v1, :cond_18

    .line 279
    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v2, Lcom/google/android/gms/auth/authzen/transaction/b/h;->g:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/ab/b/a/a/m;->F:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_18
    iget-boolean v1, v0, Lcom/google/ab/b/a/a/m;->o:Z

    if-eqz v1, :cond_0

    .line 284
    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/b/e;->a:Landroid/os/Bundle;

    sget-object v2, Lcom/google/android/gms/auth/authzen/transaction/b/h;->h:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/ab/b/a/a/m;->p:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
