.class public Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;
.super Lcom/google/android/gms/auth/login/g;
.source "SourceFile"

# interfaces
.implements Lcom/android/setupwizard/navigationbar/a;


# static fields
.field private static final q:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private r:Ljava/lang/String;

.field private s:Lcom/google/android/setupwizard/util/b;

.field private t:Landroid/view/View;

.field private u:Landroid/content/BroadcastReceiver;

.field private v:Landroid/os/Handler;

.field private w:Landroid/content/IntentFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "GLSActivity"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "CheckinInterstitialActivity"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->q:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/g;-><init>()V

    .line 53
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.checkin.CHECKIN_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->w:Landroid/content/IntentFilter;

    return-void
.end method

.method public static a(Landroid/content/Context;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "useImmersiveMode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "theme"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 33
    sget-object v2, Lcom/google/android/gms/auth/firstparty/shared/k;->m:Lcom/google/android/gms/auth/firstparty/shared/k;

    move-object v1, v0

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/k;ZZZ)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->finish()V

    return-void
.end method

.method static synthetic l()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->q:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method private m()V
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    sget v1, Lcom/google/android/gms/p;->bv:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/util/b;->c(II)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->t:Landroid/view/View;

    .line 173
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public final a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->o:Z

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a(ZZ)V

    .line 189
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 190
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 191
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->t:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/b;->removeView(Landroid/view/View;)V

    .line 181
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->m()V

    .line 183
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/auth/login/ab;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/ab;-><init>(Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->v:Landroid/os/Handler;

    .line 64
    new-instance v0, Lcom/google/android/gms/auth/login/ac;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->v:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/login/ac;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->u:Landroid/content/BroadcastReceiver;

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->u:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->w:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 67
    if-nez p1, :cond_0

    .line 69
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .line 70
    if-nez p1, :cond_0

    .line 71
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->r:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 79
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    const-string v0, "material_light"

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->r:Ljava/lang/String;

    .line 85
    :cond_1
    :goto_0
    const-string v0, "material"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 86
    sget v0, Lcom/google/android/gms/q;->z:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->setTheme(I)V

    .line 90
    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->requestWindowFeature(I)Z

    .line 91
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onCreate(Landroid/os/Bundle;)V

    .line 92
    new-instance v0, Lcom/google/android/setupwizard/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->setContentView(Landroid/view/View;)V

    .line 94
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->m()V

    .line 96
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.server.checkin.CHECKIN_NOW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->sendBroadcast(Landroid/content/Intent;)V

    sget-object v0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->q:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Starting checkin broadcast."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->v:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->v:Landroid/os/Handler;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    sget-object v0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->q:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Starting timeout of 120000ms."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 97
    return-void

    .line 82
    :cond_2
    const-string v0, "holo"

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->r:Ljava/lang/String;

    goto :goto_0

    .line 88
    :cond_3
    sget v0, Lcom/google/android/gms/q;->A:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->setTheme(I)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onDestroy()V

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->u:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 110
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 103
    const-string v0, "theme"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-void
.end method
