.class final Lcom/google/android/gms/wallet/common/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;


# static fields
.field private static final a:Ljava/util/concurrent/TimeUnit;


# instance fields
.field private final b:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 317
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sput-object v0, Lcom/google/android/gms/wallet/common/ac;->a:Ljava/util/concurrent/TimeUnit;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 322
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ac;->b:Ljava/util/concurrent/CountDownLatch;

    .line 323
    return-void
.end method


# virtual methods
.method public final T_()V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ac;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 336
    return-void
.end method

.method public final W_()V
    .locals 0

    .prologue
    .line 341
    return-void
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 327
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ac;->b:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x64

    sget-object v1, Lcom/google/android/gms/wallet/common/ac;->a:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ac;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 346
    return-void
.end method
