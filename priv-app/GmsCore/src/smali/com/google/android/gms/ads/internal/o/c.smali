.class public final Lcom/google/android/gms/ads/internal/o/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/ads/internal/o/e;

.field public final b:Ljava/util/LinkedList;

.field public final c:Ljava/lang/Object;

.field public d:J

.field public e:J

.field public f:Z

.field public g:J

.field public h:J

.field public i:J

.field public j:J

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/ads/internal/o/e;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    .line 97
    iput-wide v2, p0, Lcom/google/android/gms/ads/internal/o/c;->d:J

    .line 99
    iput-wide v2, p0, Lcom/google/android/gms/ads/internal/o/c;->e:J

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/o/c;->f:Z

    .line 103
    iput-wide v2, p0, Lcom/google/android/gms/ads/internal/o/c;->g:J

    .line 105
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/o/c;->h:J

    .line 107
    iput-wide v2, p0, Lcom/google/android/gms/ads/internal/o/c;->i:J

    .line 109
    iput-wide v2, p0, Lcom/google/android/gms/ads/internal/o/c;->j:J

    .line 117
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/o/c;->a:Lcom/google/android/gms/ads/internal/o/e;

    .line 118
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/o/c;->k:Ljava/lang/String;

    .line 119
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/o/c;->l:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/c;->b:Ljava/util/LinkedList;

    .line 121
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->a()Lcom/google/android/gms/ads/internal/o/e;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/ads/internal/o/c;-><init>(Lcom/google/android/gms/ads/internal/o/e;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 10

    .prologue
    .line 233
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/c;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 234
    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 235
    const-string v0, "seq_num"

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/o/c;->k:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v0, "slotid"

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/o/c;->l:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v0, "ismediation"

    iget-boolean v3, p0, Lcom/google/android/gms/ads/internal/o/c;->f:Z

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 238
    const-string v0, "treq"

    iget-wide v4, p0, Lcom/google/android/gms/ads/internal/o/c;->i:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 239
    const-string v0, "tresponse"

    iget-wide v4, p0, Lcom/google/android/gms/ads/internal/o/c;->j:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 240
    const-string v0, "timp"

    iget-wide v4, p0, Lcom/google/android/gms/ads/internal/o/c;->e:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 241
    const-string v0, "tload"

    iget-wide v4, p0, Lcom/google/android/gms/ads/internal/o/c;->g:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 242
    const-string v0, "pcc"

    iget-wide v4, p0, Lcom/google/android/gms/ads/internal/o/c;->h:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 243
    const-string v0, "tfetch"

    iget-wide v4, p0, Lcom/google/android/gms/ads/internal/o/c;->d:J

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 244
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/c;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/o/d;

    .line 246
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v6, "topen"

    iget-wide v8, v0, Lcom/google/android/gms/ads/internal/o/d;->a:J

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v6, "tclose"

    iget-wide v8, v0, Lcom/google/android/gms/ads/internal/o/d;->b:J

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 248
    :cond_0
    :try_start_1
    const-string v0, "tclick"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 249
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method
