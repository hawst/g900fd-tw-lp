.class final Lcom/google/android/gms/update/w;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/gms/update/w;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/update/w;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 101
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "Received a status changed notification, redrawing."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/update/w;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    .line 106
    :goto_0
    return-void

    .line 105
    :cond_0
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "Received a status changed notification, ignoring."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
