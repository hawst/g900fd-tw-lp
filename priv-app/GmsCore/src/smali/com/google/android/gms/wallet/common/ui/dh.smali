.class public final Lcom/google/android/gms/wallet/common/ui/dh;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/wallet/service/k;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 4

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/dh;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/dh;-><init>()V

    .line 43
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 44
    const-string v2, "serviceConnectionFlags"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 45
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 46
    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 47
    const-string v2, "localMode"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 48
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dh;->setArguments(Landroid/os/Bundle;)V

    .line 49
    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/dh;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/dh;-><init>()V

    .line 63
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 64
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 65
    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 66
    const-string v2, "sessionId"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v2, "localMode"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 68
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dh;->setArguments(Landroid/os/Bundle;)V

    .line 69
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":TransactionRetainerFragment"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wallet/service/k;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    return-object v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    if-nez v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/dh;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 101
    const-string v0, "buyFlowConfig"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 102
    const-string v1, "account"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    .line 103
    const-string v3, "localMode"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 104
    if-eqz v3, :cond_1

    .line 105
    const-string v3, "sessionId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 106
    new-instance v3, Lcom/google/android/gms/wallet/service/d;

    invoke-direct {v3, v0, v1, p1, v2}, Lcom/google/android/gms/wallet/service/d;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    .line 113
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/service/k;->a()V

    .line 115
    :cond_0
    return-void

    .line 109
    :cond_1
    const-string v3, "serviceConnectionFlags"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 110
    new-instance v3, Lcom/google/android/gms/wallet/service/e;

    invoke-direct {v3, v2, v0, v1, p1}, Lcom/google/android/gms/wallet/service/e;-><init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 120
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dh;->setRetainInstance(Z)V

    .line 121
    return-void
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/service/k;->b()V

    .line 128
    return-void
.end method
