.class public final Lcom/google/android/gms/social/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    const-string v0, "location_sharing.help_url"

    const-string v1, "http://www.google.com/support/mobile/"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/social/a/a;->a:Lcom/google/android/gms/common/a/d;

    .line 23
    const-string v0, "location_sharing.server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/social/a/a;->b:Lcom/google/android/gms/common/a/d;

    .line 29
    const-string v0, "location_sharing.api_path"

    const-string v1, "/socialuserlocation/v1/userLocationFrontend/"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/social/a/a;->c:Lcom/google/android/gms/common/a/d;

    .line 35
    const-string v0, "location_sharing.server_api_scope"

    const-string v1, "https://www.googleapis.com/auth/social.userlocation"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/social/a/a;->d:Lcom/google/android/gms/common/a/d;

    .line 43
    const-string v0, "location_sharing.apiary_trace"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/social/a/a;->e:Lcom/google/android/gms/common/a/d;

    return-void
.end method
