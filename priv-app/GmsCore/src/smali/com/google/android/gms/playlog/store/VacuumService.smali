.class public Lcom/google/android/gms/playlog/store/VacuumService;
.super Lcom/google/android/gms/gcm/ae;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/gms/gcm/ae;-><init>()V

    return-void
.end method

.method public static b()V
    .locals 6

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/gms/playlog/a/b;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 22
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 23
    invoke-static {}, Lcom/google/android/gms/playlog/store/VacuumService;->c()V

    .line 35
    :goto_0
    return-void

    .line 26
    :cond_0
    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v0, v1, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 28
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/gcm/aq;

    invoke-direct {v3}, Lcom/google/android/gms/gcm/aq;-><init>()V

    iput-wide v0, v3, Lcom/google/android/gms/gcm/aq;->a:J

    iput-wide v0, v3, Lcom/google/android/gms/gcm/aq;->b:J

    const-class v0, Lcom/google/android/gms/playlog/store/VacuumService;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/gcm/aq;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/aq;

    move-result-object v0

    const-string v1, "VacuumService"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/aq;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/aq;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/aq;->a(Z)Lcom/google/android/gms/gcm/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/aq;->b()Lcom/google/android/gms/gcm/PeriodicTask;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    goto :goto_0
.end method

.method private static c()V
    .locals 3

    .prologue
    .line 38
    const-string v0, "VacuumService"

    const-string v1, "Turn off VacuumService"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v0

    const-string v1, "VacuumService"

    const-class v2, Lcom/google/android/gms/playlog/store/VacuumService;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/gcm/y;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/gcm/be;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 45
    const-string v0, "VacuumService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Vacuum at: now="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tag="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/gms/gcm/be;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    sget-object v0, Lcom/google/android/gms/playlog/a/b;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 48
    invoke-static {}, Lcom/google/android/gms/playlog/store/VacuumService;->c()V

    move v0, v1

    .line 60
    :goto_0
    return v0

    .line 53
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/playlog/store/a;->a()Lcom/google/android/gms/playlog/store/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/store/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v2, "VACUUM"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    const/4 v0, 0x1

    .line 60
    :goto_1
    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    const-string v2, "VacuumService"

    const-string v3, "Could not vacuum the database"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_1

    .line 60
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
