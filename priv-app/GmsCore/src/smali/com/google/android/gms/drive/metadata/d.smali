.class public final Lcom/google/android/gms/drive/metadata/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/database/r;

.field public final b:Lcom/google/android/gms/drive/api/k;

.field public final c:Landroid/content/Context;

.field public final d:Lcom/google/android/gms/drive/a/a/a;

.field private final e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

.field private final f:Lcom/google/android/gms/drive/d/f;

.field private final g:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/d;->f:Lcom/google/android/gms/drive/d/f;

    .line 50
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/d;->a:Lcom/google/android/gms/drive/database/r;

    .line 51
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->k()Lcom/google/android/gms/drive/api/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/d;->b:Lcom/google/android/gms/drive/api/k;

    .line 52
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/d;->c:Landroid/content/Context;

    .line 53
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/d;->e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/c;

    .line 54
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/d;->g:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    .line 55
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->l()Lcom/google/android/gms/drive/a/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/d;->d:Lcom/google/android/gms/drive/a/a/a;

    .line 56
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;J)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 126
    invoke-static {p1}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    .line 127
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 129
    :goto_0
    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p3, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v6, 0x1

    :goto_1
    move-object v1, p0

    move-wide v4, p4

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_2

    .line 134
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x5dd

    const-string v2, "Unique resource with the same identifier already exists."

    invoke-direct {v0, v1, v2, v7}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 127
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    invoke-interface {p0, v2, v0}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v6, v7

    .line 129
    goto :goto_1

    .line 137
    :cond_2
    return-void
.end method
