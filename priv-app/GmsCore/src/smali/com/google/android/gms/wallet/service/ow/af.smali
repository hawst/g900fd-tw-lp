.class public final Lcom/google/android/gms/wallet/service/ow/af;
.super Lcom/google/android/gms/wallet/c/b;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/wallet/service/ow/ak;

.field private final d:Lcom/google/android/gms/wallet/service/p;

.field private final e:Lcom/google/android/gms/wallet/service/ow/ar;

.field private final f:Lcom/google/android/gms/wallet/cache/h;

.field private final g:Lcom/google/android/gms/wallet/service/ow/a;

.field private final h:Lcom/google/android/gms/wallet/cache/j;

.field private final i:Lcom/google/android/gms/wallet/service/ow/m;

.field private final j:Lcom/google/android/gms/wallet/service/ow/w;

.field private final k:Lcom/google/android/gms/wallet/service/ow/k;

.field private final l:Lcom/google/android/gms/wallet/cache/f;

.field private final m:Lcom/google/android/gms/wallet/common/ab;

.field private final n:Lcom/google/android/gms/wallet/service/r;

.field private final o:Lcom/google/android/gms/wallet/service/ow/an;

.field private p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/google/android/gms/wallet/b/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/af;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/ak;Lcom/google/android/gms/wallet/service/ow/ar;Lcom/google/android/gms/wallet/cache/h;Lcom/google/android/gms/wallet/service/ow/a;Lcom/google/android/gms/wallet/cache/j;Lcom/google/android/gms/wallet/service/ow/m;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/service/ow/k;Lcom/google/android/gms/wallet/cache/f;Lcom/google/android/gms/wallet/common/ab;Lcom/google/android/gms/wallet/service/r;Lcom/google/android/gms/wallet/service/ow/an;)V
    .locals 2

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/android/gms/wallet/c/b;-><init>()V

    .line 152
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    .line 153
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/af;->c:Lcom/google/android/gms/wallet/service/ow/ak;

    .line 154
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/af;->e:Lcom/google/android/gms/wallet/service/ow/ar;

    .line 155
    new-instance v0, Lcom/google/android/gms/wallet/service/p;

    const-string v1, "NetworkOwService"

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/wallet/service/p;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->d:Lcom/google/android/gms/wallet/service/p;

    .line 156
    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/af;->f:Lcom/google/android/gms/wallet/cache/h;

    .line 157
    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ow/af;->g:Lcom/google/android/gms/wallet/service/ow/a;

    .line 158
    iput-object p6, p0, Lcom/google/android/gms/wallet/service/ow/af;->h:Lcom/google/android/gms/wallet/cache/j;

    .line 159
    iput-object p7, p0, Lcom/google/android/gms/wallet/service/ow/af;->i:Lcom/google/android/gms/wallet/service/ow/m;

    .line 160
    iput-object p8, p0, Lcom/google/android/gms/wallet/service/ow/af;->j:Lcom/google/android/gms/wallet/service/ow/w;

    .line 161
    iput-object p9, p0, Lcom/google/android/gms/wallet/service/ow/af;->k:Lcom/google/android/gms/wallet/service/ow/k;

    .line 162
    iput-object p10, p0, Lcom/google/android/gms/wallet/service/ow/af;->l:Lcom/google/android/gms/wallet/cache/f;

    .line 163
    iput-object p11, p0, Lcom/google/android/gms/wallet/service/ow/af;->m:Lcom/google/android/gms/wallet/common/ab;

    .line 164
    iput-object p12, p0, Lcom/google/android/gms/wallet/service/ow/af;->n:Lcom/google/android/gms/wallet/service/r;

    .line 165
    iput-object p13, p0, Lcom/google/android/gms/wallet/service/ow/af;->o:Lcom/google/android/gms/wallet/service/ow/an;

    .line 166
    return-void
.end method

.method private a(Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/i;Lcom/google/android/gms/wallet/common/b;Lcom/google/android/gms/wallet/service/ow/as;)Lcom/google/android/gms/wallet/FullWallet;
    .locals 10

    .prologue
    .line 966
    if-nez p1, :cond_1

    .line 967
    const-string v0, "NetworkOwService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 968
    const-string v0, "NetworkOwService"

    const-string v1, "fullWalletResponse=null, probably due to exception in FetchFullWalletTask"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ai;

    const/16 v1, 0x19d

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(I)V

    throw v0

    .line 973
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b()Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 974
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 998
    const-string v1, "NetworkOwService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unexpected ServerResponse type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ai;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(I)V

    throw v0

    .line 979
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    iget-object v2, p3, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    iget-object v3, p3, Lcom/google/aa/b/a/a/a/a/i;->g:Ljava/lang/String;

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    move-object v1, p2

    move-object v4, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 987
    new-instance v1, Lcom/google/android/gms/wallet/service/ow/ai;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(Landroid/app/PendingIntent;)V

    throw v1

    .line 989
    :sswitch_1
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ai;

    const/16 v1, 0x19b

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(I)V

    throw v0

    .line 991
    :sswitch_2
    const-string v0, "NetworkOwService"

    const-string v1, "NetworkError in getFullWallet"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 992
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ai;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(I)V

    throw v0

    .line 994
    :sswitch_3
    new-instance v1, Lcom/google/android/gms/wallet/service/ow/ai;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/b/a/c;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/b/a/c;)I

    move-result v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(I)V

    throw v1

    .line 1002
    :sswitch_4
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/aa/b/a/a/a/a/j;

    .line 1003
    iget-object v0, v7, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v0, v0

    .line 1004
    const/4 v8, 0x0

    .line 1005
    const/4 v1, 0x0

    .line 1007
    add-int/lit8 v0, v0, -0x1

    move v9, v0

    move-object v0, v1

    :goto_0
    if-ltz v9, :cond_6

    .line 1008
    iget-object v1, v7, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    aget v1, v1, v9

    packed-switch v1, :pswitch_data_0

    .line 1034
    :pswitch_0
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ai;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(I)V

    throw v0

    .line 1014
    :pswitch_1
    const/4 v1, 0x1

    .line 1007
    :goto_1
    add-int/lit8 v2, v9, -0x1

    move v9, v2

    move v8, v1

    goto :goto_0

    .line 1017
    :pswitch_2
    if-eqz p5, :cond_2

    iget-object v0, p5, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    :cond_2
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ai;

    const/16 v1, 0x19a

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(I)V

    throw v0

    :cond_3
    iget-object v1, p5, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->h:Lcom/google/android/gms/wallet/cache/j;

    iget-object v2, p5, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;I)Lcom/google/aa/a/a/a/b;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_4

    iget-object v0, v2, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    :cond_4
    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ai;

    const/16 v1, 0x19a

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(I)V

    throw v0

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v1, v0, p3, p2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;Lcom/google/checkout/inapp/proto/j;Lcom/google/aa/b/a/a/a/a/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/app/PendingIntent;

    move-result-object v0

    move v1, v8

    .line 1019
    goto :goto_1

    .line 1023
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    iget-object v2, p3, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    iget-object v3, p3, Lcom/google/aa/b/a/a/a/a/i;->g:Ljava/lang/String;

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    move-object v1, p2

    move-object v4, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;ILjava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    move v1, v8

    .line 1031
    goto :goto_1

    .line 1037
    :cond_6
    if-eqz v8, :cond_7

    .line 1038
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ai;

    const/16 v1, 0x199

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(I)V

    throw v0

    .line 1041
    :cond_7
    if-eqz v0, :cond_8

    .line 1042
    new-instance v1, Lcom/google/android/gms/wallet/service/ow/ai;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/ow/ai;-><init>(Landroid/app/PendingIntent;)V

    throw v1

    .line 1045
    :cond_8
    const-string v0, "onlinewallet"

    const-string v1, "full_wallet"

    invoke-static {p4, v0, v1}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    iget-object v0, p5, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->a()J

    move-result-wide v2

    invoke-static {v7, p3, v0, v2, v3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/j;Lcom/google/aa/b/a/a/a/a/i;Ljava/lang/String;J)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v1

    .line 1053
    iget-object v0, p5, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    if-nez v0, :cond_9

    .line 1054
    new-instance v0, Lcom/google/aa/a/a/a/d;

    invoke-direct {v0}, Lcom/google/aa/a/a/a/d;-><init>()V

    iput-object v0, p5, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    .line 1056
    :cond_9
    iget-object v0, p5, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/aa/a/a/a/d;->e:Z

    .line 1057
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->e:Lcom/google/android/gms/wallet/service/ow/ar;

    iget-object v3, p3, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/wallet/b/c;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v3, p5, v4, v5}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;J)V

    .line 1060
    return-object v1

    .line 974
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0x10 -> :sswitch_4
        0x11 -> :sswitch_3
        0x16 -> :sswitch_0
    .end sparse-switch

    .line 1008
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;
    .locals 2

    .prologue
    .line 1310
    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lcom/google/android/gms/wallet/o;

    move-result-object v0

    .line 1311
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1312
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/o;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    .line 1314
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1315
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/o;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    .line 1317
    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/service/ow/af;)Lcom/google/android/gms/wallet/service/ow/ak;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->c:Lcom/google/android/gms/wallet/service/ow/ak;

    return-object v0
.end method

.method private a(ILjava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1156
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/af;->getCallingUid()I

    move-result v2

    invoke-static {v1, p1, p2, v2, p3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;ILjava/lang/String;ILandroid/accounts/Account;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1163
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 1161
    :catch_1
    move-exception v1

    goto :goto_0

    .line 1159
    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/service/ow/af;ILjava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/af;->a(ILjava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1412
    const/4 v0, 0x1

    invoke-static {p4}, Lcom/google/android/gms/wallet/common/c;->b(Landroid/os/Bundle;)I

    move-result v1

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/gms/wallet/b/e;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1415
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 1417
    :try_start_0
    new-instance v0, Lcom/google/k/f/w;

    invoke-direct {v0}, Lcom/google/k/f/w;-><init>()V

    .line 1418
    invoke-static {}, Lcom/google/android/gms/common/analytics/f;->a()Lcom/google/android/gms/common/analytics/a/c;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/w;->a:Lcom/google/android/gms/common/analytics/a/c;

    .line 1419
    iput-object p3, v0, Lcom/google/k/f/w;->c:Ljava/lang/String;

    .line 1420
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/w;->f:Ljava/lang/Integer;

    .line 1421
    iput-object p2, v0, Lcom/google/k/f/w;->g:Ljava/lang/String;

    .line 1422
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v1, p3}, Lcom/google/android/gms/common/util/o;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1424
    if-eqz v1, :cond_3

    .line 1425
    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v0, Lcom/google/k/f/w;->d:Ljava/lang/Integer;

    .line 1426
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1427
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/k/f/w;->e:Ljava/lang/String;

    .line 1429
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/common/util/o;->a(Landroid/content/pm/PackageInfo;)I

    move-result v1

    .line 1430
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    .line 1431
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/k/f/w;->b:Ljava/lang/Integer;

    .line 1438
    :cond_1
    :goto_0
    new-instance v1, Lcom/google/android/gms/playlog/a;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    const/16 v5, 0x17

    invoke-direct {v1, v4, v5}, Lcom/google/android/gms/playlog/a;-><init>(Landroid/content/Context;I)V

    .line 1440
    const-string v4, "merchantError"

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v1, v4, v0, v5}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 1442
    invoke-virtual {v1}, Lcom/google/android/gms/playlog/a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1444
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 1447
    :cond_2
    return-void

    .line 1435
    :cond_3
    :try_start_1
    const-string v1, "NetworkOwService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to retrieve package info to log merchant error for: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1444
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method private static a(Landroid/os/Bundle;Lcom/google/android/gms/wallet/service/ow/as;)V
    .locals 2

    .prologue
    .line 684
    iget-object v0, p1, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 685
    const-string v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    iget-object v1, p1, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 687
    :cond_0
    const-string v0, "com.google.android.gms.wallet.EXTRA_ALLOW_ACCOUNT_SELECTION"

    iget-boolean v1, p1, Lcom/google/android/gms/wallet/service/ow/as;->f:Z

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 689
    return-void
.end method

.method private static a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/FullWalletRequest;I)V
    .locals 2

    .prologue
    .line 1178
    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lcom/google/android/gms/wallet/f;

    move-result-object v0

    .line 1179
    if-eqz p1, :cond_1

    .line 1180
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1181
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    .line 1183
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1184
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    .line 1187
    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p0, p2, v0, v1}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V

    .line 1188
    return-void
.end method

.method private a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/cache/i;)V
    .locals 1

    .prologue
    .line 1279
    invoke-static {p3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/u;)Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v0

    .line 1281
    invoke-direct {p0, p1, p2, v0, p4}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Lcom/google/android/gms/wallet/cache/i;)V

    .line 1283
    return-void
.end method

.method private a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Lcom/google/android/gms/wallet/cache/i;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 1242
    invoke-static {p2}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1243
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/as;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v3

    invoke-static {p3, v1, v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/b;)Lcom/google/aa/b/a/a/a/a/u;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->d()Z

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v7

    sget-object v2, Lcom/google/android/gms/wallet/b/h;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v1, v7, v2}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/aa/a/a/a/d;

    move-result-object v10

    move-object v2, v1

    move-object v7, v1

    move v8, v6

    move v9, v6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/wallet/service/ow/as;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;ZZ[Ljava/lang/String;ZZLcom/google/aa/a/a/a/d;)V

    .line 1256
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->e:Lcom/google/android/gms/wallet/service/ow/ar;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;)V

    .line 1258
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    invoke-static {v0, p2, v2, v3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1270
    :goto_0
    const/4 v2, 0x6

    invoke-virtual {p3}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/wallet/service/ow/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v1

    invoke-interface {p1, v2, v1, v0}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    .line 1273
    return-void

    .line 1262
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    invoke-static {v0, p2, p3, p4, v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Lcom/google/android/gms/wallet/cache/i;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    invoke-static {v0, p4, v1, v2}, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->a(Landroid/content/Context;IILjava/lang/String;)Ljava/lang/String;

    .line 1172
    invoke-static {p2, p3}, Lcom/google/android/gms/wallet/service/ow/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v0

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p1, p4, v0, v1}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    .line 1174
    return-void
.end method

.method static a(Lcom/google/android/gms/wallet/firstparty/b;[Lcom/google/checkout/inapp/proto/j;[I)V
    .locals 6

    .prologue
    .line 1467
    array-length v0, p1

    .line 1468
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1469
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1470
    array-length v3, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, p1, v0

    .line 1471
    if-eqz p2, :cond_0

    array-length v5, p2

    if-eqz v5, :cond_0

    iget v5, v4, Lcom/google/checkout/inapp/proto/j;->d:I

    invoke-static {p2, v5}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1473
    :cond_0
    iget-object v5, v4, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1474
    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1470
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1478
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/firstparty/b;->a([Ljava/lang/String;)Lcom/google/android/gms/wallet/firstparty/b;

    .line 1480
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [[B

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[B

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/firstparty/b;->a([[B)Lcom/google/android/gms/wallet/firstparty/b;

    .line 1483
    return-void
.end method

.method private a(Lcom/google/aa/b/a/a/a/a/n;Lcom/google/aa/b/a/a/a/a/m;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/common/b;Lcom/google/android/gms/wallet/c/i;Landroid/os/Bundle;Lcom/google/android/gms/wallet/Cart;ILjava/lang/String;Lcom/google/android/gms/wallet/cache/i;)Z
    .locals 24

    .prologue
    .line 533
    invoke-static/range {p7 .. p7}, Lcom/google/android/gms/wallet/common/c;->b(Landroid/os/Bundle;)I

    move-result v6

    .line 534
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    move-object/from16 v19, v0

    .line 536
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v4, :cond_1

    .line 537
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    invoke-static {v4}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/p;)Lcom/google/aa/a/a/a/f;

    move-result-object v7

    .line 539
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/af;->h:Lcom/google/android/gms/wallet/cache/j;

    move-object/from16 v0, v19

    iget-boolean v8, v0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    move-object/from16 v0, v19

    iget-boolean v9, v0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    move-object/from16 v0, v19

    iget-boolean v10, v0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    move-object/from16 v5, p4

    invoke-static/range {v4 .. v10}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/j;Landroid/accounts/Account;ILcom/google/aa/a/a/a/f;ZZZ)V

    .line 547
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/af;->l:Lcom/google/android/gms/wallet/cache/f;

    move-object/from16 v0, p10

    invoke-static {v4, v0, v6, v7}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/f;Ljava/lang/String;ILcom/google/aa/a/a/a/f;)V

    .line 552
    move-object/from16 v0, v19

    iget-boolean v4, v0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    if-eqz v4, :cond_0

    .line 553
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/wallet/service/ow/af;->f:Lcom/google/android/gms/wallet/cache/h;

    move-object/from16 v9, p4

    move-object/from16 v10, p10

    move v11, v6

    move-object v12, v7

    move-object/from16 v13, p11

    invoke-static/range {v8 .. v13}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/h;Landroid/accounts/Account;Ljava/lang/String;ILcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/i;)V

    .line 561
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/af;->n:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-static {v0, v5}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {v4, v5, v8, v7}, Lcom/google/android/gms/wallet/service/r;->b(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 565
    :cond_1
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 566
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    array-length v4, v4

    if-lez v4, :cond_4

    .line 567
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_3

    aget v7, v5, v4

    .line 568
    packed-switch v7, :pswitch_data_0

    .line 567
    :cond_2
    :pswitch_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 571
    :pswitch_1
    const/4 v4, 0x0

    const/16 v5, 0x199

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v4, v2, v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    .line 574
    const/4 v4, 0x0

    .line 678
    :goto_1
    return v4

    .line 576
    :pswitch_2
    if-lez p9, :cond_2

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 577
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 579
    const/4 v4, 0x1

    goto :goto_1

    .line 583
    :cond_3
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p3

    move-object/from16 v3, p11

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/cache/i;)V

    .line 585
    const/4 v4, 0x0

    goto :goto_1

    .line 587
    :cond_4
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    move-object/from16 v20, v0

    .line 588
    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    .line 589
    move-object/from16 v0, v20

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v4, :cond_7

    move-object/from16 v0, v20

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    iget-object v8, v4, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    .line 591
    :goto_2
    move-object/from16 v0, v20

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v4, :cond_8

    move-object/from16 v0, v20

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v9, v4, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    .line 593
    :goto_3
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    move/from16 v22, v0

    .line 594
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v4, v0, v5, v1}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/aa/a/a/a/d;

    move-result-object v17

    .line 598
    new-instance v7, Lcom/google/android/gms/wallet/service/ow/as;

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    const-string v4, "com.google.android.gms.wallet.EXTRA_ALLOW_ACCOUNT_SELECTION"

    move-object/from16 v0, p7

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v10, p4

    invoke-direct/range {v7 .. v17}, Lcom/google/android/gms/wallet/service/ow/as;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;ZZ[Ljava/lang/String;ZZLcom/google/aa/a/a/a/d;)V

    .line 608
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/af;->e:Lcom/google/android/gms/wallet/service/ow/ar;

    move-object/from16 v0, v21

    invoke-virtual {v4, v0, v7}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;)V

    .line 612
    move-object/from16 v0, p11

    iput-object v8, v0, Lcom/google/android/gms/wallet/cache/i;->d:Ljava/lang/String;

    .line 614
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 615
    move-object/from16 v0, p11

    iput-object v9, v0, Lcom/google/android/gms/wallet/cache/i;->f:Ljava/lang/String;

    .line 617
    :cond_5
    move/from16 v0, v22

    move-object/from16 v1, p11

    iput-boolean v0, v1, Lcom/google/android/gms/wallet/cache/i;->e:Z

    .line 618
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/af;->f:Lcom/google/android/gms/wallet/cache/h;

    move-object/from16 v0, p4

    move-object/from16 v1, p10

    move-object/from16 v2, p11

    invoke-virtual {v4, v6, v0, v1, v2}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;Lcom/google/android/gms/wallet/cache/i;)V

    .line 623
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    .line 625
    invoke-static {}, Lcom/google/aa/b/a/i;->a()[Lcom/google/aa/b/a/i;

    move-result-object v4

    move-object/from16 v0, v20

    iput-object v4, v0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    .line 626
    move-object/from16 v0, v20

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    array-length v8, v4

    .line 627
    if-lez v8, :cond_6

    .line 628
    move-object/from16 v0, p11

    iget-object v4, v0, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 629
    move-object/from16 v0, p11

    iget-object v4, v0, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    const-string v5, "dont_send_loyalty_wob_id"

    invoke-static {v4, v5}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 631
    invoke-static {}, Lcom/google/aa/b/a/h;->a()[Lcom/google/aa/b/a/h;

    move-result-object v4

    move-object/from16 v0, v20

    iput-object v4, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    .line 660
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    move/from16 v0, v22

    move-object/from16 v1, v21

    invoke-static {v4, v0, v1, v5}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 662
    const-string v4, "onlinewallet"

    const-string v5, "preauthorized_masked_wallet"

    move-object/from16 v0, p5

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    move-object/from16 v0, p4

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v4

    .line 666
    const/4 v5, 0x0

    sget-object v6, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    move-object/from16 v0, p6

    invoke-interface {v0, v5, v4, v6}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    .line 667
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/wallet/service/ow/af;->j:Lcom/google/android/gms/wallet/service/ow/w;

    iget-object v13, v7, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    iget-object v14, v7, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    iget-boolean v15, v7, Lcom/google/android/gms/wallet/service/ow/as;->i:Z

    iget-boolean v0, v7, Lcom/google/android/gms/wallet/service/ow/as;->j:Z

    move/from16 v16, v0

    iget-object v4, v7, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-boolean v0, v4, Lcom/google/aa/a/a/a/d;->d:Z

    move/from16 v18, v0

    move-object/from16 v10, p4

    move-object/from16 v11, v19

    move-object/from16 v12, p8

    move-object/from16 v17, p3

    invoke-static/range {v8 .. v18}, Lcom/google/android/gms/wallet/service/ow/j;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/gms/wallet/shared/BuyFlowConfig;Z)Lcom/google/android/gms/wallet/service/ow/j;

    move-result-object v4

    .line 673
    new-instance v5, Lcom/google/android/gms/wallet/service/ow/l;

    iget-object v6, v7, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    iget-object v8, v7, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-boolean v8, v8, Lcom/google/aa/a/a/a/d;->d:Z

    iget-object v7, v7, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v5, v0, v6, v8, v7}, Lcom/google/android/gms/wallet/service/ow/l;-><init>(Lcom/google/aa/b/a/a/a/a/u;Ljava/lang/String;ZLjava/lang/String;)V

    .line 676
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/wallet/service/ow/af;->k:Lcom/google/android/gms/wallet/service/ow/k;

    invoke-virtual {v6, v5, v4}, Lcom/google/android/gms/wallet/service/ow/k;->a(Lcom/google/android/gms/wallet/service/ow/l;Lcom/google/android/gms/wallet/service/ow/j;)V

    .line 677
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-static {v4, v5}, Lcom/google/android/gms/common/util/e;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 678
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 589
    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 591
    :cond_8
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 633
    :cond_9
    const/4 v5, 0x0

    .line 634
    const/4 v4, 0x0

    move/from16 v23, v4

    move-object v4, v5

    move/from16 v5, v23

    :goto_5
    if-ge v5, v8, :cond_a

    .line 635
    move-object/from16 v0, v20

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    aget-object v4, v4, v5

    .line 636
    iget-object v6, v4, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-object v9, v0, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    invoke-static {v6, v9}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 638
    const/4 v6, 0x0

    .line 634
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v4, v6

    goto :goto_5

    .line 644
    :cond_a
    if-eqz v4, :cond_b

    .line 645
    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/aa/b/a/h;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    move-object/from16 v0, v20

    iput-object v5, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    goto/16 :goto_4

    .line 648
    :cond_b
    invoke-static {}, Lcom/google/aa/b/a/h;->a()[Lcom/google/aa/b/a/h;

    move-result-object v4

    move-object/from16 v0, v20

    iput-object v4, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    goto/16 :goto_4

    .line 654
    :cond_c
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p3

    move-object/from16 v3, p11

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/cache/i;)V

    .line 657
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 568
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/google/android/gms/wallet/Cart;ZLjava/lang/StringBuilder;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1352
    if-nez p0, :cond_2

    .line 1353
    if-nez p1, :cond_1

    .line 1354
    const-string v1, "Cart is a required field"

    invoke-static {v1, p2}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1363
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1357
    goto :goto_0

    .line 1360
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Cart.totalPrice"

    invoke-static {v2, v3, p2}, Lcom/google/android/gms/wallet/service/ow/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v2

    .line 1361
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Cart;->c()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Cart.currencyCode"

    invoke-static {v3, v4, p2}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1192
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f()Landroid/os/Bundle;

    move-result-object v1

    .line 1193
    if-eqz v1, :cond_0

    const-string v2, "com.google.android.gms.wallet.CLIENT"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z
    .locals 2

    .prologue
    .line 1369
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/wallet/service/ow/af;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1370
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is invalid. The input was \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\", but should be a string in the regex format \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/service/ow/af;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1373
    const/4 v0, 0x0

    .line 1375
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z
    .locals 1

    .prologue
    .line 1393
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1394
    const-string v0, "googleTransactionId is a required field."

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1395
    const/4 v0, 0x0

    .line 1397
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/service/ow/af;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 1402
    const-string v0, "WalletClient"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1403
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1404
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1406
    :cond_0
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1407
    return-void
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z
    .locals 2

    .prologue
    .line 1382
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is a required field."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 1384
    const/4 v0, 0x0

    .line 1386
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/service/ow/af;)Lcom/google/android/gms/wallet/cache/h;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->f:Lcom/google/android/gms/wallet/cache/h;

    return-object v0
.end method

.method static synthetic c(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/c;->b(Landroid/os/Bundle;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1073
    if-nez p1, :cond_0

    .line 1121
    :goto_0
    return-object v1

    .line 1079
    :cond_0
    const-string v0, "androidPackageName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1080
    const-string v0, "com.google.android.gms.wallet.EXTRA_ENVIRONMENT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1083
    const-string v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 1086
    if-nez v0, :cond_3

    .line 1089
    const-string v0, "com.google.android.gms.wallet.EXTRA_ALLOW_ACCOUNT_SELECTION"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1092
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v0, v4}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1094
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1095
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, v2, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    const-string v1, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1103
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 1106
    if-eqz v0, :cond_1

    .line 1108
    array-length v6, v2

    move v1, v5

    :goto_2
    if-ge v1, v6, :cond_1

    .line 1109
    aget-object v7, v2, v1

    invoke-virtual {v0, v7}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1110
    aget-object v6, v2, v5

    .line 1111
    aput-object v0, v2, v5

    .line 1112
    aput-object v6, v2, v1

    .line 1121
    :cond_1
    :goto_3
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/ah;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/ah;-><init>(Lcom/google/android/gms/wallet/service/ow/af;[Landroid/accounts/Account;ILjava/lang/String;Landroid/os/Bundle;)V

    const-string v1, "check_preauth"

    invoke-static {v6, v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/util/w;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    .line 1108
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1118
    :cond_3
    new-array v2, v2, [Landroid/accounts/Account;

    aput-object v0, v2, v5

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method private e(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1332
    const-string v0, "parameters is required"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1333
    const-string v0, "androidPackageName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1334
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "packageName is required"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1335
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1336
    return-object v1

    .line 1334
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/ow/af;->e(Landroid/os/Bundle;)Ljava/lang/String;

    .line 172
    invoke-static {p1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    .line 173
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 175
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    const/4 v3, 0x2

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->a(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    .line 178
    return-void

    .line 174
    :cond_0
    const-string v0, "noAccount"

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 194
    const-string v0, "callbacks is required"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/ow/af;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 196
    new-instance v2, Lcom/google/android/gms/wallet/common/b;

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/wallet/common/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 197
    const-string v0, "onlinewallet"

    const-string v3, "check_for_pre_auth"

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    const/16 v0, 0x199

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p2, v0, v1, v2}, Lcom/google/android/gms/wallet/c/i;->a(IZLandroid/os/Bundle;)V

    .line 211
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/ow/af;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 209
    :goto_1
    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p2, v1, v0, v2}, Lcom/google/android/gms/wallet/c/i;->a(IZLandroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 207
    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 7

    .prologue
    const/16 v2, 0x194

    const/4 v1, 0x0

    .line 427
    const-string v0, "callbacks is required"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/af;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    .line 429
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "CreateWalletObjects "

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    if-nez p1, :cond_1

    const-string v0, "CreateWalletObjectsRequest was null."

    invoke-static {v0, v5}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    move v0, v1

    :cond_0
    :goto_0
    if-eqz v0, :cond_6

    .line 430
    :goto_1
    if-eqz v1, :cond_7

    .line 431
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v1, v0}, Lcom/google/android/gms/wallet/c/i;->a(ILandroid/os/Bundle;)V

    .line 437
    :goto_2
    return-void

    .line 429
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->b()Lcom/google/android/gms/wallet/LoyaltyWalletObject;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/LoyaltyWalletObject;->f()Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    move-result-object v3

    :cond_2
    :goto_3
    if-nez v3, :cond_4

    const-string v0, "WalletObject is null."

    invoke-static {v0, v5}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->c()Lcom/google/android/gms/wallet/OfferWalletObject;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;->c()Lcom/google/android/gms/wallet/OfferWalletObject;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/OfferWalletObject;->c()Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    move-result-object v3

    goto :goto_3

    :cond_4
    invoke-virtual {v3}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->f()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v0, "issuerName is not defined for WalletObject."

    invoke-static {v0, v5}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    move v0, v1

    :cond_5
    invoke-virtual {v3}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v0, "name is not defined for WalletObject."

    invoke-static {v0, v5}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0, v4, p2}, Lcom/google/android/gms/wallet/service/ow/af;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move v1, v2

    goto :goto_1

    .line 433
    :cond_7
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {p3, v0, v1}, Lcom/google/android/gms/wallet/c/i;->a(ILandroid/os/Bundle;)V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/wallet/FullWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 11

    .prologue
    const/16 v10, 0x19a

    const/16 v2, 0x194

    const/4 v9, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 694
    const-string v3, "callbacks is required"

    invoke-static {p3, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/af;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v6

    .line 697
    new-instance v4, Lcom/google/android/gms/wallet/common/b;

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-direct {v4, v3, v6}, Lcom/google/android/gms/wallet/common/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 698
    const-string v3, "onlinewallet"

    const-string v5, "load_full_wallet"

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v3, "FullWallet"

    invoke-direct {v7, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_1

    const-string v0, "FullWalletRequest was null."

    invoke-static {v0, v7}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    :cond_0
    :goto_0
    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 702
    :goto_2
    if-eqz v1, :cond_5

    .line 703
    invoke-static {p3, p1, v1}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    .line 784
    :goto_3
    return-void

    .line 701
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/google/android/gms/wallet/service/ow/af;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v0

    :goto_4
    if-eqz v3, :cond_10

    iget-object v5, p0, Lcom/google/android/gms/wallet/service/ow/af;->e:Lcom/google/android/gms/wallet/service/ow/ar;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/service/ow/as;

    move-result-object v5

    if-eqz v5, :cond_10

    iget-object v8, v5, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    if-eqz v8, :cond_10

    iget-object v5, v5, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    iget-boolean v5, v5, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    :goto_5
    if-eqz v5, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->d()Lcom/google/android/gms/wallet/Cart;

    move-result-object v0

    if-eqz v0, :cond_f

    const-string v0, "Cart should not be set for billing agreement requests."

    invoke-static {v0, v7}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_4

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->d()Lcom/google/android/gms/wallet/Cart;

    move-result-object v5

    invoke-static {v5, v1, v7}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/Cart;ZLjava/lang/StringBuilder;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0, v6, p2}, Lcom/google/android/gms/wallet/service/ow/af;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move v1, v2

    goto :goto_2

    .line 708
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 709
    const/16 v0, 0x199

    invoke-static {p3, p1, v0}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto :goto_3

    .line 714
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v1

    .line 715
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->e:Lcom/google/android/gms/wallet/service/ow/ar;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/service/ow/as;

    move-result-object v5

    .line 717
    if-eqz v5, :cond_9

    iget-object v0, v5, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    if-eqz v0, :cond_9

    iget-object v0, v5, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-boolean v0, v0, Lcom/google/aa/a/a/a/d;->e:Z

    if-eqz v0, :cond_9

    .line 719
    sget-object v0, Lcom/google/android/gms/wallet/b/c;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 720
    const-string v0, "NetworkOwService"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 721
    const-string v0, "NetworkOwService"

    const-string v2, "Full wallet requested after a previous successful attempt but without a change masked wallet request; Re-routing request to changeMaskedWallet()"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0, p2, p3}, Lcom/google/android/gms/wallet/service/ow/af;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V

    goto/16 :goto_3

    .line 729
    :cond_8
    const-string v0, "NetworkOwService"

    const-string v2, "Must first call change masked wallet before using the same google transaction id for another full wallet request"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    const/4 v5, 0x0

    .line 734
    :cond_9
    if-eqz v5, :cond_a

    iget-object v0, v5, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, v5, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    if-nez v0, :cond_c

    .line 737
    :cond_a
    const-string v0, "NetworkOwService"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 738
    const-string v0, "NetworkOwService"

    const-string v2, "Full wallet requested without buyer account or a selected instrument/address/account"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "GetFullWallet\ninvalid google transaction id: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v10, v0, v6, p2}, Lcom/google/android/gms/wallet/service/ow/af;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 746
    invoke-static {p3, p1, v10}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto/16 :goto_3

    .line 749
    :cond_c
    invoke-static {p2, v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Landroid/os/Bundle;Lcom/google/android/gms/wallet/service/ow/as;)V

    .line 751
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->g:Lcom/google/android/gms/wallet/service/ow/a;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/service/ow/a;->a(Ljava/lang/String;)Lcom/google/aa/b/a/a/a/a/b;

    move-result-object v0

    .line 752
    if-nez v0, :cond_d

    .line 753
    const/16 v0, 0x19d

    invoke-static {p3, p1, v0}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto/16 :goto_3

    .line 757
    :cond_d
    invoke-static {p1, v5}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/FullWalletRequest;Lcom/google/android/gms/wallet/service/ow/as;)Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v3

    .line 759
    iput-object v0, v3, Lcom/google/aa/b/a/a/a/a/i;->a:Lcom/google/aa/b/a/a/a/a/b;

    .line 760
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    iget-object v1, v5, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;-><init>(Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/i;Ljava/lang/String;)V

    .line 763
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/af;->i:Lcom/google/android/gms/wallet/service/ow/m;

    invoke-virtual {v1, v0, p2, v4}, Lcom/google/android/gms/wallet/service/ow/m;->a(Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/common/b;)Landroid/util/Pair;

    move-result-object v0

    .line 766
    :try_start_0
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/i;Lcom/google/android/gms/wallet/common/b;Lcom/google/android/gms/wallet/service/ow/as;)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v0

    .line 769
    const/4 v1, 0x0

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v1, v0, v2}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/wallet/service/ow/ai; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    .line 770
    :catch_0
    move-exception v0

    .line 771
    iget v1, v0, Lcom/google/android/gms/wallet/service/ow/ai;->a:I

    packed-switch v1, :pswitch_data_0

    .line 780
    iget v0, v0, Lcom/google/android/gms/wallet/service/ow/ai;->a:I

    invoke-static {p3, p1, v0}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto/16 :goto_3

    .line 773
    :pswitch_0
    iget-object v1, v0, Lcom/google/android/gms/wallet/service/ow/ai;->b:Landroid/app/PendingIntent;

    if-nez v1, :cond_e

    .line 774
    const/16 v0, 0x8

    invoke-static {p3, p1, v0}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    goto/16 :goto_3

    .line 776
    :cond_e
    iget-object v0, v0, Lcom/google/android/gms/wallet/service/ow/ai;->b:Landroid/app/PendingIntent;

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lcom/google/android/gms/wallet/f;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/f;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/f;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    const/4 v2, 0x6

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/app/PendingIntent;)Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {p3, v2, v1, v0}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_f
    move v0, v3

    goto/16 :goto_1

    :cond_10
    move v5, v1

    goto/16 :goto_5

    .line 771
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/f;)V
    .locals 10

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 884
    invoke-static {p2}, Lcom/google/android/gms/wallet/common/c;->b(Landroid/os/Bundle;)I

    move-result v4

    .line 885
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->o:Lcom/google/android/gms/wallet/service/ow/an;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/service/ow/an;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    .line 886
    if-nez v0, :cond_2

    .line 887
    const-string v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v5, v0

    if-eqz v5, :cond_1

    aget-object v0, v0, v2

    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v5

    if-eqz p1, :cond_3

    invoke-static {p1, v3, v3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/b;)Lcom/google/aa/b/a/a/a/a/u;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_1

    new-instance v6, Lcom/google/aa/b/a/a/a/a/o;

    invoke-direct {v6}, Lcom/google/aa/b/a/a/a/a/o;-><init>()V

    iput-object v3, v6, Lcom/google/aa/b/a/a/a/a/o;->a:Lcom/google/aa/b/a/a/a/a/u;

    iput v1, v6, Lcom/google/aa/b/a/a/a/a/o;->f:I

    new-instance v3, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;

    invoke-direct {v3, v0, v6, v2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;-><init>(Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/o;Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->j:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, v5, v3}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 888
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->o:Lcom/google/android/gms/wallet/service/ow/an;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/service/ow/an;->a(I)Ljava/lang/Boolean;

    move-result-object v0

    .line 891
    :cond_2
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v3, :cond_6

    .line 892
    const/4 v0, 0x1

    .line 896
    :goto_1
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v2, v0, v1}, Lcom/google/android/gms/wallet/c/f;->a(IILandroid/os/Bundle;)V

    .line 897
    return-void

    .line 887
    :cond_3
    const-string v6, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/wallet/service/ow/af;->e:Lcom/google/android/gms/wallet/service/ow/ar;

    invoke-virtual {v7, v6}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/service/ow/as;

    move-result-object v7

    if-eqz v7, :cond_4

    iget-object v8, v7, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    if-nez v8, :cond_5

    :cond_4
    const-string v7, "NetworkOwService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Invalid google transaction id: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " can not check service availability."

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    iget-object v3, v7, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    goto :goto_0

    .line 893
    :cond_6
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne v0, v3, :cond_7

    move v0, v1

    .line 894
    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 20

    .prologue
    .line 260
    const/16 v17, 0x1

    move-object/from16 v14, p3

    move-object/from16 v7, p2

    move-object/from16 v3, p0

    :goto_0
    const-string v2, "callbacks is required"

    invoke-static {v14, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {v3, v7}, Lcom/google/android/gms/wallet/service/ow/af;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v18

    if-lez v17, :cond_1

    invoke-static {v7}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_1
    invoke-static {v4}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v3, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-static {v5, v6, v2, v4}, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->a(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    :cond_0
    iget-object v5, v3, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v5, v2, v4}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    :cond_1
    new-instance v13, Lcom/google/android/gms/wallet/common/b;

    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-direct {v13, v2, v0}, Lcom/google/android/gms/wallet/common/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v2, "onlinewallet"

    const-string v4, "load_masked_wallet"

    invoke-static {v13, v2, v4}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v2, "MaskedWallet"

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_6

    const-string v2, "MaskedWalletRequest was null."

    invoke-static {v2, v4}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    :cond_2
    :goto_2
    const/4 v2, 0x0

    :cond_3
    :goto_3
    if-eqz v2, :cond_b

    const/4 v2, 0x0

    move v4, v2

    :goto_4
    if-eqz v4, :cond_d

    if-nez p1, :cond_c

    const/4 v2, 0x0

    :goto_5
    const/4 v5, 0x0

    invoke-direct {v3, v14, v5, v2, v4}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    .line 261
    :cond_4
    :goto_6
    return-void

    .line 260
    :cond_5
    const-string v2, "noAccount"

    goto :goto_1

    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->h()Ljava/lang/String;

    move-result-object v2

    const-string v5, "MaskedWalletRequest.currencyCode"

    invoke-static {v2, v5, v4}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->l()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v5

    if-eqz v5, :cond_7

    const-string v2, "Cart should not be set for billing agreement requests."

    invoke-static {v2, v4}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    const/4 v2, 0x0

    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v2, "Estimated total price should not be set for billing agreement requests."

    invoke-static {v2, v4}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    goto :goto_2

    :cond_8
    const/4 v2, 0x0

    goto :goto_7

    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MaskedWalletRequest.estimatedTotalPrice"

    invoke-static {v5, v6, v4}, Lcom/google/android/gms/wallet/service/ow/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v5

    if-eqz v5, :cond_a

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v5, v6, v4}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/Cart;ZLjava/lang/StringBuilder;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :cond_a
    const/4 v2, 0x0

    goto :goto_8

    :cond_b
    const/16 v2, 0x194

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-direct {v3, v2, v4, v0, v7}, Lcom/google/android/gms/wallet/service/ow/af;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const/16 v2, 0x194

    move v4, v2

    goto :goto_4

    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    :cond_d
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/wallet/common/aa;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;)Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object p1

    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x199

    invoke-direct {v3, v14, v2, v4, v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_6

    :cond_e
    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_f

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x7

    invoke-direct {v3, v14, v2, v4, v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_6

    :cond_f
    invoke-direct {v3, v7}, Lcom/google/android/gms/wallet/service/ow/af;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    if-lez v17, :cond_10

    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    iget-object v4, v3, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    :cond_10
    invoke-static {v7}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v5

    const-string v2, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {v7, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/accounts/Account;

    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/af;->f:Lcom/google/android/gms/wallet/cache/h;

    move-object/from16 v0, v18

    invoke-virtual {v2, v5, v4, v0}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/i;

    move-result-object v19

    sget-object v2, Lcom/google/android/gms/wallet/b/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_11

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_11

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->l()Z

    move-result v2

    if-eqz v2, :cond_12

    :cond_11
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v3, v14, v11, v0, v1}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Lcom/google/android/gms/wallet/cache/i;)V

    goto/16 :goto_6

    :cond_12
    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/af;->g:Lcom/google/android/gms/wallet/service/ow/a;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/service/ow/a;->a(Ljava/lang/String;)Lcom/google/aa/b/a/a/a/a/b;

    move-result-object v2

    if-nez v2, :cond_13

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x19d

    invoke-direct {v3, v14, v2, v4, v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_6

    :cond_13
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v6, v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/b;)Lcom/google/aa/b/a/a/a/a/u;

    move-result-object v2

    new-instance v6, Lcom/google/aa/b/a/a/a/a/m;

    invoke-direct {v6}, Lcom/google/aa/b/a/a/a/a/m;-><init>()V

    move-object/from16 v0, v19

    iget-boolean v9, v0, Lcom/google/android/gms/wallet/cache/i;->g:Z

    if-eqz v9, :cond_15

    move-object/from16 v0, v19

    iget-object v9, v0, Lcom/google/android/gms/wallet/cache/i;->d:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_15

    move-object/from16 v0, v19

    iget-object v9, v0, Lcom/google/android/gms/wallet/cache/i;->d:Ljava/lang/String;

    iput-object v9, v6, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    iget-boolean v9, v2, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    if-eqz v9, :cond_14

    move-object/from16 v0, v19

    iget-object v9, v0, Lcom/google/android/gms/wallet/cache/i;->f:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_14

    move-object/from16 v0, v19

    iget-object v9, v0, Lcom/google/android/gms/wallet/cache/i;->f:Ljava/lang/String;

    iput-object v9, v6, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    :cond_14
    move-object/from16 v0, v19

    iget-boolean v9, v0, Lcom/google/android/gms/wallet/cache/i;->e:Z

    iput-boolean v9, v6, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    :cond_15
    iput-object v2, v6, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-static {v7}, Lcom/google/android/gms/wallet/common/c;->b(Landroid/os/Bundle;)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(I)Z

    move-result v2

    iput-boolean v2, v6, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    iput-object v8, v6, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/ao;->a()Lcom/google/aa/b/a/a/a/a/t;

    move-result-object v2

    iput-object v2, v6, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    iget-object v2, v6, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    const/4 v8, 0x2

    invoke-static {v2, v8}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v2

    iput-object v2, v6, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/af;->m:Lcom/google/android/gms/wallet/common/ab;

    iget-object v8, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v2, v8, v0}, Lcom/google/android/gms/wallet/common/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_16

    iput-object v2, v6, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    :cond_16
    iget-object v8, v3, Lcom/google/android/gms/wallet/service/ow/af;->d:Lcom/google/android/gms/wallet/service/p;

    new-instance v2, Lcom/google/android/gms/wallet/service/ow/ag;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/wallet/service/ow/ag;-><init>(Lcom/google/android/gms/wallet/service/ow/af;Landroid/accounts/Account;ILcom/google/aa/b/a/a/a/a/m;Landroid/os/Bundle;)V

    invoke-virtual {v8, v2}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    const-string v4, "NetworkOwService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unexpected ServerResponse type: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x8

    invoke-direct {v3, v14, v2, v4, v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_6

    :sswitch_0
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v3, v14, v11, v0, v1}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Lcom/google/android/gms/wallet/cache/i;)V

    goto/16 :goto_6

    :sswitch_1
    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x19b

    invoke-direct {v3, v14, v2, v4, v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_6

    :sswitch_2
    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x7

    invoke-direct {v3, v14, v2, v4, v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_6

    :sswitch_3
    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/checkout/b/a/c;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/b/a/c;Z)Z

    move-result v4

    if-eqz v4, :cond_17

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v3, v14, v11, v0, v1}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Lcom/google/android/gms/wallet/cache/i;)V

    goto/16 :goto_6

    :cond_17
    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/b/a/c;)I

    move-result v2

    invoke-direct {v3, v14, v4, v5, v2}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_6

    :sswitch_4
    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v9

    check-cast v9, Lcom/google/aa/b/a/a/a/a/n;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j()Lcom/google/android/gms/wallet/Cart;

    move-result-object v16

    move-object v8, v3

    move-object v10, v6

    move-object v12, v4

    move-object v15, v7

    invoke-direct/range {v8 .. v19}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/aa/b/a/a/a/a/n;Lcom/google/aa/b/a/a/a/a/m;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/common/b;Lcom/google/android/gms/wallet/c/i;Landroid/os/Bundle;Lcom/google/android/gms/wallet/Cart;ILjava/lang/String;Lcom/google/android/gms/wallet/cache/i;)Z

    move-result v2

    if-eqz v2, :cond_4

    add-int/lit8 v17, v17, -0x1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0xd -> :sswitch_4
        0x11 -> :sswitch_3
        0x16 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/16 v3, 0x194

    .line 827
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/af;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    .line 828
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NotifyTransactionStatus"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 830
    if-nez p1, :cond_0

    .line 831
    const-string v1, "NotifyTransactionStatusRequest was null."

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/service/ow/af;->b(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 832
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0, v2, p2}, Lcom/google/android/gms/wallet/service/ow/af;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 879
    :goto_0
    return-void

    .line 834
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/service/ow/af;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 836
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0, v2, p2}, Lcom/google/android/gms/wallet/service/ow/af;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 839
    :cond_1
    const-string v1, "UNKNOWN"

    .line 840
    const-string v0, ""

    .line 841
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;->b()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 870
    :pswitch_0
    const-string v0, "UNKNOWN"

    .line 874
    :goto_1
    new-instance v3, Lcom/google/android/gms/wallet/common/b;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-direct {v3, v4, v2}, Lcom/google/android/gms/wallet/common/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 876
    const-string v2, "onlinewallet"

    const-string v4, "notify_transaction_status"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object v0, v5, v1

    invoke-static {v3, v2, v4, v5}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 843
    :pswitch_1
    const-string v1, "SUCCESS"

    goto :goto_1

    .line 846
    :pswitch_2
    const-string v1, "FAILURE"

    .line 847
    const-string v0, "AVS_DECLINE"

    goto :goto_1

    .line 850
    :pswitch_3
    const-string v1, "FAILURE"

    .line 851
    const-string v0, "BAD_CARD"

    goto :goto_1

    .line 854
    :pswitch_4
    const-string v1, "FAILURE"

    .line 855
    const-string v0, "BAD_CVC"

    goto :goto_1

    .line 858
    :pswitch_5
    const-string v1, "FAILURE"

    .line 859
    const-string v0, "DECLINED"

    goto :goto_1

    .line 862
    :pswitch_6
    const-string v1, "FAILURE"

    .line 863
    const-string v0, "FRAUD_DECLINE"

    goto :goto_1

    .line 866
    :pswitch_7
    const-string v1, "FAILURE"

    .line 867
    const-string v0, "OTHER"

    goto :goto_1

    .line 841
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_2
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 7

    .prologue
    .line 943
    const-string v0, "callbacks is required"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 944
    const-string v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/accounts/Account;

    .line 945
    const-string v0, "account is required"

    invoke-static {v3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 947
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/ow/af;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 949
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 952
    new-instance v5, Lcom/google/android/gms/wallet/service/ow/aj;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/firstparty/GetInstrumentsRequest;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/af;->h:Lcom/google/android/gms/wallet/cache/j;

    invoke-direct {v5, p3, v0, v1}, Lcom/google/android/gms/wallet/service/ow/aj;-><init>(Lcom/google/android/gms/wallet/c/i;[ILcom/google/android/gms/wallet/cache/j;)V

    .line 956
    new-instance v0, Lcom/google/android/gms/wallet/cache/c;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/gms/wallet/common/c;->b(Landroid/os/Bundle;)I

    move-result v4

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/wallet/cache/c;-><init>(Landroid/content/Context;Lcom/android/volley/s;Landroid/accounts/Account;ILcom/google/android/gms/wallet/cache/e;Landroid/os/Looper;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/cache/c;->run()V

    .line 960
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/wallet/c/i;)V
    .locals 6

    .prologue
    const/16 v5, 0x19a

    const/16 v4, 0x194

    .line 216
    const-string v0, "callbacks is required"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    invoke-direct {p0, p3}, Lcom/google/android/gms/wallet/service/ow/af;->e(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 218
    new-instance v1, Lcom/google/android/gms/wallet/common/b;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/common/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 219
    const-string v2, "onlinewallet"

    const-string v3, "change_masked_wallet"

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ChangeMaskedWallet"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 223
    invoke-static {p1, v1}, Lcom/google/android/gms/wallet/service/ow/af;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 224
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v4, v1, v0, p3}, Lcom/google/android/gms/wallet/service/ow/af;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 226
    invoke-direct {p0, p4, p1, p2, v4}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    .line 255
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 233
    const/16 v0, 0x199

    invoke-direct {p0, p4, p1, p2, v0}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 238
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->e:Lcom/google/android/gms/wallet/service/ow/ar;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/service/ow/as;

    move-result-object v4

    .line 240
    if-eqz v4, :cond_2

    iget-object v2, v4, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    if-nez v2, :cond_3

    .line 241
    :cond_2
    const-string v2, "\ninvalid transaction id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v5, v1, v0, p3}, Lcom/google/android/gms/wallet/service/ow/af;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 244
    invoke-direct {p0, p4, p1, p2, v5}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/c/i;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 248
    :cond_3
    invoke-static {p3, v4}, Lcom/google/android/gms/wallet/service/ow/af;->a(Landroid/os/Bundle;Lcom/google/android/gms/wallet/service/ow/as;)V

    .line 250
    invoke-static {p3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/af;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    invoke-static {p1, v1, v0, v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    :goto_1
    const/4 v1, 0x6

    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/service/ow/af;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v2

    invoke-interface {p4, v1, v2, v0}, Lcom/google/android/gms/wallet/c/i;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/gms/wallet/service/ow/af;->p:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/ow/af;->e(Landroid/os/Bundle;)Ljava/lang/String;

    .line 184
    invoke-static {p1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    .line 185
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 187
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/af;->b:Landroid/content/Context;

    const-string v3, "com.google.android.gms.wallet.fragment.BUTTON"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v2, v1, v0, v3}, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;I)Ljava/lang/String;

    .line 189
    return-void

    .line 186
    :cond_0
    const-string v0, "noAccount"

    goto :goto_0
.end method
