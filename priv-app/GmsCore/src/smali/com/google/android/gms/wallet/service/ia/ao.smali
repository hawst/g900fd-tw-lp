.class final Lcom/google/android/gms/wallet/service/ia/ao;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ia/ad;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/ao;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ia/ao;->c:Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ia/ao;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 11

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ao;->c:Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;->b()Lcom/google/checkout/inapp/proto/ab;

    move-result-object v5

    .line 184
    iget-object v0, v5, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    if-eqz v0, :cond_2

    iget-object v0, v5, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    iget-object v0, v0, Lcom/google/checkout/a/a/b/b;->c:Lcom/google/checkout/a/a/a/d;

    if-eqz v0, :cond_2

    .line 186
    iget-object v0, v5, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    iget-object v0, v0, Lcom/google/checkout/a/a/b/b;->c:Lcom/google/checkout/a/a/a/d;

    .line 187
    iget v1, v0, Lcom/google/checkout/a/a/a/d;->a:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 188
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Create profile currently only supports credit cards."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_0
    iget-object v0, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v0, v0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    .line 194
    invoke-static {v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v5

    check-cast v5, Lcom/google/checkout/inapp/proto/ab;

    .line 195
    iget-object v1, v5, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    iget-object v1, v1, Lcom/google/checkout/a/a/b/b;->c:Lcom/google/checkout/a/a/a/d;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ia/ao;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/service/ia/ad;)Lcom/google/android/gms/wallet/service/ia/o;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ia/ao;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ia/ao;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v2}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ia/ao;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, Lcom/google/checkout/a/a/a/c;->a:Ljava/lang/String;

    iget-object v7, v0, Lcom/google/checkout/a/a/a/c;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ao;->a:Landroid/accounts/Account;

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ao;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v9

    iget-object v0, v5, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    if-eqz v0, :cond_1

    iget-object v0, v5, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    iget-object v0, v0, Lcom/google/checkout/a/a/b/b;->c:Lcom/google/checkout/a/a/a/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/o;->a(Lcom/google/checkout/a/a/a/d;)V

    :cond_1
    iget-object v10, v1, Lcom/google/android/gms/wallet/service/ia/o;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/ab;

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/wallet/service/ia/ab;-><init>(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/checkout/inapp/proto/ab;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "create_profile"

    invoke-static {v10, v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/util/w;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 201
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ao;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/service/ia/ad;)Lcom/google/android/gms/wallet/service/ia/o;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ao;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ao;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ao;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ao;->a:Landroid/accounts/Account;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ao;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lcom/google/android/gms/wallet/service/ia/o;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/ac;

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/wallet/service/ia/ac;-><init>(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/checkout/inapp/proto/ab;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "create_profile"

    invoke-static {v8, v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/util/w;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0
.end method
