.class public abstract Lcom/google/android/gms/drive/database/model/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Lcom/google/android/gms/drive/database/model/ae;

.field protected final e:Lcom/google/android/gms/drive/database/i;

.field public f:J


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ad;->e:Lcom/google/android/gms/drive/database/i;

    .line 32
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ae;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ad;->b:Lcom/google/android/gms/drive/database/model/ae;

    .line 33
    iput-object p3, p0, Lcom/google/android/gms/drive/database/model/ad;->a:Landroid/net/Uri;

    .line 34
    return-void
.end method

.method private a()Landroid/content/ContentValues;
    .locals 7

    .prologue
    .line 51
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 53
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/drive/database/model/ad;->a(Landroid/content/ContentValues;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    return-object v1

    .line 54
    :catch_0
    move-exception v2

    .line 55
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ad;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 64
    :goto_0
    const-string v3, "DatabaseRow"

    const-string v4, "Error in fillContentValues()"

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const-string v3, "DatabaseRow"

    const-string v4, "Error in fillContentValues() on %s; partial result: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v1, v5, v0

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 67
    throw v2

    .line 58
    :catch_1
    move-exception v0

    .line 59
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[additional RuntimeException thrown by toString(): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Landroid/content/ContentValues;)V
.end method

.method public final d(J)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 42
    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 43
    iput-wide p1, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    .line 44
    return-void

    .line 42
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 7

    .prologue
    .line 76
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ad;->e:Lcom/google/android/gms/drive/database/i;

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ad;->b:Lcom/google/android/gms/drive/database/model/ae;

    invoke-direct {p0}, Lcom/google/android/gms/drive/database/model/ad;->a()Landroid/content/ContentValues;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/drive/database/model/ad;->a:Landroid/net/Uri;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/drive/database/i;->b(JLcom/google/android/gms/drive/database/model/ae;Landroid/content/ContentValues;Landroid/net/Uri;)J

    move-result-wide v0

    .line 78
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 79
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error saving "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;->d(J)V

    .line 82
    return-void
.end method

.method public final j()V
    .locals 5

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ad;->e:Lcom/google/android/gms/drive/database/i;

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ad;->b:Lcom/google/android/gms/drive/database/model/ae;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/google/android/gms/drive/database/i;->a(JLcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)I

    .line 90
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;->d(J)V

    .line 91
    return-void

    .line 88
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 4

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 114
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "DatabaseRow[%s, sqlId=%s, values=%s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/ad;->b:Lcom/google/android/gms/drive/database/model/ae;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-direct {p0}, Lcom/google/android/gms/drive/database/model/ad;->a()Landroid/content/ContentValues;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
