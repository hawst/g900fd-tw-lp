.class public final Lcom/google/android/gms/people/d/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[Lcom/google/android/gms/people/d/e;

.field public c:Lcom/google/android/gms/people/d/i;

.field public d:Lcom/google/android/gms/people/d/h;

.field public e:Lcom/google/android/gms/people/d/d;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1168
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1169
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/people/d/b;->a:I

    invoke-static {}, Lcom/google/android/gms/people/d/e;->a()[Lcom/google/android/gms/people/d/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    iput-object v1, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    iput-object v1, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    iput-object v1, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/b;->cachedSize:I

    .line 1170
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 1271
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1272
    iget v1, p0, Lcom/google/android/gms/people/d/b;->a:I

    if-eqz v1, :cond_0

    .line 1273
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/people/d/b;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1276
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 1277
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1278
    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    aget-object v2, v2, v0

    .line 1279
    if-eqz v2, :cond_1

    .line 1280
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1277
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1285
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    if-eqz v1, :cond_4

    .line 1286
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1289
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    if-eqz v1, :cond_5

    .line 1290
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1293
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    if-eqz v1, :cond_6

    .line 1294
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1297
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1184
    if-ne p1, p0, :cond_1

    .line 1225
    :cond_0
    :goto_0
    return v0

    .line 1187
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 1188
    goto :goto_0

    .line 1190
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/b;

    .line 1191
    iget v2, p0, Lcom/google/android/gms/people/d/b;->a:I

    iget v3, p1, Lcom/google/android/gms/people/d/b;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1192
    goto :goto_0

    .line 1194
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    iget-object v3, p1, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1196
    goto :goto_0

    .line 1198
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    if-nez v2, :cond_5

    .line 1199
    iget-object v2, p1, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1200
    goto :goto_0

    .line 1203
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    iget-object v3, p1, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/i;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1204
    goto :goto_0

    .line 1207
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    if-nez v2, :cond_7

    .line 1208
    iget-object v2, p1, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    if-eqz v2, :cond_8

    move v0, v1

    .line 1209
    goto :goto_0

    .line 1212
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    iget-object v3, p1, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1213
    goto :goto_0

    .line 1216
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    if-nez v2, :cond_9

    .line 1217
    iget-object v2, p1, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1218
    goto :goto_0

    .line 1221
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    iget-object v3, p1, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1222
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1230
    iget v0, p0, Lcom/google/android/gms/people/d/b;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1232
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1234
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1236
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1238
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1240
    return v0

    .line 1234
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/i;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1236
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/h;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1238
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    invoke-virtual {v1}, Lcom/google/android/gms/people/d/d;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 233
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/people/d/b;->a:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/people/d/e;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/people/d/e;

    invoke-direct {v3}, Lcom/google/android/gms/people/d/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/people/d/e;

    invoke-direct {v3}, Lcom/google/android/gms/people/d/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/people/d/i;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/people/d/h;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/gms/people/d/d;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1246
    iget v0, p0, Lcom/google/android/gms/people/d/b;->a:I

    if-eqz v0, :cond_0

    .line 1247
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/people/d/b;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1249
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1250
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1251
    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    aget-object v1, v1, v0

    .line 1252
    if-eqz v1, :cond_1

    .line 1253
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1250
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1257
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    if-eqz v0, :cond_3

    .line 1258
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1260
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    if-eqz v0, :cond_4

    .line 1261
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1263
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    if-eqz v0, :cond_5

    .line 1264
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1266
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1267
    return-void
.end method
