.class public final Lcom/google/android/gms/notifications/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;

.field public static final g:Lcom/google/android/gms/common/a/d;

.field public static final h:Lcom/google/android/gms/common/a/d;

.field public static final i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;

.field public static final k:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    const-string v0, "gnots:social_application_id"

    const/16 v1, 0xd0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->a:Lcom/google/android/gms/common/a/d;

    .line 33
    const-string v0, "gnots:oauth2_scope"

    const-string v1, "https://www.googleapis.com/auth/plus.me"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->b:Lcom/google/android/gms/common/a/d;

    .line 39
    const-string v0, "gnots:gnots_set_read_states_path"

    const-string v1, "/gmsgnotssetreadstates?alt=proto"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->c:Lcom/google/android/gms/common/a/d;

    .line 46
    const-string v0, "gnots:gnots_fetch_by_identifier_path"

    const-string v1, "/gmsgnotsfetchbyidentifier?alt=proto"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->d:Lcom/google/android/gms/common/a/d;

    .line 54
    const-string v0, "gnots:account_notifier_app"

    const-string v1, "account_notifier"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->e:Lcom/google/android/gms/common/a/d;

    .line 60
    const-string v0, "gnots:icon_type:security_threat"

    const-string v1, "security_threat"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->f:Lcom/google/android/gms/common/a/d;

    .line 66
    const-string v0, "gnots:icon_type:security_notification"

    const-string v1, "security_notification"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->g:Lcom/google/android/gms/common/a/d;

    .line 72
    const-string v0, "gnots:icon_type:login_event"

    const-string v1, "login_event"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->h:Lcom/google/android/gms/common/a/d;

    .line 78
    const-string v0, "gnots:include_default_allowed_domains"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->i:Lcom/google/android/gms/common/a/d;

    .line 86
    const-string v0, "gnots:additional_allowed_domains"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->j:Lcom/google/android/gms/common/a/d;

    .line 93
    const-string v0, "gnots:excluded_allowed_domains"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/notifications/a/a;->k:Lcom/google/android/gms/common/a/d;

    return-void
.end method

.method public static final a(Ljava/lang/String;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 100
    const-string v0, "|"

    invoke-static {v0}, Lcom/google/k/a/at;->a(Ljava/lang/String;)Lcom/google/k/a/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/a/at;->a()Lcom/google/k/a/at;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/k/a/at;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ljava/util/List;
    .locals 3

    .prologue
    .line 107
    const/16 v0, 0x14b

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "security.google.com"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "support.google.com"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "www.google.com"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "accounts.google.com"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "accounts.google.net"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "accounts.google.org"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "accounts.google.biz"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accounts.google.info"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accounts.google.mobi"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "accounts.google.name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "accounts.google.pro"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "accounts.google.us"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "accounts.google.kids.us"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "accounts.google.ag"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "accounts.google.ba"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "accounts.google.ca"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "accounts.google.co.cr"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "accounts.google.co.uk"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "accounts.g.cn"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "accounts.google.cn"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "accounts.google.com.cn"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "accounts.guge.com"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "accounts.guge.com.cn"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "accounts.guge.cn"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "accounts.googel.cn"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "accounts.xn--flW351E.cn"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "accounts.google.al"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "accounts.google.org.uk"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "accounts.google.cl"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "accounts.google.de"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "accounts.google.ec"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "accounts.google.ee"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "accounts.google.fi"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "accounts.google.fr"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "accounts.google.gd"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "accounts.google.ge"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "accounts.google.gy"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "accounts.google.com.gy"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "accounts.google.ie"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "accounts.google.in"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "accounts.google.net.in"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "accounts.google.org.in"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "accounts.google.it"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "accounts.google.co.nz"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "accounts.google.org.nz"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "accounts.google.net.nz"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "accounts.google.co.at"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "accounts.google.com.af"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "accounts.google.org.af"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "accounts.google.at"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "accounts.google.com.au"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "accounts.google.com.br"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "accounts.google.com.gt"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "accounts.google.com.mx"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "accounts.google.mx"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "accounts.google.co.ma"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "accounts.google.jobs"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "accounts.google.cz"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "accounts.google.hu"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "accounts.google.co.hu"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "accounts.google.nl"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "accounts.google.is"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "accounts.google.dk"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "accounts.google.pf"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "accounts.google.com.pg"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "accounts.google.pl"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "accounts.google.com.pl"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "accounts.google.com.ph"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "accounts.google.com.pr"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "accounts.google.ro"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "accounts.google.se"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "accounts.google.ch"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "accounts.google.gm"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "accounts.xn--2e0b0k.kr"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "accounts.google.co.kr"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "accounts.google.kr"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "accounts.google.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "accounts.google.co.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "accounts.google.ne.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "accounts.google.co.je"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "accounts.google.com.ar"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "accounts.google.am"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "accounts.google.be"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "accounts.google.fm"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "accounts.google.tv"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "accounts.google.vg"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "accounts.google.bi"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "accounts.google.cc"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "accounts.google.cd"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "accounts.google.co.il"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "accounts.google.kz"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "accounts.google.la"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "accounts.google.lv"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "accounts.google.lt"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "accounts.google.md"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "accounts.google.mw"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "accounts.google.mr"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "accounts.google.ms"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "accounts.google.nr"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "accounts.google.nu"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "accounts.google.ps"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "accounts.google.com.ps"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "accounts.google.com.nf"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "accounts.google.com.pa"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "accounts.google.com.py"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "accounts.google.ru"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "accounts.google.com.ru"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "accounts.xn--c1aay4a.xn--p1ai"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "accounts.google.rw"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "accounts.google.sh"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "accounts.google.com.ec"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "accounts.google.co.gg"

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "accounts.google.li"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "accounts.google.com.ua"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "accounts.google.ua"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "accounts.google.co.ua"

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "accounts.google.as"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "accounts.google.ws"

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "accounts.google.sg"

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "accounts.google.co.th"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "accounts.google.ae"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "accounts.google.gr"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "accounts.google.com.gr"

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "accounts.google.no"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "accounts.google.com.pt"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "accounts.google.pt"

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "accounts.google.com.sg"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "accounts.google.io"

    aput-object v2, v0, v1

    const/16 v1, 0x80

    const-string v2, "accounts.google.td"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, "accounts.google.tw"

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "accounts.google.com.hk"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "accounts.google.hk"

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "accounts.google.com.pe"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, "accounts.google.com.tw"

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "accounts.google.je"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, "accounts.google.com.tr"

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "accounts.google.ac"

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "accounts.google.ad"

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "accounts.google.it.ao"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, "accounts.google.aw"

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "accounts.google.az"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, "accounts.google.bf"

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "accounts.google.bg"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, "accounts.google.bj"

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "accounts.google.bm"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "accounts.google.bn"

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "accounts.google.bo"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, "accounts.google.bs"

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "accounts.google.bt"

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "accounts.google.by"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, "accounts.google.bz"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "accounts.google.cat"

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "accounts.google.cf"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "accounts.google.cg"

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "accounts.google.ci"

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "accounts.google.cm"

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "accounts.google.co.ao"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, "accounts.google.co.ba"

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "accounts.google.co.bi"

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "accounts.google.co.bw"

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "accounts.google.co.ci"

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "accounts.google.co.ck"

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "accounts.google.co.gl"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, "accounts.google.co.gy"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "accounts.google.co.id"

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "accounts.google.co.im"

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "accounts.google.co.in"

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "accounts.google.co.it"

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "accounts.google.co.ke"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, "accounts.google.co.ls"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, "accounts.google.co.mu"

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "accounts.google.co.mw"

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, "accounts.google.co.mz"

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, "accounts.google.co.pn"

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "accounts.google.co.rs"

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "accounts.google.co.tt"

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "accounts.google.co.tz"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, "accounts.google.co.ug"

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "accounts.google.co.uz"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, "accounts.google.co.ve"

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "accounts.google.co.vi"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, "accounts.google.co.za"

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "accounts.google.co.zm"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, "accounts.google.co.zw"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, "accounts.google.com.ag"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, "accounts.google.com.ai"

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "accounts.google.com.az"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, "accounts.google.com.bd"

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "accounts.google.com.bh"

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "accounts.google.com.bi"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    const-string v2, "accounts.google.com.bn"

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "accounts.google.com.bo"

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "accounts.google.com.bs"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, "accounts.google.com.by"

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "accounts.google.com.bz"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, "accounts.google.com.cy"

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "accounts.google.com.dz"

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "accounts.google.com.eg"

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, "accounts.google.com.er"

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, "accounts.google.com.et"

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "accounts.google.com.ge"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, "accounts.google.com.gh"

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string v2, "accounts.google.com.gi"

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string v2, "accounts.google.com.gl"

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string v2, "accounts.google.com.gp"

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string v2, "accounts.google.com.hn"

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string v2, "accounts.google.com.hr"

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string v2, "accounts.google.com.ht"

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string v2, "accounts.google.com.iq"

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string v2, "accounts.google.com.jm"

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string v2, "accounts.google.com.jo"

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, "accounts.google.com.kg"

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, "accounts.google.com.kh"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, "accounts.google.com.ki"

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, "accounts.google.com.kw"

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, "accounts.google.com.kz"

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, "accounts.google.com.lb"

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    const-string v2, "accounts.google.com.lc"

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, "accounts.google.com.lk"

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, "accounts.google.com.lv"

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "accounts.google.com.ly"

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, "accounts.google.com.mk"

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, "accounts.google.com.mm"

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    const-string v2, "accounts.google.com.mt"

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, "accounts.google.com.mu"

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, "accounts.google.com.mw"

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, "accounts.google.com.my"

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, "accounts.google.com.na"

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, "accounts.google.com.nc"

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, "accounts.google.com.ng"

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    const-string v2, "accounts.google.com.ni"

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, "accounts.google.com.np"

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, "accounts.google.com.nr"

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, "accounts.google.com.om"

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, "accounts.google.com.pk"

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, "accounts.google.com.qa"

    aput-object v2, v0, v1

    const/16 v1, 0xec

    const-string v2, "accounts.google.com.sa"

    aput-object v2, v0, v1

    const/16 v1, 0xed

    const-string v2, "accounts.google.com.sb"

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, "accounts.google.com.sc"

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, "accounts.google.com.sl"

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, "accounts.google.com.sv"

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, "accounts.google.com.tj"

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, "accounts.google.com.tm"

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    const-string v2, "accounts.google.com.tn"

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    const-string v2, "accounts.google.com.tt"

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, "accounts.google.com.uy"

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, "accounts.google.com.uz"

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, "accounts.google.com.vc"

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, "accounts.google.com.vi"

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    const-string v2, "accounts.google.com.vn"

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, "accounts.google.com.ws"

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, "accounts.google.com.cu"

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, "accounts.google.cv"

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    const-string v2, "accounts.google.cx"

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    const-string v2, "accounts.google.dj"

    aput-object v2, v0, v1

    const/16 v1, 0xff

    const-string v2, "accounts.google.dm"

    aput-object v2, v0, v1

    const/16 v1, 0x100

    const-string v2, "accounts.google.do"

    aput-object v2, v0, v1

    const/16 v1, 0x101

    const-string v2, "accounts.google.dz"

    aput-object v2, v0, v1

    const/16 v1, 0x102

    const-string v2, "accounts.google.es"

    aput-object v2, v0, v1

    const/16 v1, 0x103

    const-string v2, "accounts.google.eu"

    aput-object v2, v0, v1

    const/16 v1, 0x104

    const-string v2, "accounts.google.nom.es"

    aput-object v2, v0, v1

    const/16 v1, 0x105

    const-string v2, "accounts.google.org.es"

    aput-object v2, v0, v1

    const/16 v1, 0x106

    const-string v2, "accounts.google.ga"

    aput-object v2, v0, v1

    const/16 v1, 0x107

    const-string v2, "accounts.google.gf"

    aput-object v2, v0, v1

    const/16 v1, 0x108

    const-string v2, "accounts.google.gg"

    aput-object v2, v0, v1

    const/16 v1, 0x109

    const-string v2, "accounts.google.gl"

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    const-string v2, "accounts.google.gp"

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    const-string v2, "accounts.google.gw"

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    const-string v2, "accounts.google.hn"

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    const-string v2, "accounts.google.hr"

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    const-string v2, "accounts.google.ht"

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    const-string v2, "accounts.google.im"

    aput-object v2, v0, v1

    const/16 v1, 0x110

    const-string v2, "accounts.google.in.rs"

    aput-object v2, v0, v1

    const/16 v1, 0x111

    const-string v2, "accounts.google.iq"

    aput-object v2, v0, v1

    const/16 v1, 0x112

    const-string v2, "accounts.google.jo"

    aput-object v2, v0, v1

    const/16 v1, 0x113

    const-string v2, "accounts.google.kg"

    aput-object v2, v0, v1

    const/16 v1, 0x114

    const-string v2, "accounts.google.ki"

    aput-object v2, v0, v1

    const/16 v1, 0x115

    const-string v2, "accounts.google.km"

    aput-object v2, v0, v1

    const/16 v1, 0x116

    const-string v2, "accounts.google.kn"

    aput-object v2, v0, v1

    const/16 v1, 0x117

    const-string v2, "accounts.google.lk"

    aput-object v2, v0, v1

    const/16 v1, 0x118

    const-string v2, "accounts.google.lu"

    aput-object v2, v0, v1

    const/16 v1, 0x119

    const-string v2, "accounts.google.ma"

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    const-string v2, "accounts.google.me"

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    const-string v2, "accounts.google.mg"

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    const-string v2, "accounts.google.mh"

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    const-string v2, "accounts.google.mk"

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    const-string v2, "accounts.google.ml"

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    const-string v2, "accounts.google.mn"

    aput-object v2, v0, v1

    const/16 v1, 0x120

    const-string v2, "accounts.google.mu"

    aput-object v2, v0, v1

    const/16 v1, 0x121

    const-string v2, "accounts.google.mv"

    aput-object v2, v0, v1

    const/16 v1, 0x122

    const-string v2, "accounts.google.ne"

    aput-object v2, v0, v1

    const/16 v1, 0x123

    const-string v2, "accounts.google.nf"

    aput-object v2, v0, v1

    const/16 v1, 0x124

    const-string v2, "accounts.google.ng"

    aput-object v2, v0, v1

    const/16 v1, 0x125

    const-string v2, "accounts.google.off.ai"

    aput-object v2, v0, v1

    const/16 v1, 0x126

    const-string v2, "accounts.google.ph"

    aput-object v2, v0, v1

    const/16 v1, 0x127

    const-string v2, "accounts.google.pk"

    aput-object v2, v0, v1

    const/16 v1, 0x128

    const-string v2, "accounts.google.pn"

    aput-object v2, v0, v1

    const/16 v1, 0x129

    const-string v2, "accounts.google.pr"

    aput-object v2, v0, v1

    const/16 v1, 0x12a

    const-string v2, "accounts.google.qa"

    aput-object v2, v0, v1

    const/16 v1, 0x12b

    const-string v2, "accounts.google.re"

    aput-object v2, v0, v1

    const/16 v1, 0x12c

    const-string v2, "accounts.google.rs"

    aput-object v2, v0, v1

    const/16 v1, 0x12d

    const-string v2, "accounts.google.sc"

    aput-object v2, v0, v1

    const/16 v1, 0x12e

    const-string v2, "accounts.google.si"

    aput-object v2, v0, v1

    const/16 v1, 0x12f

    const-string v2, "accounts.google.sk"

    aput-object v2, v0, v1

    const/16 v1, 0x130

    const-string v2, "accounts.google.sl"

    aput-object v2, v0, v1

    const/16 v1, 0x131

    const-string v2, "accounts.google.sm"

    aput-object v2, v0, v1

    const/16 v1, 0x132

    const-string v2, "accounts.google.sn"

    aput-object v2, v0, v1

    const/16 v1, 0x133

    const-string v2, "accounts.google.so"

    aput-object v2, v0, v1

    const/16 v1, 0x134

    const-string v2, "accounts.google.sr"

    aput-object v2, v0, v1

    const/16 v1, 0x135

    const-string v2, "accounts.google.st"

    aput-object v2, v0, v1

    const/16 v1, 0x136

    const-string v2, "accounts.google.sz"

    aput-object v2, v0, v1

    const/16 v1, 0x137

    const-string v2, "accounts.google.tk"

    aput-object v2, v0, v1

    const/16 v1, 0x138

    const-string v2, "accounts.google.tg"

    aput-object v2, v0, v1

    const/16 v1, 0x139

    const-string v2, "accounts.google.tm"

    aput-object v2, v0, v1

    const/16 v1, 0x13a

    const-string v2, "accounts.google.tn"

    aput-object v2, v0, v1

    const/16 v1, 0x13b

    const-string v2, "accounts.google.to"

    aput-object v2, v0, v1

    const/16 v1, 0x13c

    const-string v2, "accounts.google.tt"

    aput-object v2, v0, v1

    const/16 v1, 0x13d

    const-string v2, "accounts.google.ug"

    aput-object v2, v0, v1

    const/16 v1, 0x13e

    const-string v2, "accounts.google.uz"

    aput-object v2, v0, v1

    const/16 v1, 0x13f

    const-string v2, "accounts.google.vc"

    aput-object v2, v0, v1

    const/16 v1, 0x140

    const-string v2, "accounts.google.vn"

    aput-object v2, v0, v1

    const/16 v1, 0x141

    const-string v2, "accounts.google.vu"

    aput-object v2, v0, v1

    const/16 v1, 0x142

    const-string v2, "accounts.google.yt"

    aput-object v2, v0, v1

    const/16 v1, 0x143

    const-string v2, "accounts.google.af"

    aput-object v2, v0, v1

    const/16 v1, 0x144

    const-string v2, "accounts.google.com.ve"

    aput-object v2, v0, v1

    const/16 v1, 0x145

    const-string v2, "accounts.google.tel"

    aput-object v2, v0, v1

    const/16 v1, 0x146

    const-string v2, "accounts.google.tp"

    aput-object v2, v0, v1

    const/16 v1, 0x147

    const-string v2, "accounts.google.tl"

    aput-object v2, v0, v1

    const/16 v1, 0x148

    const-string v2, "accounts.google.com.do"

    aput-object v2, v0, v1

    const/16 v1, 0x149

    const-string v2, "accounts.google.com.co"

    aput-object v2, v0, v1

    const/16 v1, 0x14a

    const-string v2, "accounts.google.com.fj"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
