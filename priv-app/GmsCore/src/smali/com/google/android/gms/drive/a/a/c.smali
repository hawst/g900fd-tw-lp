.class public final Lcom/google/android/gms/drive/a/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/d/c;
.implements Lcom/google/android/gms/drive/g/d;


# instance fields
.field private final a:Lcom/google/android/gms/drive/g/n;

.field private final b:Lcom/google/android/gms/drive/database/r;

.field private final c:Lcom/google/android/gms/drive/a/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->e()Lcom/google/android/gms/drive/g/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/c;->a:Lcom/google/android/gms/drive/g/n;

    .line 24
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/c;->b:Lcom/google/android/gms/drive/database/r;

    .line 25
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->l()Lcom/google/android/gms/drive/a/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/c;->c:Lcom/google/android/gms/drive/a/a/a;

    .line 26
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/c;->a:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a/c;->c()V

    .line 33
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a/c;->c()V

    .line 39
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/c;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/c;->c:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/a;->a()Lcom/google/android/gms/drive/a/a/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/m;->c()V

    .line 45
    :cond_0
    return-void
.end method
