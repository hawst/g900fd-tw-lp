.class public Lcom/google/android/gms/wallet/common/ui/TopBarView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/widget/TextView;

.field c:I

.field d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/content/Context;)V

    .line 58
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 61
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 62
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setOrientation(I)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->bZ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 64
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setPadding(IIII)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->aI:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setBackgroundColor(I)V

    .line 70
    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 71
    sget v1, Lcom/google/android/gms/l;->hF:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 73
    sget v0, Lcom/google/android/gms/j;->bS:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a:Landroid/view/View;

    .line 74
    sget v0, Lcom/google/android/gms/j;->bT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b:Landroid/widget/TextView;

    .line 75
    sget v0, Lcom/google/android/gms/j;->lu:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    .line 76
    return-void

    .line 67
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setOrientation(I)V

    goto :goto_0
.end method

.method private b()Lcom/google/android/gms/wallet/common/ui/AccountSelector;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->d:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    .line 178
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->c:I

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 119
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 101
    if-eqz p1, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 104
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 106
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->c:I

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b()Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    .line 131
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/cq;)V
    .locals 2

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b()Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_1

    .line 144
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a([Landroid/accounts/Account;)[Lcom/google/android/gms/wallet/common/ui/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a([Lcom/google/android/gms/wallet/common/ui/a;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Lcom/google/android/gms/wallet/common/ui/cq;)V

    .line 146
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b()Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->setEnabled(Z)V

    .line 161
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 89
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 90
    check-cast p1, Landroid/os/Bundle;

    .line 91
    const-string v0, "instanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 92
    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    .line 97
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 80
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 81
    const-string v1, "instanceState"

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 82
    const-string v1, "titleId"

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    return-object v0
.end method
