.class public final Lcom/google/android/gms/plus/e/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/plus/e/e;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/android/gms/plus/e/e;
    .locals 2

    .prologue
    .line 50
    const-class v1, Lcom/google/android/gms/plus/e/e;

    monitor-enter v1

    .line 51
    :try_start_0
    sget-object v0, Lcom/google/android/gms/plus/e/e;->a:Lcom/google/android/gms/plus/e/e;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/google/android/gms/plus/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/plus/e/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/e/e;->a:Lcom/google/android/gms/plus/e/e;

    .line 54
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/e/e;->a:Lcom/google/android/gms/plus/e/e;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/HashMap;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    .prologue
    .line 64
    if-eqz p2, :cond_0

    const-string v0, "application_name"

    invoke-virtual {p2, v0}, Lcom/google/android/gms/common/server/ClientContext;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    :goto_0
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/plus/e/e;->a(Landroid/content/Context;Ljava/util/HashMap;Ljava/lang/String;)V

    .line 68
    return-void

    .line 64
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/16 v4, 0x3b

    .line 88
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    sget-object p2, Lcom/google/android/gms/common/analytics/a;->a:Ljava/lang/String;

    .line 91
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "3"

    .line 93
    :goto_0
    const-string v1, "com.google.android.pano.v1"

    invoke-static {p0, v1}, Lcom/google/android/gms/common/util/e;->g(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 94
    const-string v0, "9"

    move-object v1, v0

    .line 96
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    const-string v0, "X-Api-Client"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 98
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 100
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 102
    :cond_1
    const-string v0, "device="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 104
    const-string v0, "platform=2"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 106
    const-string v0, "application="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const-string v0, "X-Api-Client"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    return-void

    .line 91
    :cond_2
    const-string v0, "2"

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method
