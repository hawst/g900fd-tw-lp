.class public final Lcom/google/android/gms/lockbox/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/lockbox/m;


# instance fields
.field private final a:Landroid/app/ActivityManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/e;->a:Landroid/app/ActivityManager;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(J)Ljava/util/List;
    .locals 5

    .prologue
    .line 40
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/e;->a:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 43
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 44
    new-instance v3, Lcom/google/android/gms/lockbox/a/b;

    invoke-direct {v3}, Lcom/google/android/gms/lockbox/a/b;-><init>()V

    iget-object v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    iput v4, v3, Lcom/google/android/gms/lockbox/a/b;->h:I

    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    iput v4, v3, Lcom/google/android/gms/lockbox/a/b;->j:I

    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    iput v4, v3, Lcom/google/android/gms/lockbox/a/b;->b:I

    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonCode:I

    iput v4, v3, Lcom/google/android/gms/lockbox/a/b;->c:I

    iget-object v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonComponent:Landroid/content/ComponentName;

    if-eqz v4, :cond_0

    iget-object v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonComponent:Landroid/content/ComponentName;

    invoke-static {v4}, Lcom/google/android/gms/lockbox/a/f;->a(Landroid/content/ComponentName;)Lcom/google/android/gms/lockbox/a/c;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    :cond_0
    iget v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonPid:I

    iput v4, v3, Lcom/google/android/gms/lockbox/a/b;->e:I

    iget-object v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v0, v0

    invoke-static {v4, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    :cond_1
    iput-wide p1, v3, Lcom/google/android/gms/lockbox/a/b;->k:J

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/android/gms/lockbox/a/b;->m:Z

    .line 45
    invoke-static {v3}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 47
    :cond_2
    return-object v1
.end method
