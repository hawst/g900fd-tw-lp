.class public final Lcom/google/android/gms/cast/media/o;
.super Landroid/support/v7/c/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/internal/o;
.implements Lcom/google/android/gms/cast/media/af;


# instance fields
.field private A:I

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Z

.field private E:Lcom/google/android/gms/cast/media/al;

.field final a:Lcom/google/android/gms/cast/CastDevice;

.field b:Ljava/lang/String;

.field c:Lcom/google/android/gms/cast/media/x;

.field d:Lcom/google/android/gms/cast/media/x;

.field e:Lcom/google/android/gms/cast/media/x;

.field f:Lcom/google/android/gms/cast/media/x;

.field g:Lcom/google/android/gms/cast/media/x;

.field h:Lcom/google/android/gms/cast/media/ae;

.field i:Lcom/google/android/gms/cast/internal/l;

.field j:J

.field k:Z

.field l:Z

.field m:Z

.field n:Lcom/google/android/gms/cast_mirroring/JGCastService;

.field final o:Lcom/google/android/gms/cast/e/h;

.field final p:Ljava/util/List;

.field q:Lcom/google/android/gms/cast/media/w;

.field final synthetic r:Lcom/google/android/gms/cast/media/a;

.field private s:Lcom/google/android/gms/cast/b/e;

.field private t:D

.field private u:Ljava/lang/String;

.field private v:Lcom/google/android/gms/cast/LaunchOptions;

.field private w:Z

.field private x:Lcom/google/android/gms/cast/media/x;

.field private y:Lcom/google/android/gms/cast/media/x;

.field private z:Landroid/app/PendingIntent;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1189
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    invoke-direct {p0}, Landroid/support/v7/c/j;-><init>()V

    .line 1131
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->u:Ljava/lang/String;

    .line 1155
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastRouteController"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    .line 1190
    const-string v0, "instance-%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/gms/cast/media/a;->d()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->C:Ljava/lang/String;

    .line 1195
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;)V

    .line 1196
    iput-object p2, p0, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    .line 1197
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/cast/media/o;->t:D

    .line 1198
    iput v3, p0, Lcom/google/android/gms/cast/media/o;->A:I

    .line 1199
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->p:Ljava/util/List;

    .line 1200
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/cast/media/o;->j:J

    .line 1201
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/CastDevice;B)V
    .locals 0

    .prologue
    .line 1123
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/cast/media/o;-><init>(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/CastDevice;)V

    return-void
.end method

.method static a(Lorg/json/JSONObject;)Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2400
    if-nez p0, :cond_0

    .line 2427
    :goto_0
    return-object v1

    .line 2406
    :cond_0
    const-string v0, "httpStatus"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2408
    :try_start_0
    const-string v0, "httpStatus"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 2409
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2412
    :try_start_1
    const-string v1, "android.media.status.extra.HTTP_STATUS_CODE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2415
    :goto_1
    const-string v1, "httpHeaders"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2417
    :try_start_2
    const-string v1, "httpHeaders"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/cast/media/ak;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v2

    .line 2419
    if-nez v0, :cond_1

    .line 2420
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    move-object v0, v1

    .line 2422
    :cond_1
    const-string v1, "android.media.status.extra.HTTP_RESPONSE_HEADERS"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :goto_2
    move-object v1, v0

    .line 2427
    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 1712
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->h()J

    move-result-wide v0

    .line 1713
    const-wide/16 v2, 0x2

    or-long/2addr v2, v0

    .line 1714
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 1716
    const-string v0, "com.google.android.gms.cast.EXTRA_CAST_STOP_APPLICATION_WHEN_SESSION_ENDS"

    iget-boolean v4, p0, Lcom/google/android/gms/cast/media/o;->w:Z

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/media/o;->w:Z

    .line 1719
    const-string v0, "com.google.android.gms.cast.EXTRA_CAST_RELAUNCH_APPLICATION"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 1723
    const/4 v0, 0x0

    .line 1724
    const-string v5, "com.google.android.gms.cast.EXTRA_CAST_LANGUAGE_CODE"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1726
    if-eqz v5, :cond_0

    .line 1727
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 1730
    :cond_0
    new-instance v5, Lcom/google/android/gms/cast/e;

    invoke-direct {v5}, Lcom/google/android/gms/cast/e;-><init>()V

    invoke-virtual {v5, v4}, Lcom/google/android/gms/cast/e;->a(Z)Lcom/google/android/gms/cast/e;

    move-result-object v4

    if-eqz v0, :cond_1

    :goto_0
    iget-object v5, v4, Lcom/google/android/gms/cast/e;->a:Lcom/google/android/gms/cast/LaunchOptions;

    invoke-static {v0}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/cast/LaunchOptions;->a(Ljava/lang/String;)V

    iget-object v0, v4, Lcom/google/android/gms/cast/e;->a:Lcom/google/android/gms/cast/LaunchOptions;

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->v:Lcom/google/android/gms/cast/LaunchOptions;

    .line 1735
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v4, "launch options: %s"

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/gms/cast/media/o;->v:Lcom/google/android/gms/cast/LaunchOptions;

    aput-object v7, v5, v6

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1737
    const-string v0, "com.google.android.gms.cast.EXTRA_DEBUG_LOGGING_ENABLED"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1738
    const-string v0, "com.google.android.gms.cast.EXTRA_DEBUG_LOGGING_ENABLED"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 1740
    if-eqz v4, :cond_2

    .line 1741
    const-wide/16 v0, 0x1

    or-long/2addr v0, v2

    .line 1745
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/cast/e/h;->a(Z)V

    .line 1748
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/cast/b/e;->a(J)V

    .line 1749
    return-void

    .line 1730
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    goto :goto_0

    .line 1743
    :cond_2
    const-wide/16 v0, -0x2

    and-long/2addr v0, v2

    goto :goto_1

    :cond_3
    move-wide v0, v2

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/cast/media/x;)Z
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1453
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "processRemotePlaybackRequest()"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1455
    iget-object v9, p1, Lcom/google/android/gms/cast/media/x;->a:Landroid/content/Intent;

    .line 1456
    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    .line 1459
    const-string v0, "com.google.android.gms.cast.EXTRA_CUSTOM_DATA"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1461
    if-eqz v0, :cond_2e

    .line 1462
    invoke-static {v0, v1}, Lcom/google/android/gms/cast/media/ak;->a(Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1465
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "got remote playback request; action=%s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v10, v4, v8

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1468
    :try_start_0
    const-string v2, "android.media.intent.action.PLAY"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-virtual {v9}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_18

    .line 1470
    const-string v2, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v9, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1471
    const-string v2, "com.google.android.gms.cast.EXTRA_CAST_APPLICATION_ID"

    invoke-virtual {v9, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1473
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1474
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->c()Ljava/lang/String;

    move-result-object v2

    .line 1476
    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/cast/media/o;->u:Ljava/lang/String;

    .line 1479
    :cond_1
    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v7

    .line 1708
    :goto_1
    return v0

    .line 1483
    :cond_2
    invoke-virtual {v9}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 1484
    if-nez v4, :cond_3

    move v0, v8

    .line 1485
    goto :goto_1

    .line 1487
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Device received play request, uri %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    invoke-virtual {v2, v3, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1489
    const-string v2, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {v9, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    if-nez v5, :cond_5

    .line 1492
    :cond_4
    :goto_2
    new-instance v2, Lcom/google/android/gms/cast/h;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/cast/h;-><init>(Ljava/lang/String;)V

    iget-object v3, v2, Lcom/google/android/gms/cast/h;->a:Lcom/google/android/gms/cast/g;

    const/4 v4, 0x1

    iput v4, v3, Lcom/google/android/gms/cast/g;->b:I

    invoke-virtual {v9}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/gms/cast/h;->a:Lcom/google/android/gms/cast/g;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_12

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "content type cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1706
    :catch_0
    move-exception v0

    .line 1704
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "can\'t process command; %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v8

    .line 1705
    goto :goto_1

    .line 1489
    :cond_5
    :try_start_1
    const-string v2, "android.media.metadata.ALBUM_TITLE"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v2, "android.media.metadata.ALBUM_ARTIST"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v2, "android.media.metadata.COMPOSER"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v2, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v2, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v3, v2

    :goto_3
    const-string v2, "android.media.metadata.TRACK_NUMBER"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string v1, "android.media.metadata.TRACK_NUMBER"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v2, v1

    :goto_4
    if-nez v6, :cond_6

    if-nez v3, :cond_6

    if-eqz v2, :cond_11

    :cond_6
    new-instance v1, Lcom/google/android/gms/cast/i;

    const/4 v13, 0x3

    invoke-direct {v1, v13}, Lcom/google/android/gms/cast/i;-><init>(I)V

    if-eqz v6, :cond_7

    const-string v13, "com.google.android.gms.cast.metadata.ALBUM_TITLE"

    invoke-virtual {v1, v13, v6}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    if-eqz v11, :cond_8

    const-string v6, "com.google.android.gms.cast.metadata.ALBUM_ARTIST"

    invoke-virtual {v1, v6, v11}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    if-eqz v12, :cond_9

    const-string v6, "com.google.android.gms.cast.metadata.COMPOSER"

    invoke-virtual {v1, v6, v12}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    if-eqz v3, :cond_a

    const-string v6, "com.google.android.gms.cast.metadata.DISC_NUMBER"

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v6, v3}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;I)V

    :cond_a
    if-eqz v2, :cond_b

    const-string v3, "com.google.android.gms.cast.metadata.TRACK_NUMBER"

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;I)V

    :cond_b
    :goto_5
    const-string v2, "android.media.metadata.TITLE"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_c

    const-string v3, "com.google.android.gms.cast.metadata.TITLE"

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    const-string v2, "android.media.metadata.ARTIST"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_d

    const-string v3, "com.google.android.gms.cast.metadata.ARTIST"

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    const-string v2, "android.media.metadata.YEAR"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "android.media.metadata.YEAR"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    const/4 v6, 0x1

    invoke-virtual {v3, v6, v2}, Ljava/util/Calendar;->set(II)V

    const-string v2, "com.google.android.gms.cast.metadata.RELEASE_DATE"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;Ljava/util/Calendar;)V

    :cond_e
    const-string v2, "android.media.metadata.ARTWORK_URI"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "android.media.metadata.ARTWORK_URI"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v3, Lcom/google/android/gms/common/images/WebImage;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/google/android/gms/common/images/WebImage;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/cast/i;->a(Lcom/google/android/gms/common/images/WebImage;)V

    goto/16 :goto_2

    :cond_f
    move-object v3, v1

    goto/16 :goto_3

    :cond_10
    move-object v2, v1

    goto/16 :goto_4

    :cond_11
    new-instance v1, Lcom/google/android/gms/cast/i;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/cast/i;-><init>(I)V

    goto :goto_5

    .line 1492
    :cond_12
    iput-object v3, v4, Lcom/google/android/gms/cast/g;->c:Ljava/lang/String;

    iget-object v3, v2, Lcom/google/android/gms/cast/h;->a:Lcom/google/android/gms/cast/g;

    iput-object v1, v3, Lcom/google/android/gms/cast/g;->d:Lcom/google/android/gms/cast/i;

    iget-object v1, v2, Lcom/google/android/gms/cast/h;->a:Lcom/google/android/gms/cast/g;

    iget-object v3, v1, Lcom/google/android/gms/cast/g;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_13

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "content ID cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    iget-object v3, v1, Lcom/google/android/gms/cast/g;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_14

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "content type cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    iget v1, v1, Lcom/google/android/gms/cast/g;->b:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_15

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "a valid stream type must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    iget-object v3, v2, Lcom/google/android/gms/cast/h;->a:Lcom/google/android/gms/cast/g;

    .line 1499
    const-string v1, "android.media.intent.extra.HTTP_HEADERS"

    invoke-virtual {v9, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 1501
    if-eqz v1, :cond_2d

    .line 1503
    const/4 v2, 0x0

    :try_start_2
    invoke-static {v1, v2}, Lcom/google/android/gms/cast/media/ak;->a(Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;

    move-result-object v2

    .line 1505
    if-nez v0, :cond_16

    .line 1506
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    move-object v0, v1

    .line 1508
    :cond_16
    const-string v1, "httpHeaders"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v6, v0

    .line 1512
    :goto_6
    :try_start_3
    const-string v0, "android.media.intent.extra.ITEM_POSITION"

    const-wide/16 v4, 0x0

    invoke-virtual {v9, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 1514
    const-string v0, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1518
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    move-object v2, p0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/cast/internal/l;->a(Lcom/google/android/gms/cast/internal/o;Lcom/google/android/gms/cast/g;JLorg/json/JSONObject;)J

    move-result-wide v2

    .line 1521
    new-instance v1, Lcom/google/android/gms/cast/media/w;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/gms/cast/media/w;-><init>(Lcom/google/android/gms/cast/media/o;J)V

    .line 1522
    iput-object v0, v1, Lcom/google/android/gms/cast/media/w;->d:Landroid/app/PendingIntent;

    .line 1523
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1530
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/google/android/gms/cast/media/o;->j:J

    .line 1532
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v4, "loading media with item id assigned as %s, request ID %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v9, v1, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    aput-object v9, v5, v6

    const/4 v6, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1535
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1537
    const-string v2, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/o;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1538
    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/gms/cast/media/o;->d(I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1541
    const-string v2, "android.media.intent.extra.ITEM_ID"

    iget-object v1, v1, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1543
    new-instance v1, Landroid/support/v7/c/b;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Landroid/support/v7/c/b;-><init>(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/c/b;->a(J)Landroid/support/v7/c/b;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/c/b;->a()Landroid/support/v7/c/a;

    move-result-object v1

    .line 1547
    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    iget-object v1, v1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1550
    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_17
    :goto_7
    move v0, v7

    .line 1708
    goto/16 :goto_1

    :catch_1
    move-exception v1

    move-object v6, v0

    goto :goto_6

    .line 1551
    :catch_2
    move-exception v0

    .line 1552
    :try_start_5
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception while processing %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1553
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(I)V

    goto :goto_7

    .line 1556
    :cond_18
    const-string v1, "android.media.intent.action.PAUSE"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 1557
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_0

    move-result v1

    if-nez v1, :cond_19

    move v0, v7

    .line 1558
    goto/16 :goto_1

    .line 1562
    :cond_19
    :try_start_6
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/gms/cast/internal/l;->a(Lcom/google/android/gms/cast/internal/o;Lorg/json/JSONObject;)J

    move-result-wide v0

    .line 1563
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->e:Lcom/google/android/gms/cast/media/x;

    .line 1564
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->e:Lcom/google/android/gms/cast/media/x;

    iput-wide v0, v2, Lcom/google/android/gms/cast/media/x;->c:J
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_7

    .line 1565
    :catch_3
    move-exception v0

    .line 1566
    :try_start_7
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception while processing %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1567
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(I)V

    goto :goto_7

    .line 1569
    :cond_1a
    const-string v1, "android.media.intent.action.RESUME"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 1570
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_0

    move-result v1

    if-nez v1, :cond_1b

    move v0, v7

    .line 1571
    goto/16 :goto_1

    .line 1575
    :cond_1b
    :try_start_8
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/gms/cast/internal/l;->c(Lcom/google/android/gms/cast/internal/o;Lorg/json/JSONObject;)J

    move-result-wide v0

    .line 1577
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->f:Lcom/google/android/gms/cast/media/x;

    .line 1578
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->f:Lcom/google/android/gms/cast/media/x;

    iput-wide v0, v2, Lcom/google/android/gms/cast/media/x;->c:J
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_7

    .line 1579
    :catch_4
    move-exception v0

    .line 1580
    :try_start_9
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception while processing %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1581
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(I)V

    goto :goto_7

    .line 1583
    :cond_1c
    const-string v1, "android.media.intent.action.STOP"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 1584
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z
    :try_end_9
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_9} :catch_0

    move-result v1

    if-nez v1, :cond_1d

    move v0, v7

    .line 1585
    goto/16 :goto_1

    .line 1589
    :cond_1d
    :try_start_a
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/gms/cast/internal/l;->b(Lcom/google/android/gms/cast/internal/o;Lorg/json/JSONObject;)J

    move-result-wide v0

    .line 1591
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->g:Lcom/google/android/gms/cast/media/x;

    .line 1592
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->g:Lcom/google/android/gms/cast/media/x;

    iput-wide v0, v2, Lcom/google/android/gms/cast/media/x;->c:J
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_a} :catch_0

    goto/16 :goto_7

    .line 1593
    :catch_5
    move-exception v0

    .line 1594
    :try_start_b
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception while processing %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1595
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(I)V

    goto/16 :goto_7

    .line 1597
    :cond_1e
    const-string v1, "android.media.intent.action.SEEK"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 1598
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z

    move-result v1

    if-nez v1, :cond_1f

    move v0, v7

    .line 1599
    goto/16 :goto_1

    .line 1601
    :cond_1f
    const-string v1, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {v9, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/media/o;->e(Ljava/lang/String;)V

    .line 1603
    const-string v1, "android.media.intent.extra.ITEM_POSITION"

    const-wide/16 v2, 0x0

    invoke-virtual {v9, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J
    :try_end_b
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_b} :catch_0

    move-result-wide v2

    .line 1606
    :try_start_c
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v4, "seeking to %d ms"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v5, v6

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1607
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v1, p0, v2, v3, v0}, Lcom/google/android/gms/cast/internal/l;->a(Lcom/google/android/gms/cast/internal/o;JLorg/json/JSONObject;)J

    move-result-wide v0

    .line 1610
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->d:Lcom/google/android/gms/cast/media/x;

    .line 1611
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->d:Lcom/google/android/gms/cast/media/x;

    iput-wide v0, v2, Lcom/google/android/gms/cast/media/x;->c:J
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_0

    goto/16 :goto_7

    .line 1612
    :catch_6
    move-exception v0

    .line 1613
    :try_start_d
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception while processing %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1614
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(I)V

    goto/16 :goto_7

    .line 1616
    :cond_20
    const-string v0, "android.media.intent.action.GET_STATUS"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1617
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z

    move-result v0

    if-nez v0, :cond_21

    move v0, v7

    .line 1618
    goto/16 :goto_1

    .line 1621
    :cond_21
    const-string v0, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/media/o;->e(Ljava/lang/String;)V

    .line 1623
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    if-eqz v0, :cond_22

    .line 1624
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1625
    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/o;->g()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1627
    const-string v1, "android.media.intent.extra.SESSION_STATUS"

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/gms/cast/media/o;->d(I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1629
    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V

    goto/16 :goto_7

    .line 1631
    :cond_22
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(I)V

    goto/16 :goto_7

    .line 1633
    :cond_23
    const-string v0, "com.google.android.gms.cast.ACTION_SYNC_STATUS"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1634
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z

    move-result v0

    if-nez v0, :cond_24

    move v0, v7

    .line 1635
    goto/16 :goto_1

    .line 1638
    :cond_24
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_0

    if-eqz v0, :cond_26

    .line 1641
    :try_start_e
    iget-wide v0, p0, Lcom/google/android/gms/cast/media/o;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_25

    .line 1643
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/cast/internal/l;->a(Lcom/google/android/gms/cast/internal/o;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/media/o;->j:J

    .line 1645
    :cond_25
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7
    .catch Ljava/lang/IllegalStateException; {:try_start_e .. :try_end_e} :catch_0

    goto/16 :goto_7

    .line 1646
    :catch_7
    move-exception v0

    .line 1647
    const/4 v1, 0x0

    :try_start_f
    iput-object v1, p0, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;

    .line 1648
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception while processing %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1649
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(I)V

    goto/16 :goto_7

    .line 1653
    :cond_26
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(I)V

    goto/16 :goto_7

    .line 1655
    :cond_27
    const-string v0, "android.media.intent.action.START_SESSION"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 1656
    const-string v0, "com.google.android.gms.cast.EXTRA_CAST_APPLICATION_ID"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1658
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 1659
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->c()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1662
    :goto_8
    const-string v0, "android.media.intent.extra.SESSION_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 1664
    if-nez v0, :cond_28

    .line 1665
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "No status update receiver supplied to %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v10, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v8

    .line 1666
    goto/16 :goto_1

    .line 1669
    :cond_28
    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->z:Landroid/app/PendingIntent;

    .line 1670
    iput-object v1, p0, Lcom/google/android/gms/cast/media/o;->u:Ljava/lang/String;

    .line 1671
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->y:Lcom/google/android/gms/cast/media/x;

    .line 1672
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z

    move-result v0

    if-nez v0, :cond_17

    move v0, v7

    .line 1673
    goto/16 :goto_1

    .line 1675
    :cond_29
    const-string v0, "android.media.intent.action.GET_SESSION_STATUS"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 1676
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z

    .line 1678
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1679
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/cast/media/o;->d(I)Landroid/os/Bundle;

    move-result-object v1

    .line 1681
    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1683
    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V

    goto/16 :goto_7

    .line 1684
    :cond_2a
    const-string v0, "android.media.intent.action.END_SESSION"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1685
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;I)Z

    .line 1687
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/o;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/cast/media/o;->b(Ljava/lang/String;I)V

    .line 1691
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->z:Landroid/app/PendingIntent;

    .line 1693
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/o;->l()V

    .line 1695
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1696
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/cast/media/o;->d(I)Landroid/os/Bundle;

    move-result-object v1

    .line 1697
    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1699
    invoke-virtual {p1, v0}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_f .. :try_end_f} :catch_0

    goto/16 :goto_7

    :cond_2b
    move v0, v8

    .line 1701
    goto/16 :goto_1

    :cond_2c
    move-object v1, v0

    goto :goto_8

    :cond_2d
    move-object v6, v0

    goto/16 :goto_6

    :cond_2e
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/gms/cast/media/x;I)Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1383
    iget-object v2, p1, Lcom/google/android/gms/cast/media/x;->a:Landroid/content/Intent;

    const-string v3, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1384
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/o;->f()Ljava/lang/String;

    move-result-object v3

    .line 1386
    iget-object v4, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v5, "checkSession() sessionId=%s, currentSessionId=%s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v1

    aput-object v3, v6, v0

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1388
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1389
    iget-boolean v2, p0, Lcom/google/android/gms/cast/media/o;->k:Z

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 1390
    iput-boolean v1, p0, Lcom/google/android/gms/cast/media/o;->k:Z

    .line 1435
    :goto_0
    return v0

    .line 1396
    :cond_0
    if-ne p2, v0, :cond_5

    .line 1398
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->x:Lcom/google/android/gms/cast/media/x;

    .line 1399
    iput-boolean v0, p0, Lcom/google/android/gms/cast/media/o;->k:Z

    .line 1401
    iget-object v0, p1, Lcom/google/android/gms/cast/media/x;->a:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/media/o;->a(Landroid/content/Intent;)V

    .line 1403
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1405
    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/media/o;->e(I)V

    :goto_1
    move v0, v1

    .line 1411
    goto :goto_0

    .line 1408
    :cond_1
    iput v7, p0, Lcom/google/android/gms/cast/media/o;->A:I

    .line 1409
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->B:Ljava/lang/String;

    goto :goto_1

    .line 1413
    :cond_2
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1416
    iput-boolean v1, p0, Lcom/google/android/gms/cast/media/o;->k:Z

    goto :goto_0

    .line 1418
    :cond_3
    if-nez v3, :cond_5

    .line 1421
    iget-object v0, p1, Lcom/google/android/gms/cast/media/x;->a:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/media/o;->a(Landroid/content/Intent;)V

    .line 1423
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->x:Lcom/google/android/gms/cast/media/x;

    .line 1424
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1425
    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/media/o;->d(Ljava/lang/String;)V

    :goto_2
    move v0, v1

    .line 1430
    goto :goto_0

    .line 1427
    :cond_4
    iput v7, p0, Lcom/google/android/gms/cast/media/o;->A:I

    .line 1428
    iput-object v2, p0, Lcom/google/android/gms/cast/media/o;->B:Ljava/lang/String;

    goto :goto_2

    .line 1434
    :cond_5
    invoke-virtual {p1, v7}, Lcom/google/android/gms/cast/media/x;->a(I)V

    move v0, v1

    .line 1435
    goto :goto_0
.end method

.method private b(Ljava/lang/String;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2030
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->z:Landroid/app/PendingIntent;

    if-nez v0, :cond_1

    .line 2045
    :cond_0
    :goto_0
    return-void

    .line 2034
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2035
    const-string v1, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2036
    invoke-virtual {p0, p2}, Lcom/google/android/gms/cast/media/o;->d(I)Landroid/os/Bundle;

    move-result-object v1

    .line 2037
    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2040
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Invoking session status PendingIntent with: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2041
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->z:Landroid/app/PendingIntent;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-object v2, v2, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2042
    :catch_0
    move-exception v0

    .line 2043
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception while sending PendingIntent"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "resumeSession()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1317
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    if-nez v0, :cond_0

    .line 1319
    new-instance v0, Lcom/google/android/gms/cast/media/ae;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-object v3, v2, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/e/h;->b()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/cast/media/o;->C:Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/cast/media/ae;-><init>(Lcom/google/android/gms/cast/b/e;Lcom/google/android/gms/cast/media/af;Landroid/os/Handler;ZLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    .line 1322
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/cast/media/ae;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    return-void
.end method

.method private e(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1299
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "startSession()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1301
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    if-nez v0, :cond_0

    .line 1302
    new-instance v0, Lcom/google/android/gms/cast/media/ae;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-object v3, v2, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/e/h;->b()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/cast/media/o;->C:Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/cast/media/ae;-><init>(Lcom/google/android/gms/cast/b/e;Lcom/google/android/gms/cast/media/af;Landroid/os/Handler;ZLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    .line 1306
    :cond_0
    if-ne p1, v6, :cond_1

    .line 1307
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    invoke-static {}, Lcom/google/android/gms/cast/media/a;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/cast/e;

    invoke-direct {v2}, Lcom/google/android/gms/cast/e;-><init>()V

    invoke-virtual {v2, v6}, Lcom/google/android/gms/cast/e;->a(Z)Lcom/google/android/gms/cast/e;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/cast/e;->a:Lcom/google/android/gms/cast/LaunchOptions;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/media/ae;->a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    .line 1312
    :goto_0
    return-void

    .line 1310
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->u:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->v:Lcom/google/android/gms/cast/LaunchOptions;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/media/ae;->a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1365
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    if-nez v0, :cond_0

    .line 1366
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no current item"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1368
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1369
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "item ID does not match current item"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1371
    :cond_1
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0, p0}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/media/o;)Lcom/google/android/gms/cast/b/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    .line 1291
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1292
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/o;->h()V

    .line 1296
    :cond_0
    :goto_0
    return-void

    .line 1293
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1294
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->c()V

    goto :goto_0
.end method

.method private l()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1331
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "endSession()"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1333
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    if-eqz v0, :cond_0

    .line 1334
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "mStopApplicationWhenSessionEnds: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/google/android/gms/cast/media/o;->w:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1335
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    iget-boolean v1, p0, Lcom/google/android/gms/cast/media/o;->m:Z

    iget-boolean v2, p0, Lcom/google/android/gms/cast/media/o;->w:Z

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/ae;->a(Z)V

    .line 1337
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1283
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onRelease"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1285
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-boolean v1, p0, Lcom/google/android/gms/cast/media/o;->D:Z

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/media/o;Z)V

    .line 1286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    .line 1287
    return-void
.end method

.method public final a(I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1251
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onSetVolume() volume=%d"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1252
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    if-nez v0, :cond_0

    .line 1262
    :goto_0
    return-void

    .line 1257
    :cond_0
    int-to-double v0, p1

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    div-double v2, v0, v2

    .line 1258
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    iget-wide v4, p0, Lcom/google/android/gms/cast/media/o;->t:D

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/cast/b/e;->a(DDZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1259
    :catch_0
    move-exception v0

    .line 1260
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Unable to set volume: %s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 1958
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onApplicationDisconnected: statusCode=%d, sessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1960
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/ae;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1962
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/media/ae;->b(I)V

    .line 1964
    :cond_1
    return-void
.end method

.method public final a(JILorg/json/JSONObject;)V
    .locals 7

    .prologue
    .line 2228
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-object v6, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v0, Lcom/google/android/gms/cast/media/v;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/cast/media/v;-><init>(Lcom/google/android/gms/cast/media/o;JILorg/json/JSONObject;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2234
    return-void
.end method

.method final a(Lcom/google/android/gms/cast/media/w;)V
    .locals 6

    .prologue
    .line 1783
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    if-ne v0, p1, :cond_0

    .line 1784
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    .line 1786
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "removeTrackedItem() for item ID %s, load request %d, media session ID %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p1, Lcom/google/android/gms/cast/media/w;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p1, Lcom/google/android/gms/cast/media/w;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1789
    return-void
.end method

.method final a(Lcom/google/android/gms/cast/media/w;ILandroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2061
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "sendPlaybackStateForItem for item: %s, playbackState: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2063
    iget-object v0, p1, Lcom/google/android/gms/cast/media/w;->d:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    .line 2085
    :goto_0
    return-void

    .line 2067
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2068
    const-string v1, "android.media.intent.extra.ITEM_ID"

    iget-object v2, p1, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2070
    new-instance v1, Landroid/support/v7/c/b;

    invoke-direct {v1, p2}, Landroid/support/v7/c/b;-><init>(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/c/b;->a(J)Landroid/support/v7/c/b;

    move-result-object v1

    .line 2073
    if-eqz p3, :cond_1

    .line 2074
    invoke-virtual {v1, p3}, Landroid/support/v7/c/b;->a(Landroid/os/Bundle;)Landroid/support/v7/c/b;

    .line 2077
    :cond_1
    invoke-virtual {v1}, Landroid/support/v7/c/b;->a()Landroid/support/v7/c/a;

    move-result-object v1

    .line 2078
    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    iget-object v1, v1, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2081
    :try_start_0
    iget-object v1, p1, Lcom/google/android/gms/cast/media/w;->d:Landroid/app/PendingIntent;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-object v2, v2, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2082
    :catch_0
    move-exception v0

    .line 2083
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception while sending PendingIntent"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 2496
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "onSessionEnded: sessionId=%s, statusCode=%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2497
    if-nez p2, :cond_1

    move v0, v1

    .line 2499
    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x5

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/o;->c(I)V

    .line 2502
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/cast/media/o;->b(Ljava/lang/String;I)V

    .line 2504
    iget-boolean v0, p0, Lcom/google/android/gms/cast/media/o;->l:Z

    if-eqz v0, :cond_4

    .line 2505
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "shutting down mirroring"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2506
    if-eqz p2, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/o;->a(Z)V

    .line 2507
    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/o;->a()V

    .line 2513
    :cond_0
    :goto_3
    iput-object v6, p0, Lcom/google/android/gms/cast/media/o;->y:Lcom/google/android/gms/cast/media/x;

    .line 2514
    iput-object v6, p0, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;

    .line 2515
    iput-object v6, p0, Lcom/google/android/gms/cast/media/o;->d:Lcom/google/android/gms/cast/media/x;

    .line 2516
    iput-object v6, p0, Lcom/google/android/gms/cast/media/o;->e:Lcom/google/android/gms/cast/media/x;

    .line 2517
    iput-object v6, p0, Lcom/google/android/gms/cast/media/o;->f:Lcom/google/android/gms/cast/media/x;

    .line 2518
    iput-object v6, p0, Lcom/google/android/gms/cast/media/o;->g:Lcom/google/android/gms/cast/media/x;

    .line 2519
    iput-object v6, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    .line 2520
    iput-object v6, p0, Lcom/google/android/gms/cast/media/o;->v:Lcom/google/android/gms/cast/LaunchOptions;

    .line 2521
    iput-boolean v2, p0, Lcom/google/android/gms/cast/media/o;->m:Z

    .line 2522
    iput-boolean v2, p0, Lcom/google/android/gms/cast/media/o;->w:Z

    .line 2523
    return-void

    :cond_1
    move v0, v2

    .line 2497
    goto :goto_0

    .line 2499
    :cond_2
    const/4 v0, 0x6

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2506
    goto :goto_2

    .line 2509
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "detaching media channel"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2510
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "detachMediaChannel"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/e;->b(Lcom/google/android/gms/cast/internal/b;)V

    :cond_5
    iput-object v6, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    goto :goto_3
.end method

.method final a(Z)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2211
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "shutDownMirroringIfNeeded. error:%b"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2212
    iget-boolean v0, p0, Lcom/google/android/gms/cast/media/o;->l:Z

    if-eqz v0, :cond_5

    .line 2213
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->n:Lcom/google/android/gms/cast_mirroring/JGCastService;

    if-eqz v0, :cond_0

    .line 2214
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Destroying mirroring client"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2215
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->n:Lcom/google/android/gms/cast_mirroring/JGCastService;

    invoke-virtual {v0}, Lcom/google/android/gms/cast_mirroring/JGCastService;->disconnect()V

    .line 2216
    iput-object v4, p0, Lcom/google/android/gms/cast/media/o;->n:Lcom/google/android/gms/cast_mirroring/JGCastService;

    .line 2218
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->E:Lcom/google/android/gms/cast/media/al;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->E:Lcom/google/android/gms/cast/media/al;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/e;->b(Lcom/google/android/gms/cast/internal/b;)V

    :cond_1
    iput-object v4, p0, Lcom/google/android/gms/cast/media/o;->E:Lcom/google/android/gms/cast/media/al;

    .line 2219
    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-object v1, v1, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->dM:I

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-object v5, p0, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/cast/media/a;->b(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->e(Lcom/google/android/gms/cast/media/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/aa;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-interface {v0, v2, p1}, Lcom/google/android/gms/cast/media/aa;->a(Lcom/google/android/gms/cast/CastDevice;Z)V

    goto :goto_0

    .line 2220
    :cond_4
    iput-boolean v6, p0, Lcom/google/android/gms/cast/media/o;->l:Z

    .line 2222
    :cond_5
    return-void
.end method

.method public final a(D)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2048
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    if-eqz v2, :cond_0

    .line 2049
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "onVolumeChanged to %f, was %f"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v1

    iget-wide v6, p0, Lcom/google/android/gms/cast/media/o;->t:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2050
    iget-wide v2, p0, Lcom/google/android/gms/cast/media/o;->t:D

    cmpl-double v2, p1, v2

    if-eqz v2, :cond_0

    .line 2051
    iput-wide p1, p0, Lcom/google/android/gms/cast/media/o;->t:D

    .line 2055
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;Landroid/support/v7/c/w;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1441
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Received control request %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1442
    new-instance v1, Lcom/google/android/gms/cast/media/x;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/cast/media/x;-><init>(Lcom/google/android/gms/cast/media/a;Landroid/content/Intent;Landroid/support/v7/c/w;)V

    .line 1444
    const-string v2, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1446
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;)Z

    move-result v0

    .line 1449
    :cond_1
    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1968
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    if-eqz v0, :cond_0

    .line 1969
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1970
    iput-object p1, p0, Lcom/google/android/gms/cast/media/o;->b:Ljava/lang/String;

    .line 1971
    const/4 v0, 0x1

    .line 1974
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1220
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onSelect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/o;->k()V

    .line 1222
    return-void
.end method

.method public final b(I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1267
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onUpdateVolume() delta=%d"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1268
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    if-nez v0, :cond_0

    .line 1278
    :goto_0
    return-void

    .line 1273
    :cond_0
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/cast/media/o;->t:D

    int-to-double v2, p1

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    div-double/2addr v2, v4

    add-double/2addr v2, v0

    .line 1274
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    iget-wide v4, p0, Lcom/google/android/gms/cast/media/o;->t:D

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/cast/b/e;->a(DDZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1275
    :catch_0
    move-exception v0

    .line 1276
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Unable to update volume: %s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2439
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/ae;->a()Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v0

    .line 2442
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->y:Lcom/google/android/gms/cast/media/x;

    if-eqz v1, :cond_0

    .line 2443
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2444
    const-string v2, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2445
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->y:Lcom/google/android/gms/cast/media/x;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V

    .line 2446
    iput-object v3, p0, Lcom/google/android/gms/cast/media/o;->y:Lcom/google/android/gms/cast/media/x;

    .line 2450
    :cond_0
    invoke-direct {p0, p1, v4}, Lcom/google/android/gms/cast/media/o;->b(Ljava/lang/String;I)V

    .line 2452
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->u:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2453
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "attachMediaChannel"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/cast/media/p;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->C:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/cast/media/p;-><init>(Lcom/google/android/gms/cast/media/o;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/internal/b;)V

    .line 2455
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->x:Lcom/google/android/gms/cast/media/x;

    if-eqz v0, :cond_1

    .line 2456
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->x:Lcom/google/android/gms/cast/media/x;

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/x;)Z

    .line 2457
    iput-object v3, p0, Lcom/google/android/gms/cast/media/o;->x:Lcom/google/android/gms/cast/media/x;

    .line 2466
    :cond_1
    :goto_0
    iget-wide v0, p0, Lcom/google/android/gms/cast/media/o;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    if-eqz v0, :cond_2

    .line 2469
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/cast/internal/l;->a(Lcom/google/android/gms/cast/internal/o;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/media/o;->j:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2474
    :cond_2
    :goto_1
    return-void

    .line 2459
    :cond_3
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2460
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/media/o;->m:Z

    .line 2461
    new-instance v0, Lcom/google/android/gms/cast/media/s;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/e;->o()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/media/o;->C:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/cast/media/s;-><init>(Lcom/google/android/gms/cast/media/o;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->E:Lcom/google/android/gms/cast/media/al;

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->s:Lcom/google/android/gms/cast/b/e;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->E:Lcom/google/android/gms/cast/media/al;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/internal/b;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->E:Lcom/google/android/gms/cast/media/al;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/al;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Failed to send offer"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2470
    :catch_1
    move-exception v0

    .line 2471
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Exception while requesting media status"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1227
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onUnselect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1229
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/media/o;->D:Z

    .line 1230
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/o;->l()V

    .line 1231
    return-void
.end method

.method final c(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1774
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "untrackAllItems()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1775
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/w;

    .line 1776
    invoke-virtual {p0, v0, p1, v3}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;ILandroid/os/Bundle;)V

    goto :goto_0

    .line 1778
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1779
    iput-object v3, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    .line 1780
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2478
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->y:Lcom/google/android/gms/cast/media/x;

    if-eqz v0, :cond_1

    .line 2479
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->y:Lcom/google/android/gms/cast/media/x;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/x;->a(I)V

    .line 2481
    iput-object v3, p0, Lcom/google/android/gms/cast/media/o;->y:Lcom/google/android/gms/cast/media/x;

    .line 2491
    :cond_0
    :goto_0
    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/cast/media/o;->b(Ljava/lang/String;I)V

    .line 2492
    return-void

    .line 2482
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->x:Lcom/google/android/gms/cast/media/x;

    if-eqz v0, :cond_0

    .line 2483
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->x:Lcom/google/android/gms/cast/media/x;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/x;->a:Landroid/content/Intent;

    .line 2484
    if-eqz v0, :cond_2

    const-string v1, "android.media.intent.action.PLAY"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2485
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->x:Lcom/google/android/gms/cast/media/x;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/media/x;->a(I)V

    .line 2488
    :cond_2
    iput-object v3, p0, Lcom/google/android/gms/cast/media/o;->x:Lcom/google/android/gms/cast/media/x;

    goto :goto_0
.end method

.method final d(I)Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1887
    new-instance v2, Landroid/support/v7/c/y;

    invoke-direct {v2, p1}, Landroid/support/v7/c/y;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/l;->e()Lcom/google/android/gms/cast/k;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v0, v0, Lcom/google/android/gms/cast/k;->d:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v3, v2, Landroid/support/v7/c/y;->a:Landroid/os/Bundle;

    const-string v4, "queuePaused"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/support/v7/c/y;->a(J)Landroid/support/v7/c/y;

    move-result-object v0

    new-instance v2, Landroid/support/v7/c/x;

    iget-object v0, v0, Landroid/support/v7/c/y;->a:Landroid/os/Bundle;

    invoke-direct {v2, v0, v1}, Landroid/support/v7/c/x;-><init>(Landroid/os/Bundle;B)V

    .line 1892
    iget-object v0, v2, Landroid/support/v7/c/x;->a:Landroid/os/Bundle;

    return-object v0

    :cond_0
    move v0, v1

    .line 1887
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1234
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Starting mirroring on device %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1236
    iput-boolean v5, p0, Lcom/google/android/gms/cast/media/o;->l:Z

    .line 1237
    iput v5, p0, Lcom/google/android/gms/cast/media/o;->A:I

    .line 1238
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->B:Ljava/lang/String;

    .line 1240
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/o;->k()V

    .line 1241
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Stopping mirroring on device"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1245
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/o;->l()V

    .line 1246
    return-void
.end method

.method final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1340
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->h:Lcom/google/android/gms/cast/media/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/ae;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method final g()Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v0, 0x5

    const/4 v1, 0x7

    .line 1817
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/internal/l;->e()Lcom/google/android/gms/cast/k;

    move-result-object v2

    .line 1818
    if-eqz v2, :cond_1

    .line 1819
    iget v3, v2, Lcom/google/android/gms/cast/k;->d:I

    .line 1822
    iget v4, v2, Lcom/google/android/gms/cast/k;->e:I

    .line 1824
    packed-switch v3, :pswitch_data_0

    move v0, v1

    .line 1864
    :goto_0
    :pswitch_0
    new-instance v1, Landroid/support/v7/c/b;

    invoke-direct {v1, v0}, Landroid/support/v7/c/b;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/l;->b()J

    move-result-wide v4

    iget-object v0, v1, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v3, "contentDuration"

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/l;->a()J

    move-result-wide v4

    iget-object v0, v1, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v3, "contentPosition"

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Landroid/support/v7/c/b;->a(J)Landroid/support/v7/c/b;

    move-result-object v0

    .line 1870
    iget-object v1, v2, Lcom/google/android/gms/cast/k;->g:Lorg/json/JSONObject;

    invoke-static {v1}, Lcom/google/android/gms/cast/media/o;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v1

    .line 1871
    if-eqz v1, :cond_0

    .line 1872
    invoke-virtual {v0, v1}, Landroid/support/v7/c/b;->a(Landroid/os/Bundle;)Landroid/support/v7/c/b;

    .line 1875
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/c/b;->a()Landroid/support/v7/c/a;

    move-result-object v0

    .line 1876
    iget-object v0, v0, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    .line 1881
    :goto_1
    return-object v0

    .line 1826
    :pswitch_1
    packed-switch v4, :pswitch_data_1

    move v0, v1

    .line 1846
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 1833
    goto :goto_0

    .line 1836
    :pswitch_3
    const/4 v0, 0x4

    .line 1837
    goto :goto_0

    .line 1840
    :pswitch_4
    const/4 v0, 0x6

    .line 1841
    goto :goto_0

    .line 1849
    :pswitch_5
    const/4 v0, 0x1

    .line 1850
    goto :goto_0

    .line 1853
    :pswitch_6
    const/4 v0, 0x2

    .line 1854
    goto :goto_0

    .line 1857
    :pswitch_7
    const/4 v0, 0x3

    .line 1858
    goto :goto_0

    .line 1878
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "*** media status is null!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1881
    new-instance v1, Landroid/support/v7/c/b;

    invoke-direct {v1, v0}, Landroid/support/v7/c/b;-><init>(I)V

    invoke-virtual {v1}, Landroid/support/v7/c/b;->a()Landroid/support/v7/c/a;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v7/c/a;->a:Landroid/os/Bundle;

    goto :goto_1

    .line 1824
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 1826
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method final h()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1900
    iget v0, p0, Lcom/google/android/gms/cast/media/o;->A:I

    packed-switch v0, :pswitch_data_0

    .line 1915
    :goto_0
    iput v4, p0, Lcom/google/android/gms/cast/media/o;->A:I

    .line 1922
    return-void

    .line 1902
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "starting pending session for mirroring"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1903
    invoke-direct {p0, v3}, Lcom/google/android/gms/cast/media/o;->e(I)V

    goto :goto_0

    .line 1907
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "starting pending session for media with session ID %s"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/cast/media/o;->B:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1909
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1910
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->B:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/media/o;->d(Ljava/lang/String;)V

    .line 1911
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/o;->B:Ljava/lang/String;

    goto :goto_0

    .line 1913
    :cond_0
    invoke-direct {p0, v4}, Lcom/google/android/gms/cast/media/o;->e(I)V

    goto :goto_0

    .line 1900
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1925
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/o;->a(Z)V

    .line 1926
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/media/o;Z)V

    .line 1927
    return-void
.end method

.method final j()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1978
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "sendItemStatusUpdate(); current item is %s"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1979
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    if-eqz v0, :cond_3

    .line 1982
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/w;->d:Landroid/app/PendingIntent;

    .line 1984
    if-eqz v0, :cond_1

    .line 1985
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "found a PendingIntent for item %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1986
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1987
    const-string v2, "android.media.intent.extra.ITEM_ID"

    iget-object v3, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    iget-object v3, v3, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1988
    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {p0}, Lcom/google/android/gms/cast/media/o;->g()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1991
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/internal/l;->f()Lcom/google/android/gms/cast/g;

    move-result-object v2

    .line 1992
    if-eqz v2, :cond_0

    .line 1993
    invoke-static {v2}, Lcom/google/android/gms/cast/media/ak;->a(Lcom/google/android/gms/cast/g;)Landroid/os/Bundle;

    move-result-object v2

    .line 1994
    iget-object v3, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v4, "adding metadata bundle: %s"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1995
    const-string v3, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1999
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Invoking item status PendingIntent with: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2000
    iget-object v2, p0, Lcom/google/android/gms/cast/media/o;->r:Lcom/google/android/gms/cast/media/a;

    iget-object v2, v2, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2007
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    if-eqz v0, :cond_3

    .line 2008
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/l;->e()Lcom/google/android/gms/cast/k;

    move-result-object v0

    .line 2009
    if-eqz v0, :cond_2

    iget v0, v0, Lcom/google/android/gms/cast/k;->d:I

    if-ne v0, v8, :cond_3

    .line 2011
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v1, "player state is now IDLE; removing tracked item %s"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2012
    iget-object v0, p0, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;)V

    .line 2016
    :cond_3
    return-void

    .line 2001
    :catch_0
    move-exception v0

    .line 2002
    iget-object v1, p0, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "exception while sending PendingIntent"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
